package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.CarParameter

/**
 * Created by zorin.a on 021 21.02.18.
 */

data class CarModelsResponse(@SerializedName("models") val models: List<CarParameter>) : BaseResponse()