package com.wezom.taxi.driver.injection.module

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.wezom.taxi.driver.injection.ViewModelFactory
import com.wezom.taxi.driver.injection.annotation.ViewModelKey
import com.wezom.taxi.driver.presentation.acceptorder.AcceptOrderViewModel
import com.wezom.taxi.driver.presentation.addcard.AddCardViewModel
import com.wezom.taxi.driver.presentation.balance.*
import com.wezom.taxi.driver.presentation.block.BlockViewModel
import com.wezom.taxi.driver.presentation.bonus_program.BonusProgramViewModel
import com.wezom.taxi.driver.presentation.changephone.ChangePhoneViewModel
import com.wezom.taxi.driver.presentation.changephoneverification.ChangePhoneVerificationViewModel
import com.wezom.taxi.driver.presentation.completedorder.CompletedOrderViewModel
import com.wezom.taxi.driver.presentation.disabled.DisabledUserViewModel
import com.wezom.taxi.driver.presentation.discounts.DiscountsViewModel
import com.wezom.taxi.driver.presentation.ether.EtherViewModel
import com.wezom.taxi.driver.presentation.executeorder.ExecuteOrderViewModel
import com.wezom.taxi.driver.presentation.filter.CreateFilterViewModel
import com.wezom.taxi.driver.presentation.filter.EditFilterViewModel
import com.wezom.taxi.driver.presentation.filter.FilterViewModel
import com.wezom.taxi.driver.presentation.gains.GainsViewModel
import com.wezom.taxi.driver.presentation.help.HelpViewModel
import com.wezom.taxi.driver.presentation.history.OrderHistoryListViewModel
import com.wezom.taxi.driver.presentation.imagepicker.ImagePickerViwModel
import com.wezom.taxi.driver.presentation.main.MainViewModel
import com.wezom.taxi.driver.presentation.mainmap.MainMapViewModel
import com.wezom.taxi.driver.presentation.neworder.NewOrderViewModel
import com.wezom.taxi.driver.presentation.notpaid.NotPaidViewModel
import com.wezom.taxi.driver.presentation.orderfound.OrderFoundViewModel
import com.wezom.taxi.driver.presentation.photoviewer.PhotoViewModel
import com.wezom.taxi.driver.presentation.picklocation.PickLocationMapViewModel
import com.wezom.taxi.driver.presentation.picklocation.PickLocationViewModel
import com.wezom.taxi.driver.presentation.privatestatistics.PrivateStatisticsViewModel
import com.wezom.taxi.driver.presentation.profile.AboutCarViewModel
import com.wezom.taxi.driver.presentation.profile.AboutUserViewModel
import com.wezom.taxi.driver.presentation.profile.DocumentsViewModel
import com.wezom.taxi.driver.presentation.profile.ProfileViewModel
import com.wezom.taxi.driver.presentation.rating.RatingViewModel
import com.wezom.taxi.driver.presentation.refuseorder.RefuseOrderViewModel
import com.wezom.taxi.driver.presentation.refuseorder.TotalCostViewModel
import com.wezom.taxi.driver.presentation.registration.SimpleRegistrationViewModel
import com.wezom.taxi.driver.presentation.selectwaypoint.SelectWayPointViewModel
import com.wezom.taxi.driver.presentation.settings.SettingSignalViewModel
import com.wezom.taxi.driver.presentation.settings.SettingsViewModel
import com.wezom.taxi.driver.presentation.splash.SplashViewModel
import com.wezom.taxi.driver.presentation.tutorial.TutorialViewModel
import com.wezom.taxi.driver.presentation.userinfo.UserInfoViewModel
import com.wezom.taxi.driver.presentation.verification.AuthViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


@Suppress("unused")
@Module
interface ViewModelBuilder {
    @Binds
    fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    fun bindMainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    fun bindSplashViewModel(viewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TutorialViewModel::class)
    fun bindTutorialViewModel(viewModel: TutorialViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainMapViewModel::class)
    fun bindMainMapViewModel(viewModel: MainMapViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel::class)
    fun bindProfileViewModel(viewModel: ProfileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AboutUserViewModel::class)
    fun bindAboutUserViewModel(viewModel: AboutUserViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AboutCarViewModel::class)
    fun bindAboutCarViewModel(viewModel: AboutCarViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DocumentsViewModel::class)
    fun bindDocumentsViewModel(viewModel: DocumentsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddCardViewModel::class)
    fun bindAddCardViewModel(viewModel: AddCardViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AuthViewModel::class)
    fun bindVerificationPhoneViewModel(viewModel: AuthViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PhotoViewModel::class)
    fun bindPhotoViewModel(viewModel: PhotoViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OrderFoundViewModel::class)
    fun bindOrderFoundViewModel(viewModel: OrderFoundViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OrderHistoryListViewModel::class)
    fun bindOrderHistoryListViewModel(viewModel: OrderHistoryListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChangePhoneViewModel::class)
    fun bindChangePhoneViewModel(viewModel: ChangePhoneViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SettingsViewModel::class)
    fun bindSettingsViewModel(viewModel: SettingsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PrivateStatisticsViewModel::class)
    fun bindPrivateStatisticsViewModel(viewModel: PrivateStatisticsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CompletedOrderViewModel::class)
    fun bindCompletedOrderViewModel(viewModel: CompletedOrderViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BalanceViewModel::class)
    fun bindBalanceViewModel(viewModel: BalanceViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HowToReplenishViewModel::class)
    fun bindHowToReplenishViewModel(viewModel: HowToReplenishViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DiscountsViewModel::class)
    fun bindDiscountsViewModel(viewModel: DiscountsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(GainsViewModel::class)
    fun bindGainsViewModel(viewModel: GainsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DebitedBalancesViewModel::class)
    fun bindDebiteBalancesViewModel(viewModel: DebitedBalancesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ReceivedBalancesViewModel::class)
    fun bindReceivedBalancesViewModel(viewModel: ReceivedBalancesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RatingViewModel::class)
    fun bindRatingViewModel(viewModel: RatingViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HelpViewModel::class)
    fun bindHelpViewModel(viewModel: HelpViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RefuseOrderViewModel::class)
    fun bindRefuseOrderViewModel(viwModel: RefuseOrderViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NotPaidViewModel::class)
    fun bindNotPaidViewModel(viwModel: NotPaidViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TotalCostViewModel::class)
    fun bindTotalCostViewModel(viewModel: TotalCostViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ExecuteOrderViewModel::class)
    fun bindExecuteOrderViewModel(viewModel: ExecuteOrderViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NewOrderViewModel::class)
    fun bindNewOrderViewModel(viewModel: NewOrderViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChangePhoneVerificationViewModel::class)
    fun bindChangePhoneVerificationViewModel(viewModel: ChangePhoneVerificationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BlockViewModel::class)
    fun bindBlockViewModel(viewModel: BlockViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SelectWayPointViewModel::class)
    fun bindSelectWayPointViewModel(viewModel: SelectWayPointViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DisabledUserViewModel::class)
    fun bindDisabledUserViewModel(viewModel: DisabledUserViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SimpleRegistrationViewModel::class)
    fun bindSimpleRegistrationViewModel(viewModel: SimpleRegistrationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ImagePickerViwModel::class)
    fun bindImagePickerViwModel(viewModel: ImagePickerViwModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BonusProgramViewModel::class)
    fun bindBonusProgramViewModel(viewModel: BonusProgramViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EtherViewModel::class)
    fun bindEtherViewModel(viewModel: EtherViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AcceptOrderViewModel::class)
    fun bindAcceptOrderViewModel(viewModel: AcceptOrderViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UserInfoViewModel::class)
    fun bindUserInfoViewModel(viewModel: UserInfoViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FilterViewModel::class)
    fun bindFilterViewModell(viewModel: FilterViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CreateFilterViewModel::class)
    fun bindCreateFilterViewModel(viewModel: CreateFilterViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PickLocationViewModel::class)
    fun bindPickLocationViewModel(viewModel: PickLocationViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(EditFilterViewModel::class)
    fun bindEditFilterFragment(viewModel: EditFilterViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(PickLocationMapViewModel::class)
    fun bindPickLocationMapViewModel(viewModel: PickLocationMapViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(SettingSignalViewModel::class)
    fun bindSettingSignalViewModel(viewModel: SettingSignalViewModel): ViewModel
}