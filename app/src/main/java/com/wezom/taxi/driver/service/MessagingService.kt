package com.wezom.taxi.driver.service

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Handler
import android.support.v4.content.LocalBroadcastManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.presentation.main.MainActivity
import com.wezom.taxi.driver.presentation.main.events.SocketServiceResponseEvent
import com.wezom.taxi.driver.service.notification.NotificationType
import com.wezom.taxi.driver.service.notification.NotificationsUtil
import dagger.android.AndroidInjection
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by zorin.a on 28.02.2018.
 */

class MessagingService : FirebaseMessagingService() {

    @Inject
    lateinit var notifications: NotificationsUtil


    override fun onCreate() {
        AndroidInjection.inject(this)
        super.onCreate()
    }

    @SuppressLint("CheckResult")
    override fun onMessageReceived(remoteMessage: RemoteMessage?) {

        val data = remoteMessage?.data ?: return
        val typeId = data["typeId"]?.toInt() ?: return
        val pushType = NotificationType.getPushType(typeId) ?: return
        if (pushType == NotificationType.DRIVER_BLOCKED || pushType == NotificationType.UNLOCK) {
            blockedMessageToActivity(typeId)
        }

        if (pushType == NotificationType.LIMITED) {
//            if (Looper.myLooper() == null) {
//                Looper.prepare()
//            }
            Completable.fromCallable {
                Handler().post {
                    RxBus.publish(SocketServiceResponseEvent(SocketServiceResponseEvent.Result.NEGATIVE,
                            playSound = true))
                }
            }.subscribeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {},
                            Timber::e)

        }

        Timber.tag(FIREBASE_LOG_TAG).d("NOTIFICATION_PAYLOAD sound" + remoteMessage.notification!!.sound)

        for (el in data) {
            Timber.tag(FIREBASE_LOG_TAG).d("DATA_PAYLOAD Key: ${el.key} Value: ${el.value}")
        }
        //todo add timeShowAfter to new order push

        val title = data["title"] ?: ""
        val description = data["body"] ?: ""

        val notification = notifications.makeNotification(
                pushType,
                title,
                data["sound"] ?: "default",
                description,
                data
        )

        if (MainActivity.isForeground && pushType == NotificationType.NEW_ORDER_AVAILABLE) return
        else notifications.show(notification)

    }

    private fun blockedMessageToActivity(typeId: Int) {
        val intent = Intent(USER_BLOCKED_INTENT)
        intent.putExtra(TYPE_ID, typeId)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    companion object {
        const val FIREBASE_LOG_TAG = "FIREBASE_LOG"
        const val TYPE_ID = "typeId"
        const val USER_BLOCKED_INTENT = "user_blocked_intent"
    }

}