package com.wezom.taxi.driver.net.request

import com.google.gson.annotations.SerializedName

class CoordinatesOSM {
    @SerializedName("coordinates")
    var coordinates: List<PointsNumOCM>? = null


    constructor(coordinates: List<PointsNumOCM>?) {
        this.coordinates = coordinates
    }

    constructor()

}