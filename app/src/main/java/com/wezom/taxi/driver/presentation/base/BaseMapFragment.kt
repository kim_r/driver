package com.wezom.taxi.driver.presentation.base

import android.Manifest
import android.animation.ObjectAnimator
import android.animation.TypeEvaluator
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.util.Property
import android.view.View
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.*
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.common.*
import com.wezom.taxi.driver.ext.animateTo
import com.wezom.taxi.driver.ext.checkGpsEnabled
import com.wezom.taxi.driver.service.SocketService
import java.lang.ref.WeakReference
import javax.inject.Inject


/**
 * Created by zorin.a on 07.05.2018.
 */
abstract class BaseMapFragment : BaseFragment(), OnMapReadyCallback, DriverLocationListener.DriverLocationCallback {
    protected var googleMap: GoogleMap? = null
    private var mapView: MapView? = null
    private val permission = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)
    private var driverMarker: Marker? = null
    private var driverMarkerOptions: MarkerOptions? = null
    private var startPointOptions: MarkerOptions? = null
    private var middlePointOptions: MarkerOptions? = null
    private var endPointOptions: MarkerOptions? = null
    private val interpolator = LinearInterpolator()
    private var driverLocationListener: DriverLocationListener? = null
    protected var lastLocation: Location? = null

    private val needPermissionsDialog by lazy {
        android.app.AlertDialog.Builder(activity)
                .setPositiveButton(R.string.ok) { self, _ ->
                    openAppSettings()
                    self.dismiss()
                }
                .setMessage(R.string.need_permission)
                .create()
    }

    @Inject
    lateinit var locationStorage: LocationStorage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MapsInitializer.initialize(activity!!.application)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<MapView>(R.id.map)?.let {
            mapView = it
            it.onCreate(null)
            getGoogleMap()

            prepareDriverMarker()
            prepareOrderMarkers()
        }
        givenPermission(permission, LOCATION_PERMISSIONS_REQUEST) { onPermissionsGranted() }
    }

    override fun onStart() {
        super.onStart()
        mapView?.onStart()
    }

    override fun onResume() {
        super.onResume()
        mapView?.onResume()
        if (!context!!.checkGpsEnabled()) {
            showGpsDialog(context!!, fragmentManager!!)
        }
    }

    override fun onPause() {
        super.onPause()
        mapView?.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapView?.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView?.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView?.onLowMemory()
    }

    override fun onMapReady(map: GoogleMap?) {
        googleMap = map
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        grantResults.forEachIndexed { i, result ->
            if (result == PackageManager.PERMISSION_DENIED) {
                if (!shouldShowRequestPermissionRationale(permissions[i])) {
                    needPermissionsDialog.show()
                }
                return
            }
        }
        SocketService.stop(WeakReference(context!!))
        SocketService.start(WeakReference(context!!))
        onPermissionsGranted()
    }

    override fun onLocationAvailable(isAvailable: Boolean) {
    }

    override fun onLocationChanged(location: Location) {
        lastLocation = location
        locationStorage.storeLocation(location)
        // RxBus.publish(LocationEvent(location))
    }

    fun onPermissionsGranted() {
        initLocationListener()
    }

    private fun getGoogleMap() {
        mapView?.getMapAsync(this)
    }

    fun drawRoute(route: List<LatLng>?) {
        val polylineOptions = PolylineOptions()
                .color(ContextCompat.getColor(activity!!, R.color.colorDarkerGreen))
                .width(6f)
                .addAll(route)
        googleMap?.addPolyline(polylineOptions)
    }

    fun setMapStyle(isMapWhite: Boolean?) {
        if (isMapWhite == true) {
            googleMap?.setMapStyle(MapStyleOptions("[]"))
        } else {
            googleMap?.setMapStyle(MapStyleOptions.loadRawResourceStyle(activity, R.raw.map_dark_style))
        }
    }

    fun updateOrderMarkers(coordinates: List<LatLng>) {
        when (coordinates.size) {
            1 -> {
                startPointOptions?.position(coordinates[0])!!.anchor(0.5f, 1.0f)
                googleMap?.addMarker(startPointOptions)
            }
            2 -> {
                val startPoint = coordinates.first()
                val endPoint = coordinates.lastOrNull()

                startPointOptions?.position(startPoint)!!.anchor(0.5f, 1.0f)
                googleMap?.addMarker(startPointOptions)
                endPointOptions?.position(endPoint!!)!!.anchor(0.5f, 0.5f)
                googleMap?.addMarker(endPointOptions)
            }
            else -> {
                coordinates.forEachIndexed { index, it ->
                    when (index) {
                        0 -> {
                            startPointOptions?.position(it)!!.anchor(0.5f, 1.0f)
                            googleMap?.addMarker(startPointOptions)
                        }
                        coordinates.lastIndex -> {
                            endPointOptions?.position(it)!!.anchor(0.5f, 0.5f)
                            googleMap?.addMarker(endPointOptions)
                        }
                        else -> {
                            middlePointOptions?.position(it)!!.anchor(0.5f, 0.5f)
                            googleMap?.addMarker(middlePointOptions)
                        }
                    }
                }
            }
        }
    }

    fun updateStartEndOrderMarkers(coordinates: List<LatLng>) {
        when (coordinates.size) {
            1 -> {
                startPointOptions?.position(coordinates[0])!!.anchor(0.5f, 1.0f)
                googleMap?.addMarker(startPointOptions)
            }
            2 -> {
                val startPoint = coordinates.first()
                val endPoint = coordinates.lastOrNull()

                startPointOptions?.position(startPoint)!!.anchor(0.5f, 1.0f)
                googleMap?.addMarker(startPointOptions)
                endPointOptions?.position(endPoint!!)!!.anchor(0.5f, 0.5f)
                googleMap?.addMarker(endPointOptions)
            }
            else -> {
                coordinates.forEachIndexed { index, it ->
                    when (index) {
                        0 -> {
                            startPointOptions?.position(it)!!.anchor(0.5f, 1.0f)
                            googleMap?.addMarker(startPointOptions)
                        }
                        coordinates.lastIndex -> {
                            endPointOptions?.position(it)!!.anchor(0.5f, 0.5f)
                            googleMap?.addMarker(endPointOptions)
                        }
                    }
                }
            }
        }
    }

    fun isMapReady(): Boolean = googleMap != null

    fun updateDriverMarker() {
        if (lastLocation == null || googleMap == null) {
            return
        }

        if (driverMarker == null) {
            if (driverMarkerOptions == null) {
                prepareDriverMarker()
            }
            driverMarkerOptions?.position(LatLng(lastLocation!!.latitude, lastLocation!!.longitude))!!.flat(true).anchor(0.5f, 0.5f)
            driverMarker = googleMap?.addMarker(driverMarkerOptions)
        } else {
            if (lastLocation!!.hasBearing()) {
                driverMarker!!.rotation = lastLocation!!.bearing
            }
            animateMarker(driverMarker!!, lastLocation!!)
        }
    }

    fun addCarMarker() {
        if (driverMarkerOptions != null) {
            driverMarker = googleMap?.addMarker(driverMarkerOptions)
        }
    }

    fun updateDriverCamera() {
        if (lastLocation != null) {
            try {
                googleMap?.animateTo(LatLng(lastLocation!!.latitude, lastLocation!!.longitude))
            } catch (e: IllegalStateException) {
                googleMap?.setOnMapLoadedCallback {
                    googleMap?.animateTo(LatLng(lastLocation!!.latitude, lastLocation!!.longitude))
                }
            }
        }
    }

    fun updateMultipleTargetCamera(driverLocation: Location? = null, coordinates: List<LatLng>) {
        try {
            if (coordinates.size == 1) {
                googleMap?.animateTo(LatLng(coordinates[0].latitude, coordinates[0].longitude))
            } else {
                if (driverLocation != null) {
                    val allCoordinates = mutableListOf<LatLng>()
                    allCoordinates.add(LatLng(driverLocation.latitude, driverLocation.longitude))
                    allCoordinates.addAll(coordinates)
                    googleMap?.animateTo(allCoordinates)
                } else {
                    googleMap?.animateTo(coordinates)
                }
            }
        } catch (e: IllegalStateException) {
            googleMap?.setOnMapLoadedCallback {
                if (coordinates.size == 1) {
                    googleMap?.animateTo(LatLng(coordinates[0].latitude, coordinates[0].longitude))
                } else {
                    if (driverLocation != null) {
                        val allCoordinates = mutableListOf<LatLng>()
                        allCoordinates.add(LatLng(driverLocation.latitude, driverLocation.longitude))
                        allCoordinates.addAll(coordinates)
                        googleMap?.animateTo(allCoordinates)
                    } else {
                        googleMap?.animateTo(coordinates)
                    }
                }
            }
        }
    }

    fun enableMapGestures(isEnable: Boolean) {
        googleMap?.uiSettings?.setAllGesturesEnabled(isEnable)
    }

    private fun prepareDriverMarker() {
        if (driverMarkerOptions == null) {
            val carImage = BitmapDescriptorFactory.fromResource(R.drawable.ic_car)
            driverMarkerOptions = MarkerOptions().icon(carImage)
        }
    }

    private fun initLocationListener() {
        if (driverLocationListener == null)
            driverLocationListener = DriverLocationListener(context!!.applicationContext, this, this)
    }

    fun clearMap() {
        googleMap?.clear()
        driverMarker = null
        driverMarkerOptions = null
    }

    fun disableMarkerClicks() {
        googleMap?.setOnMarkerClickListener { true }
    }

    private fun prepareOrderMarkers() {
        val startPoint = BitmapDescriptorFactory.fromResource(R.drawable.passenger_point)
        val middlePoint = BitmapDescriptorFactory.fromResource(R.drawable.middle_point)
        val endPoint = BitmapDescriptorFactory.fromResource(R.drawable.driver_point)

        startPointOptions = MarkerOptions().icon(startPoint)
        middlePointOptions = MarkerOptions().icon(middlePoint)
        endPointOptions = MarkerOptions().icon(endPoint)
    }

    private fun animateMarker(marker: Marker, position: Location) {
        val finalPosition = LatLng(position.latitude, position.longitude)
        val typeEvaluator = TypeEvaluator<LatLng> { fraction, startValue, endValue ->
            interpolator.interpolate(fraction, startValue, endValue)
        }
        val property = Property.of(Marker::class.java, LatLng::class.java, "position")
        val objectAnimator = ObjectAnimator.ofObject(marker, property, typeEvaluator, finalPosition)
        objectAnimator.duration = Constants.LOCATION_UPDATE_INTERVAL
        objectAnimator.start()
    }

    class LinearInterpolator {
        fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng {
            val lat = (b.latitude - a.latitude) * fraction + a.latitude
            var lngDelta = b.longitude - a.longitude

            // Take the shortest path across the 180th meridian.
            if (Math.abs(lngDelta) > 180) {
                lngDelta -= Math.signum(lngDelta) * 360
            }
            val lng = lngDelta * fraction + a.longitude
            return LatLng(lat, lng)
        }
    }

    companion object {
        const val LOCATION_PERMISSIONS_REQUEST = 365

    }
}