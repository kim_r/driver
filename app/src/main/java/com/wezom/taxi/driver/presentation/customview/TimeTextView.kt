package com.wezom.taxi.driver.presentation.customview

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.TextView
import com.wezom.taxi.driver.databinding.ViewTimeTextBinding
import com.wezom.taxi.driver.ext.setVisible

/**
 *Created by Zorin.A on 18.June.2019.
 */
class TimeTextView : ConstraintLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context,
                attrs: AttributeSet?) : super(context,
                                              attrs)

    constructor(context: Context,
                attrs: AttributeSet?,
                attrsId: Int) : super(context,
                                      attrs,
                                      attrsId)

    private val binding = ViewTimeTextBinding.inflate(LayoutInflater.from(context),
                                                      this,
                                                      true)

    fun getText(): TextView {
        return binding.textForTime
    }

    fun setTime(time: String) {
        switchMode(isTimeVisible = true,
                   isTimeNowVisible = false)
        binding.textForTime.text = time
    }

    fun setNowTime() {
        switchMode(isTimeVisible = false,
                   isTimeNowVisible = true)
    }

    private fun switchMode(isTimeVisible: Boolean,
                           isTimeNowVisible: Boolean) {
        binding.textForTimeFrame.setVisible(isTimeVisible)
        binding.textForNow.setVisible(isTimeNowVisible)
    }
}