package com.wezom.taxi.driver.net.websocket.models

import android.arch.persistence.room.Embedded
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.Coordinate


/**
 * Created by zorin.a on 19.04.2018.
 */

data class Formula(
        @SerializedName("Opt") var opt: Double?,// грн: итоговая стоимость за все указанные пассажиру услуги
        @SerializedName("P_o") var pO: Double?, // грн: стоимость за предварительный заказ (п.11), в случае наличия такового условия
        @SerializedName("Lc") var lc: Double?, //грн: стоимость посадки
        @Embedded @SerializedName("Sm") var sm: Sm?, //итоговая стоимость за время в пути
        @Embedded @SerializedName("Sw") var sw: Sw?, //стоимость ожидания для Пассажира
        @Embedded @SerializedName("Cr") var cr: Cr?, //итоговая стоимость за пройденный километраж
        @SerializedName("minPrice") var minPrice: Int?, //минимальная стоимость проезда
        @SerializedName("use_new_formula") var enableWait: Int?, //минимальная стоимость проезда
        @SerializedName("bonuses") var bonuses: Double?, // бонусы доступные пассажиру
        @SerializedName("estimatedBonuses") var estimatedBonuses: Double?, // расчётные бонусы доступные пассажиру
        @SerializedName("allowablePrice") var allowablePrice: Int?, // допустимая разница между стоимостями
        @SerializedName("increasedPriceBy") var increasedPriceBy: Int?, // увеличение стоимости
        @SerializedName("coordinates") var coordinates: List<Coordinate>? // координаты границ города
) : Parcelable {
    constructor() : this(0.0, 0.0, 0.0, Sm(), Sw(), Cr(), 0, 0, 0.0, 0.0, 0, 0, emptyList())
    constructor(source: Parcel) : this(
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readParcelable<Sm>(Sm::class.java.classLoader),
            source.readParcelable<Sw>(Sw::class.java.classLoader),
            source.readParcelable<Cr>(Cr::class.java.classLoader),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.createTypedArrayList(Coordinate.CREATOR)
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeValue(opt)
        writeValue(pO)
        writeValue(lc)
        writeParcelable(sm, 0)
        writeParcelable(sw, 0)
        writeParcelable(cr, 0)
        writeValue(minPrice)
        writeValue(enableWait)
        writeValue(bonuses)
        writeValue(estimatedBonuses)
        writeValue(allowablePrice)
        writeValue(increasedPriceBy)
        writeTypedList(coordinates)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Formula> = object : Parcelable.Creator<Formula> {
            override fun createFromParcel(source: Parcel): Formula = Formula(source)
            override fun newArray(size: Int): Array<Formula?> = arrayOfNulls(size)
        }
    }
}

data class Sm(@SerializedName("Cf") var cf: Double? // грн: стоимость 1 минуты в пути
) : Parcelable {
    constructor() : this(0.0)
    constructor(source: Parcel) : this(
            source.readValue(Double::class.java.classLoader) as Double?
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeValue(cf)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Sm> = object : Parcelable.Creator<Sm> {
            override fun createFromParcel(source: Parcel): Sm = Sm(source)
            override fun newArray(size: Int): Array<Sm?> = arrayOfNulls(size)
        }
    }
}

data class Sw( //стоимость ожидания для Пассажира
        @SerializedName("deviateTime") var deviateTime: Long?, //время, на которое отклоняется запуск таксометра (миллисекунды)
        @SerializedName("Fw") var fw: Long?, //время бесплатного ожидания (миллисекунды)
        @SerializedName("Cm") var cm: Double?, // грн: стоимость 1 минуты платного ожидания
        @SerializedName("Wmp") var wmp: Double?, //ціна за хвилину простою
        @SerializedName("Fwt") var fwt: Double? // час безкоштовного простою
) : Parcelable {
    constructor() : this(0, 0, 0.0, 0.0, 0.0)
    constructor(source: Parcel) : this(
            source.readValue(Long::class.java.classLoader) as Long?,
            source.readValue(Long::class.java.classLoader) as Long?,
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readValue(Long::class.java.classLoader) as Double?,
            source.readValue(Double::class.java.classLoader) as Double?
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeValue(deviateTime)
        writeValue(fw)
        writeValue(cm)
        writeValue(wmp)
        writeValue(fwt)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Sw> = object : Parcelable.Creator<Sw> {
            override fun createFromParcel(source: Parcel): Sw = Sw(source)
            override fun newArray(size: Int): Array<Sw?> = arrayOfNulls(size)
        }
    }
}

data class Cr( ////итоговая стоимость за пройденный километраж
        @SerializedName("C_t") var cT: Double?, // грн: стоимость за 1 км в пределе “города”
        @SerializedName("C_tb") var cTb: Double?  //грн: стоимость за 1 км ЗА пределами “города”
) : Parcelable {
    constructor() : this(0.0, 0.0)
    constructor(source: Parcel) : this(
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readValue(Double::class.java.classLoader) as Double?
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeValue(cT)
        writeValue(cTb)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Cr> = object : Parcelable.Creator<Cr> {
            override fun createFromParcel(source: Parcel): Cr = Cr(source)
            override fun newArray(size: Int): Array<Cr?> = arrayOfNulls(size)
        }
    }
}