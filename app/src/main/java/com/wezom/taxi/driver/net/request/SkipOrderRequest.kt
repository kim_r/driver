package com.wezom.taxi.driver.net.request

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by zorin.a on 01.06.2018.
 */
class SkipOrderRequest(@SerializedName("eventTime") val eventTime: Long?):Serializable