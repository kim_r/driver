package com.wezom.taxi.driver.presentation.base

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import android.content.SharedPreferences
import com.google.android.gms.maps.model.LatLng
import com.wezom.taxi.driver.ext.IS_MAP_WHITE
import com.wezom.taxi.driver.ext.boolean
import com.wezom.taxi.driver.common.decodePolyline
import com.wezom.taxi.driver.net.api.ApiManager
import com.wezom.taxi.driver.net.request.CoordinatesOSM
import com.wezom.taxi.driver.net.request.PointsNumOCM
import com.wezom.taxi.driver.net.response.PointsOCM
import com.wezom.taxi.driver.net.response.RouteResponse
import com.wezom.taxi.driver.net.response.RouteResponseOCM
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import timber.log.Timber

/**
 * Created by zorin.a on 11.05.2018.
 */
abstract class BaseMapViewModel(screenRouterManager: ScreenRouterManager,
                                private val apiManager: ApiManager,
                                sharedPreferences: SharedPreferences) : BaseViewModel(screenRouterManager) {

    var routeLiveData = MutableLiveData<List<LatLng>>()
    var isMapWhite: Boolean by sharedPreferences.boolean(IS_MAP_WHITE, true)
    val isMapWhiteThemeLiveData = MutableLiveData<Boolean>()

    @SuppressLint("CheckResult")
    fun getMapGoogle(origin: String, destination: String) {
        apiManager.getMapRoute(origin, destination).subscribe({ response ->
            App.instance.getLogger()!!.log("getMapRoute suc ")
            if (response.status.equals("ok", true)) {
                val decodedPoints = getPolyline(response)
                routeLiveData.postValue(decodedPoints)
            }
        }, { t ->
            App.instance.getLogger()!!.log("getMapRoute error ")
            Timber.d(t)
        })
    }

    @SuppressLint("CheckResult")
    fun getMapGoogleManyPoints(origin: String, destination: String, points: String) {
        apiManager.getMapRoute(origin, destination, points).subscribe({ response ->
            App.instance.getLogger()!!.log("getMapRoute suc ")
            if (response.status.equals("ok", true)) {
                val decodedPoints = getPolyline(response)
                routeLiveData.postValue(decodedPoints)
            }
        }, { t ->
            App.instance.getLogger()!!.log("getMapRoute error ")
            Timber.d(t)
        })
    }

    fun getSubPoint(coordinates: List<LatLng>): String {
        val sub = coordinates.subList(1, coordinates.lastIndex)
        val str = StringBuilder()
        sub.forEach { it ->
            str.append("${it.latitude},${it.longitude}|")
        }
        return str.toString()
    }


    fun getMapRoute(coordinates: List<LatLng>) {
        if (coordinates.size <= 1) return
        when (coordinates.size) {
            1 -> {
                //just skip
            }
            2 -> {
                val coordinatesToOCM: MutableList<PointsNumOCM> = ArrayList()
                coordinatesToOCM.add(PointsNumOCM(coordinates[0].longitude, coordinates[0].latitude, 0))
                coordinatesToOCM.add(PointsNumOCM(coordinates.last().longitude, coordinates.last().latitude, 1))
                App.instance.getLogger()!!.log("getMapRouteOSM start ")
                apiManager.getMapRouteOCM(CoordinatesOSM(coordinatesToOCM)).subscribe({ response ->
                    if (response.isSuccess!!) {
                        val decodedPoints = getPolylineOSM(response)
                        routeLiveData.postValue(decodedPoints)
                        App.instance.getLogger()!!.log("getMapRouteOSM suc ")
                    } else {
                        App.instance.getLogger()!!.log("getMapRouteOSM error ")
                        getMapGoogle("${coordinates[0].latitude}, ${coordinates[0].longitude}",
                                "${coordinates.last().latitude}, ${coordinates.last().longitude}")
                    }
                }, { t ->
                    App.instance.getLogger()!!.log("getMapRouteOSM error ")
                    getMapGoogle("${coordinates[0].latitude}, ${coordinates[0].longitude}",
                            "${coordinates.last().latitude}, ${coordinates.last().longitude}")
                })
            }
            else -> {
                val coordinatesToOCM: MutableList<PointsNumOCM> = ArrayList()
                coordinates.forEachIndexed { index, it ->
                    coordinatesToOCM.add(PointsNumOCM(it.longitude, it.latitude, index))
                }
                App.instance.getLogger()!!.log("getMapRouteOSM start ")
                apiManager.getMapRouteOCM(CoordinatesOSM(coordinatesToOCM)).subscribe({ response ->
                    if (response.isSuccess!!) {
                        val decodedPoints = getPolylineOSM(response)
                        routeLiveData.postValue(decodedPoints)
                        App.instance.getLogger()!!.log("getMapRouteOSM suc ")
                    } else {
                        App.instance.getLogger()!!.log("getMapRouteOSM error ")
                        getMapGoogleManyPoints("${coordinates[0].latitude}, ${coordinates[0].longitude}",
                                "${coordinates.last().latitude}, ${coordinates.last().longitude}", getSubPoint(coordinates))
                    }
                }, { t ->
                    App.instance.getLogger()!!.log("getMapRouteOSM error ")
                    getMapGoogleManyPoints("${coordinates[0].latitude}, ${coordinates[0].longitude}",
                            "${coordinates.last().latitude}, ${coordinates.last().longitude}", getSubPoint(coordinates))
                })

            }
        }
    }

    fun switchMapStyle() {
        isMapWhite = !isMapWhite
        setMapStyle()
    }

    fun setMapStyle() {
        isMapWhiteThemeLiveData.postValue(isMapWhite)
    }

    private fun getPolylineOSM(response: RouteResponseOCM): List<LatLng> {
        val encoded: MutableList<PointsOCM> = response.routeOCM!!.pointsOCM!!.toMutableList()
        val coordinates: MutableList<LatLng> = ArrayList()
        for (point in encoded) {
            coordinates.add(LatLng(point.latitude!!, point.longitude!!))
        }
        return coordinates
    }

    private fun getPolyline(response: RouteResponse): List<LatLng> {
        val encoded = response.routes!![0].overviewPolyline!!.points
        val decodedPoints = decodePolyline(encoded!!)
        return decodedPoints
    }
}