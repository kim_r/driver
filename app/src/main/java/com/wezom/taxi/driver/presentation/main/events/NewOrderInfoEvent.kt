package com.wezom.taxi.driver.presentation.main.events

import com.wezom.taxi.driver.data.models.NewOrderInfo

/**
 * Created by zorin.a on 21.05.2018.
 */
data class NewOrderInfoEvent constructor(val orderInfo: NewOrderInfo)