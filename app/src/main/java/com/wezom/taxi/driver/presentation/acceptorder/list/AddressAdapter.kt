package com.wezom.taxi.driver.presentation.acceptorder.list

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.graphics.Color
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatDelegate
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.data.models.Address
import com.wezom.taxi.driver.databinding.ItemAddressFirstBinding
import com.wezom.taxi.driver.databinding.ItemAddressLastBinding
import com.wezom.taxi.driver.databinding.ItemAddressMiddleBinding
import com.wezom.taxi.driver.ext.THEME_MODE
import javax.inject.Inject
import kotlin.properties.Delegates

/**
 *Created by Zorin.A on 19.June.2019.
 */
class AddressAdapter @Inject constructor() : RecyclerView.Adapter<AddressAdapter.AddressViewHolder>() {

    var callback: ((position: Int, address: Address) -> Unit)? = null
    private var content: Pair<MutableList<Address>, FromToDistance> by Delegates.observable(Pair(mutableListOf(),
            FromToDistance()),
            { _, _, newValue ->
                emptyLiveData.value =
                        newValue.first.size
                notifyDataSetChanged()
            })
    val emptyLiveData: MutableLiveData<Int> = MutableLiveData()

    fun addAll(data: Pair<MutableList<Address>, FromToDistance>) {
        if (data.first.size == 1)
            data.first.add(Address("-1", "", 0.0, 0.0, -1, ""))
        content = data
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): AddressViewHolder {
        val inflater = LayoutInflater.from(parent?.context)
        val binding = DataBindingUtil.inflate<ViewDataBinding>(inflater,
                viewType,
                parent,
                false)
        return AddressViewHolder(binding)
    }

    override fun onBindViewHolder(holder: AddressViewHolder,
                                  position: Int) {
        holder?.bind(content.first[position],
                content.second)
        holder?.callback = callback
    }

    override fun getItemCount(): Int = content.first.size


    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> {
                R.layout.item_address_first
            }
            content.first.size - 1 -> {
                R.layout.item_address_last
            }
            else -> {
                R.layout.item_address_middle
            }
        }
    }


    class AddressViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

        public fun getThemeMode(context: Context): Int {
            val themeMode = PreferenceManager.getDefaultSharedPreferences(context)
                    .getInt(THEME_MODE,
                            AppCompatDelegate.MODE_NIGHT_YES)
            return themeMode
        }

        var callback: ((position: Int, address: Address) -> Unit)? = null

        @SuppressLint("SetTextI18n")
        fun bind(address: Address,
                 fromTo: FromToDistance) {


            when (binding) {
                is ItemAddressFirstBinding -> {
                    binding.run {
                        if (getThemeMode(binding.fromText.context) == 2) {
                            fromText.setTextColor(Color.parseColor("#e0e0e0"))
                            fromDistanceValue.setTextColor(Color.parseColor("#e0e0e0"))
                            fromDistanceMeasure.setTextColor(Color.parseColor("#e0e0e0"))
                        }
                        fromText.text = formatAddress(address)
////                        if (fromTo.from.equals("0.0") || fromTo.from.equals(",0")) {
//                            fromDistanceMeasure.setVisible(false)
//                            fromArrowImage.setVisible(false)
//                            fromDistanceValue.setVisible(false)
//                        } else
                        fromDistanceValue.text = fromTo.from
                    }
                }
                is ItemAddressMiddleBinding -> {
                    binding.run {
                        if (getThemeMode(binding.fromText.context) == 2) {
                            fromText.setTextColor(Color.parseColor("#e0e0e0"))
                        }
                        fromText.text = formatAddress(address)
                    }
                }
                is ItemAddressLastBinding -> {
                    binding.run {
                        if (getThemeMode(binding.toText.context) == 2) {
                            toText.setTextColor(Color.parseColor("#e0e0e0"))
                            toDistanceValue.setTextColor(Color.parseColor("#e0e0e0"))
                            toDistanceMeasure.setTextColor(Color.parseColor("#e0e0e0"))
                        }
                        if (address.id.equals("-1")) {
                            toText.setText(com.wezom.taxi.driver.R.string.around_town)
                            toDistanceValue.visibility = View.GONE
                            toDistanceMeasure.visibility = View.GONE
                            toArrowImage.visibility = View.GONE
                        } else {
                            toText.text = formatAddress(address)
                            toDistanceValue.text = fromTo.to

                        }
                    }
                }
            }
        }
    }

    companion object {
        fun formatAddress(address: Address): String {
            val entrance = address.entrance
            return if (!entrance.isNullOrEmpty() && !address.name.equals(entrance)) {
                "${address.name}, п $entrance" //V - значит вендетта, п - значит ПОДЪЕЗД
            } else {
                "${address.name}"
            }
        }
    }

    class FromToDistance(val from: String? = "0",
                         val to: String? = "0")
}