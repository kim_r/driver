package com.wezom.taxi.driver.presentation.main.events

import com.wezom.taxi.driver.data.models.map.Hexagon

class HexagonEvent constructor(val hexagon: Hexagon)