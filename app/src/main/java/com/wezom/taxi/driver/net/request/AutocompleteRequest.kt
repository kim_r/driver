package com.wezom.taxi.driver.net.request

/**
 *Created by Zorin.A on 01.August.2019.
 */
data class AutocompleteRequest(val query: String,
                               val lang: String?,
                               val lat: Double?,
                               val lon: Double?)