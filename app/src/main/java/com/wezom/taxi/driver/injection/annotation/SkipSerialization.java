package com.wezom.taxi.driver.injection.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Created by zorin.a on 20.04.2018.
 */

@Target(ElementType.FIELD)
public @ interface SkipSerialization {
}

