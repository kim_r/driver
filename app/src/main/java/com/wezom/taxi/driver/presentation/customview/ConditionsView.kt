package com.wezom.taxi.driver.presentation.customview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.common.dpToPx
import com.wezom.taxi.driver.data.models.ActionCondition
import com.wezom.taxi.driver.data.models.Condition
import com.wezom.taxi.driver.databinding.BonusConditionsView

/**
 * Created by zorin.a on 3/11/19.
 */
class ConditionsView : LinearLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context,
                attrs: AttributeSet?) : super(context,
                                              attrs)

    constructor(context: Context,
                attrs: AttributeSet?,
                attributeSetId: Int) : super(context,
                                             attrs,
                                             attributeSetId)

    private val binding = BonusConditionsView.inflate(LayoutInflater.from(context),
                                                      this,
                                                      true)

    private val itemParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
                                                      )

    init {
        itemParams.topMargin = dpToPx(8)
    }

    fun setActionConditionData(data: List<ActionCondition>?) {
        if (data == null) return
        binding.run {

            data.forEachIndexed { index, actionCondition ->
                val item = ActionConditionItem(context)
                when (index) {
                    0 -> {
                        item.setData(Triple(actionCondition,
                                            "${context.getString(R.string.accepted_orders_2)}: > ${actionCondition.descriptionValue}%",
                                            "${actionCondition.percentOfCompleteon}%"))
                    }
                    1 -> {
                        item.setData(Triple(actionCondition,
                                            "${context.getString(R.string.completed_orders2)}: > ${actionCondition.descriptionValue}%",
                                            "${actionCondition.percentOfCompleteon}%"))
                    }
                    2 -> {
                        item.setData(Triple(actionCondition,
                                            "${context.getString(R.string.driver_rating)}: > ${actionCondition.descriptionValue}",
                                            "${actionCondition.percentOfCompleteon}"))
                    }
                }
                conditionsContainer.addView(item,
                                            itemParams)
            }
        }
    }

    fun setDrivesConditionData(data: List<Condition>?) {
        if (data == null) return
        var isRewardedExists = false
        var rewardItemIndex = 0
        data.forEachIndexed { index, it ->
            it.state = ConditionState.HIDDEN
            if (it.isDone) {
                isRewardedExists = true
                rewardItemIndex = index
            }
        }

        binding.run {
            data.forEachIndexed { index, it ->
                if (isRewardedExists) {
                    if (index == rewardItemIndex) {
                        it.state = ConditionState.APPROVED
                    } else {
                        it.state = ConditionState.HIDDEN
                    }
                } else {
                    it.state = ConditionState.REJECTED
                }
                val item = BonusProgramDrivesItem(context)
                item.setData(it)
                conditionsContainer.addView(item,
                                            itemParams)
            }
        }
    }

    enum class ConditionState {
        HIDDEN,
        REJECTED,
        APPROVED
    }
}
