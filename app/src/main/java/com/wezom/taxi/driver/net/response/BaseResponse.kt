package com.wezom.taxi.driver.net.response

import android.support.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.Error

/**
 * Created by zorin.a on 020 20.02.18.
 */
@Keep
open class BaseResponse {
    @SerializedName("success")
    var isSuccess: Boolean? = null
    @SerializedName("error")
    var error: Error? = null


    constructor(isSuccess: Boolean?, error: Error?){
        this.isSuccess = isSuccess
        this.error = error
    }

    constructor()
}