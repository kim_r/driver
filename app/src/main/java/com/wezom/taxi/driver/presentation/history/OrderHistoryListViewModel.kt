package com.wezom.taxi.driver.presentation.history

import android.arch.lifecycle.MutableLiveData
import com.wezom.taxi.driver.common.COMPLETED_ORDER_SCREEN
import com.wezom.taxi.driver.data.models.ResponseState
import com.wezom.taxi.driver.data.models.Trip
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.repository.DriverRepository
import javax.inject.Inject

/**
 * Created by Andrew on 13.03.2018.
 */
class OrderHistoryListViewModel @Inject constructor(screenRouterManager: ScreenRouterManager, private val repository: DriverRepository) : BaseViewModel(screenRouterManager) {

    var ordersLiveData = MutableLiveData<List<Trip>>()

    fun getHistoryList(limit: Int, offset: Int) {
        loadingLiveData.postValue(ResponseState(ResponseState.State.LOADING))
        App.instance.getLogger()?.log("getCompletedOrders start ")
        disposable.add(repository.getCompletedOrders(limit, offset)
                .subscribe({ response ->
                    App.instance.getLogger()?.log("getCompletedOrders suc ")
                    handleResponseState(response)
                    response?.isSuccess?.let { isSuccess ->
                        if (isSuccess) ordersLiveData.postValue(response.trips)
                    }
                }, {
                    App.instance.getLogger()?.log("getCompletedOrders error ")
                    loadingLiveData.postValue(ResponseState(ResponseState.State.NETWORK_ERROR))
                }))
    }

    fun openOrder(id: Int) {
        switchScreen(COMPLETED_ORDER_SCREEN, id)
    }
}