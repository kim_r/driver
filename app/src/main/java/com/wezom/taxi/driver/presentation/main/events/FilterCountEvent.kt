package com.wezom.taxi.driver.presentation.main.events

class FilterCountEvent constructor(val count: Int)