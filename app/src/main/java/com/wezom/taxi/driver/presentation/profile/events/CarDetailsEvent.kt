package com.wezom.taxi.driver.presentation.profile.events

import com.wezom.taxi.driver.data.models.CarParameter

/**
 * Created by zorin.a on 04.04.2018.
 */
class CarDetailsEvent constructor(
        val brand: CarParameter? = null,
        val model: CarParameter? = null,
        val type: ArrayList<Int>? = null,
        val year: Int? = null,
        val color: CarParameter? = null,
        val number: String? = null,
        val photo: String? = null)