package com.wezom.taxi.driver.data.models.notifications

class DownloadingMissingPhoto(val typeId: String, val title: String, val body: String): BasicNotification