package com.wezom.taxi.driver.data.models
import com.google.gson.annotations.SerializedName


/**
 * Created by zorin.a on 022 22.02.18.
 */

data class OrderOptions(
		@SerializedName("id") val id: Int?,
		@SerializedName("name") val name: String?,
		@SerializedName("icon") val icon: String?)