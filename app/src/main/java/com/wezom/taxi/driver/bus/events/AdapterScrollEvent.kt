package com.wezom.taxi.driver.bus.events

class AdapterScrollEvent constructor(val position: Int)