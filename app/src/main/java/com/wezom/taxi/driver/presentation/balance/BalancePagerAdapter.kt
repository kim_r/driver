package com.wezom.taxi.driver.presentation.balance

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

/**
 * Created by andre on 21.03.2018.
 */
class BalancePagerAdapter(fm: FragmentManager?) : FragmentPagerAdapter(fm) {

    private var fragments: MutableList<Fragment> = ArrayList(3)
    private var titles: MutableList<String> = ArrayList(3)

    override fun getItem(position: Int): Fragment = fragments[position]
    override fun getCount(): Int = fragments.size
    override fun getPageTitle(position: Int): CharSequence? = titles[position]

    fun addEntry(toPos: Int, fragment: Fragment, title: String) {
        fragments.add(toPos, fragment)
        titles.add(toPos, title)
    }

    companion object {
        const val RECEIVED_ENTRY = 0
        const val DEBITED_ENTRY = 1
    }
}