package com.wezom.taxi.driver.data.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


/**
 * Created by Andrew on 13.03.2018.
 */
data class Evos(
        @SerializedName("isUsed") val isUsed: Boolean,
        @SerializedName("info") val info: String,
        @SerializedName("phone") val phone: String
) : Parcelable {
    constructor(source: Parcel) : this(
            1 == source.readInt(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt((if (isUsed) 1 else 0))
        writeString(info)
        writeString(phone)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Evos> = object : Parcelable.Creator<Evos> {
            override fun createFromParcel(source: Parcel): Evos = Evos(source)
            override fun newArray(size: Int): Array<Evos?> = arrayOfNulls(size)
        }
    }
}
        