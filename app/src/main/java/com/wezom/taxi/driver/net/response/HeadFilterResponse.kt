package com.wezom.taxi.driver.net.response

import android.support.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.Error

@Keep
open class HeadFilterResponse {
    @SerializedName("success")
    val isSuccess: Boolean? = null
    @SerializedName("error")
    val error: Error? = null
    @SerializedName("result")
    val result: List<FilterResponse>? = null

}