package com.wezom.taxi.driver.ext

import android.app.*
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.PorterDuff
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v4.app.NotificationCompat
import android.support.v4.content.FileProvider
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import com.wezom.taxi.driver.BuildConfig
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.common.Constants.REQUEST_IMAGE_CAPTURE
import com.wezom.taxi.driver.common.Constants.REQUEST_IMAGE_PICK
import com.wezom.taxi.driver.common.createImageFile
import com.wezom.taxi.driver.presentation.base.BaseFragment
import com.wezom.taxi.driver.service.SocketService
import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


/**
 * Created by zorin.a on 023 23.02.18.
 */

fun Any.shortToast(context: Context?) = Toast.makeText(context,
                                                       this.toString(),
                                                       Toast.LENGTH_SHORT).show()

fun Any.longToast(context: Context?) = Toast.makeText(context,
                                                      this.toString(),
                                                      Toast.LENGTH_LONG).show()


inline fun <reified T : ViewModel> ViewModelProvider.getViewModelOfType(): T = get(T::class.java)

inline fun <reified T : ViewModel> BaseFragment.getViewModel(): T {
    return ViewModelProviders.of(this,
                                 viewModelFactory).get(T::class.java)
}

inline fun <reified T> toArray(list: List<String?>): Array<T> = (list as List<T>).toTypedArray()

infix fun View.setVisible(isVisible: Boolean) {
    this.visibility = if (isVisible) VISIBLE else GONE
}

fun Activity?.hideKeyboard() = this?.currentFocus?.run {
    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
    imm?.hideSoftInputFromWindow(currentFocus.windowToken,
                                 0)
}

fun Activity?.showKeyboard() = this?.currentFocus?.run {
    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
    imm?.showSoftInput(currentFocus,
                       0)
}

fun Fragment?.takePicture(request: Int? = null): File? {
    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
    var photoFile: File? = null
    val photoURI: Uri
    try {
        photoFile = createImageFile(this?.activity)
    } catch (ex: IOException) {
        ex.printStackTrace()
    }

    if (photoFile != null) {
        photoURI = FileProvider.getUriForFile(this?.activity!!,
                                              BuildConfig.APPLICATION_ID + ".provider",
                                              photoFile)
        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI)
        if (intent.resolveActivity(this.activity?.packageManager) != null) {
            this.startActivityForResult(intent,
                                        request ?: REQUEST_IMAGE_CAPTURE)
        }
    }
    return photoFile
}

fun Fragment?.pickPicture(request: Int? = null) {
    val photoPickerIntent = Intent(Intent.ACTION_PICK)
    photoPickerIntent.type = "image/*"
    this?.startActivityForResult(photoPickerIntent,
                                 request ?: REQUEST_IMAGE_PICK)
}

fun String?.toRequestBody(): RequestBody = RequestBody.create(MediaType.parse("text/plain"),
                                                              this!!)

fun File?.toRequestBody(): RequestBody = RequestBody.create(MediaType.parse("image/jpeg"),
                                                            this!!)

fun File?.putBitmap(bitmap: Bitmap?) {
    val bos = ByteArrayOutputStream()
    bitmap?.compress(Bitmap.CompressFormat.JPEG,
                     100,
                     bos)
    val bitmapData = bos.toByteArray()
    FileOutputStream(this).use {
        it.write(bitmapData)
    }
}

fun Bitmap.toByteArray(): ByteArray {
    ByteArrayOutputStream().use {
        this.compress(Bitmap.CompressFormat.PNG,
                      90,
                      it)
        return it.toByteArray()
    }
}

//fun Activity.startLocationUpdates() {
//    val i = Intent(this, LocationService::class.java)
//    this.startService(i)
//}
//
//fun Activity.stopLocationUpdates() {
//    val i = Intent(this, LocationService::class.java)
//    this.stopService(i)
//}

fun Activity.connectSockets() {
    val i = Intent(this,
                   SocketService::class.java)
    this.startService(i)
}

fun Activity.diconnectSockets() {
    val i = Intent(this,
                   SocketService::class.java)
    this.stopService(i)
}

fun Context.checkGpsEnabled(): Boolean {
    val lm = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    return lm.isProviderEnabled(LocationManager.GPS_PROVIDER) || lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
}

fun Service.showServiceNotification(channelId: String,
                                    title: String,
                                    text: String): Notification {
    val nm = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val channel = NotificationChannel(channelId,
                                          this.getString(R.string.app_name),
                                          NotificationManager.IMPORTANCE_DEFAULT).apply {
            enableLights(false)
            enableVibration(false)
            setSound(null,
                     null)
        }
        nm.createNotificationChannel(channel)
    }

    return NotificationCompat.Builder(this,
                                      channelId)
        .setSmallIcon(R.mipmap.ic_launcher)
        .setAutoCancel(true)
        .setPriority(NotificationCompat.PRIORITY_HIGH)
        .setCategory(Notification.CATEGORY_SERVICE)
        .setContentTitle(title)
        .setContentText(text)
        .build()

}

fun EditText.isEmpty() = this.text.toString().isEmpty()

fun Double.roundTwoDecimal() = Math.round(this.times(100.0)).div(100.0)

fun View.setErrorColor() {
    val background = this.background
    background.setColorFilter(resources.getColor(R.color.colorRed),
                              PorterDuff.Mode.SRC_ATOP)
    this.background = background
}





