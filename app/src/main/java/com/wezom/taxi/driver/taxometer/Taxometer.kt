package com.wezom.taxi.driver.taxometer

import android.location.Location
import com.wezom.taxi.driver.data.models.ChangedParams
import com.wezom.taxi.driver.data.models.Coordinate
import com.wezom.taxi.driver.data.models.OnTheWay
import io.reactivex.disposables.Disposable
import timber.log.Timber
import java.util.*
import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.TimeUnit
import kotlin.math.roundToLong


/**
 * Created by zorin.a on 08.05.2018.
 */
class Taxometer constructor(private val firstLocation: OnTheWay,
                            private val mode: TaxometerMode,
                            private val pricePerMinInStop: Double,
                            public val timeToStopFree: Long,
                            public val timeToWaitFree: Long,
                            private val activationTime: Long, //Время запуска таксометра (для просчёта простоя, если есть)
                            private val loadingPrice: Double, //погрузка
                            private val pricePerMinAwaiting: Double, //когда ждём пассажира (цена за 1 минуту)
                            private val pricePerMinInTrip: Double,
                            private val pricePerKmInsideCity: Double,
                            private val pricePerKmOutsideCity: Double,
                            private val priceForAdditionalServices: Double,
                            private val allowablePercent: Int, //процент разницы
                            private val coefficient: Double, // текущий коэфициент по заказу
                            private val bonuses: Double, // бонусы пользователя
                            private val preOrderPrice: Double, //доплата за предзаказ
                            private val estimatedCost: Int, //цена, расчитанная сервером по гугл карте. (Целое для простоты)
                            private val estimatedCostBonusless: Int, //цена, расчитанная сервером без учёта бонусов  (Целое для простоты)
                            private val minPrice: Int, //минимальная стоимость проезда
                            private val calculatedTime: Long, //теоретическое время поездки
                            private val calculatedPath: Double, // протяженность рута
                            private val listener: TaxometerResultListener? = null,
                            coordinateUpdateTime: Long, //отправка координат
                            private var increasedPriceBy: Int = 0

) {
    private var isInTrip: Boolean = false //состояние, когда стоял в режиме DELAYED_START, потом поехал
    private var factTimeInTrip: Long = 0
    private var passedDistanceFull: Double = 0.0
    private var passedDistanceInsideCity: Double = 0.0
    private var passedDistanceOutsideCity: Double = 0.0
    private var timeTillActualDrive: Long = 0 //время начала реального пути, если ждал в режиме DELAYED_START (нажал OK через диалог)
    private var finalCostForTraveledDistance: Double = 0.0
    private var costForTraveledDistanceInsideCity: Double = 0.0
    private var costForTraveledDistanceOutsideCity: Double = 0.0
    private var finalCostForTraveledTime: Double = 0.0
    private var finalCostForStopTime: Long = 0
    private var finalCostForAwaiting: Double = 0.0
    private var resultF1: Double = 0.0
    private var resultF2: Double = 0.0
    private var resultF3: Double = 0.0
    private var factCost: Double = 0.0
    private var resultCost: Double = 0.0
    private var location: Location? = null
    private var route: CopyOnWriteArrayList<Coordinate> = CopyOnWriteArrayList()
    private var disposable: Disposable? = null
    private var changedParams = ChangedParams.NO_CHANGES
    private var isEstimatedCostTaken: Boolean? = null

    init {
        if (firstLocation.latitude != 0.0 && firstLocation.longitude != 0.0) {
            addLocalion(firstLocation.latitude, firstLocation.longitude)
        }
    }

    fun restoreTaxometr(resultF3: Double,
                        factCost: Double,
                        resultCost: Double,
                        factTimeInTrip: Long,
                        passedDistanceFull: Double,
                        changedParams: ChangedParams,
                        isEstimatedCostTaken: Boolean) {
        this.resultF3 = resultF3
        this.factCost = factCost
        this.resultCost = resultCost
        this.factTimeInTrip = factTimeInTrip
        this.changedParams = changedParams
        this.passedDistanceFull = passedDistanceFull
        this.isEstimatedCostTaken = isEstimatedCostTaken
    }


    init {
        Timber.d("******************* INIT *******************")
        Timber.d("mode $mode")
        Timber.d("activationTime ${Date(activationTime)}")
        Timber.d("loadingPrice $loadingPrice")
        Timber.d("pricePerMinAwaiting $pricePerMinAwaiting")
        Timber.d("pricePerMinInTrip $pricePerMinInTrip")
        Timber.d("pricePerKmInsideCity $pricePerKmInsideCity")
        Timber.d("pricePerKmOutsideCity $pricePerKmOutsideCity")
        Timber.d("priceForAdditionalServices $priceForAdditionalServices")
        Timber.d("allowablePercent $allowablePercent")
        Timber.d("coefficient $coefficient")
        Timber.d("bonuses $bonuses")
        Timber.d("preOrderPrice $preOrderPrice")
        Timber.d("estimatedCost $estimatedCost")
        Timber.d("minPrice $minPrice")

//        disposable = Observable.interval(0, coordinateUpdateTime, TimeUnit.MILLISECONDS).subscribe {
//            if (location != null)
//                addLocalion(location?.latitude!!, location?.longitude!!)
//        }
    }

    fun addLocalion(latitude: Double, longitude: Double) {
        if (latitude == 0.0 && longitude == 0.0) return
        for (loc in route) {
            if (loc.latitude == latitude && loc.longitude == longitude) {
                return
            }
        }
        route.add(Coordinate(latitude = latitude, longitude = longitude,
                degree = 0))
    }

    fun getRoute(): CopyOnWriteArrayList<Coordinate> {
        return route
    }

    fun initRoute(routeFromDb: List<Coordinate>) {
        route.clear()
        route.addAll(routeFromDb)
    }

    fun updateData(isInsideCity: Boolean,
                   isStartInsideCity: Boolean,
                   currentSpeed: Double,
                   currentPriceWait: Long,
                   currentTimeWait: Long,
                   passedDistanceFull: Double,
                   passedDistanceInsideCity: Double,
                   passedDistanceOutsideCity: Double,
                   location: Location?,
                   lastLocalDistance: Float,
                   isCalcFinalResult: Boolean,
                   isCalcFinalResultFactCost: Boolean) {

        this.passedDistanceInsideCity = passedDistanceInsideCity
        this.passedDistanceOutsideCity = passedDistanceOutsideCity
        this.location = location

        this.passedDistanceFull = passedDistanceFull


        when (mode) {
            TaxometerMode.INSTANT_START -> {
                finalCostForTraveledDistance = calcPriceForTraveledDistance(isInsideCity, isStartInsideCity)
                finalCostForTraveledTime = calcPriceForTimeInTrip()
            }
            TaxometerMode.DELAYED_START -> {
                if (currentSpeed >= MIN_SPEED_TO_START && !isInTrip) {
                    timeTillActualDrive = calcDelayedTimeTillStart()
                    finalCostForAwaiting = calcPriceForDelayedTimeTillStart(timeTillActualDrive)
                    isInTrip = true
                }
                if (isInTrip) {
                    finalCostForTraveledDistance = calcPriceForTraveledDistance(isInsideCity, isStartInsideCity)
                    finalCostForTraveledTime = calcPriceForTimeInTrip()
                }
            }
        }

        Timber.d("******************* UPDATE *******************")
        Timber.d("IS INSIDE CITY = $isInsideCity")
        Timber.d("mode $mode")
        Timber.d("activationTime ${Date(activationTime)}")
        Timber.d("loadingPrice $loadingPrice")
        Timber.d("pricePerMinAwaiting $pricePerMinAwaiting")
        Timber.d("pricePerMinInTrip $pricePerMinInTrip")
        Timber.d("pricePerKmInsideCity $pricePerKmInsideCity")
        Timber.d("pricePerKmOutsideCity $pricePerKmOutsideCity")
        Timber.d("priceForAdditionalServices $priceForAdditionalServices")
        Timber.d("allowablePercent $allowablePercent")
        Timber.d("coefficient $coefficient")
        Timber.d("bonuses $bonuses")
        Timber.d("preOrderPrice $preOrderPrice")
        Timber.d("estimatedCost $estimatedCost")
        Timber.d("estimatedCostBonusless $estimatedCostBonusless")
        Timber.d("minPrice $minPrice")
        Timber.d("factTimeInTrip $factTimeInTrip")
        Timber.d("passedDistanceFull ${this.passedDistanceFull}")
        Timber.d("passedDistanceOutsideCity ${this.passedDistanceOutsideCity}")
        Timber.d("passedDistanceInsideCity ${this.passedDistanceInsideCity}")
        Timber.d("costForTraveledDistanceInsideCity $costForTraveledDistanceInsideCity")
        Timber.d("costForTraveledDistanceOutsideCity $costForTraveledDistanceOutsideCity")
        Timber.d("finalCostForTraveledDistance $finalCostForTraveledDistance")
        Timber.d("finalCostForTraveledTime $finalCostForTraveledTime")


        resultF1 = finalCostForTraveledDistance + finalCostForTraveledTime + loadingPrice + preOrderPrice + priceForAdditionalServices
        Timber.d("resultF1 $resultF1")
        resultF2 = if (resultF1 <= minPrice) minPrice.toDouble() else resultF1
        Timber.d("resultF2 $resultF2")
        resultF3 = resultF2 * coefficient  //fact cost without bonus
        resultF3 = resultF3 + increasedPriceBy //add price from client
        factCost = resultF3 - bonuses
        resultF3 = calcFinalResult(resultF3, estimatedCostBonusless.toDouble(), allowablePercent, isCalcFinalResult, isCalcFinalResultFactCost)
        Timber.d("resultF3 $resultF3")
        Timber.d("factCost $factCost")

//        factCost =

        resultCost = resultF3 + currentPriceWait //додавання вартість очікування, завжди окремо не залежно мінімальна чи це вартість


        Timber.d("resultCost $resultCost")

        listener?.onResult(
                if (resultCost >= 0) resultCost else 0.0,
                if (factCost >= 0) factCost else 0.0,
                estimatedCost,
                Math.round(resultF3).toInt(),
                this.passedDistanceFull,
                route,
                TimeUnit.MINUTES.toMillis(factTimeInTrip),
                currentTimeWait,
                currentPriceWait.toDouble(),
                changedParams,
                isEstimatedCostTaken,
                passedDistanceInsideCity,
                passedDistanceOutsideCity
        )
    }

    fun updateData(currentMoneyWait: Long, currentTimeWait: Long,
                   isCalcFinalResult: Boolean,
                   isCalcFinalResultFactCost: Boolean) {


        Timber.d("currentTineToStop $currentMoneyWait")

        resultF1 = finalCostForTraveledDistance + finalCostForTraveledTime + loadingPrice + preOrderPrice + priceForAdditionalServices
        Timber.d("resultF1 $resultF1")
        resultF2 = if (resultF1 <= minPrice) minPrice.toDouble() else resultF1
        Timber.d("resultF2 $resultF2")
        resultF3 = resultF2 * coefficient  //fact cost without bonus
        resultF3 = resultF3 + increasedPriceBy //add price from client
        factCost = resultF3 - bonuses
        resultF3 = calcFinalResult(resultF3, estimatedCostBonusless.toDouble(), allowablePercent, isCalcFinalResult, isCalcFinalResultFactCost)
        Timber.d("resultF3 $resultF3")
        Timber.d("factCost $factCost")

        resultCost = resultF3 + currentMoneyWait //додавання вартість очікування, завжди окремо не залежно мінімальна чи це вартість

        Timber.d("resultCost $resultCost")

        listener?.onResult(
                if (resultCost >= 0) resultCost else 0.0,
                if (factCost >= 0) factCost else 0.0,
                estimatedCost,
                Math.round(resultF3).toInt(),
                this.passedDistanceFull,
                route,
                TimeUnit.MINUTES.toMillis(factTimeInTrip),
                currentTimeWait,
                currentMoneyWait.toDouble(),
                changedParams,
                isEstimatedCostTaken,
                passedDistanceInsideCity,
                passedDistanceOutsideCity
        )
    }


    fun stop() {
        disposable?.dispose()
    }

    private fun calcPriceForTraveledDistance(isInsideCity: Boolean, isStartInsideCity: Boolean): Double {
        costForTraveledDistanceInsideCity = passedDistanceInsideCity * pricePerKmInsideCity
        if (isStartInsideCity && isInsideCity)
            costForTraveledDistanceOutsideCity = passedDistanceOutsideCity * pricePerKmInsideCity
        else
            costForTraveledDistanceOutsideCity = passedDistanceOutsideCity * pricePerKmOutsideCity

        Timber.d("IN CITY: $isStartInsideCity $isInsideCity")
        Timber.d("DIS IN CITY: $pricePerKmInsideCity  DIS OUT CITY: $pricePerKmOutsideCity")
        Timber.d("DIS IN: $costForTraveledDistanceOutsideCity")
        return costForTraveledDistanceInsideCity + costForTraveledDistanceOutsideCity
    }

    private fun calcPriceForTimeInTrip(): Double {
        factTimeInTrip = TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - activationTime)
        return factTimeInTrip * pricePerMinInTrip
    }

    private fun calcPriceForStop(secondToStop: Long): Long {
        val house = secondToStop / 3600
        val minut: Long = (secondToStop - 3600 * house) / 60
        return (pricePerMinInStop * minut).roundToLong()
    }

    private fun calcPriceForDelayedTimeTillStart(timeTillActualDrive: Long): Double {
        return pricePerMinAwaiting * timeTillActualDrive
    }

    private fun calcDelayedTimeTillStart(): Long {
        return TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - activationTime)
    }

    private fun calcFinalResult(factCostBonusless: Double, estimatedCostBonusless: Double, allowablePercent: Int,
                                isCalcFinalResult: Boolean,
                                isCalcFinalResultFactCost: Boolean): Double {
        val estPercentValue = estimatedCostBonusless.times(allowablePercent).div(100)

        val costDifference = Math.abs(estimatedCostBonusless - factCostBonusless)
        return if ((costDifference <= estPercentValue || (estimatedCostBonusless > factCostBonusless && isCalcFinalResultFactCost)) && isCalcFinalResult) {
            if (isEstimatedCostTaken != true)
                isEstimatedCostTaken = true
            changedParams = ChangedParams.NO_CHANGES
            estimatedCost.toDouble()
        } else {
            Timber.d("take factCost: $factCost")
            if (isEstimatedCostTaken != false)
                isEstimatedCostTaken = false
            defineChangedParams()
            factCost
        }
    }

//    private fun calcFinalResult(factCost: Double, estimatedCost: Double, allowablePercent: Int): Double {
//        val diffValue = estimatedCost.times(allowablePercent).div(100)
//        val minBound = estimatedCost - diffValue
//        val maxBound = estimatedCost + diffValue
//
//        Timber.d("minBound: $minBound")
//        Timber.d("maxBound: $maxBound")
//
//        return if (factCost in minBound..maxBound) {
//            Timber.d("take estimatedCost: $estimatedCost")
//            if (isEstimatedCostTaken != true)
//                isEstimatedCostTaken = true
//            changedParams = ChangedParams.NO_CHANGES
//            estimatedCost
//        } else {
//            Timber.d("take factCost: $factCost")
//            if (isEstimatedCostTaken != false)
//                isEstimatedCostTaken = false
//            defineChangedParams()
//            factCost
//        }
//    }

    private fun defineChangedParams() {
        val timeChanged = factTimeInTrip != calculatedTime
        val pathLengthChanged = passedDistanceFull != calculatedPath

        if (timeChanged && pathLengthChanged) {
            changedParams = ChangedParams.DISTANCE_AND_TIME
        }
        if (timeChanged && !pathLengthChanged) {
            changedParams = ChangedParams.TIME
        }
        if (!timeChanged && pathLengthChanged) {
            changedParams = ChangedParams.DISTANCE
        }
        if (!timeChanged && !pathLengthChanged) {
            changedParams = ChangedParams.NO_CHANGES
        }
    }

    companion object {
        const val MIN_SPEED_TO_START = 5
        fun checkIsInsideCity(point: Location, polygon: List<Coordinate>?): Boolean {
            if (polygon == null) return true
            val sides = polygon.size
            var j = sides - 1
            var oddNodes = false
            for (i in 0 until sides) {
                if (polygon[i].latitude!! < point.latitude && polygon[j].latitude!! >= point.latitude ||
                        polygon[j].latitude!! < point.latitude && polygon[i].latitude!! >= point.latitude) {
                    if (polygon[i].longitude!! + (point.latitude - polygon[i].latitude!!) /
                            (polygon[j].latitude!! - polygon[i].latitude!!) *
                            (polygon[j].longitude!! - polygon[i].longitude!!) < point.longitude) {
                        oddNodes = !oddNodes
                    }
                }
                j = i
            }
            return oddNodes
        }
    }

    enum class TaxometerMode { INSTANT_START, DELAYED_START }

    interface TaxometerResultListener {
        fun onResult(resultPrice: Double,
                     factResultPrice: Double,
                     estimatedCost: Int,
                     resultF3: Int,
                     passedDistance: Double,
                     route: CopyOnWriteArrayList<Coordinate>, factTimeMillis: Long,
                     waitingTime: Long,
                     waitingCost: Double,
                     changedParams: ChangedParams,
                     estimatedCostTaken: Boolean?,
                     passedDistanceInsideCity: Double,
                     passedDistanceOutsideCity: Double)
    }
}