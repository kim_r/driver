package com.wezom.taxi.driver.injection.component

import android.app.Application
import com.wezom.taxi.driver.injection.module.*
import com.wezom.taxi.driver.injection.scope.AppScope
import com.wezom.taxi.driver.presentation.app.App
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

/**
 * Created by zorin.a on 16.02.2018.
 */

@AppScope
@Component(
        modules = [
            AndroidSupportInjectionModule::class,
            AppModule::class,
            ActivityBuilder::class,
            ViewModelBuilder::class,
            ServiceBuilder::class,
            NetworkModule::class,
            DbModule::class]
)
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bindApplication(application: Application): Builder

        fun build(): AppComponent
    }

    override fun inject(app: App)
}