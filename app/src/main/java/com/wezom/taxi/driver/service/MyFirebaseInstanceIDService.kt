package com.wezom.taxi.driver.service

import android.preference.PreferenceManager
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService
import com.wezom.taxi.driver.ext.FIREBASE_TOKEN_KEY
import timber.log.Timber

/**
 * Created by zorin.a on 28.02.2018.
 */
open class MyFirebaseInstanceIDService : FirebaseInstanceIdService() {
    override fun onTokenRefresh() {
        super.onTokenRefresh()
        val refreshedToken = FirebaseInstanceId.getInstance().token
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        val preferences = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        preferences.edit().putString(FIREBASE_TOKEN_KEY, refreshedToken).apply()
        Timber.d("onTokenRefresh: token - $refreshedToken")
    }
}