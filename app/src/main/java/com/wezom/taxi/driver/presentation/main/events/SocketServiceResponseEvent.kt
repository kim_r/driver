package com.wezom.taxi.driver.presentation.main.events

/**
 * Created by zorin.a on 26.04.2018.
 */
class SocketServiceResponseEvent constructor(
    val result: Result,
    val playSound: Boolean = true
) {
    enum class Result { POSITIVE, NEGATIVE }
}