package com.wezom.taxi.driver.net.jobqueue.jobs

import android.annotation.SuppressLint
import com.wezom.taxi.driver.net.request.AcceptOrderRequest

/**
 * Created by zorin.a on 04.05.2018.
 */
class AcceptOrderJob constructor(private val id: Int, private val request: AcceptOrderRequest) : BaseJob() {

    @SuppressLint("CheckResult")
    override fun onRun() {
        apiManager.acceptOrder(id, request).blockingGet() //don nothing on error
    }
}