package com.wezom.taxi.driver.presentation.privatestatistics.dialog

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import com.wezom.taxi.driver.R


/**
 * Created by zorin.a on 05.03.2018.
 */
class StatisticsInfoDialog : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(context!!, R.style.BlackDialogTheme)
                .setTitle(R.string.information)
                .setMessage(context!!.getString(R.string.private_statistics_info))
                .setPositiveButton(R.string.ok, { dialogInterface, which -> this.dismiss() })
                .create()
    }

    companion object {
        val TAG: String = StatisticsInfoDialog.javaClass.simpleName
    }
}