package com.wezom.taxi.driver.net.request

import com.google.gson.annotations.SerializedName

/**
 * Created by zorin.a on 020 20.02.18.
 */

data class SendPhoneRequest(@SerializedName("phone") var phone: String?,
                            @SerializedName("language") var language: Int?=null)