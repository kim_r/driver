package com.wezom.taxi.driver.net.request

import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.SendHelpTimeType
import java.io.Serializable


/**
 * Created by zorin.a on 05.07.2018.
 */

data class SendHelpTimeRequest(@SerializedName("type") val type: SendHelpTimeType): Serializable