package com.wezom.taxi.driver.presentation.privatestatistics.list

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import com.wezom.taxi.driver.data.models.Comment
import com.wezom.taxi.driver.databinding.CommentItemBinding
import com.wezom.taxi.driver.presentation.base.lists.ItemModel
import java.text.SimpleDateFormat

/**
 * Created by zorin.a on 15.03.2018.
 */

class CommentsItemView : ConstraintLayout, ItemModel<Comment> {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, attributeSetId: Int) : super(context, attrs, attributeSetId)

    private val binding = CommentItemBinding.inflate(LayoutInflater.from(context), this, true)

    override fun setData(data: Comment) {
        binding.run {
            userName.text = data.userName
            ratingBar.rating = data.rating!!.toFloat()
            comment.text = data.comment
            date.text = SimpleDateFormat("dd.MM.yyyy").format(data.time)

        }
    }


}