package com.wezom.taxi.driver.bus.events

/**
 * Created by zorin.a on 12.06.2018.
 */
class ShowDialogEvent(val title: String? = null, val message: String, val action: (() -> Unit)? = null)