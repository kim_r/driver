package com.wezom.taxi.driver.presentation.profile

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

/**
 * Created by zorin.a on 023 23.02.18.
 */
class ProfilePagerAdapter(fm: FragmentManager?) : FragmentPagerAdapter(fm) {

    private var fragments: MutableList<Fragment> = ArrayList(3)
    private var titles: MutableList<String> = ArrayList(3)

    override fun getItem(position: Int): Fragment = fragments[position]
    override fun getCount(): Int = fragments.size
    override fun getPageTitle(position: Int): CharSequence? = titles[position]

    fun addEntry(toPos: Int, fragment: Fragment, title: String) {
        fragments.add(toPos, fragment)
        titles.add(toPos, title)
    }

    companion object {
        const val ABOUT_USER_ENTRY = 0
        const val ABOUT_CAR_ENTRY = 1
        const val DOCUMENTS_ENTRY = 2
    }
}