package com.wezom.taxi.driver.presentation.imagepicker

/**
 * Created by zorin.a on 19.12.2018.
 */
data class ImagePickerData constructor(val title: Int,
                                       val description: Int,
                                       val requirementsTitle: Int? = null,
                                       val requirements: List<Int>? = null,
                                       val source: String? = null,
                                       val placeholderRes: Int? = null,
                                       val isShowDescriptionTitle: Boolean = true
) {
}