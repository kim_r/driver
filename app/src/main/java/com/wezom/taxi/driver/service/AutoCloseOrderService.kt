package com.wezom.taxi.driver.service

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.os.IBinder
import android.util.Log
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.bus.events.AutoCloseOrderEvent
import com.wezom.taxi.driver.bus.events.ResultDataEvent
import com.wezom.taxi.driver.common.BeepUtil
import com.wezom.taxi.driver.common.isPreAndroidO
import com.wezom.taxi.driver.data.models.NewOrderInfo
import com.wezom.taxi.driver.ext.showServiceNotification
import com.wezom.taxi.driver.taxometer.Action
import com.wezom.taxi.driver.taxometer.Command
import dagger.android.DaggerService
import timber.log.Timber
import java.lang.Exception
import java.lang.ref.WeakReference
import javax.inject.Inject

class AutoCloseOrderService : DaggerService() {

    @Inject
    lateinit var context: Context

    private var handler: Handler = Handler()

    private var orderInfo: NewOrderInfo? = null
    private var fiftenMin = 900000L

    override fun onBind(intent: Intent?): IBinder? = null

    var timeWait: Long = 0L

    override fun onCreate() {
        super.onCreate()
        showNotification()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Timber.d("TAXOMETER onStartCommand")
        intent!!.extras?.getParcelable<Command>(KEY_ACTION)?.let { it ->
            val command = it
            command.data?.let {
                orderInfo = command.data
            }
        }

        if (orderInfo!!.order!!.calculatedTime!! < -1) {
            //якщо очікування пройшло
            orderInfo!!.order!!.calculatedTime = 1
        }

        if (orderInfo!!.order!!.calculatedTime == 0L) {
            //якщо сервер незміг прорахувати неохідний час для очікування. Додаємо + 5 хв
            orderInfo!!.order!!.calculatedTime = 300
        }


        timeWait = 0

        timeWait = ((((orderInfo!!.order!!.calculatedTime!!) + (orderInfo!!.order!!.durationCalculated!! * 60)) * 1000)).toLong()
        if (orderInfo!!.order!!.durationCalculated == 0.0) {
            timeWait = fiftenMin * 8
        }
//        timeWait = 20000
        Log.d("AutoClose", "time ${orderInfo!!.order!!.calculatedTime} ${orderInfo!!.order!!.durationCalculated} $timeWait ")

        startTimer()
        return Service.START_REDELIVER_INTENT
    }

    override fun onDestroy() {
        super.onDestroy()
        handler.removeCallbacks(runnable)
        BeepUtil.stop()
    }


    private var runnable: Runnable = Runnable {
        try {
            RxBus.publish(AutoCloseOrderEvent())
        } catch (e: Exception) {
            e.printStackTrace()
        }
//        BeepUtil.playMassage()
        timeWait = fiftenMin * 4
        startTimer()
    }

    private fun startTimer() {
        handler.postDelayed(runnable, timeWait)
    }

    private fun showNotification() {
        val notification = this.showServiceNotification(
                TAXI_TAXOMETER_CHANNEL,
                getString(R.string.taxometer), getString(R.string.time_order)
        )
        startForeground(NOTIFICATION_ID, notification)
    }

    companion object {
        private const val KEY_ACTION = "action"
        const val NOTIFICATION_ID = 201
        const val TAXI_TAXOMETER_CHANNEL = "taxi taxometer"
        var result: ResultDataEvent? = null

        fun start(context: WeakReference<Context>, orderInfo: NewOrderInfo) {
            val intent = Intent(context.get(), AutoCloseOrderService::class.java)
            intent.putExtra(KEY_ACTION, Command(Action.START, orderInfo))
            launch(context, intent)
        }

        private fun launch(context: WeakReference<Context>, intent: Intent) {
            if (isPreAndroidO()) {
                context.get()?.startService(intent)
            } else {
                context.get()?.startForegroundService(intent)
            }
        }
    }
}