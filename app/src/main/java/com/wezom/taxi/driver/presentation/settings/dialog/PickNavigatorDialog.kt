package com.wezom.taxi.driver.presentation.settings.dialog

import android.app.Dialog
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.databinding.NavigatorSwitchDialogBinding
import android.content.Intent
import android.net.Uri
import com.wezom.taxi.driver.common.Constants.GOOGLE_NAV_PACKAGE_NAME
import com.wezom.taxi.driver.common.Constants.GOOGLE_NAV_MARKET_REDIRECT
import com.wezom.taxi.driver.common.Constants.YANDEX_NAV_PACKAGE_NAME
import com.wezom.taxi.driver.common.Constants.YANDEX_NAV_MARKET_REDIRECT
import com.wezom.taxi.driver.common.isNavigatorInstalled


/**
 * Created by udovik.s on 14.03.2018.
 */
class PickNavigatorDialog : DialogFragment() {

    private lateinit var binding: NavigatorSwitchDialogBinding
    var listener: NavigatorClickListener? = null
    private var pos: Int? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        pos = arguments?.getInt(CHECKED_ITEM_KEY)
        binding = DataBindingUtil.inflate(activity!!.layoutInflater, R.layout.navigator_switch_dialog, null, false)
        updateUi()
        val builder = AlertDialog.Builder(context!!, R.style.WhiteDialogTheme)
        builder.setView(binding.root)

        return builder.create()
    }

    fun updateUi() {
        binding.run {
            title.text = getString(R.string.choose_navigator)
            when (pos) {
                GOOGLE_NAV -> googleNav.isChecked = true
                YANDEX_NAV -> yandexNav.isChecked = true
            }
            container.setOnCheckedChangeListener { group, checkedId ->
                when (checkedId) {
                    googleNav.id -> {
                        listener?.onNavigatorSelected(GOOGLE_NAV)
                        if (isNavigatorInstalled(context!!, GOOGLE_NAV_PACKAGE_NAME)) {
                            dismiss()
                        } else {
                            val intent = Intent(Intent.ACTION_VIEW)
                            intent.data = Uri.parse(GOOGLE_NAV_MARKET_REDIRECT)
                            startActivity(intent)
                        }
                    }
                    yandexNav.id -> {
                        listener?.onNavigatorSelected(YANDEX_NAV)
                        if (isNavigatorInstalled(context!!, YANDEX_NAV_PACKAGE_NAME)) {
                            dismiss()
                        } else {
                            val intent = Intent(Intent.ACTION_VIEW)
                            intent.data = Uri.parse(YANDEX_NAV_MARKET_REDIRECT)
                            startActivity(intent)
                        }
                    }
                }
            }
        }
    }

    interface NavigatorClickListener {
        fun onNavigatorSelected(position: Int)
    }

    companion object {
        val TAG: String = PickNavigatorDialog::class.java.simpleName
        private const val CHECKED_ITEM_KEY: String = "checked_item_key"

        const val GOOGLE_NAV = 0
        const val YANDEX_NAV = 1
        fun newInstance(checkedItem: Int? = 0): PickNavigatorDialog {
            val args = Bundle()
            val fragment = PickNavigatorDialog()
            checkedItem?.let { args.putInt(CHECKED_ITEM_KEY, it) }
            fragment.arguments = args
            return fragment
        }
    }
}