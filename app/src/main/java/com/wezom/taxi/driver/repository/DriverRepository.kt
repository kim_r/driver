package com.wezom.taxi.driver.repository

import com.wezom.taxi.driver.data.models.OrderFinalization
import com.wezom.taxi.driver.data.models.RateRequest
import com.wezom.taxi.driver.net.api.ApiManager
import com.wezom.taxi.driver.net.request.*
import com.wezom.taxi.driver.net.response.BaseResponse
import com.wezom.taxi.driver.net.response.HeadFilterResponse
import com.wezom.taxi.driver.net.response.HeadTypeCarResponse
import com.wezom.taxi.driver.net.response.RequiredDocumentsResponse
import io.reactivex.Single
import okhttp3.MultipartBody
import javax.inject.Inject

/**
 * Created by zorin.a on 020 20.02.18.
 */

class DriverRepository @Inject constructor(private val apiManager: ApiManager) {

    fun sendPhone(phone: String) = apiManager.sendPhone(SendPhoneRequest(phone))

    fun confirmCode(phone: String,
                    code: Int) = apiManager.confirmCode(ConfirmCodeRequest(phone,
            code))

    fun carModels(brandId: Int) = apiManager.carModels(brandId)

    fun carBrands() = apiManager.carBands()

    fun carColors() = apiManager.carColors()

    fun carIssueYears(modelId: Int) = apiManager.carIssueYears(modelId)

    fun existedProfileFields() = apiManager.existedProfileFields()

    fun userRegistration(request: RegistrationRequest): Single<BaseResponse> = apiManager.userRegistration(request)

    fun registrationDocument(userImageBody: MultipartBody.Part?,
                             carImageBody: MultipartBody.Part?,
                             doc1ImageBody: MultipartBody.Part?,
                             doc2ImageBody: MultipartBody.Part?,
                             doc3ImageBody: MultipartBody.Part?,
                             doc4ImageBody: MultipartBody.Part?) = apiManager.registrationDocument(userImageBody,
            carImageBody,
            doc1ImageBody,
            doc2ImageBody,
            doc3ImageBody,
            doc4ImageBody)

    fun requiredDocsList() = apiManager.requiredDocsList()

    fun profileAndRegistrationCorrectness() = apiManager.profileAndRegistrationCorrectness()

    fun checkUserStatus() = apiManager.checkUserStatus()

    fun changeCard(number: Long,
                   expMonth: Int,
                   expYear: Int,
                   cardCvv: Int,
                   name: String) = apiManager.changeCard(ChangeCardRequest(number,
            expMonth,
            expYear,
            cardCvv,
            name))

    fun acceptOrder(id: Int,
                    request: AcceptOrderRequest) = apiManager.acceptOrder(id,
            request)

    fun skipOrder(id: Int,
                  request: SkipOrderRequest) = apiManager.skipOrder(id,
            request)

    fun getCurrentOrders() = apiManager.currentOrders()

    fun refuseOrder(id: Int,
                    request: RefuseOrderRequest) = apiManager.refuseOrder(id,
            request)

    fun refuseReasons(stageId: Int) = apiManager.refuseReasons(stageId)

    fun getNotPaidReasons() = apiManager.getNotPaidReasons()

    //TODO: delete if not needed by the end of development
//    fun arrivedAtPlace(orderInfo: Int, lon: Double? = null, lat: Double? = null, eventTime: Long) = apiManager.arrivedAtPlace(orderInfo, DriverEventRequest(lon, lat, eventTime)
//
//            fun driverOnTheWay (orderInfo: Int) = apiManager.driverOnTheWay(orderInfo, )

    fun getCompletedOrders(limit: Int?,
                           offset: Int?) = apiManager.getCompletedOrders(limit,
            offset)

    fun getCompletedOrder(orderId: Int) = apiManager.getCompletedOrder(orderId)

    fun getPersonalStatistics() = apiManager.getPersonalStatistics()

    fun getDiscounts() = apiManager.getDiscounts()

    fun getDebitedBalances(startTime: Long,
                           finishTime: Long) = apiManager.getDebitedBalances(startTime,
            finishTime)

    fun getReceivedBalances(startTime: Long,
                            finishTime: Long) = apiManager.getReceivedBalances(startTime,
            finishTime)

    fun getIncomes(startDate: Long,
                   endDate: Long) = apiManager.getIncomes(startDate,
            endDate)

    fun rateTrip(id: Int,
                 rate: Int,
                 comment: String? = null) = apiManager.rateTrip(id,
            RateRequest(rate,
                    comment))

    fun changePhone(phone: String) = apiManager.changePhone(phone)

    fun changePhoneVerification(phone: String,
                                code: Int) = apiManager.changePhoneVerification(phone,
            code)

    fun getHelpEmail() = apiManager.getHelpEmail()

    fun getCardVerificationInfo() = apiManager.getCardVerificationInfo()

    fun getRequiredDocuments(): Single<RequiredDocumentsResponse> = apiManager.getRequiredDocuments()

    fun sendDeviceKey(request: SendDeviceKeyRequest) = apiManager.sendDeviceKey(request)

    fun finalizeOrder(orderId: Int,
                      request: OrderFinalization): Single<BaseResponse> = apiManager.finalizeOrder(orderId,
            request)

    fun forceCloseOrder(orderId: Int): Single<BaseResponse> = apiManager.forceCloseOrder(orderId)

    fun saveFilter(filter: CreateFilterRequest): Single<BaseResponse> = apiManager.saveFilter(filter)

    fun editFilter(filterId: Int, filter: CreateFilterRequest): Single<BaseResponse> = apiManager.editFilter(filterId, filter)

    fun getFilter(): Single<HeadFilterResponse> = apiManager.getFilter()

    fun deleteFilter(filterId: Int): Single<HeadFilterResponse> = apiManager.deleteFilter(filterId)

    fun getType(): Single<HeadTypeCarResponse> = apiManager.getType()

    fun activateFilter(filterId: Int): Single<HeadFilterResponse> = apiManager.activateFilter(filterId);

    fun deactivateFilter(filterId: Int): Single<HeadFilterResponse> = apiManager.deactivateFilter(filterId);

    fun setMoveCar(): Single<BaseResponse> = apiManager.setMoveCar()

    fun setMoveStopCar(): Single<BaseResponse> = apiManager.setMoveStopCar()

}