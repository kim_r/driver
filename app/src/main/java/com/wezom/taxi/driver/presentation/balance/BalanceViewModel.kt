package com.wezom.taxi.driver.presentation.balance

import android.arch.lifecycle.MutableLiveData
import android.content.SharedPreferences
import com.wezom.taxi.driver.common.HOW_TO_REPLENISH_SCREEN
import com.wezom.taxi.driver.ext.USER_BALANCE
import com.wezom.taxi.driver.common.getDaysDatesOfWeek
import com.wezom.taxi.driver.ext.string
import com.wezom.taxi.driver.data.models.DatesOfWeekModel
import com.wezom.taxi.driver.data.models.ResponseState
import com.wezom.taxi.driver.data.models.ResponseState.State.*
import com.wezom.taxi.driver.net.api.ApiManager
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import io.reactivex.rxkotlin.plusAssign
import javax.inject.Inject

/**
 * Created by zorin.a on 15.03.2018.
 */
class BalanceViewModel @Inject constructor(screenRouterManager: ScreenRouterManager,
                                           sharedPreferences: SharedPreferences,
                                           private val apiManager: ApiManager) : BaseViewModel(screenRouterManager) {

    val balanceLiveData: MutableLiveData<Double> = MutableLiveData()
    val datesOfWeekLiveData: MutableLiveData<DatesOfWeekModel> = MutableLiveData()

    private var userBalance: String by sharedPreferences.string(key = USER_BALANCE, default = "00.00")

    fun switchToHowToReplenish() {
        switchScreen(HOW_TO_REPLENISH_SCREEN)
    }

    fun getUserStatus() {
        loadingLiveData.postValue(ResponseState(LOADING))
        App.instance.getLogger()!!.log("checkCurrentBalance start ")
        disposable += apiManager.checkCurrentBalance()
                .subscribe({ it ->
                    App.instance.getLogger()!!.log("checkCurrentBalance suc ")
                    handleResponseState(it)
                    if (it!!.isSuccess!!) {
                        it.balance?.let {
                            userBalance = it.toString()
                            balanceLiveData.postValue(it)
                        }
                    }
                }, {
                    App.instance.getLogger()!!.log("checkCurrentBalance error ")
                    loadingLiveData.postValue(ResponseState(NETWORK_ERROR))
                })
    }

    fun getDatesOfWeek(week: Int) {
        datesOfWeekLiveData.postValue(getDaysDatesOfWeek(week))
    }
}