package com.wezom.taxi.driver.presentation.customview

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.support.v7.widget.Toolbar
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewTreeObserver
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.common.BeepUtil
import com.wezom.taxi.driver.databinding.ToolbarBinding
import com.wezom.taxi.driver.ext.setVisible
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.main.events.*
import com.wezom.taxi.driver.presentation.main.events.SocketServiceEvent.SocketCommand.START
import com.wezom.taxi.driver.presentation.main.events.SocketServiceEvent.SocketCommand.STOP
import com.wezom.taxi.driver.presentation.main.events.SocketServiceResponseEvent.Result.NEGATIVE
import com.wezom.taxi.driver.presentation.main.events.SocketServiceResponseEvent.Result.POSITIVE
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxCompoundButton
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber

/**
 * Created by zorin.a on 022 22.02.18.
 */

class DriverToolbar : Toolbar {

    constructor(context: Context) : super(context)
    constructor(context: Context,
                attrs: AttributeSet?) : super(context,
            attrs)

    constructor(context: Context,
                attrs: AttributeSet?,
                attributeSetId: Int) : super(context,
            attrs,
            attributeSetId)

    val binding: ToolbarBinding = ToolbarBinding.inflate(LayoutInflater.from(context),
            this,
            true)
    private var disposable = CompositeDisposable()
    val onlineStateLiveData = MutableLiveData<SocketServiceResponseEvent>()

    init {
        val listener = object : ViewTreeObserver.OnGlobalLayoutListener {
            @SuppressLint("ClickableViewAccessibility")
            override fun onGlobalLayout() {
                Timber.d("TOOLBAR SOCKET IS ONLINE onGlobalLayout(): $IS_ONLINE_STATE")
                binding.toolbar.viewTreeObserver.removeOnGlobalLayoutListener(this)
                setOnlineState(IS_ONLINE_STATE)
            }
        }
        binding.toolbar.viewTreeObserver.addOnGlobalLayoutListener(listener)

        Timber.d("TOOLBAR SOCKET IS ONLINE init{}: $IS_ONLINE_STATE")

        disposable += RxView.clicks(binding.toolbarSwitch)
                .subscribe {
                    Timber.d("TOOLBAR SOCKET IS ONLINE init click{}: $IS_ONLINE_STATE")
                    Timber.d("state: isChecked = $IS_ONLINE_STATE")
                    IS_ONLINE_STATE = !IS_ONLINE_STATE
                    App.instance.getLogger()!!.log("TOOLBAR SOCKET IS ONLINE $IS_ONLINE_STATE (USER_CLICK)")
                    if (IS_ONLINE_STATE) {
                        RxBus.publish(SocketServiceEvent(START))
                    } else {
                        RxBus.publish(SocketServiceEvent(STOP))
                    }
                }

        disposable += RxView.clicks(binding.newOrder)
                .subscribe {
                    RxBus.publish(ShowSecondaryOrderEvent())
                }

        disposable += RxCompoundButton.checkedChanges(binding.toolbarSwitch)
                .subscribe { isChecked ->
                    binding.toolbarSwitch.setText(if (isChecked) R.string.online else R.string.offline)
                }

        disposable += RxBus.listen(SocketServiceResponseEvent::class.java)
                .subscribe { response ->
                    onlineStateLiveData.postValue(response)
                    IS_ONLINE_STATE = when (response.result) {
                        POSITIVE -> {
                            if (response.playSound) BeepUtil.playDriverStatus(true)
                            setOnlineState(true)
                            true
                        }
                        NEGATIVE -> {
                            if (response.playSound) BeepUtil.playDriverStatus(false)
                            setOnlineState(false)
                            false
                        }
                    }
                    Timber.d("TOOLBAR SOCKET IS ONLINE SocketServiceResponseEvent: $IS_ONLINE_STATE")
                }

        disposable += RxBus.listen(DisableOnlineStatusEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    IS_ONLINE_STATE = it.online
                    BeepUtil.playDriverStatus(it.online)
                    setOnlineState(it.online)
                    Timber.d("TOOLBAR SOCKET IS ONLINE: ${it.online}")
                }

        disposable += RxBus.listen(ShowSecondaryOrderButtonEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { binding.newOrder.setVisible(it.isShow) }

        //Broadcasts
        disposable += RxBus.listen(BroadcastCountEvent::class.java)
                .subscribe {
                    Timber.d("TOOLBAR SOCKET BROADCAST: BroadcastCountEvent()")
                    binding.etherButton.updateCounter(it.count)
                }
        disposable += RxBus.listen(NewBroadcastEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Timber.d("TOOLBAR SOCKET BROADCAST: NewBroadcastEvent()")
                    binding.etherButton.increaseCounter()
                }

        disposable += RxBus.listen(DeleteBroadcastEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Timber.d("TOOLBAR SOCKET BROADCAST: DeleteBroadcastEvent()")
                    binding.etherButton.decreaseCounter()
                }

        //Filters
        disposable += RxBus.listen(FilterCountEvent::class.java)
                .subscribe {
                    binding.filter.updateCounter(it.count)
                }
        disposable += RxBus.listen(NewFilterEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    binding.filter.increaseCounter()
                }

        disposable += RxBus.listen(DeleteFilterEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    binding.filter.decreaseCounter()
                }
    }

    private fun setOnlineState(isChecked: Boolean) {
        Timber.d("TOOLBAR SOCKET IS ONLINE setOnlineState(): $isChecked")
        binding.toolbarSwitch.isChecked = isChecked
        RxBus.publish(BroadcastPing(isChecked))
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
//        Timber.d("TOOLBAR SOCKET IS ONLINE onAttachedToWindow: $IS_ONLINE_STATE")
//        setOnlineState(IS_ONLINE_STATE)
        binding.run {
            newOrder.setVisible(IS_SECONDARY_ORDER_EXIST)
            etherButton.setVisible(!IS_SECONDARY_ORDER_EXIST)
        }
    }

    override fun onDetachedFromWindow() {
        disposable.dispose()
        Timber.d("TOOLBAR SOCKET IS ONLINE onDetachedFromWindow: $IS_ONLINE_STATE")
        super.onDetachedFromWindow()
    }

    companion object {
        var IS_ONLINE_STATE = false
        var IS_SECONDARY_ORDER_EXIST = false
    }
}