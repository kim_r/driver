package com.wezom.taxi.driver.ext

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds

/**
 * Created by zorin.a on 09.07.2018.
 */

fun GoogleMap?.animateTo(location: LatLng) {
    this?.animateCamera(CameraUpdateFactory.newLatLngZoom(location,
                                                          15f))
}

fun GoogleMap?.animateTo(locations: List<LatLng>) {
    if (locations.isEmpty()) return
    this?.let {
        val padding = 100
        val builder = LatLngBounds.builder()
        locations.forEach { builder.include(it) }
        val bounds = builder.build()
        val cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds,
                                                               padding)
        animateCamera(cameraUpdate)
    }
}

fun GoogleMap?.zoomTo(zoom: Float? = 12F) {
    this?.animateCamera(CameraUpdateFactory.zoomTo(zoom!!))
}

fun GoogleMap?.disableMapScrolling() {
    this?.uiSettings?.let {
        it.isScrollGesturesEnabled = false
        it.isZoomGesturesEnabled = false
    }
}

fun GoogleMap?.enableMapScrolling() {
    this?.uiSettings?.let {
        it.isScrollGesturesEnabled = true
        it.isZoomGesturesEnabled = true
    }
}