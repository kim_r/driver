package com.wezom.taxi.driver.service

import android.Manifest
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.support.v4.content.ContextCompat
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.bus.events.LocationEvent
import com.wezom.taxi.driver.common.Constants.LOCATION_UPDATE_INTERVAL
import com.wezom.taxi.driver.common.LocationStorage
import com.wezom.taxi.driver.ext.showServiceNotification
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.repository.DriverRepository
import dagger.android.DaggerService
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import java.lang.ref.WeakReference
import javax.inject.Inject

/**
 * Created by zorin.a on 13.04.2018.
 */
open class LocationService : DaggerService() {
    //region var
    @Inject
    lateinit var context: Context
    @Inject
    lateinit var locationStorage: LocationStorage
    @Inject
    lateinit var repository: DriverRepository

    protected var location: Location? = null
    protected var disposable = CompositeDisposable()

    private val googleApiClient: GoogleApiClient? by lazy(mode = LazyThreadSafetyMode.SYNCHRONIZED) {
        GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(connectionCallbacks)
                .addOnConnectionFailedListener(connectionFailedListener)
                .build()
    }
    private var count = 0
    private var move = false
    private var handler: Handler? = null

    private var timeCurrent: Long? = System.currentTimeMillis()


    private val connectionCallbacks = object : GoogleApiClient.ConnectionCallbacks {
        override fun onConnected(bundle: Bundle?) {
            Timber.d("onConnected")
            val locationRequest = LocationRequest()
            locationRequest.interval = LOCATION_UPDATE_INTERVAL
            locationRequest.fastestInterval = LOCATION_UPDATE_INTERVAL
            locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            if (ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED) {
                Timber.d("Location permissions granted")
                LocationServices.getFusedLocationProviderClient(context).lastLocation.addOnSuccessListener {
                    Timber.d("getFusedLocationProviderClient: $it ")
                    if (it != null) {
                        locationStorage.storeLocation(it)
                        location = it
                    }
                    if (location != null)
                        RxBus.publish(LocationEvent(location!!))
                    LocationServices.getFusedLocationProviderClient(context).requestLocationUpdates(locationRequest, localCallbacks2
                            , Looper.getMainLooper())
                }

            } else {
//                "Location permissions rejected".shortToast(context)
            }
        }

        override fun onConnectionSuspended(p0: Int) {
            googleApiClient?.reconnect()
        }
    }

    var localCallbacks2 = object : LocationCallback() {
        override fun onLocationResult(loc: LocationResult?) {
            Timber.d("onLocationResult: ${loc!!.lastLocation} ")
            if ((System.currentTimeMillis() - timeCurrent!!) > 1000) {
                timeCurrent = System.currentTimeMillis()
                Timber.d("LOCATION: ${loc!!.lastLocation} ")
                App.instance.getLogger()!!.log("setMoveStopCar start and end")
                if (locationStorage.getCachedLocation().distanceTo(loc!!.lastLocation) < 2 && count >= 0) {
                    count++
                    if (count == 10) {
                        App.instance.getLogger()!!.log("setMoveStopCar start and end")
                        repository.setMoveStopCar()
                        count = -1
                        move = false
                    }
                } else if (locationStorage.getCachedLocation().distanceTo(loc.lastLocation) > 2 && count <= -1) {
                    count--
                    if (count == -3) {
                        repository.setMoveCar()
                        count = 0
                        move = true
                    }
                }
            }
            locationStorage.storeLocation(loc!!.lastLocation) //cache coordinates
            location = loc.lastLocation
        }
    }

    private val connectionFailedListener =
            GoogleApiClient.OnConnectionFailedListener { connectionResult ->
                Timber.e("onConnectionFailed: ${connectionResult.isSuccess}, ${connectionResult.errorMessage}")
            }
    //endregion

    //region override
    override fun onCreate() {
        super.onCreate()
        initGoogleApiClient()
        showNotification()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Timber.d("LOCATION_SERVICE onStartCommand")
        return Service.START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? = null

    override fun onDestroy() {
        Timber.d("LOCATION_SERVICE onDestroy")
        googleApiClient?.disconnect()
        disposable.dispose()
        super.onDestroy()
    }
    //endregion

    //region fun
    private fun showNotification() {
        val notification = this.showServiceNotification(
                CHANNEL_ID,
                getString(R.string.location_service), getString(R.string.location_service_enabled)
        )
        startForeground(SocketService.NOTIFICATION_ID, notification)
    }

    private fun initGoogleApiClient() {
        googleApiClient?.connect()
    }
    //endregion

    companion object {
        private const val permission = Manifest.permission.ACCESS_FINE_LOCATION
        const val CHANNEL_ID = "Location channel"

        fun start(context: WeakReference<Context>) {
            Timber.d("LOCATION_SERVICE start")
            val intent = Intent(context.get(), LocationService::class.java)
            context.get()?.startService(intent)
        }

        fun stop(context: WeakReference<Context>) {
            Timber.d("LOCATION_SERVICE stop")
            val intent = Intent(context.get(), LocationService::class.java)
            context.get()?.stopService(intent)
        }
    }
}