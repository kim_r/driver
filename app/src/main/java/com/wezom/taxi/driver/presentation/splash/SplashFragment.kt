package com.wezom.taxi.driver.presentation.splash


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.databinding.SplashFragmentBinding
import com.wezom.taxi.driver.ext.longToast
import com.wezom.taxi.driver.presentation.base.BaseFragment
import com.wezom.taxi.driver.presentation.customview.StateView
import timber.log.Timber


class SplashFragment : BaseFragment() {

    private lateinit var binding: SplashFragmentBinding
    private lateinit var viewModel: SplashViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.d("isLocked splash onCreate: true")
        lockDrawer(true)
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
        viewModel.drawerLockLiveData.observe(this@SplashFragment, Observer { locked ->
            Timber.d("isLocked splash observer: true")
            lockDrawer(locked!!)
        })
        viewModel.notEnoughMoneyLiveData.observe(this, Observer {
            getString(R.string.not_enough_money).longToast(context)
        })

        viewModel.loadingLiveData.observe(this, progressObserver)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = SplashFragmentBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.stateView.retryListener = object : StateView.OnRetryListener {
            override fun onRetryClicked() {
                viewModel.switchScreen()
            }
        }

        viewModel.switchScreen()

    }

    companion object {
        val TAG = this::class.java.simpleName
    }
}
