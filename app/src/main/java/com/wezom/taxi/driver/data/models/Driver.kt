package com.wezom.taxi.driver.data.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


/**
 * Created by zorin.a on 14.03.2018.
 */

data class Driver(
        @SerializedName("id") val id: Int?,
        @SerializedName("rating") val rating: Double?,
        @SerializedName("fiveStarsCount") val fiveStarsCount: Int?,
        @SerializedName("acceptedOrders") val acceptedOrders: Double?,
        @SerializedName("completedOrders") val completedOrders: Double?,
        @SerializedName("comments") val comments: List<Comment>?,
        @SerializedName("name") val name: String,
        @SerializedName("image") val image: String,
        @SerializedName("phone") val phone: String
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readValue(Double::class.java.classLoader) as Double?,
            ArrayList<Comment>().apply { source.readList(this, Comment::class.java.classLoader) },
            source.readString(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeValue(id)
        writeValue(rating)
        writeValue(fiveStarsCount)
        writeValue(acceptedOrders)
        writeValue(completedOrders)
        writeList(comments)
        writeString(name)
        writeString(image)
        writeString(phone)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Driver> = object : Parcelable.Creator<Driver> {
            override fun createFromParcel(source: Parcel): Driver = Driver(source)
            override fun newArray(size: Int): Array<Driver?> = arrayOfNulls(size)
        }
    }
}