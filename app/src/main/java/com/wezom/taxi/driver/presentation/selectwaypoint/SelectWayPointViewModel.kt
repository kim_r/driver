package com.wezom.taxi.driver.presentation.selectwaypoint

import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import javax.inject.Inject

/**
 * Created by udovik.s on 24.04.2018.
 */
class SelectWayPointViewModel@Inject constructor(router: ScreenRouterManager) : BaseViewModel(router) {

    fun backToOrder(resultCode : Int, id: Int) {
        backWithResult(resultCode, id)
    }


}