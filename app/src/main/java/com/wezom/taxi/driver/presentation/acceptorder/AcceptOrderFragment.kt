package com.wezom.taxi.driver.presentation.acceptorder

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.res.Resources
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.BottomSheetBehavior.STATE_COLLAPSED
import android.support.design.widget.BottomSheetBehavior.STATE_EXPANDED
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.common.*
import com.wezom.taxi.driver.data.models.NewOrderInfo
import com.wezom.taxi.driver.databinding.FragmentAcceptOrderBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.ext.setVisible
import com.wezom.taxi.driver.presentation.acceptorder.dialog.PreliminaryTimeDialog
import com.wezom.taxi.driver.presentation.acceptorder.list.AddressAdapter
import com.wezom.taxi.driver.presentation.base.BaseMapFragment
import com.wezom.taxi.driver.presentation.main.events.DeleteBroadcastEvent
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber
import java.text.DecimalFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.math.roundToInt


class AcceptOrderFragment : BaseMapFragment() {
    @Inject
    lateinit var adapter: AddressAdapter
    lateinit var viewModel: AcceptOrderViewModel
    lateinit var binding: FragmentAcceptOrderBinding

    private var orderAddressCoordinates: MutableList<LatLng> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this,
                viewModelFactory)
                .getViewModelOfType()

        arguments?.let {
            val orderInfo = it.getParcelable(ORDER_INFO_KEY) as NewOrderInfo
            viewModel.setOrder(orderInfo)

            orderInfo.order!!.coordinates?.let { list ->
                list.forEach { address ->
                    orderAddressCoordinates.add(LatLng(address.latitude!!,
                            address.longitude!!))
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentAcceptOrderBinding.inflate(inflater,
                container,
                false)
        binding.setLifecycleOwner(this)
        initBroadcastListeners()
        return binding.root
    }

    override fun onViewCreated(view: View,
                               savedInstanceState: Bundle?) {
        super.onViewCreated(view,
                savedInstanceState)
        initUi()

        viewModel.isMapWhiteThemeLiveData.observe(this,
                Observer { isMapWhite ->
                    setMapStyle(isMapWhite)
                })
        viewModel.routeLiveData.observe(this,
                Observer {
                    drawRoute(it)
                })
    }

    override fun onMapReady(map: GoogleMap?) {
        super.onMapReady(map)
        viewModel.setMapStyle()
        map?.setOnMapLoadedCallback { handleCamera() }
    }

    private fun initUi() {
        binding.run {
            back.setOnClickListener {
                viewModel.onBackPressed()
            }
            acceptButton.setOnClickListener {
                viewModel.acceptOrder(lastLocation?.longitude,
                        lastLocation?.latitude)
            }
        }
        initBottomSheet()
        initRecyclerView()

    }

    private fun initRecyclerView() {
        val order = viewModel.orderInfo.order
        order?.coordinates?.let {
            val adapterData = Pair(it.toMutableList(),
                    AddressAdapter.FromToDistance(from = DecimalFormat("#0.0").format(order.distanceFromDriver!! / 1000).replace(",", "."),
                            to = order.calculatedPath.toString()))
            adapter.addAll(adapterData)
        }
        binding.bottomSheetLayout?.addressList?.adapter = adapter

    }


    private fun initBottomSheet() {
        val upDrawable by lazy { context?.getDrawable(R.drawable.bg_up) }
        val downDrawable by lazy { context?.getDrawable(R.drawable.bg_down) }

        val bottomSheetLayout = binding.bottomSheetLayout

        val bottomsheet =
                BottomSheetBehavior.from<ConstraintLayout>(bottomSheetLayout?.bottomSheetRoot)
        bottomsheet.peekHeight = dpToPx(198)
        bottomsheet.state = STATE_EXPANDED
        bottomSheetLayout?.bottomSheetHead?.setImageDrawable(downDrawable)
        bottomsheet.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View,
                                 slideOffset: Float) {
            }

            override fun onStateChanged(bottomSheet: View,
                                        newState: Int) {
                Timber.d("BottomSheet state: $newState")
                when (newState) {
                    STATE_EXPANDED -> {
                        bottomSheetLayout?.bottomSheetHead?.setImageDrawable(downDrawable)
                    }
                    STATE_COLLAPSED -> {
                        bottomSheetLayout?.bottomSheetHead?.setImageDrawable(upDrawable)
                    }
                }
            }
        })

        val orderInfo = viewModel.orderInfo

        if (viewModel.orderInfo.order?.coordinates!!.size > 1) {
            var estCostValue: Double =
                    orderInfo.order?.payment?.estimatedCost?.plus(orderInfo.order?.payment?.surcharge!!)
                            ?: 0.0
            val estBonus: Double = orderInfo.formula?.estimatedBonuses ?: 0.0
            estCostValue += estBonus
            bottomSheetLayout?.priceValue?.text = Math.round(estCostValue)
                    .toString()
        } else {
            bottomSheetLayout?.rateValue!!.visibility = View.GONE
            bottomSheetLayout.rateMeasureLabel.visibility = View.GONE
            bottomSheetLayout.priceValue.text = context!!.resources.getString(R.string.auto_check)
        }


        orderInfo.order?.payment?.method?.let { getPaymentResource(it) }
                ?.let { bottomSheetLayout?.payMethodImage?.setImageResource(it) }

        orderInfo.order?.user?.rating?.let {
            bottomSheetLayout?.ratingLabel?.text = it.toString()
        }


        bottomSheetLayout?.userImage?.let {
            loadRoundedImage(context = context!!,
                    view = it,
                    url = orderInfo.order?.user?.photo,
                    placeHolderId = R.drawable.ic_ava_vector40px)
        }
        bottomSheetLayout?.userImage?.setOnClickListener {
            orderInfo.order?.user?.let { it1 -> viewModel.showUser(it1) }
        }

        bottomSheetLayout?.positiveDrivesLabel?.text =
                orderInfo.order?.user?.doneOrdersCount.toString()
        bottomSheetLayout?.negativeDrivesLabel?.text =
                orderInfo.order?.user?.undoneOrdersCount.toString()

        if (!orderInfo.order?.user?.name.isNullOrEmpty()) {
            bottomSheetLayout?.userName?.text = orderInfo.order?.user?.name
        } else {
            bottomSheetLayout?.userName?.text = getString(R.string.name_client)
        }

        orderInfo.order?.services?.let {
            if (it.isNotEmpty() && checkService(it)) {
                bottomSheetLayout?.services?.setActiveStatus(true)
                bottomSheetLayout?.services?.addServices(*it.toIntArray())
                bottomSheetLayout?.services?.setVisible(true)
                bottomSheetLayout?.servicesLabel?.setVisible(true)
            } else {
                bottomSheetLayout?.servicesContainer?.setVisible(false)
            }
        }

        if (orderInfo.order?.carArrival == 0) {
            bottomSheetLayout?.timeTextView?.setVisible(false)
        } else {
            orderInfo.order?.preliminaryTime?.let {
                //set time
                val time = formatTime(it)
                Timber.d("TIME_: $time")
                bottomSheetLayout?.timeTextView?.setTime(time)
                bottomSheetLayout?.timeTextView?.setOnClickListener {
                    val dialog = PreliminaryTimeDialog.newInstance(time)
                    dialog.show(fragmentManager,
                            PreliminaryTimeDialog.TAG)
                }
                bottomSheetLayout?.servicesContainer?.setVisible(true)
            }
        }
        if (!orderInfo.order?.comment.isNullOrEmpty()) {
            bottomSheetLayout?.comment?.text = orderInfo.order?.comment
        } else {
            bottomSheetLayout?.comment?.setVisible(false)
        }
        if (orderInfo.order?.kilometerPrice != 0.0) {
            bottomSheetLayout?.rateValue?.text = orderInfo.order?.kilometerPrice?.let { if (it != 0.0) it.roundToInt().toString() else it.toString() }
        } else {
            bottomSheetLayout?.rateValue?.setVisible(false)
            bottomSheetLayout?.rateMeasureLabel?.setVisible(false)
        }

        val params: ConstraintLayout.LayoutParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)
        params.bottomToBottom =  bottomSheetLayout!!.topContainer.id
        params.endToStart =  bottomSheetLayout.payMethodImage.id
        params.topToTop =  bottomSheetLayout.topContainer.id
//        if (orderInfo.order?.coordinates!!.size > 1) {
//            params.setMargins(0, 0,
//                    pxFromDp(bottomSheetLayout.rateValue.resources, ((bottomSheetLayout.rateValue.text.toString().length - 1) * 32f)).toInt(), 0)
//        }else {
            if (Locale.getDefault().language == "uk") {
                params.setMargins(0, 0, pxFromDp(bottomSheetLayout.rateValue.resources, 110f).toInt() -
                        pxFromDp(bottomSheetLayout.rateValue.resources, ((bottomSheetLayout.rateValue.text.toString().length - 1) * 10f)).toInt(), 0)
            } else {
                params.setMargins(0, 0, (pxFromDp(bottomSheetLayout.rateValue.resources, 71f) -
                        pxFromDp(bottomSheetLayout.rateValue.resources, ((bottomSheetLayout.rateValue.text.toString().length - 1) * 10f))).toInt(), 0)
            }
//        }
        bottomSheetLayout.rateMeasureLabel.layoutParams = params
    }

    fun pxFromDp(resources: Resources, dp: Float): Float {
        return (dp * resources.getDisplayMetrics().density)
    }

    private fun handleCamera() {
        if (orderAddressCoordinates.isNotEmpty()) {
            disposable += Single.timer(1,
                    TimeUnit.SECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        updateOrderMarkers(orderAddressCoordinates)
                        updateDriverMarker()
                        updateMultipleTargetCamera(lastLocation,
                                orderAddressCoordinates)
                        viewModel.getMapRoute(orderAddressCoordinates)
                    },
                            Timber::e)
        } else {
            updateDriverMarker()
            updateDriverCamera()
        }
    }

    private fun initBroadcastListeners() {
        disposable.clear()
        disposable += RxBus.listen(DeleteBroadcastEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Timber.d("ETHER_FRAGMENT: DeleteBroadcastEvent ${it.orderId}")
                    if(viewModel.orderInfo.order!!.id == it.orderId){
                        activity!!.onBackPressed()
                    }
                }
    }

    companion object {
        const val ORDER_INFO_KEY = "order_info_key"
        fun newInstance(orderInfo: NewOrderInfo): AcceptOrderFragment {
            val args = Bundle()
            args.putParcelable(ORDER_INFO_KEY,
                    orderInfo)
            val fragment = AcceptOrderFragment()
            fragment.arguments = args
            return fragment
        }

        fun checkService(it: List<Int>): Boolean {
            for (item in it) {
                if (item > 4)
                    return false
            }
            return true
        }
    }
}