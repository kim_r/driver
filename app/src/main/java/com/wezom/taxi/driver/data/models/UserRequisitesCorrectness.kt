package com.wezom.taxi.driver.data.models
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


/**
 * Created by zorin.a on 021 21.02.18.
 */

data class UserRequisitesCorrectness(
		@SerializedName("isNameCorrect") val isNameCorrect: Boolean?,
		@SerializedName("isSurnameCorrect") val isSurnameCorrect: Boolean?,
		@SerializedName("isEmailCorrect") val isEmailCorrect: Boolean?,
		@SerializedName("isPhotoCorrect") val isPhotoCorrect: Boolean?


) : Parcelable {
	constructor(source: Parcel) : this(
			source.readValue(Boolean::class.java.classLoader) as Boolean?,
			source.readValue(Boolean::class.java.classLoader) as Boolean?,
			source.readValue(Boolean::class.java.classLoader) as Boolean?,
			source.readValue(Boolean::class.java.classLoader) as Boolean?
	)

	override fun describeContents() = 0

	override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
		writeValue(isNameCorrect)
		writeValue(isSurnameCorrect)
		writeValue(isEmailCorrect)
		writeValue(isPhotoCorrect)
	}

	companion object {
		@JvmField
		val CREATOR: Parcelable.Creator<UserRequisitesCorrectness> = object : Parcelable.Creator<UserRequisitesCorrectness> {
			override fun createFromParcel(source: Parcel): UserRequisitesCorrectness = UserRequisitesCorrectness(source)
			override fun newArray(size: Int): Array<UserRequisitesCorrectness?> = arrayOfNulls(size)
		}
	}
}