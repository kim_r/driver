package com.wezom.taxi.driver.presentation.picklocation

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wezom.taxi.driver.databinding.FragmentPickLocationBinding
import com.wezom.taxi.driver.ext.getViewModel
import com.wezom.taxi.driver.ext.hideKeyboard
import com.wezom.taxi.driver.ext.showKeyboard
import com.wezom.taxi.driver.net.response.AutoCompleteResult
import com.wezom.taxi.driver.presentation.base.BaseFragment
import com.wezom.taxi.driver.presentation.base.lists.AdapterClickListener
import com.wezom.taxi.driver.presentation.filter.CreateFilterViewModel
import com.wezom.taxi.driver.presentation.picklocation.list.AutoCompleteAdapter
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 *Created by Zorin.A on 31.July.2019.
 */
class PickLocationFragment : BaseFragment() {
    @Inject
    lateinit var adapter: AutoCompleteAdapter
    val viewModel by lazy {
        getViewModel() as PickLocationViewModel
    }
    lateinit var binding: FragmentPickLocationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.loadingLiveData.observe(this,
                                          progressObserver)
        setBackPressFun { viewModel.onBackPressed() }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentPickLocationBinding.inflate(inflater,
                                                      container,
                                                      false)
        return binding.root
    }

    override fun onViewCreated(view: View,
                               savedInstanceState: Bundle?) {
        super.onViewCreated(view,
                            savedInstanceState)
        initViews()
        initListeners()
    }

    private fun initViews() {
        binding.run {
            searchInput.requestFocus()
            activity?.showKeyboard()
            clear.setOnClickListener {
                searchInput.setText("")
                adapter.clearData()
            }
            mapPickerContainer.setOnClickListener {
                hideKeyboard()
                viewModel.navigateToMap()
            }
            predictionList.adapter = adapter
            predictionList.hasFixedSize()
            adapter.listener = object : AdapterClickListener<AutoCompleteResult> {
                override fun onItemClick(position: Int,
                                         data: AutoCompleteResult) {
                    Timber.d("Click: ${data.fullAddress}")
                    activity.hideKeyboard()
                    viewModel.backWithResult(CreateFilterViewModel.RESULT_CODE,
                                             data )
                }
            }
        }
    }

    private fun hideKeyboard() {
        view?.let { activity?.hideKeyboard() }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposable.clear()
    }

    private fun initListeners() {
        disposable += Observable.create<String> {
            binding.searchInput.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                }

                override fun beforeTextChanged(p0: CharSequence?,
                                               p1: Int,
                                               p2: Int,
                                               p3: Int) {
                }

                override fun onTextChanged(p0: CharSequence?,
                                           p1: Int,
                                           p2: Int,
                                           p3: Int) {
                    it.onNext(p0.toString())
                }
            })
        }
            .observeOn(Schedulers.io())
            .filter { it.length > 1 }
            .debounce(400,
                      TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                viewModel.queryAutoComplete(it)
                    .observe(this,
                             Observer { list ->
                                 list?.let {
                                     adapter.setData(it)
                                 }
                             })
            }
    }


    companion object {
        fun newInstance(): PickLocationFragment = PickLocationFragment()
    }
}