package com.wezom.taxi.driver.net.request

import com.google.gson.annotations.SerializedName

/**
 * Created by zorin.a on 021 21.02.18.
 */
data class ConfirmCodeRequest(
        @SerializedName("phone") private val phone: String?,
        @SerializedName("code") private val code: Int?)