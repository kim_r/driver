package com.wezom.taxi.driver.presentation.changephone

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxTextView
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.databinding.ChangePhoneBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.ext.hideKeyboard
import com.wezom.taxi.driver.presentation.base.BaseFragment
import io.reactivex.rxkotlin.plusAssign
import com.wezom.taxi.driver.common.NumericMaskWatcher

/**
 * Created by udovik.s on 07.03.2018.
 */
class ChangePhoneFragment : BaseFragment() {
    private lateinit var binding: ChangePhoneBinding
    private lateinit var viewModel: ChangePhoneViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setBackPressFun { viewModel.onBackPressed() }
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
        viewModel.loadingLiveData.observe(this, progressObserver)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = ChangePhoneBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.run {
            toolbar.setToolbarTitle(getString(R.string.change_phone))
            val phoneMask = NumericMaskWatcher(PHONE_MASK)
            changePhoneEdit.setText(getString(R.string.phone_prefix))
            emptyPhoneEdit.addTextChangedListener(phoneMask)
            changePhoneEdit.addTextChangedListener(phoneMask)

            updateUi()

            disposable += RxView.clicks(btnSavePhone).subscribe {
                verificationPhone(getPhone())
            }

            disposable += RxTextView.textChanges(changePhoneEdit).subscribe { text ->
                enableRateButton(isValuesValid(text.toString()))
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        activity.hideKeyboard()
    }

    private fun updateUi() {
        binding.run {
            emptyPhoneEdit.isFocusable = false
            emptyPhoneEdit.text = arguments?.getString(PHONE_KEY)
            changePhoneEdit.requestFocus()
        }
    }

    private fun getPhone(): String {
        return binding.changePhoneEdit.text.toString().replace(" ", "")
    }

    private fun verificationPhone(phone: String) {
        viewModel.changePhone(phone)
    }

    private fun isValuesValid(phone: String): Boolean {
        return Patterns.PHONE.matcher(phone).matches() && phone.length == 17
    }

    private fun enableRateButton(value: Boolean) {
        if (value) {
            binding.btnSavePhone.setBackgroundColor(resources.getColor(R.color.colorAccent2))
            binding.btnSavePhone.isEnabled = true
        } else {
            binding.btnSavePhone.setBackgroundColor(resources.getColor(R.color.colorLiteGrey))
            binding.btnSavePhone.isEnabled = false
        }
    }

    companion object {
        private const val PHONE_MASK = "+-- --- --- -- --"
        const val PHONE_KEY = "status_key"
        fun newInstance(phone: String): ChangePhoneFragment {
            val args = Bundle()
            args.putString(PHONE_KEY, phone)
            val fragment = ChangePhoneFragment()
            fragment.arguments = args
            return fragment
        }
    }
}