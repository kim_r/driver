package com.wezom.taxi.driver.presentation.app

import android.app.Activity
import android.app.Application
import android.os.Bundle
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.common.BeepUtil
import com.wezom.taxi.driver.presentation.main.MainActivity
import com.wezom.taxi.driver.presentation.main.events.SocketServiceEvent

class AppLifecycleTracker : Application.ActivityLifecycleCallbacks {
    private var count: Int = 0
    companion object {
        var activityVisible = false
    }

    override fun onActivityPaused(p0: Activity?) {
        activityVisible = false
    }

    override fun onActivityResumed(p0: Activity?) {
        activityVisible = true
        BeepUtil.stop()
    }


    override fun onActivityDestroyed(p0: Activity?) {
        if (p0 is MainActivity && p0.isTaskRoot && count == 1)
            RxBus.publish(SocketServiceEvent(SocketServiceEvent.SocketCommand.STOP))
        count--
    }

    override fun onActivitySaveInstanceState(p0: Activity?, p1: Bundle?) {
    }

    override fun onActivityCreated(p0: Activity?, p1: Bundle?) {
        count++
    }

    override fun onActivityStarted(activity: Activity?) {
    }

    override fun onActivityStopped(activity: Activity?) {
    }

}