package com.wezom.taxi.driver.data.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


/**
 * Created by zorin.a on 021 21.02.18.
 */

data class Document(
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: String? = null,
        @SerializedName("url")  val url: String? = null,
        @SerializedName("photoStatus")  val photoStatus: Int

) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt())


    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)
        parcel.writeString(url)
        parcel.writeInt(photoStatus)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<Document> {
        override fun createFromParcel(parcel: Parcel): Document = Document(parcel)

        override fun newArray(size: Int): Array<Document?> = arrayOfNulls(size)
    }
}