package com.wezom.taxi.driver.presentation.notpaid

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import com.wezom.taxi.driver.common.EXECUTE_ORDER_SCREEN
import com.wezom.taxi.driver.common.MAIN_MAP_SCREEN
import com.wezom.taxi.driver.common.OrderManager
import com.wezom.taxi.driver.data.db.DbManager
import com.wezom.taxi.driver.data.models.OrderStatus
import com.wezom.taxi.driver.data.models.Reason
import com.wezom.taxi.driver.net.api.ApiManager
import com.wezom.taxi.driver.net.jobqueue.JobFactory
import com.wezom.taxi.driver.net.jobqueue.jobs.CustomerDidNotPayJob
import com.wezom.taxi.driver.net.request.NotPaidRequest
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by udovik.s on 27.03.2018.
 */
class NotPaidViewModel @Inject constructor(screenRouterManager: ScreenRouterManager,
                                           private val orderManager: OrderManager,
                                           private val apiManager: ApiManager,
                                           private val dbManager: DbManager) : BaseViewModel(screenRouterManager) {
    val reasonsLiveData: MutableLiveData<List<Reason>> = MutableLiveData()

    @SuppressLint("CheckResult")
    fun getNotPaidReasons() {
        val reasons = dbManager.queryReasonByStatus(OrderStatus.NOT_COMPLETED)
        if (reasons.isEmpty()) {
            apiManager.getNotPaidReasons().subscribe({ response ->
                if (response.isSuccess!!) {
                    Timber.e("NOT_PAID_REASONS: ${response.reasons}")
                    val mutable = response.reasons
                    mutable.map { reason -> reason.orderStatusToCorrespondFor = OrderStatus.NOT_COMPLETED }
                    dbManager.insertReasons(mutable)
                    Timber.d("insert getNotPaidReasons reasons: $mutable")
                    reasonsLiveData.postValue(mutable)
                }
            }, {
                Timber.e(it)
            })
        } else {
            reasonsLiveData.postValue(reasons)
        }
    }

    fun customerDidNotPay(orderId: Int, longitude: Double?, latitude: Double?, reasonId: Int, comment: String? = null, eventTime: Long) {
        val request = NotPaidRequest(longitude = longitude,
                latitude = latitude,
                reasonId = reasonId,
                comment = if (comment.isNullOrEmpty()) "..." else comment,
                eventTime = eventTime)
        JobFactory.instance.getJobManager()?.addJobInBackground(CustomerDidNotPayJob(orderId, request))
        performNextAction()
    }

    private fun performNextAction() {
        if (orderManager.isSecondaryOrderExist()) {
            setRootScreen(EXECUTE_ORDER_SCREEN, orderManager.getSecondaryOrder())
        } else {
            //just continue on road
            setRootScreen(MAIN_MAP_SCREEN)
        }
        orderManager.finishOrder()
    }
}