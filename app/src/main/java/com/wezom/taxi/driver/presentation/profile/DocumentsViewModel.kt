package com.wezom.taxi.driver.presentation.profile

import android.arch.lifecycle.MutableLiveData
import android.content.SharedPreferences
import android.net.Uri
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.common.*
import com.wezom.taxi.driver.ext.*
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.presentation.photoviewer.PhotoViewerFragment
import com.wezom.taxi.driver.presentation.profile.DocumentsFragment.Companion.CAR_IMG_ID
import com.wezom.taxi.driver.presentation.profile.DocumentsFragment.Companion.DOC_1_IMG_ID
import com.wezom.taxi.driver.presentation.profile.DocumentsFragment.Companion.DOC_4_IMG_ID
import com.wezom.taxi.driver.presentation.profile.DocumentsFragment.Companion.DOC_2_IMG_ID
import com.wezom.taxi.driver.presentation.profile.DocumentsFragment.Companion.DOC_3_IMG_ID
import com.wezom.taxi.driver.presentation.profile.DocumentsFragment.Companion.USER_IMG_ID
import com.wezom.taxi.driver.presentation.profile.events.ProfileActionClickEvent
import ru.terrakok.cicerone.result.ResultListener
import javax.inject.Inject

/**
 * Created by zorin.a on 28.02.2018.
 */
class DocumentsViewModel @Inject constructor(screenRouterManager: ScreenRouterManager,
                                             sharedPreferences: SharedPreferences) : BaseViewModel(screenRouterManager) {

    private var userImageUriString: String by sharedPreferences.string(USER_IMAGE_URI)
    private var carImageUriString: String by sharedPreferences.string(CAR_IMAGE_URI)
    private var doc1UriString: String by sharedPreferences.string(FIRST_DOC_URI)
    private var doc2UriString: String by sharedPreferences.string(SECOND_DOC_URI)
    private var doc3UriString: String by sharedPreferences.string(THIRD_DOC_URI)
    private var doc4UriString: String by sharedPreferences.string(FOURTH_DOC_URI)

    var userImageLiveData = MutableLiveData<String>()
    var carImageLiveData = MutableLiveData<String>()
    var doc1LiveData = MutableLiveData<String>()
    var doc2LiveData = MutableLiveData<String>()
    var doc3LiveData = MutableLiveData<String>()
    var doc4LiveData = MutableLiveData<String>()

    private fun getCropModeForDoc(imageId: Int): PhotoViewerFragment.Companion.CropOptions {
        return when (imageId) {
            USER_IMG_ID -> PhotoViewerFragment.Companion.CropOptions.CROP_ROUNDED
            CAR_IMG_ID -> PhotoViewerFragment.Companion.CropOptions.RATIO_16_12
            else -> {
                PhotoViewerFragment.Companion.CropOptions.FREE
            }
        }
    }

    fun cropImage(source: String, docId: Int) {
        val cropMode = getCropModeForDoc(docId)

        startScreenForResult(PHOTO_VIEWER_SCREEN, Triple(source, cropMode, docId), ResultListener { it ->
            val tmp = it as Pair<String, Int> //tmp uri and image id


            when (tmp.second) {
                USER_IMG_ID -> {
                    userImageUriString = saveCroppedImageUri(tmp.first, "user").toString()
                    userImageLiveData.postValue(userImageUriString)
                }
                CAR_IMG_ID -> {
                    carImageUriString = saveCroppedImageUri(tmp.first, "car").toString()
                    carImageLiveData.postValue(carImageUriString)
                }
                DOC_1_IMG_ID -> {
                    doc1UriString = saveCroppedImageUri(tmp.first, "doc1").toString()
                    doc1LiveData.postValue(doc1UriString) //get tmp uri of cropped file
                }
                DOC_2_IMG_ID -> {
                    doc2UriString = saveCroppedImageUri(tmp.first, "doc2").toString()
                    doc2LiveData.postValue(doc2UriString)
                }
                DOC_3_IMG_ID -> {
                    doc3UriString = saveCroppedImageUri(tmp.first, "doc3").toString()
                    doc3LiveData.postValue(doc3UriString)
                }
                DOC_4_IMG_ID -> {
                    doc4UriString = saveCroppedImageUri(tmp.first, "doc4").toString()
                    doc4LiveData.postValue(doc4UriString)
                }
            }
            removeResultListener(docId)
        }, docId)
    }

    private fun saveCroppedImageUri(croppedUriString: String?, fileName: String): Uri {
        val bitmap = decodeUriToBitmap(context!!, Uri.parse(croppedUriString))
        val uri = storeBitmapToDisk(context!!, bitmap, fileName)
        return uri
    }

//        when (docId) {
//            DOC_1_IMG_ID -> {
//                startScreenForResult(PHOTO_VIEWER_SCREEN, Triple(source, CROP_SQUARE, docId), ResultListener { it ->
//                    Timber.d("onCropped 1")
//                    doc1LiveData.postValue(it as String)
//                    removeResultListener(REQUEST_IMAGE_CAPTURE)
//                }, REQUEST_IMAGE_CAPTURE)
//            }
//            DOC_2_IMG_ID -> {
//                startScreenForResult(PHOTO_VIEWER_SCREEN, Triple(source, CROP_SQUARE, docId), ResultListener { it ->
//                    Timber.d("onCropped 2")
//                    doc2LiveData.postValue(it as String)
//                    removeResultListener(REQUEST_IMAGE_CAPTURE)
//                }, REQUEST_IMAGE_CAPTURE)
//            }
//            DOC_3_IMG_ID -> {
//                startScreenForResult(PHOTO_VIEWER_SCREEN, Triple(source, CROP_SQUARE, docId), ResultListener { it ->
//                    Timber.d("onCropped 3")
//                    doc3LiveData.postValue(it as String)
//                    removeResultListener(REQUEST_IMAGE_CAPTURE)
//                }, REQUEST_IMAGE_CAPTURE)
//            }
//            DOC_4_IMG_ID -> {
//                startScreenForResult(PHOTO_VIEWER_SCREEN, Triple(source, CROP_SQUARE, docId), ResultListener { it ->
//                    Timber.d("onCropped 4")
//                    doc4LiveData.postValue(it as String)
//                    removeResultListener(REQUEST_IMAGE_CAPTURE)
//                }, REQUEST_IMAGE_CAPTURE)
//            }
//        }
//    }

//    fun saveDocImageUri(docId: Int, uriString: String) {
//        when (docId) {
//            DOC_1_IMG_ID -> {
//                doc1UriString = uriString
//            }
//            DOC_2_IMG_ID -> {
//                doc2UriString = uriString
//            }
//            DOC_3_IMG_ID -> {
//                doc3UriString = uriString
//            }
//            DOC_4_IMG_ID -> {
//                doc4UriString = uriString
//            }
//        }
//    }

//    fun getUserImageUri(docId: Int) {
//        when (docId) {
//            DOC_1_IMG_ID -> {
//                if (doc1UriString.isNotEmpty())
//                    firstDocLiveData.postValue(doc1UriString)
//            }
//            DOC_2_IMG_ID -> {
//                if (doc2UriString.isNotEmpty())
//                    secondDocLiveData.postValue(doc2UriString)
//            }
//            DOC_3_IMG_ID -> {
//                if (doc3UriString.isNotEmpty())
//                    thirdDocLiveData.postValue(doc3UriString)
//            }
//            DOC_4_IMG_ID -> {
//                if (doc4UriString.isNotEmpty())
//                    fourthDocLiveData.postValue(doc4UriString)
//            }
//        }
//    }

    fun saveProfileDocs() {
        RxBus.publish(ProfileActionClickEvent(ProfileActionClickEvent.ActionType.SAVE_IMAGES))
    }

//    fun getRequiredDocs() {
//        repository.getRequiredDocuments().subscribe({
//            handleResponseState(it)
//            if (it!!.isSuccess!!) {
//                requiredDocumentsIdLiveData.postValue(it.necessaryDocumentsId)
//            }
//        }, {
//            Timber.d(it)
//        })
//    }

    fun clearDocCache() {
        userImageUriString = ""
        carImageUriString = ""
        doc1UriString = ""
        doc2UriString = ""
        doc3UriString = ""
        doc4UriString = ""
    }

    fun updateLocalImages() {
        userImageLiveData.postValue(userImageUriString)
        carImageLiveData.postValue(carImageUriString)
        doc1LiveData.postValue(doc1UriString)
        doc2LiveData.postValue(doc2UriString)
        doc3LiveData.postValue(doc3UriString)
        doc4LiveData.postValue(doc4UriString)
    }

    fun imageItemClick(docId: Int, source: String?) {
        switchScreen(IMAGE_PICKER_SCREEN, Pair(docId, source))
    }
}