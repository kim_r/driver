package com.wezom.taxi.driver.injection.module

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule
import com.wezom.taxi.driver.injection.scope.AppScope

/**
 * Created by zorin.a on 02.03.2018.
 */

@GlideModule
@AppScope
class MyGlideModule : AppGlideModule()