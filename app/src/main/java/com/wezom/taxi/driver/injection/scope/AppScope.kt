package com.wezom.taxi.driver.injection.scope

import javax.inject.Scope

/**
 *Created by Zorin.A on 19.March.2019.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class AppScope