package com.wezom.taxi.driver.presentation.history

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.OrientationHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.data.models.Trip
import com.wezom.taxi.driver.databinding.OrderHistoryListBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.ext.setVisible
import com.wezom.taxi.driver.presentation.base.BaseFragment
import com.wezom.taxi.driver.presentation.base.lists.AdapterClickListener
import com.wezom.taxi.driver.presentation.base.lists.RecyclerViewScrollListener.Companion.ITEMS_COUNT_LIMIT
import com.wezom.taxi.driver.presentation.history.list.OrderHistoryAdapter
import javax.inject.Inject


/**
 * Created by Andrew on 13.03.2018.
 */
class OrderHistoryListFragment : BaseFragment(), AdapterClickListener<Trip> {

    private lateinit var binding: OrderHistoryListBinding
    private lateinit var viewModel: OrderHistoryListViewModel

    @Inject
    internal lateinit var adapter: OrderHistoryAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setBackPressFun { viewModel.onBackPressed() }
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
        setOnRefreshCallback {
            clearData()
            viewModel.getHistoryList(ITEMS_COUNT_LIMIT, 0)
        }
        viewModel.loadingLiveData.observe(this, progressObserver)
        viewModel.ordersLiveData.observe(this, Observer<List<Trip>> { trips ->
            if (trips != null) adapter.addData(trips)
            binding.stateView.binding.message.setVisible(trips.isNullOrEmpty())
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = OrderHistoryListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            historyToolbar.setToolbarTitle(getString(R.string.history_toolbar_title))
            setupRecyclerView()
        }
        clearData()
        viewModel.getHistoryList(ITEMS_COUNT_LIMIT, 0)
    }

    override fun onItemClick(position: Int, data: Trip) {
        viewModel.openOrder(data.id)
    }

    private fun setupRecyclerView() {
        adapter.setClickListener(this)
        binding.run {
            recyclerView.adapter = adapter
            recyclerView.addItemDecoration(DividerItemDecoration(activity!!, DividerItemDecoration.VERTICAL))
            val lManager = GridLayoutManager(activity, OrientationHelper.VERTICAL, 1, false)
            recyclerView.layoutManager = lManager
        }
    }

    private fun clearData() {
        adapter.clearData()
    }
}