package com.wezom.taxi.driver.data.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


/**
 * Created by Andrew on 13.03.2018.
 */
data class Trip(
        @SerializedName("id") val id: Int,
        @SerializedName("route") val route: List<Coordinate?>?,
        @SerializedName("coordinates") val coordinates: List<Address>,
        @SerializedName("evos") val evos: Evos,
        @SerializedName("carDelivery") val carDelivery: CarDelivery,
        @SerializedName("driver") val driver: Driver,
        @SerializedName("car") val car: Car,
        @SerializedName("startAddress") val startAddress: Address,
        @SerializedName("finishAddress") val finishAddress: Address?,
        @SerializedName("user") val user: User,
        @SerializedName("time") val time: Long,
        @SerializedName("income") val income: Double,
        @SerializedName("payment") val payment: Payment,
        @SerializedName("services") val services: List<Int>?,
        @SerializedName("comment") val comment: String,
        @SerializedName("notPaidMessage") val notPaidMessage: String?,
        @SerializedName("cancelMessage") val cancelMessage: String?

) : Parcelable {
    constructor(source: Parcel) : this(
            source.readInt(),
            source.createTypedArrayList(Coordinate.CREATOR),
            source.createTypedArrayList(Address.CREATOR),
            source.readParcelable<Evos>(Evos::class.java.classLoader),
            source.readParcelable<CarDelivery>(CarDelivery::class.java.classLoader),
            source.readParcelable<Driver>(Driver::class.java.classLoader),
            source.readParcelable<Car>(Car::class.java.classLoader),
            source.readParcelable<Address>(Address::class.java.classLoader),
            source.readParcelable<Address>(Address::class.java.classLoader),
            source.readParcelable<User>(User::class.java.classLoader),
            source.readLong(),
            source.readDouble(),
            source.readParcelable<Payment>(Payment::class.java.classLoader),
            ArrayList<Int>().apply { source.readList(this, Int::class.java.classLoader) },
            source.readString(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(id)
        writeTypedList(route)
        writeTypedList(coordinates)
        writeParcelable(evos, 0)
        writeParcelable(carDelivery, 0)
        writeParcelable(driver, 0)
        writeParcelable(car, 0)
        writeParcelable(startAddress, 0)
        writeParcelable(finishAddress, 0)
        writeParcelable(user, 0)
        writeLong(time)
        writeDouble(income)
        writeParcelable(payment, 0)
        writeList(services)
        writeString(comment)
        writeString(notPaidMessage)
        writeString(cancelMessage)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Trip> = object : Parcelable.Creator<Trip> {
            override fun createFromParcel(source: Parcel): Trip = Trip(source)
            override fun newArray(size: Int): Array<Trip?> = arrayOfNulls(size)
        }
    }
}