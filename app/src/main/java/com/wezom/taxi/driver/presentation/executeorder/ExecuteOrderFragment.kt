package com.wezom.taxi.driver.presentation.executeorder

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.app.ActivityManager
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.bus.events.*
import com.wezom.taxi.driver.bus.events.CloseOrderAuto
import com.wezom.taxi.driver.common.*
import com.wezom.taxi.driver.data.models.*
import com.wezom.taxi.driver.databinding.ExecuteOrderBinding
import com.wezom.taxi.driver.ext.*
import com.wezom.taxi.driver.presentation.base.BaseMapFragment
import com.wezom.taxi.driver.presentation.customview.EtherSwitchButton
import com.wezom.taxi.driver.presentation.executeorder.dialog.CancelOrderDialog
import com.wezom.taxi.driver.presentation.executeorder.dialog.NotToEndLocationClientDialog
import com.wezom.taxi.driver.presentation.executeorder.dialog.RecommendationDialog
import com.wezom.taxi.driver.presentation.executeorder.dialog.StartTripDialog
import com.wezom.taxi.driver.presentation.main.events.CancelOrderEvent
import com.wezom.taxi.driver.service.AutoCloseOrderService
import com.wezom.taxi.driver.service.TaxometerService
import com.wezom.taxi.driver.taxometer.Action
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import org.jetbrains.anko.support.v4.toast
import timber.log.Timber
import java.lang.ref.WeakReference
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class ExecuteOrderFragment : BaseMapFragment() {
    //region var
    private lateinit var binding: ExecuteOrderBinding
    private lateinit var viewModel: ExecuteOrderViewModel
    lateinit var orderInfo: NewOrderInfo

    private var isShowDialog: Boolean = false

    private var showDialogInTrip: Boolean = false

    private var isShowDialogCloseOrder: Boolean = false

    @Inject
    lateinit var sharedPreferences: SharedPreferences

    private val acceptSwipeAnimatorSet: AnimatorSet by lazy {
        getLeftRightAnimatorSet(binding.acceptLabel)
    }
    private val addressItemParams by lazy {
        LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT)
    }
    private val gestureListener: OnSwipeTouchListener by lazy {
        object : OnSwipeTouchListener(activity!!.applicationContext) {
            override fun onSwipeLeft() {}
            override fun onSwipeTop() {}
            override fun onSwipeBottom() {}

            override fun onTap() {
                context!!.resources.getString(R.string.not_click__swip).longToast(context)
            }

            @SuppressLint("ClickableViewAccessibility")
            override fun onSwipeRight() {
                acceptSwipeAnimatorSet.addListener(object : Animator.AnimatorListener {
                    override fun onAnimationRepeat(animation: Animator?) {}
                    override fun onAnimationCancel(animation: Animator?) {}

                    override fun onAnimationEnd(animation: Animator?) {
                        binding.acceptSwipe.setOnTouchListener(gestureListener)
                    }

                    override fun onAnimationStart(animation: Animator?) {
                        binding.acceptSwipe.setOnTouchListener(null)
                    }
                })

                if (viewModel.isNextOrderStepAllowed(getDelay())) {
                    incrementTripStep(Action.START)
                    updateUiState()
                    viewModel.saveOrderStepTime()
                    acceptSwipeAnimatorSet.start()
                } else {
                    toast(getString(R.string.wait_next_step,
                            getDelay()))
                }
            }
        }
    }

//    private var isNextStepAllowed: Boolean = false
    //endregion

    //region override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initListeners()
        arguments?.let {
            @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
            orderInfo = it.getParcelable(ORDER_INFO_KEY)
        }

        Timber.d("AcceptOrderRequest ${orderInfo.order!!.distanceFromDriver}")

        if (orderInfo.order?.customerAppOrdersCount == 0)
            showDialogCallClient()
        viewModel = ViewModelProviders.of(this,
                viewModelFactory)
                .getViewModelOfType()

//        viewModel.getRefuseReasons()

        viewModel.timerLiveData.observe(this,
                Observer {
                    sePlan(it!!, orderInfo.formula?.sw?.fw!!)
//                    val dialog = showDecisionDialog(R.string.awaiting,
//                            R.string.awating_text,
//                            true)
//                    dialog.listener =
//                            object : InfoDialog.DialogClickListener {
//                                override fun onPositiveClick() {
//                                    incrementTripStep(Action.START_DELAYED)
//                                    viewModel.saveOrderStepTime(0) // allow next step
//                                    updateUiState()
//                                    dialog.dismiss()
//                                }
//
//                                override fun onNegativeClick() {
////                                    viewModel.startTimer(orderInfo.formula?.sw?.deviateTime ?: 0)
////                                    dialog.dismiss()
//                                }
//                            }
                })

        viewModel.realTimerLiveData.observe(this,
                Observer {
                    if (it!! >= orderInfo.formula?.sw?.fw!! * 60)
                        realTimeWait += it
                })
    }

    var realTimeWait: Long = 0
    var realCounterTimer: Long = 0

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        binding = ExecuteOrderBinding.inflate(inflater)
//        if (orderInfo.order!!.status == OrderStatus.ON_THE_WAY)
//            if (orderInfo.formula?.enableWait == 0)
//                binding.plainContainer.setVisible(false)
//            else
//                binding.plainContainer.setVisible(true)

        if (orderInfo.order!!.status == OrderStatus.ARRIVED_AT_PLACE && orderInfo.order!!.calculatedTime!! <= 0) {
            binding.etaContainer.setVisible(true)
            if (!isMyServiceRunning(TaxometerService::class.java)) {
                TaxometerService.startWait(WeakReference(activity!!),
                        orderInfo)
            }
        } else if (orderInfo.order!!.status == OrderStatus.ACCEPTED) {
            if (orderInfo.order!!.carArrivalTime == 0L || orderInfo.order!!.carArrivalTime == null) {
                orderInfo.order!!.carArrivalTime = orderInfo.order!!.currentTime!!
            }
            var currentTime = orderInfo.order!!.currentTime!!.toLong()
            if (orderInfo.order!!.preliminaryTime!!.toLong() != 0L) {
                currentTime = orderInfo.order!!.preliminaryTime!!.toLong() - currentTime
            } else {
                currentTime = orderInfo.order!!.carArrivalTime!!.toLong() - currentTime
            }
            orderInfo.order!!.calculatedTime = currentTime / 1000
            if (!isMyServiceRunning(TaxometerService::class.java)) {
                TaxometerService.startAcceptTime(WeakReference(activity!!),
                        orderInfo)
            }
        }

        if (!isMyServiceRunning(AutoCloseOrderService::class.java))
            AutoCloseOrderService.start(WeakReference(activity!!),
                    orderInfo)

        return binding.root
    }


    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        var manager: ActivityManager = context!!.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        @Suppress("DEPRECATION")
        for (service: ActivityManager.RunningServiceInfo in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }


    override fun onViewCreated(view: View,
                               savedInstanceState: Bundle?) {
        super.onViewCreated(view,
                savedInstanceState)


        binding.run {
            toolbar.binding.run {
                etherButton.initAsList()
                etherButton.listener = object : EtherSwitchButton.OnEtherClickListener {
                    override fun onClick(state: EtherSwitchButton.State) {
                        viewModel.switchEtherScreen()
                    }
                }
            }
        }
        setViewVisibility()
        setData()
        clicks()
        updateUiState()
        if (viewModel.isStepTimeUnspecified()) {
            viewModel.saveOrderStepTime()
        }
        disposable += RxBus.listen(PlanTaxometr::class.java).subscribe {
            realCounterTimer = it.second
            sePlan(it.second, orderInfo.formula?.sw?.fwt!!.toLong())
        }

        disposable += RxBus.listen(PlanTaxometrTrip::class.java).subscribe {
            sePlan(it.second)
        }

        disposable += RxBus.listen(PlanTaxometrAccept::class.java).subscribe {
            sePlanAccept(it.second)
        }

        disposable += RxBus.listen(PlanTaxometrSecond::class.java).subscribe {
            realCounterTimer = it.second
            if (it.second > 0)
                sePlan(it.second, orderInfo.formula?.sw?.fw!!.toLong())
        }

        disposable += RxBus.listen(MoveDriverTaxometr::class.java).subscribe {
            showStartTripDialog()
        }

        disposable += RxBus.listen(PlanTaxometrWait::class.java).subscribe {
            if (it.realSecond >= orderInfo.formula?.sw?.fw!! * 60)
                realTimeWait += it.realSecond
        }


    }
//endregion

    //region fun
    private fun initListeners() {
        disposable += RxBus.listen(CancelOrderEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Timber.d("CancelOrderEvent")
                    viewModel.cancelOrderByCustomer(it.orderId,
                            lastLocation)
                    showCustomerCancelOrderDialog()
                },
                        Timber::e)

        disposable += RxBus.listen(CloseOrderAuto::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Timber.d("CancelOrderEventAuto")

                    WeakReference(activity!!).get()!!.stopService(Intent(WeakReference(activity!!).get(), TaxometerService::class.java))
                    WeakReference(activity!!).get()!!.stopService(Intent(WeakReference(activity!!).get(), AutoCloseOrderService::class.java))

                    it.resultData.orderId?.let { it1 ->
                        viewModel.cancelOrderAuto(it1,
                                lastLocation)
                    }
                    showAutoCancelOrderDialog()
                },
                        Timber::e)

        disposable += RxBus.listen(GpsEvent::class.java)
                .subscribe {
                    if (!it.isEnabled) showGpsDialog(context!!,
                            fragmentManager!!)
                }

        disposable += RxBus.listen(AutoCloseOrderEvent::class.java)
                .subscribe {
                    showCancelOrderDialog()
                }
    }

    override fun onResume() {
        super.onResume()
        Handler().postDelayed({
            if (isShowDialogCloseOrder) {
                showCancelOrderDialog()
            }
        }, 1000)
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun clicks() {
        binding.run {
            disposable += RxView.clicks(etaContainer)
                    .subscribe {
                        showInfoDialog(R.drawable.ic_arrival_time_ill,
                                R.string.time_filing,
                                R.string.time_filing_text)
                    }

            disposable += RxView.clicks(coefficient)
                    .subscribe {

                    }

            disposable += RxView.clicks(phoneImage)
                    .subscribe {
                        orderInfo.order?.user!!.phone?.let {
                            makeCallClick()
                            viewModel.sendHelpTime(orderInfo.order!!.id!!,
                                    SendHelpTimeType.PHONE)
                        }
                    }

            disposable += RxView.clicks(cancel)
                    .debounce(200, TimeUnit.MILLISECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        viewModel.cancelOrder(orderInfo.order!!)
                    }

            disposable += RxView.clicks(navigator)
                    .subscribe {
                        viewModel.onNavigatorClick(lastLocation!!,
                                orderInfo.order!!)
                    }

            disposable += RxView.clicks(paymentMethodIcon)
                    .subscribe {
                        if (orderInfo.order?.payment?.method == PaymentMethod.CASH) {
                            showInfoDialog(R.string.cash,
                                    R.string.cash_text)
                        } else if (orderInfo.order?.payment?.method == PaymentMethod.CARD_AND_BONUS) {
                            showInfoDialog(R.string.bank_card,
                                    R.string.bank_card_text)
                        }
                    }

            acceptSwipe.setOnTouchListener(gestureListener)
        }
    }

    private fun showDialogCallClient() {
        if (sharedPreferences.getBoolean(CALL_CLIENT, true)) {
            val dialog = RecommendationDialog()
            dialog.listener = object : RecommendationDialog.DialogClickListener {
                override fun onPositiveClick() {
                    dialog.dismiss()
                    makeCallClick()
                    sharedPreferences.edit().putBoolean(CALL_CLIENT, false).apply()
                }

                override fun onNegativeClick() {
                    dialog.dismiss()
                    sharedPreferences.edit().putBoolean(CALL_CLIENT, false).apply()
                }
            }
            dialog.show(fragmentManager, RecommendationDialog.TAG)
        }
    }

    private fun showDialogNotToEndLocationClient() {
        val dialog = NotToEndLocationClientDialog()
        dialog.listener = object : NotToEndLocationClientDialog.DialogClickListener {
            override fun onPositiveClick() {
                viewModel.finishOrder(orderInfo,
                        lastLocation)
                dialog.dismiss()
            }

            override fun onNegativeClick() {
                dialog.dismiss()
            }
        }
        dialog.show(fragmentManager, NotToEndLocationClientDialog.TAG)
    }

    private var isShowStartTripDialogNext: Boolean = false
    private var isShowCancelOrderDialog: Boolean = false
    private var time = System.currentTimeMillis()

    private fun showStartTripDialog() {
        if ((time + 180000L) < System.currentTimeMillis()) {
            time = System.currentTimeMillis()
            if (!showDialogInTrip)
                when (orderInfo.order!!.status) {
                    OrderStatus.ARRIVED_AT_PLACE -> {
                        showDialogInTrip = true
                        if (!isShowDialog) {
                            isShowStartTripDialogNext = false
                            isShowDialog = true
                            val dialog = StartTripDialog()
                            dialog.listener = object : StartTripDialog.DialogClickListener {
                                override fun onPositiveClick() {
                                    incrementTripStep(Action.START)
                                    updateUiState()
                                    viewModel.saveOrderStepTime()
                                    acceptSwipeAnimatorSet.start()
                                    showDialogInTrip = false
                                    isShowDialog = false
                                    if (isShowCancelOrderDialog) {
                                        showCancelOrderDialog()
                                    }
                                }

                                override fun onNegativeClick() {
                                    showDialogInTrip = false
                                    isShowDialog = false
                                    if (isShowCancelOrderDialog) {
                                        showCancelOrderDialog()
                                    }
                                }
                            }
                            try {
                                dialog.show(fragmentManager, StartTripDialog.TAG)
                            } catch (e: IllegalStateException) {
                                e.printStackTrace()
                            }

                        } else {
                            isShowStartTripDialogNext = true
                        }
                    }
                }
        }

    }


    private fun showCancelOrderDialog() {
        if (!isShowDialog) {
            isShowCancelOrderDialog = false
            isShowDialog = true
            val dialog = CancelOrderDialog()
            dialog.listener = object : CancelOrderDialog.DialogClickListener {
                override fun onPositiveClick() {
//                orderInfo.order!!.status = OrderStatus.ON_THE_WAY
//                incrementTripStep(Action.START)
//                    viewModel.closeOrder(orderInfo)
//                    if (orderInfo.order!!.getLocationLastPoint() != null &&
//                            lastLocation!!.distanceTo(orderInfo.order!!.getLocationLastPoint()) >= 500) {
//                        showDialogNotToEndLocationClient()
//                        return
//                    }


                    if (OrderStatus.ON_THE_WAY == orderInfo.order!!.status) {
                        viewModel.finishOrder(orderInfo,
                                lastLocation)
                    } else {
                        if (orderInfo.order!!.status == OrderStatus.ACCEPTED) {
                            orderInfo.order!!.status = OrderStatus.ARRIVED_AT_PLACE
                            viewModel.arrivedAtPlace(orderInfo.order!!.id!!,
                                    lastLocation?.longitude,
                                    lastLocation?.latitude,
                                    System.currentTimeMillis())
                            viewModel.updateOrder(orderInfo,
                                    lastLocation)
                        }
                        if (orderInfo.order!!.status == OrderStatus.ARRIVED_AT_PLACE) {
                            orderInfo.order!!.status = OrderStatus.ON_THE_WAY
                            orderInfo.startTimeWaitPassager = realTimeWait
                            viewModel.onTheWay(orderInfo.order!!.id!!,
                                    lastLocation?.longitude,
                                    lastLocation?.latitude,
                                    System.currentTimeMillis())
                            viewModel.updateOrder(orderInfo,
                                    lastLocation)
                        }

                        viewModel.autoCloseOrder(orderInfo,
                                lastLocation)
                    }
                    isShowDialog = false
                    dialog.dismiss()
                    WeakReference(context).get()!!.stopService(Intent(WeakReference(context).get(), AutoCloseOrderService::class.java))
//                    if (isShowStartTripDialogNext) {
//                        showStartTripDialog()
//                    }
                }

                override fun onNegativeClick() {
                    isShowDialog = false
                    dialog.dismiss()
                    if (isShowStartTripDialogNext) {
                        showStartTripDialog()
                    }
                }
            }
            if (isResumed() && !isRemoving()) {
                dialog.show(fragmentManager, CancelOrderDialog.TAG)
                isShowDialogCloseOrder = false
            } else {
                isShowDialogCloseOrder = true
                isShowCancelOrderDialog = false
                isShowDialog = false
            }
        } else {
            isShowCancelOrderDialog = true
        }
    }

    private fun showCustomerCancelOrderDialog() {
        val builder = AlertDialog.Builder(context!!)
                .setTitle(getString(R.string.attention2))
                .setMessage(getString(R.string.order_canceled_by_customer))
                .setPositiveButton(R.string.ok) { p0, _ ->
                    p0.dismiss()
                }
                .setCancelable(false)

        val dialog = builder.create()
        dialog.show()
    }

    private fun showAutoCancelOrderDialog() {
        val builder = AlertDialog.Builder(context!!)
                .setTitle(getString(R.string.attention2))
                .setMessage(getString(R.string.order_canceled_by_auto))
                .setPositiveButton(R.string.ok) { p0, _ ->
                    p0.dismiss()
                }
                .setCancelable(false)

        val dialog = builder.create()
        dialog.show()
    }

    private fun updateUiState() {
        binding.acceptLabel.text = setNextActionText()
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<out String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode,
                permissions,
                grantResults)
        grantResults.filter { it == PackageManager.PERMISSION_DENIED }
                .forEach { return }

        when (requestCode) {
            CALL_PHONE_REQUEST -> {
                orderInfo.order?.user!!.phone?.let {
                    dialUser(it)
                }
            }
        }
    }

    private fun setData() {
        binding.run {
            orderInfo.order?.let {
                if (it.carArrival == 0) {
                    eta.text = "00:00"
                } else {
                    eta.text = it.preliminaryTime?.convertToMMss()
                }

                val coef = it.currentCoefficient!!
                if (coef <= 1.0) {
                    coefficientContainer.setVisible(false)
                }


                coefficient.text = coef.toString()
                services.setActiveStatus(true)
                services.addServices(*it.services?.toIntArray() ?: intArrayOf())
                comment.text = it.comment
                if (!it.user?.name.isNullOrEmpty()) {
                    userName.text = it.user?.name
                } else {
                    userName.text = getString(R.string.name_client)
                }

                val ratingVal = it.user?.rating
                ratingVal?.let {
                    userRating.text = it.toString()
                }

                if (ratingVal == 0.0f || ratingVal == null) {
                    ratingContainer.setVisible(false)
                } else {
                    ratingContainer.setVisible(true)
                }

                it.payment?.let { it ->
                    val estBonus: Double = orderInfo.formula?.estimatedBonuses ?: 0.0
                    Timber.d("ESTIMATE_estimatedBonuses: $estBonus")
                    val surchargeValue = it.surcharge?.let { it1 -> Math.round(it1) } ?: 0
                    if (surchargeValue == 0L && estBonus == 0.0) {
                        surchargeContainer.setVisible(false)
                    } else {
                        surchargeContainer.setVisible(true)
                    }
                    surcharge.text = (surchargeValue.plus(estBonus)).toInt()
                            .toString()
                }
                paymentMethodIcon.setImageResource(getPaymentResource(it.payment?.method!!))

                it.payment?.estimatedCost?.let {
                    estimatedAmount.text = it.toString()
                }

                Timber.d("ESTIMATE_estimatedCost: ${orderInfo.order?.payment?.estimatedCost}")
                Timber.d("ESTIMATE_surcharge: ${orderInfo.order?.payment?.surcharge!!}")

                Timber.d("orfer_fragment ${orderInfo.order!!.distanceFromDriver}")

                var distans = 0.0
                if (it.distanceFromDriver != null && it.distanceFromDriver != 0.0) {
                    distans = String.format("%.1f", it.distanceFromDriver!! / 1000).replace(",", ".").toDouble()
                }

                populateDestination(it.coordinates!!,
                        distans,
                        it.calculatedPath)
                val photo = orderInfo.order?.user?.photo
                if (!photo.isNullOrBlank()) {
                    loadRoundedImage(context!!,
                            avatarImage,
                            url = photo)
                }

                if (orderInfo.order!!.payment!!.method!! == PaymentMethod.CARD) {
                    compensationLabel.text = getString(R.string.pay_suc)
                    if (orderInfo.order!!.payment!!.cardType == 0) {
                        paymentMethodIcon.setImageResource(R.mipmap.ic_visa)
                    } else {
                        paymentMethodIcon.setImageResource(R.mipmap.ic_mastercard)
                    }
                }
            }
            if (orderInfo.order!!.coordinates!!.size == 1) {
                paymentMethodIcon.visibility = View.GONE
                uaSign2.visibility - View.GONE
                estimatedAmount.text = estimatedAmount.resources.getString(com.wezom.taxi.driver.R.string.auto_check)
            }
        }
    }


    private fun setNextActionText(): String {
        return when (orderInfo.order!!.status) {
            OrderStatus.ACCEPTED -> {
                getString(R.string.in_place)
            }
            OrderStatus.ARRIVED_AT_PLACE -> {
                getString(R.string.start_trip)
            }
            OrderStatus.ON_THE_WAY -> {
                getString(R.string.total_cost)
            }
            else -> ""
        }
    }

    private fun getDelay(): Long {
        return when (orderInfo.order?.status) {
            OrderStatus.ACCEPTED -> 5
            OrderStatus.ARRIVED_AT_PLACE -> 5
            OrderStatus.ON_THE_WAY -> 5
            else -> 0
        }
    }

    private fun setViewVisibility() {
        binding.run {
            orderInfo.order?.acceptedOrderShowing?.let {
                viewsVisibilitySwitch(it.isPreliminaryTimeShow!!,
                        etaCoefContainer)
                viewsVisibilitySwitch(it.isCurrentCoefficientShow!!,
                        coefficientContainer)
                viewsVisibilitySwitch(if (orderInfo.order?.services!!.isEmpty() || orderInfo.order?.services == null) false else it.isServiceShow!!,
                        servicesContainer)
                viewsVisibilitySwitch(if (orderInfo.order?.comment.isNullOrEmpty()) false else it.isCommentShow!!,
                        commentContainer)
                viewsVisibilitySwitch(it.isSurchargeShow!!,
                        surchargeContainer)
                viewsVisibilitySwitch(it.isMethodShow!!,
                        customerPaymentContainer)
                viewsVisibilitySwitch(it.isEstimatedCostShow!!,
                        estimatedAmount,
                        uaSign2)

                contactsContainer.setVisible(true)
                navigator.setVisible(true)
                controlsContainer.setVisible(true)

            }
        }
    }

    private fun getPaymentResource(payment: PaymentMethod): Int {
        return when (payment) {
            PaymentMethod.CASH -> R.drawable.ic_cash
            PaymentMethod.CASH_AND_BONUS -> R.drawable.ic_cash
            PaymentMethod.CARD -> R.drawable.ic_card
            PaymentMethod.CARD_AND_BONUS -> R.drawable.ic_card
        }
    }

    private fun makeCallClick() {
        if (!isPermissionGranted(this@ExecuteOrderFragment,
                        Manifest.permission.CALL_PHONE)) {
            requestPermissions(arrayOf(Manifest.permission.CALL_PHONE),
                    CALL_PHONE_REQUEST)
        } else {
            orderInfo.order?.user!!.phone?.let {
                dialUser(it)
            }
        }
    }

    private fun populateDestination(addresses: List<Address>,
                                    start: Double?,
                                    end: Double?) {
        with(binding.addressContainer) {
            removeAllViews()
            if (viewModel.isAddressShow() != null && viewModel.isAddressShow() == true) {
                when (addresses.size) {
                    0 -> return
                    1 -> {
                        val first = getOpeningItem(start,
                                formatAddress(addresses[0]))
                        val second = AddressItem(context).apply {
                            setData(indicator = R.drawable.ic_address_to,
                                    itemIcon = R.drawable.ic_route,
                                    distanceText = "$end км",
                                    addressText = getString(R.string.around_town))
                        }

                        addAll(first,
                                second)
                    }
                    2 -> {
                        val first = getOpeningItem(start,
                                formatAddress(addresses[0]))
                        val second = AddressItem(context).apply {
                            setData(R.drawable.ic_address_to,
                                    R.drawable.ic_route,
                                    "$end км",
                                    formatAddress(addresses[1]))
                        }

                        addAll(first,
                                second)
                    }
                    else -> {
                        addresses.forEachIndexed { index, address ->
                            addView(when (index) {
                                0 -> {
                                    AddressItem(context).apply {
                                        setData(R.drawable.ic_address_from,
                                                R.drawable.ic_near_me,
                                                "$start км",
                                                formatAddress(address),
                                                showBottomSeparator = true)
                                    }
                                }

                                addresses.size - 1 -> {
                                    AddressItem(context).apply {
                                        setData(R.drawable.ic_address_to,
                                                R.drawable.ic_route,
                                                "$end км",
                                                formatAddress(address),
                                                showTopSeparator = true)
                                    }
                                }
                                else -> {
                                    AddressItem(context).apply {
                                        setData(addressText = formatAddress(address),
                                                showSeparator = true)
                                    }
                                }
                            },
                                    addressItemParams)
                        }
                    }
                }
            } else {
                if (addresses.isNotEmpty()) {
                    val first = getOpeningItem(start,
                            formatAddress(addresses[0]))
                    val second = AddressItem(context).apply {
                        setData(itemIcon = R.drawable.ic_route,
                                distanceText = "$end км")
                    }
                    addAll(first,
                            second)
                } else return
            }


            setVisible(true)
        }
    }

    private fun getOpeningItem(start: Double?,
                               address: String): AddressItem {
        return AddressItem(context!!).apply {
            setData(R.drawable.ic_address_from,
                    R.drawable.ic_near_me,
                    "$start км",
                    address)
        }
    }

    private fun getLeftRightAnimatorSet(view: View): AnimatorSet {
        val animatorSet = AnimatorSet()
        val translateRight = ObjectAnimator.ofFloat(view,
                "translationX",
                600f)

        val hide = ObjectAnimator.ofFloat(view,
                View.ALPHA,
                1f,
                0f)
        hide.duration = 0

        val show = ObjectAnimator.ofFloat(view,
                View.ALPHA,
                0f,
                1f)
        show.duration = 0

        val translateLeft = ObjectAnimator.ofFloat(view,
                "translationX",
                -600f)

        val translateInitial = ObjectAnimator.ofFloat(view,
                "translationX",
                0f)

        animatorSet.playSequentially(translateRight,
                hide,
                translateLeft,
                show,
                translateInitial)
        return animatorSet
    }

    @SuppressLint("SetTextI18n")
    public fun sePlan(second: Long, timeFree: Long) {

        setTime(second)
        val house = second / 3600
        val minut = (second - 3600 * house) / 60
        if (minut >= timeFree || house > 0) {
            binding.etaName.setText(R.string.pay_wait)
        } else {
            binding.etaName.setText(R.string.free_wait)
        }
    }

    public fun setTime(second: Long) {
        binding.eta.setText("")
        val house = second / 3600
        val minut = (second - 3600 * house) / 60
        val sec = second % 60
        if (house == 0L) {
            binding.eta.append("")
        } else if (house <= 9) {
            binding.eta.append("0$house:")
        } else {
            binding.eta.append("$house:")
        }

        if (minut == 0L) {
            binding.eta.append("00:")
        } else if (minut <= 9) {
            binding.eta.append("0$minut:")
        } else {
            binding.eta.append("$minut:")
        }


        if (sec == 0L) {
            binding.eta.append("00")
        } else if (sec <= 9) {
            binding.eta.append("0$sec")
        } else {
            binding.eta.append("$sec")
        }


    }


    public fun sePlan(second: Long) {
        setTime(second)
        // if (minut >= orderInfo.formula?.sw?.fw!! || house > 0) {
        binding.etaName.setText(R.string.in_trip)
        //binding.plain.text = "$house:$minut:$sec"
    }

    public fun sePlanAccept(second: Long) {
        if (second < 0)
            setTime(second * -1)
        else
            setTime(second)
        orderInfo.order!!.calculatedTime = second
        // if (minut >= orderInfo.formula?.sw?.fw!! || house > 0) {
        binding.etaName.setText(R.string.to_order)
        if (second < 0) {
            binding.etaName.setText(R.string.late)
        }
        //binding.plain.text = "$house:$minut:$sec"
    }


    private fun incrementTripStep(action: Action) {
        when (orderInfo.order!!.status) {
            OrderStatus.ACCEPTED -> {
                binding.etaContainer.setVisible(true)
                WeakReference(activity!!).get()!!.stopService(Intent(WeakReference(activity!!).get(), TaxometerService::class.java))
                TaxometerService.startWait(WeakReference(activity!!),
                        orderInfo)
                orderInfo.order!!.status = OrderStatus.ARRIVED_AT_PLACE
                viewModel.arrivedAtPlace(orderInfo.order!!.id!!,
                        lastLocation?.longitude,
                        lastLocation?.latitude,
                        System.currentTimeMillis())
                viewModel.updateOrder(orderInfo,
                        lastLocation)
                sharedPreferences.edit().putInt(TIMER_NOT_ENTER_CLIENT, 0).apply()
            }
            OrderStatus.ARRIVED_AT_PLACE -> {
                sharedPreferences.edit().putBoolean(CALL_CLIENT, true).apply()
                if (realCounterTimer >= orderInfo.formula?.sw?.fw!! * 60)
                    realTimeWait += realCounterTimer
                if (realTimeWait < orderInfo.formula?.sw?.fw!! * 60)
                    realTimeWait = 0

                WeakReference(activity!!).get()!!.stopService(Intent(WeakReference(activity!!).get(), TaxometerService::class.java))

                orderInfo.order!!.status = OrderStatus.ON_THE_WAY
                orderInfo.startTimeWaitPassager = realTimeWait
//                if (orderInfo.formula?.enableWait == 0)
//                    binding.etaContainer.setVisible(false)
//                else
//                    binding.etaContainer.setVisible(true)
                viewModel.onTheWay(orderInfo.order!!.id!!,
                        lastLocation?.longitude,
                        lastLocation?.latitude,
                        System.currentTimeMillis())

                if (action == Action.START) {
                    TaxometerService.start(WeakReference(activity!!),
                            orderInfo)
                }
                if (action == Action.START_DELAYED) {
                    TaxometerService.startDelayed(WeakReference(activity!!),
                            orderInfo)
                }
                viewModel.updateOrder(orderInfo,
                        lastLocation)
            }
            OrderStatus.ON_THE_WAY -> {
                if (orderInfo.order!!.getLocationLastPoint() != null &&
                        lastLocation!!.distanceTo(orderInfo.order!!.getLocationLastPoint()) >= 500) {
                    showDialogNotToEndLocationClient()
                    return
                }
                viewModel.finishOrder(orderInfo,
                        lastLocation)

            }
        }
    }

    fun isPreAndroidO(): Boolean = Build.VERSION.SDK_INT < Build.VERSION_CODES.O

    private fun Long.convertToMMss() = SimpleDateFormat("mm:ss",
            Locale("uk",
                    "UA")).format(Date(this))

    private fun ViewGroup.addAll(vararg views: View) {
        views.forEach {
            addView(it,
                    addressItemParams)
        }
    }
//endregion

    override fun onDestroy() {
        super.onDestroy()

    }

    companion object {

        private const val ORDER_INFO_KEY = "order_info_key"

        const val CALL_PHONE_REQUEST = 999
        const val SEND_SMS_REQUEST = 998

        fun newInstance(orderInfo: NewOrderInfo): ExecuteOrderFragment {
            val args = Bundle()
            args.putParcelable(ORDER_INFO_KEY,
                    orderInfo)
            val fragment = ExecuteOrderFragment()
            Timber.d("AcceptOrderRequest ${orderInfo.order!!.distanceFromDriver}")
            fragment.arguments = args
            return fragment
        }
    }
}