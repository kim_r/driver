package com.wezom.taxi.driver.presentation.discounts.list

import android.view.ViewGroup
import com.wezom.taxi.driver.data.models.Shares
import com.wezom.taxi.driver.presentation.base.lists.BaseAdapter
import com.wezom.taxi.driver.presentation.base.lists.BaseViewHolder
import javax.inject.Inject

/**
 * Created by udovik.s on 19.03.2018.
 */
class DiscountsAdapter @Inject constructor() :
    BaseAdapter<Shares, DiscountsItemView, BaseViewHolder<DiscountsItemView>>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<DiscountsItemView> {
        val itemView = DiscountsItemView(parent.context)
        return BaseViewHolder(itemView)
    }
}