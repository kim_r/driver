package com.wezom.taxi.driver.presentation.executeorder

import android.content.Context
import android.support.annotation.DrawableRes
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.wezom.taxi.driver.databinding.AddressItemBinding


class AddressItem : LinearLayout {
    private val binding: AddressItemBinding =
            AddressItemBinding.inflate(LayoutInflater.from(context), this, true)

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
            super(context, attrs, defStyleAttr)

    fun setData(
            @DrawableRes indicator: Int? = null,
            @DrawableRes itemIcon: Int? = null,
            distanceText: String? = null,
            addressText: String? = null,
            showSeparator: Boolean = false,
            showTopSeparator: Boolean = false,
            showBottomSeparator: Boolean = false
    ) {
        with(binding) {
            if (showSeparator)
                line.visibility = View.VISIBLE

            if (showTopSeparator)
                smallSeparatorTop.visibility = View.VISIBLE

            if (showBottomSeparator)
                smallSeparatorBottom.visibility = View.VISIBLE

            distance.text = distanceText
            address.text = addressText

            indicator?.let {
                indicatorIcon.setImageDrawable(ContextCompat.getDrawable(context, it))
            }

            itemIcon?.let {
                icon.setImageDrawable(context.resources.getDrawable(it))
            }
        }
    }
}