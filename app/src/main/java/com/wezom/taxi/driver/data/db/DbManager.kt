package com.wezom.taxi.driver.data.db

import com.wezom.taxi.driver.data.models.*
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by zorin.a on 21.05.2018.
 */
class DbManager @Inject constructor(private val orderInfoDao: OrderInfoDao,
                                    private val orderCoordinatesDao: OrderCoordinatesDao,
                                    private val reasonsDao: ReasonsDao,
                                    private val orderResultDataDao: OrderResultDataDao,
                                    private val finalizationDao: OrderFinalizationDao) {
    fun insertOrderInfo(orderInfo: NewOrderInfo): Single<Long> {
        return Single.fromCallable {
            orderInfoDao.insertOrderInfo(orderInfo)
        }.subscribeOn(Schedulers.io())
    }

    fun deleteOrderInfo(orderInfo: NewOrderInfo) {
        orderInfoDao.delete(orderInfo)
    }

    fun deleteOrderById(id: Long) {
        orderInfoDao.deleteOrderById(id)
    }

    fun updateOrderInfo(orderInfo: NewOrderInfo) {
        orderInfoDao.updateOrderInfo(orderInfo)
    }

    fun queryOrderInfoById(id: Int): NewOrderInfo {
        return orderInfoDao.queryOrderInfo(id)
    }

    fun queryOrdersQuantity(): Int {
//        var tmp = 0
//        orderInfoDao.queryAll().subscribe({ it ->
//            tmp = it.size
//        })
        return orderInfoDao.queryAll().size
    }

    fun insertOrderCoordinates(orderInfo: OrderCoordinates) {
        orderCoordinatesDao.insertOrderCoordinates(orderInfo)
    }

    fun deleteOrderCoordinates(orderInfo: OrderCoordinates) {
        orderCoordinatesDao.delete(orderInfo)
    }

    fun deleteOrderCoordinatesById(id: Long) {
        orderCoordinatesDao.deleteOrderCoordinatesById(id)
    }

    fun updateOrderCoordinates(orderInfo: OrderCoordinates) {
        orderCoordinatesDao.updateOrderCoordinates(orderInfo)
    }

    fun queryOrderCoordinatesById(id: Int): OrderCoordinates? = orderCoordinatesDao.queryOrderCoordinates(id)


    fun clearCoordinates() {
        orderCoordinatesDao.clearCoordinates()
    }

    fun clearDatabase() {
        orderInfoDao.clearDatabase()
    }

    fun queryAll(): List<NewOrderInfo> {
        return orderInfoDao.queryAll()
//        var tmp = mutableListOf<NewOrderInfo>()
//        orderInfoDao.queryAll().subscribe({ it ->
//            tmp = it.toMutableList()
//        }, {})
//        return tmp
    }

    fun insertReasons(reasonList: List<Reason>) {
        reasonsDao.insertReasons(reasonList)
    }

    fun queryAllReasons(): List<Reason> {
        return reasonsDao.queryAll()
    }

    fun queryReasonByStatus(status: OrderStatus): List<Reason> = reasonsDao.queryReasonByStatus(status)


    fun clearReasonsTable() = reasonsDao.clearDatabase()


    fun insertOrderResultData(orderResultData: OrderResultData) {
        orderResultDataDao.insertOrderResultData(orderResultData)
    }

    fun gueryOrderResult(id: Int): OrderResultData? {
        return orderResultDataDao.queryOrderResultData(id)
    }

    fun deleteOrderResult(id: Int) {
        orderResultDataDao.deleteOrderResultById(id.toLong())
    }

    fun clearOrderResult() {
        orderResultDataDao.clearOrderResult()
    }

    fun queryFinalization() = finalizationDao.queryFinalization()

    fun updateFinalization(orderFinalization: OrderFinalization) = finalizationDao.updateFinalization(orderFinalization)

    fun insertFinalization(orderFinalization: OrderFinalization) = finalizationDao.insertFinalization(orderFinalization)

    fun deleteFinalization() = finalizationDao.clearDatabase()
}