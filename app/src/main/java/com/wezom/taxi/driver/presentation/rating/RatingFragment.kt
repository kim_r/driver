package com.wezom.taxi.driver.presentation.rating

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxRatingBar
import com.jakewharton.rxbinding2.widget.RxTextView
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.common.formatAddress
import com.wezom.taxi.driver.common.loadRoundedImage
import com.wezom.taxi.driver.data.models.NewOrderInfo
import com.wezom.taxi.driver.data.models.Trip
import com.wezom.taxi.driver.databinding.RatingBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.ext.setVisible
import com.wezom.taxi.driver.ext.shortToast
import com.wezom.taxi.driver.presentation.base.BaseFragment
import io.reactivex.rxkotlin.Observables
import io.reactivex.rxkotlin.plusAssign

/**
 * Created by udovik.s on 22.03.2018.
 */
class RatingFragment : BaseFragment() {

    //region var
    private lateinit var binding: RatingBinding
    private lateinit var viewModel: RatingViewModel

    private lateinit var orderInfo: NewOrderInfo
    private lateinit var delayedData: Trip
    private var mode: Int = 0
    //endregion

    //region override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setBackPressFun { viewModel.onBackPressed() }
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
        mode = arguments!!.getInt(MODE_KEY)
        if (mode == MODE_INSTANT_RATE) {
            orderInfo = arguments!!.getParcelable(DATA_KEY)
        }
        if (mode == MODE_DELAYED_RATE || mode == MODE_HISTORY_RATE) {
            delayedData = arguments!!.getParcelable(DATA_KEY)
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = RatingBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (mode == MODE_INSTANT_RATE) {
            updateUi(orderInfo)
        }
        if (mode == MODE_DELAYED_RATE || mode == MODE_HISTORY_RATE) {
            binding.tvRateLater.setVisible(false)
            updateUi(delayedData)
        }

        binding.run {
            toolbar.binding.newOrder.setVisible(false)

            disposable += RxView.clicks(btnRate).subscribe({
                //Todo close current fragment, show main, or other order screen if it exits
                getString(R.string.thanks_for_rate).shortToast(context)
                viewModel.rateTrip(
                        if (mode == MODE_INSTANT_RATE) orderInfo.order!!.id!! else delayedData.id,
                        ratingBar.rating.toInt(),
                        editAddComment.text.toString(),
                        mode == MODE_HISTORY_RATE)
            })

            disposable += RxView.clicks(tvRateLater).subscribe({
                viewModel.nextAction(mode == MODE_HISTORY_RATE)
            })

            val rateObservable = RxRatingBar.ratingChanges(ratingBar)
            val commentObservable = RxTextView.textChanges(editAddComment)

            val isButtonEnabled = Observables.combineLatest(rateObservable, commentObservable) { rate, comment ->
                isValuesValid(rate.toInt(), comment.toString())
            }

            disposable += isButtonEnabled.subscribe({ it ->
                enableRateButton(it)
            })
        }
    }
    //endregion

    //region fun
    fun updateUi(data: NewOrderInfo) {
        val order = data.order
        binding.run {
            tvFrom.text = formatAddress(order!!.coordinates!!.first())
            when (order.coordinates?.size) {
                0 -> return
                1 -> {
                    tvTo.text = getString(R.string.around_town)
                }
                else -> {
                    tvTo.text = formatAddress(order.coordinates!!.last())
                }
            }

            tvName.text = order.user!!.name
            val photo = order.user!!.photo
            if (!photo.isNullOrBlank()) {
                loadRoundedImage(context!!, imAvatar, url = order.user!!.photo)
            }
        }
    }

    fun updateUi(data: Trip) {
        binding.run {
            tvFrom.text = formatAddress(data.coordinates.first())

            when (data.coordinates.size) {
                0 -> return
                1 -> {
                    tvTo.text = getString(R.string.around_town)
                }
                else -> {
                    tvTo.text = formatAddress(data.coordinates.last())
                }
            }

            tvName.text = data.user.name
            if (!data.user.photo.isNullOrBlank())
                loadRoundedImage(context!!, imAvatar, url = data.user.photo)
        }
    }

    private fun isValuesValid(rate: Int, comment: String): Boolean {
//        return rate > 3 || (rate < 4 && comment.isNotEmpty() && rate > 0 && comment.length >= 10 && comment.length <= 200)
        return rate >= 1
    }

    private fun enableRateButton(value: Boolean) {
        if (value) {
            binding.btnRate.setBackgroundColor(resources.getColor(R.color.colorAccent2))
            binding.btnRate.isEnabled = true
        } else {
            binding.btnRate.setBackgroundColor(resources.getColor(R.color.colorLiteGrey))
            binding.btnRate.isEnabled = false
        }
    }
    //endregion

    companion object {
        const val MODE_INSTANT_RATE = 1
        const val MODE_DELAYED_RATE = 2
        const val MODE_HISTORY_RATE = 3

        private const val DATA_KEY = "rate_data_key"
        private const val MODE_KEY = "rate_mode_key"

        fun newInstance(data: Pair<Int, Any>): RatingFragment {
            val mode = data.first
            val args = Bundle()
            args.putInt(MODE_KEY, mode)
            if (mode == MODE_INSTANT_RATE) {
                args.putParcelable(DATA_KEY, data.second as NewOrderInfo)
            }
            if (mode == MODE_DELAYED_RATE || mode == MODE_HISTORY_RATE) {
                args.putParcelable(DATA_KEY, data.second as Trip)
            }
            val fragment = RatingFragment()
            fragment.arguments = args
            return fragment
        }
    }
}