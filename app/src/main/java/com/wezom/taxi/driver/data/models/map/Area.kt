package com.wezom.taxi.driver.data.models.map

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.Coordinate

data class Area(@SerializedName("id") var id: Int,
                @SerializedName("coordinates") var coordinates: List<Coordinate>,
                @SerializedName("coefficient") var coefficient: Double) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.createTypedArrayList(Coordinate.CREATOR),
            parcel.readDouble())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeTypedList(coordinates)
        parcel.writeDouble(coefficient)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Area> {
        override fun createFromParcel(parcel: Parcel): Area {
            return Area(parcel)
        }

        override fun newArray(size: Int): Array<Area?> {
            return arrayOfNulls(size)
        }
    }
}