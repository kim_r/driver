package com.wezom.taxi.driver.presentation.settings

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatDelegate
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxCompoundButton
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.common.loadRoundedImage
import com.wezom.taxi.driver.common.requestOverlayPermissions
import com.wezom.taxi.driver.databinding.SettingsBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.ext.setVisible
import com.wezom.taxi.driver.presentation.base.BaseFragment
import com.wezom.taxi.driver.presentation.settings.dialog.PickNavigatorDialog
import com.wezom.taxi.driver.presentation.settings.dialog.PickThemeDialog
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber

/**
 * Created by udovik.s on 13.03.2018.
 */
class SettingsFragment : BaseFragment() {

    private lateinit var binding: SettingsBinding
    private lateinit var viewModel: SettingsViewModel
    private lateinit var navigators: Array<String>
    private var selectedThemeId: Int = AppCompatDelegate.MODE_NIGHT_YES

    private var themeObserver = Observer<Int> { themeId ->
        selectedThemeId = themeId!!
        binding.tvThemeVal.text = setThemeTitle()
    }

    private var navigatorObserver = Observer<Int> { navigatorType ->
        binding.tvNavigatorVal.text = localePosToType(navigatorType!!)
    }

    private val userImageObserver = Observer<String> { uriString ->
        if (uriString.isNullOrBlank()) {
            setUserImage(defaultRes = R.drawable.ic_driver_vector_ava)
        } else {
            setUserImage(uriString)
        }
    }

    private fun setUserImage(source: String? = null, defaultRes: Int? = null) {
        binding.run {
            if (source.isNullOrBlank()) {
                imProfile.setImageResource(defaultRes!!)
            } else {
                if (source.startsWith("http")) {
                    loadRoundedImage(activity!!, imProfile, url = source)
                } else {
                    loadRoundedImage(activity!!, imProfile, uri = Uri.parse(source))
                }
            }
        }
    }


    private val vibroObserver = Observer<Boolean> { value ->
        binding.swVibroSignal.isChecked = value as Boolean
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setBackPressFun { viewModel.onBackPressed() }
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
        viewModel.themeLiveData.observe(this, themeObserver)
        viewModel.userImageLiveData.observe(this, userImageObserver)
        viewModel.navigatorLiveData.observe(this, navigatorObserver)
        viewModel.vibroLiveData.observe(this, vibroObserver)
        viewModel.overlayLiveData.observe(this, Observer { it ->
            Timber.d("overlayLiveData isChecked: $it")
            binding.swOverlaySignal.isChecked = it!!
        })
        viewModel.enableSystemOverlayLiveData.observe(this, Observer { enable ->
            if (enable!!) {
                requestOverlayPermissions(activity!!)
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = SettingsBinding.inflate(inflater, container, false)
        navigators = resources.getStringArray(R.array.navigators)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getTheme()
        viewModel.getNavigator()
        viewModel.getSound()
        viewModel.getVibro()
        viewModel.getOrderOverlayState()

        binding.run {
            toolbar.setToolbarTitle(getString(R.string.settings))

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                swVibroSignal.setVisible(false)

                val openSettingsListener = View.OnClickListener {
                    openSettings()
                }

//                tvSoundSignal.setOnClickListener(openSettingsListener)
                tvVibroSignal.setOnClickListener(openSettingsListener)
//                icSoundChevron.setOnClickListener(openSettingsListener)
                icVibroChevron.setOnClickListener(openSettingsListener)
            } else {
//                icSoundChevron.setVisible(false)
                icVibroChevron.setVisible(false)
            }

            RxView.clicks(signal).subscribe {
                viewModel.settingSignal()
            }
            disposable += RxView.clicks(profileContainer).subscribe {
                viewModel.onProfileClick()
            }

            disposable += RxCompoundButton.checkedChanges(swVibroSignal).subscribe { isEnabled ->
                viewModel.setVibroEnabled(isEnabled)
            }
            disposable += RxCompoundButton.checkedChanges(swOverlaySignal).subscribe { isEnabled ->
                Timber.d("overlay check: $isEnabled")
                viewModel.setOverlayEnabled(isEnabled)
            }

            disposable += RxView.clicks(themeContainer).subscribe {
                val checkedThemePos = if (selectedThemeId == AppCompatDelegate.MODE_NIGHT_NO) 1 else 0
                val dialog = PickThemeDialog.newInstance(checkedThemePos)
                dialog.listener = object : PickThemeDialog.ThemeClickListener {
                    override fun onThemeSelected(position: Int) {
                        if (position == PickThemeDialog.DARK) {
                            viewModel.setNewTheme(AppCompatDelegate.MODE_NIGHT_YES)
                        }
                        if (position == PickThemeDialog.LIGHT) {
                            viewModel.setNewTheme(AppCompatDelegate.MODE_NIGHT_NO)
                        }
                        dialog.dismiss()
                        activity?.recreate()
                        dialog.dismiss()
                    }
                }
                dialog.show(childFragmentManager, PickThemeDialog.TAG)
            }

            disposable += RxView.clicks(navigatorContainer).subscribe {
                var checkedNavPos = 0
                when (binding.tvNavigatorVal.text) {
                    navigators[PickNavigatorDialog.GOOGLE_NAV] -> checkedNavPos = PickNavigatorDialog.GOOGLE_NAV
                    navigators[PickNavigatorDialog.YANDEX_NAV] -> checkedNavPos = PickNavigatorDialog.YANDEX_NAV
                }
                val dialog = PickNavigatorDialog.newInstance(checkedNavPos)
                dialog.listener = object : PickNavigatorDialog.NavigatorClickListener {
                    override fun onNavigatorSelected(position: Int) {
                        viewModel.setNavigator(position)
                        dialog.dismiss()
                    }
                }
                dialog.show(childFragmentManager, PickNavigatorDialog.TAG)
            }

            disposable += RxView.clicks(exitContainer).subscribe {
                viewModel.logout()
            }
        }
        viewModel.getUserImage()
    }

    private fun setThemeTitle(): String {
        val themes = resources.getStringArray(R.array.themes)
        return if (selectedThemeId == AppCompatDelegate.MODE_NIGHT_NO) themes[1] else themes[0]
    }

    private fun localePosToType(position: Int): String {
        return if (position == PickNavigatorDialog.GOOGLE_NAV) navigators[PickNavigatorDialog.GOOGLE_NAV] else navigators[PickNavigatorDialog.YANDEX_NAV]
    }

    private fun openSettings() {
        val intent = Intent()
        intent.action = "android.settings.APP_NOTIFICATION_SETTINGS"
        intent.putExtra("android.provider.extra.APP_PACKAGE", context?.packageName)
        context?.startActivity(intent)
    }
}