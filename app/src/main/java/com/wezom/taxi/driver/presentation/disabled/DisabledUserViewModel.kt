package com.wezom.taxi.driver.presentation.disabled

import android.content.SharedPreferences
import com.wezom.taxi.driver.common.*
import com.wezom.taxi.driver.ext.*
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.service.LocationService
import com.wezom.taxi.driver.service.SocketService
import timber.log.Timber
import java.lang.ref.WeakReference
import javax.inject.Inject

/**
 * Created by zorin.a on 17.07.2018.
 */
class DisabledUserViewModel @Inject constructor(screenRouterManager: ScreenRouterManager,
                                                sharedPreferences: SharedPreferences) : BaseViewModel(screenRouterManager) {
    private var token: String by sharedPreferences.string(TOKEN)

    private var userImageString: String by sharedPreferences.string(USER_IMAGE_URI)
    private var carImageString: String by sharedPreferences.string(CAR_IMAGE_URI)
    private var userName: String by sharedPreferences.string(USER_NAME_CACHED)
    private var userRating: String by sharedPreferences.string(USER_RATING_CACHED)
    private var userPhone by sharedPreferences.string(USER_PHONE)
    private var userBalanceId by sharedPreferences.int(USER_BALANCE_ID)

    private var doc1 by sharedPreferences.string(FIRST_DOC_URI)
    private var doc2 by sharedPreferences.string(SECOND_DOC_URI)
    private var doc3 by sharedPreferences.string(THIRD_DOC_URI)
    private var doc4 by sharedPreferences.string(FOURTH_DOC_URI)

    private var isCardAdded: Boolean by sharedPreferences.boolean(IS_CARD_ADDED)

    fun switchAccount() {
        logOut()
    }

    private fun logOut() {
        val tokenCopy = "Bearer $token"
        Timber.d("LOGOUT logOut $tokenCopy")
        userImageString = ""
        carImageString = ""
        userName = ""
        userRating = ""
        userPhone = ""
        isCardAdded = false
        userBalanceId = 0
        doc1 = ""
        doc2 = ""
        doc3 = ""
        doc4 = ""
        token = ""
        setRootScreen(VERIFICATION_PHONE_SCREEN)
        LocationService.stop(context = WeakReference(context))
        SocketService.stop(context = WeakReference(context))
    }
}