package com.wezom.taxi.driver.presentation.base

import android.arch.lifecycle.Observer
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.support.v4.widget.DrawerLayout
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.data.models.ResponseState
import com.wezom.taxi.driver.injection.ViewModelFactory
import com.wezom.taxi.driver.presentation.base.dialog.MessengerChooser
import com.wezom.taxi.driver.presentation.customview.StateView
import com.wezom.taxi.driver.presentation.profile.dialog.InfoDialog
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable
import org.jetbrains.anko.find
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by zorin.a on 16.02.2018.
 */
abstract class BaseFragment : DaggerFragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    protected var disposable = CompositeDisposable()
    private var toolbar: Toolbar? = null
    private var refreshLayout: SwipeRefreshLayout? = null
    private var stateView: StateView? = null
    private var backPressFun: ((View) -> Unit)? = null
    private var onRefreshCallback: (() -> Unit)? = null

    protected var progressObserver = Observer<ResponseState> { state ->
        stateView?.setState(state!!)
    }

    override fun onSaveInstanceState(outState: Bundle) {

    }

    override fun onViewCreated(view: View,
                               savedInstanceState: Bundle?) {
        super.onViewCreated(view,
                            savedInstanceState)
        prepareToolbar(view)

        stateView = view.findViewById(R.id.state_view)
        refreshLayout = view.findViewById(R.id.swipe_layout)

        stateView?.let {
            it.retryListener = object : StateView.OnRetryListener {
                override fun onRetryClicked() {
                    onRefreshCallback?.invoke()
                }
            }
        }

        refreshLayout?.let {
            it.setColorSchemeColors(Color.RED,
                                    Color.GREEN,
                                    Color.BLUE,
                                    Color.CYAN)
            stateView?.setRefreshLayout(it)
            it.setOnRefreshListener {
                onRefreshCallback?.invoke()
            }
        }
    }

    private fun prepareToolbar(view: View) {
        toolbar = view.findViewById(R.id.toolbar)
        if (toolbar != null) {
            (activity as AppCompatActivity).setSupportActionBar(toolbar)
            (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)

            val drawerToggle = getDrawerToggle()

            val entryCount = fragmentManager!!.backStackEntryCount
            if (entryCount > 0) {
                enableBackAction(true,
                                 drawerToggle)
            } else {
                enableBackAction(false,
                                 drawerToggle)
            }
        }
    }

    protected fun getDrawerToggle(): ActionBarDrawerToggle {
        val drawerLayout: DrawerLayout = activity!!.find(R.id.drawer_layout)
        val drawerToggle = ActionBarDrawerToggle(activity,
                                                 drawerLayout,
                                                 toolbar,
                                                 R.string.open_drawer,
                                                 R.string.close_drawer)
        drawerLayout.addDrawerListener(drawerToggle)
        return drawerToggle
    }

    protected fun enableBackAction(enable: Boolean,
                                   drawerToggle: ActionBarDrawerToggle) {
        if (enable) {
            drawerToggle.isDrawerIndicatorEnabled = false
            toolbar!!.setNavigationOnClickListener(backPressFun)

        } else {
            drawerToggle.isDrawerIndicatorEnabled = true
            drawerToggle.syncState()
        }
    }

    private fun createDialog(title: String? = "",
                             message: String,
                             listener: DialogInterface.OnClickListener? = null): AlertDialog =
            if (listener == null) {
                AlertDialog.Builder(context!!)
                    .setTitle(title)
                    .setMessage(message)
                    .setPositiveButton(R.string.ok) { _, _ ->
                        DialogInterface.OnClickListener { dialog, which -> dialog?.dismiss() }
                    }
                    .create()
            } else AlertDialog.Builder(context!!).setTitle(title).setMessage(message).setPositiveButton(R.string.ok,
                                                                                                        listener).create()


    fun showErrorDialog(title: String? = null,
                        error: String,
                        listener: DialogInterface.OnClickListener? = null) {
        val dialog = createDialog(title ?: getString(R.string.error),
                                  error,
                                  listener)
        dialog.show()
    }

    fun showInfoDialog(title: Int,
                       text: Int) {
        val dialog = InfoDialog.newInstance(title,
                                            text)
        dialog.listener = object : InfoDialog.DialogClickListener {
            override fun onPositiveClick() {
                dialog.dismiss()
            }

            override fun onNegativeClick() {
            }
        }
        dialog.show(childFragmentManager,
                    InfoDialog.TAG)
    }

    fun showInfoDialog(image: Int,
                       title: Int,
                       text: Int) {
        val dialog = InfoDialog.newInstance(image,
                                            title,
                                            text)
        dialog.listener = object : InfoDialog.DialogClickListener {
            override fun onPositiveClick() {
                dialog.dismiss()
            }

            override fun onNegativeClick() {
            }
        }
        dialog.show(childFragmentManager,
                    InfoDialog.TAG)
    }

    fun showDecisionDialog(title: Int,
                           text: Int,
                           isNegativeButtonEnabled: Boolean = false): InfoDialog {
        val dialog = InfoDialog.newInstance(title,
                                            text,
                                            isNegativeButtonEnabled)
        dialog.show(childFragmentManager,
                    InfoDialog.TAG)
        return dialog
    }

    fun dialUser(phone: String) {
        startActivity(Intent(Intent.ACTION_CALL).setData(Uri.parse("tel:$phone")))
    }

    fun sendSms(phone: String) {
        val intent = Intent(Intent.ACTION_SENDTO).setData(Uri.parse("smsto:$phone"))
        if (intent.resolveActivity(activity?.packageManager) != null) {
            startActivity(intent)
        }
    }

    fun openViber(phone: String) {
        val intent = Intent(Intent.ACTION_VIEW).apply {
            `package` = "com.viber.voip"
            data = Uri.parse("tel:$phone")
        }
        startActivity(intent)
    }

    fun isViberInstalled(): Boolean {
        val pm = context!!.packageManager
        try {
            pm.getPackageInfo("com.viber.voip",
                              PackageManager.GET_ACTIVITIES)
            return true
        } catch (e: PackageManager.NameNotFoundException) {
        }

        return false
    }

    fun openMessengerChooserDialog(phone: String) {
        val dialog = MessengerChooser()
        dialog.show(childFragmentManager,
                    MessengerChooser.TAG)
        dialog.listener = object : MessengerChooser.ChoiceDialogSelection {
            override fun onSelected(type: MessengerChooser.MessengerType) {
                if (type == MessengerChooser.MessengerType.SMS) {
                    sendSms(phone)
                } else {
                    openViber(phone)
                }
                dialog.dismiss()
            }
        }
    }


    override fun onDestroy() {
        disposable.dispose()
        super.onDestroy()
    }

    protected fun setBackPressFun(callback: (View) -> Unit) {
        backPressFun = callback
    }

    protected fun setOnRefreshCallback(callback: () -> Unit) {
        onRefreshCallback = callback
    }

    fun lockDrawer(isLock: Boolean) {
        Timber.d("isLock: $isLock")
        (activity as BaseActivity).lockDrawer(isLock)
    }

    fun openAppSettings() {
        val intent = Intent()
        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        val uri = Uri.fromParts("package",
                                context?.packageName,
                                null)
        intent.data = uri
        startActivity(intent)
    }
}
