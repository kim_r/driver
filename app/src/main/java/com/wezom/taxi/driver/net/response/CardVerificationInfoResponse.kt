package com.wezom.taxi.driver.net.response

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.PaymentInfo


data class CardVerificationInfoResponse(@SerializedName("paymentData") val paymentInfo: PaymentInfo) : BaseResponse(), Parcelable {
    constructor(parcel: Parcel) : this(parcel.readParcelable<PaymentInfo>(PaymentInfo::class.java.classLoader))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(paymentInfo, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CardVerificationInfoResponse> {
        override fun createFromParcel(parcel: Parcel): CardVerificationInfoResponse {
            return CardVerificationInfoResponse(parcel)
        }

        override fun newArray(size: Int): Array<CardVerificationInfoResponse?> {
            return arrayOfNulls(size)
        }
    }
}