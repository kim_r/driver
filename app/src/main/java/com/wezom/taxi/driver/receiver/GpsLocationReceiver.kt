package com.wezom.taxi.driver.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.bus.events.GpsEvent
import com.wezom.taxi.driver.common.checkGpsEnabled

class GpsLocationReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action!!.matches(LocationManager.PROVIDERS_CHANGED_ACTION.toRegex())) {
            if (!checkGpsEnabled(context)) {
                RxBus.publish(GpsEvent(false))
            } else {
                RxBus.publish(GpsEvent(true))
            }
        }
    }
}