package com.wezom.taxi.driver.presentation.refuseorder

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxTextView
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.bus.events.ResultDataEvent
import com.wezom.taxi.driver.common.loadRoundedImage
import com.wezom.taxi.driver.data.models.Order
import com.wezom.taxi.driver.data.models.OrderStatus
import com.wezom.taxi.driver.data.models.Reason
import com.wezom.taxi.driver.databinding.RefuseOrderBinding
import com.wezom.taxi.driver.presentation.base.BaseFragment
import com.wezom.taxi.driver.presentation.refuseorder.dialog.ReasonDialog
import io.reactivex.rxkotlin.Observables
import io.reactivex.rxkotlin.plusAssign
import java.util.*
import android.text.InputFilter
import javax.inject.Inject
import com.wezom.taxi.driver.common.swap
import com.wezom.taxi.driver.ext.*
import com.wezom.taxi.driver.presentation.executeorder.dialog.BlockingClientDialog


/**
 * Created by udovik.s on 26.03.2018.
 */
class RefuseOrderFragment : BaseFragment() {
    //region var
    private lateinit var binding: RefuseOrderBinding
    private lateinit var viewModel: RefuseOrderViewModel

    @Inject
    lateinit var sharedPreferences: SharedPreferences

    private lateinit var order: Order
    private var checkedReasonPos = 0
    private var refuseReasons: MutableList<Reason>? = null
    private var data = ArrayList<Reason>()
    private var allReasons: Map<OrderStatus, List<Reason>>? = null
    private var taxometerData: ResultDataEvent? = null
    private var tempReason: Reason? = null
    private var chosenReason: Reason? = null
    //endregion



    //region override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setBackPressFun { viewModel.onBackPressed() }

        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
//        viewModel.getRefuseReasons()
        // data = viewModel.getReason() as ArrayList<Reason>
        viewModel.refuseReasonsLiveData.observe(this, Observer<List<Reason>> { reasons ->

            refuseReasons = reasons?.toMutableList()
            reasons?.find { it.id == 7 }?.let { refuseReasons?.swap(0, reasons.indexOf(it)) }

            for (reason in refuseReasons!!) {
                if (reason.timeToShowAfter == null) {
//                    if (!data.contains(reason.name.toString()))
                    data.add(reason)
                } else {
                    tempReason = reason
                }
            }
        })

        arguments?.let {
            order = it.getParcelable(ORDER_KEY)
            allReasons = it.getSerializable(ALL_REASONS_KEY) as Map<OrderStatus, List<Reason>>
            it.getParcelable<ResultDataEvent>(RESULT_DATA)?.let {
                taxometerData = it
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = RefuseOrderBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateRefuseReasons()
        updateUi(order)
        binding.run {

            toolbar.setToolbarTitle(getString(R.string.cancel_1))

            val reasonObservable = RxTextView.textChanges(editChooseReason)
            val commentObservable = RxTextView.textChanges(editAddComment)
            if (Locale.getDefault().language == "ru") {
                editAddComment.inputType = InputType.TYPE_CLASS_TEXT
                editAddComment.filters = arrayOf(InputFilter { src, start, end, s, dstart, dend ->
                    var c = ""
                    for (item in src) {
                        if (item.toString().matches("[а-яА-Я ,.():;]+".toRegex())) {
                            c += item
                        }
                    }
                    c
                })
//                editAddComment.keyListener = DigitsKeyListener.getInstance("АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЬьЭэЮюЯя0123456789.,:;'()")
            } else {
                editAddComment.inputType = InputType.TYPE_CLASS_TEXT
                editAddComment.filters = arrayOf(InputFilter { src, _, _, _, _, _ ->
                    var c = ""
                    for (item in src) {
                        if (item.toString().matches("[а-яА-Я ,.():;]+".toRegex())) {
                            c += item
                        }
                    }
                    c
                })
//                editAddComment.keyListener = DigitsKeyListener.getInstance("АаБбВвГгҐґДдЕеЄєЖжЗзИиІіЇїЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЬьЮюЯя0123456789.,:;'()")
            }

            editChooseReason.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    showAddCommentField(s.toString())
                }

                override fun afterTextChanged(s: Editable) {
                }
            })

            val isButtonEnabled =
                    Observables.combineLatest(reasonObservable, commentObservable) { reason, comment ->
                        isValuesValid(reason.toString(), comment.toString())
                    }

            disposable += isButtonEnabled.subscribe { value ->
                enableAcceptButton(value)
            }

            disposable += RxView.clicks(btnAccept).subscribe {
                if (layoutAddComment.visibility == View.VISIBLE && editAddComment.text.isEmpty()) {
                    getString(R.string.add_comment_please).shortToast(context)
                } else {
                    if (order.status!! != OrderStatus.ACCEPTED
                            && (selectReason == 19 || selectReason == 12 || selectReason == 14)) {
                        showDialogBlockingClient(editAddComment.text.toString())
                    } else {
                        var comment: String = editAddComment.text.toString()

                        viewModel.refuseOrder(id = order.id!!,
                                reasonId = chosenReason?.id,
                                comment = if (comment.length > 2) comment else null,
                                status = order.status?.value,
                                factCost = taxometerData?.factCost,
                                actualCost = taxometerData?.resultCost,
                                factTime = taxometerData?.factTime ?: 1,
                                factPath = taxometerData?.factPath,
                                eventTime = System.currentTimeMillis(),
                                route = taxometerData?.passedRoute)
                    }
                }
            }

            disposable += RxView.clicks(editChooseReason).subscribe {
                if (tempReason != null) {
                    if (tempReason!!.timeToShowAfter != null &&
                            tempReason!!.timeToShowAfter!! <= (sharedPreferences.getInt(TIMER_NOT_ENTER_CLIENT, 0) * 1000)
                            && !containsReason) {
                        containsReason = data.add(tempReason!!)
                    }
                }

                val dialog = ReasonDialog.newInstance(data, checkedReasonPos)
                dialog.show(childFragmentManager, ReasonDialog.TAG)
                dialog.listener = object : ReasonDialog.ChoiceDialogSelection {
                    override fun onSelected(position: Int, reason: Reason) {
                        checkedReasonPos = position
                        chosenReason = reason
                        binding.editChooseReason.setText(chosenReason?.name)
                        selectReason = chosenReason?.id!!
                    }
                }
            }
        }
    }

    private var containsReason: Boolean = false
    private var selectReason: Int = 0

    private fun showDialogBlockingClient(comment: String) {
        val dialog = BlockingClientDialog()
        dialog.listener = object : BlockingClientDialog.DialogClickListener {
            override fun onPositiveClick() {
                dialog.dismiss()
                getString(R.string.order_canceled).shortToast(context)

                viewModel.refuseOrder(id = order.id!!,
                        reasonId = chosenReason?.id,
                        comment = if (comment.length > 2) comment else null,
                        status = order.status?.value,
                        factCost = taxometerData?.factCost,
                        actualCost = taxometerData?.resultCost,
                        factTime = taxometerData?.factTime ?: 1,
                        factPath = taxometerData?.factPath,
                        eventTime = System.currentTimeMillis(),
                        route = taxometerData?.passedRoute)
            }

            override fun onNegativeClick() {
                dialog.dismiss()
            }
        }
        dialog.show(fragmentManager, BlockingClientDialog.TAG)
    }

    private fun updateRefuseReasons() {
        allReasons?.let {
            viewModel.getRefuseReasons(order.status!!, it)
        }
    }
    //endregion

    //region fun
    fun updateUi(currentOrder: Order) {
        binding.run {
            tvFrom.text = currentOrder.coordinates!![0].name
            tvName.text = currentOrder.user?.name
            if (currentOrder.coordinates?.size!! > 1) {
                tvTo.text = currentOrder.coordinates!![currentOrder.coordinates!!.size - 1].name
            } else {
                tvTo.text = getString(R.string.around_town)
            }

            val ratingVal = currentOrder.user?.rating
            ratingVal?.let {
                tvRate.text = it.toString()
            }

            if (ratingVal == 0.0f || ratingVal == null) {
                ratingContainer.setVisible(false)
                ratingContainer.setVisible(false)
            }
            val photo = currentOrder.user?.photo
            if (!photo.isNullOrBlank()) {
                loadRoundedImage(context!!, imAvatar, url = currentOrder.user?.photo)
            }
        }
    }

    private fun isValuesValid(reason: String, comment: String): Boolean {
        if (binding.layoutAddComment.visibility == View.VISIBLE) {
            return reason.isNotEmpty() && comment.isNotEmpty() && comment.length >= 10 && comment.length <= 200
        }
        return reason.isNotBlank() && reason.isNotEmpty()
    }

    private fun showAddCommentField(reason: String) {
        binding.run {
            for (refuseReason: Reason in refuseReasons!!) {
                if (refuseReason.name.equals(reason)) {
                    if (refuseReason.isRequiredMessage!!) {
                        layoutAddComment.setVisible(true)
                        editAddComment.clearFocus()
                        editAddComment.text.clear()
                    } else {
                        layoutAddComment.setVisible(false)
                        editAddComment.clearFocus()
                        editAddComment.text.clear()
                    }
                }
            }
        }
    }

    private fun enableAcceptButton(value: Boolean) {
        if (value) {
            binding.btnAccept.setBackgroundColor(resources.getColor(R.color.colorAccent2))
            binding.btnAccept.isEnabled = true
        } else {
            binding.btnAccept.setBackgroundColor(resources.getColor(R.color.colorLiteGrey))
            binding.btnAccept.isEnabled = false
        }
    }
    //endregion

    companion object {
        const val ORDER_KEY = "trip_key"
        const val ALL_REASONS_KEY = "all_reasons_key"
        const val RESULT_DATA = "result_data"
        fun newInstance(data: Triple<Order, HashMap<OrderStatus, List<Reason>>, ResultDataEvent>): RefuseOrderFragment {
            val order = data.first
            val allReasons = data.second
            val resultData = data.third

            val args = Bundle()
            args.putParcelable(ORDER_KEY, order)
            args.putSerializable(ALL_REASONS_KEY, allReasons)
            args.putParcelable(RESULT_DATA, resultData)
            val fragment = RefuseOrderFragment()
            fragment.arguments = args
            return fragment
        }
    }
}