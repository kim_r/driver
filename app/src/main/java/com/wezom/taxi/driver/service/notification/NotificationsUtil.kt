package com.wezom.taxi.driver.service.notification

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import android.preference.PreferenceManager
import android.support.annotation.RawRes
import android.support.annotation.StringRes
import android.support.v4.app.NotificationCompat
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.ext.SOUND_ENABLED
import com.wezom.taxi.driver.ext.VIBRO_ENABLED
import com.wezom.taxi.driver.presentation.main.MainActivity
import com.wezom.taxi.driver.service.AutoCloseOrderService
import com.wezom.taxi.driver.service.MessagingService
import com.wezom.taxi.driver.service.TaxometerService
import timber.log.Timber
import java.lang.ref.WeakReference
import java.util.*
import javax.inject.Inject

class NotificationsUtil @Inject constructor(private val context: Context) {

    private var manager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    private val prefs = PreferenceManager.getDefaultSharedPreferences(context)

    init {
        val isSoundEnabled = prefs.getBoolean(SOUND_ENABLED, true)
        val isVibroEnabled = prefs.getBoolean(VIBRO_ENABLED, true)

        createNotificationChannels(
                context,
                manager,
                context.packageName,
                isVibroEnabled,
                isSoundEnabled
        )
    }

    fun makeNotification(
            pushType: NotificationType,
            title: String,
            sound: String,
            description: String,
            data: Map<String, String>
    ): Notification {
        val isSoundEnabled = prefs.getBoolean(SOUND_ENABLED, true)
        val isVibrationEnabled = prefs.getBoolean(VIBRO_ENABLED, true)

        return when (pushType) {
            NotificationType.ADDED_NEW_DISCOUNT -> {
                makeCommonNotification(
                        ADDED_DISCOUNT_PUSH_CHANNEL_ID,
                        title,
                        description,
                        if (isSoundEnabled && sound != "default") pushType.sound else null,
                        isVibrationEnabled,
                        data
                )
            }

            NotificationType.CANCEL_ORDER -> {

                WeakReference(context).get()!!.stopService(Intent(WeakReference(context).get(), TaxometerService::class.java))
                WeakReference(context).get()!!.stopService(Intent(WeakReference(context).get(), AutoCloseOrderService::class.java))

                makeCommonNotification(
                        CANCEL_ORDER_PUSH_CHANNEL_ID,
                        title,
                        description,
                        if (isSoundEnabled && sound != "default") pushType.sound else null,
                        isVibrationEnabled,
                        data
                )
            }

            NotificationType.CUSTOM_PUSH -> {
                makeCommonNotification(
                        CUSTOM_PUSH_CHANNEL_ID,
                        title,
                        description,
                        if (isSoundEnabled && sound != "default") pushType.sound else null,
                        isVibrationEnabled,
                        data
                )
            }

            NotificationType.DOWNLOADING_MISSING_PHOTO -> {
                makeCommonNotification(
                        DOWNLOADING_MISSING_PHOTO_PUSH_CHANNEL_ID,
                        title,
                        description,
                        if (isSoundEnabled && sound != "default") pushType.sound else null,
                        isVibrationEnabled,
                        data
                )
            }

            NotificationType.DRIVER_BLOCKED -> {
                makeCommonNotification(
                        DRIVER_BLOCKED_PUSH_CHANNEL_ID,
                        title,
                        description,
                        if (isSoundEnabled && sound != "default") pushType.sound else null,
                        isVibrationEnabled,
                        data
                )
            }

            NotificationType.INVALID_REGISTRATION_DATA -> {
                makeCommonNotification(
                        INVALID_REG_DATA_PUSH_CHANNEL_ID,
                        title,
                        description,
                        if (isSoundEnabled && sound != "default") pushType.sound else null,
                        isVibrationEnabled,
                        data
                )
            }

            NotificationType.NEW_ORDER_AVAILABLE -> {
                makeCommonNotification(
                        NEW_ORDER_PUSH_CHANNEL_ID,
                        title,
                        description,
                        if (isSoundEnabled && sound != "default") pushType.sound else null,
                        isVibrationEnabled,
                        data
                )
            }

            NotificationType.SUCCESSFUL_MODERATION -> {
                makeCommonNotification(
                        SUCCESS_MODERATION_PUSH_CHANNEL_ID,
                        title,
                        description,
                        if (isSoundEnabled && sound != "default") pushType.sound else null,
                        isVibrationEnabled,
                        data
                )
            }

            NotificationType.UNLOCK -> {
                makeCommonNotification(
                        UNLOCK_CUSTOM_PUSH_CHANNEL_ID,
                        title,
                        description,
                        if (isSoundEnabled && sound != "default") pushType.sound else null,
                        isVibrationEnabled,
                        data
                )
            }
            NotificationType.LIMITED -> {
                makeCommonNotification(
                        LIMITED_ID,
                        title,
                        description,
                        if (isSoundEnabled && sound != "default") pushType.sound else null,
                        isVibrationEnabled,
                        data
                )
            }
        }
    }

    fun show(notification: Notification) =
            manager.notify(Random().nextInt(Int.MAX_VALUE), notification)

    private fun makeCommonNotification(
            channelId: String,
            title: String?,
            description: String?,
            @RawRes sound: Int? = null,
            vibrate: Boolean,
            data: Map<String, String>
    ): Notification {
        val builder = NotificationCompat.Builder(context, channelId)
                .setContentTitle(title)
                .setContentText(description)
                .setContentIntent(getMainActivityPendingIntent(data))
                .setSmallIcon(R.drawable.ic_notification)
                .setVibrate(if (vibrate) VIBRATION_PATTERN else longArrayOf(0))
                .setAutoCancel(true)

        if (sound != null)
            builder.setSound(Uri.parse("android.resource://${context.packageName}/$sound"))



        return builder.build()
    }

    private fun getMainActivityPendingIntent(data: Map<String, String>?): PendingIntent {
        val typeId = data!![MessagingService.TYPE_ID].toString().toInt()
        return when (typeId) {
            NotificationType.CUSTOM_PUSH.typeId, NotificationType.SUCCESSFUL_MODERATION.typeId, NotificationType.INVALID_REGISTRATION_DATA.typeId ->
                PendingIntent.getActivity(context, 0, Intent(), PendingIntent.FLAG_UPDATE_CURRENT)
            else -> {
                val intent = Intent(context, MainActivity::class.java)
                for (el in data) {
                    intent.putExtra(el.key, el.value)
                }
                val requestID = System.currentTimeMillis().toInt()
                PendingIntent.getActivity(context, requestID, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            }
        }
    }


    companion object {
        val VIBRATION_PATTERN = longArrayOf(0, 300, 0, 300)

        private const val CUSTOM_PUSH_CHANNEL_ID = "com.wezom.taxi.commonchannel"
        private const val CANCEL_ORDER_PUSH_CHANNEL_ID = "com.wezom.taxi.cancelorderpush"
        private const val ADDED_DISCOUNT_PUSH_CHANNEL_ID = "com.wezom.taxi.addednewdiscountpush"
        private const val DOWNLOADING_MISSING_PHOTO_PUSH_CHANNEL_ID = "com.wezom.taxi.downloadingmissingphotopush"
        private const val DRIVER_BLOCKED_PUSH_CHANNEL_ID = "com.wezom.taxi.driverblockedpush"
        private const val INVALID_REG_DATA_PUSH_CHANNEL_ID = "com.wezom.taxi.invalidregisrationpush"
        private const val NEW_ORDER_PUSH_CHANNEL_ID = "com.wezom.taxi.neworderpush"
        private const val SUCCESS_MODERATION_PUSH_CHANNEL_ID = "com.wezom.taxi.successmoderationpush"
        private const val UNLOCK_CUSTOM_PUSH_CHANNEL_ID = "com.wezom.taxi.unlockpush"
        private const val LIMITED_ID = "com.wezom.taxi.lmitedpush"

        private val NOTIFICATION_TYPES = mapOf(
                CUSTOM_PUSH_CHANNEL_ID to R.string.custom_push,
                CANCEL_ORDER_PUSH_CHANNEL_ID to R.string.cancel_order_push,
                ADDED_DISCOUNT_PUSH_CHANNEL_ID to R.string.added_new_discount_push,
                DOWNLOADING_MISSING_PHOTO_PUSH_CHANNEL_ID to R.string.downloading_missing_photo,
                DRIVER_BLOCKED_PUSH_CHANNEL_ID to R.string.driver_blocked,
                INVALID_REG_DATA_PUSH_CHANNEL_ID to R.string.invalid_registration_data,
                NEW_ORDER_PUSH_CHANNEL_ID to R.string.new_order_available,
                SUCCESS_MODERATION_PUSH_CHANNEL_ID to R.string.success_moderation,
                UNLOCK_CUSTOM_PUSH_CHANNEL_ID to R.string.unlock,
                LIMITED_ID to R.string.driver_limited
        )

        private fun createNotificationChannel(
                context: Context,
                notificationManager: NotificationManager,
                packageName: String,
                channelId: String,
                @StringRes channelName: Int,
                @RawRes sound: Int? = null,
                isVibroEnabled: Boolean,
                isSoundEnabled: Boolean
        ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel = NotificationChannel(
                        channelId,
                        context.getString(channelName),
                        NotificationManager.IMPORTANCE_DEFAULT
                )
                Timber.d("createNotificationChannel: id: $channelId, " +
                        "name: ${context.getString(channelName)}, " +
                        "sound: ${"android.resource://$packageName/$sound"}")

                channel.enableVibration(true)

                if (sound != null)
                    channel.setSound(
                            Uri.parse("android.resource://$packageName/$sound"),
                            AudioAttributes.Builder()
                                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                                    .build()
                    )
                channel.vibrationPattern = NotificationsUtil.VIBRATION_PATTERN

                notificationManager.createNotificationChannel(channel)
            }
        }

        private fun recreateNotificationChannel(
                context: Context,
                notificationManager: NotificationManager,
                packageName: String,
                channelId: String,
                @StringRes channelName: Int,
                @RawRes sound: Int? = null,
                isVibroEnabled: Boolean,
                isSoundEnabled: Boolean
        ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notificationManager.deleteNotificationChannel(channelId)
                createNotificationChannel(
                        context,
                        notificationManager,
                        packageName,
                        channelId,
                        channelName,
                        sound,
                        isVibroEnabled,
                        isSoundEnabled
                )
            }
        }

        fun createNotificationChannels(
                context: Context,
                notificationManager: NotificationManager,
                packageName: String,
                isVibrationEnabled: Boolean,
                isSoundEnabled: Boolean
        ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                for (notificationType in NOTIFICATION_TYPES) {
                    val sound = when (notificationType.key) {
                        CUSTOM_PUSH_CHANNEL_ID -> R.raw.custom_sound
                        CANCEL_ORDER_PUSH_CHANNEL_ID -> R.raw.custom_sound
                        ADDED_DISCOUNT_PUSH_CHANNEL_ID -> R.raw.custom_sound
                        DOWNLOADING_MISSING_PHOTO_PUSH_CHANNEL_ID -> R.raw.custom_sound
                        DRIVER_BLOCKED_PUSH_CHANNEL_ID -> R.raw.custom_sound
                        INVALID_REG_DATA_PUSH_CHANNEL_ID -> R.raw.custom_sound
                        NEW_ORDER_PUSH_CHANNEL_ID -> R.raw.new_order_sound
                        SUCCESS_MODERATION_PUSH_CHANNEL_ID -> R.raw.custom_sound
                        UNLOCK_CUSTOM_PUSH_CHANNEL_ID -> R.raw.custom_sound
                        LIMITED_ID -> R.raw.custom_sound
                        else -> null
                    }

                    recreateNotificationChannel(
                            context,
                            notificationManager, packageName, notificationType.key,
                            notificationType.value,
                            sound,
                            isVibrationEnabled,
                            isSoundEnabled
                    )
                }
            }
        }
    }
}