package com.wezom.taxi.driver.data.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by zorin.a on 28.04.2018.
 */
data class AcceptedOrderShowing(
        @SerializedName("isPreliminaryTimeShow") var isPreliminaryTimeShow: Boolean?,
        @SerializedName("isCurrentCoefficientShow") var isCurrentCoefficientShow: Boolean?,
        @SerializedName("isCommentShow") var isCommentShow: Boolean?,
        @SerializedName("isAddressShow") var isAddressShow: Boolean?,
        @SerializedName("isServiceShow") var isServiceShow: Boolean?,
        @SerializedName("isMethodShow") var isMethodShow: Boolean?,
        @SerializedName("isSurchargeShow") var isSurchargeShow: Boolean?,
        @SerializedName("isEstimatedCostShow") var isEstimatedCostShow: Boolean?) : Parcelable {

    constructor() : this(false, false, false, false, false, false, false, false)
    constructor(source: Parcel) : this(
            source.readValue(Boolean::class.java.classLoader) as Boolean?,
            source.readValue(Boolean::class.java.classLoader) as Boolean?,
            source.readValue(Boolean::class.java.classLoader) as Boolean?,
            source.readValue(Boolean::class.java.classLoader) as Boolean?,
            source.readValue(Boolean::class.java.classLoader) as Boolean?,
            source.readValue(Boolean::class.java.classLoader) as Boolean?,
            source.readValue(Boolean::class.java.classLoader) as Boolean?,
            source.readValue(Boolean::class.java.classLoader) as Boolean?
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeValue(isPreliminaryTimeShow)
        writeValue(isCurrentCoefficientShow)
        writeValue(isCommentShow)
        writeValue(isAddressShow)
        writeValue(isServiceShow)
        writeValue(isMethodShow)
        writeValue(isSurchargeShow)
        writeValue(isEstimatedCostShow)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<AcceptedOrderShowing> = object : Parcelable.Creator<AcceptedOrderShowing> {
            override fun createFromParcel(source: Parcel): AcceptedOrderShowing = AcceptedOrderShowing(source)
            override fun newArray(size: Int): Array<AcceptedOrderShowing?> = arrayOfNulls(size)
        }
    }
}