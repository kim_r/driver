package com.wezom.taxi.driver.data.db

import android.arch.persistence.room.*
import com.wezom.taxi.driver.data.models.OrderCoordinates
import io.reactivex.Single


@Dao
interface OrderCoordinatesDao {
    @Query("SELECT * FROM  'order_coordinates'")
    fun queryAll(): Single<List<OrderCoordinates>>

    @Query("SELECT * FROM 'order_coordinates' WHERE id = :id")
    fun queryOrderCoordinates(id: Int): OrderCoordinates

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateOrderCoordinates(orderInfo: OrderCoordinates)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrderCoordinates(orderInfoList: List<OrderCoordinates>): List<Long>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrderCoordinates(orderInfo: OrderCoordinates): Long

    @Delete
    fun delete(orderInfo: OrderCoordinates)

    @Delete
    fun delete(listOfOrderInfo: List<OrderCoordinates>): Int

    @Query("DELETE FROM 'order_coordinates' WHERE id = :id")
    fun deleteOrderCoordinatesById(id: Long)

    @Query("DELETE FROM 'order_coordinates'")
    fun clearCoordinates()

}