package com.wezom.taxi.driver.presentation.main.events

class DisableOnlineStatusEvent(var online: Boolean)