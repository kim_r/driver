package com.wezom.taxi.driver.presentation.photoviewer

import android.arch.lifecycle.ViewModelProviders
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import com.isseiaoki.simplecropview.CropImageView
import com.jakewharton.rxbinding2.view.RxView
import com.wezom.taxi.driver.common.Constants.TEMP_BITMAP
import com.wezom.taxi.driver.common.storeBitmapToDisk
import com.wezom.taxi.driver.databinding.PhotoViewerBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.injection.module.GlideApp
import com.wezom.taxi.driver.presentation.base.BaseFragment
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber

/**
 * Created by zorin.a on 03.03.2018.
 */
class PhotoViewerFragment : BaseFragment() {
    private lateinit var viewModel: PhotoViewModel
    private lateinit var binding: PhotoViewerBinding
    private lateinit var imageUri: Uri
    private lateinit var cropMode: CropOptions
    private var photoId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
        arguments.let {
            val p = it!!.getSerializable(ARGS_KEY) as Triple<String, CropOptions, Int>
            imageUri = Uri.parse(p.first)
            cropMode = p.second
            photoId = p.third
        }
        setBackPressFun { viewModel.onBackPressed() }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = PhotoViewerBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.run {
            disposable += RxView.clicks(save).subscribe {
                if (cropper.croppedBitmap != null) {
                    val uri = storeBitmapToDisk(context!!, cropper.croppedBitmap, TEMP_BITMAP)
                    viewModel.backWithResult(photoId, uri, photoId)
                }
            }
            cropper.setAnimationDuration(500)
            cropper.setGuideShowMode(CropImageView.ShowMode.NOT_SHOW)
            cropper.setAnimationEnabled(true)
            cropper.setInitialFrameScale(0.8f)
            cropper.setInterpolator(AccelerateDecelerateInterpolator())
            cropper.setHandleShowMode(CropImageView.ShowMode.SHOW_ALWAYS)
            cropper.setOutputMaxSize(800, 800)
            cropper.setCompressFormat(Bitmap.CompressFormat.JPEG)
            cropper.setCompressQuality(100)


            when (cropMode) {
                CropOptions.CROP_SQUARE -> {
                    cropper.setCropMode(CropImageView.CropMode.SQUARE)

                }
                CropOptions.CROP_ROUNDED -> {
                    cropper.setCropMode(CropImageView.CropMode.CIRCLE_SQUARE)
                }
                CropOptions.FREE -> {
                    cropper.setCropMode(CropImageView.CropMode.FREE)
                }
                CropOptions.RATIO_16_9 -> {
                    cropper.setCropMode(CropImageView.CropMode.RATIO_16_9)
                }
                CropOptions.RATIO_16_12 -> {
                    cropper.setCropMode(CropImageView.CropMode.CUSTOM)
                    cropper.setCustomRatio(16, 12)
                }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        Timber.d("onSaveInstanceState")
        binding.cropper.imageBitmap = null
    }

    override fun onResume() {
        super.onResume()
        GlideApp.with(context).load(imageUri).into(binding.cropper)
    }

    companion object {
        const val ARGS_KEY = "image_uri_key"

        enum class CropOptions { CROP_SQUARE, CROP_ROUNDED, FREE, RATIO_16_9, RATIO_16_12 }

        fun newInstance(arg: Triple<String, CropOptions, Int>): PhotoViewerFragment {
            val args = Bundle()
            args.putSerializable(ARGS_KEY, arg)
            val fragment = PhotoViewerFragment()
            fragment.arguments = args
            return fragment
        }
    }
}