package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.DriverStatus
import com.wezom.taxi.driver.data.models.ModerationStatus
import com.wezom.taxi.driver.data.models.User

/**
 * Created by zorin.a on 021 21.02.18.
 */
data class ConfirmCodeResponse(@SerializedName("authorizationToken") val token: String,
                               @SerializedName("moderationStatus") val moderationStatus: ModerationStatus,
                               @SerializedName("driverStatus") val driverStatus: DriverStatus,
                               @SerializedName("accessToWorkWithoutModeration") val accessToWorkWithoutModeration: Boolean,
                               @SerializedName("user") val user: User?) : BaseResponse()