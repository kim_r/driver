package com.wezom.taxi.driver.presentation.balance.list

import android.view.ViewGroup
import com.wezom.taxi.driver.data.models.Income
import com.wezom.taxi.driver.presentation.base.lists.BaseAdapter
import com.wezom.taxi.driver.presentation.base.lists.BaseViewHolder
import javax.inject.Inject

/**
 * Created by andre on 22.03.2018.
 */
class BalansesAdapter @Inject constructor() : BaseAdapter<Income, BalancesItemView, BaseViewHolder<BalancesItemView>>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<BalancesItemView> {
        val itemView = BalancesItemView(parent.context)
        return BaseViewHolder(itemView)
    }
}