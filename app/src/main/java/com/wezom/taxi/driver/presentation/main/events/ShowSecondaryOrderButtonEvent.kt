package com.wezom.taxi.driver.presentation.main.events

/**
 * Created by zorin.a on 22.05.2018.
 */
class ShowSecondaryOrderButtonEvent(val isShow: Boolean)