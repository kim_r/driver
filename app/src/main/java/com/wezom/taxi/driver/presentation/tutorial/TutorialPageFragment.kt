package com.wezom.taxi.driver.presentation.tutorial

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wezom.taxi.driver.databinding.TutorialPageBinding

/**
 * Created by zorin.a on 023 23.02.18.
 */
class TutorialPageFragment : Fragment() {
    lateinit var binding: TutorialPageBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = TutorialPageBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var imageRes = arguments?.getInt(IMAGE_KEY)
        var titleRes = arguments?.getInt(TITLE_KEY)
        var messageRes = arguments?.getInt(MESSAGE_KEY)
        binding.tutorialImage.setImageResource(imageRes!!)
        binding.tutorialTitle.text = getString(titleRes!!)
        binding.tutorialMessage.text = getString(messageRes!!)
    }

    companion object {
        const val IMAGE_KEY = "image_key"
        const val TITLE_KEY = "title_key"
        const val MESSAGE_KEY = "message_key"

        fun newInstance(imageRes: Int, titleRes: Int, messageRes: Int): TutorialPageFragment {
            var fragment = TutorialPageFragment()
            var args = Bundle()
            args.putInt(IMAGE_KEY, imageRes)
            args.putInt(TITLE_KEY, titleRes)
            args.putInt(MESSAGE_KEY, messageRes)
            fragment.arguments = args
            return fragment
        }
    }
}