package com.wezom.taxi.driver.presentation.profile

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxTextView
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.bus.events.CarValidityEvent
import com.wezom.taxi.driver.common.*
import com.wezom.taxi.driver.data.models.Car
import com.wezom.taxi.driver.data.models.CarParameter
import com.wezom.taxi.driver.data.models.CarRequisitesCorrectness
import com.wezom.taxi.driver.databinding.AboutCarBinding
import com.wezom.taxi.driver.ext.*
import com.wezom.taxi.driver.presentation.base.BaseFragment
import com.wezom.taxi.driver.presentation.profile.ProfileFragment.Companion.MODE_KEY
import com.wezom.taxi.driver.presentation.profile.dialog.CarChoiceDialog
import com.wezom.taxi.driver.presentation.profile.dialog.CarColorDialog
import com.wezom.taxi.driver.presentation.profile.dialog.CarYearDialog
import com.wezom.taxi.driver.presentation.profile.events.CarDetailsEvent
import com.wezom.taxi.driver.presentation.profile.events.ProfileActionClickEvent
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by zorin.a on 28.02.2018.
 */
class AboutCarFragment : BaseFragment() {

    //region var
    private lateinit var binding: AboutCarBinding
    private lateinit var viewModel: AboutCarViewModel

    private var carBrands: List<CarParameter>? = null
    private var carModels: List<CarParameter>? = null
    private var carColors: List<CarParameter>? = null
    private var carType: List<CarParameter>? = null
    private var carYears: List<Int>? = null
    private var photoFile: File? = null

    private val carBrandObserver = Observer<List<CarParameter>> { brands ->
        carBrands = brands
    }
    private val carTypeObserver = Observer<List<CarParameter>> { type ->
        carType = type
    }
    private val carModelObserver = Observer<List<CarParameter>> { models ->
        carModels = models
    }
    private val carColorObserver = Observer<List<CarParameter>> { colors ->
        carColors = colors
    }
    private val carYearsObserver = Observer<List<Int>> { years ->
        carYears = years
    }

    private var checkedBrandPos = UNSELECTED
    private var checkedModelPos = UNSELECTED
    private var checkedColorPos = UNSELECTED
    private var checkedYearPos = UNSELECTED

    private var car: Car? = null
    private var type: ArrayList<Int>? = null
    private var carCorrectness: CarRequisitesCorrectness? = null
    private var mode: Int? = null

    private var chosenBrand: CarParameter? = null
    private var chosenModel: CarParameter? = null
    private var chosenCarColor: CarParameter? = null
//    private var typeCar: MutableList<CarParameter>? = ArrayList()
    private var chosenCarYear: Int? = null
    private var carNumber: String? = null

    //endregion

    //region override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        car = arguments?.getParcelable(CAR_KEY)
        type = arguments?.getIntegerArrayList(TYPE_KEY)
        carCorrectness = arguments?.getParcelable(CORRECTNESS_KEY)
        mode = arguments?.getInt(MODE_KEY)
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()

        viewModel.carBrandsLiveData.observe(this, carBrandObserver)
        viewModel.carTypeLiveData.observe(this, carTypeObserver)
        viewModel.carModelsLiveData.observe(this, carModelObserver)
        viewModel.carColorsLiveData.observe(this, carColorObserver)
        viewModel.carYearsLiveData.observe(this, carYearsObserver)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = AboutCarBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        when (mode) {
            ProfileFragment.MODE_PROFILE -> setAsProfile()
            ProfileFragment.MODE_MODERATION -> setAsModeration(false)
            ProfileFragment.MODE_NEED_COMPLETE -> setAsProfile()
        }

        viewModel.getCarBrands()
        viewModel.getCarColors()
        viewModel.getType()

        sharedClicks()
        sharedListeners()
        checkCorrectness()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                Constants.REQUEST_IMAGE_PICK -> {
                    val uri = data?.data
                    uri.let {
                        cropImage(it.toString())
                    }
                }
                Constants.REQUEST_IMAGE_CAPTURE -> {
                    val uri = Uri.fromFile(photoFile)
                    uri.let {
                        cropImage(it.toString())
                    }
                }
            }
        }
    }

    //endregion

    //region click
    private fun sharedClicks() {
        binding.run {
            disposable += RxView.clicks(carBrandEditText).subscribe({

                val dialog = CarChoiceDialog.newInstance(carBrands as ArrayList<CarParameter>, checkedBrandPos)
                dialog.show(childFragmentManager, CarChoiceDialog.TAG)
                dialog.listener = object : CarChoiceDialog.ChoiceDialogSelection {
                    override fun onSelected(pos: Int, CarParameter: CarParameter) {
                        viewModel.getCarModels(CarParameter.id)
                        chosenBrand = CarParameter
                        checkedBrandPos = pos
                        binding.carBrandEditText.setText(CarParameter.name)

                        binding.carModelEditText.text = null
                        checkedModelPos = UNSELECTED

                        binding.yearEditText.text = null
                        checkedYearPos = UNSELECTED

                        chosenModel = null
                        chosenCarYear = null

                        publishCar()
                    }
                }
            }, { Timber.e(it) })

            economSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
                carType?.let {
                    if (isChecked) {
                        type!!.add(carType!![0].id)
                    } else {
                        type!!.remove(carType!![0].id)
                    }
                    publishCar()
                }
            }
            uniSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
                carType?.let {
                    if (isChecked) {
                        type!!.add(carType!![1].id)
                    } else {
                        type!!.remove(carType!![1].id)
                    }
                    publishCar()
                }
            }
            minibusSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
                carType?.let {
                    if (isChecked) {
                        type!!.add(carType!![2].id)
                    } else {
                        type!!.remove(carType!![2].id)
                    }
                    publishCar()
                }
            }

            disposable += RxView.clicks(carModelEditText).subscribe({
                if (carModels != null) {
                    val dialog = CarChoiceDialog.newInstance(carModels as ArrayList<CarParameter>, checkedModelPos)
                    dialog.show(childFragmentManager, CarChoiceDialog.TAG)
                    dialog.listener = object : CarChoiceDialog.ChoiceDialogSelection {
                        override fun onSelected(pos: Int, CarParameter: CarParameter) {
                            chosenModel = CarParameter
                            checkedModelPos = pos
                            binding.carModelEditText.setText(CarParameter.name)
                            loadIssueYears()
                            publishCar()
                        }
                    }
                }
            }, { Timber.e(it) })

            disposable += RxView.clicks(colorEditText).subscribe({
                if (carColors != null) {
                    val dialog = CarColorDialog.newInstance(carColors as ArrayList<CarParameter>, checkedColorPos)
                    dialog.show(childFragmentManager, CarColorDialog.TAG)
                    dialog.listener = object : CarColorDialog.ChoiceDialogSelection {
                        override fun onSelected(pos: Int, carColor: CarParameter) {
                            chosenCarColor = carColor
                            checkedColorPos = pos
                            binding.colorEditText.setText(carColor.name)
                            publishCar()
                        }
                    }
                }
            }, { Timber.e(it) })

            disposable += RxView.clicks(yearEditText).subscribe({
                if (carYears != null) {
                    val dialog = CarYearDialog.newInstance(carYears as ArrayList<Int>, checkedYearPos)
                    dialog.show(childFragmentManager, CarYearDialog.TAG)
                    dialog.listener = object : CarYearDialog.ChoiceDialogSelection {
                        override fun onSelected(position: Int, carYear: Int) {
                            chosenCarYear = carYear
                            checkedYearPos = position
                            binding.yearEditText.setText(carYear.toString())
                            publishCar()
                        }
                    }
                }
            }, { Timber.e(it) })
        }
    }
    //endregion

    private fun checkCorrectness() {
        if (carCorrectness != null) {
            binding.run {
                if (!carCorrectness?.isBrandCorrect!!) {
                    setErrorColor(carBrandEditText)
                }
                if (!carCorrectness?.isModelCorrect!!) {
                    setErrorColor(carModelEditText)
                }
                if (!carCorrectness?.isColorCorrect!!) {
                    setErrorColor(colorEditText)
                }
                if (!carCorrectness?.isYearCorrect!!) {
                    setErrorColor(yearEditText)
                }
                if (!carCorrectness?.isNumberCorrect!!) {
                    setErrorColor(carNumberEditText)
                }
            }
        }
    }

    private fun setErrorColor(view: View) {
        val background = view.background
        background.setColorFilter(resources.getColor(R.color.colorRed), PorterDuff.Mode.SRC_ATOP)
        view.background = background
    }

    private fun sharedListeners() {
        binding.run {
            disposable += RxTextView.textChangeEvents(carNumberEditText).subscribe {
                carNumber = carNumberEditText.text.toString()
                publishCar()
            }
            disposable += RxBus.listen(CarValidityEvent::class.java).subscribe { validity ->
                if (!validity.isBrandValid) {
                    setErrorColor(carBrandEditText)
                }
                if (!validity.isModelValid) {
                    setErrorColor(carModelEditText)
                }
                if (!validity.isColorValid) {
                    setErrorColor(colorEditText)
                }
                if (!validity.isNumberValid) {
                    setErrorColor(carNumberEditText)
                }
                if (!validity.isYearValid) {
                    setErrorColor(yearEditText)
                }
            }
        }
    }

    private fun loadIssueYears() {
        if (isBrandAndModelChecked()) {
            viewModel.getCarIssueYears(carModels?.get(checkedModelPos)!!.id)
        }
    }

    private fun isBrandAndModelChecked(): Boolean = checkedBrandPos != UNSELECTED && checkedModelPos != UNSELECTED

    private fun setAsModeration(needCompete: Boolean) {
        chosenBrand = car?.brand
        chosenModel = car?.model
        chosenCarColor = car?.color
        chosenCarYear = car?.year
        carNumber = car?.number
        publishCar()

        binding.run {
            carBrandEditText.setText(car?.brand?.name)
            carModelEditText.setText(car?.model?.name)
            colorEditText.setText(car?.color?.name)
            yearEditText.setText((car?.year ?: "").toString())
            carNumberEditText.setText(car?.number)
            for (item in type!!) {
                when {
                    item == 1 -> {
                        economSwitch.isChecked = true
                    }
                    item == 2 -> {
                        uniSwitch.isChecked = true
                    }
                    item == 3 -> {
                        minibusSwitch.isChecked = true
                    }
                }
            }

            initCarData()

            disposable += RxView.clicks(saveButton).subscribe {
                if (needCompete) {
                    if (carBrandEditText.isEmpty() ||
                            carModelEditText.isEmpty() ||
                            colorEditText.isEmpty() ||
                            yearEditText.isEmpty() ||
                            carNumberEditText.isEmpty()) {
                        getString(R.string.error_need_compleate).shortToast(context)
                        return@subscribe
                    }
                }
                publishCar()
                RxBus.publish(ProfileActionClickEvent(ProfileActionClickEvent.ActionType.SAVE_USER_DATA))
            }
        }
    }

    private fun setAsProfile() {
        binding.run {
            carBrandEditText.setText(car?.brand?.name)
            carModelEditText.setText(car?.model?.name)
            colorEditText.setText(car?.color?.name)
            yearEditText.setText((car?.year ?: "").toString())
            carNumberEditText.setText(car?.number)
            for (item in type!!) {
                when {
                    item == 1 -> {
                        economSwitch.isChecked = true
                    }
                    item == 2 -> {
                        uniSwitch.isChecked = true
                    }
                    item == 3 -> {
                        minibusSwitch.isChecked = true
                    }
                }
            }

            disposable += RxView.clicks(saveButton).subscribe {
                RxBus.publish(ProfileActionClickEvent(ProfileActionClickEvent.ActionType.SAVE_USER_DATA))
            }
        }
        chosenBrand = car?.brand
        chosenModel = car?.model
        chosenCarColor = car?.color
        chosenCarYear = car?.year
        carNumber = car?.number

        initCarData()
    }

    private fun initCarData() {
        chosenBrand?.let {
            viewModel.getCarModels(it.id)
        }

        chosenModel?.let {
            viewModel.getCarIssueYears(it.id)
        }
    }

    private fun publishCar() {
        RxBus.publish(CarDetailsEvent(chosenBrand, chosenModel, type, chosenCarYear, chosenCarColor, carNumber))
    }

    private fun cropImage(uri: String?) {
        uri?.let {
            if (it.isNotEmpty()) {
                viewModel.cropImage(it)
            }
        }
    }

    //endregion
    companion object {
        const val CAR_KEY = "car_key"
        const val CORRECTNESS_KEY = "car_correctness_key"
        const val UNSELECTED = -1
        const val TYPE_KEY = "type_key"

        fun newInstance(mode: Int, car: Car? = null, carRequisitesCorrectness: CarRequisitesCorrectness? = null, type: ArrayList<Int>?): AboutCarFragment {
            val args = Bundle()
            args.putInt(MODE_KEY, mode)
            if (car != null) {
                args.putParcelable(CAR_KEY, car)
            }
            if (type != null) {
                args.putIntegerArrayList(TYPE_KEY, type)
            }
            if (carRequisitesCorrectness != null) {
                args.putParcelable(CORRECTNESS_KEY, carRequisitesCorrectness)
            }
            val fragment = AboutCarFragment()
            fragment.arguments = args
            return fragment
        }
    }
}