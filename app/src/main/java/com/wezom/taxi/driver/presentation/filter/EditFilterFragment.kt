package com.wezom.taxi.driver.presentation.filter

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.common.LocationStorage
import com.wezom.taxi.driver.databinding.FragmentEditFilterBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.ext.hideKeyboard
import com.wezom.taxi.driver.ext.setVisible
import com.wezom.taxi.driver.net.request.CreateFilterRequest
import com.wezom.taxi.driver.net.response.AutoCompleteResult
import com.wezom.taxi.driver.net.response.FilterResponse
import com.wezom.taxi.driver.presentation.base.BaseFragment
import com.wezom.taxi.driver.presentation.filter.base.DeleteFilterEvent
import io.reactivex.disposables.Disposable
import timber.log.Timber
import javax.inject.Inject

/**
 *Created by Zorin.A on 26.June.2019.
 */
class EditFilterFragment : BaseFragment() {
    lateinit var viewModel: EditFilterViewModel
    lateinit var binding: FragmentEditFilterBinding
    var point: AutoCompleteResult? = null

    @Inject
    lateinit var locationStorage: LocationStorage

    private fun hideKeyboard() {
        view?.let { activity?.hideKeyboard() }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this,
                viewModelFactory)
                .getViewModelOfType()
        setBackPressFun {
            hideKeyboard()
            viewModel.onBackPressed()
        }
        viewModel.resultLiveData.observe(this,
                Observer {
                    binding.run {
                        it?.let {
                            addressValue.text = it
                            enableDestinationPointUi(it.isNotBlank())
                            isActiveDestination.isChecked = true
                        }
                    }
                })

        viewModel.resultPointLiveData.observe(this,
                Observer {
                    binding.run {
                        it?.let {
                            point = it
                        }
                    }
                })
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentEditFilterBinding.inflate(inflater,
                container,
                false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onViewCreated(view: View,
                               savedInstanceState: Bundle?) {
        super.onViewCreated(view,
                savedInstanceState)
        initViews()
        updateUi()
        listeners()

    }

    private lateinit var disposableFilter: Disposable

    private fun listeners() {
        disposableFilter = RxBus.listen(DeleteFilterEvent::class.java).subscribe {
            filter!!.id?.let { it1 -> viewModel.deleteFilter(it1) }
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        if (!disposableFilter.isDisposed) disposableFilter.dispose()
    }

    private fun initViews() {
        binding.run {
            toolbar.setToolbarTitle(getString(R.string.edit))
            radiusValue.text = getString(R.string.about_few_km,
                    (spinnerRadius.progress / 10.0).toString())
            destinationValue.text = getString(R.string.about_few_km,
                    (spinnerDestination.progress / 10.0).toString())

            spinnerRadius.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar?,
                                               value: Int,
                                               p2: Boolean) {
                    radiusValue.text = getString(R.string.about_few_km,
                            if (value <= 5) "0.5" else (value / 10.0).toString())
                }

                override fun onStartTrackingTouch(p0: SeekBar?) {
                }

                override fun onStopTrackingTouch(p0: SeekBar?) {
                }
            })

            spinnerDestination.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(p0: SeekBar?,
                                               value: Int,
                                               p2: Boolean) {
                    destinationValue.text = getString(R.string.about_few_km,
                            if (value <= 5) "0.5" else (value / 10.0).toString())
                }

                override fun onStartTrackingTouch(p0: SeekBar?) {
                }

                override fun onStopTrackingTouch(p0: SeekBar?) {
                }
            })

            spinnerRadius.isEnabled = isActiveRadius.isChecked
            isActiveRadius.setOnCheckedChangeListener { _, isChecked ->
                Timber.d("IS_CHECKED: $isChecked")
                spinnerRadius.isEnabled = isChecked
            }

            spinnerDestination.isEnabled = isActiveDestination.isChecked
            isActiveDestination.setOnCheckedChangeListener { _, isChecked ->
                Timber.d("IS_CHECKED: $isChecked")
                if (!isChecked) {
                    enableDestinationPointUi(false)
                }

            }
            isActiveDestination.setOnClickListener {
                if (isActiveDestination.isChecked) viewModel.openPickPlaceScreen()
                isActiveDestination.isChecked = false
            }

            save.setOnClickListener {
                val createFilterRequest = CreateFilterRequest()
                if (binding.title.text.isNotEmpty()) {
                    createFilterRequest.name = binding.title.text.toString()
                }
                if (binding.isActiveRadius.isChecked) {
                    createFilterRequest.fromLatitude = locationStorage.getCachedLocation().latitude
                    createFilterRequest.fromLongitude = locationStorage.getCachedLocation().longitude
                    createFilterRequest.fromRadius = spinnerRadius.progress / 10.0
                }

                if (binding.isActiveDestination.isChecked) {
                    point?.let {
                        createFilterRequest.toLatitude = it.lat
                        createFilterRequest.toLongitude = it.lon
                    } ?: run {
                        createFilterRequest.toLatitude = filter!!.toLatitude!!.toDouble()
                        createFilterRequest.toLongitude = filter!!.toLongitude!!.toDouble()
                        createFilterRequest.address = filter!!.address
                    }
                    createFilterRequest.address = addressValue.text.toString()
                    createFilterRequest.toRadius = spinnerDestination.progress / 10.0

                }
                hideKeyboard()
                viewModel.editFilter(filter!!.id!!, createFilterRequest)
            }
        }
    }

    var filter: FilterResponse? = null


    private fun updateUi() {
        // point = AutoCompleteResult();
        binding.run {
            filter = arguments?.getParcelable(FILTER_KEY)!!
            filter!!.name?.let {
                title.setText(filter!!.name)
            }

            filter!!.fromLatitude?.let {
                isActiveRadius.isChecked = true
                spinnerRadius.progress = (filter!!.fromRadius!! * 10).toInt()
            }

            filter!!.toLatitude?.let {
                TransitionManager.beginDelayedTransition(destinationContainer)
                destinationValue setVisible true
                spinnerDestination setVisible true
                destinationMaxValue setVisible true
                addressLabel setVisible true
                addressValue setVisible true
                addressValue.text = filter!!.address
                spinnerDestination.isEnabled = true
                isActiveDestination.isChecked = true
                spinnerDestination.progress = (filter!!.toRadius!! * 10).toInt()
            }
        }
    }

    private fun enableDestinationPointUi(enabled: Boolean) {
        binding.run {
            TransitionManager.beginDelayedTransition(destinationContainer)
            destinationValue setVisible enabled
            spinnerDestination setVisible enabled
            destinationMaxValue setVisible enabled
            addressLabel setVisible enabled
            addressValue setVisible enabled
            spinnerDestination.isEnabled = enabled
        }
    }

    companion object {
        const val FILTER_KEY = "filter_key"
        fun newInstance(data: FilterResponse): EditFilterFragment {
            val args = Bundle()
            args.putParcelable(FILTER_KEY, data)
            val fragment = EditFilterFragment()
            fragment.arguments = args
            return fragment
        }
    }
}