package com.wezom.taxi.driver.data.models.notifications

import com.wezom.taxi.driver.data.models.NewOrderInfo

class NewOrderAvailable(val typeId: String, val title: String, val body: String, val order: NewOrderInfo) : BasicNotification