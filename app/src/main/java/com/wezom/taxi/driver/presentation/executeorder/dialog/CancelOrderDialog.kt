package com.wezom.taxi.driver.presentation.executeorder.dialog

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.common.BeepUtil
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import java.util.concurrent.TimeUnit


/**
 * Created by zorin.a on 05.03.2018.
 */
class CancelOrderDialog : DialogFragment() {
    var listener: DialogClickListener? = null
    private var disposable = CompositeDisposable()
    private var count: Int = 60

    @SuppressLint("DefaultLocale")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        BeepUtil.stop()
        BeepUtil.playMassage()
        var dialog = AlertDialog.Builder(context!!, R.style.BlackDialogTheme)
                .setTitle(getString(R.string.attention2))
                .setMessage(getString(R.string.auto_close_order, count.toString()))
                .setPositiveButton(getString(R.string.yes).toUpperCase()) { _, _ ->
                    BeepUtil.stop()
                    disposable.dispose()
                    if (listener != null)
                        listener?.onPositiveClick()

                }
                .setNegativeButton(getString(R.string.not).toUpperCase()) { _, _ ->
                    BeepUtil.stop()
                    disposable.dispose()
                    listener?.onNegativeClick()
                }
                .create()

        disposable += Observable.interval(0,
                1,
                TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (count == 1) {
                        if (listener != null)
                            listener?.onPositiveClick()
                        disposable.dispose()
                        BeepUtil.stop()
                    }
                    count--
                    dialog.setMessage(getString(R.string.auto_close_order, count.toString()))
                }
        return dialog
    }

    companion object {
        val TAG: String = StartTripDialog.javaClass.simpleName
    }

    interface DialogClickListener {
        fun onPositiveClick()
        fun onNegativeClick()
    }
}