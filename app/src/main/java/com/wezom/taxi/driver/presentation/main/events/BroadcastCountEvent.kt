package com.wezom.taxi.driver.presentation.main.events

/**
 *Created by Zorin.A on 21.June.2019.
 */
class BroadcastCountEvent constructor(val count: Int)