package com.wezom.taxi.driver.presentation.addcard

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import android.view.MenuItem
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.data.models.PaymentInfo
import com.wezom.taxi.driver.databinding.AddCardActivityBinding
import com.wezom.taxi.driver.ext.longToast
import com.wezom.taxi.driver.ext.setVisible
import timber.log.Timber
import java.io.File


class AddCardActivity : AppCompatActivity() {

    private lateinit var binding: AddCardActivityBinding

    private var file: File? = null

//    private val css: String by lazy {
//        extractFromFile("way_for_pay_form.css")
//    }
//
//    private val js: String by lazy {
//        extractFromFile("way_for_pay_form.js")
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_card)
        setSupportActionBar(binding.addCardToolbar.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = getString(R.string.add_card)

        WebView.setWebContentsDebuggingEnabled(true)
        with(binding.verificationWebView) {
            settings.javaScriptEnabled = true
            isVerticalScrollBarEnabled = false
            webChromeClient = object : WebChromeClient() {
                override fun onProgressChanged(view: WebView?, newProgress: Int) {
                    super.onProgressChanged(view, newProgress)
                    binding.addCardToolbar.progress.run {
                        if (newProgress == 100)
                            setVisible(false)
                        else
                            setVisible(true)
                        progress = newProgress
                    }
                }
            }

            webViewClient = object : WebViewClient() {
                override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                    super.onPageStarted(view, url, favicon)
                    binding.verificationWebView.setVisible(false)
                }

                override fun onPageFinished(view: WebView?, url: String) {
                    Timber.d("Loading: $url")
//                    injectCSS(js, css)
                    when {
                        url.contains("confirm") -> executeConfirm()
                        url.contains("closing") -> verifyClosing()
                        else -> binding.verificationWebView.setVisible(true)
                    }
                }
            }

            intent.extras.getParcelable<PaymentInfo>(PAYMENT_INFO)?.let {
                loadData(
                        constructHtml(
                                it.orderReference,
                                it.amount,
                                it.currency,
                                it.merchantAccount,
                                it.merchantDomainName,
                                it.merchantSignature,
                                it.apiVersion,
                                it.serviceUrl,
                                it.clientPhone,
                                it.clientEmail,

                                it.orderDate,
                                it.productName,
                                it.productCount,
                                it.productPrice,
                                it.merchantTransactionType,
                                it.merchantTransactionSecureType,
                                it.holdTimeout
                        ), "text/html", "UTF-8"
                )
            } ?: throw IllegalArgumentException("Payment info can't be null")
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        file?.delete()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun extractFromFile(fileName: String): String {
        return assets.open(fileName).use {
            val buffer = ByteArray(it.available())
            it.read(buffer)
            Base64.encodeToString(buffer, Base64.NO_WRAP)
        }
    }

    private fun verifyClosing() {
        binding.verificationWebView.evaluateJavascript(
                "" +
                        "(function() {\n" +
                        "    if (document.querySelector('.text-danger') !== null) {\n" +
                        "        return true;\n" +
                        "    } else {\n" +
                        "        return false;\n" +
                        "    }\n" +
                        "})();"
        ) {
            if (it == "true") {
                getString(R.string.error_add_card).longToast(this)
                setResult(Activity.RESULT_CANCELED)
            } else {
                getString(R.string.success_add_card).longToast(this)
                setResult(Activity.RESULT_OK)
            }

            finish()
        }
    }

    private fun executeConfirm() {
        binding.verificationWebView.evaluateJavascript(
                "(function () {" +
                        "    document.getElementById('cardconfirm-amount').value = '1';" +
                        "    document.getElementById('cardconfirm-submit').click();" +
                        "})();", null
        )
    }


    private fun constructHtml(
            orderReference: String,
            amount: Int,
            currency: String,
            merchantAccount: String,
            merchantDomainName: String,
            merchantSignature: String,
            apiVersion: Int,
            serviceUrl: String,
            clientPhone: String,
            clientEmail: String? = null,
            orderDate: String,
            productName: String,
            productCount: Int,
            productPrice: Int,
            merchantTransactionType: String,
            merchantTransactionSecureType: String,
            holdTimeout: Int
    ): String {
        val emailInput = clientEmail?.let {
            "            <input\n" +
                    "                    type=\"hidden\"\n" +
                    "                    name=\"clientEmail\"\n" +
                    "                    value=\"$it\">\n"
        } ?: ""
// https://secure.wayforpay.com/verify/hub_driver
        // https://secure.wayforpay.com/pay
        //https://secure.wayforpay.com/pay/hub_driver
        return "<!DOCTYPE html>\n" +
                "<html>\n" +
                "    <head>\n" +
                "        <meta\n" +
                "                http-equiv=\"Content-Type\"\n" +
                "                content=\"text/html; charset=utf-8\">\n" +
                "        <script type=\"text/javascript\">\n" +
                "        function dosubmit() { document.forms[0].submit(); }\n" +
                "\n" +
                "        </script>\n" +
                "    </head>\n" +
                "\n" +
                "    <body onload=\"dosubmit();\">\n" +
                "        <form\n" +
                "                action=\"https://secure.wayforpay.com/pay/hub_driver\"\n" +
                "                method=\"POST\"\n" +
                "                accept-charset=\"utf-8\">\n" +
                "            <input\n" +
                "                    type=\"hidden\"\n" +
                "                    name=\"orderReference\"\n" +
                "                    value=\"$orderReference\">\n" +
                "            <input\n" +
                "                    type=\"hidden\"\n" +
                "                    name=\"amount\"\n" +
                "                    value=\"$amount\">\n" +
                "            <input\n" +
                "                    type=\"hidden\"\n" +
                "                    name=\"currency\"\n" +
                "                    value=\"$currency\">\n" +
                "            <input\n" +
                "                    type=\"hidden\"\n" +
                "                    name=\"merchantAccount\"\n" +
                "                    value=\"$merchantAccount\">\n" +
                "            <input\n" +
                "                    type=\"hidden\"\n" +
                "                    name=\"merchantDomainName\"\n" +
                "                    value=\"$merchantDomainName\">\n" +
                "            <input\n" +
                "                    type=\"hidden\"\n" +
                "                    name=\"merchantSignature\"\n" +
                "                    value=\"$merchantSignature\">\n" +
                "            <input\n" +
                "                    type=\"hidden\"\n" +
                "                    name=\"apiVersion\"\n" +
                "                    value=\"$apiVersion\">\n" +
                "            <input\n" +
                "                    type=\"hidden\"\n" +
                "                    name=\"serviceUrl\"\n" +
                "                    value=\"$serviceUrl\">\n" +
                "            <input\n" +
                "                    type=\"hidden\"\n" +
                "                    name=\"orderDate\"\n" +
                "                    value=\"$orderDate\">\n" +
                "            <input\n" +
                "                    type=\"hidden\"\n" +
                "                    name=\"productName[0]\"\n" +
                "                    value=\"${productName}\">\n" +
                "            <input\n" +
                "                    type=\"hidden\"\n" +
                "                    name=\"productCount[0]\"\n" +
                "                    value=\"${productCount}\">\n" +
                "            <input\n" +
                "                    type=\"hidden\"\n" +
                "                    name=\"productPrice[0]\"\n" +
                "                    value=\"${productPrice}\">\n" +
                "            <input\n" +
                "                    type=\"hidden\"\n" +
                "                    name=\"merchantTransactionType\"\n" +
                "                    value=\"$merchantTransactionType\">\n" +
                "            <input\n" +
                "                    type=\"hidden\"\n" +
                "                    name=\"merchantTransactionSecureType\"\n" +
                "                    value=\"$merchantTransactionSecureType\">\n" +
                "            <input\n" +
                "                    type=\"hidden\"\n" +
                "                    name=\"holdTimeout\"\n" +
                "                    value=\"$holdTimeout\">\n" +
                "            <input\n" +
                "                    type=\"hidden\"\n" +
                "                    name=\"clientPhone\"\n" +
                "                    value=\"$clientPhone\">\n" + emailInput +
                "        </form>\n" +
                "    </body>\n" +
                "\n" +
                "</html>"
    }

    companion object {
        const val PAYMENT_INFO = "payment_info"
        const val RESULT_SUCCESS = 57
        const val RESULT_ERROR = 56

        fun startAddCardActivity(context: Context, paymentInfo: PaymentInfo) {
            Timber.d("ADD_CARD: startAddCardActivity()")
            val intent = Intent(context, AddCardActivity::class.java).apply {
                putExtra(PAYMENT_INFO, paymentInfo)
                flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
            }
            (context as Activity).startActivityForResult(intent, -1)
        }
    }

}
