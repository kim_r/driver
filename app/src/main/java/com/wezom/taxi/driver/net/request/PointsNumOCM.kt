package com.wezom.taxi.driver.net.request

import com.google.gson.annotations.SerializedName

class PointsNumOCM {
    @SerializedName("longitude")
    var longitude: Double? = null
    @SerializedName("latitude")
    var latitude: Double? = null
    @SerializedName("number")
    var number: Int? = null

    constructor(longitude: Double?, latitude: Double?, number: Int?) {
        this.longitude = longitude
        this.latitude = latitude
        this.number = number
    }

    constructor()

}