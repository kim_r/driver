package com.wezom.taxi.driver.presentation.changephone

import com.wezom.taxi.driver.common.*
import com.wezom.taxi.driver.data.models.ResponseState
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.repository.DriverRepository
import io.reactivex.rxkotlin.plusAssign
import javax.inject.Inject

/**
 * Created by udovik.s on 07.03.2018.
 */
class ChangePhoneViewModel @Inject constructor(screenRouterManager: ScreenRouterManager, private val repository: DriverRepository) : BaseViewModel(screenRouterManager) {

    fun changePhone(phone: String) {
        loadingLiveData.postValue(ResponseState(ResponseState.State.LOADING))
        App.instance.getLogger()!!.log("changePhone start ")
        disposable += repository.changePhone(phone).subscribe({ response ->
            App.instance.getLogger()!!.log("changePhone suc ")
            handleResponseState(response)
            if (response?.isSuccess!!) {
                navigateToVerificationPhone(phone)
            } else {
                loadingLiveData.postValue(ResponseState(ResponseState.State.ERROR, message = response.error!!.message))
            }
        }, {
            App.instance.getLogger()!!.log("changePhone error ")
            loadingLiveData.postValue(ResponseState(ResponseState.State.NETWORK_ERROR))
        })
    }

    fun navigateToVerificationPhone(phone: String) {
        switchScreen(CHANGE_PHONE_VERIFICATION_SCREEN, phone)
    }
}