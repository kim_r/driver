package com.wezom.taxi.driver.presentation.tutorial

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.databinding.TutorialFragmentBinding

import com.wezom.taxi.driver.presentation.base.BaseFragment

/**
 * Created by zorin.a on 023 23.02.18.
 */
class TutorialFragment : BaseFragment() {

    private lateinit var binding: TutorialFragmentBinding
    private lateinit var viewModel: TutorialViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = TutorialFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()

        RxView.clicks(binding.skipTutorialButton).subscribe({ viewModel.skipTutorial() })

        binding.run {
            val adapter = TutorialPagerAdapter(childFragmentManager)
            adapter.addFragment(TutorialPageFragment.newInstance(R.drawable.img_tutorial_1, R.string.tutor_1_caption, R.string.tutor_1_message))
            adapter.addFragment(TutorialPageFragment.newInstance(R.drawable.img_tutorial_2, R.string.tutor_2_caption, R.string.tutor_2_message))
            adapter.addFragment(TutorialPageFragment.newInstance(R.drawable.img_tutorial_3, R.string.tutor_3_caption, R.string.tutor_3_message))
            adapter.addFragment(TutorialPageFragment.newInstance(R.drawable.img_tutorial_4, R.string.tutor_4_caption, R.string.tutor_4_message))
            adapter.addFragment(TutorialPageFragment.newInstance(R.drawable.img_tutorial_5, R.string.tutor_5_caption, R.string.tutor_5_message))
            binding.tutorialViewpager.adapter = adapter
            binding.tutorialTabs.setupWithViewPager(binding.tutorialViewpager)
        }
    }
 }