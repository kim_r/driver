package com.wezom.taxi.driver.presentation.profile.dialog

import android.app.Dialog
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.databinding.InfoDialogBinding

/**
 * Created by udovik.s on 05.03.2018.
 */
class InfoDialog : DialogFragment() {
    var listener: DialogClickListener? = null
    private lateinit var binding: InfoDialogBinding

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        binding = DataBindingUtil.inflate(
                activity!!.layoutInflater,
                R.layout.dialog_info,
                null,
                false
        )
        updateUi(arguments?.getInt(KEY_IMAGE)!!,
                arguments?.getInt(KEY_TITLE)!!,
                arguments?.getInt(KEY_TEXT)!!)

        val isNegativeButtonEnable = arguments?.getBoolean(KEY_IS_NEGATIVE)

        val builder = AlertDialog.Builder(context!!, R.style.WhiteDialogTheme)


        builder.setView(binding.root)
                .setPositiveButton(R.string.ok) { _, _ -> listener?.onPositiveClick() }

        if(isNegativeButtonEnable!!)
                builder.setNegativeButton(R.string.cancel_1) { _, _ -> listener?.onNegativeClick() }

        return builder.create()
    }

    private fun updateUi(image: Int? = null, title: Int, text: Int) {
        binding.run {
            if (image != null) {
                dialogDocumentImage.setImageResource(image)
            }
            dialogDocumentTitle.setText(title)
            dialogDocumentText.setText(text)
        }
    }

    interface DialogClickListener {
        fun onPositiveClick()
        fun onNegativeClick()
    }

    companion object {
        val TAG: String = "InfoDialog"
        const val KEY_IMAGE = "key_image"
        const val KEY_TITLE = "key_title"
        const val KEY_TEXT = "key_text"
        const val KEY_IS_NEGATIVE = "key_is_negative"

        fun newInstance(image: Int, title: Int, text: Int): InfoDialog {
            val args = Bundle()
            args.putInt(KEY_TITLE, title)
            args.putInt(KEY_TEXT, text)
            args.putInt(KEY_IMAGE, image)
            val fragment = InfoDialog()
            fragment.arguments = args
            return fragment
        }

        fun newInstance(title: Int, text: Int, isNegativeButtonEnable: Boolean = false): InfoDialog {
            val args = Bundle()
            args.putInt(KEY_TITLE, title)
            args.putInt(KEY_TEXT, text)
            args.putBoolean(KEY_IS_NEGATIVE, isNegativeButtonEnable)
            val fragment = InfoDialog()
            fragment.arguments = args
            return fragment
        }
    }
}