package com.wezom.taxi.driver.net.jobqueue.jobs

import android.content.Context
import com.birbit.android.jobqueue.Job
import com.birbit.android.jobqueue.Params
import com.birbit.android.jobqueue.RetryConstraint
import com.wezom.taxi.driver.data.db.DbManager
import com.wezom.taxi.driver.net.api.ApiManager
import com.wezom.taxi.driver.net.jobqueue.JobInjector


/**
 * Created by zorin.a on 020 20.02.18.
 */
abstract class BaseJob : Job(Params(PRIORITY_HIGH)
        .groupBy("driver_actions")
        .requireNetwork()
        .persist()), JobInjector {
    @Transient
    protected  lateinit  var apiManager: ApiManager
    @Transient
    protected lateinit var context: Context
    @Transient
    protected lateinit var dbManager: DbManager

    override fun inject(apiManager: ApiManager, context: Context, dbManager: DbManager) {
        this.apiManager = apiManager
        this.context = context
        this.dbManager = dbManager
    }

    override fun shouldReRunOnThrowable(throwable: Throwable, runCount: Int, maxRunCount: Int): RetryConstraint {
        return RetryConstraint.RETRY
    }

    override fun onAdded() {

    }

    override fun onCancel(cancelReason: Int, throwable: Throwable?) {

    }

       companion object {
        const val PRIORITY_HIGH: Int = 1000
        const val PRIORITY_MID: Int = 500
        const val PRIORITY_LOW: Int = 0
    }
}