package com.wezom.taxi.driver.injection.scope

import javax.inject.Scope

/**
 * Created by zorin.a on 020 20.02.18.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope