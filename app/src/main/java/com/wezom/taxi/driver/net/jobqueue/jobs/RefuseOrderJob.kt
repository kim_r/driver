package com.wezom.taxi.driver.net.jobqueue.jobs

import android.annotation.SuppressLint
import com.wezom.taxi.driver.net.request.RefuseOrderRequest

/**
 * Created by zorin.a on 07.05.2018.
 */
class RefuseOrderJob constructor(val id: Int, val request: RefuseOrderRequest) : BaseJob() {
    @SuppressLint("CheckResult")
    override fun onRun() {
        apiManager.refuseOrder(id, request).blockingGet()
    }
}