package com.wezom.taxi.driver.common

/**
 * Created by zorin.a on 16.02.2018.
 */

const val SPLASH_SCREEN = "splash_fragment"
const val MAIN_MAP_SCREEN = "main_map_screen"
const val TUTORIAL_SCREEN = "tutorial_fragment"
const val VERIFICATION_PHONE_SCREEN = "verification_phone_screen"
const val PROFILE_SCREEN = "profile_fragment"
const val ADD_CARD_SCREEN = "add_card_fragment"
const val PHOTO_VIEWER_SCREEN = "photo_viewer_screen"
const val CHANGE_PHONE_SCREEN = "change_phone_screen"
const val ORDER_FOUND_SCREEN = "order_found_screen"
const val ORDER_HISTORY_LIST_SCREEN = "order_history_list_screen"
const val SETTINGS_SCREEN = "settings_screen"
const val PRIVATE_STATISTICS_SCREEN = "private_statistics_screen"
const val COMPLETED_ORDER_SCREEN = "completed_order_screen"
const val BALANCE_SCREEN = "balance_screen"
const val HOW_TO_REPLENISH_SCREEN = "how_to_replenish_screen"
const val BONUS_SCREEN = "bonus_screen"
const val GAINS_SCREEN = "gains_screen"
const val BLOCK_SCREEN = "block_screen"
const val RATING_SCREEN = "rating_screen"
const val HELP_SCREEN = "help_screen"
const val EXECUTE_ORDER_SCREEN = "execute_order_screen"
const val REFUSE_ORDER_SCREEN = "refuse_order_screen"
const val NOT_PAID_SCREEN = "not_paid_screen"
const val TOTAL_COST_SCREEN = "total_cost_screen"
const val NEW_ORDER_SCREEN = "new_order_screen"
const val CHANGE_PHONE_VERIFICATION_SCREEN = "change_phone_verification_screen"
const val ACCOUNT_DISABLED_SCREEN = "account_disabled_screen"
const val SELECT_WAY_POINT_SCREEN = "select_way_point_screen"
const val SIMPLE_REGISTRATION_SCREEN = "simple_registration_screen"
const val IMAGE_PICKER_SCREEN = "image_picker_screen"
const val BONUS_PROGRAM_SCREEN = "bonus_program_screen"
const val ETHER_SCREEN = "ether_screen"
const val ACCEPT_ORDER_SCREEN = "accept_order_screen"
const val USER_INFO_SCREEN = "user_info_screen"
const val FILTER_SCREEN = "filter"
const val CREATE_FILTER_SCREEN = "create_filter_screen"
const val EDIT_FILTER_SCREEN = "edit_filter_screen"
const val PICK_LOCATION_SCREEN = "pick_location_screen"
const val PICK_LOCATION_MAP_SCREEN = "pick_location_map_screen"
const val SETTING_SIGNAL_FRAGMENT = "setting_signal_fragment"
