package com.wezom.taxi.driver.presentation.customview

import android.content.Context
import android.support.v7.widget.Toolbar
import android.util.AttributeSet
import android.view.LayoutInflater
import com.wezom.taxi.driver.databinding.StandartToolbarBinding

/**
 * Created by zorin.a on 022 22.02.18.
 */

class StandardToolbar @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, attributeSetId: Int = 0) : Toolbar(context, attrs, attributeSetId) {
    private var binding: StandartToolbarBinding = StandartToolbarBinding.inflate(LayoutInflater.from(context), this, true)

    fun setToolbarTitle(title: String) {
        binding.toolbar.title = title
    }
}