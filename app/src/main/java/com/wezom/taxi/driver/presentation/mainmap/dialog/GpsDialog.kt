package com.wezom.taxi.driver.presentation.mainmap.dialog

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import com.wezom.taxi.driver.R


class GpsDialog : DialogFragment() {
    var listener: DialogClickListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(context!!, R.style.BlackDialogTheme)
                .setTitle("")
                .setMessage(context!!.getString(R.string.enable_gps_message))
                .setPositiveButton(R.string.ok) { dialogInterface, which ->
                    listener?.onPositiveClick()
                }
                .create()
    }

    companion object {
        val TAG: String = "GpsDialog"
    }

    interface DialogClickListener {
        fun onPositiveClick()
    }
}