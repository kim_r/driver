package com.wezom.taxi.driver.presentation.filter.list

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.View

open class BaseItem<T>(private val binding: View) : RecyclerView.ViewHolder(binding) {
    open var callback: ((position: Int, orderInfo: T) -> Unit)? = null

    @SuppressLint("SetTextI18n")
   open fun bind(dataItem: T) {
        binding.run {
        }
    }


}