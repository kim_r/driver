package com.wezom.taxi.driver.data.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


/**
 * Created by Andrew on 13.03.2018.
 */
data class CarDelivery(
        @SerializedName("time") var time: Int,
        @SerializedName("distance") var distance: Double,
        @SerializedName("tripTime") var tripTime: Int

) : Parcelable {
    constructor(source: Parcel) : this(
            source.readInt(),
            source.readDouble(),
            source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(time)
        writeDouble(distance)
        writeInt(tripTime)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<CarDelivery> = object : Parcelable.Creator<CarDelivery> {
            override fun createFromParcel(source: Parcel): CarDelivery = CarDelivery(source)
            override fun newArray(size: Int): Array<CarDelivery?> = arrayOfNulls(size)
        }
    }
}