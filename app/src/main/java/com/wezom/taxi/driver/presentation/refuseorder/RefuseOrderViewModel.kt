package com.wezom.taxi.driver.presentation.refuseorder

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.common.*
import com.wezom.taxi.driver.data.db.DbManager
import com.wezom.taxi.driver.data.models.*
import com.wezom.taxi.driver.ext.roundTwoDecimal
import com.wezom.taxi.driver.net.api.ApiManager
import com.wezom.taxi.driver.net.jobqueue.JobFactory
import com.wezom.taxi.driver.net.jobqueue.jobs.RefuseOrderJob
import com.wezom.taxi.driver.net.request.RefuseOrderRequest
import com.wezom.taxi.driver.net.response.NotPaidReasonResponse
import com.wezom.taxi.driver.net.response.RefuseReasonResponse
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.presentation.customview.EtherSwitchButton
import com.wezom.taxi.driver.presentation.main.events.NewBroadcastEvent
import com.wezom.taxi.driver.service.TaxometerService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.lang.ref.WeakReference
import javax.inject.Inject

/**
 * Created by udovik.s on 26.03.2018.
 */
class RefuseOrderViewModel @Inject constructor(screenRouterManager: ScreenRouterManager,
                                               private val apiManager: ApiManager,
                                               private val dbManager: DbManager,
                                               private val orderManager: OrderManager) : BaseViewModel(screenRouterManager) {
    @SuppressLint("CheckResult")
    fun getRefuseReasons() {
        dbManager.clearReasonsTable()
        App.instance.getLogger()!!.log("getNotPaidReasons start ")
        apiManager.getNotPaidReasons()
                .flatMap { notPaidReasonResponse: NotPaidReasonResponse ->
                    if (notPaidReasonResponse.isSuccess!!) {
                        Timber.e("NOT_PAID_REASONS: ${notPaidReasonResponse.reasons}")
                        val mutable = notPaidReasonResponse.reasons
                        mutable.map { reason -> reason.orderStatusToCorrespondFor = OrderStatus.NOT_COMPLETED }
                        dbManager.insertReasons(mutable)
                        Timber.d("insert getNotPaidReasons reasons: $mutable")
                    }
                    return@flatMap apiManager.refuseReasons(OrderStatus.ACCEPTED.value)
                }
                .flatMap { refuserReasonReaponse1: RefuseReasonResponse ->
                    if (refuserReasonReaponse1.isSuccess!!) {
                        val mutable = refuserReasonReaponse1.reasons
                        mutable.map { reason -> reason.orderStatusToCorrespondFor = OrderStatus.ACCEPTED }
                        dbManager.insertReasons(mutable)
                        Timber.d("insert reasons: $mutable")
                    }
                    return@flatMap apiManager.refuseReasons(OrderStatus.ARRIVED_AT_PLACE.value)
                }
                .flatMap { refuserReasonReaponse2: RefuseReasonResponse ->
                    if (refuserReasonReaponse2.isSuccess!!) {
                        val mutable = refuserReasonReaponse2.reasons
                        mutable.map { reason -> reason.orderStatusToCorrespondFor = OrderStatus.ARRIVED_AT_PLACE }
                        dbManager.insertReasons(mutable)
                        Timber.d("insert reasons: $mutable")
                    }
                    return@flatMap apiManager.refuseReasons(OrderStatus.ON_THE_WAY.value)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    App.instance.getLogger()!!.log("getNotPaidReasons suc ")
                    if (response.isSuccess!!) {
                        val mutable = response.reasons
                        mutable.map { reason -> reason.orderStatusToCorrespondFor = OrderStatus.ON_THE_WAY }
                        dbManager.insertReasons(mutable)
                        Timber.d("insert reasons: $mutable")
                    }
                }, {
                    App.instance.getLogger()!!.log("getNotPaidReasons error ")
                    Timber.e(it)
                })

    }

    fun getReason(): List<Reason> {
        return dbManager.queryAllReasons()
    }

    val refuseReasonsLiveData = MutableLiveData<List<Reason>>()

    fun getRefuseReasons(orderStatus: OrderStatus, allReasons: Map<OrderStatus, List<Reason>>) {
        val reasons = allReasons[orderStatus]
        refuseReasonsLiveData.postValue(reasons)
    }


    fun refuseOrder(id: Int,
                    reasonId: Int? = 0,
                    comment: String? = null,
                    status: Int? = null,
                    factCost: Int?,
                    actualCost: Int?,
                    factTime: Long?,
                    factPath: Double?,
                    eventTime: Long?,
                    route: List<Coordinate>?) {
        val request = RefuseOrderRequest(reasonId, comment, status, factCost, actualCost, factTime, factPath?.roundTwoDecimal(), eventTime, route)
        JobFactory.instance.getJobManager()?.addJobInBackground(RefuseOrderJob(id, request))

        if (reasonId == 7) {
            RxBus.publish(NewBroadcastEvent(NewOrderInfo(0, false, 0, null, null)))
            EtherSwitchButton.cache += 1
        }

        if (id == orderManager.primaryOrderId()) {
            TaxometerService.stop(WeakReference(context))
            orderManager.finishOrder()
        } else {
            orderManager.deleteSecondaryOrder()
        }

        if (orderManager.isPrimaryOrderExist()) {
            setRootScreen(EXECUTE_ORDER_SCREEN, orderManager.getPrimaryOrder())
        } else {
            enterAsActiveUser()
        }
    }
}