package com.wezom.taxi.driver.presentation.changephoneverification

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.text.InputFilter
import android.view.*
import android.widget.EditText
import com.jakewharton.rxbinding2.widget.RxTextView
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.databinding.ChangePhoneVerificationBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.ext.hideKeyboard
import com.wezom.taxi.driver.ext.setVisible
import com.wezom.taxi.driver.ext.showKeyboard
import com.wezom.taxi.driver.presentation.base.BaseFragment
import io.reactivex.rxkotlin.plusAssign

/**
 * Created by udovik.s on 17.04.2018.
 */
class ChangePhoneVerificationFragment : BaseFragment() {

    //region var
    private lateinit var binding: ChangePhoneVerificationBinding
    private lateinit var viewModel: ChangePhoneVerificationViewModel

    private var phone: String? = null
    //endregion

    //region override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
        viewModel.loadingLiveData.observe(this, progressObserver)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = ChangePhoneVerificationBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING)

        phone = arguments?.getString(PHONE_KEY)

        binding.run {
            phoneInput.run {
                isHorizontalFadingEdgeEnabled = false
                isHorizontalScrollBarEnabled = false
                setOnClickListener {
                    handleInputAction()
                }
            }

            phoneInput.setOnFocusChangeListener { _, focused ->
                if (focused) handleInputAction()
            }

            phoneInputLayout.setOnClickListener {
                handleInputAction()
            }
            backFake.setOnClickListener({
                viewModel.onBackPressed()
            })

            setServiceCodeListeners()

            next.setOnClickListener {
                handleInputAction()
            }
            smsCodeFirst.requestFocus()
            activity.showKeyboard()
            resendSms.setVisible(false)
            viewModel.launchSmsDelayTimer()

            resendSms.setOnClickListener {
                viewModel.launchSmsDelayTimer()
                //Todo use later
                /* viewModel.sendSmsCode(
                         phone!!,
                         getCompleteCode(smsCodeFirst, smsCodeSecond, smsCodeThird, smsCodeFourth)
                 )*/
            }

            viewModel.smsDelay.observe(this@ChangePhoneVerificationFragment, Observer<Int> {
                it?.let {
                    if (it == 0) {
                        resendIn.visibility = View.INVISIBLE
                        resendSms.visibility = View.VISIBLE
                    } else {
                        resendSms.visibility = View.INVISIBLE
                        resendIn.run {
                            if (visibility != View.VISIBLE) {
                                visibility = View.VISIBLE
                            }
                            text = getString(R.string.resend_in, it.toTime())
                        }
                    }
                }
            })
            setServiceCodeListeners()
        }
    }
    //endregion

    //region fun
    private fun handleInputAction() {
        binding.run {

            if (validateSmsCodeInput()) {
                activity.hideKeyboard()
                viewModel.sendSmsCode(
                        phone!!,
                        getCompleteCode(
                                smsCodeFirst,
                                smsCodeSecond,
                                smsCodeThird,
                                smsCodeFourth
                        )
                )
            } else {

            }
        }
    }

/*    private fun applyToEach(action: (EditText) -> Unit, vararg edits: EditText) {
        edits.forEach {
            action(it)
        }
    }

    private fun clearSmsCodeErrors() {
        binding.run {
            applyToEach(
                    { it.error = null },
                    smsCodeFirst,
                    smsCodeSecond,
                    smsCodeThird,
                    smsCodeFourth
            )
        }
    }

    private fun clearSmsCodeInputs() {
        binding.run {
            applyToEach(
                    { it.setText("") },
                    smsCodeFirst,
                    smsCodeSecond,
                    smsCodeThird,
                    smsCodeFourth
            )
        }
    }*/

    private fun getCompleteCode(vararg edits: EditText): Int {
        val builder = StringBuilder()
        edits.forEach {
            builder.append(it.text.toString())
        }

        return builder.toString().toInt()
    }

    private fun setServiceCodeListeners() {
        binding.run {
            disposable += RxTextView
                    .textChanges(smsCodeFirst)
                    .subscribe { s: CharSequence ->
                        if (s.isNotEmpty())
                            binding.smsCodeSecond.requestFocus()
                    }
            disposable += RxTextView
                    .textChanges(smsCodeSecond)
                    .subscribe { s: CharSequence ->
                        if (s.isNotEmpty())
                            smsCodeThird.requestFocus()
                    }
            disposable += RxTextView
                    .textChanges(smsCodeThird)
                    .subscribe { s: CharSequence ->
                        if (s.isNotEmpty())
                            smsCodeFourth.requestFocus()
                    }
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            setupInputFilters()
        else
            setupKeyListeners()
    }

    private fun setupInputFilters() = binding.run {
        smsCodeSecond.filters =
                arrayOf(InputFilter { source, _, _, _, _, dend ->
                    if (dend == 0 && source.isEmpty()) {
                        smsCodeFirst.requestFocus()
                    }

                    source
                }, InputFilter.LengthFilter(1))

        smsCodeThird.filters =
                arrayOf(InputFilter { source, _, _, _, _, dend ->
                    if (dend == 0 && source.isEmpty()) {
                        smsCodeSecond.requestFocus()
                    }

                    source
                }, InputFilter.LengthFilter(1))

        smsCodeFourth.filters =
                arrayOf(InputFilter { source, _, _, _, _, dend ->
                    if (dend == 0 && source.isEmpty()) {
                        smsCodeThird.requestFocus()
                    }

                    source
                }, InputFilter.LengthFilter(1))
    }

    @RequiresApi(23)
    private fun setupKeyListeners() = binding.run {
        smsCodeSecond.setOnKeyListener { v, keyCode, event ->
            handleBackspace(v, keyCode, event, smsCodeFirst)
        }

        smsCodeThird.setOnKeyListener { v, keyCode, event ->
            handleBackspace(v, keyCode, event, smsCodeSecond)
        }

        smsCodeFourth.setOnKeyListener { v, keyCode, event ->
            handleBackspace(v, keyCode, event, smsCodeThird)
        }
    }

    private fun handleBackspace(
            v: View,
            keyCode: Int,
            event: KeyEvent,
            nextToReceiveFocus: EditText
    ): Boolean {
        if (keyCode == KeyEvent.KEYCODE_DEL
                && ((v as EditText).length() == 0)
                && event.action == KeyEvent.ACTION_DOWN) nextToReceiveFocus.requestFocus()
        return false
    }

    private fun validateSmsCodeInput(): Boolean {
        binding.run {
            val first = isNotEmptyInput(smsCodeFirst)
            val second = isNotEmptyInput(smsCodeSecond)
            val third = isNotEmptyInput(smsCodeThird)
            val fourth = isNotEmptyInput(smsCodeFourth)
            return first && second && third && fourth
        }
    }

    private fun isNotEmptyInput(input: EditText): Boolean {
        if (input.text.isEmpty()) {
            input.error = getString(R.string.invalid_code)
            return false
        }

        return true
    }

    //endregion

    companion object {

        private const val PHONE_KEY = "phone_key"

        fun newInstance(phone: String): ChangePhoneVerificationFragment {
            val fragment = ChangePhoneVerificationFragment()
            val arguments = Bundle()
            arguments.putString(PHONE_KEY, phone)
            fragment.arguments = arguments
            return fragment
        }
    }
}

private fun Int.toTime(): String {
    val minutes = this / 60
    val seconds = this - minutes * 60

    return if (seconds < 10) "0" + minutes.toString() + ":" + "0" + seconds
    else "0" + minutes.toString() + ":" + seconds
}