package com.wezom.taxi.driver.presentation.filter.base

import android.widget.Switch
import com.wezom.taxi.driver.net.response.FilterResponse

class FilterSwitch(var filter: FilterResponse, var switch: Switch)