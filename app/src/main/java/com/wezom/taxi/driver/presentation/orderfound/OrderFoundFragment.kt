package com.wezom.taxi.driver.presentation.orderfound

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.jakewharton.rxbinding2.view.RxView
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.bus.events.AutoCloseOrderEvent
import com.wezom.taxi.driver.common.calcTimerTime
import com.wezom.taxi.driver.common.formatTime
import com.wezom.taxi.driver.common.loadRoundedImage
import com.wezom.taxi.driver.data.models.NewOrderInfo
import com.wezom.taxi.driver.data.models.PaymentMethod
import com.wezom.taxi.driver.databinding.OrderFoundBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.ext.longToast
import com.wezom.taxi.driver.ext.setVisible
import com.wezom.taxi.driver.presentation.acceptorder.AcceptOrderFragment
import com.wezom.taxi.driver.presentation.acceptorder.dialog.PreliminaryTimeDialog
import com.wezom.taxi.driver.presentation.acceptorder.list.AddressAdapter
import com.wezom.taxi.driver.presentation.base.BaseMapFragment
import com.wezom.taxi.driver.presentation.customview.LinearProgressBar
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber
import java.text.DecimalFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.math.roundToInt

/**
 * Created by zorin.a on 07.03.2018.
 */
class OrderFoundFragment : BaseMapFragment() {

    @Inject
    lateinit var adapter: AddressAdapter
    //region var
    lateinit var binding: OrderFoundBinding
    lateinit var viewModel: OrderFoundViewModel
    lateinit var orderInfo: NewOrderInfo

    private var orderAddressCoordinates: MutableList<LatLng> = mutableListOf()
    private var isAccepted: Boolean = false
    private var isCanceled: Boolean = false
    //endregion

    //region override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        orderInfo = arguments!!.getParcelable(DATA_KEY)
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()

        orderInfo.order!!.coordinates?.let {
            it.forEach {
                orderAddressCoordinates.add(LatLng(it.latitude!!, it.longitude!!))
            }
        }
        viewModel.getRefuseReasons()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = OrderFoundBinding.inflate(inflater)
        initRecyclerView()
        return binding.root
    }

    private fun initRecyclerView() {
        val order = orderInfo.order
        order?.coordinates?.let {
            val adapterData = Pair(it.toMutableList(),
                    AddressAdapter.FromToDistance(from = DecimalFormat("#0.0").format(order.distance!!).replace(",", "."),
                            to = order.calculatedPath.toString()))
            adapter.addAll(adapterData)
        }
        binding.addressList.adapter = adapter
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        clicks()
        initListeners()

        viewModel.isMapWhiteThemeLiveData.observe(this, Observer { isMapWhite ->
            setMapStyle(isMapWhite)
        })
        viewModel.routeLiveData.observe(this, Observer {
            drawRoute(it)
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.playSound()
    }

    override fun onPause() {
        super.onPause()
        viewModel.stopSound()
    }

    override fun onMapReady(map: GoogleMap?) {
        super.onMapReady(map)
        viewModel.setMapStyle()
        map?.setOnMapLoadedCallback { handleCamera() }
    }

    //endregion

    //region fun

    @SuppressLint("SetTextI18n")
    private fun initViews() {
        binding.run {
            //            viewsVisibilitySwitch(orderInfo.order?.newOrderShowing?.isRatingShow!!, ratingImage, ratingLabel)
//            val ratingValue = orderInfo.order?.user?.rating
//            if (ratingValue == null || ratingValue == 0.0f) {
//                viewsVisibilitySwitch(false, ratingImage, ratingLabel)
//            } else {
//                ratingLabel.text = ratingValue.toString()
//            }
//
//            if (orderInfo.order?.carArrival == 0) {
//                timeTextView.setVisible(false)
//            } else {
//                timeTextView.setVisible(true)
//                orderInfo.order?.preliminaryTime?.let {
//                    timeTextView.setTime(formatTime(it))
//                }
//            }
//
//
//            viewsVisibilitySwitch(orderInfo.order?.newOrderShowing?.isCurrentCoefficientShow!!, coefficientContainer)
//
//            val coef = orderInfo.order?.currentCoefficient!!
//            if (coef <= 1.0) {
//                viewsVisibilitySwitch(false, coefficientContainer)
//            }
//
//            coefficientValue.text = coef.toString()
//            val kmPrice = orderInfo.order?.kilometerPrice
//            rateValue.text = kmPrice?.roundToInt().toString()
//            viewsVisibilitySwitch(orderInfo.order?.newOrderShowing?.isKilometerPriceShow!!, rateValue, rateMeasureLabel)
//
//            if (kmPrice == null || kmPrice <= 0.0) {
//                viewsVisibilitySwitch(false, rateValue, rateMeasureLabel)
//            }
//
//            var estCostValue: Double = orderInfo.order?.payment?.estimatedCost?.plus(orderInfo.order?.payment?.surcharge!!)
//                    ?: 0.0
//            val estBonus: Double = orderInfo.formula?.estimatedBonuses ?: 0.0
//            estCostValue += estBonus
//            viewsVisibilitySwitch(estCostValue!! > 0, priceValue, payMethodImage, uahCurrencyImage)
//            priceValue.text = Math.round(estCostValue).toString()
//            viewsVisibilitySwitch(orderInfo.order?.newOrderShowing?.isEstimatedCostShow!!, priceValue, payMethodImage, uahCurrencyImage)
//
//            Timber.d("ESTIMATE_estimatedBonuses: $estBonus")
//            Timber.d("ESTIMATE_estimatedCost: ${orderInfo.order?.payment?.estimatedCost}")
//            Timber.d("ESTIMATE_surcharge: ${orderInfo.order?.payment?.surcharge!!}")
//
//            payMethodImage.setImageResource(getPaymentResource(orderInfo.order?.payment?.method!!))
//
//
//            viewsVisibilitySwitch(orderInfo.order?.newOrderShowing?.isServicesShow!!, servicesContainer)
//            if (orderInfo.order?.newOrderShowing?.isServicesShow!! && orderInfo.order?.services?.isEmpty() == false) {
//                services.setActiveStatus(true)
//                services.addServices(*orderInfo.order?.services?.toIntArray() ?: intArrayOf())
//            } else {
//                servicesContainer.setVisible(false)
//            }
//
//            viewsVisibilitySwitch((orderInfo.order?.newOrderShowing?.isRatingShow!! or
//                    orderInfo.order?.newOrderShowing?.isCurrentCoefficientShow!! or
//                    orderInfo.order?.newOrderShowing?.isKilometerPriceShow!! or
//                    orderInfo.order?.newOrderShowing?.isMethodShow!! or
//                    orderInfo.order?.newOrderShowing?.isEstimatedCostShow!!), topContainer)

            if (orderInfo.order?.coordinates!!.size > 1) {
                var estCostValue: Double =
                        orderInfo.order?.payment?.estimatedCost?.plus(orderInfo.order?.payment?.surcharge!!)
                                ?: 0.0
                val estBonus: Double = orderInfo.formula?.estimatedBonuses ?: 0.0
                estCostValue += estBonus
                priceValue.text = estCostValue.roundToInt()
                        .toString()
            } else {
                rateValue.visibility = View.GONE
                rateMeasureLabel.visibility = View.GONE
                priceValue.text = context!!.resources.getString(R.string.auto_check)
            }

            orderInfo.order?.payment?.method?.let { com.wezom.taxi.driver.common.getPaymentResource(it) }
                    ?.let { payMethodImage?.setImageResource(it) }

            orderInfo.order?.user?.rating?.let {
                ratingLabel.text = it.toString()
            }


            userImage.let {
                loadRoundedImage(context = context!!,
                        view = it,
                        url = orderInfo.order?.user?.photo,
                        placeHolderId = R.drawable.ic_ava_vector40px)
            }
//            userImage.setOnClickListener {
//                orderInfo.order?.user?.let { it1 -> viewModel.showUser(it1) }
//            }

            positiveDrivesLabel.text =
                    orderInfo.order?.user?.doneOrdersCount.toString()
            negativeDrivesLabel.text =
                    orderInfo.order?.user?.undoneOrdersCount.toString()

            if (!orderInfo.order?.user?.name.isNullOrEmpty()) {
                userName.text = orderInfo.order?.user?.name
            } else {
                userName.text = context!!.getString(R.string.name_client)
            }

            orderInfo.order?.services?.let {
                if (it.isNotEmpty() && AcceptOrderFragment.checkService(it)) {
                    services.setActiveStatus(true)
                    services.addServices(*it.toIntArray())
                    services.setVisible(true)
                    servicesLabel.setVisible(true)
                } else {
                    servicesContainer.setVisible(false)
                }
            }

            if (orderInfo.order?.carArrival == 0) {
                timeTextView.setVisible(false)
            } else {
                orderInfo.order?.preliminaryTime?.let {
                    //set time
                    val time = formatTime(it)
                    Timber.d("TIME_: $time")
                    timeTextView.setTime(time)
                    timeTextView?.setOnClickListener {
                        val dialog = PreliminaryTimeDialog.newInstance(time)
                        dialog.show(fragmentManager,
                                PreliminaryTimeDialog.TAG)
                    }
                    servicesContainer.setVisible(true)
                }
            }
            if (!orderInfo.order?.comment.isNullOrEmpty()) {
                comment.text = orderInfo.order?.comment
            } else {
                comment.setVisible(false)
            }
            if (orderInfo.order?.kilometerPrice != 0.0) {
                rateValue.text = orderInfo.order?.kilometerPrice?.let { if (it != 0.0) it.roundToInt().toString() else it.toString() }
            } else {
                rateValue.setVisible(false)
                rateMeasureLabel.setVisible(false)
            }

            val answerTimeMillis = orderInfo.order?.answerTime!! //~15 sec default
            Timber.d("_TIME answerTime: ${Date(answerTimeMillis)}")
            val serverTimeMillis = orderInfo.order?.serverTime!!
            Timber.d("_TIME serverTime: ${Date(serverTimeMillis)}")
            val timerTime = calcTimerTime(serverTimeMillis, answerTimeMillis)
            Timber.d("_TIME timer time millis: $timerTime")
            Timber.d("_TIME timer time seconds: ${TimeUnit.MILLISECONDS.toSeconds(timerTime)}")

            progress.startTimer(timerTime)
            progress.listener = object : LinearProgressBar.DoneListener {
                override fun onComplete() {
                    progress.stopTimer()
                    viewModel.onBackPressed()
                }
            }
        }
    }

    private fun handleCamera() {
        if (orderAddressCoordinates.isNotEmpty()) {
            disposable += Single.timer(1, TimeUnit.SECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        updateOrderMarkers(orderAddressCoordinates)
                        updateDriverMarker()
                        updateMultipleTargetCamera(lastLocation, orderAddressCoordinates)
                        viewModel.getMapRoute(orderAddressCoordinates)
                    }, Timber::e)
        } else {
            updateDriverMarker()
            updateDriverCamera()
        }
    }

    private fun getPaymentResource(payment: PaymentMethod): Int {
        return when (payment) {
            PaymentMethod.CASH -> R.drawable.ic_cash
            PaymentMethod.CASH_AND_BONUS -> R.drawable.ic_cash
            PaymentMethod.CARD -> R.drawable.ic_card
            PaymentMethod.CARD_AND_BONUS -> R.drawable.ic_card
        }
    }

    private fun initListeners() {
        disposable += RxBus.listen(AutoCloseOrderEvent::class.java)
                .subscribe {
                    isAccepted = false
                    getString(R.string.internet_error).longToast(context)
                }
    }

    private fun clicks() {
        disposable += RxView.clicks(binding.skipButton).subscribe {
            if (!isCanceled) {
                viewModel.exitOrder(orderInfo.order!!.id!!)
                isCanceled = true
            }
        }

        disposable += RxView.clicks(binding.acceptButton).subscribe {
            if (!isAccepted) {
                viewModel.acceptOrder(orderInfo, lastLocation?.longitude, lastLocation?.latitude)
                isAccepted = true
            }
        }
    }
//endregion

    companion object {
        const val DATA_KEY = "data_key"
        fun newInstance(orderInfo: NewOrderInfo): OrderFoundFragment {
            val args = Bundle()
            args.putParcelable(DATA_KEY, orderInfo)
            val fragment = OrderFoundFragment()
            fragment.arguments = args
            return fragment
        }
    }
}