package com.wezom.taxi.driver.presentation.base.dialog

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.widget.ArrayAdapter
import com.wezom.taxi.driver.R

/**
 * Created by zorin.a on 30.05.2018.
 */
class MessengerChooser : DialogFragment() {
    var listener: ChoiceDialogSelection? = null
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val stringData: List<String> = listOf("SMS", "Viber")
        val adapter = ArrayAdapter<CharSequence>(
                activity, R.layout.custom_list_item_single_choice, stringData)

        return AlertDialog.Builder(context!!)
                .setSingleChoiceItems(adapter, -1, { _, pos ->
                    when (pos) {
                        0 -> {
                            listener?.onSelected(MessengerType.SMS)
                        }
                        1 -> {
                            listener?.onSelected(MessengerType.VIBER)
                        }
                    }
                    this.dismiss()
                })
                .create()
    }

    interface ChoiceDialogSelection {
        fun onSelected(type: MessengerType)
    }

    enum class MessengerType { VIBER, SMS }
    companion object {
        const val TAG = "MessengerChooser"
    }
}