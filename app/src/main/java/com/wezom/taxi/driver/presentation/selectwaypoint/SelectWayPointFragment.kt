package com.wezom.taxi.driver.presentation.selectwaypoint

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.common.Constants.POINT_RESULT_CODE
import com.wezom.taxi.driver.data.models.Order
import com.wezom.taxi.driver.databinding.SelectWayPointBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.presentation.base.BaseFragment

/**
 * Created by udovik.s on 24.04.2018.
 */
class SelectWayPointFragment : BaseFragment() {

    //region var
    private lateinit var binding: SelectWayPointBinding
    private lateinit var viewModel: SelectWayPointViewModel
    private lateinit var order: Order
    //endregion

    //region override

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setBackPressFun { viewModel.onBackPressed() }
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
        order = arguments!!.getParcelable(ORDER_KEY)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = SelectWayPointBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateUi(order)
        binding.run {
            toolbar.setToolbarTitle(getString(R.string.select_way_point))

            binding.wayPointsList.setOnCheckedChangeListener({ _, checkedId ->
                viewModel.backToOrder(POINT_RESULT_CODE, checkedId)
            })
        }
    }
    //endregion

    //region fun
    private fun updateUi(order: Order) {
        order.coordinates!!.forEachIndexed { index, value ->
            binding.run {
                if (index != 0) {
                    val params: RadioGroup.LayoutParams = RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT)
                    val radioButton = activity!!.layoutInflater.inflate(
                            R.layout.item_radio_button_way_point,
                            null
                    ) as RadioButton
                    radioButton.layoutParams = params
                    radioButton.text = value.name
                    radioButton.id = index
                    wayPointsList.addView(radioButton)
                }
            }
        }
    }

    //endregion

    companion object {
        private const val ORDER_KEY = "order_key"
        fun newInstance(currentOrder: Order): SelectWayPointFragment {
            val args = Bundle()
            args.putParcelable(ORDER_KEY, currentOrder)
            val fragment = SelectWayPointFragment()
            fragment.arguments = args
            return fragment
        }
    }
}