package com.wezom.taxi.driver.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by udovik.s on 19.03.2018.
 */
data class Shares(
        @SerializedName("id") val id: Int?,
        @SerializedName("name") val name: String?,
        @SerializedName("description") val description: String?,
        @SerializedName("image") val image: String?,
        @SerializedName("url") val url: String?,
        @SerializedName("date") val date: Long?
)