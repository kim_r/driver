package com.wezom.taxi.driver.presentation.base

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import com.wezom.taxi.driver.bus.events.ResultDataEvent
import com.wezom.taxi.driver.common.EXECUTE_ORDER_SCREEN
import com.wezom.taxi.driver.common.ErrorHandler
import com.wezom.taxi.driver.common.MAIN_MAP_SCREEN
import com.wezom.taxi.driver.common.TOTAL_COST_SCREEN
import com.wezom.taxi.driver.data.models.*
import com.wezom.taxi.driver.net.response.BaseResponse
import com.wezom.taxi.driver.presentation.base.route.Routable
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.service.TaxometerService
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import java.lang.ref.WeakReference
import javax.inject.Inject
import kotlin.math.roundToInt

/**
 * Created by zorin.a on 16.02.2018.
 */

abstract class BaseViewModel(screenRouterManager: ScreenRouterManager) : ViewModel(), Routable by screenRouterManager {
    var disposable: CompositeDisposable = CompositeDisposable()
    var loadingLiveData = MutableLiveData<ResponseState>()
    val drawerLockLiveData = MutableLiveData<Boolean>()

    @Inject
    lateinit var errorHandler: ErrorHandler

    @Inject
    lateinit var context: Context

    protected fun handleResponseState(response: BaseResponse) {
        response.isSuccess?.let { isSuccess ->
            if (isSuccess) {
                loadingLiveData.postValue(ResponseState(ResponseState.State.IDLE))
            } else {
                loadingLiveData.postValue(ResponseState(ResponseState.State.ERROR, message = response.error?.message))
                errorHandler.onError(response.error!!.code!!, response.error!!.message)
            }
        }
    }

    protected open fun setOffline() {

    }

    protected fun setAppState(driverStatus: DriverStatus, modStatus: ModerationStatus, workWithoutModeration: Boolean): AppState {
        return when (driverStatus) {
            DriverStatus.NOT_ACTIVE -> {
                AppState.NOT_ACTIVE
            }

            DriverStatus.ACTIVE -> {
                var state: AppState = AppState.ACTIVE_ONLINE

                if (modStatus == ModerationStatus.NOT_VERIFIED) {
                    state = if (workWithoutModeration) {
                        AppState.ACTIVE_ONLINE
                    } else {
                        AppState.ACTIVE_OFFLINE
                    }
                }

                if (modStatus == ModerationStatus.VERIFIED) {
                    state = AppState.ACTIVE_ONLINE
                }

                if (modStatus == ModerationStatus.MODERATION) {
                    state = if (workWithoutModeration) {
                        AppState.ACTIVE_ONLINE
                    } else {
                        AppState.MODERATION
                    }
                }
                state
            }

            DriverStatus.NEW_DRIVER -> {
                AppState.NEW
            }

            DriverStatus.BLOCKED -> {
                AppState.BLOCKED
            }
            DriverStatus.NEED_COMPLETE -> {
                var state: AppState
                state = if (modStatus == ModerationStatus.MODERATION) {
                    AppState.MODERATION
                } else {
                    AppState.NEED_COMPLETE
                }
                state
            }
            DriverStatus.LIMITED -> {
                AppState.LIMITED
            }
        }


    }

    fun enterAsActiveUser() {
        Timber.d("enterAsActiveUser")
        drawerLockLiveData.postValue(false)
        setRootScreen(MAIN_MAP_SCREEN)
    }

    override fun onCleared() {
        disposable.dispose()
        super.onCleared()
    }

    fun restoreOrder(orderInfo: NewOrderInfo, restoredData: RestoredData? = null, resultDataEvent: OrderResultData? = null) {
        drawerLockLiveData.postValue(false)
        orderInfo.id = orderInfo.order!!.id!!

        if (orderInfo.order?.status == OrderStatus.TOTAL_COST) {
            val rde: ResultDataEvent
            if (resultDataEvent != null) {
                rde = resultDataEvent.resultData
            } else {
                rde = ResultDataEvent(
                        resultCost = restoredData?.actualCost ?: 0,
                        factCost = (restoredData?.actualCost!! - orderInfo.formula!!.bonuses!!).toInt(),
                        resultF3 = restoredData?.resultF3 ?: 0,
                        bonus = orderInfo.formula!!.bonuses?.roundToInt(),
                        estimatedBonus = orderInfo.formula!!.estimatedBonuses?.roundToInt() ?: 0,
                        isEstimatedCostTaken = restoredData?.isEstimatedCostTaken,
                        estimatedCost = orderInfo?.order?.payment?.estimatedCost ?: 0,
                        surcharge = orderInfo.order!!.payment?.surcharge!!.roundToInt(),
                        paymentMethod = orderInfo.order!!.payment!!.method,
                        changedParams = restoredData.changedParams ?: ChangedParams.NO_CHANGES,
                        user = orderInfo.order!!.user,
                        address = orderInfo.order!!.coordinates,
                        factTime = 0, //not in use
                        factPath = 0.0,//not in use
                        currentCoordinate = null,
                        passedRoute = null,
                        orderInfo = orderInfo)
            }
            setRootScreen(TOTAL_COST_SCREEN, rde)
        } else {
            if (orderInfo.order?.status == OrderStatus.NEW || orderInfo.order!!.status == OrderStatus.DIRECTED) {
                orderInfo.order?.status = OrderStatus.ACCEPTED
            }
            if (orderInfo.order?.status == OrderStatus.ON_THE_WAY || orderInfo.order!!.status == OrderStatus.ARRIVED_AT_PLACE) {
//                WeakReference(context).get()!!.stopService(Intent(WeakReference(context).get(), TaxometerService::class.java))

                TaxometerService.restore(WeakReference(context), orderInfo, restoredData?.actualCost
                        ?: 0)
            }
            setRootScreen(EXECUTE_ORDER_SCREEN, orderInfo)
        }
    }
}