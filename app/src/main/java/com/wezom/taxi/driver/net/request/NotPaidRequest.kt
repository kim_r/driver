package com.wezom.taxi.driver.net.request

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by udovik.s on 27.03.2018.
 */
data class NotPaidRequest(
        @SerializedName("longitude") val longitude: Double?,
        @SerializedName("latitude") val latitude: Double?,
        @SerializedName("reasonId") val reasonId: Int?,
        @SerializedName("comment") val comment: String?,
        @SerializedName("eventTime") val eventTime: Long?) : Serializable