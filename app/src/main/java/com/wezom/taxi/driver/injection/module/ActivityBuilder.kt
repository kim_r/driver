package com.wezom.taxi.driver.injection.module

import android.support.v7.app.AppCompatActivity
import com.wezom.taxi.driver.injection.scope.ActivityScope
import com.wezom.taxi.driver.presentation.main.MainActivity
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by zorin.a on 16.02.2018.
 */

@Module
interface ActivityBuilder {

    @Binds
    @ActivityScope
    fun provideAppCompatActivity(activity: MainActivity): AppCompatActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [FragmentBuilder::class])
    fun contributeMainActivity(): MainActivity
}