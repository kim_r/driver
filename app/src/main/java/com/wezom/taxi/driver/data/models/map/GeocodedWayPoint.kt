package com.wezom.taxi.driver.data.models.map

import com.google.gson.annotations.SerializedName

/**
 * Created by zorin.a on 11.05.2018.
 */
class GeocodedWayPoint constructor(@SerializedName("geocoder_status")
                                    var geocoderStatus: String? = null,
                                   @SerializedName("place_id")
                                    var placeId: String? = null,
                                   @SerializedName("types")
                                    var types: List<String>? = null)