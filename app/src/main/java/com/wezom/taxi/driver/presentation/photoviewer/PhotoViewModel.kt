package com.wezom.taxi.driver.presentation.photoviewer

import android.net.Uri
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import javax.inject.Inject

/**
 * Created by zorin.a on 03.03.2018.
 */
class PhotoViewModel @Inject constructor(screenRouterManager: ScreenRouterManager) : BaseViewModel(screenRouterManager) {

    fun backWithResult(requestCode: Int, temp: Uri?, imageId: Int) {
        backWithResult(requestCode, Pair(temp.toString(), imageId))
    }
}