package com.wezom.taxi.driver.data.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by zorin.a on 022 22.02.18.
 */
data class Payment(
        @SerializedName("method") var method: PaymentMethod?,
        @SerializedName("discount") var discount: Double?,
        @SerializedName("isSurchargeShow") var isSurchargeShow: Boolean?,
        @SerializedName("surcharge") var surcharge: Double?,
        @SerializedName("income") var income: Double?,
        @SerializedName("actualCost") var actualCost: Double?,
        @SerializedName("commission") var commission: Double?,
        @SerializedName("estimatedCost") var estimatedCost: Int?,
        @SerializedName("estimatedCostBonusless") var estimatedCostBonusless: Int?,
        @SerializedName("card_type") var cardType: Int?,
        @SerializedName("card_number") var cardNumber: Int?,
@SerializedName("notPaidMessage") var notPaidMessage: String?
) : Parcelable {
    constructor() : this(PaymentMethod.CASH, 0.0, false, 0.0, 0.0, 0.0, 0.0,0, 0,-1,-1, null)
    constructor(source: Parcel) : this(
            source.readValue(Int::class.java.classLoader)?.let { PaymentMethod.values()[it as Int] },
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readValue(Boolean::class.java.classLoader) as Boolean?,
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeValue(method?.ordinal)
        writeValue(discount)
        writeValue(isSurchargeShow)
        writeValue(surcharge)
        writeValue(income)
        writeValue(actualCost)
        writeValue(commission)
        writeValue(estimatedCost)
        writeValue(estimatedCostBonusless)
        writeValue(cardType)
        writeValue(cardNumber)
        writeValue(notPaidMessage)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Payment> = object : Parcelable.Creator<Payment> {
            override fun createFromParcel(source: Parcel): Payment = Payment(source)
            override fun newArray(size: Int): Array<Payment?> = arrayOfNulls(size)
        }
    }
}
