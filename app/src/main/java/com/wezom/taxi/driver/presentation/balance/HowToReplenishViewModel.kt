package com.wezom.taxi.driver.presentation.balance

import android.arch.lifecycle.MutableLiveData
import android.content.SharedPreferences
import com.wezom.taxi.driver.ext.USER_BALANCE_ID
import com.wezom.taxi.driver.ext.int
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import javax.inject.Inject

/**
 * Created by zorin.a on 16.03.2018.
 */
class HowToReplenishViewModel @Inject constructor(screenRouterManager: ScreenRouterManager, sharedPreferences: SharedPreferences) : BaseViewModel(screenRouterManager) {

    private var userBalanceId: Int by sharedPreferences.int(key = USER_BALANCE_ID)

    val userBalanceIdLiveData = MutableLiveData<Int>()

    fun getBalanceId() {
        userBalanceIdLiveData.postValue(userBalanceId)
    }
}