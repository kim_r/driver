package com.wezom.taxi.driver.presentation.ether.list

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import android.content.SharedPreferences
import android.content.res.Resources
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.bus.events.AdapterScrollEvent
import com.wezom.taxi.driver.common.formatTime
import com.wezom.taxi.driver.common.getPaymentResource
import com.wezom.taxi.driver.data.models.NewOrderInfo
import com.wezom.taxi.driver.databinding.ItemEtherBinding
import com.wezom.taxi.driver.ext.SOFT_FILTER
import com.wezom.taxi.driver.ext.boolean
import com.wezom.taxi.driver.ext.setVisible
import timber.log.Timber
import java.text.DecimalFormat
import java.util.*
import javax.inject.Inject
import kotlin.math.roundToInt
import kotlin.properties.Delegates


/**
 *Created by Zorin.A on 19.June.2019.
 */
class EtherAdapter @Inject constructor(var sharedPreferences: SharedPreferences) : RecyclerView.Adapter<EtherAdapter.EtherViewHolder>() {
    var callback: ((position: Int, orderInfo: NewOrderInfo) -> Unit)? = null
    public var content: MutableList<NewOrderInfo> by Delegates.observable(mutableListOf(),
            { _, _, newValue ->
                Timber.d("ETHER_ADAPTER newValue: ${newValue.size}")
//                emptyLiveData.value =
//                        newValue.size
                countOreder(newValue)
                notifyDataSetChanged()
            })
    val emptyLiveData: MutableLiveData<Int> = MutableLiveData()

    var softFilter: Boolean by sharedPreferences.boolean(SOFT_FILTER)

    var count: Int = 0

    fun addAll(data: List<NewOrderInfo>) {
        count = 0
        if (data.isNotEmpty()) {
            var temp = data.sortedWith(Comparator { a, b ->
                when {
                    a.order!!.distanceFromDriver!! > b.order!!.distanceFromDriver!! -> 1
                    a.order!!.distanceFromDriver!! < b.order!!.distanceFromDriver!! -> -1
                    else -> 0
                }
            })
            temp = temp.toMutableList()
            for (order: NewOrderInfo in temp) {
                for (orderTemp: NewOrderInfo in data) {
                    if (order.order!!.id == orderTemp.order!!.id) {
                        if (count == 1) {
                            content.remove(order)
                            addAll(temp)
                        } else {
                            count++
                        }
                    }
                }
                count = 0
            }
            content = temp.toMutableList()
        }

    }

    fun getAllItem(): MutableList<NewOrderInfo> {
        return content
    }

    fun countOreder(orders: MutableList<NewOrderInfo>) {
        var count = 0
        if (!softFilter) {
            for (item in orders) {
                if (!item.filter) {
                    count++
                }
            }
            emptyLiveData.value =
                    count
        } else {
            emptyLiveData.value =
                    orders.size
        }

    }

    fun addItem(dataItem: NewOrderInfo) {
        Timber.d("ETHER_ADAPTER addItem: $dataItem")
        for (order in content) {
            if (order.order!!.id == dataItem.order!!.id) {
                return
            }
        }
        for (i in content.indices) {
            if (content[i].order!!.distanceFromDriver!! > dataItem.order!!.distanceFromDriver!!) {
                content.add(i, dataItem)
                notifyItemInserted(i)
                countOreder(content)
                RxBus.publish(AdapterScrollEvent(i))
                return
            }
        }
        content.add(content.size, dataItem)
        notifyItemInserted(content.size)
        countOreder(content)
        RxBus.publish(AdapterScrollEvent(content.size))
        // emptyLiveData.postValue(content.size)
    }

    fun removeItem(orderId: Int) {
        Timber.d("ETHER_ADAPTER removeItem: $orderId")
        val order = content.find { it.order!!.id == orderId }
        val index = content.indexOf(order)
        content.remove(order)
        countOreder(content)
        notifyItemRemoved(index)
//        emptyLiveData.postValue(content.size)
    }

    fun clear() {
        content.clear()
        countOreder(content)
//        emptyLiveData.postValue(content.size)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): EtherViewHolder {
        val inflater = LayoutInflater.from(parent?.context)
        val binding = ItemEtherBinding.inflate(inflater,
                parent,
                false)
        return EtherViewHolder(binding, sharedPreferences)
    }

    override fun getItemCount(): Int = content.size

    override fun onBindViewHolder(holder: EtherViewHolder,
                                  position: Int) {
        holder?.bind(content[position])
        holder?.callback = callback
    }

    class EtherViewHolder(private val binding: ItemEtherBinding, var sharedPreferences: SharedPreferences) : RecyclerView.ViewHolder(binding.root) {
        var callback: ((position: Int, orderInfo: NewOrderInfo) -> Unit)? = null
        var softFilter: Boolean by sharedPreferences.boolean(SOFT_FILTER)
        @SuppressLint("SetTextI18n")
        fun bind(dataItem: NewOrderInfo) {
            binding.run {
                if (dataItem.filter && !softFilter) {
                    rootInfo.visibility = View.GONE
                } else if (dataItem.filter && softFilter) {
                    filter.visibility = View.VISIBLE
                } else {
                    rootInfo.visibility = View.VISIBLE
                    filter.visibility = View.GONE
                }
                rootInfo.setOnClickListener {
                    callback?.invoke(layoutPosition,
                            dataItem)
                }
                if (dataItem.order?.coordinates != null) {
                    count.setVisible(dataItem.order?.coordinates?.size!! > 2)
                    count.run {
                        val addressCount = dataItem.order?.coordinates?.size!!
                        if (addressCount > 2) {
                            setVisible(true)
                            text = (addressCount - 2).toString()
                        } else {
                            setVisible(false)
                        }
                    }
                }
                fromDistanceValue.text = dataItem.order?.distance.toString()

                val entrance = dataItem.order?.coordinates!![0].entrance
                if (!entrance.isNullOrEmpty() && !dataItem.order?.coordinates!![0].name.equals(entrance)) {
                    fromText.text =
                            "${dataItem.order?.coordinates!![0].name}, п $entrance" //V - значит вендетта, п - значит ПОДЪЕЗД
                } else {
                    fromText.text = "${dataItem.order?.coordinates!![0].name}"
                }

                val addressess = dataItem.order?.coordinates
                if (addressess!!.size > 1) {
                    val lastAddress =
                            dataItem.order?.coordinates!!.sortedWith(compareBy { it.number })
                                    .last()

                    if (!lastAddress.entrance.isNullOrEmpty() && !lastAddress.name.equals(lastAddress.entrance)) {
                        toText.text = "${lastAddress.name}, ${lastAddress.entrance}"
                    } else {
                        toText.text = "${lastAddress.name}"
                    }
                    rateValue.setVisible(true)
                    rateMeasureLabel.setVisible(true)
                    toDistanceValue.setVisible(true)
                    toDistanceMeasure.setVisible(true)
                    toArrowImage.setVisible(true)
                } else {
                    toText.setText(com.wezom.taxi.driver.R.string.around_town)
                    rateValue.setVisible(false)
                    rateMeasureLabel.setVisible(false)
                    toDistanceValue.setVisible(false)
                    toDistanceMeasure.setVisible(false)
                    toArrowImage.setVisible(false)
                }

                toDistanceValue.text = dataItem.order?.calculatedPath.toString()

                if (dataItem.order?.distanceFromDriver != null) {
                    fromDistanceValue.text =
                            DecimalFormat("#0.0").format(dataItem.order?.distanceFromDriver!! / 1000).replace(",", ".")
                    fromDistanceValue.visibility = View.VISIBLE
                    fromDistanceMeasure.visibility = View.VISIBLE
                    fromArrowImage.visibility = View.VISIBLE
                } else {
                    fromDistanceValue.visibility = View.GONE
                    fromDistanceMeasure.visibility = View.GONE
                    fromArrowImage.visibility = View.GONE
                }

                dataItem.order?.payment?.method?.let { getPaymentResource(it) }
                        ?.let { payMethodImage.setImageResource(it) }

                rateValue.text = dataItem.order?.kilometerPrice?.let { if (it != 0.0) it.roundToInt().toString() else it.toString() }

                var estCostValue: Double =
                        dataItem.order?.payment?.estimatedCost?.plus(dataItem.order?.payment?.surcharge!!)
                                ?: 0.0
                if (addressess.size > 1) {
                    val estBonus: Double = dataItem.formula?.estimatedBonuses ?: 0.0
                    estCostValue += estBonus
                    priceValue.text = Math.round(estCostValue)
                            .toString()
                } else {
                    rateValue.visibility = View.GONE
                    rateMeasureLabel.visibility = View.GONE
                    priceValue.text = itemView.resources.getString(com.wezom.taxi.driver.R.string.auto_check)
                }
                val params: ConstraintLayout.LayoutParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)
                params.bottomToBottom = topContainer.id
                params.endToStart = payMethodImage.id
                params.topToTop = topContainer.id
//                if (addressess.size > 1) {
//                    params.setMargins(0, 0,
//                            pxFromDp(rateValue.resources, ((rateValue.text.toString().length - 1) * 32f)).toInt(), 0)
//                }else {
                if (Locale.getDefault().language == "uk") {
                    params.setMargins(0, 0, pxFromDp(rateValue.resources, 110f).toInt() -
                            pxFromDp(rateValue.resources, ((rateValue.text.toString().length - 1) * 10f)).toInt(), 0)
                } else {
                    params.setMargins(0, 0, (pxFromDp(rateValue.resources, 71f) -
                            pxFromDp(rateValue.resources, ((rateValue.text.toString().length - 1) * 10f))).toInt(), 0)
                }
//                }
                rateMeasureLabel.layoutParams = params
                if (dataItem.order?.carArrival == 0) {
                    timeTextView.setNowTime()
                } else {
                    dataItem.order?.preliminaryTime?.let {
                        //set time
                        val time = formatTime(it)
                        Timber.d("TIME_: $time")
                        timeTextView.setTime(formatTime(it))
                    }
                }
            }
        }

        fun dpFromPx(resources: Resources, px: Float): Float {
            return (px / resources.getDisplayMetrics().density)
        }

        fun pxFromDp(resources: Resources, dp: Float): Float {
            return (dp * resources.getDisplayMetrics().density)
        }
    }


}