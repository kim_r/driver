package com.wezom.taxi.driver.data.models

/**
 * Created by zorin.a on 27.03.2018.
 */
class DayRange constructor(var startDay: String, var startMonth: String, var endDay: String, var endMonth: String)