package com.wezom.taxi.driver.presentation.rating

import com.wezom.taxi.driver.common.EXECUTE_ORDER_SCREEN
import com.wezom.taxi.driver.common.MAIN_MAP_SCREEN
import com.wezom.taxi.driver.common.OrderManager
import com.wezom.taxi.driver.data.models.RateRequest
import com.wezom.taxi.driver.net.jobqueue.JobFactory
import com.wezom.taxi.driver.net.jobqueue.jobs.RateTripJob
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import javax.inject.Inject

/**
 * Created by udovik.s on 22.03.2018.
 */
class RatingViewModel @Inject constructor(screenRouterManager: ScreenRouterManager,
                                          private val orderManager: OrderManager) : BaseViewModel(screenRouterManager) {

    fun rateTrip(id: Int, rating: Int, comment: String?, isFromHistory: Boolean) {
        JobFactory.instance.getJobManager()?.addJobInBackground(RateTripJob(id, RateRequest(rating, comment, System.currentTimeMillis())))
        performNextAction(isFromHistory)
    }

    fun nextAction(isFromHistory: Boolean) {
        //secondary order became primary while we press finishOrder(id: Int) to not loose our state if something happens
        performNextAction(isFromHistory)
    }

    private fun performNextAction(isFromHistory: Boolean) {
//        if (orderManager.isPrimaryOrderExist()) {
//            setRootScreen(EXECUTE_ORDER_SCREEN, orderManager.getSecondaryOrder())
//            if (orderManager.isSecondaryOrderExist()) {
//                setRootScreen(EXECUTE_ORDER_SCREEN, orderManager.getSecondaryOrder())
//            } else {
//                //just continue on road
//                setRootScreen(MAIN_MAP_SCREEN)
//            }
//            orderManager.finishOrder()
//        } else {
//            onBackPressed()
//        }
        if (isFromHistory) {
            onBackPressed()
        } else {
            if (orderManager.isPrimaryOrderExist()) {
                setRootScreen(EXECUTE_ORDER_SCREEN, orderManager.getPrimaryOrder())
            } else {
                setRootScreen(MAIN_MAP_SCREEN)
            }

        }
    }
}