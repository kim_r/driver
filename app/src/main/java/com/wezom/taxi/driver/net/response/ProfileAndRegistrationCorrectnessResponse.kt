package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.*
import com.wezom.taxi.driver.data.models.User


/**
 * Created by zorin.a on 021 21.02.18.
 */
data class ProfileAndRegistrationCorrectnessResponse(@SerializedName("userCorrectness") val userCorrectness: UserRequisitesCorrectness?,
                                                     @SerializedName("carCorrectness") val carCorrectness: CarRequisitesCorrectness?,
                                                     @SerializedName("user") val user: User?,
                                                     @SerializedName("car") val car: Car?,
                                                     @SerializedName("orders_types") val orderType: ArrayList<Int>?,
                                                     @SerializedName("photos") val photos: Photos?) : BaseResponse()


