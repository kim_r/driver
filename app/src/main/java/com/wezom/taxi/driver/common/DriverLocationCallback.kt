package com.wezom.taxi.driver.common

import android.Manifest
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Looper
import android.support.v4.content.ContextCompat
import com.google.android.gms.location.*
import timber.log.Timber

/**
 * Created by zorin.a on 06.03.2018.
 */
class DriverLocationListener constructor(val context: Context,
                                         private val lifecycleOwner: LifecycleOwner,
                                         private val locationCallback: DriverLocationCallback) : LifecycleObserver {
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var callback: LocationCallback? = null

    init {
        lifecycleOwner.lifecycle.addObserver(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
        callback = object : LocationCallback() {
            override fun onLocationResult(result: LocationResult?) {
                val location = result?.lastLocation
//                Timber.d("onLocationResult location: $location")
                if (location != null) {
                    locationCallback.onLocationChanged(location)
                }
            }

            override fun onLocationAvailability(la: LocationAvailability?) {
                super.onLocationAvailability(la)
                val isAvailable = la?.isLocationAvailable
                locationCallback.onLocationAvailable(isAvailable!!)
                Timber.d("onLocationResult isAvailable: $isAvailable")
            }
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun addLocationListener() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            fusedLocationClient?.lastLocation?.apply {
                addOnSuccessListener {
                    Timber.d("addOnSuccessListener: $it")
                    if (it != null) {
                        locationCallback.onLocationChanged(it)
                    }
                }
            }

            val locationRequest = LocationRequest()
            locationRequest.interval = Constants.LOCATION_UPDATE_INTERVAL
            locationRequest.fastestInterval = Constants.LOCATION_UPDATE_INTERVAL
            locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            fusedLocationClient?.requestLocationUpdates(locationRequest, callback, Looper.getMainLooper())
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun removeLocationListener() {
        fusedLocationClient?.removeLocationUpdates(callback)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        lifecycleOwner.lifecycle.removeObserver(this)
    }

    interface DriverLocationCallback {
        fun onLocationChanged(location: Location)
        fun onLocationAvailable(isAvailable: Boolean)
    }
}

