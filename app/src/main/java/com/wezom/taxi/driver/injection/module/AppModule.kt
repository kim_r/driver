package com.wezom.taxi.driver.injection.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.wezom.taxi.driver.injection.scope.AppScope
import com.wezom.taxi.driver.service.notification.PushNotificationItemResolver
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

/**
 * Created by zorin.a on 16.02.2018.
 */

@Module
internal object AppModule {
    @AppScope
    @Provides
    @JvmStatic
    fun provideContext(application: Application): Context = application

    @AppScope
    @Provides
    @JvmStatic
    fun provideNavigatorHolder(cicerone: Cicerone<Router>): NavigatorHolder {
        val holder by lazy {
            cicerone.navigatorHolder
        }
        return holder
    }

    @AppScope
    @Provides
    @JvmStatic
    fun provideCicerone(): Cicerone<Router> {
        val cicerone by lazy {
            Cicerone.create()
        }
        return cicerone
    }

    @AppScope
    @Provides
    @JvmStatic
    fun provideRouter(cicerone: Cicerone<Router>): Router {
        val router by lazy {
            cicerone.router
        }
        return router
    }

    @AppScope
    @Provides
    @JvmStatic
    fun providePrefs(context: Context): SharedPreferences {
        val prefs by lazy {
            PreferenceManager.getDefaultSharedPreferences(context)
        }
        return prefs
    }

    @AppScope
    @Provides
    @JvmStatic
    fun providesPushNotificationItemResolver(): PushNotificationItemResolver = PushNotificationItemResolver()
}