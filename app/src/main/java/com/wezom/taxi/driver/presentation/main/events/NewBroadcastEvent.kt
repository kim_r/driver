package com.wezom.taxi.driver.presentation.main.events

import com.wezom.taxi.driver.data.models.NewOrderInfo

/**
 *Created by Zorin.A on 19.June.2019.
 */
class NewBroadcastEvent constructor( val orderInfo:NewOrderInfo)