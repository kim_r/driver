package com.wezom.taxi.driver.net.jobqueue.services

import com.birbit.android.jobqueue.JobManager
import com.birbit.android.jobqueue.scheduling.GcmJobSchedulerService
import com.wezom.taxi.driver.net.jobqueue.JobFactory

/**
 * Created by zorin.a on 23.04.2018.
 */
class GcmJobService : GcmJobSchedulerService() {
    override fun getJobManager(): JobManager = JobFactory.instance.getJobManager()!!
}