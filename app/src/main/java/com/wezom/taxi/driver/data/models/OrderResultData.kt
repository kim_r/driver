package com.wezom.taxi.driver.data.models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.wezom.taxi.driver.bus.events.ResultDataEvent


@Entity(tableName = "order_result_data")
data class OrderResultData(
        @PrimaryKey
        @ColumnInfo(name = "result_data_id") var id: Int,
        @Embedded var resultData: ResultDataEvent
)