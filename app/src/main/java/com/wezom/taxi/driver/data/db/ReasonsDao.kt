package com.wezom.taxi.driver.data.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.wezom.taxi.driver.data.models.OrderStatus
import com.wezom.taxi.driver.data.models.Reason

/**
 * Created by zorin.a on 10.07.2018.
 */
@Dao
interface ReasonsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertReasons(reasonList: List<Reason>): List<Long>

    @Query("SELECT * FROM  'order_reasons'")
    fun queryAll(): List<Reason>

    @Query("SELECT * FROM  'order_reasons' WHERE orderStatusToCorrespondFor = :status")
    fun queryReasonByStatus(status: OrderStatus): List<Reason>

    @Query("DELETE FROM 'order_reasons'")
    fun clearDatabase()
}