package com.wezom.taxi.driver.presentation.customview

import android.annotation.SuppressLint
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.data.models.Condition
import com.wezom.taxi.driver.databinding.BonusProgramDrivesBinding
import com.wezom.taxi.driver.presentation.base.lists.ItemModel

/**
 * Created by zorin.a on 3/12/19.
 */
class BonusProgramDrivesItem : ConstraintLayout, ItemModel<Condition> {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleRes: Int) : super(context, attrs, defStyleRes)

    private val binding = BonusProgramDrivesBinding.inflate(LayoutInflater.from(context), this, true)

    @SuppressLint("ResourceAsColor")
    override fun setData(data: Condition) {
        binding.run {
            cashValue.text = data.reward.toString()
            tripsStep.text = data.tripsCount.toString()

            when (data.state) {
                ConditionsView.ConditionState.HIDDEN -> {
                    tripsCompleteonIcon.visibility = View.INVISIBLE
                    conditionsDescription.visibility = View.INVISIBLE
                }
                ConditionsView.ConditionState.APPROVED -> {
                    tripsCompleteonIcon.setImageResource(R.drawable.ic_verified_bonus_condition)
                    conditionsDescription.text = context.getString(R.string.conditions_met)
                    conditionsDescription.setTextColor(ContextCompat.getColor(context, R.color.colorAccent))
                }
                ConditionsView.ConditionState.REJECTED -> {
                    tripsCompleteonIcon.setImageResource(R.drawable.ic_rejected_bonus_condition)
                    conditionsDescription.text = context.getString(R.string.conditions_not_met)
                    conditionsDescription.setTextColor(ContextCompat.getColor(context, R.color.colorRed))
                }
            }
        }
    }
}