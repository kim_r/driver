package com.wezom.taxi.driver.presentation.profile

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import android.content.SharedPreferences
import com.wezom.taxi.driver.ext.CAR_IMAGE_URI
import com.wezom.taxi.driver.common.Constants
import com.wezom.taxi.driver.common.PHOTO_VIEWER_SCREEN
import com.wezom.taxi.driver.ext.string
import com.wezom.taxi.driver.data.models.CarParameter
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.presentation.photoviewer.PhotoViewerFragment
import com.wezom.taxi.driver.repository.DriverRepository
import ru.terrakok.cicerone.result.ResultListener
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by zorin.a on 28.02.2018.
 */
class AboutCarViewModel @Inject constructor(screenRouterManager: ScreenRouterManager,
                                            private val repository: DriverRepository,
                                            sharedPreferences: SharedPreferences) : BaseViewModel(screenRouterManager) {
    private var carPhotoUriString: String by sharedPreferences.string(CAR_IMAGE_URI)

    val carBrandsLiveData = MutableLiveData<List<CarParameter>>()
    val carTypeLiveData = MutableLiveData<List<CarParameter>>()
    val carModelsLiveData = MutableLiveData<List<CarParameter>>()
    val carColorsLiveData = MutableLiveData<List<CarParameter>>()
    val carYearsLiveData = MutableLiveData<List<Int>>()

    val carAvatarLiveData = MutableLiveData<String>()
    var croppedCarImageLiveData = MutableLiveData<String>()

    val requiredDocumentLiveData = MutableLiveData<Boolean>()

    @SuppressLint("CheckResult")
    fun getCarBrands() {
        App.instance.getLogger()!!.log("carBrands start ")
        repository.carBrands().subscribe({ response ->
            App.instance.getLogger()!!.log("carBrands suc ")
            carBrandsLiveData.postValue(response.brands)
        }, {
            App.instance.getLogger()!!.log("carBrands error ")
            Timber.e(it)
        })
    }

    @SuppressLint("CheckResult")
    fun getCarModels(id: Int) {
        App.instance.getLogger()!!.log("carModels start ")
        repository.carModels(id).subscribe({ response ->
            App.instance.getLogger()!!.log("carModels suc ")
            carModelsLiveData.postValue(response.models)
        }, {
            App.instance.getLogger()!!.log("carModels error ")
            Timber.e(it) })
    }

    @SuppressLint("CheckResult")
    fun getType() {
        App.instance.getLogger()!!.log("getType start")
        repository.getType()
                .subscribe({ response ->
                    App.instance.getLogger()!!.log("getType suc")
                    carTypeLiveData.postValue(response.result)
                },
                        {
                            App.instance.getLogger()!!.log("getType error")
                            Timber.e(it) })
    }

    @SuppressLint("CheckResult")
    fun getCarColors() {
        App.instance.getLogger()!!.log("carColors start ")
        repository.carColors().subscribe({ response ->
            App.instance.getLogger()!!.log("carColors suc ")
            carColorsLiveData.postValue(response.colors)
        }, {
            App.instance.getLogger()!!.log("carColors error ")
            Timber.e(it) })
    }

    @SuppressLint("CheckResult")
    fun getCarIssueYears(modelId: Int) {
        App.instance.getLogger()!!.log("carIssueYears start ")
        repository.carIssueYears(modelId).subscribe({ response ->
            App.instance.getLogger()!!.log("carIssueYears suc ")
            carYearsLiveData.postValue(response.years)
        }, {
            App.instance.getLogger()!!.log("carIssueYears error ")
            Timber.e(it) })
    }

    fun deleteCarImage() {
        carPhotoUriString = ""
        carAvatarLiveData.postValue(carPhotoUriString)
    }

    fun saveCarImageUri(uriString: String) {
        carPhotoUriString = uriString
    }

    fun getCarImageUri() {
        carAvatarLiveData.postValue(carPhotoUriString)
    }

    fun cropImage(uri: String) {
        startScreenForResult(PHOTO_VIEWER_SCREEN, Pair(uri, PhotoViewerFragment.Companion.CropOptions.CROP_ROUNDED), ResultListener { it ->
            removeResultListener(Constants.REQUEST_IMAGE_CAPTURE)
            croppedCarImageLiveData.postValue(it as String)
        }, Constants.REQUEST_IMAGE_CAPTURE)
    }

    @SuppressLint("CheckResult")
    fun getRequiredDocs() {
        repository.getRequiredDocuments().subscribe({
            handleResponseState(it)
            if (it!!.isSuccess!!) {
                requiredDocumentLiveData.postValue(it.isCarPhotoRequired)
            }
        }, {
            Timber.e(it)
        })
    }
}