package com.wezom.taxi.driver.presentation.customview

import android.content.Context
import android.support.v7.widget.SwitchCompat
import android.util.AttributeSet
import android.view.MotionEvent

/**
 * Created by zorin.a on 27.12.2018.
 */
class OnlineStateSwitch : SwitchCompat {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, attributeSetId: Int) : super(context, attrs, attributeSetId)

    override fun onTouchEvent(ev: MotionEvent?): Boolean {
        return if (ev?.action == MotionEvent.ACTION_MOVE) {
            true
        } else
            super.onTouchEvent(ev)
    }
}