package com.wezom.taxi.driver.net.websocket.messages

/**
 * Created by zorin.a on 20.04.2018.
 */
abstract class BaseSocketMessage {
    abstract var room: String

    companion object {
        const val LOGIN_ROOM = "login"
        const val SUBMIT_COORDINATES_ROOM = "submitCoordinates"
        const val PING = "stateCheck"
    }
}
