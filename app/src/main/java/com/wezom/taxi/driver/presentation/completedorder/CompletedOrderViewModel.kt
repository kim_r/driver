package com.wezom.taxi.driver.presentation.completedorder

import android.arch.lifecycle.MutableLiveData
import android.content.SharedPreferences
import com.google.android.gms.maps.model.LatLng
import com.wezom.taxi.driver.common.RATING_SCREEN
import com.wezom.taxi.driver.data.models.ResponseState
import com.wezom.taxi.driver.data.models.ResponseState.State.*
import com.wezom.taxi.driver.data.models.SendHelpTimeType
import com.wezom.taxi.driver.data.models.Trip
import com.wezom.taxi.driver.net.api.ApiManager
import com.wezom.taxi.driver.net.jobqueue.JobFactory
import com.wezom.taxi.driver.net.jobqueue.jobs.SendHelpTimeJob
import com.wezom.taxi.driver.net.request.SendHelpTimeRequest
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseMapViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.presentation.rating.RatingFragment
import com.wezom.taxi.driver.repository.DriverRepository
import io.reactivex.rxkotlin.plusAssign
import javax.inject.Inject

/**
 * Created by andre on 15.03.2018.
 */
class CompletedOrderViewModel @Inject constructor(
        screenRouterManager: ScreenRouterManager,
        apiManager: ApiManager,
        private val repository: DriverRepository,
        sharedPreferences: SharedPreferences) : BaseMapViewModel(screenRouterManager, apiManager, sharedPreferences) {

    val orderLiveData: MutableLiveData<Trip> = MutableLiveData()

    fun getOrder(orderId: Int) {
        loadingLiveData.postValue(ResponseState(LOADING))
        App.instance.getLogger()!!.log("getCompletedOrder start ")
        disposable += repository.getCompletedOrder(orderId)
                .subscribe({ response ->
                    App.instance.getLogger()!!.log("getCompletedOrder suc ")
                    handleResponseState(response)
                    if (response?.isSuccess!!) {
                        loadingLiveData.postValue(ResponseState(IDLE))
                        orderLiveData.postValue(response.trip)
                    } else {
                        loadingLiveData.postValue(ResponseState(ERROR))
                    }
                }, {
                    App.instance.getLogger()!!.log("getCompletedOrder error ")
                    loadingLiveData.postValue(ResponseState(NETWORK_ERROR)) })
    }

    fun openRatingScreen(trip: Trip) {
        val data = Pair<Int, Any>(RatingFragment.MODE_HISTORY_RATE, trip)
        switchScreen(RATING_SCREEN, data)
    }

    fun updateRoute(list: List<LatLng>) {
        routeLiveData.postValue(list)
    }

    fun sendHelpTime(orderId: Int, type: SendHelpTimeType) {
        JobFactory.instance.getJobManager()?.addJobInBackground(SendHelpTimeJob(orderId, SendHelpTimeRequest(type)))
    }
}