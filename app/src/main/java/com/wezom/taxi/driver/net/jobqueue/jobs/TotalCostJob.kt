package com.wezom.taxi.driver.net.jobqueue.jobs

import android.annotation.SuppressLint
import com.wezom.taxi.driver.net.request.TotalCostRequest
import com.wezom.taxi.driver.presentation.app.App

/**
 * Created by zorin.a on 17.05.2018.
 */
class TotalCostJob constructor(val orderId: Int, val request: TotalCostRequest) : BaseJob() {
    @SuppressLint("CheckResult")
    override fun onRun() {
        App.instance.getLogger()!!.log("totalCost start and end")
        apiManager.totalCost(orderId, request).blockingGet()
    }
}