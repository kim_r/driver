package com.wezom.taxi.driver.data.models

/**
 * Created by zorin.a on 20.03.2018.
 */
enum class AppState(id: Int) {
    NEW(1),
    ACTIVE_ONLINE(2),
    ACTIVE_OFFLINE(3),
    MODERATION(4),
    BLOCKED(5),
    NOT_ACTIVE(6),
    NEED_COMPLETE(7),
    LIMITED(8)
}