package com.wezom.taxi.driver.presentation.picklocation.list

import android.view.ViewGroup
import com.wezom.taxi.driver.net.response.AutoCompleteResult
import com.wezom.taxi.driver.presentation.base.lists.AdapterClickListener
import com.wezom.taxi.driver.presentation.base.lists.BaseAdapter
import com.wezom.taxi.driver.presentation.base.lists.BaseViewHolder
import javax.inject.Inject

/**
 *Created by Zorin.A on 01.August.2019.
 */
class AutoCompleteAdapter @Inject constructor() : BaseAdapter<AutoCompleteResult, AutoCompleteItemView, BaseViewHolder<AutoCompleteItemView>>() {
    lateinit var listener: AdapterClickListener<AutoCompleteResult>
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): BaseViewHolder<AutoCompleteItemView> {
        val itemView = AutoCompleteItemView(parent.context)
        return BaseViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<AutoCompleteItemView>,
                                  position: Int) {
        holder.view.listener = listener
        holder.view.position = position
        super.onBindViewHolder(holder,
                               position)
    }
}
