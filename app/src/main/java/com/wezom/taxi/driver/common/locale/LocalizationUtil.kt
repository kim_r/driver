package com.wezom.taxi.driver.common.locale

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.os.LocaleList
import android.preference.PreferenceManager
import android.support.annotation.StringDef
import com.wezom.taxi.driver.ext.LANGUAGE
import com.wezom.taxi.driver.ext.LANGUAGE_CHANGED
import timber.log.Timber
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.util.*

/**
 * Created by zorin.a on 30.03.2018.
 */

class LocalizationUtil private constructor(var context: Context) {
    fun getPersistedLocale() = persistedData

    private val persistedData: String
        get() = PreferenceManager.getDefaultSharedPreferences(context).getString(LANGUAGE, "")

    private val langChanged: Boolean
        get() = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(LANGUAGE_CHANGED, false)

    fun setLocale(@LocaleDef language: String): LocaleContextWrapper {
        persistLocale(language)
        return updateResources(language)
    }

    @SuppressLint("ApplySharedPref")
    fun persistLocale(language: String) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(LANGUAGE, language).commit()
    }

    private fun updateResources(language: String): LocaleContextWrapper {
        val resources = context.resources
        val configuration = resources.configuration
        val locale = Locale(language)
        if (isAfter24()) {
            configuration.setLocale(locale)
            val localeList = LocaleList(locale)
            LocaleList.setDefault(localeList)
            configuration.locales = localeList
            context = context.createConfigurationContext(configuration)
        } else if (isAfter17()) {
            configuration.setLocale(locale)
            context = context.createConfigurationContext(configuration)
        } else {
            configuration.locale = locale
            resources.updateConfiguration(configuration, resources.displayMetrics)
        }
        return LocaleContextWrapper(context)
    }

    @SuppressLint("ApplySharedPref")
    fun createWrapper(): LocaleContextWrapper {
        val wrapper: LocaleContextWrapper
        var persistedLocale = persistedData
        if (!langChanged) {
            persistedLocale = Locale.getDefault().language
            PreferenceManager.getDefaultSharedPreferences(context).edit().putString(LANGUAGE, persistedLocale.toLowerCase()).commit()
        }

        Timber.d("LOCALE %s", persistedLocale)
        Timber.d("LOCALE IS langChanged %s", langChanged)
        wrapper = if (persistedLocale.equals(UKRAINE_CODE, ignoreCase = true)) {
            setLocale(UKRAINE_CODE)
        } else setLocale(RUSSIAN_CODE)
        return wrapper
    }

    private fun isAfter17(): Boolean = Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1

    private fun isAfter24(): Boolean = Build.VERSION.SDK_INT >= Build.VERSION_CODES.N

    @Retention(RetentionPolicy.SOURCE)
    @StringDef(RUSSIAN_CODE, UKRAINE_CODE)
    internal annotation class LocaleDef

    companion object : SingletonHolder<LocalizationUtil, Context>(::LocalizationUtil) {
        const val RUSSIAN_CODE = "ru"
        const val UKRAINE_CODE = "uk"
    }
}