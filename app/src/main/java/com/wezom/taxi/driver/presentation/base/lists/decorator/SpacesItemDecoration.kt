package com.wezom.taxi.driver.presentation.base.lists.decorator

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View
import com.wezom.taxi.driver.common.dpToPx
import javax.inject.Inject

/**
 * Created by zorin.a on 15.03.2018.
 */
class SpacesItemDecoration @Inject constructor() : RecyclerView.ItemDecoration() {
    var space: Int = 0

    override fun getItemOffsets(outRect: Rect?, view: View?, parent: RecyclerView?, state: RecyclerView.State?) {
        val dp = dpToPx(space)
        outRect!!.top = dp

        if (parent!!.getChildAdapterPosition(view) == 0) {
            outRect.top = space
        }
//        if (parent!!.getChildAdapterPosition(view) == parent.adapter.itemCount - 1) {
//            outRect.bottom = 24
//        }
    }
}