package com.wezom.taxi.driver.net.jobqueue.jobs

import com.wezom.taxi.driver.data.models.OrderFinalization
import com.wezom.taxi.driver.presentation.app.App

class FinalizeOrderJob constructor(val orderId: Int, val request: OrderFinalization) : BaseJob() {
    override fun onRun() {
        App.instance.getLogger()!!.log("finalizeOrder start ")
        val response = apiManager.finalizeOrder(orderId, request).blockingGet()
        if (response.isSuccess == true) {
            App.instance.getLogger()!!.log("finalizeOrder end ")
            dbManager.deleteFinalization()
            dbManager.deleteOrderResult(orderId)
        }
    }
}