package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName

/**
 *Created by Zorin.A on 01.August.2019.
 */
data class AutoCompleteResult constructor(
        @SerializedName("error")
        val error: Boolean = false,
        @SerializedName("object")
        val obj: String?,
        @SerializedName("street")
        val street: String?,
        @SerializedName("house_number")
        val number: String?,
        @SerializedName("place")
        val place: String?, // city
        @SerializedName("region")
        val region: String?,
        @SerializedName("lat")
        val rawLat: String?,
        @SerializedName("lon")
        val rawLon: String?) {
    val lat: Double get() = rawLat?.toDouble() ?: .0
    val lon: Double get() = rawLon?.toDouble() ?: .0

    val title: String
        get() = obj ?: shortAddress

    val subtitle: String
        get() = if (obj == null) place ?: "" else fullAddress

    val totalAddress: String
        get() = gather(obj,
                       fullAddress)

    val fullAddress: String
        get() = gather(shortAddress,
                       place)

    private val shortAddress: String
        get() = gather(street,
                       number)

    private fun gather(vararg pieces: String?): String {
        var result = ""
        pieces.forEach { piece ->
            piece?.let { result += if (result.isNotBlank()) ", $it" else it }
        }
        return result
    }
}