package com.wezom.taxi.driver.presentation.customview

import android.animation.Animator
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.animation.AccelerateInterpolator
import com.wezom.taxi.driver.common.OnSwipeTouchListener
import com.wezom.taxi.driver.databinding.IntroBinding

/**
 * Created by zorin.a on 19.04.2018.
 */
class IntroView : ConstraintLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, attributeSetId: Int) : super(context, attrs, attributeSetId)

    private val binding = IntroBinding.inflate(LayoutInflater.from(context), this, true)

    var listener: SwipeListener? = null
    private val animListener = object : Animator.AnimatorListener {
        override fun onAnimationRepeat(animation: Animator?) {
        }

        override fun onAnimationEnd(animation: Animator?) {
            listener?.onSwipeDone()
        }

        override fun onAnimationCancel(animation: Animator?) {
        }

        override fun onAnimationStart(animation: Animator?) {

        }
    }
    private val gestureListener: OnSwipeTouchListener by lazy {
        object : OnSwipeTouchListener(context!!.applicationContext) {
            override fun onSwipeLeft() {
                animateView(this@IntroView, DIRECTION_LEFT)
            }

            override fun onSwipeTop() {
            }

            override fun onSwipeRight() {
                animateView(this@IntroView, DIRECTION_RIGHT)
            }

            override fun onSwipeBottom() {

            }

            override fun onTap() {
            }
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        binding.run {
            root.setOnTouchListener(gestureListener)
        }
    }

    fun animateView(view: View, direction: Int) {
        view.animate()
                .translationX(500F * direction)
                .alpha(0F)
                .setDuration(300)
                .setInterpolator(AccelerateInterpolator())
                .setListener(animListener)
                .start()

    }

    interface SwipeListener {
        fun onSwipeDone()
    }

    companion object {
        const val DIRECTION_LEFT = -1
        const val DIRECTION_RIGHT = 1
    }
}