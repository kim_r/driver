package com.wezom.taxi.driver.net.websocket.channel

import android.content.SharedPreferences
import android.location.Location
import com.google.gson.Gson
import com.wezom.taxi.driver.BuildConfig
import com.wezom.taxi.driver.data.models.Coordinate
import com.wezom.taxi.driver.injection.qualifiers.SocketOkHttpClient
import com.wezom.taxi.driver.net.websocket.messages.BaseSocketMessage
import com.wezom.taxi.driver.net.websocket.messages.BaseSocketMessage.Companion.SUBMIT_COORDINATES_ROOM
import com.wezom.taxi.driver.net.websocket.messages.DriverLocationMessage
import com.wezom.taxi.driver.net.websocket.messages.LoginMessage
import com.wezom.taxi.driver.presentation.app.App
import okhttp3.OkHttpClient
import timber.log.Timber

/**
 * Created by zorin.a on 022 22.02.18.
 */

class WebSocketChannel constructor(@SocketOkHttpClient client: OkHttpClient, gson: Gson,
                                   sharedPreferences: SharedPreferences) : BaseWebSocketChannel(client, gson, sharedPreferences) {
    fun sendPing() {
        ping()
    }

    override fun provideBaseUrl(): String = BuildConfig.WS_API

    fun login() {
        Timber.d("SOCKET login()")
        val message = LoginMessage(BaseSocketMessage.LOGIN_ROOM, token)
        sendMessage(message, LoginMessage::class.java)
    }

    fun submitCoordinates(location: Location?) {
        App.instance.getLogger()!!.log("location ${location?.latitude} + ${location?.longitude}")
        if (location?.latitude != 0.0 && location?.longitude != 0.0) {
            Timber.d("SOCKET submitCoordinates() + ${location?.latitude} + ${location?.longitude}")
            val message = DriverLocationMessage(SUBMIT_COORDINATES_ROOM,
                    Coordinate(latitude = location?.latitude, longitude = location?.longitude, degree = location!!.bearing.toInt()))
            sendMessage(message, DriverLocationMessage::class.java)
        }
    }

    fun getTariffiedRegions() {
        Timber.d("SOCKET getTariffiedRegions()")
        emit("tariffedRegions")
    }
}