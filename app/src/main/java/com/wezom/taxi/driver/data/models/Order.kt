package com.wezom.taxi.driver.data.models

import android.arch.persistence.room.Embedded
import android.location.Location
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by zorin.a on 022 22.02.18.
 */

data class Order(
        @SerializedName("id") var id: Int?,
        @SerializedName("currentTime") var currentTime: Long?,
        @SerializedName("carArrivalTime") var carArrivalTime: Long?,
        @SerializedName("kilometerPrice") var kilometerPrice: Double?,
        @SerializedName("distanceFromDriver") var distanceFromDriver: Double?,
        @SerializedName("calculatedPath") var calculatedPath: Double?,
        @SerializedName("calculatedTime") var calculatedTime: Long?,
        @SerializedName("distance") var distance: Double?,
        @SerializedName("currentCoefficient") var currentCoefficient: Double?,
        @SerializedName("answerTime") var answerTime: Long?,
        @SerializedName("serverTime") var serverTime: Long?,
        @SerializedName("coordinates") var coordinates: List<Address>?,
        @SerializedName("services") var services: List<Int>?,
        @Embedded(prefix = "payment") @SerializedName("payment") var payment: Payment?,
        @Embedded(prefix = "showing_") @SerializedName("newOrderShowing") var newOrderShowing: NewOrderShowing?,
        @Embedded(prefix = "accepted_showing_") @SerializedName("acceptedOrderShowing") var acceptedOrderShowing: AcceptedOrderShowing?,
        @SerializedName("preliminaryTime") var preliminaryTime: Long?,
        @SerializedName("carArrival") var carArrival: Int?,
        @SerializedName("comment") var comment: String?,
        @Embedded(prefix = "user_") @SerializedName("user") var user: User?,
        @SerializedName("status") var status: OrderStatus?,
        @SerializedName("durationCalculated") var durationCalculated: Double?,
        @SerializedName("customerAppOrdersCount") var customerAppOrdersCount: Int?,
//        @SerializedName("surcharge") var surcharge: Double?,
        var actualCost: Int? //only for local db
) : Parcelable {
    constructor() : this(
            0,
            0,
            0,
            0.0,
            0.0,
            0.0,
            0,
            0.0,
            0.0,
            0,
            0,
            listOf<Address>(),
            listOf<Int>(),
            Payment(),
            NewOrderShowing(),
            AcceptedOrderShowing(),
            0,
            0,
            "",
            User(),
            OrderStatus.NEW,
            0.0,
            0,
            0
            )

    constructor(source: Parcel) : this(
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Long::class.java.classLoader) as Long?,
            source.readValue(Long::class.java.classLoader) as Long?,
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readValue(Long::class.java.classLoader) as Long?,
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readValue(Long::class.java.classLoader) as Long?,
            source.readValue(Long::class.java.classLoader) as Long?,
            source.createTypedArrayList(Address.CREATOR),
            ArrayList<Int>().apply { source.readList(this, Int::class.java.classLoader) },
            source.readParcelable<Payment>(Payment::class.java.classLoader),
            source.readParcelable<NewOrderShowing>(NewOrderShowing::class.java.classLoader),
            source.readParcelable<AcceptedOrderShowing>(AcceptedOrderShowing::class.java.classLoader),
            source.readValue(Long::class.java.classLoader) as Long?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readParcelable<User>(User::class.java.classLoader),
            source.readValue(Int::class.java.classLoader)?.let { OrderStatus.values()[it as Int] },
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?
    )

    override fun describeContents() = 0

    fun getLocationLastPoint(): Location? {
        if (coordinates != null && coordinates!!.size > 1) {
            val location = Location("LastPointPoder")
            location.latitude = coordinates!!.get(1).latitude!!
            location.longitude = coordinates!!.get(1).longitude!!
            return location
        }
        return null
    }

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeValue(id)
        writeValue(currentTime)
        writeValue(carArrivalTime)
        writeValue(kilometerPrice)
        writeValue(distanceFromDriver)
        writeValue(calculatedPath)
        writeValue(calculatedTime)
        writeValue(distance)
        writeValue(currentCoefficient)
        writeValue(answerTime)
        writeValue(serverTime)
        writeTypedList(coordinates)
        writeList(services)
        writeParcelable(payment, 0)
        writeParcelable(newOrderShowing, 0)
        writeParcelable(acceptedOrderShowing, 0)
        writeValue(preliminaryTime)
        writeValue(carArrival)
        writeString(comment)
        writeParcelable(user, 0)
        writeValue(status?.ordinal)
        writeValue(durationCalculated)
        writeValue(customerAppOrdersCount)
        writeValue(actualCost)
    }

    override fun toString(): String {
        return "Order{id=$id, kilometerPrice=$kilometerPrice, calculatedPath=$calculatedPath, calculatedTime=$calculatedTime," +
                " distance=$distance, currentCoefficient=$currentCoefficient, answerTime=$answerTime, serverTime=$serverTime, coordinates=$coordinates, services=$services, payment=$payment, newOrderShowing=$newOrderShowing, acceptedOrderShowing=$acceptedOrderShowing, preliminaryTime=$preliminaryTime," +
                " carArrival=$carArrival, comment=$comment, user=$user, status=$status}"
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Order> = object : Parcelable.Creator<Order> {
            override fun createFromParcel(source: Parcel): Order = Order(source)
            override fun newArray(size: Int): Array<Order?> = arrayOfNulls(size)
        }
    }
}



