package com.wezom.taxi.driver.data.models

import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.presentation.customview.ConditionsView

/**
 * Created by zorin.a on 3/11/19.
 */
data class Condition(
        @SerializedName("is_done")
        var isDone: Boolean,
        @SerializedName("reward")
        val reward: Int?,
        @SerializedName("trips_count")
        val tripsCount: Int?,
        var state: ConditionsView.ConditionState
)