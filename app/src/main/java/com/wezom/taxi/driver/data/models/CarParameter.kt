package com.wezom.taxi.driver.data.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by zorin.a on 021 21.02.18.
 */
data class CarParameter(@SerializedName("id") val id: Int,
                        @SerializedName("name") val name: String? = null,
                        @SerializedName("value") val value: String? = null) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)
        parcel.writeString(value)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<CarParameter> {
        override fun createFromParcel(parcel: Parcel): CarParameter = CarParameter(parcel)

        override fun newArray(size: Int): Array<CarParameter?> = arrayOfNulls(size)
    }
}