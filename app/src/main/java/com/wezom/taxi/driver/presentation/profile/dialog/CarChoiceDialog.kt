package com.wezom.taxi.driver.presentation.profile.dialog

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.widget.ArrayAdapter
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.data.models.CarParameter


/**
 * Created by zorin.a on 07.03.2018.
 */
class CarChoiceDialog : DialogFragment() {
    private var data: List<CarParameter>? = null
    private var pos: Int? = null
    var listener: ChoiceDialogSelection? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        data = arguments?.getParcelableArrayList(DATA_KEY)
        pos = arguments?.getInt(CHECKED_ITEM_KEY)

        var stringData: List<String?>? = null
        if (data != null) {
            stringData = data!!.map { CarParameter -> CarParameter.name }
        }

        val adapter = ArrayAdapter<CharSequence>(
                activity, R.layout.custom_list_item_single_choice, stringData)

        return AlertDialog.Builder(context!!)
                .setSingleChoiceItems(adapter, pos!!) { _, pos ->
                    listener?.onSelected(pos, data!![pos])
                    this.dismiss()
                }
                .create()
    }

    companion object {
        val TAG: String = CarChoiceDialog.javaClass.simpleName
        const val DATA_KEY: String = "data_key"
        const val CHECKED_ITEM_KEY: String = "checked_item_key"

        fun newInstance(data: ArrayList<CarParameter>, checkedItem: Int? = 0): CarChoiceDialog {
            val args = Bundle()
            args.putParcelableArrayList(DATA_KEY, data)
            checkedItem?.let { args.putInt(CHECKED_ITEM_KEY, it) }
            var fragment = CarChoiceDialog()
            fragment.arguments = args
            return fragment
        }
    }

    interface ChoiceDialogSelection {
        fun onSelected(position: Int, CarParameter: CarParameter)
    }
}