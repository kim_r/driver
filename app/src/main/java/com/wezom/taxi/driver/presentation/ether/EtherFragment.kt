package com.wezom.taxi.driver.presentation.ether

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.bus.events.AdapterScrollEvent
import com.wezom.taxi.driver.data.models.NewOrderInfo
import com.wezom.taxi.driver.databinding.FragmentEtherBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.ext.setVisible
import com.wezom.taxi.driver.net.websocket.channel.WebSocketChannel
import com.wezom.taxi.driver.presentation.base.BaseFragment
import com.wezom.taxi.driver.presentation.base.lists.decorator.SpacesItemDecoration
import com.wezom.taxi.driver.presentation.customview.DriverToolbar
import com.wezom.taxi.driver.presentation.customview.EtherSwitchButton
import com.wezom.taxi.driver.presentation.ether.list.EtherAdapter
import com.wezom.taxi.driver.presentation.main.events.DeleteBroadcastEvent
import com.wezom.taxi.driver.presentation.main.events.NewBroadcastEvent
import com.wezom.taxi.driver.presentation.main.events.SocketServiceEvent
import com.wezom.taxi.driver.presentation.main.events.SocketServiceResponseEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber
import javax.inject.Inject

/**
 *Created by Zorin.A on 18.June.2019.
 */
class EtherFragment : BaseFragment() {

    @Inject
    lateinit var adapter: EtherAdapter
    @Inject
    lateinit var decorator: SpacesItemDecoration

    @Inject
    lateinit var socketChannel: WebSocketChannel

    lateinit var viewModel: EtherViewModel
    lateinit var binding: FragmentEtherBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this,
                viewModelFactory)
                .getViewModelOfType()
        viewModel.etherLiveData.observe(this,
                Observer {
                    Timber.d("ETHER_ADAPTER etherLiveData size: ${it?.size}")
                    binding.emptyText.setVisible(it!!.isEmpty())
                    adapter.addAll(it)
                })
        viewModel.loadingLiveData.observe(this,
                progressObserver)
        adapter.emptyLiveData.observe(this,
                Observer {
                    Timber.d("ETHER_ADAPTER emptyLiveData ")
                    binding.toolbar.binding.etherButton.updateCounter(it!!)
                    EtherSwitchButton.cache = it
                    if (it <= 0) {
                        if (DriverToolbar.IS_ONLINE_STATE) {
                            binding.emptyText.setVisible(true)
//                            longToast(R.string.no_orders)
                        }
                    }
                })
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentEtherBinding.inflate(inflater,
                container,
                false)
        binding.setLifecycleOwner(this)

        initBroadcastListeners()
        return binding.root
    }

    @SuppressLint("CheckResult")
    override fun onStart() {
        super.onStart()
    }


    override fun onStop() {
        super.onStop()
    }


    override fun onViewCreated(view: View,
                               savedInstanceState: Bundle?) {
        super.onViewCreated(view,
                savedInstanceState)
        initViews()
        viewModel.getFilter()
        initListeners()
//        if (DriverToolbar.IS_ONLINE_STATE) {
        viewModel.getEther()
//        } else {
//            startForceUpdate()
//        }
    }

    @SuppressLint("CheckResult")
    private fun initListeners() {
        RxBus.listen(SocketServiceEvent::class.java)
                .subscribe {
                    Timber.d("Ether init listeners")
                    when {
                        it.command == SocketServiceEvent.SocketCommand.START -> {
                            stopForceUpdate()
                        }
                        it.command == SocketServiceEvent.SocketCommand.STOP -> {
                            startForceUpdate()
                        }
                    }
                }

        RxBus.listen(AdapterScrollEvent::class.java)
                .subscribe {
                    if (it.position == 0)
                        binding.etherList.scrollToPosition(it.position)
                }

    }

    private fun startForceUpdate() {
//        Timber.d("Ether START MANUAL UPDATES")
//        disposable += Observable.interval(1,
//                                          TimeUnit.SECONDS)
//            .subscribe {
//                Timber.d("Ether EXECUTE MANUAL UPDATE")
//                viewModel.getEther()
//            }
    }

    private fun stopForceUpdate() {
//        Timber.d("Ether STOP MANUAL UPDATES")
//        disposable.clear()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getEther()
    }

    private fun initViews() {
        binding.run {
            toolbar.onlineStateLiveData.observe(this@EtherFragment,
                    Observer {
                        when (it?.result) {
                            SocketServiceResponseEvent.Result.POSITIVE -> {
                                viewModel.getEther()
                                socketChannel.sendPing()
                            }
                            SocketServiceResponseEvent.Result.NEGATIVE -> {
                                adapter.clear()
                            }
                        }
                    })
            toolbar.binding.run {
                etherButton.initAsMap()
                etherButton.listener = object : EtherSwitchButton.OnEtherClickListener {
                    override fun onClick(state: EtherSwitchButton.State) {
                        viewModel.performMapClick()
                    }
                }
                filter.show()
                filter.clickListener = {
                    viewModel.showFilterScreen()
                }
            }
            etherList.setHasFixedSize(true)
            decorator.space = 8
            etherList.addItemDecoration(decorator)
            adapter.callback = { position: Int, order: NewOrderInfo ->
                viewModel.enterAcceptOrderScreen(order)
            }
            etherList.adapter = adapter


        }
    }

    private fun initBroadcastListeners() {
        disposable.clear()
        disposable += RxBus.listen(NewBroadcastEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Timber.d("ETHER_FRAGMENT: NewBroadcastEvent")
                    adapter.addItem(it.orderInfo)
                    binding.emptyText.setVisible(adapter.getAllItem().isEmpty())
                }

        disposable += RxBus.listen(DeleteBroadcastEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Timber.d("ETHER_FRAGMENT: DeleteBroadcastEvent ${it.orderId}")
                    if (adapter.content.isNotEmpty()) {
                        adapter.removeItem(it.orderId)
                        binding.emptyText.setVisible(adapter.getAllItem().isEmpty())
                    }
                }
    }

    companion object {
        fun newInstance(): EtherFragment {
            return EtherFragment()
        }
    }
}