package com.wezom.taxi.driver.net.websocket.channel

import com.wezom.taxi.driver.net.websocket.messages.BaseSocketMessage
import java.lang.reflect.Type

/**
 * Created by zorin.a on 022 22.02.18.
 */
interface SocketChannel {
    fun connect()
    fun disconnect()
    fun <T : BaseSocketMessage> sendMessage(src: T, type: Type)
    fun emit(src: String)
    fun ping()
}