package com.wezom.taxi.driver.data.db

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.wezom.taxi.driver.data.models.*
import java.util.*


class Converters {

    var gson = Gson()

    @TypeConverter
    fun toOrderStatusConvert(value: Int): OrderStatus {
        return when (value) {
            0 -> OrderStatus.NEW
            2 -> OrderStatus.ACCEPTED
            3 -> OrderStatus.ARRIVED_AT_PLACE
            4 -> OrderStatus.ON_THE_WAY
            5 -> OrderStatus.NOT_COMPLETED
            6 -> OrderStatus.COMPLETED
            7 -> OrderStatus.TOTAL_COST
            else -> OrderStatus.NEW

        }
    }

    @TypeConverter
    fun fromOrderStatusConvert(status: OrderStatus): Int {
        return when (status) {
            OrderStatus.NEW -> 0
            OrderStatus.ACCEPTED -> 2
            OrderStatus.ARRIVED_AT_PLACE -> 3
            OrderStatus.ON_THE_WAY -> 4
            OrderStatus.NOT_COMPLETED -> 5
            OrderStatus.COMPLETED -> 6
            OrderStatus.TOTAL_COST -> 7
            else -> 0
        }
    }

    @TypeConverter
    fun toPaymentMethodConvert(value: Int): PaymentMethod {
        return when (value) {
            1 -> PaymentMethod.CASH
            2 -> PaymentMethod.CASH_AND_BONUS
            3 -> PaymentMethod.CARD
            4 -> PaymentMethod.CARD_AND_BONUS
            else -> PaymentMethod.CASH

        }
    }

    @TypeConverter
    fun fromPaymentMethodConvert(status: PaymentMethod): Int {
        return when (status) {
            PaymentMethod.CASH -> 1
            PaymentMethod.CASH_AND_BONUS -> 2
            PaymentMethod.CARD -> 3
            PaymentMethod.CARD_AND_BONUS -> 4
        }
    }

    @TypeConverter
    fun stringToAddress(data: String?): List<Address>? {
        if (data == null) {
            return Collections.emptyList()
        }

        val listType = object : TypeToken<List<Address>>() {
        }.type

        return gson.fromJson<List<Address>>(data, listType)
    }

    @TypeConverter
    fun addressToString(someObjects: List<Address>?): String {
        return gson.toJson(someObjects)
    }


    @TypeConverter
    fun stringToCoordinate(data: String?): List<Coordinate>? {
        if (data == null) {
            return Collections.emptyList()
        }

        val listType = object : TypeToken<List<Coordinate>>() {

        }.type

        return gson.fromJson<List<Coordinate>>(data, listType)
    }

    @TypeConverter
    fun coordinatesToString(someObjects: List<Coordinate>?): String {
        return gson.toJson(someObjects)
    }


    @TypeConverter
    fun stringToServices(data: String?): List<Int>? {
        if (data == null) {
            return Collections.emptyList()
        }

        val listType = object : TypeToken<List<Int>>() {

        }.type

        return gson.fromJson<List<Int>>(data, listType)
    }

    @TypeConverter
    fun servicesToString(someObjects: List<Int>?): String {
        return gson.toJson(someObjects)
    }

    @TypeConverter
    fun toChangedParamsConvert(value: Int): ChangedParams {
        return when (value) {
            0 -> ChangedParams.NO_CHANGES
            1 -> ChangedParams.TIME
            2 -> ChangedParams.DISTANCE
            3 -> ChangedParams.DISTANCE_AND_TIME

            else -> ChangedParams.NO_CHANGES

        }
    }

    @TypeConverter
    fun fromChangedParamsConvert(status: ChangedParams): Int {
        return when (status) {
            ChangedParams.NO_CHANGES -> 0
            ChangedParams.TIME -> 1
            ChangedParams.DISTANCE -> 2
            ChangedParams.DISTANCE_AND_TIME -> 3
        }
    }

    @TypeConverter
    fun toCardConverter(data: String?): List<Card> {
        if (data == null) {
            return Collections.emptyList()
        }

        val listType = object : TypeToken<List<Card>>() {

        }.getType()

        return gson.fromJson(data, listType)
    }

    @TypeConverter
    fun fromCardConverte(someObjects: List<Card>): String {
        return gson.toJson(someObjects)
    }

    @TypeConverter
    fun stringToRoute(data: String?): List<Route>? {
        if (data == null) {
            return Collections.emptyList()
        }

        val listType = object : TypeToken<List<Route>>() {

        }.type

        return gson.fromJson<List<Route>>(data, listType)
    }

    @TypeConverter
    fun routesToString(someObjects: List<Route>?): String {
        return gson.toJson(someObjects)
    }

}