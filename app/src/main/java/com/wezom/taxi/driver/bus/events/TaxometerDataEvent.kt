package com.wezom.taxi.driver.bus.events

/**
 * Created by zorin.a on 02.08.2018.
 */
class TaxometerDataEvent (val currentSpeed: Double,
                          val passedDistance: Double,
                          val localDistance: Float,
                          val accuracy: Float,
                          val velocity: Float)