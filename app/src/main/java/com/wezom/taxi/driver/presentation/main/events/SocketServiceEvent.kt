package com.wezom.taxi.driver.presentation.main.events

/**
 * Created by zorin.a on 26.04.2018.
 */
class SocketServiceEvent constructor(val command: SocketCommand) {
    enum class SocketCommand { START, STOP }
}