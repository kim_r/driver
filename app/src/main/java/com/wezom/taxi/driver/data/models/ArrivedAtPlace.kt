package com.wezom.taxi.driver.data.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ArrivedAtPlace(
        @SerializedName("longitude") var longitude: Double,
        @SerializedName("latitude") var latitude: Double,
        @SerializedName("eventTime") var eventTime: Long
): Parcelable, Serializable {
    constructor(parcel: Parcel) : this(
            parcel.readDouble(),
            parcel.readDouble(),
            parcel.readLong())

    constructor() : this(longitude = 0.0, latitude = 0.0, eventTime = 0)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(longitude)
        parcel.writeDouble(latitude)
        parcel.writeLong(eventTime)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ArrivedAtPlace> {
        override fun createFromParcel(parcel: Parcel): ArrivedAtPlace {
            return ArrivedAtPlace(parcel)
        }

        override fun newArray(size: Int): Array<ArrivedAtPlace?> {
            return arrayOfNulls(size)
        }
    }
}