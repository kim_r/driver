package com.wezom.taxi.driver.presentation.bonus_program

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import com.wezom.taxi.driver.data.models.ResponseState
import com.wezom.taxi.driver.net.api.ApiManager
import com.wezom.taxi.driver.net.response.CurrendDiscountResponse
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import javax.inject.Inject

/**
 * Created by zorin.a on 3/11/19.
 */
class BonusProgramViewModel @Inject constructor(screenRouterManager: ScreenRouterManager,
                                                private val apiManager: ApiManager)
    : BaseViewModel(screenRouterManager) {
    val bonusLiveData = MutableLiveData<CurrendDiscountResponse>()

    @SuppressLint("CheckResult")
    fun getCurrentBonusProgram() {
        loadingLiveData.postValue(ResponseState(ResponseState.State.LOADING))
        App.instance.getLogger()!!.log("getDiscountDetails start ")
        apiManager.getDiscountDetails()
                .subscribe({ response ->
                    App.instance.getLogger()!!.log("getDiscountDetails suc ")
                    handleResponseState(response)

                    var notMetRequirements = false
                    response.actionConditions?.forEach {
                        if (!it.isPassed!!) {
                            notMetRequirements = true
                            return@forEach
                        }
                    }

                    if (notMetRequirements) {
                        response.actionTripsInfo
                                ?.conditions
                                ?.forEach {
                                    it.isDone = false
                                }
                    }
                    bonusLiveData.value = response
                },
                        {
                            App.instance.getLogger()!!.log("getDiscountDetails error")
                            loadingLiveData.postValue(
                                    ResponseState(ResponseState.State.NETWORK_ERROR, message = it.message)
                            )
                        })

//******  this is mock data
//        val actionCondition1 = ActionCondition("20", true, "30")
//        val actionCondition2 = ActionCondition("40", true, "55")
//        val actionCondition3 = ActionCondition("4", true, "4.2")
//        val listActionConditions = mutableListOf<ActionCondition>()
//        listActionConditions.add(actionCondition1)
//        listActionConditions.add(actionCondition2)
//        listActionConditions.add(actionCondition3)
//
//        val cond1 = Condition(true, 100, 5, HIDDEN)
//        val cond2 = Condition(false, 200, 10, HIDDEN)
//        val cond3 = Condition(false, 300, 50, HIDDEN)
//        val condList = mutableListOf<Condition>()
//        condList.add(cond1)
//        condList.add(cond2)
//        condList.add(cond3)
//        val actionTripsInfo = ActionTripsInfo(8, condList)
//        val resp = CurrentDiscountResponse(listActionConditions, actionTripsInfo, "Соверши 1000 поездок с 05.03 и получи великолепное ничего")
//        bonusLiveData.value = resp
    }
}