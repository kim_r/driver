package com.wezom.taxi.driver.presentation.privatestatistics

import android.arch.lifecycle.MutableLiveData
import com.wezom.taxi.driver.data.models.Driver
import com.wezom.taxi.driver.data.models.ResponseState
import com.wezom.taxi.driver.data.models.ResponseState.State.*
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.repository.DriverRepository
import io.reactivex.rxkotlin.plusAssign
import javax.inject.Inject

/**
 * Created by zorin.a on 14.03.2018.
 */
class PrivateStatisticsViewModel @Inject constructor(screenRouterManager: ScreenRouterManager,
                                                     private val repository: DriverRepository) : BaseViewModel(screenRouterManager) {

    val statisticsLiveData: MutableLiveData<Driver> = MutableLiveData()


    fun getPersonalStatistics() {
        App.instance.getLogger()!!.log("getPersonalStatistics start ")
        loadingLiveData.postValue(ResponseState(LOADING))
        disposable += repository.getPersonalStatistics().subscribe({ response ->
            App.instance.getLogger()!!.log("getPersonalStatistics suc ")
            handleResponseState(response)
            if (response?.isSuccess!!) {
                statisticsLiveData.postValue(response.driver)
            }
        }, { it ->
            App.instance.getLogger()!!.log("getPersonalStatistics error ")
            loadingLiveData.postValue(ResponseState(NETWORK_ERROR))
        })
    }
}