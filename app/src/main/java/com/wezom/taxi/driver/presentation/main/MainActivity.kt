package com.wezom.taxi.driver.presentation.main

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.*
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.databinding.DataBindingUtil
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.PowerManager
import android.support.v4.content.LocalBroadcastManager
import android.support.v4.view.GravityCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallState
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.ActivityResult
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import com.wezom.taxi.driver.BuildConfig
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.R.id.*
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.bus.events.*
import com.wezom.taxi.driver.common.BeepUtil
import com.wezom.taxi.driver.common.loadRoundedImage
import com.wezom.taxi.driver.data.models.AppState
import com.wezom.taxi.driver.databinding.MainActivityBinding
import com.wezom.taxi.driver.ext.*
import com.wezom.taxi.driver.presentation.base.BaseActivity
import com.wezom.taxi.driver.presentation.customview.DriverToolbar
import com.wezom.taxi.driver.presentation.main.events.DisableOnlineStatusEvent
import com.wezom.taxi.driver.presentation.main.events.NewOrderInfoEvent
import com.wezom.taxi.driver.presentation.main.events.ShowSecondaryOrderEvent
import com.wezom.taxi.driver.presentation.main.events.SocketServiceEvent
import com.wezom.taxi.driver.presentation.main.events.SocketServiceEvent.SocketCommand.START
import com.wezom.taxi.driver.presentation.main.events.SocketServiceEvent.SocketCommand.STOP
import com.wezom.taxi.driver.presentation.profile.events.UpdateUserDataEvent
import com.wezom.taxi.driver.presentation.profile.events.UpdateUserImageEvent
import com.wezom.taxi.driver.receiver.GpsLocationReceiver
import com.wezom.taxi.driver.service.MessagingService
import com.wezom.taxi.driver.service.MessagingService.Companion.TYPE_ID
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import org.jetbrains.anko.find
import timber.log.Timber

class MainActivity : BaseActivity(), InstallStateUpdatedListener {
    //region var
    private lateinit var viewModel: MainViewModel
    private lateinit var binding: MainActivityBinding
    private var appState: AppState = AppState.ACTIVE_OFFLINE

    private var gpsReceiver = GpsLocationReceiver()

    private lateinit var appUpdateManager: AppUpdateManager

    private val userImageObserver = Observer<String> { uriString ->
        if (uriString.isNullOrBlank()) {
            setUserImageToDrawer(defaultRes = R.drawable.ic_driver_vector_ava)
        } else {
            if (uriString.startsWith("http")) {
                setUserImageToDrawer(url = uriString)
            } else {
                setUserImageToDrawer(uri = Uri.parse(uriString))
            }
        }
    }

    override fun onStateUpdate(installState: InstallState) {
        if (installState.installStatus() == InstallStatus.DOWNLOADED) {
            appUpdateManager.completeUpdate()
        }
    }

    private val userNameObserver = Observer<String> { data ->
        setUserNameToDrawer(data)
    }

    private val userRatingObserver = Observer<String> { data ->
        setUserRatingToDrawer(data)
    }

//    private val pushModelObserver = Observer<BasicNotification> { model ->
//        if (model != null) viewModel.handleNotificationModel(model)
//    }
    //endregion

    //region override


    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        Timber.d("onNewIntent: ${intent.toString()}")
        val typeId = getIntent().extras?.get(TYPE_ID) //Fb logging
//        if (getIntent().extras != null) {
//            for (key in getIntent().extras!!.keySet()) {
//                val value = getIntent().extras!!.getString(key)
//                Timber.tag(FIREBASE_LOG_TAG).d("DATA FIREBASE Key: $key Value: $value")
//            }
//        }

        if (typeId != null) {
            viewModel.handleIntent(intent!!)
        } else viewModel.setInitScreen()
    }

    private fun notifyUser() {
        "Update".longToast(this)
    }

    @SuppressLint("LogNotTimber")
    fun initUpdate() {
        appUpdateManager = AppUpdateManagerFactory.create(this)
        appUpdateManager.registerListener(this)

        appUpdateManager.appUpdateInfo.addOnFailureListener {
            Log.e("appUpdateInfoError", it.localizedMessage)
        }

        appUpdateManager.appUpdateInfo.addOnSuccessListener {
            if (it.availableVersionCode().toString().length >= 7) {
                "${it.availableVersionCode()}".longToast(baseContext)
                val fixVer: Int = it.availableVersionCode().toString().substring(it.availableVersionCode().toString().length - 3, it.availableVersionCode().toString().length).toInt()
                val updateVer: Int = it.availableVersionCode().toString().substring(it.availableVersionCode().toString().length - 6, it.availableVersionCode().toString().length - 3).toInt()
                val appVer: Int = it.availableVersionCode().toString().substring(0, it.availableVersionCode().toString().length - 6).toInt()

                val fixVerInApp: Int = BuildConfig.VERSION_CODE.toString().substring(BuildConfig.VERSION_CODE.toString().length - 3, BuildConfig.VERSION_CODE.toString().length).toInt()
                val updateVerInApp: Int = BuildConfig.VERSION_CODE.toString().substring(BuildConfig.VERSION_CODE.toString().length - 6, BuildConfig.VERSION_CODE.toString().length - 3).toInt()
                val appVerInApp: Int = BuildConfig.VERSION_CODE.toString().substring(0, BuildConfig.VERSION_CODE.toString().length - 6).toInt()


                if (updateVer > updateVerInApp || appVer > appVerInApp) {
                    requestUpdate(it)
                }
            }
        }
    }


    @SuppressLint("BatteryLife")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initUpdate()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val packageName = applicationContext.packageName
            val pm: PowerManager = applicationContext.getSystemService(Context.POWER_SERVICE) as PowerManager
            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                val intent = Intent()
                intent.action = android.provider.Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS
                intent.flags = FLAG_ACTIVITY_NEW_TASK
                intent.data = Uri.parse("package:$packageName")
                applicationContext.startActivity(intent)
            }
        }

        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        onNewIntent(intent)

        viewModel.userImageLiveData.observe(this, userImageObserver)
        viewModel.userNameLiveData.observe(this, userNameObserver)
        viewModel.userRatingLiveData.observe(this, userRatingObserver)
        viewModel.lockDrawerLiveData.observe(this, Observer {
            lockDrawer(it!!)
        })
//        viewModel.pushModelLiveData.observe(this, pushModelObserver)
        viewModel.drawerLockLiveData.observe(this@MainActivity, Observer { locked -> lockDrawer(locked!!) })

        viewModel.updateUserImage()
        viewModel.updateUserName()
        viewModel.updateUserRating()

        initBusListeners()

        clicks()

        BeepUtil.init(this, DriverToolbar.IS_ONLINE_STATE)

        initServiceIntentListener()


    }

    private fun requestUpdate(appUpdateInfo: AppUpdateInfo?) {
        appUpdateManager.startUpdateFlowForResult(
                appUpdateInfo,
                AppUpdateType.IMMEDIATE, //  HERE specify the type of update flow you want
                this,   //  the instance of an activity
                REQUEST_CODE_FLEXI_UPDATE
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_FLEXI_UPDATE) {
            when (resultCode) {
                Activity.RESULT_OK -> {

                }
                Activity.RESULT_CANCELED -> {
                    this.recreate()
                }
                ActivityResult.RESULT_IN_APP_UPDATE_FAILED -> {
                    this.recreate()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.setStatusBackground(false)
        isForeground = true
        try {
            registerReceiver(gpsReceiver, IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION))
        } catch (e: RuntimeException) {
            e.printStackTrace()
        }

        viewModel.isOrderExist()

        appUpdateManager.appUpdateInfo.addOnSuccessListener {
            if (it.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                appUpdateManager.startUpdateFlowForResult(
                        it,
                        AppUpdateType.IMMEDIATE,
                        this,
                        REQUEST_CODE_FLEXI_UPDATE
                )
            } else if (it.installStatus() == InstallStatus.DOWNLOADED) {
                notifyUser()
            }
        }


        Timber.d("onResume()")
    }

    override fun onPause() {
        super.onPause()
        isForeground = false
        try {
            unregisterReceiver(gpsReceiver)
        } catch (e: RuntimeException) {
        }
        Timber.d("onPause()")
    }

    override fun onDestroy() {
        super.onDestroy()
        RxBus.publish(SocketServiceEvent(STOP))
        BeepUtil.clear()
        appUpdateManager.unregisterListener(this)
    }

    override fun onStop() {
        viewModel.setStatusBackground(true)

        super.onStop()
    }

//endregion

    //region fun
    private fun clicks() {
        binding.run {
            navigationView.getHeaderView(0).find<View>(header_root).setOnClickListener {
                drawerLayout.closeDrawer(GravityCompat.START)
                viewModel.switchToUserProfile(appState)
            }
            navigationView.getHeaderView(0).find<View>(settings_button).setOnClickListener {
                viewModel.switchToSettings()
                drawerLayout.closeDrawer(GravityCompat.START)
            }

            navigationView.setNavigationItemSelectedListener { menuItem ->
                when (menuItem.itemId) {
                    action_main_screen -> viewModel.switchToMainScreen()
                    action_gains_screen -> viewModel.switchToGainsScreen()
                    action_balance_screen -> viewModel.switchToBalanceScreen()
                    action_statistics_screen -> viewModel.switchToStatisticsScreen()
                    action_bonus_screen -> viewModel.switchToBonusScreen()
                    action_history_screen -> viewModel.switchToHistoryScreen()
                    action_help_screen -> viewModel.switchToHelpScreen()
                    // action_exit -> viewModel.logout()
                    else -> false
                }
                drawerLayout.closeDrawer(GravityCompat.START)
                return@setNavigationItemSelectedListener true
            }

            RxView.clicks(becomeCustomer).subscribe {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse("https://wezom.com")
                startActivity(intent)
            }
        }
    }


    private fun initBusListeners() {

        //RxBus.publish(SocketServiceEvent(STOP))

//if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            String packageName = context.getPackageName();
//            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
//            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
//                Intent intent = new Intent();
//                intent.setAction(android.provider.Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
//                intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
//                intent.setData(Uri.parse("package:" + packageName));
//                context.startActivity(intent);
//            }
//        }
        disposable += RxBus.listen(UpdateUserDataEvent::class.java).subscribe {
            viewModel.updateUserName()
            viewModel.updateUserRating()
        }

        disposable += RxBus.listen(UpdateUserImageEvent::class.java).subscribe {
            viewModel.updateUserImage()
        }

        disposable += RxBus.listen(SocketServiceEvent::class.java).subscribe { event ->
            when (event.command) {
                START -> {
                    viewModel.sendOnlineStatus(true)
                    DriverToolbar.IS_ONLINE_STATE = true
                }
                STOP -> {
                    //diconnectSockets()
                    DriverToolbar.IS_ONLINE_STATE = false
                    viewModel.sendOnlineStatus(false)
                }
            }
        }

        disposable += RxBus.listen(NewOrderInfoEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    viewModel.onNewOrderInfo(it.orderInfo)
                }

        disposable += RxBus.listen(ShowSecondaryOrderEvent::class.java).subscribe {
            viewModel.showSecondaryOrder()
        }

        disposable += RxBus.listen(DrawerLockEvent::class.java).subscribe {
            lockDrawer(it.isBlocked)
        }

        disposable += RxBus.listen(ShowDialogEvent::class.java).subscribe {
            createDialog(it.title, it.message, it.action)
        }

        disposable += RxBus.listen(ProfileModeEvent::class.java).subscribe { event ->
            appState = event.modeEvent
        }

        disposable += RxBus.listen(BadGpsEvent::class.java).subscribe { event ->
            Timber.d("IS NORMAL GPS %s", event.isGpsNormal)
            binding.offline.setVisible(!event.isGpsNormal)
        }

        disposable += RxBus.listen(BadInternetEvent::class.java).subscribe { event ->
            if (!event.isInternetNormal) {
                handler.postDelayed(runnable, 30000)
            } else {
                handler.removeCallbacks(runnable)
                binding.offlineSocket.setVisible(false)
            }

        }

        disposable += RxBus.listen(DisableOnlineStatusEvent::class.java).subscribe {
            if (!it.online)
                BeepUtil.playDriverStatus(false)
            viewModel.stopSocket()
        }
    }

    var handler: Handler = Handler()
    var runnable: Runnable = Runnable {
        binding.offlineSocket.setVisible(true)
    }

    private fun setUserImageToDrawer(uri: Uri? = null, url: String? = null, defaultRes: Int? = null) {
        val imageView = binding.navigationView.getHeaderView(0).find<ImageView>(R.id.user_image)
        when {
            url != null -> loadRoundedImage(this, imageView, url = url)
            uri != null -> loadRoundedImage(this, imageView, uri = uri)
            else -> imageView.setImageResource(defaultRes!!)
        }
    }

    var dialog: AlertDialog? = null

    private fun createDialog(title: String? = "", message: String, action: (() -> Unit)?): Dialog? {
        if (dialog != null) {
            dialog = AlertDialog.Builder(this).setTitle(title)
                    .setMessage(message)
                    .setPositiveButton(R.string.ok) { p0, _ ->
                        p0.dismiss()
                        action?.invoke()
                        dialog = null
                    }
                    .create()
            dialog!!.show()
        }
        return dialog
    }

    private fun setUserNameToDrawer(name: String?) {
        binding.navigationView.getHeaderView(0).find<TextView>(R.id.user_name).text = name
    }

    private fun setUserRatingToDrawer(rating: String?) {
        val ratingText = binding.navigationView.getHeaderView(0).find<TextView>(R.id.rating_text)
        val ratingImage = binding.navigationView.getHeaderView(0).find<ImageView>(R.id.rating_image)

        if (rating.equals("0.0") || rating.isNullOrBlank()) {
            ratingText.setVisible(false)
            ratingImage.setVisible(false)
        } else {
            ratingText.text = rating
            ratingText.setVisible(true)
            ratingImage.setVisible(true)
        }
    }

    private fun initServiceIntentListener() {
        val serviceIntentReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                viewModel.handleMessagingServiceIntent(intent.extras[TYPE_ID].toString().toInt())
            }
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(serviceIntentReceiver, IntentFilter(MessagingService.USER_BLOCKED_INTENT))
    }

//endregion

    companion object {
        const val FIREBASE_LOG_TAG = "FIREBASE_LOG"
        const val CODE_DRAW_OVER_OTHER_APP_PERMISSION = 909
        var isForeground: Boolean = false
        private const val REQUEST_CODE_FLEXI_UPDATE = 17362
    }
}
