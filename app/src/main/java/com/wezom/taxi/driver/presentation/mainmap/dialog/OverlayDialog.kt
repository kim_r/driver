package com.wezom.taxi.driver.presentation.mainmap.dialog

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import com.wezom.taxi.driver.R

class OverlayDialog : DialogFragment() {
    var listener: DialogClickListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(context!!, R.style.BlackDialogTheme)
                .setTitle(R.string.need_following_permissions)
                .setMessage(R.string.overlay_permission_message)
                .setPositiveButton(R.string.ok) { _, _ -> listener?.onPositiveClick() }
            .create()
    }

    companion object {
        const val TAG: String = "OverlayDialog"
    }

    interface DialogClickListener {
        fun onPositiveClick()
    }
}