package com.wezom.taxi.driver.presentation.base.lists.decorator

import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView

/**
 * Created by zorin.a on 07.11.2018.
 */
class DividerDecorator  constructor(private val drawable: Drawable,
                                           private val paddingLeft: Int = 0,
                                           private val paddingRight: Int = 0) : RecyclerView.ItemDecoration() {

    override fun onDrawOver(c: Canvas?, parent: RecyclerView?, state: RecyclerView.State?) {
        val left = parent?.paddingLeft?.plus(paddingLeft)
        val right = parent?.width?.minus(paddingRight)
        val childCount = parent?.childCount?.minus(1)
        for (i in 0..childCount!!) {
            val child = parent.getChildAt(i)
            val params = child.layoutParams as RecyclerView.LayoutParams
            val top = child.bottom.plus(params.bottomMargin)
            val bottom = top + drawable.intrinsicHeight
            drawable.setBounds(left!!, top, right!!, bottom)
            drawable.draw(c)
        }
    }
}