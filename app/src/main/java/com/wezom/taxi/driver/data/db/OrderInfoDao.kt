package com.wezom.taxi.driver.data.db

import android.arch.persistence.room.*
import com.wezom.taxi.driver.data.models.NewOrderInfo

@Dao
interface OrderInfoDao {
    @Query("SELECT * FROM  'order_info'")
    fun queryAll(): List<NewOrderInfo>

    @Query("SELECT * FROM 'order_info' WHERE id = :id")
    fun queryOrderInfo(id: Int): NewOrderInfo

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateOrderInfo(orderInfo: NewOrderInfo)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrderInfo(orderInfoList: List<NewOrderInfo>): List<Long>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrderInfo(orderInfo: NewOrderInfo): Long

    @Delete
    fun delete(orderInfo: NewOrderInfo)

    @Delete
    fun delete(listOfOrderInfo: List<NewOrderInfo>): Int

    @Query("DELETE FROM 'order_info' WHERE id = :id")
    fun deleteOrderById(id: Long)

    @Query("DELETE FROM 'order_info'")
    fun clearDatabase()


}