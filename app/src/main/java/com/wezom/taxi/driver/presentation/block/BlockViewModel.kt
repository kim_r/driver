package com.wezom.taxi.driver.presentation.block

import android.arch.lifecycle.MutableLiveData
import android.content.SharedPreferences
import com.wezom.taxi.driver.data.models.ResponseState
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.repository.DriverRepository
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.zipWith
import javax.inject.Inject

/**
 * Created by udovik.s on 20.04.2018.
 */
class BlockViewModel @Inject constructor(screenRouterManager: ScreenRouterManager, private val repository: DriverRepository, sharedPreferences: SharedPreferences) : BaseViewModel(screenRouterManager) {

    val helpEmailPhoneLiveData: MutableLiveData<Pair<String, String>> = MutableLiveData()
    val blockMessageLiveData: MutableLiveData<String> = MutableLiveData()
    val blockTimeLiveData: MutableLiveData<String> = MutableLiveData()
    val emptyTimeLiveData: MutableLiveData<String> = MutableLiveData()

    fun getUserStatus() {
        loadingLiveData.postValue(ResponseState(ResponseState.State.LOADING))
        App.instance.getLogger()!!.log("checkUserStatus start ")
        disposable += repository.checkUserStatus()
                .subscribe({ response ->
                    App.instance.getLogger()!!.log("checkUserStatus suc ")
                    handleResponseState(response)
                    if (response?.isSuccess!!) {
                        blockMessageLiveData.postValue(response.blockedMessage)
                        if (!response.blockedEndDate.isNullOrEmpty())
                            blockTimeLiveData.postValue(response.blockedEndDate)
                        else
                            emptyTimeLiveData.postValue("")
                    }
                }, {
                    App.instance.getLogger()!!.log("checkUserStatus error ")
                    loadingLiveData.postValue(ResponseState(ResponseState.State.NETWORK_ERROR))
                })
    }

    fun getHelpEmailAndPhone() {
        loadingLiveData.postValue(ResponseState(ResponseState.State.LOADING))
        App.instance.getLogger()!!.log("getHelpEmail start and end ")
        val helpEmail = repository.getHelpEmail()
        val userProfile = repository.profileAndRegistrationCorrectness()
        disposable += helpEmail.zipWith(userProfile) { email, profile ->
            Pair(email.email, profile.user!!.phone)
        }.subscribe({ response ->
            loadingLiveData.postValue(ResponseState(ResponseState.State.IDLE))
            helpEmailPhoneLiveData.postValue(Pair(response.first, response.second.toString()))
        }, {
            loadingLiveData.postValue(ResponseState(ResponseState.State.NETWORK_ERROR))
        })
    }
}