package com.wezom.taxi.driver.net.request

import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.Car
import com.wezom.taxi.driver.data.models.User

/**
 * Created by zorin.a on 10.12.2018.
 */
data class RegistrationRequest(@SerializedName("user") val user: User,
                               @SerializedName("car") val car: Car,
                               @SerializedName("orders_types") val type: ArrayList<Int>?) {
    constructor(user: User, car: Car) : this(user, car, null)
}