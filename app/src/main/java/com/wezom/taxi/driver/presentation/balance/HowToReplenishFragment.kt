package com.wezom.taxi.driver.presentation.balance

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.databinding.HowToReplenishBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.presentation.base.BaseFragment
import io.reactivex.rxkotlin.plusAssign

/**
 * Created by zorin.a on 15.03.2018.
 */
class HowToReplenishFragment : BaseFragment() {
    lateinit var binding: HowToReplenishBinding
    lateinit var viewModel: HowToReplenishViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
        setBackPressFun { viewModel.onBackPressed() }

        viewModel.userBalanceIdLiveData.observe(this, Observer { balanceIdValue ->
            binding.userId.text = balanceIdValue.toString()
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = HowToReplenishBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getBalanceId()
        binding.run {
            toolbar.setToolbarTitle(getString(R.string.how_to_replenish))
            disposable += RxView.clicks(goToPrivat24).subscribe { switchToSite(PRIVATE_URL) }
            disposable += RxView.clicks(privatLogo).subscribe { switchToSite(PRIVATE_URL) }
            disposable += RxView.clicks(whereToFindTerminal).subscribe {}
            disposable += RxView.clicks(moreInfoButton).subscribe { switchToSite(WEBSITE_URL) }
        }
    }

    private fun switchToSite(url: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        context?.startActivity(intent)
    }

    companion object {
        private const val PRIVATE_URL = "https://www.privat24.ua"
        private const val WEBSITE_URL = "https://wezom.com/"
    }
}