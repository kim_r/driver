package com.wezom.taxi.driver.net.request

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by zorin.a on 24.05.2018.
 */
class SetOnlineStatusRequest constructor(@SerializedName("isOnline") val isOnline: Boolean) : Serializable