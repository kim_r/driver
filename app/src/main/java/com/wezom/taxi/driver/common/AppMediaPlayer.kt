package com.wezom.taxi.driver.common

import android.content.Context
import android.media.MediaPlayer
import javax.inject.Inject

/**
 * Created by zorin.a on 20.09.2018.
 */
class AppMediaPlayer @Inject constructor(private val context: Context) {
    private var mediaPlayer: MediaPlayer? = null

    fun playSound(res: Int, isLooped: Boolean = false) {
        mediaPlayer = MediaPlayer.create(context, res)
        mediaPlayer?.isLooping = isLooped
        mediaPlayer?.start()
    }

    fun stopSound() {
        mediaPlayer?.stop()
        mediaPlayer = null
    }
}