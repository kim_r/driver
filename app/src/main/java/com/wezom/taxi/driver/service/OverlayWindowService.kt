package com.wezom.taxi.driver.service

import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.content.SharedPreferences
import android.graphics.PixelFormat
import android.os.Build
import android.os.IBinder
import android.os.PowerManager
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatDelegate
import android.view.Gravity.TOP
import android.view.WindowManager
import android.widget.Toast
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.common.AppMediaPlayer
import com.wezom.taxi.driver.common.LocationStorage
import com.wezom.taxi.driver.common.OrderManager
import com.wezom.taxi.driver.common.checkIsSystemOverlayEnabled
import com.wezom.taxi.driver.data.db.DbManager
import com.wezom.taxi.driver.data.models.Accepted
import com.wezom.taxi.driver.data.models.NewOrderInfo
import com.wezom.taxi.driver.data.models.OrderFinalization
import com.wezom.taxi.driver.ext.SOUND_ENABLED
import com.wezom.taxi.driver.ext.THEME_MODE
import com.wezom.taxi.driver.net.api.ApiManager
import com.wezom.taxi.driver.net.request.AcceptOrderRequest
import com.wezom.taxi.driver.net.request.SkipOrderRequest
import com.wezom.taxi.driver.presentation.acceptorder.list.AddressAdapter
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.customview.OrderFoundOverlayWindow
import com.wezom.taxi.driver.presentation.main.MainActivity
import dagger.android.DaggerService
import timber.log.Timber
import java.lang.ref.WeakReference
import javax.inject.Inject


/**
 * Created by zorin.a on 25.09.2018.
 */
class OverlayWindowService : DaggerService() {

    @Inject
    lateinit var apiManager: ApiManager
    @Inject
    lateinit var orderManager: OrderManager
    @Inject
    lateinit var appMediaPlayer: AppMediaPlayer
    @Inject
    lateinit var sharedPreferences: SharedPreferences
    @Inject
    lateinit var locationStorage: LocationStorage
    @Inject
    lateinit var adapter: AddressAdapter
    @Inject
    lateinit var dbManager: DbManager

    private lateinit var windowManager: WindowManager
    private lateinit var powerManager: PowerManager
    private var overlayWindow: OrderFoundOverlayWindow? = null
    private var isSoundEnabled: Boolean = true

    override fun onCreate() {
        super.onCreate()
        Timber.d("onCreate")
        windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        powerManager = getSystemService(Context.POWER_SERVICE) as PowerManager
        isSoundEnabled = sharedPreferences.getBoolean(SOUND_ENABLED, true)

        wakeScreen()
        if (checkIsSystemOverlayEnabled(this)) {
            addOverlayWindow()
        }
        Timber.d("isSoundEnabled:$isSoundEnabled")
        if (isSoundEnabled) {
            appMediaPlayer.playSound(R.raw.new_order_sound, true)
        }
    }

    override fun attachBaseContext(base: Context?) {
        val themeMode = PreferenceManager.getDefaultSharedPreferences(base)
                .getInt(THEME_MODE,
                        AppCompatDelegate.MODE_NIGHT_YES)
        AppCompatDelegate.setDefaultNightMode(themeMode)
        super.attachBaseContext(base)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Timber.d("onStartCommand")

        intent!!.extras?.getParcelable<NewOrderInfo>(OverlayWindowService.KEY)?.let { it ->
            overlayWindow?.setModel(it)
        }
        return Service.START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.d("onDestroy")
        overlayWindow?.let {
            windowManager.removeView(it)
            appMediaPlayer.stopSound()
            overlayWindow = null
        }
    }

    override fun onBind(p0: Intent?): IBinder? = null


    private fun addOverlayWindow() {
        val type = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
        } else {
            WindowManager.LayoutParams.TYPE_PHONE
        }

        val params = WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                type,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT)
        params.gravity = TOP
        params.x = 0
        params.y = resources.getDimension(R.dimen.indent_huge).toInt()

        overlayWindow = OrderFoundOverlayWindow(baseContext).apply {
            setClickListener(object : OverlayWindowClickListener {
                override fun onPositiveClick(model: NewOrderInfo) {
                    Timber.d("onPositiveClick")
                    acceptOrder(model,
                            locationStorage.getCachedLocation().longitude,
                            locationStorage.getCachedLocation().latitude)
                    stopSelf()
                }

                @SuppressLint("CheckResult")
                override fun onNegativeClick(model: NewOrderInfo) {
                    Timber.d("onNegativeClick")
                    dismissOrder(model)
                }

                override fun onTimeRunOver() {
                    Timber.d("onTimeRunOver")
                    stopSelf()
                }
            })
        }
        overlayWindow!!.adapter = adapter
        windowManager.addView(overlayWindow, params)
    }


    @SuppressLint("InvalidWakeLockTag")
    private fun wakeScreen() {
        val isScreenOn = powerManager.isInteractive
        Timber.d("isScreenOn: $isScreenOn")
        if (!isScreenOn) {
            val wl = powerManager.newWakeLock(
                    PowerManager.FULL_WAKE_LOCK
                            or PowerManager.ACQUIRE_CAUSES_WAKEUP
                            or PowerManager.ON_AFTER_RELEASE, "HUB_SCREENLOCK")
            wl.acquire(2000)
        }
    }

    @SuppressLint("CheckResult")
    fun acceptOrder(orderInfo: NewOrderInfo, lon: Double, lat: Double) {
        Timber.d("acceptOrder")

        val request = AcceptOrderRequest(longitude = lon, latitude = lat, eventTime = System.currentTimeMillis())
        App.instance.getLogger()!!.log("acceptOrder start ")
        apiManager.acceptOrder(orderInfo.order!!.id!!, request).subscribe({ response ->
            TaxometerService.stop(WeakReference(baseContext))
            if (response.isSuccess!!) {
                Timber.d("acceptOrderResponse: $response")
                App.instance.getLogger()!!.log("acceptOrder suc ")
//                dbManager.deleteOrderResult(orderInfo.id)
                if (response?.isSuccess!!) {
                    App.instance.getLogger()!!.log("currentOrder start ")
                    apiManager.currentOrder(orderInfo.order!!.id!!).subscribe({ currentOrder ->
                        App.instance.getLogger()!!.log("currentOrder suc ")
                        Timber.d("OrderAccept ${orderInfo.order!!.distance}")
                        currentOrder.order!!.distanceFromDriver = orderInfo.order!!.distance!! * 1000
                        orderInfo.order = currentOrder.order

                        orderManager.acceptOrder(orderInfo)
                        var finalization = orderManager.queryFinalization()

                        if (finalization == null) {
                            finalization = OrderFinalization()
                        }
                        finalization.accepted = Accepted(
                                longitude = lon,
                                latitude = lat,
                                eventTime = System.currentTimeMillis(),
                                driverId = orderInfo.order?.user?.id!!)

                        orderManager.insertFinalization(finalization)
                        val intent = Intent(this, MainActivity::class.java).apply {
                            addFlags(FLAG_ACTIVITY_NEW_TASK)
                            addFlags(FLAG_ACTIVITY_CLEAR_TASK)
                        }
                        startActivity(intent)
                    }, { App.instance.getLogger()!!.log("currentOrder error ") })
                }
            } else {
                Toast.makeText(this,
                        response.error!!.message,
                        Toast.LENGTH_LONG)
                        .show()
            }
        }, { App.instance.getLogger()!!.log("acceptOrder error ") }) //don nothing on error
    }

    @SuppressLint("CheckResult")
    private fun dismissOrder(model: NewOrderInfo) {
        App.instance.getLogger()!!.log("skipOrder start ")
        apiManager.skipOrder(model.order!!.id!!, SkipOrderRequest(System.currentTimeMillis())).subscribe({
            stopSelf()
            App.instance.getLogger()!!.log("skipOrder suc ")
        }, {
            stopSelf()
            App.instance.getLogger()!!.log("skipOrder error ")
        })
    }

    companion object {
        const val TAG = "OverlayWindowService"
        private const val KEY = "payload"

        fun startService(context: WeakReference<Context>, orderInfo: NewOrderInfo) {
            Timber.d("OVERLAY_SERVICE start")
            val intent = Intent(context.get(), OverlayWindowService::class.java)
            intent.putExtra(OverlayWindowService.KEY, orderInfo)
            context.get()?.startService(intent)
        }

        fun stopService(context: WeakReference<Context>) {
            Timber.d("OVERLAY_SERVICE stop")
            val intent = Intent(context.get(), OverlayWindowService::class.java)
            context.get()?.stopService(intent)
        }
    }

    interface OverlayWindowClickListener {
        fun onPositiveClick(model: NewOrderInfo)
        fun onNegativeClick(model: NewOrderInfo)
        fun onTimeRunOver()
    }

}