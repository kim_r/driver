package com.wezom.taxi.driver.data.models

import com.google.gson.annotations.SerializedName

data class CloseOrderAuto(
        @SerializedName("orderId") val orderId: Int?
)