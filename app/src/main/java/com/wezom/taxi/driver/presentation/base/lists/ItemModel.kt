package com.wezom.taxi.driver.presentation.base.lists

/**
 * Created by zorin.a on 14.03.2018.
 */
interface ItemModel<in T> {
    fun setData(data: T)
}