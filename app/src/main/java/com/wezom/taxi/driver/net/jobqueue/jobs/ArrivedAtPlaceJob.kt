package com.wezom.taxi.driver.net.jobqueue.jobs

import android.annotation.SuppressLint
import com.wezom.taxi.driver.net.request.DriverEventRequest
import com.wezom.taxi.driver.presentation.app.App

/**
 * Created by zorin.a on 07.05.2018.
 */
class ArrivedAtPlaceJob constructor(val id: Int, val request: DriverEventRequest) : BaseJob() {

    @SuppressLint("CheckResult")
    override fun onRun() {
        App.instance.getLogger()!!.log("arrivedAtPlace start and end ")
        apiManager.arrivedAtPlace(id, request).blockingGet()
    }
}