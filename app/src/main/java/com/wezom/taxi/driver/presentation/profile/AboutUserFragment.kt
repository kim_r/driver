package com.wezom.taxi.driver.presentation.profile

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Bundle
import android.telephony.PhoneNumberFormattingTextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.bus.events.UpdateCardDataEvent
import com.wezom.taxi.driver.bus.events.UpdateCardEvent
import com.wezom.taxi.driver.bus.events.UserValidityEvent
import com.wezom.taxi.driver.common.Constants.ADD_CARD
import com.wezom.taxi.driver.common.Constants.REQUEST_IMAGE_CAPTURE
import com.wezom.taxi.driver.common.Constants.REQUEST_IMAGE_PICK
import com.wezom.taxi.driver.data.models.User
import com.wezom.taxi.driver.data.models.UserRequisitesCorrectness
import com.wezom.taxi.driver.databinding.AboutUserBinding
import com.wezom.taxi.driver.ext.*
import com.wezom.taxi.driver.presentation.base.BaseFragment
import com.wezom.taxi.driver.presentation.profile.ProfileFragment.Companion.MODE_KEY
import com.wezom.taxi.driver.presentation.profile.events.ProfileActionClickEvent
import com.wezom.taxi.driver.presentation.profile.events.UserDetailsEvent
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber
import java.io.File


/**
 * Created by zorin.a on 28.02.2018.
 */

class AboutUserFragment : BaseFragment() {
    //region var
    private var mode: Int? = 0
    private var user: User? = null
    private var userCorrectness: UserRequisitesCorrectness? = null
    private var photoFile: File? = null
    private var isPhotoRequired: Boolean = false

    private lateinit var viewModel: AboutUserViewModel
    private lateinit var binding: AboutUserBinding

    private var phoneObserver = Observer<String> { data ->
        phoneString = data
        binding.phoneEditText.setText(phoneString)
    }

    private var name: String? = null
    private var surname: String? = null
    private var phoneString: String? = null
    private var mail: String? = null
    private var cardNumbers: String? = null
    private var inviteCode: String? = null
    private var photoUri: Uri? = null

//    private val userImageCallbacks: Callbacks  by lazy {
//        object : Callbacks {
//            override fun onGalleryClicked() {
//                pickPicture()
//            }
//
//            override fun onWatchClicked() {
//                if (photoUri == null) {
//                    if (binding.avatar.drawable is BitmapDrawable) {
//                        getBitmapFromImage(binding.avatar)?.let {
//                            val uri = storeBitmapToDisk(context!!, it, TEMP_BITMAP)
//                            uri.let {
//                                cropImage(it.toString())
//                            }
//                        }
//                    }
//                } else {
//                    cropImage(photoUri.toString())
//                }
//            }
//
//            override fun onCameraClicked() {
//                photoFile = takePicture()
//            }
//
//            override fun onSettingsOpen() {
//                openAppSettings()
//            }
//        }
//    }
    //endregion

    //region override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()

        mode = arguments?.getInt(ProfileFragment.MODE_KEY)
        user = arguments?.getParcelable(USER_KEY)
        userCorrectness = arguments?.getParcelable(CORRECTNESS_KEY)
        viewModel.phoneLiveData.observe(this, phoneObserver)
//        viewModel.userAvatarLiveData.observe(this, userImageObserver)
//        viewModel.croppedProfileImageLiveData.observe(this, croppedImageObserver)
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = AboutUserBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        when (mode) {
            ProfileFragment.MODE_PROFILE -> setAsProfile()
//            ProfileFragment.MODE_REGISTRATION -> setAsRegistration()
            ProfileFragment.MODE_MODERATION -> setAsModeration()
            ProfileFragment.MODE_NEED_COMPLETE -> setAsProfile()
        }

//        setUserImage()
        sharedClicks()
        sharedListeners()
        checkCorrectness()
    }

//    private fun setUserImage() {
//        if (user?.photo != null && user?.photo!!.isNotBlank() && photoUri == null) {
//            loadRoundedImage(context!!.applicationContext, binding.avatar, url = user?.photo)
//        } else {
//            viewModel.getUserImageUri()
//        }
//    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                REQUEST_IMAGE_PICK -> {
                    val uri = data?.data
                    uri.let {
                        cropImage(it.toString())
                    }
                }
                REQUEST_IMAGE_CAPTURE -> {
                    val uri = Uri.fromFile(photoFile)
                    uri.let {
                        cropImage(it.toString())
                    }
                }
                ADD_CARD -> {
                    RxBus.publish(UpdateCardEvent())
                }
            }
        }
    }
    //endregion

    //region fun

    private fun sharedListeners() {
        disposable += RxBus.listen(UpdateCardDataEvent::class.java).subscribe {
            if (it.user.cardNumbers.isNotEmpty())
                binding.cardNumber.text = it.user.cardNumbers[0].number
        }

        binding.run {
            phoneEditText.addTextChangedListener(PhoneNumberFormattingTextWatcher())

            disposable += RxTextView.textChangeEvents(nameEditText).subscribe { value ->
                name = value.text().toString()
                publishUser()
            }

            disposable += RxTextView.textChangeEvents(surnameEditText).subscribe { value ->
                surname = value.text().toString()
                publishUser()
            }

            disposable += RxTextView.textChangeEvents(phoneEditText).subscribe { value ->
                phoneString = value.text().toString()
                publishUser()
            }

            disposable += RxTextView.textChangeEvents(emailEditText).subscribe { value ->
                mail = value.text().toString()
                publishUser()
            }

            disposable += RxTextView.textChangeEvents(inviteEditText).subscribe { value ->
                inviteCode = value.text().toString()
                publishUser()
            }

            disposable += RxBus.listen(UserValidityEvent::class.java).subscribe { validity ->
                if (!validity.nameCorrect) {
                    setErrorColor(nameEditText)
                }
                if (!validity.surnameCorrect) {
                    setErrorColor(surnameEditText)
                }
//                highLightRequiredImage(isPhotoRequired)
                //                if (!validity.mailCorrect) {
                //                    emailFault.setVisible(true)
                //                    setErrorColor(emailEditText)
                //                } else {
                //                    emailFault.setVisible(false)
                //                }
            }
        }
    }

    private fun setErrorColor(view: View) {
        val background = view.background
        background.setColorFilter(resources.getColor(R.color.colorRed), PorterDuff.Mode.SRC_ATOP)
        view.background = background
    }

    private fun sharedClicks() {
        binding.run {
            disposable += RxView.clicks(saveButton).subscribe {
                publishUser()
                RxBus.publish(ProfileActionClickEvent(ProfileActionClickEvent.ActionType.SAVE_USER_DATA))
            }

//            disposable += RxView.clicks(avatar).subscribe {
//                val dialog = ChoosePhotoSourceDialog.newInstance(photoUri != null || !user?.photo.isNullOrBlank())
//                dialog.callbacks = imageCallbacks
//                dialog.show(childFragmentManager, ChoosePhotoSourceDialog.TAG)
//            }
        }
    }

    private fun setAsModeration() {
        binding.run {
            inviteRow.setVisible(false)
            nameEditText.setText(user?.name)
            surnameEditText.setText(user?.surname)
            phoneEditText.setText(user?.phone)
            emailEditText.setText(user?.email)
            idValue.text = user?.balanceId.toString()
            if (user?.cardNumbers != null && user?.cardNumbers?.size!! > 0)
                cardNumber.text = user?.cardNumbers?.get(0)!!.number
//            aboutUserInfo.setVisible(false)
//            rightChevron.setVisible(false)

            disposable += RxView.clicks(userIdRow).subscribe {
                viewModel.switchToAddCardScreen(this@AboutUserFragment)
            }

            phoneEditText.isFocusable = false
            disposable += RxView.clicks(binding.phoneEditText).subscribe {
                viewModel.switchToChangePhoneScreen(binding.phoneEditText.text.toString())
            }
        }

        name = user?.name
        surname = user?.surname
        phoneString = user?.phone
        mail = user?.email
        if (user?.cardNumbers != null && user?.cardNumbers?.size!! > 0)
            cardNumbers = user?.cardNumbers?.get(0)!!.number
    }

    private fun setAsRegistration() {
//        viewModel.saveUserImageUri("")
        binding.run {
            viewModel.getRequiredDocs()
            viewModel.requiredDocumentLiveData.observe(this@AboutUserFragment, Observer<Boolean> { isPhotoRequired ->
                this@AboutUserFragment.isPhotoRequired = isPhotoRequired!!
            })
            user = User()

            userIdRow.setVisible(false)
            inviteRow.setVisible(true)
//            nextIcon.setVisible(true)
//            rightChevron.setVisible(false)
            phoneRow.setVisible(false)
//            next.text = getString(R.string.next)
//            disposable += RxView.clicks(aboutUserInfo).subscribe {
//                showInfoDialog(R.drawable.ic_your_photo_vector, R.string.your_photo, R.string.take_your_photo)
//            }
            viewModel.getUserPhone()
        }
    }

//    private fun highLightRequiredImage(isPhotoRequired: Boolean) {
//        binding.run {
//            isUserRegistrationImageOk = !isPhotoRequired
//            if (photoUri == null) {
//                avatarBorder.setVisible(this@AboutUserFragment.isPhotoRequired)
//                avatarFault.setVisible(this@AboutUserFragment.isPhotoRequired)
//            } else {
//                avatarBorder.setVisible(false)
//                avatarFault.setVisible(false)
//                isUserRegistrationImageOk = true
//            }
//        }
//    }

    private fun publishUser() {
        RxBus.publish(UserDetailsEvent(
                name = name.toString(),
                surname = surname.toString(),
                phone = phoneString.toString(),
                mail = mail.toString(),
                inviteCode = inviteCode.toString(),
                photoUri = photoUri))
    }

    private fun setAsProfile() {
        binding.run {
            inviteRow.setVisible(false)
//            nextIcon.setVisible(false)
//            next.setText(R.string.save)
            nameEditText.setText(user?.name)
            surnameEditText.setText(user?.surname)
            phoneEditText.setText(user?.phone)
            emailEditText.setText(user?.email)
            idValue.text = user?.balanceId.toString()
            if (user?.cardNumbers != null && user?.cardNumbers?.size!! > 0)
                cardNumber.text = user?.cardNumbers?.get(0)!!.number
                        ?: getString(R.string.card_number_holder)
//            aboutUserInfo.setVisible(false)
//            rightChevron.setVisible(false)
//            nextIcon.setVisible(false)

            disposable += RxView.clicks(userIdRow).subscribe {
                Timber.d("ADD_CARD: userIdRow click()")
                viewModel.switchToAddCardScreen(this@AboutUserFragment)
            }

            phoneEditText.isFocusable = false
            disposable += RxView.clicks(phoneEditText).subscribe {
                viewModel.switchToChangePhoneScreen(phoneEditText.text.toString())
            }
        }

        name = user?.name
        surname = user?.surname
        phoneString = user?.phone
        mail = user?.email
        if (user?.cardNumbers != null && user?.cardNumbers?.size!! > 0)
            cardNumbers = user?.cardNumbers?.get(0)!!.number
    }

    private fun cropImage(uri: String?) {
        uri?.let {
            if (it.isNotEmpty()) {
                viewModel.cropImage(it)
            }
        }
    }

    private fun checkCorrectness() {
        if (userCorrectness != null) {
            binding.run {
                //                avatarBorder.setVisible(!userCorrectness?.isPhotoCorrect!!)
//                avatarFault.setVisible(!userCorrectness?.isPhotoCorrect!!)
//                nameFault.setVisible(!userCorrectness?.isNameCorrect!!)
//                surnameFault.setVisible(!userCorrectness?.isSurnameCorrect!!)
//                emailFault.setVisible(!userCorrectness?.isEmailCorrect!!)
            }
        }
    }
    //endregion

    companion object {
        const val USER_KEY = "user_key"
        const val CORRECTNESS_KEY = "user_correctness_key"

        var isUserRegistrationImageOk: Boolean = false
        fun newInstance(mode: Int, user: User? = null, userRequisitesCorrectness: UserRequisitesCorrectness? = null): AboutUserFragment {
            val args = Bundle()
            args.putInt(MODE_KEY, mode)
            if (user != null) {
                args.putParcelable(USER_KEY, user)
            }
            if (userRequisitesCorrectness != null) {
                args.putParcelable(CORRECTNESS_KEY, userRequisitesCorrectness)
            }
            val fragment = AboutUserFragment()
            fragment.arguments = args
            return fragment
        }
    }
}