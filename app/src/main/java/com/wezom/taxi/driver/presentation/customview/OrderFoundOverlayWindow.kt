package com.wezom.taxi.driver.presentation.customview

import android.content.Context
import android.content.res.Resources
import android.graphics.Color
import android.preference.PreferenceManager
import android.support.constraint.ConstraintLayout
import android.support.v7.app.AppCompatDelegate
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.common.*
import com.wezom.taxi.driver.data.models.NewOrderInfo
import com.wezom.taxi.driver.data.models.PaymentMethod
import com.wezom.taxi.driver.databinding.OverlayWindowBinding
import com.wezom.taxi.driver.ext.THEME_MODE
import com.wezom.taxi.driver.ext.setVisible
import com.wezom.taxi.driver.presentation.acceptorder.AcceptOrderFragment
import com.wezom.taxi.driver.presentation.acceptorder.list.AddressAdapter
import com.wezom.taxi.driver.service.OverlayWindowService.OverlayWindowClickListener
import timber.log.Timber
import java.text.DecimalFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.roundToInt

/**
 * Created by zorin.a on 25.09.2018.
 */
class OrderFoundOverlayWindow : ConstraintLayout {
    lateinit var overlayWindowClickListener: OverlayWindowClickListener
    private lateinit var model: NewOrderInfo

    lateinit var adapter: AddressAdapter


    constructor(context: Context) : super(context) {
    }

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet)
    constructor(context: Context, attrs: AttributeSet?, attributeSetId: Int) : super(context, attrs, attributeSetId)

    private var binding = OverlayWindowBinding.inflate(LayoutInflater.from(context), this, true)

    init {
        binding.run {
            acceptButton.setOnClickListener {
                overlayWindowClickListener.onPositiveClick(model)
            }

            cancelButton.setOnClickListener {
                overlayWindowClickListener.onNegativeClick(model)
            }
        }
    }

    fun setClickListener(overlayWindowClickListener: OverlayWindowClickListener) {
        this.overlayWindowClickListener = overlayWindowClickListener
    }

    private fun initRecyclerView(orderInfo: NewOrderInfo) {
        val order = orderInfo.order
        order?.coordinates?.let {
            val adapterData = Pair(it.toMutableList(),
                    AddressAdapter.FromToDistance(from = DecimalFormat("#0.0").format(order.distance!!).replace(",", "."),
                            to = order.calculatedPath.toString()))
            adapter.addAll(adapterData)
        }
        binding.addressList.adapter = adapter
    }

    fun setModel(orderInfo: NewOrderInfo) {
        model = orderInfo
        Timber.d("setModel() $orderInfo")
        val themeMode = PreferenceManager.getDefaultSharedPreferences(context)
                .getInt(THEME_MODE,
                        AppCompatDelegate.MODE_NIGHT_YES)
        binding.run {
            //            viewsVisibilitySwitch(orderInfo.order?.newOrderShowing?.isRatingShow!!, ratingImage, ratingLabel)
//            val ratingValue = orderInfo.order?.user?.rating
//            if (ratingValue == null || ratingValue == 0.0f) {
//                viewsVisibilitySwitch(false, ratingImage, ratingLabel)
//            } else {
//                ratingLabel.text = ratingValue.toString()
//                ratingImage.setImageResource(R.drawable.ic_star_24_x_24)
//            }
//
//            if (orderInfo.order?.carArrival == 0) {
//                timeTextView.setVisible(false)
//            } else {
//                timeTextView.setVisible(true)
//                orderInfo.order?.preliminaryTime?.let {
//                    timeTextView.setTime(formatTime(it))
//                }
//            }
//
//            viewsVisibilitySwitch(orderInfo.order?.newOrderShowing?.isCurrentCoefficientShow!!, coefficientContainer)
//
//            val coef = orderInfo.order?.currentCoefficient
//            if (coef != null && coef <= 1.0) {
//                viewsVisibilitySwitch(false, coefficientContainer)
//            }
//            if (coef == null) {
//                viewsVisibilitySwitch(false, coefficientContainer)
//            }
//
//            coefficientValue.text = coef.toString()
//
//            val rate = orderInfo.order?.kilometerPrice
//            rateValue.text = rate?.roundToInt().toString()
//            viewsVisibilitySwitch(orderInfo.order?.newOrderShowing?.isKilometerPriceShow!!, rateValue, rateMeasureLabel)
//            if (rate == null) {
//                viewsVisibilitySwitch(false, rateValue, rateMeasureLabel)
//            }
//            if (rate == 0.0) {
//                viewsVisibilitySwitch(false, rateValue, rateMeasureLabel)
//            }
//
//            var estCostValue: Double = orderInfo.order?.payment?.estimatedCost?.plus(orderInfo.order?.payment?.surcharge!!)
//                    ?: 0.0
//            val estBonus: Double = orderInfo.formula?.estimatedBonuses ?: 0.0
//            estCostValue += estBonus
//            viewsVisibilitySwitch(estCostValue!! > 0, priceValue, payMethodImage, uahCurrencyImage)
//
//
//            priceValue.text = Math.round(estCostValue).toString()
//            viewsVisibilitySwitch(orderInfo.order?.newOrderShowing?.isEstimatedCostShow!!, priceValue, payMethodImage, uahCurrencyImage)
//
//            payMethodImage.setImageResource(getPaymentResource(orderInfo.order?.payment?.method!!))
//
//            viewsVisibilitySwitch((orderInfo.order?.newOrderShowing?.isRatingShow!! or
//                    orderInfo.order?.newOrderShowing?.isCurrentCoefficientShow!! or
//                    orderInfo.order?.newOrderShowing?.isKilometerPriceShow!! or
//                    orderInfo.order?.newOrderShowing?.isMethodShow!! or
//                    orderInfo.order?.newOrderShowing?.isEstimatedCostShow!!), topContainer)
//
//            val answerTimeMillis = orderInfo.order?.answerTime!! //~15 sec default
//            Timber.d("_TIME answerTime: ${Date(answerTimeMillis)}")
//            val serverTimeMillis = orderInfo.order?.serverTime!!
//            Timber.d("_TIME serverTime: ${Date(serverTimeMillis)}")
//            val timerTime = calcTimerTime(serverTimeMillis, answerTimeMillis)
//            Timber.d("_TIME timer time millis: $timerTime")
//            Timber.d("_TIME timer time seconds: ${TimeUnit.MILLISECONDS.toSeconds(timerTime)}")
//            val progress = LinearProgressBar(context)
//            progress.startTimer(timerTime)
//            progress.listener = object : LinearProgressBar.DoneListener {
//                override fun onComplete() {
//                    progress.stopTimer()
//                    overlayWindowClickListener.onTimeRunOver()
//                }
//            }
            if (themeMode == 2) {
                topContainer.setBackgroundColor(Color.parseColor("#434343"))
                addressList.setBackgroundColor(Color.parseColor("#525252"))
                servicesContainer.setBackgroundColor(Color.parseColor("#434343"))
                comment.setBackgroundColor(Color.parseColor("#434343"))
                comment.setTextColor(Color.parseColor("#e0e0e0"))
                userName.setTextColor(Color.parseColor("#e0e0e0"))
                ratingLabel.setTextColor(Color.parseColor("#e0e0e0"))
                servicesLabel.setTextColor(Color.parseColor("#e0e0e0"))
                timeTextView.getText().setTextColor(Color.parseColor("#e0e0e0"))

            }

            if (orderInfo.order?.coordinates!!.size > 1) {
                var estCostValue: Double =
                        orderInfo.order?.payment?.estimatedCost?.plus(orderInfo.order?.payment?.surcharge!!)
                                ?: 0.0
                val estBonus: Double = orderInfo.formula?.estimatedBonuses ?: 0.0
                estCostValue += estBonus
                priceValue.text = estCostValue.roundToInt()
                        .toString()
            } else {
                rateValue.visibility = View.GONE
                rateMeasureLabel.visibility = View.GONE
                priceValue.text = context!!.resources.getString(R.string.auto_check)
            }

            orderInfo.order?.payment?.method?.let { com.wezom.taxi.driver.common.getPaymentResource(it) }
                    ?.let { payMethodImage?.setImageResource(it) }

            orderInfo.order?.user?.rating?.let {
                ratingLabel.text = it.toString()
            }


            userImage.let {
                loadRoundedImage(context = context!!,
                        view = it,
                        url = orderInfo.order?.user?.photo,
                        placeHolderId = R.drawable.ic_ava_vector40px)
            }
//            userImage.setOnClickListener {
//                orderInfo.order?.user?.let { it1 -> viewModel.showUser(it1) }
//            }

            positiveDrivesLabel.text =
                    orderInfo.order?.user?.doneOrdersCount.toString()
            negativeDrivesLabel.text =
                    orderInfo.order?.user?.undoneOrdersCount.toString()

            if (!orderInfo.order?.user?.name.isNullOrEmpty()) {
                userName.text = orderInfo.order?.user?.name
            } else {
                userName.text = context.getString(R.string.name_client)
            }

            orderInfo.order?.services?.let {
                if (it.isNotEmpty() && AcceptOrderFragment.checkService(it)) {
                    services.setActiveStatus(true)
                    services.addServices(*it.toIntArray())
                    services.setVisible(true)
                    servicesLabel.setVisible(true)
                } else {
                    servicesContainer.setVisible(false)
                }
            }

            if (orderInfo.order?.carArrival == 0) {
                timeTextView.setVisible(false)
            } else {
                orderInfo.order?.preliminaryTime?.let {
                    //set time
                    val time = formatTime(it)
                    Timber.d("TIME_: $time")
                    timeTextView.setTime(time)
//                    timeTextView?.setOnClickListener {
//                        val dialog = PreliminaryTimeDialog.newInstance(time)
//                        dialog.show(fragmentManager,
//                                PreliminaryTimeDialog.TAG)
//                    }
                    servicesContainer.setVisible(true)
                }
            }
            if (!orderInfo.order?.comment.isNullOrEmpty()) {
                comment.text = orderInfo.order?.comment
            } else {
                comment.setVisible(false)
            }
            if (orderInfo.order?.kilometerPrice != 0.0) {
                rateValue.text = orderInfo.order?.kilometerPrice?.let { if (it != 0.0) it.roundToInt().toString() else it.toString() }
            } else {
                rateValue.setVisible(false)
                rateMeasureLabel.setVisible(false)
            }

            val params: ConstraintLayout.LayoutParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)
            params.bottomToBottom = topContainer.id
            params.endToStart = payMethodImage.id
            params.topToTop = topContainer.id
//            if (orderInfo.order?.coordinates!!.size > 1) {
//                params.setMargins(0, 0,
//                        pxFromDp(rateValue.resources, ((rateValue.text.toString().length - 1) * 32f)).toInt(), 0)
//            }else {
                if (Locale.getDefault().language == "uk") {
                    params.setMargins(0, 0, pxFromDp(rateValue.resources, 110f).toInt() -
                            pxFromDp(rateValue.resources, ((rateValue.text.toString().length - 1) * 10f)).toInt(), 0)
                } else {
                    params.setMargins(0, 0, (pxFromDp(rateValue.resources, 71f) -
                            pxFromDp(rateValue.resources, ((rateValue.text.toString().length - 1) * 10f))).toInt(), 0)
                }
//            }
            rateMeasureLabel.layoutParams = params

            val answerTimeMillis = orderInfo.order?.answerTime!! //~15 sec default
            Timber.d("_TIME answerTime: ${Date(answerTimeMillis)}")
            val serverTimeMillis = orderInfo.order?.serverTime!!
            Timber.d("_TIME serverTime: ${Date(serverTimeMillis)}")
            val timerTime = calcTimerTime(serverTimeMillis, answerTimeMillis)
            Timber.d("_TIME timer time millis: $timerTime")
            Timber.d("_TIME timer time seconds: ${TimeUnit.MILLISECONDS.toSeconds(timerTime)}")
            val progress = LinearProgressBar(context)
            progress.startTimer(timerTime)
            progress.listener = object : LinearProgressBar.DoneListener {
                override fun onComplete() {
                    progress.stopTimer()
                    overlayWindowClickListener.onTimeRunOver()
                }
            }
            initRecyclerView(orderInfo)
        }
    }

    fun pxFromDp(resources: Resources, dp: Float): Float {
        return (dp * resources.getDisplayMetrics().density)
    }

    private fun getPaymentResource(payment: PaymentMethod): Int {
        return when (payment) {
            PaymentMethod.CASH -> R.drawable.ic_cash
            PaymentMethod.CASH_AND_BONUS -> R.drawable.ic_cash
            PaymentMethod.CARD -> R.drawable.ic_card
            PaymentMethod.CARD_AND_BONUS -> R.drawable.ic_card
        }
    }
}