package com.wezom.taxi.driver.data.models

import android.arch.persistence.room.TypeConverters
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.converter.UserConverter

/**
 * Created by zorin.a on 021 21.02.18.
 */

data class User(
        @SerializedName("id") var id: Int?,
        @SerializedName("name") var name: String?,
        @SerializedName("surname") var surname: String?,
        @SerializedName("email") var email: String?,
        @SerializedName("phone") var phone: String?,
        @SerializedName("photo") var photo: String?,
        @SerializedName("photo_original") var photoOrigin: String?,
        @SerializedName("balanceId") var balanceId: Int?,

        @SerializedName("cardNumbers")
        @TypeConverters(UserConverter::class)
        var cardNumbers: List<Card>,
        @SerializedName("invitationCode") var invitationCode: String?,
        @SerializedName("isRated") var isRated: Boolean?,
        @SerializedName("rating") var rating: Float?,
        @SerializedName("totalRating") var totalRating: Float?,
        @SerializedName("comment") var comment: String?,
        @SerializedName("doneOrdersCount") var doneOrdersCount: Int?,
        @SerializedName("notComeOrdersCount") var undoneOrdersCount: Int?

) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.createTypedArrayList(Card),
            parcel.readString(),
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Float::class.java.classLoader) as? Float,
            parcel.readValue(Float::class.java.classLoader) as? Float,
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int) {
    }

    constructor() : this(0, "", "", "", "", "", "", 0, ArrayList<Card>(), "", false, 0.0F, 0.0F, "", 0, 0)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(name)
        parcel.writeString(surname)
        parcel.writeString(email)
        parcel.writeString(phone)
        parcel.writeString(photo)
        parcel.writeString(photoOrigin)
        parcel.writeValue(balanceId)
        parcel.writeTypedList(cardNumbers)
        parcel.writeString(invitationCode)
        parcel.writeValue(isRated)
        parcel.writeValue(rating)
        parcel.writeValue(totalRating)
        parcel.writeString(comment)
        parcel.writeValue(doneOrdersCount)
        parcel.writeValue(undoneOrdersCount)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }


}
