package com.wezom.taxi.driver.net.request

import com.google.gson.annotations.SerializedName


/**
 * Created by zorin.a on 18.06.2018.
 */

data class SendDeviceKeyRequest(@SerializedName("deviceKey") val deviceKey: String)