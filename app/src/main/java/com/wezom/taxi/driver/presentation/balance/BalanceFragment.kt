package com.wezom.taxi.driver.presentation.balance

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.*
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.common.formatBalanceMoney
import com.wezom.taxi.driver.data.models.DatesOfWeekModel
import com.wezom.taxi.driver.databinding.BalanceBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.presentation.balance.events.ChangeDateEvent
import com.wezom.taxi.driver.presentation.base.BaseFragment
import com.wezom.taxi.driver.presentation.gains.dialog.DateDialog
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.rxkotlin.plusAssign
import java.util.*


/**
 * Created by zorin.a on 15.03.2018.
 */
class BalanceFragment : BaseFragment() {
    lateinit var binding: BalanceBinding
    lateinit var viewModel: BalanceViewModel
    private lateinit var adapter: BalancePagerAdapter
    private val calendar = GregorianCalendar(SimpleTimeZone.getDefault())

    private var week: Int = 0
    private val balanceObserver: Observer<Double> = Observer { balance ->
      binding.run {
          balancePrice.text = formatBalanceMoney(balance!!)
      }
    }

    private val datesOfWeekObserver: Observer<DatesOfWeekModel> = Observer { datesOfWeek ->
        binding.run {
            date.text = datesOfWeek!!.formatetDates
            RxBus.publish(ChangeDateEvent(datesOfWeek.firstDay, datesOfWeek.lastDay))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
        setBackPressFun { viewModel.onBackPressed() }
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
        setOnRefreshCallback {
            viewModel.getUserStatus()
        }
        viewModel.loadingLiveData.observe(this, progressObserver)
        viewModel.balanceLiveData.observe(this, balanceObserver)
        viewModel.datesOfWeekLiveData.observe(this, datesOfWeekObserver)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = BalanceBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getUserStatus()

        week = Calendar.getInstance().get(Calendar.WEEK_OF_YEAR)
        viewModel.getDatesOfWeek(week)

        initViews()
        disposable += RxView.clicks(binding.howToReplenish).subscribe({
            viewModel.switchToHowToReplenish()
        })

        disposable += RxView.clicks(binding.leftArrowImage).subscribe({
            week--
            viewModel.getDatesOfWeek(week)
        })

        disposable += RxView.clicks(binding.rightArrowImage).subscribe({
            if (week < Calendar.getInstance().get(Calendar.WEEK_OF_YEAR)) {
                week++
                viewModel.getDatesOfWeek(week)
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.balance_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_calendar -> showCalendar()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showCalendar() {
        val dialog = DateDialog()
        dialog.listener = object : DateDialog.DatePickerListener {
            override fun onDatePicked(year: Int, month: Int, day: Int) {
                calendar.set(year, month, day)
                week = calendar.get(Calendar.WEEK_OF_YEAR)
                viewModel.getDatesOfWeek(week)
            }
        }
        dialog.show(childFragmentManager, DateDialog.TAG)
    }

    private fun initViews() {
        binding.run {
            toolbar.setToolbarTitle(getString(R.string.balance))
            adapter = BalancePagerAdapter(childFragmentManager)
            adapter.addEntry(BalancePagerAdapter.RECEIVED_ENTRY, ReceivedBalancesFragment(), getString(R.string.received))
            adapter.addEntry(BalancePagerAdapter.DEBITED_ENTRY, DebitedBalancesFragment(), getString(R.string.debited))
            viewPager.adapter = adapter
            tabLayout.setupWithViewPager(binding.viewPager)
            viewPager.offscreenPageLimit = 2
            viewPager.setOnTouchListener(object : View.OnTouchListener {
                override fun onTouch(v: View, event: MotionEvent): Boolean {
                    swipeLayout.isEnabled = false
                    when (event.action) {
                        MotionEvent.ACTION_UP -> swipeLayout.isEnabled = true
                    }
                    return false
                }
            })
        }
    }
}