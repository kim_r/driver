package com.wezom.taxi.driver.presentation.tutorial

import android.content.SharedPreferences
import com.wezom.taxi.driver.common.*
import com.wezom.taxi.driver.ext.IS_TUTOR_COMPLETED
import com.wezom.taxi.driver.ext.TOKEN
import com.wezom.taxi.driver.ext.boolean
import com.wezom.taxi.driver.ext.string
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import javax.inject.Inject

/**
 * Created by zorin.a on 023 23.02.18.
 */

class TutorialViewModel @Inject constructor(screenRouterManager: ScreenRouterManager, sharedPreferences: SharedPreferences) : BaseViewModel(screenRouterManager) {
    private var isTutorCompleted: Boolean by sharedPreferences.boolean(key = IS_TUTOR_COMPLETED, default = false)
    private var token: String by sharedPreferences.string(TOKEN)

    fun skipTutorial() {
        isTutorCompleted = true
        if(token.isEmpty()){
          setRootScreen(VERIFICATION_PHONE_SCREEN)
        } else {
            setRootScreen(MAIN_MAP_SCREEN)
        }
    }
}