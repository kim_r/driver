package com.wezom.taxi.driver.presentation.refuseorder

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.location.Location
import android.widget.Toast
import com.wezom.taxi.driver.BuildConfig
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.bus.events.ResultDataEvent
import com.wezom.taxi.driver.common.*
import com.wezom.taxi.driver.data.db.DbManager
import com.wezom.taxi.driver.data.models.FinishOrder
import com.wezom.taxi.driver.data.models.OrderFinalization
import com.wezom.taxi.driver.ext.FINAL_ORDER
import com.wezom.taxi.driver.ext.LAST_ORDER_STEP_TIME
import com.wezom.taxi.driver.ext.long
import com.wezom.taxi.driver.net.jobqueue.JobFactory
import com.wezom.taxi.driver.net.jobqueue.jobs.FinalizeOrderJob
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.presentation.rating.RatingFragment
import com.wezom.taxi.driver.repository.DriverRepository
import com.wezom.taxi.driver.service.AutoCloseOrderService
import com.wezom.taxi.driver.service.TaxometerService
import java.lang.ref.WeakReference
import javax.inject.Inject

/**
 * Created by zorin.a on 28.03.2018.
 */
class TotalCostViewModel @Inject constructor(screenRouterManager: ScreenRouterManager,
                                             private val locationStorage: LocationStorage,
                                             prefs: SharedPreferences,
                                             private val ctx: Context,
                                             private val dbManager: DbManager,
                                             private val repository: DriverRepository,
                                             private val orderManager: OrderManager) : BaseViewModel(screenRouterManager) {

    private var lastStepTime: Long by prefs.long(key = LAST_ORDER_STEP_TIME,
            default = Constants.STEP_TIME_UNSPECIFIED)

    private var finalOrder: Long by prefs.long(key = FINAL_ORDER,
            default = -1)

    fun isFinishAllowed(delay: Long): Boolean {
        return (System.currentTimeMillis() - lastStepTime) > delay
    }

    fun finalizationOrder(data: ResultDataEvent?) {
        App.instance.getLogger()!!.log("BuildConfig = ${BuildConfig.VERSION_CODE}")
        val finalization = orderManager.queryFinalization()
        if (finalization != null) {
            orderManager.queryFinalization()
                    ?.let {
                        App.instance.getLogger()!!.log("taxometr Finalization = ${data?.orderInfo!!.id} " +
                                "factCost = ${it.totalCost!!.factCost}")
                        val fo =
                                FinishOrder(longitude = data?.currentCoordinate?.longitude
                                        ?: it.totalCost?.longitude!!,
                                        latitude = data?.currentCoordinate?.latitude
                                                ?: it.totalCost?.latitude!!,
                                        eventTime = System.currentTimeMillis())
                        it.finishOrder = fo
                        JobFactory.instance.getJobManager()
                                ?.addJobInBackground(FinalizeOrderJob(orderId = data?.orderInfo!!.id,
                                        request = it))
                    }
//            dbManager.deleteOrderResult(data?.orderInfo!!.id)
        } else {
            App.instance.getLogger()!!.log("forceCloseOrder start ")
            data?.orderInfo?.order?.id?.let {
                App.instance.getLogger()!!.log("taxometr forceCloseOrder = ${data.orderInfo!!.id} " +
                        "factCost = 0")
                repository.forceCloseOrder(it)
                        .subscribe({
                            App.instance.getLogger()!!.log("forceCloseOrder suc ")
                            Toast.makeText(ctx,
                                    R.string.force_close,
                                    Toast.LENGTH_LONG)
                                    .show()

                        },
                                {
                                    App.instance.getLogger()!!.log("forceCloseOrder error ")
                                    Toast.makeText(ctx,
                                            "Error force close",
                                            Toast.LENGTH_LONG)
                                            .show()
                                })
            }
//            dbManager.deleteOrderResult(data?.orderInfo!!.id)
        }

        finalOrder = data!!.orderInfo!!.order!!.id!!.toLong()

        val rateScreenData = Pair<Int, Any>(RatingFragment.MODE_INSTANT_RATE,
                data.orderInfo!!)
        orderManager.finishOrder()
        setRootScreen(RATING_SCREEN,
                rateScreenData)
        lastStepTime = Constants.STEP_TIME_UNSPECIFIED

        WeakReference(context).get()!!.stopService(Intent(WeakReference(context).get(), TaxometerService::class.java))
        WeakReference(context).get()!!.stopService(Intent(WeakReference(context).get(), AutoCloseOrderService::class.java))
    }

    fun notPaid(orderId: Int) {
        switchScreen(NOT_PAID_SCREEN,
                orderId)
    }

    fun cancelOrderByCustomer(orderId: Int,
                              lastLocation: Location?) {
//        Timber.d("cancelOrderByCustomer() id = $orderId")
//        val request = TotalCostRequest(
//                factCost = 0,
//                actualCost = 0,
//                factTime = 0,
//                factPath = 0.0,
//                longitude = lastLocation?.longitude,
//                latitude = lastLocation?.latitude,
//                route = null,
//                eventTime = System.currentTimeMillis(),
//                changedParams = null)
//        JobFactory.instance.getJobManager().addJobInBackground(DataSynchronizationJob(orderId, request))
        if (TaxometerService.isAlive) TaxometerService.stop(WeakReference(context))
        performNextAction(lastLocation)
    }

    private fun performNextAction(lastLocation: Location?) {
        if (orderManager.isSecondaryOrderExist()) {
            setRootScreen(EXECUTE_ORDER_SCREEN,
                    orderManager.getSecondaryOrder())
        } else {
            //just continue on road
            setRootScreen(MAIN_MAP_SCREEN)
        }

        var finalization = orderManager.queryFinalization()
        if (finalization == null) {
            finalization = OrderFinalization()
        }

        finalization.finishOrder =
                FinishOrder(longitude = lastLocation?.longitude
                        ?: locationStorage.getCachedLocation().longitude,
                        latitude = lastLocation?.latitude
                                ?: locationStorage.getCachedLocation().latitude,
                        eventTime = System.currentTimeMillis())
        orderManager.insertFinalization(finalization)

        orderManager.finishOrder()
    }
}