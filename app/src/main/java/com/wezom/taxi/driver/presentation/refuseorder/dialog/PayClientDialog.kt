package com.wezom.taxi.driver.presentation.refuseorder.dialog

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import com.wezom.taxi.driver.R


/**
 * Created by zorin.a on 05.03.2018.
 */
class PayClientDialog : DialogFragment() {
    var listener: DialogClickListener? = null

    @SuppressLint("DefaultLocale")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(context!!, R.style.BlackDialogTheme)
                .setTitle(getString(R.string.message))
                .setMessage(getString(R.string.pay_card_client))
                .setPositiveButton(getString(R.string.undestend).toUpperCase()) { _, _ -> listener?.onPositiveClick() }
                .create()
    }

    companion object {
        val TAG: String = PayClientDialog.javaClass.simpleName
    }

    interface DialogClickListener {
        fun onPositiveClick()
    }
}