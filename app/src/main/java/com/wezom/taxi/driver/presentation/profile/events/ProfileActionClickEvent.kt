package com.wezom.taxi.driver.presentation.profile.events

/**
 * Created by zorin.a on 023 23.02.18.
 */

class ProfileActionClickEvent(val actionType: ActionType) {
    enum class ActionType { SAVE_USER_DATA,  SAVE_IMAGES }
}

