package com.wezom.taxi.driver.presentation.acceptorder

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.widget.Toast
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.common.EXECUTE_ORDER_SCREEN
import com.wezom.taxi.driver.common.LocationStorage
import com.wezom.taxi.driver.common.OrderManager
import com.wezom.taxi.driver.common.USER_INFO_SCREEN
import com.wezom.taxi.driver.data.db.DbManager
import com.wezom.taxi.driver.data.models.Accepted
import com.wezom.taxi.driver.data.models.NewOrderInfo
import com.wezom.taxi.driver.data.models.OrderFinalization
import com.wezom.taxi.driver.data.models.User
import com.wezom.taxi.driver.ext.CALL_CLIENT
import com.wezom.taxi.driver.ext.USER_ID
import com.wezom.taxi.driver.ext.boolean
import com.wezom.taxi.driver.ext.int
import com.wezom.taxi.driver.net.api.ApiManager
import com.wezom.taxi.driver.net.request.AcceptOrderRequest
import com.wezom.taxi.driver.net.response.BaseResponse
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseMapViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.presentation.customview.DriverToolbar
import com.wezom.taxi.driver.presentation.main.events.ShowSecondaryOrderButtonEvent
import com.wezom.taxi.driver.service.TaxometerService
import java.lang.ref.WeakReference
import java.util.*
import javax.inject.Inject

/**
 *Created by Zorin.A on 20.June.2019.
 */
class AcceptOrderViewModel @Inject constructor(screenRouterManager: ScreenRouterManager,
                                               private val apiManager: ApiManager,
                                               private val orderManager: OrderManager,
                                               private val locationStorage: LocationStorage,
                                               private val dbManager: DbManager,
                                               sharedPreferences: SharedPreferences) : BaseMapViewModel(screenRouterManager,
        apiManager,
        sharedPreferences) {
    private var userId by sharedPreferences.int(USER_ID)
    private var callClient by sharedPreferences.boolean(CALL_CLIENT)


    lateinit var orderInfo: NewOrderInfo


    @SuppressLint("CheckResult")
    fun acceptOrder(lon: Double?,
                    lat: Double?) {
        val request = AcceptOrderRequest(longitude = lon,
                latitude = lat,
                eventTime = System.currentTimeMillis())
        App.instance.getLogger()!!.log("acceptOrder start ")
        apiManager.acceptOrder(orderInfo.order!!.id!!,
                request)
                .subscribe({ response ->
                    callClient = true
                    App.instance.getLogger()!!.log("acceptOrder suc ")
                    handleResponseState(response = response)
                    if (response?.isSuccess!!) {
//                        dbManager.clearOrderResult()
                        App.instance.getLogger()!!.log("currentOrder start ")
                        apiManager.currentOrder(orderInfo.order!!.id!!).subscribe({ currentOrder ->
                            App.instance.getLogger()!!.log("currentOrder suc ")
                            handleResponseState(response = BaseResponse(true, null))
                            currentOrder.order!!.distanceFromDriver = orderInfo.order!!.distanceFromDriver!!
                            orderInfo.order = currentOrder.order
                            if (orderInfo.order!!.carArrivalTime == 0L) {
                                orderInfo.order!!.carArrivalTime = orderInfo.order!!.currentTime!!
                            }
//                            if (orderInfo.order!!.preliminaryTime!!.toLong() == 0L )
//                                orderInfo.order!!.preliminaryTime = orderInfo.order!!.carArrivalTime

                            orderManager.acceptOrder(orderInfo)
                            var finalization = orderManager.queryFinalization()
                            if (finalization == null) {
                                finalization = OrderFinalization()
                            }
                            finalization.accepted =
                                    Accepted(longitude = lon
                                            ?: locationStorage.getCachedLocation().longitude,
                                            latitude = lat
                                                    ?: locationStorage.getCachedLocation().latitude,
                                            eventTime = System.currentTimeMillis(),
                                            driverId = userId)
                            orderManager.insertFinalization(finalization)

                            if (orderManager.isSecondaryOrderExist()) {
                                setRootScreen(EXECUTE_ORDER_SCREEN,
                                        orderManager.getPrimaryOrder())
                            } else {
                                TaxometerService.stop(WeakReference(context))
                                setRootScreen(EXECUTE_ORDER_SCREEN,
                                        orderInfo)
                            }
                        }, { App.instance.getLogger()!!.log("currentOrder error ") })
                    } else {
                        Toast.makeText(context,
                                response.error!!.message,
                                Toast.LENGTH_LONG)
                                .show()
                        onBackPressed()
                    }
                },
                        { it -> App.instance.getLogger()!!.log("acceptOrder error ") }) //don nothing on error
//        JobFactory.instance.getJobManager().addJobInBackground(AcceptOrderJob(orderInfo.order!!.id!!, request))
    }

    private fun showSecondaryOrderButton() {
        DriverToolbar.IS_SECONDARY_ORDER_EXIST = true
        RxBus.publish(ShowSecondaryOrderButtonEvent(true))
    }

    fun setOrder(orderInfo: NewOrderInfo) {
        this.orderInfo = orderInfo
    }

    fun showUser(user: User) {
        switchScreen(USER_INFO_SCREEN,
                user)
    }
}