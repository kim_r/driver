package com.wezom.taxi.driver.presentation.refuseorder

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.bus.events.ResultDataEvent
import com.wezom.taxi.driver.common.MAIN_MAP_SCREEN
import com.wezom.taxi.driver.common.formatMoneyRounded
import com.wezom.taxi.driver.data.models.ChangedParams
import com.wezom.taxi.driver.data.models.Coordinate
import com.wezom.taxi.driver.data.models.PaymentMethod
import com.wezom.taxi.driver.databinding.TotalCostBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.ext.setVisible
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseMapFragment
import com.wezom.taxi.driver.presentation.main.events.CancelOrderEvent
import com.wezom.taxi.driver.presentation.refuseorder.dialog.PayClientDialog
import com.wezom.taxi.driver.service.TaxometerService
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import org.jetbrains.anko.support.v4.toast
import timber.log.Timber
import java.lang.ref.WeakReference

/**
 * Created by zorin.a on 28.03.2018.
 */
class TotalCostFragment : BaseMapFragment() {

    lateinit var binding: TotalCostBinding
    lateinit var viewModel: TotalCostViewModel

    private var data: ResultDataEvent? = null

    private var handler: Handler = Handler()

    private var runnable: Runnable = Runnable {
        val temp = if (lastLocation == null) {
            Coordinate(latitude = 0.0, longitude = 0.0, degree = 0)
        } else {
            Coordinate(longitude = lastLocation?.longitude,
                    latitude = lastLocation?.latitude,
                    degree = lastLocation?.bearing!!.toInt())
        }
        data!!.currentCoordinate = temp
//                viewModel.finishOrder(data!!)
        viewModel.finalizationOrder(data)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
        setBackPressFun { viewModel.onBackPressed() }
        arguments?.let {
            data = it.getParcelable(DATA_KEY)
        }
        WeakReference(activity!!).get()!!.stopService(Intent(WeakReference(activity!!).get(), TaxometerService::class.java))

        handler.postDelayed(runnable, 300000)

    }

    override fun onDetach() {
        super.onDetach()
        handler.removeCallbacks(runnable)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = TotalCostBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initListeners()
    }

    private fun disableIfZeroDiscont() {
        binding.discountLabel.setVisible(false)
        binding.discountValue.setVisible(false)
        binding.uahImage2.setVisible(false)
    }

    private fun disableIfZeroCompensation() {
        binding.compensationLabel.setVisible(false)
        binding.infoImageCompensation.setVisible(false)
        binding.compensationValue.setVisible(false)
        binding.uahImage3.setVisible(false)
    }

    @SuppressLint("SetTextI18n", "StringFormatMatches")
    public fun sePlan(second: Long, waitingCost: Int) {
        val minut = (second) / 60
        binding.plain.text = "+ $waitingCost"
        App.instance.getLogger()!!.log("Wait time id order ${data!!.orderInfo!!.order!!.id}  time = $second second")
        if (minut == 0L) {
            binding.plainContainer.setVisible(false)
            return
        } else {
            binding.planText.text = resources.getString(R.string.pay_wait_total_cost, "$minut")
        }

    }

    private fun showPayClientDialog() {
        Handler().postDelayed({
            val dialog = PayClientDialog()
            dialog.listener = object : PayClientDialog.DialogClickListener {
                override fun onPositiveClick() {
                    dialog.dismiss()
                }
            }
            dialog.show(fragmentManager, PayClientDialog.TAG)
        }, 1000)

    }


    @SuppressLint("SetTextI18n")
    private fun initViews() {
        Timber.e(data.toString())
        binding.run {
            getChangedParams(totalCostChangedValue, totalCostChangedLabel)
            data?.waitingTime?.let { sePlan(it, data?.waitingCost!!.toInt()) }
            when (data?.orderInfo?.order?.payment?.method!!) {
                PaymentMethod.CARD, PaymentMethod.CARD_AND_BONUS ->
                    showPayClientDialog()
            }
            paymentType.text = paymentTypeToString(data?.orderInfo?.order?.payment?.method!!)
            val totalMoney = data?.resultCost!!
            val resultF3 = data?.resultF3!!

            // if no value in arguments -- try to get it from service; otherwise -- show estimated cost
            val showEstimated =
                    data?.isEstimatedCostTaken ?: TaxometerService.result?.isEstimatedCostTaken
                    ?: true

            if (showEstimated) {
                priceValue.text = formatMoneyRounded(if (totalMoney > 0) totalMoney else 0)
                rateValue.text = formatMoneyRounded(data?.estimatedCost!!.plus(data?.estimatedBonus!!))
                discountValue.text = "-${formatMoneyRounded(data?.estimatedBonus!!)}"
                compensationValue.text = "+${formatMoneyRounded(data?.surcharge!!.plus(data?.estimatedBonus!!))}"
            } else {
                priceValue.text = formatMoneyRounded(if (totalMoney > 0) totalMoney else 0)
                rateValue.text = formatMoneyRounded(resultF3.plus(data?.estimatedBonus!!))
                val discount = data?.bonus!!
//                val discount = if (data?.bonus!! < (data!!.resultF3 ?: 0)) data?.bonus!!
//                else (resultF3)
                discountValue.text = "-${formatMoneyRounded(discount)}"
                compensationValue.text = "+${formatMoneyRounded(data?.surcharge!!.plus(discount))}"
            }
            disposable += RxView.clicks(infoImage).subscribe {
                if (data!!.paymentMethod == PaymentMethod.CARD_AND_BONUS || data!!.paymentMethod == PaymentMethod.CARD) {
                    showInfoDialog(R.string.bank_card, R.string.bank_card_text)
                } else {
                    showInfoDialog(R.string.cash, R.string.cash_text)
                }
            }

            disposable += RxView.clicks(infoImageCompensation).subscribe {
                showInfoDialog(R.string.compensation, R.string.compensation_text)
            }

            disposable += RxView.clicks(finishButton).subscribe {
                if (!viewModel.isFinishAllowed(FINISH_DELAY)) {
                    toast(R.string.finish_not_allowed)
                    return@subscribe
                }
                handler.removeCallbacks(runnable)
                val temp = if (lastLocation == null) {
                    Coordinate(latitude = 0.0, longitude = 0.0, degree = 0)
                } else {
                    Coordinate(longitude = lastLocation?.longitude,
                            latitude = lastLocation?.latitude,
                            degree = lastLocation?.bearing!!.toInt())
                }
                data!!.currentCoordinate = temp
//                viewModel.finishOrder(data!!)
                viewModel.finalizationOrder(data)
            }

            if (data!!.paymentMethod == PaymentMethod.CARD_AND_BONUS || data!!.paymentMethod == PaymentMethod.CARD) {
                notPaid.setVisible(false)
            } else {
                notPaid.setVisible(true)
                disposable += RxView.clicks(notPaid).subscribe {
                    viewModel.notPaid(data!!.orderInfo!!.order!!.id!!)
                }
            }

            if (data?.estimatedBonus!! == 0) {
                disableIfZeroDiscont()
            }

            if (data?.surcharge!!.plus(data?.estimatedBonus!!) == 0) {
                disableIfZeroCompensation()
            }
            if (data!!.orderInfo!!.order!!.payment!!.method!! == PaymentMethod.CARD) {
                compensationLabel.text = getString(R.string.pay_suc)
                paymentMethodIcon.visibility = View.VISIBLE
                if (data!!.orderInfo!!.order!!.payment!!.cardType == 0) {
                    paymentMethodIcon.setImageResource(R.mipmap.ic_visa)
                } else {
                    paymentMethodIcon.setImageResource(R.mipmap.ic_mastercard)
                }
            }


        }
    }


    private fun initListeners() {
        disposable += RxBus.listen(CancelOrderEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Timber.d("CancelOrderEvent")
                    viewModel.cancelOrderByCustomer(it.orderId, lastLocation)
                    showCustomerCancelOrderDialog()
                }, Timber::e)
    }

    private fun paymentTypeToString(method: PaymentMethod): String {
        return if (method == PaymentMethod.CARD_AND_BONUS || method == PaymentMethod.CARD) {
            getString(R.string.bank_card)
        } else {
            getString(R.string.cash)
        }
    }


    @SuppressLint("SetTextI18n")
    private fun getChangedParams(totalCostChangedValue: TextView, totalCostChangedLabel: TextView) {
        return when (data?.changedParams) {
            ChangedParams.NO_CHANGES -> {
                totalCostChangedLabel.setVisible(false)
                totalCostChangedValue.setVisible(false)
            }
            ChangedParams.TIME -> {
                totalCostChangedLabel.setVisible(true)
                totalCostChangedValue.setVisible(true)
                totalCostChangedValue.text = "${activity!!.getString(R.string.change)} " +
                        "${activity!!.getString(R.string.time)} " +
                        activity!!.getString(R.string.in_trip)
            }
            ChangedParams.DISTANCE -> {
                totalCostChangedLabel.setVisible(true)
                totalCostChangedValue.setVisible(true)
                totalCostChangedValue.text = "${activity!!.getString(R.string.change)} " +
                        "${activity!!.getString(R.string.distance)} " +
                        activity!!.getString(R.string.in_trip)
            }
            ChangedParams.DISTANCE_AND_TIME -> {
                totalCostChangedLabel.setVisible(true)
                totalCostChangedValue.setVisible(true)
                totalCostChangedValue.text = "${activity!!.getString(R.string.change)} " +
                        "${activity!!.getString(R.string.distance)} " +
                        "${activity!!.getString(R.string.and)} " +
                        "${activity!!.getString(R.string.time)} " +
                        activity!!.getString(R.string.in_trip)
            }
            else -> {
                totalCostChangedLabel.setVisible(false)
                totalCostChangedValue.setVisible(false)
            }
        }
    }

    private fun showCustomerCancelOrderDialog() {
        val builder = AlertDialog.Builder(context!!)
                .setTitle(getString(R.string.attention2))
                .setMessage(getString(R.string.order_canceled_by_customer))
                .setPositiveButton(R.string.ok) { p0, _ ->
                    viewModel.setRootScreen(MAIN_MAP_SCREEN)
                    p0.dismiss()
                }
                .setCancelable(false)

        val dialog = builder.create()
        dialog.show()
    }

    companion object {

        const val DATA_KEY = "total_cost_data_key"

        private const val FINISH_DELAY = 5000L

        fun newInstance(result: ResultDataEvent): TotalCostFragment {
            val args = Bundle()
            args.putParcelable(DATA_KEY, result)
            val fragment = TotalCostFragment()
            fragment.arguments = args
            return fragment
        }
    }
}