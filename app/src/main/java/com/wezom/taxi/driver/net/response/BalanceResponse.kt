package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.Income


/**
 * Created by andre on 22.03.2018.
 */
class BalanceResponse(
        @SerializedName("totalMoney") var totalMoney: Double,
        @SerializedName("income") var income: List<Income>) : BaseResponse()