package com.wezom.taxi.driver.presentation.discounts

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.common.BONUS_PROGRAM_SCREEN
import com.wezom.taxi.driver.common.Constants.TAXI_URL
import com.wezom.taxi.driver.databinding.DiscountsBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.net.response.DiscountsResponse
import com.wezom.taxi.driver.presentation.base.BaseFragment
import com.wezom.taxi.driver.presentation.base.lists.RecyclerTouchListener
import com.wezom.taxi.driver.presentation.base.lists.decorator.DividerDecorator
import com.wezom.taxi.driver.presentation.discounts.list.DiscountsAdapter
import io.reactivex.rxkotlin.plusAssign
import org.jetbrains.anko.support.v4.toast
import javax.inject.Inject

/**
 * Created by udovik.s on 15.03.2018.
 */
class DiscountsFragment : BaseFragment() {
    //region var
    private lateinit var binding: DiscountsBinding
    private lateinit var viewModel: DiscountsViewModel
    private val copyShareBottomSheet = CopyShareBottomSheet()

    @Inject
    lateinit var adapter: DiscountsAdapter
    @Inject
    lateinit var touchListener: RecyclerTouchListener

    var decorator: DividerDecorator? = null

    private val discountsObserver: Observer<DiscountsResponse> = Observer { discount ->
        updateUi(discount)
    }
    //endregion

    //region override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setBackPressFun { viewModel.onBackPressed() }
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
        setOnRefreshCallback {
            viewModel.getDiscounts()
        }
        viewModel.discountsLiveData.observe(this, discountsObserver)
        viewModel.loadingLiveData.observe(this, progressObserver)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DiscountsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            toolbar.setToolbarTitle(getString(R.string.bonus_offering))
            disposable += RxView.clicks(imMore).subscribe {
                copyShareBottomSheet.show(fragmentManager, copyShareBottomSheet::class.java.name)
            }
            disposable += RxView.clicks(binding.btnMoreOnSite).subscribe {
                switchToSite(TAXI_URL)
            }

        }

        copyShareBottomSheet.onCopyClick = {
            val cb = context!!.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            cb.primaryClip = ClipData.newPlainText("code", binding.tvPromoCode.text)
            toast(R.string.code_was_copied)
        }

        copyShareBottomSheet.onSocialClick = {
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, binding.tvPromoCode.text)
            startActivity(Intent.createChooser(shareIntent, getString(R.string.share)))
        }
        setupRecyclerView()
        viewModel.getDiscounts()
    }
    //endregion

    //region fun
    private fun updateUi(discount: DiscountsResponse?) {
        binding.run {
            if (!discount!!.status!!) {
                rootPromoCode.visibility = View.INVISIBLE
            }
            tvDetailAboutPromoCode.visibility = VISIBLE
            disposable += RxView.clicks(tvDetailAboutPromoCode).subscribe {
                if (discount!!.url != null) {
                    switchToSite(discount.url.toString())
                }
            }
            tvPromoCode.text = discount?.promoCodeValue

            if (discount?.shares != null && discount.shares.isNotEmpty()) {
                tvYourBonus.visibility = VISIBLE
                tvNoBonus.visibility = GONE
                adapter.setData(discount.shares)
            } else {
                tvNoBonus.visibility = VISIBLE
                tvYourBonus.visibility = GONE
            }
        }
    }

    private fun setupRecyclerView() {
        if (decorator == null)
            decorator = DividerDecorator(activity!!.getDrawable(R.drawable.shape_divider))
        binding.run {
            rvPromo.adapter = adapter
            rvPromo.setHasFixedSize(false)
//            decorator.space = 5
            rvPromo.addItemDecoration(decorator)
            rvPromo.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
            rvPromo.addOnItemTouchListener(touchListener)
            touchListener.rv = rvPromo
            touchListener.listener = object : RecyclerTouchListener.ClickListener {
                override fun onPress(position: Int, view: View) {
                    val item = adapter.getData(position)
                    if (item.id == BONUS_PROGRAM_ID) {//
                        viewModel.switchScreen(BONUS_PROGRAM_SCREEN)
                    } else {
                        val discountUrl = item.url
                        switchToSite(discountUrl.toString())
                    }
                }

                override fun onLongPress(position: Int, view: View) {
                }
            }
        }
    }

    private fun switchToSite(url: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        activity?.startActivity(intent)
    }

    //endregion
    companion object {
        private const val BONUS_PROGRAM_ID = 2
    }
}