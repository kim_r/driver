package com.wezom.taxi.driver.presentation.settings

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.common.BeepUtil
import com.wezom.taxi.driver.databinding.SettingSignalBinding
import com.wezom.taxi.driver.ext.*
import com.wezom.taxi.driver.presentation.base.BaseFragment

class SettingSignalFragment : BaseFragment() {

    lateinit var viewModel: SettingSignalViewModel
    lateinit var binding: SettingSignalBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
        setBackPressFun {
            viewModel.onBackPressed()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = SettingSignalBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            toolbar.setToolbarTitle(getString(R.string.sound_signals))
            swSoundSignal.isChecked = viewModel.songSignal
            swSignalInOnline.isChecked = viewModel.signalOnline
            swSignalInOnlineFilter.isChecked = viewModel.signalOnlineFilter
            swSignalBackground.isChecked = viewModel.signalBackground
            swSignalBackgroundWait.isChecked = viewModel.signalBackgroundWait

            swSoundSignal.setOnCheckedChangeListener { _, b ->
                viewModel.songSignal = b
                if (!b) {
                    viewModel.signalOnline = false
                    viewModel.signalOnlineFilter = false
                    viewModel.signalBackground = false
                    viewModel.signalBackgroundWait = false
                    swSignalInOnline.isChecked = viewModel.signalOnline
                    swSignalInOnlineFilter.isChecked = viewModel.signalOnlineFilter
                    swSignalBackground.isChecked = viewModel.signalBackground
                    swSignalBackgroundWait.isChecked = viewModel.signalBackgroundWait
                }
            }
            swSignalInOnline.setOnCheckedChangeListener { _, b ->
                viewModel.signalOnline = b
                enableRootSwitch(b)
                if(b){
                    BeepUtil.playSongForSetting()
                }
            }
            swSignalBackgroundWait.setOnCheckedChangeListener { _, b ->
                viewModel.signalBackgroundWait = b
                enableRootSwitch(b)
                if(b){
                    BeepUtil.playSongForSetting()
                }
            }
            swSignalInOnlineFilter.setOnCheckedChangeListener { _, b ->
                viewModel.signalOnlineFilter = b
                enableRootSwitch(b)
                if(b){
                    BeepUtil.playSongForSetting()
                }
            }
            swSignalBackground.setOnCheckedChangeListener { _, b ->
                viewModel.signalBackground = b
                enableRootSwitch(b)
            }
        }
    }

    fun enableRootSwitch(b: Boolean) {
        if (b) {
            viewModel.songSignal = b
            binding.swSoundSignal.isChecked = viewModel.songSignal
        }
    }

    companion object {
        fun newInstance(): SettingSignalFragment {
            return SettingSignalFragment()
        }
    }

}