package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName


/**
 * Created by Andrew on 14.03.2018.
 */
class StartAddress(
        @SerializedName("name") var name: String,
        @SerializedName("entrance") var entrance: String,
        @SerializedName("longitude") var longitude: Double,
        @SerializedName("latitude") var latitude: Double
)