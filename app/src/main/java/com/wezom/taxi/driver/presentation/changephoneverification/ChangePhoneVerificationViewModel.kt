package com.wezom.taxi.driver.presentation.changephoneverification

import android.arch.lifecycle.MutableLiveData
import android.content.SharedPreferences
import com.wezom.taxi.driver.common.MAIN_MAP_SCREEN
import com.wezom.taxi.driver.ext.USER_PHONE
import com.wezom.taxi.driver.ext.string
import com.wezom.taxi.driver.data.models.ResponseState
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.repository.DriverRepository
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by udovik.s on 17.04.2018.
 */
class ChangePhoneVerificationViewModel @Inject constructor(screenRouterManager: ScreenRouterManager, sharedPreferences: SharedPreferences, private val repository: DriverRepository)
    : BaseViewModel(screenRouterManager) {

    val smsDelay = MutableLiveData<Int>()

    private var phone: String by sharedPreferences.string(key = USER_PHONE)

    private var delay = 120
    private var smsDelayTimer: Disposable? = null

    fun sendSmsCode(newPhone: String, code: Int) {
        loadingLiveData.postValue(ResponseState(ResponseState.State.LOADING))
        App.instance.getLogger()!!.log("changePhoneVerification start ")
        disposable += repository.changePhoneVerification(newPhone, code).subscribe({ response ->
            handleResponseState(response)
            App.instance.getLogger()!!.log("changePhoneVerification suc ")
            if (response?.isSuccess!!) {
                phone = newPhone
                navigateToMainScreen()
            } else {
                loadingLiveData.postValue(ResponseState(ResponseState.State.IDLE))
            }
        }, {
            App.instance.getLogger()!!.log("changePhoneVerification error ")
            loadingLiveData.postValue(ResponseState(ResponseState.State.NETWORK_ERROR))
        })
    }


    fun navigateToMainScreen() {
        setRootScreen(MAIN_MAP_SCREEN)
    }

    fun launchSmsDelayTimer() {
        smsDelayTimer?.run { if (isDisposed) startTimer() } ?: startTimer()
    }

    private fun startTimer() {
        delay = 60
        smsDelayTimer = Observable
                .interval(1, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(onNext = {
                    --delay
                    smsDelay.value = delay
                    if (delay == 0) killTimer()
                })
    }

    private fun killTimer() = smsDelayTimer?.takeUnless { it.isDisposed }?.dispose()
}