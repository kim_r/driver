package com.wezom.taxi.driver.presentation.imagepicker

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.common.loadRoundedImage
import com.wezom.taxi.driver.databinding.ImagePickerBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.ext.pickPicture
import com.wezom.taxi.driver.ext.setVisible
import com.wezom.taxi.driver.ext.takePicture
import com.wezom.taxi.driver.presentation.base.BaseFragment
import com.wezom.taxi.driver.presentation.base.dialog.ChoosePhotoSourceDialog
import com.wezom.taxi.driver.presentation.customview.ConditionItemView
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber
import java.io.File

/**
 * Created by zorin.a on 19.12.2018.
 */
class ImagePickerFragment : BaseFragment() {
    //region var
    private lateinit var binding: ImagePickerBinding
    private lateinit var viewModel: ImagePickerViwModel
    private var imageId: Int? = null
    private var source: String? = null
    private var photoFile: File? = null

    private var screenObserver = Observer<ImagePickerData> { data: ImagePickerData? ->
        Timber.d("PHOTO_VIEWER_SCREEN screenObserver")
        binding.run {
            imageTitle.text = getString(data!!.title)
            imageDescription.text = getString(data!!.description)

            if (data.source.isNullOrBlank()) {
                saveButton.text = getString(R.string.make_photo_2)
                data.placeholderRes?.let { loadRoundedImage(activity!!, image, uri = null, placeHolderId = it) }
            } else {
                saveButton.text = getString(R.string.change_photo)
            }

            data.requirements?.let { req ->
                req.forEach { resId ->
                    val v = ConditionItemView(activity!!)
                    v.setText(getString(resId))
                    infoContainer.addView(v)
                }
            }
            imageRequirementsTitle.setVisible(data.isShowDescriptionTitle)
        }
    }

    private var imageObserver = Observer<String> { uriString ->
        Timber.d("PHOTO_VIEWER_SCREEN imageObserver")
        if (!uriString.isNullOrBlank()) {
            source = uriString
            setImageSource()
        }
    }
    private val imageCallbacks: ChoosePhotoSourceDialog.Callbacks  by lazy {
        object : ChoosePhotoSourceDialog.Callbacks {
            override fun onGalleryClicked() {
                pickPicture(PICK_PICTURE_REQUEST)
            }

            override fun onWatchClicked() {
                if (!source.isNullOrBlank()) {
                    cropImage(source!!, imageId!!)
                }
            }

            override fun onCameraClicked() {
                photoFile = takePicture(TAKE_PICTURE_REQUEST)
            }

            override fun onSettingsOpen() {
                openAppSettings()
            }
        }
    }

    private fun cropImage(source: String, imageId: Int) {
        viewModel.cropImage(source, imageId)
    }

    //endregion

    //region override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
        initLiveData()
        setBackPressFun { viewModel.onBackPressed() }
        setArguments()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = ImagePickerBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.d("PHOTO_VIEWER_SCREEN onViewCreated")
        initViews()
        initClicks()
        viewModel.prepareScreen(imageId, source)
        viewModel.prepareImage(source)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == TAKE_PICTURE_REQUEST) {
                val uri = Uri.fromFile(photoFile)
                uri?.let {
                    cropImage(it.toString(), imageId!!)
                }
            }
            if (requestCode == PICK_PICTURE_REQUEST) {
                val uri = data?.data
                uri?.let {
                    cropImage(it.toString(), imageId!!)
                }
            }
        }
    }

    //endregion

    //region fun
    private fun initViews() {
        binding.run {
            toolbar.setToolbarTitle(getString(R.string.documents))
        }
    }

    private fun initClicks() {
        disposable += RxView.clicks(binding.saveButton).subscribe {
            val dialog = ChoosePhotoSourceDialog.newInstance(!source.isNullOrBlank())
            dialog.callbacks = imageCallbacks
            dialog.show(childFragmentManager, ChoosePhotoSourceDialog.TAG)
        }


    }

    private fun initLiveData() {
        viewModel.screenPrepareLiveData.observe(this, screenObserver)
        viewModel.imageLiveData.observe(this, imageObserver)
    }

    private fun setImageSource() {
        binding.run {
            if (source != null) {
                if (source!!.startsWith("http")) {
                    loadRoundedImage(activity!!, image, url = source)
                } else {
                    loadRoundedImage(activity!!, image, uri = Uri.parse(source))
                }
            }
        }
    }

    private fun setArguments() {
        arguments?.let {
            val args = it.getSerializable(IMAGE_SRC_KEY) as Pair<Int, String?>
            imageId = args.first
            source = args.second
        }
    }
    //endregion

    companion object {
        private const val IMAGE_SRC_KEY = "image_src_key"

        const val TAKE_PICTURE_REQUEST = 666

        const val PICK_PICTURE_REQUEST = 999

        fun newInstance(data: Pair<Int, String?>): ImagePickerFragment {
            val bundle = Bundle()
            bundle.putSerializable(IMAGE_SRC_KEY, data)

            val fragment = ImagePickerFragment()
            fragment.arguments = bundle
            return fragment
        }
    }
}