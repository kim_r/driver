package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName


/**
 * Created by zorin.a on 021 21.02.18.
 */
class CarIssueYearsResponse(@SerializedName("years") val years: List<Int>) : BaseResponse()