package com.wezom.taxi.driver.presentation.profile

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.jakewharton.rxbinding2.view.RxView
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.common.*
import com.wezom.taxi.driver.data.models.Document
import com.wezom.taxi.driver.data.models.Photos
import com.wezom.taxi.driver.databinding.DocumentsBinding
import com.wezom.taxi.driver.ext.*
import com.wezom.taxi.driver.presentation.base.BaseFragment
import com.wezom.taxi.driver.presentation.base.dialog.ChoosePhotoSourceDialog
import com.wezom.taxi.driver.presentation.base.dialog.ChoosePhotoSourceDialog.Callbacks
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber
import java.io.File

/**
 * Created by zorin.a on 28.02.2018.
 */
class DocumentsFragment : BaseFragment() {

    //region var
    private lateinit var binding: DocumentsBinding
    private lateinit var viewModel: DocumentsViewModel


    private var userImageUri: Uri? = null
    private var carImageUri: Uri? = null
    private var doc1Uri: Uri? = null
    private var doc2Uri: Uri? = null
    private var doc3Uri: Uri? = null
    private var doc4Uri: Uri? = null
    private var photoFile: File? = null

    private val userCallback: Callbacks  by lazy {
        object : Callbacks {
            override fun onGalleryClicked() {
                pickPicture(PICK_PICTURE_USER_IMG_REQUEST)
            }

            override fun onWatchClicked() {
                if (userImageUri == null) {
                    if (binding.userImage.drawable is BitmapDrawable) {
                        getBitmapFromImage(binding.userImage)?.let {
                            val uri = storeBitmapToDisk(context!!, it, Constants.TEMP_BITMAP)
                            uri.let {
                                cropImage(it.toString(), USER_IMG_ID)
                            }
                        }
                    }
                } else {
                    cropImage(userImageUri.toString(), USER_IMG_ID)
                }
            }

            override fun onCameraClicked() {
                photoFile = takePicture(TAKE_PICTURE_USER_IMG_REQUEST)
            }

            override fun onSettingsOpen() {
                openAppSettings()
            }
        }
    }
    private val carCallback: Callbacks  by lazy {
        object : Callbacks {
            override fun onGalleryClicked() {
                pickPicture(PICK_PICTURE_CAR_IMG_REQUEST)
            }

            override fun onWatchClicked() {
                if (userImageUri == null) {
                    if (binding.userImage.drawable is BitmapDrawable) {
                        getBitmapFromImage(binding.userImage)?.let {
                            val uri = storeBitmapToDisk(context!!, it, Constants.TEMP_BITMAP)
                            uri.let {
                                cropImage(it.toString(), CAR_IMG_ID)
                            }
                        }
                    }
                } else {
                    cropImage(userImageUri.toString(), CAR_IMG_ID)
                }
            }

            override fun onCameraClicked() {
                photoFile = takePicture(TAKE_PICTURE_CAR_IMG_REQUEST)
            }

            override fun onSettingsOpen() {
                openAppSettings()
            }
        }
    }
    private val doc1Callback: Callbacks by lazy {
        object : Callbacks {
            override fun onGalleryClicked() {
                pickPicture(PICK_PICTURE_FIRST_DOC_REQUEST)
            }

            override fun onWatchClicked() {
                if (doc1Uri == null) {
                    if (binding.driverLicenseImage1.drawable is BitmapDrawable) {
                        getBitmapFromImage(binding.driverLicenseImage1)?.let {
                            val uri = storeBitmapToDisk(context!!, it, Constants.TEMP_BITMAP)
                            uri.let {
                                cropImage(it.toString(), DOC_1_IMG_ID)
                            }
                        }
                    }
                } else {
                    cropImage(doc1Uri.toString(), DOC_1_IMG_ID)
                }
            }

            override fun onCameraClicked() {
                photoFile = takePicture(TAKE_PICTURE_FIRST_DOC_REQUEST)
            }

            override fun onSettingsOpen() {
                openAppSettings()
            }
        }
    }
    private val doc2Callback: Callbacks by lazy {
        object : Callbacks {
            override fun onGalleryClicked() {
                pickPicture(PICK_PICTURE_SECOND_DOC_REQUEST)
            }

            override fun onWatchClicked() {
                if (doc2Uri == null) {
                    if (binding.driverLicenseImage2.drawable is BitmapDrawable) {
                        getBitmapFromImage(binding.driverLicenseImage2)?.let {
                            val uri = storeBitmapToDisk(context!!, it, Constants.TEMP_BITMAP)
                            uri.let {
                                cropImage(it.toString(), DOC_2_IMG_ID)
                            }
                        }
                    }
                } else {
                    cropImage(doc2Uri.toString(), DOC_2_IMG_ID)
                }
            }

            override fun onCameraClicked() {
                photoFile = takePicture(TAKE_PICTURE_SECOND_DOC_REQUEST)
            }

            override fun onSettingsOpen() {
                openAppSettings()
            }
        }
    }
    private val doc3Callback: Callbacks  by lazy {
        object : Callbacks {
            override fun onGalleryClicked() {
                pickPicture(PICK_PICTURE_THIRD_DOC_REQUEST)
            }

            override fun onWatchClicked() {
                if (doc3Uri == null) {
                    if (binding.techPassportImage1.drawable is BitmapDrawable) {
                        getBitmapFromImage(binding.techPassportImage1)?.let {
                            val uri = storeBitmapToDisk(context!!, it, Constants.TEMP_BITMAP)
                            uri.let {
                                cropImage(it.toString(), DOC_3_IMG_ID)
                            }
                        }
                    }
                } else {
                    cropImage(doc3Uri.toString(), DOC_3_IMG_ID)
                }
            }

            override fun onCameraClicked() {
                photoFile = takePicture(TAKE_PICTURE_THIRD_DOC_REQUEST)
            }

            override fun onSettingsOpen() {
                openAppSettings()
            }
        }
    }
    private val doc4Callback: Callbacks   by lazy {
        object : Callbacks {
            override fun onGalleryClicked() {
                pickPicture(PICK_PICTURE_FOURTH_DOC_REQUEST)
            }

            override fun onWatchClicked() {
                if (doc4Uri == null) {
                    if (binding.techPassportImage2.drawable is BitmapDrawable) {
                        getBitmapFromImage(binding.techPassportImage2)?.let {
                            val uri = storeBitmapToDisk(context!!, it, Constants.TEMP_BITMAP)
                            uri.let {
                                cropImage(it.toString(), DOC_4_IMG_ID)
                            }
                        }
                    }
                } else {
                    cropImage(doc4Uri.toString(), DOC_4_IMG_ID)
                }
            }

            override fun onCameraClicked() {
                photoFile = takePicture(TAKE_PICTURE_FOURTH_DOC_REQUEST)
            }

            override fun onSettingsOpen() {
                openAppSettings()
            }
        }
    }

    private var userImageObserver = Observer<String> { uriString ->
        Timber.d(" userImageObserver")
        if (!uriString.isNullOrBlank()) {
            userImageUri = Uri.parse(uriString)
            loadImage(context!!, binding.userImage, uri = userImageUri, placeHolderId = R.drawable.ic_user_photo)
        }
        photoFile = null
    }
    private var carImageObserver = Observer<String> { uriString ->
        Timber.d(" carImageObserver")
        if (!uriString.isNullOrBlank()) {
            carImageUri = Uri.parse(uriString)
            loadImage(context!!, binding.carImage, uri = carImageUri, placeHolderId = R.drawable.ic_car_photo)
        }
        photoFile = null
    }
    private var doc1ImageObserver = Observer<String> { uriString ->
        Timber.d(" doc1ImageObserver")
        if (!uriString.isNullOrBlank()) {
            doc1Uri = Uri.parse(uriString)
            loadImage(context!!, binding.driverLicenseImage1, uri = doc1Uri, placeHolderId = R.drawable.ic_doc_1)
        }
        photoFile = null
    }
    private var doc2ImageObserver = Observer<String> { uriString ->
        Timber.d("doc2ImageObserver")
        if (!uriString.isNullOrBlank()) {
            doc2Uri = Uri.parse(uriString)
            loadImage(context!!, binding.driverLicenseImage2, uri = doc2Uri, placeHolderId = R.drawable.ic_doc_2)
        }
        photoFile = null
    }
    private var doc3ImageObserver = Observer<String> { uriString ->
        Timber.d("doc3ImageObserver")
        if (!uriString.isNullOrBlank()) {
            doc3Uri = Uri.parse(uriString)
            loadImage(context!!, binding.techPassportImage1, uri = doc3Uri, placeHolderId = R.drawable.ic_doc_3)
        }
        photoFile = null
    }
    private var doc4ImageObserver = Observer<String> { uriString ->
        Timber.d("doc4ImageObserver")
        if (!uriString.isNullOrBlank()) {
            doc4Uri = Uri.parse(uriString)
            loadImage(context!!, binding.techPassportImage2, uri = doc4Uri, placeHolderId = R.drawable.ic_doc_4)
        }
        photoFile = null
    }

    private var mode: Int? = null
    private var photos: Photos? = null
    //endregion

    //region override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
        arguments?.let {
            mode = it.getInt(ProfileFragment.MODE_KEY)
            photos = it.getParcelable(PHOTOS_KEY)
            photos?.documents = photos?.documents?.sortedWith(compareBy(Document::id))
        }

        viewModel.userImageLiveData.observe(this, userImageObserver)
        viewModel.carImageLiveData.observe(this, carImageObserver)
        viewModel.doc1LiveData.observe(this, doc1ImageObserver)
        viewModel.doc2LiveData.observe(this, doc2ImageObserver)
        viewModel.doc3LiveData.observe(this, doc3ImageObserver)
        viewModel.doc4LiveData.observe(this, doc4ImageObserver)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DocumentsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.updateLocalImages()
        when (mode) {
            ProfileFragment.MODE_PROFILE -> {
                setAsProfile()
            }
            ProfileFragment.MODE_MODERATION -> setAsModeration()
            ProfileFragment.MODE_NEED_COMPLETE -> setAsModeration()
        }
        setImages()
        sharedClicks()
        setDocumentState()
        Timber.d("onViewCreated")
    }


    override fun onDestroy() {
        super.onDestroy()
        Timber.d("onDestroy() clearDocCache()")
        viewModel.clearDocCache()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                PICK_PICTURE_USER_IMG_REQUEST -> {
                    val uri = data?.data
                    uri?.let {
                        cropImage(it.toString(), USER_IMG_ID)
                    }
                }
                TAKE_PICTURE_USER_IMG_REQUEST -> {
                    val uri = Uri.fromFile(photoFile)
                    uri?.let {
                        cropImage(it.toString(), USER_IMG_ID)
                    }
                }

                PICK_PICTURE_CAR_IMG_REQUEST -> {
                    val uri = data?.data
                    uri?.let {
                        cropImage(it.toString(), CAR_IMG_ID)
                    }
                }
                TAKE_PICTURE_CAR_IMG_REQUEST -> {
                    val uri = Uri.fromFile(photoFile)
                    uri?.let {
                        cropImage(it.toString(), CAR_IMG_ID)
                    }
                }

                PICK_PICTURE_FIRST_DOC_REQUEST -> {
                    val uri = data?.data
                    uri?.let {
                        cropImage(it.toString(), DOC_1_IMG_ID)
                    }
                }
                TAKE_PICTURE_FIRST_DOC_REQUEST -> {
                    val uri = Uri.fromFile(photoFile)
                    uri?.let {
                        cropImage(it.toString(), DOC_1_IMG_ID)
                    }
                }

                PICK_PICTURE_SECOND_DOC_REQUEST -> {
                    val uri = data?.data
                    uri.let {
                        cropImage(it.toString(), DOC_2_IMG_ID)
                    }
                }
                TAKE_PICTURE_SECOND_DOC_REQUEST -> {
                    val uri = Uri.fromFile(photoFile)
                    uri.let {
                        cropImage(it.toString(), DOC_2_IMG_ID)
                    }
                }

                PICK_PICTURE_THIRD_DOC_REQUEST -> {
                    val uri = data?.data
                    uri.let {
                        cropImage(it.toString(), DOC_3_IMG_ID)
                    }
                }
                TAKE_PICTURE_THIRD_DOC_REQUEST -> {
                    val uri = Uri.fromFile(photoFile)
                    uri.let {
                        cropImage(it.toString(), DOC_3_IMG_ID)
                    }
                }

                PICK_PICTURE_FOURTH_DOC_REQUEST -> {
                    val uri = data?.data
                    uri.let {
                        cropImage(it.toString(), DOC_4_IMG_ID)
                    }
                }
                TAKE_PICTURE_FOURTH_DOC_REQUEST -> {
                    val uri = Uri.fromFile(photoFile)
                    uri.let {
                        cropImage(it.toString(), DOC_4_IMG_ID)
                    }
                }
            }
        }
    }

    //endregion

    //region fun


    private fun setAsModeration() {
        binding.run {
            disposable += RxView.clicks(saveButton).subscribe {
                viewModel.saveProfileDocs()
            }
        }
    }


    private fun setAsProfile() {

    }


    private fun setDocumentState() {
        binding.run {
            setStatus(infoCarImageStatus, carImageStatus, photos?.userPhoto?.photoStatus)
            setStatus(infoDriverImageStatus, driverImageStatus, photos?.carPhoto?.photoStatus)
            setStatus(infoDriverLicense1Status, driverLicence1Status, selectDocById(photos?.documents, 1)?.photoStatus)
            setStatus(infoDriverLicense2Status, driverLicence2Status, selectDocById(photos?.documents, 2)?.photoStatus)
            setStatus(infoTechPassport1Status, techPassport1Status, selectDocById(photos?.documents, 3)?.photoStatus)
            setStatus(infoTechPassport2Status, techPassport2Status, selectDocById(photos?.documents, 4)?.photoStatus)
        }
    }

    private fun setStatus(iconView: ImageView, descriptionView: TextView, status: Int?) {
        when (status) {
            null -> {
                iconView.setVisible(false)
                descriptionView.setVisible(false)
            }
            STATUS_NOT_NECESSARY -> {
                iconView.setVisible(false)
                descriptionView.setVisible(false)
            }
            STATUS_AVAITING_DATA -> {
                iconView.setImageResource(R.drawable.ic_waiting)
                descriptionView.setTextColor(resources.getColor(R.color.colorAccent2))
                descriptionView.setText(R.string.img_status_awaiting)
            }
            STATUS_AVAITING_MODERATION -> {
                iconView.setImageResource(R.drawable.ic_waiting_2)
                descriptionView.setTextColor(resources.getColor(R.color.strokeColor2))
                descriptionView.setText(R.string.img_status_in_progress)
            }
            STATUS_REJECTED -> {
                iconView.setImageResource(R.drawable.ic_rejected)
                descriptionView.setTextColor(resources.getColor(R.color.colorRed))
                descriptionView.setText(R.string.img_status_rejected)
            }
            STATUS_APPROVED -> {
                iconView.setImageResource(R.drawable.ic_verified)
                descriptionView.setTextColor(resources.getColor(R.color.colorLiteGreen))
                descriptionView.setText(R.string.img_status_approved)
            }
        }
    }

    private fun checkCorrectness(isCorrect: Boolean?, redFrame: View, faultIcon: View) {
        isCorrect?.let {
            faultIcon.setVisible(!isCorrect)
            redFrame.setVisible(!isCorrect)
        }
    }

    private fun sharedClicks() {
        binding.run {
                        disposable += RxView.clicks(userImage).subscribe {
                val dialog = ChoosePhotoSourceDialog.newInstance(userImageUri != null || photos?.userPhoto?.url != null)
                dialog.callbacks = userCallback
                dialog.show(childFragmentManager, ChoosePhotoSourceDialog.TAG)
            }

            disposable += RxView.clicks(carImage).subscribe {
                val dialog = ChoosePhotoSourceDialog.newInstance(carImageUri != null || photos?.carPhoto?.url != null)
                dialog.callbacks = carCallback
                dialog.show(childFragmentManager, ChoosePhotoSourceDialog.TAG)
            }

            disposable += RxView.clicks(driverLicenseImage1).subscribe {
                val dialog = ChoosePhotoSourceDialog.newInstance(doc1Uri != null || photos?.documents?.getOrNull(0) != null)
                dialog.callbacks = doc1Callback
                dialog.show(childFragmentManager, ChoosePhotoSourceDialog.TAG)
            }

            disposable += RxView.clicks(driverLicenseImage2).subscribe {
                val dialog = ChoosePhotoSourceDialog.newInstance(doc2Uri != null || photos?.documents?.getOrNull(1) != null)
                dialog.callbacks = doc2Callback
                dialog.show(childFragmentManager, ChoosePhotoSourceDialog.TAG)
            }

            disposable += RxView.clicks(techPassportImage1).subscribe {
                val dialog = ChoosePhotoSourceDialog.newInstance(doc3Uri != null || photos?.documents?.getOrNull(2) != null)
                dialog.callbacks = doc3Callback
                dialog.show(childFragmentManager, ChoosePhotoSourceDialog.TAG)
            }

            disposable += RxView.clicks(techPassportImage2).subscribe {
                val dialog = ChoosePhotoSourceDialog.newInstance(doc4Uri != null || photos?.documents?.getOrNull(3) != null)
                dialog.callbacks = doc4Callback
                dialog.show(childFragmentManager, ChoosePhotoSourceDialog.TAG)
            }


            disposable += RxView.clicks(driverImagePlaceholder).subscribe {
                viewModel.imageItemClick(USER_IMG_ID, pickSource(photos?.userPhoto?.url, userImageUri))
            }
            disposable += RxView.clicks(carImagePlaceholder).subscribe {
                viewModel.imageItemClick(CAR_IMG_ID, pickSource(photos?.carPhoto?.url, carImageUri))
            }
            disposable += RxView.clicks(driverLicenseSide1).subscribe {
                viewModel.imageItemClick(DOC_1_IMG_ID, pickSource(selectDocById(photos?.documents, 1)?.url, doc1Uri))
            }
            disposable += RxView.clicks(driverLicenseSide2).subscribe {
                viewModel.imageItemClick(DOC_2_IMG_ID, pickSource(selectDocById(photos?.documents, 2)?.url, doc2Uri))
            }
            disposable += RxView.clicks(techPassportSide1).subscribe {
                viewModel.imageItemClick(DOC_3_IMG_ID, pickSource(selectDocById(photos?.documents, 3)?.url, doc3Uri))
            }
            disposable += RxView.clicks(techPassportSide2).subscribe {
                viewModel.imageItemClick(DOC_4_IMG_ID, pickSource(selectDocById(photos?.documents, 4)?.url, doc4Uri))
            }

            disposable += RxView.clicks(saveButton).subscribe {
                viewModel.saveProfileDocs()
                Timber.d("PPP click save")
            }
        }
    }

    private fun pickSource(url: String?, uri: Uri?): String? {
        return uri?.toString() ?: url
    }


    private fun setImages() {
        binding.run {
            if (userImageUri == null) {
                loadImage(context!!, userImage, url = photos?.userPhoto?.url, placeHolderId = R.drawable.ic_user_photo)
            } else {
                loadImage(context!!, userImage, uri = userImageUri, placeHolderId = R.drawable.ic_user_photo)
            }
            if (carImageUri == null) {
                loadImage(context!!, carImage, url = photos?.carPhoto?.url, placeHolderId = R.drawable.ic_car_photo)
            } else {
                loadImage(context!!, carImage, uri = carImageUri, placeHolderId = R.drawable.ic_car_photo)
            }
            if (doc1Uri == null) {
                loadImage(context!!, driverLicenseImage1, url = selectDocById(photos?.documents, 1)?.url, placeHolderId = R.drawable.ic_doc_1)
            } else {
                loadImage(context!!, driverLicenseImage1, uri = doc1Uri, placeHolderId = R.drawable.ic_doc_1)
            }
            if (doc2Uri == null) {
                loadImage(context!!, driverLicenseImage2, url = selectDocById(photos?.documents, 2)?.url, placeHolderId = R.drawable.ic_doc_2)
            } else {
                loadImage(context!!, driverLicenseImage2, uri = doc2Uri, placeHolderId = R.drawable.ic_doc_2)
            }
            if (doc3Uri == null) {
                loadImage(context!!, techPassportImage1, url = selectDocById(photos?.documents, 3)?.url, placeHolderId = R.drawable.ic_doc_3)
            } else {
                loadImage(context!!, techPassportImage1, uri = doc3Uri, placeHolderId = R.drawable.ic_doc_3)
            }
            if (doc4Uri == null) {
                loadImage(context!!, techPassportImage2, url = selectDocById(photos?.documents, 4)?.url, placeHolderId = R.drawable.ic_doc_4)
            } else {
                loadImage(context!!, techPassportImage2, uri = doc4Uri, placeHolderId = R.drawable.ic_doc_4)
            }
        }
    }

    private fun selectDocById(docs: List<Document>?, id: Int): Document? {
        return docs?.find { it.id == id }
    }

    private fun cropImage(uri: String?, requestCode: Int) {
        uri?.let {
            if (it.isNotEmpty()) {
                viewModel.cropImage(it, requestCode)
            }
        }
    }

    //endregion

    companion object {
        const val PHOTOS_KEY = "photos_key"

        const val TAKE_PICTURE_FIRST_DOC_REQUEST = 1
        const val TAKE_PICTURE_SECOND_DOC_REQUEST = 2
        const val TAKE_PICTURE_THIRD_DOC_REQUEST = 3
        const val TAKE_PICTURE_FOURTH_DOC_REQUEST = 4
        const val TAKE_PICTURE_USER_IMG_REQUEST = 5
        const val TAKE_PICTURE_CAR_IMG_REQUEST = 6

        const val PICK_PICTURE_FIRST_DOC_REQUEST = 7
        const val PICK_PICTURE_SECOND_DOC_REQUEST = 8
        const val PICK_PICTURE_THIRD_DOC_REQUEST = 9
        const val PICK_PICTURE_FOURTH_DOC_REQUEST = 10
        const val PICK_PICTURE_USER_IMG_REQUEST = 11
        const val PICK_PICTURE_CAR_IMG_REQUEST = 12

        const val DOC_1_IMG_ID = 0
        const val DOC_2_IMG_ID = 1
        const val DOC_3_IMG_ID = 2
        const val DOC_4_IMG_ID = 3
        const val USER_IMG_ID = 4
        const val CAR_IMG_ID = 5

        var isFirstDocImageOk: Boolean = true
        var isSecondDocImageOk: Boolean = true
        var isThirdDocImageOk: Boolean = true
        var isFourthDocImageOk: Boolean = true

        const val STATUS_NOT_NECESSARY = 0
        const val STATUS_AVAITING_DATA = 1
        const val STATUS_AVAITING_MODERATION = 2
        const val STATUS_REJECTED = 3
        const val STATUS_APPROVED = 4

        fun checkImagesOk(): Boolean {
            Timber.d("VALIDATION isFirstDocImageOk $isFirstDocImageOk isSecondDocImageOk $isSecondDocImageOk isThirdDocImageOk $isThirdDocImageOk isFourthDocImageOk $isFourthDocImageOk")
            return isFirstDocImageOk && isSecondDocImageOk && isThirdDocImageOk && isFourthDocImageOk
        }

        fun newInstance(mode: Int, photos: Photos? = null): DocumentsFragment {
            val args = Bundle()
            args.putInt(ProfileFragment.MODE_KEY, mode)
            if (photos != null)
                args.putParcelable(PHOTOS_KEY, photos)
            val fragment = DocumentsFragment()
            fragment.arguments = args
            return fragment
        }
    }
}