package com.wezom.taxi.driver.data.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class TotalCost(
        @SerializedName("factCost") var factCost: Int?,
        @SerializedName("actualCost") var actualCost: Int?,
        @SerializedName("fact_time") var factTime: Long?,
        @SerializedName("fact_path") var factPath: Double?,
        @SerializedName("waitingTime") var waitingTime: Long?,
        @SerializedName("waitingCost") var waitingCost: Double?,
        @SerializedName("longitude") var longitude: Double?,
        @SerializedName("latitude") var latitude: Double?,
        @SerializedName("route") var route: List<Coordinate>?,
        @SerializedName("eventTime") var eventTime: Long?,
        @SerializedName("changedParams") var changedParams: Int?,
        @SerializedName("resultF3") var resultF3: Int?
) : Parcelable, Serializable {

    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Long::class.java.classLoader) as? Long,
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readValue(Long::class.java.classLoader) as? Long,
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.createTypedArrayList(Coordinate),
            parcel.readValue(Long::class.java.classLoader) as? Long,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int) {
    }

    constructor() : this(factCost = 0,
            actualCost = 0,
            factTime = 0,
            factPath = 0.0,
            waitingTime = 0,
            waitingCost = 0.0,
            longitude = 0.0,
            latitude = 0.0,
            route = listOf(Coordinate()),
            eventTime = 0,
            changedParams = 0,
            resultF3 = 0)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(factCost)
        parcel.writeValue(actualCost)
        parcel.writeValue(factTime)
        parcel.writeValue(factPath)
        parcel.writeValue(waitingTime)
        parcel.writeValue(waitingCost)
        parcel.writeValue(longitude)
        parcel.writeValue(latitude)
        parcel.writeTypedList(route)
        parcel.writeValue(eventTime)
        parcel.writeValue(changedParams)
        parcel.writeValue(resultF3)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TotalCost> {
        override fun createFromParcel(parcel: Parcel): TotalCost {
            return TotalCost(parcel)
        }

        override fun newArray(size: Int): Array<TotalCost?> {
            return arrayOfNulls(size)
        }
    }

}