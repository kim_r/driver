package com.wezom.taxi.driver.presentation.userinfo

import com.wezom.taxi.driver.data.models.User
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import javax.inject.Inject

/**
 *Created by Zorin.A on 21.June.2019.
 */
class UserInfoViewModel @Inject constructor(screenRouterManager: ScreenRouterManager) : BaseViewModel(screenRouterManager) {
    lateinit var user: User
}