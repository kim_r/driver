package com.wezom.taxi.driver.presentation.verification

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import android.content.SharedPreferences
import android.util.Log
import android.widget.Toast
import com.google.firebase.iid.FirebaseInstanceId
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.bus.events.ProfileModeEvent
import com.wezom.taxi.driver.common.*
import com.wezom.taxi.driver.data.models.*
import com.wezom.taxi.driver.data.models.ResponseState.State.*
import com.wezom.taxi.driver.ext.*
import com.wezom.taxi.driver.net.api.ApiManager
import com.wezom.taxi.driver.net.jobqueue.JobFactory
import com.wezom.taxi.driver.net.jobqueue.jobs.DataSynchronizationJob
import com.wezom.taxi.driver.net.jobqueue.jobs.LogoutJob
import com.wezom.taxi.driver.net.request.ConfirmCodeRequest
import com.wezom.taxi.driver.net.request.SendDeviceKeyRequest
import com.wezom.taxi.driver.net.request.SendPhoneRequest
import com.wezom.taxi.driver.net.request.TotalCostRequest
import com.wezom.taxi.driver.net.response.UserStatusResponse
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.presentation.customview.DriverToolbar
import com.wezom.taxi.driver.presentation.main.events.SocketServiceEvent
import com.wezom.taxi.driver.presentation.profile.ProfileFragment
import com.wezom.taxi.driver.presentation.profile.events.UpdateUserDataEvent
import com.wezom.taxi.driver.presentation.profile.events.UpdateUserImageEvent
import com.wezom.taxi.driver.service.SocketService
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.lang.ref.WeakReference
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class AuthViewModel @Inject constructor(screenRouterManager: ScreenRouterManager,
                                        private val apiManager: ApiManager,
                                        private val orderManager: OrderManager,
                                        sharedPreferences: SharedPreferences) : BaseViewModel(screenRouterManager) {
    val smsDelayLiveData = MutableLiveData<Int>()
    val switchToSmsLiveData = MutableLiveData<Boolean>()

    private var userImageString: String by sharedPreferences.string(USER_IMAGE_URI)
    private var carImageString: String by sharedPreferences.string(CAR_IMAGE_URI)
    private var userName: String by sharedPreferences.string(USER_NAME_CACHED)
    private var userRating: String by sharedPreferences.string(USER_RATING_CACHED)
    private var userPhone: String by sharedPreferences.string(key = USER_PHONE)
    private var userBalance: String by sharedPreferences.string(USER_BALANCE)
    private var userBalanceId by sharedPreferences.int(USER_BALANCE_ID)

    private var token: String by sharedPreferences.string(TOKEN)
    private var needComplete: Boolean by sharedPreferences.boolean(IS_NEED_COMPLETE)
    private var language: String by sharedPreferences.string(LANGUAGE, default = "ru")
    private var coordinatesUpdateTime: Long by sharedPreferences.long(key = COORDINATES_UPDATE_TIME, default = 10000)

    private var doc1 by sharedPreferences.string(FIRST_DOC_URI)
    private var doc2 by sharedPreferences.string(SECOND_DOC_URI)
    private var doc3 by sharedPreferences.string(THIRD_DOC_URI)
    private var doc4 by sharedPreferences.string(FOURTH_DOC_URI)
    private var userId by sharedPreferences.int(USER_ID)
    private var isCardAdded: Boolean by sharedPreferences.boolean(IS_CARD_ADDED)

    private var finalOrder: Long by sharedPreferences.long(FINAL_ORDER)

    private var delay = 120

    private var smsDelayTimer: Disposable? = null

    fun sendSmsCode(phoneNumber: String, code: Int) {
        loadingLiveData.postValue(ResponseState(LOADING))
        userPhone = phoneNumber

        App.instance.getLogger()!!.log("confirmCode start ")
        disposable += apiManager.confirmCode(ConfirmCodeRequest(phoneNumber, code))
                .subscribe({ response ->
                    App.instance.getLogger()!!.log("confirmCode success ")
                    handleResponseState(response)
                    clearData()
                    if (response?.isSuccess!!) {
                        token = response.token
                        userId = response.user?.id!!
                        App.instance.getLogger()!!.log("confirmCode success $userId")
                        App.instance.getLogger()!!.log("checkUserStatus start ")
                        disposable += apiManager.checkUserStatus().subscribe({ response ->
                            handleResponseState(response)
                            App.instance.getLogger()!!.log("checkUserStatus suc ")
                            if (response?.isSuccess!!) {
                                val user = response.user
                                if (user != null) {
                                    user.photo?.let {
                                        userImageString = it
                                    }
                                    user.name?.let {
                                        userName = it
                                    }

                                    user.balanceId?.let {
                                        userBalanceId = it
                                    }

                                    userRating = user.rating?.toString() ?: "0.0"
                                    RxBus.publish(UpdateUserDataEvent())
                                    RxBus.publish(UpdateUserImageEvent())
                                }
                                setOnlineStatus(response)

                                SocketService.start(WeakReference(context))

                                userBalance = response.balance.toString()
                                coordinatesUpdateTime = response.coordinatesUpdateTime!!

                                if (response.isCurrentOrderExist!!) {
                                    App.instance.getLogger()!!.log("currentOrders start ")
                                    disposable += apiManager.currentOrders()
                                            .subscribe({ currentOrdersResponse ->

                                                App.instance.getLogger()!!.log("currentOrders suc ")
                                                val orders = currentOrdersResponse.orders

                                                val primaryOrder = orders[0]

                                                Log.d("RestoreOrder", "$finalOrder == ${primaryOrder.order!!.id!!.toLong()}")
                                                Timber.d( "$finalOrder == ${primaryOrder.order!!.id!!.toLong()}")
                                                if (finalOrder == primaryOrder.order!!.id!!.toLong()) {
                                                    orderManager.wipeDatabase()
                                                    enterApplication(response)
                                                    finalOrder = -1
                                                }else {
                                                    val actualCost = currentOrdersResponse?.restoredData?.actualCost
                                                    if (actualCost != null)
                                                        primaryOrder.order?.actualCost = actualCost
                                                    if (primaryOrder.order?.status == OrderStatus.NOT_COMPLETED) {
                                                        //send request to cancel order
                                                        cancelOrder(primaryOrder)
                                                    } else {
                                                        orderManager.addPrimaryOrderToDb(primaryOrder)
                                                    }

                                                    if (orders.size > 1) {
                                                        val secondaryOrder = orders[1]
                                                        if (secondaryOrder.order?.status == OrderStatus.NOT_COMPLETED) {
                                                            //send request to cancel order
                                                            cancelOrder(secondaryOrder)
                                                        } else {
                                                            orderManager.addSecondaryOrderToDb(secondaryOrder)
                                                            DriverToolbar.IS_SECONDARY_ORDER_EXIST = true
                                                        }
                                                    }
                                                    setDrawerState(response)
                                                    if (orderManager.isPrimaryOrderExist()) {
                                                        restoreOrder(primaryOrder, currentOrdersResponse.restoredData, orderManager.getOrderResult(primaryOrder.id))
                                                    } else {
                                                        enterApplication(response)
                                                    }
                                                }

                                            }, {
                                                App.instance.getLogger()!!.log("currentOrders error ")
                                                Timber.d("Can`t get current orders")
                                            })
                                } else {
                                    orderManager.wipeDatabase()
                                    enterApplication(response)
                                }
                            }
                            val refreshedToken = FirebaseInstanceId.getInstance().token
                            refreshedToken?.let {
                                sendDeviceKey(it)
                            }
                        }, {
                            App.instance.getLogger()!!.log("checkUserStatus error ")
                            loadingLiveData.postValue(ResponseState(NETWORK_ERROR))
                        })
                    }
                    loadingLiveData.postValue(ResponseState(IDLE))
                }, {
                    App.instance.getLogger()!!.log("confirmCode error ")
                    loadingLiveData.postValue(ResponseState(NETWORK_ERROR))
                })
    }

    fun sendDeviceKey(refreshToken: String) {
        App.instance.getLogger()!!.log("sendDeviceKey start")
        apiManager.sendDeviceKey(SendDeviceKeyRequest(refreshToken)).subscribe({
            App.instance.getLogger()!!.log("sendDeviceKey suc")
            handleResponseState(it)
            Timber.d("sendDeviceKey: $refreshToken,  ${if (it.isSuccess!!) "success" else "false"}")
        }, { it ->
            App.instance.getLogger()!!.log("sendDeviceKey error")
            Timber.e(it)
        })
    }

    private fun enterApplication(response: UserStatusResponse) {
        val state = setAppState(response.driverStatus!!, response.moderationStatus!!, response.accessToWorkWithoutModeration!!)
        RxBus.publish(ProfileModeEvent(state))
        when (state) {
            AppState.NEW -> {
                enterAsNewUser()
            }
            AppState.ACTIVE_ONLINE -> {
                enterAsActiveUser(response)
            }
            AppState.ACTIVE_OFFLINE -> {
                enterAsOfflineUser()
            }
            AppState.MODERATION -> {
                enterAsModerationUser()
            }
            AppState.NOT_ACTIVE -> {
                enterAsNotActiveUser()
            }
            AppState.BLOCKED -> {
                enterAsBlockedUser(response)
            }
            AppState.NEED_COMPLETE -> {
                enterAsNeedCompleteUser()
            }
            AppState.LIMITED -> {
                enterAsOfflineUser()
            }
        }
    }

    private fun setDrawerState(response: UserStatusResponse) {
        val state = setAppState(response.driverStatus!!, response.moderationStatus!!, response.accessToWorkWithoutModeration!!)
        when (state) {
            AppState.NEW -> {
                DriverToolbar.IS_ONLINE_STATE = false
            }
            AppState.ACTIVE_ONLINE -> {
                drawerLockLiveData.postValue(false)
            }
            AppState.ACTIVE_OFFLINE -> {
                drawerLockLiveData.postValue(false)
            }
            AppState.MODERATION -> {
                drawerLockLiveData.postValue(false)
            }
            AppState.NOT_ACTIVE -> {
                drawerLockLiveData.postValue(true)
            }
            AppState.BLOCKED -> {
                drawerLockLiveData.postValue(true)
            }
            AppState.NEED_COMPLETE -> {
                drawerLockLiveData.postValue(false)
            }
        }
    }

    private fun cancelOrder(order: NewOrderInfo) {
        val request = TotalCostRequest(
                factCost = 0,
                actualCost = 0,
                factTime = 0,
                factPath = 0.0,
                longitude = 0.0,
                latitude = 0.0,
                route = listOf<Coordinate>(),
                eventTime = System.currentTimeMillis(),
                changedParams = null,
                waitingCost = 0.0,
                waitingTime = 0)
        JobFactory.instance.getJobManager()?.addJobInBackground(DataSynchronizationJob(order.id, request))
    }

    private fun enterAsNeedCompleteUser() {
        drawerLockLiveData.postValue(false)
        needComplete = true
        enterAsActiveUser()
    }

    private fun enterAsBlockedUser(response: UserStatusResponse) {
        drawerLockLiveData.postValue(true)
        needComplete = false
        if (response.blockedMessage != null) {
            setRootScreen(BLOCK_SCREEN, response.blockedMessage)
        } else {
            setRootScreen(BLOCK_SCREEN)
        }
    }

    private fun enterAsNotActiveUser() {
        drawerLockLiveData.postValue(true)
        needComplete = false
        setRootScreen(ACCOUNT_DISABLED_SCREEN)
    }

    private fun enterAsModerationUser() {
        drawerLockLiveData.postValue(false)
        needComplete = false
        setRootScreen(PROFILE_SCREEN, ProfileFragment.MODE_MODERATION)
    }

    private fun enterAsOfflineUser() {
        drawerLockLiveData.postValue(false)
        needComplete = false
        enterAsActiveUser()
    }

    private fun enterAsActiveUser(response: UserStatusResponse) {
        drawerLockLiveData.postValue(false)
        needComplete = false
        enterAsActiveUser()
    }

    private fun setOnlineStatus(response: UserStatusResponse) {
//        if (response.isOnline == true) {
//           // DriverToolbar.IS_ONLINE_STATE = true
//            RxBus.publish(SocketServiceEvent(SocketServiceEvent.SocketCommand.START))
//        } else {
        DriverToolbar.IS_ONLINE_STATE = false
        RxBus.publish(SocketServiceEvent(SocketServiceEvent.SocketCommand.STOP))
//        }
    }

    private fun enterAsNewUser() {
        DriverToolbar.IS_ONLINE_STATE = false
        switchScreen(SIMPLE_REGISTRATION_SCREEN)
    }


    private fun logOut() {
        val tokenCopy = "Bearer $token"
        Timber.d("LOGOUT logOut $tokenCopy")
        JobFactory.instance.getJobManager()?.addJobInBackground(LogoutJob(tokenCopy))
        userImageString = ""
        carImageString = ""
        userName = ""
        userRating = ""
        userPhone = ""
        isCardAdded = false
        doc1 = ""
        doc2 = ""
        doc3 = ""
        doc4 = ""
        drawerLockLiveData.postValue(true)
        setRootScreen(SPLASH_SCREEN)
        token = ""
    }

    private fun clearData() {
        userImageString = ""
        carImageString = ""
        userName = ""
        userRating = ""
        userPhone = ""
        isCardAdded = false
        doc1 = ""
        doc2 = ""
        doc3 = ""
        doc4 = ""
        token = ""
    }

    @SuppressLint("CheckResult")
    fun sendPhone(phoneNumber: String) {
        val langCode = langToIntCode(language)
        loadingLiveData.postValue(ResponseState(LOADING))
        App.instance.getLogger()!!.log("sendPhone start $phoneNumber")
        apiManager.sendPhone(SendPhoneRequest(phoneNumber, langCode))
                .subscribe({ response ->
                    handleResponseState(response)
                    if (response.isSuccess!!) {
                        context.getString(R.string.sms_sent).longToast(context!!)
                        switchToSmsLiveData.postValue(true)
                        launchSmsDelayTimer()
                    }else{
                        Toast.makeText(context,
                                response.error!!.message,
                                Toast.LENGTH_LONG)
                                .show()
                    }
                    App.instance.getLogger()!!.log("sendPhone end suc $phoneNumber")
                }, {
                    App.instance.getLogger()!!.log("sendPhone end error $phoneNumber")
                    loadingLiveData.postValue(ResponseState(NETWORK_ERROR))
                })
    }

    fun launchSmsDelayTimer() {
        smsDelayTimer?.run { if (isDisposed) startTimer() } ?: startTimer()
    }

    private fun startTimer() {
        delay = 120
        smsDelayTimer = Observable
                .interval(1, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(onNext = {
                    --delay
                    smsDelayLiveData.postValue(delay)
                    if (delay == 0) killTimer()
                }, onError = Timber::e)
    }

    private fun killTimer() = smsDelayTimer?.takeUnless { it.isDisposed }?.dispose()
}