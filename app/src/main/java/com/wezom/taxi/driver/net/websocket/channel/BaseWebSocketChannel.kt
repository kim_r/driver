package com.wezom.taxi.driver.net.websocket.channel

import android.content.SharedPreferences
import com.google.gson.Gson
import com.wezom.taxi.driver.data.models.CloseOrderAuto
import com.wezom.taxi.driver.data.models.DeletedBoradcast
import com.wezom.taxi.driver.data.models.NewOrderInfo
import com.wezom.taxi.driver.data.models.OrdersCount
import com.wezom.taxi.driver.data.models.map.Hexagon
import com.wezom.taxi.driver.ext.SOFT_FILTER
import com.wezom.taxi.driver.ext.TOKEN
import com.wezom.taxi.driver.ext.boolean
import com.wezom.taxi.driver.ext.string
import com.wezom.taxi.driver.net.response.BaseResponse
import com.wezom.taxi.driver.net.websocket.SocketListener
import com.wezom.taxi.driver.net.websocket.messages.BaseSocketMessage
import com.wezom.taxi.driver.net.websocket.models.CancelOrder
import com.wezom.taxi.driver.net.websocket.models.DisableOnlineStatus
import com.wezom.taxi.driver.presentation.app.App
import io.socket.client.IO
import io.socket.client.Socket
import okhttp3.OkHttpClient
import org.json.JSONObject
import timber.log.Timber
import java.lang.reflect.Type

/**
 * Created by zorin.a on 022 22.02.18.
 */

abstract class BaseWebSocketChannel(private val okHttpClient: OkHttpClient,
                                    private val gson: Gson,
                                    sharedPreferences: SharedPreferences) : SocketChannel {
    private var socket: Socket? = null
    var listener: SocketListener? = null
    protected val token: String by sharedPreferences.string(key = TOKEN)
    protected var softFilter: Boolean by sharedPreferences.boolean(SOFT_FILTER)
    abstract fun provideBaseUrl(): String

    init {
        socket = prepareSocket()
        initListener(socket)
    }

    override fun connect() {
        Timber.d("SOCKET CONNECT: token: $token, url: ${provideBaseUrl()}")
        socket?.connect()
    }

    override fun disconnect() {
        socket?.disconnect()
    }

    override fun ping() {
        socket?.emit(BaseSocketMessage.PING)
    }



    override fun <T : BaseSocketMessage> sendMessage(src: T,
                                                     type: Type) {
        val message = gson.toJson(src,
                type)
        val jsonObject = JSONObject(message)
        Timber.d("SOCKET sendMessage: $jsonObject")
        socket?.emit(src.room,
                jsonObject)
    }

    override fun emit(src: String) {
        socket?.emit(src)
    }

    protected fun <T> fromMessage(src: String,
                                  type: Type): T {
        return gson.fromJson<T>(src,
                type)
    }

    private fun prepareSocket(): Socket {
        Timber.d("SOCKET prepareSocket()")
        IO.setDefaultOkHttpCallFactory(okHttpClient)
        val opts = IO.Options()
        opts.callFactory = okHttpClient
        return IO.socket(provideBaseUrl(),
                opts)
    }

    private fun initListener(socket: Socket?) {
        //connect
        socket?.on(Socket.EVENT_CONNECT) {
            App.instance.getLogger()!!.log("EVENT_CONNECT")
            listener?.onConnected()
        }

        //login
        socket?.on("auth") { it ->
            Timber.d("SOCKET auth: ${it[0]}")
            App.instance.getLogger()!!.log("EVENT_AUTH ${it[0]}")
            listener?.onLoginDone()
        }

        socket?.on("errors") { it ->
            Timber.d("SOCKET errors: ${it[0]}")
            App.instance.getLogger()!!.log("EVENT_ERROR")
            val error = gson.fromJson(it[0].toString(),
                    BaseResponse::class.java)
          //  listener?.onError(error)
        }

        //does`t work
        socket?.on(Socket.EVENT_CONNECT_TIMEOUT) {
            Timber.d("SOCKET CONNECT_TIMEOUT")
            App.instance.getLogger()!!.log("EVENT_CONNECT_TIMEOUT")
            listener?.onConnectError()
        }

        //network error
        socket?.on(Socket.EVENT_CONNECT_ERROR) {
            Timber.d("SOCKET CONNECT_ERROR")
            App.instance.getLogger()!!.log("EVENT_CONNECT_ERROR")
            listener?.onConnectError()
        }

        //disconnect
        socket?.on(Socket.EVENT_DISCONNECT) {
            Timber.d("SOCKET DISCONNECT")
            App.instance.getLogger()!!.log("EVENT_DISCONNECT")
            listener?.onDisconnected()
        }

        //new orderInfo
        socket?.on("newOrderInfo") { orderInfoArray ->
            Timber.d("SOCKET newOrderInfo: ${orderInfoArray[0]}")
            App.instance.getLogger()!!.log("EVENT_ORDER")
            val src = orderInfoArray[0].toString()
            val nof = fromMessage<NewOrderInfo>(src,
                    NewOrderInfo::class.java)
            listener?.onNewOrder(nof)
        }

        socket?.on("submitCoordinates") { orderInfoArray ->
            App.instance.getLogger()!!.log("EVENT_COORDINATES")
            listener?.onCoordinatesSubmitted()
        }
        socket?.on("broadcast") { orderInfoArray ->
            Timber.d("SOCKET broadcast: ${orderInfoArray[0]}")
            App.instance.getLogger()!!.log("EVENT_BROADCAST")
            val src = orderInfoArray[0].toString()
            val nof = fromMessage<NewOrderInfo>(src,
                    NewOrderInfo::class.java)
            listener?.onNewBroadcast(nof)
        }
        socket?.on("deleteBroadcast") { data ->
            App.instance.getLogger()!!.log("EVENT_DELETE_BROADCAST")
            val src = data[0].toString()
            Timber.d("SOCKET deleteBroadcast: $src")
            val delBroadcast: DeletedBoradcast = fromMessage(src,
                    DeletedBoradcast::class.java)
            listener?.onDeleteBroadcast(delBroadcast.orderId)
        }

        socket?.on("closeOrderAutomatically") { data ->
            App.instance.getLogger()!!.log("CLOSE_ORDER_AUTO")
            val src = data[0].toString()
            Timber.d("SOCKET deleteBroadcast: $src")
            val closeOrder: CloseOrderAuto = fromMessage(src,
                    CloseOrderAuto::class.java)
            listener?.onCloseOrderAutomatically(closeOrder)
        }

        socket?.on("broadcastOrdersCount") {
            val src = it[0].toString()
            App.instance.getLogger()!!.log("EVENT_COUNT_BROADCAST")
            Timber.d("SOCKET broadcastOrdersCount: ${src}")
            val oc: OrdersCount = fromMessage(src,
                    OrdersCount::class.java)
            if (oc.count <= oc.filteredCount || softFilter) {
                listener?.onBroadcastOrdersCount(count = oc.count)
            } else {
                listener?.onBroadcastOrdersCount(count = oc.count - oc.filteredCount)
            }
        }

//        emit("tariffedRegions")

        socket?.on("tariffedRegions") {
            App.instance.getLogger()!!.log("EVENT_TARIFFED_RED")
            Timber.d("SOCKET tariffedRegions: ${it[0]}")
            val hexagon = gson.fromJson(it[0].toString(),
                    Hexagon::class.java)
            listener?.onHexagonReceived(hexagon)
        }

//        emit("cancelOrder")

        socket?.on("cancelOrder") {
            App.instance.getLogger()!!.log("EVENT_CANCEL_ORDER")
            Timber.d("SOCKET cancelOrder: ${it[0]}")
            val response = gson.fromJson(it[0].toString(),
                    CancelOrder::class.java)
            listener?.onCancelOrder(response.orderId)
        }

//        emit("disableOnlineStatus")

        socket?.on("disableOnlineStatus") {
            Timber.d("SOCKET disableOnlineStatus: ${it[0]}")
            App.instance.getLogger()!!.log("EVENT_ONLINE STATUS")
            val response = gson.fromJson(it[0].toString(),
                    DisableOnlineStatus::class.java)
            listener?.onDisableOnlineStatus(response.isOnline)
        }
    }

    fun isConnected() = socket?.connected()
}