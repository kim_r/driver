package com.wezom.taxi.driver.presentation.privatestatistics

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import android.view.View.GONE
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.data.models.Driver
import com.wezom.taxi.driver.databinding.PrivateStatisticsBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.presentation.base.BaseFragment
import com.wezom.taxi.driver.presentation.base.lists.RecyclerTouchListener
import com.wezom.taxi.driver.presentation.base.lists.decorator.SpacesItemDecoration
import com.wezom.taxi.driver.presentation.privatestatistics.list.CommentsAdapter
import javax.inject.Inject

/**
 * Created by zorin.a on 14.03.2018.
 */

class PrivateStatisticsFragment : BaseFragment() {
    //region var
    lateinit var viewModel: PrivateStatisticsViewModel
    lateinit var binding: PrivateStatisticsBinding
    @Inject
    lateinit var adapter: CommentsAdapter
    @Inject
    lateinit var touchListener: RecyclerTouchListener
    @Inject
    lateinit var decorator: SpacesItemDecoration

    private val statisticsObserver: Observer<Driver> = Observer { driver ->
        if (driver != null) {
            updateUi(driver)
        }
    }
    //endregion

    //region override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()

        viewModel.statisticsLiveData.observe(this, statisticsObserver)
        viewModel.loadingLiveData.observe(this, progressObserver)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = PrivateStatisticsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            toolbar.setToolbarTitle(getString(R.string.private_statistics))
        }
        setupRecyclerView()
        viewModel.getPersonalStatistics()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.private_statistics_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        showInfoDialog(R.string.information, R.string.private_statistics_info)
        return super.onOptionsItemSelected(item)
    }
    //endregion

    //region fun

    @SuppressLint("SetTextI18n")
    private fun updateUi(driver: Driver) {
        binding.run {
            ratingValue.text = if(driver.rating != null) driver.rating.toString() else "0"
            ratingBar.rating = if (driver.rating != null) driver.rating.toFloat() else 0f
            ratingDescription.text = getString(R.string.rating_description, driver.fiveStarsCount.toString())
            acceptedOrdersPercent.text = "${Math.round(driver.acceptedOrders!!)}%"
            completedOrdersPercent.text = "${Math.round(driver.completedOrders!!)}%"

            if (driver.comments != null && driver.comments.isNotEmpty()) {
                adapter.setData(driver.comments)
                emptyView.visibility = GONE
            }
        }
    }

    private fun setupRecyclerView() {
        binding.run {
            list.adapter = adapter
            list.setHasFixedSize(false)
            decorator.space = 24
            list.addItemDecoration(decorator)
            list.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
            list.addOnItemTouchListener(touchListener)
            touchListener.rv = list
            touchListener.listener = object : RecyclerTouchListener.ClickListener {
                override fun onPress(position: Int, view: View) {
//                    "onPress $position".shortToast(context)
                }

                override fun onLongPress(position: Int, view: View) {
//                    "onLongPress $position".shortToast(context)
                }
            }
        }
    }
    //endregion
}