package com.wezom.taxi.driver.presentation.profile

import android.arch.lifecycle.MutableLiveData
import android.content.Intent
import android.content.SharedPreferences
import com.wezom.taxi.driver.common.*
import com.wezom.taxi.driver.ext.USER_IMAGE_URI
import com.wezom.taxi.driver.ext.USER_PHONE
import com.wezom.taxi.driver.ext.string
import com.wezom.taxi.driver.presentation.addcard.AddCardActivity
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.presentation.photoviewer.PhotoViewerFragment.Companion.CropOptions.CROP_ROUNDED
import com.wezom.taxi.driver.repository.DriverRepository
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import ru.terrakok.cicerone.result.ResultListener
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by zorin.a on 28.02.2018.
 */
class AboutUserViewModel @Inject constructor(screenRouterManager: ScreenRouterManager,
                                             sharedPreferences: SharedPreferences,
                                             val repository: DriverRepository) : BaseViewModel(screenRouterManager) {
    var phoneLiveData = MutableLiveData<String>()
    var userAvatarLiveData = MutableLiveData<String>()
    var croppedProfileImageLiveData = MutableLiveData<String>()
    val requiredDocumentLiveData = MutableLiveData<Boolean>()

    private var phone: String by sharedPreferences.string(USER_PHONE)
    private var userUriString by sharedPreferences.string(USER_IMAGE_URI)

    fun getUserPhone() {
        phoneLiveData.postValue(phone)
    }

    fun setUserPhone(phone: String) {
        this.phone = phone
    }

    fun saveUserImageUri(uriString: String) {
        userUriString = uriString
    }

    fun getUserImageUri() {
        userAvatarLiveData.postValue(userUriString)
    }

    fun deleteUserImage() {
        userUriString = ""
        userAvatarLiveData.postValue(userUriString)
    }

    fun cropImage(source: String) {
        startScreenForResult(PHOTO_VIEWER_SCREEN, Pair(source, CROP_ROUNDED), ResultListener { it ->
            removeResultListener(Constants.REQUEST_IMAGE_CAPTURE)
            if (it != null)
                croppedProfileImageLiveData.postValue(it as String)
        }, Constants.REQUEST_IMAGE_CAPTURE)
    }

    fun switchToAddCardScreen(activity: AboutUserFragment) {
        Timber.d("ADD_CARD: switchToAddCardScreen()")
        App.instance.getLogger()!!.log("getCardVerificationInfo start ")
        disposable += repository.getCardVerificationInfo()
                .subscribeBy(
                        onSuccess = {
                            if (it.isSuccess == true) {
                                App.instance.getLogger()!!.log("getCardVerificationInfo suc ")
                                Timber.d("ADD_CARD: startAddCardActivity()")
                                val intent = Intent(context, AddCardActivity::class.java).apply {
                                    putExtra(AddCardActivity.PAYMENT_INFO, it.paymentInfo)
//                                    flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
//                                    flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                }
                                activity.startActivityForResult(intent, 100)
//                                    AddCardActivity.startAddCardActivity(context, it.paymentInfo)
                            }
                        }, onError = {
                    App.instance.getLogger()!!.log("getCardVerificationInfo error ")
                }
                )
    }

    fun switchToChangePhoneScreen(data: String) {
        switchScreen(CHANGE_PHONE_SCREEN, data)
    }

    fun getRequiredDocs() {
        App.instance.getLogger()!!.log("getCardVerificationInfo start ")
        repository.getRequiredDocuments().subscribe({
            handleResponseState(it)
            App.instance.getLogger()!!.log("getCardVerificationInfo suc ")
            if (it!!.isSuccess!!) {
                requiredDocumentLiveData.postValue(it.isDriverPhotoRequired)
            }
        }, {
            App.instance.getLogger()!!.log("getCardVerificationInfo error ")
            Timber.d(it)
        })
    }
}