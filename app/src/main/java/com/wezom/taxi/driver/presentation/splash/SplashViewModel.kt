package com.wezom.taxi.driver.presentation.splash

import android.arch.lifecycle.MutableLiveData
import android.content.SharedPreferences
import com.google.firebase.iid.FirebaseInstanceId
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.bus.events.ProfileModeEvent
import com.wezom.taxi.driver.common.*
import com.wezom.taxi.driver.data.models.*
import com.wezom.taxi.driver.data.models.ResponseState.State.NETWORK_ERROR
import com.wezom.taxi.driver.ext.*
import com.wezom.taxi.driver.net.jobqueue.JobFactory
import com.wezom.taxi.driver.net.jobqueue.jobs.DataSynchronizationJob
import com.wezom.taxi.driver.net.request.SendDeviceKeyRequest
import com.wezom.taxi.driver.net.request.TotalCostRequest
import com.wezom.taxi.driver.net.response.UserStatusResponse
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.presentation.customview.DriverToolbar
import com.wezom.taxi.driver.presentation.customview.DriverToolbar.Companion.IS_ONLINE_STATE
import com.wezom.taxi.driver.presentation.main.events.SocketServiceEvent
import com.wezom.taxi.driver.presentation.main.events.SocketServiceEvent.SocketCommand.START
import com.wezom.taxi.driver.presentation.main.events.SocketServiceEvent.SocketCommand.STOP
import com.wezom.taxi.driver.presentation.profile.ProfileFragment
import com.wezom.taxi.driver.presentation.profile.events.UpdateUserDataEvent
import com.wezom.taxi.driver.presentation.profile.events.UpdateUserImageEvent
import com.wezom.taxi.driver.repository.DriverRepository
import com.wezom.taxi.driver.service.SocketService
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.lang.ref.WeakReference
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by zorin.a on 16.02.2018.
 */
class SplashViewModel @Inject constructor(screenRouterManager: ScreenRouterManager,
                                          private val repository: DriverRepository,
                                          private val orderManager: OrderManager,
                                          sharedPreferences: SharedPreferences) : BaseViewModel(screenRouterManager) {

    private val isTutorCompleted: Boolean by sharedPreferences.boolean(IS_TUTOR_COMPLETED)
    private var token: String by sharedPreferences.string(TOKEN)
    private var userBalance: String by sharedPreferences.string(USER_BALANCE)
    private var coordinatesUpdateTime: Long by sharedPreferences.long(key = COORDINATES_UPDATE_TIME, default = 10000)
    private var userImageString: String by sharedPreferences.string(USER_IMAGE_URI)
    private var userName: String by sharedPreferences.string(USER_NAME_CACHED)
    private var userRating: String by sharedPreferences.string(USER_RATING_CACHED)
    private var userBalanceId by sharedPreferences.int(USER_BALANCE_ID)
    private var needComplete: Boolean by sharedPreferences.boolean(IS_NEED_COMPLETE)
    private var userId by sharedPreferences.int(USER_ID)
    private var finalOrder: Long by sharedPreferences.long(FINAL_ORDER)

    var notEnoughMoneyLiveData = MutableLiveData<Boolean>()

    private var nextAction: (() -> Unit)? = null

    fun switchScreen() {
        if (isTutorCompleted) {
            if (token.isEmpty()) {
                nextAction = { showVerificationScreen() }
            } else {
                val token = FirebaseInstanceId.getInstance().token
                token?.let {
                    sendDeviceKey(it)
                }
                App.instance.getLogger()!!.log("checkUserStatus start ")
                disposable += repository.checkUserStatus().subscribe({ response ->
                    handleResponseState(response)
                    App.instance.getLogger()!!.log("checkUserStatus suc ")
                    if (response?.isSuccess!!) {
                        val user = response.user

                        if (user != null) {
                            userId = user.id!!
                            user.photo?.let {
                                userImageString = it
                            }
                            user.name?.let {
                                userName = it
                            }

                            user.balanceId?.let {
                                userBalanceId
                            }

                            userRating = user.rating?.toString() ?: "0.0"
                            RxBus.publish(UpdateUserDataEvent())
                            RxBus.publish(UpdateUserImageEvent())
                        }

                        setOnlineStatus(response)
                        userBalance = response.balance.toString()
                        coordinatesUpdateTime = response.coordinatesUpdateTime!!
                        orderManager.wipeDatabase()
                        if (response.isCurrentOrderExist!!) {

                            App.instance.getLogger()!!.log("currentOrders start ")
                            disposable += repository.getCurrentOrders()
                                    .subscribe({ currentOrdersResponse ->
                                        App.instance.getLogger()!!.log("currentOrders suc ")

                                        val orders = currentOrdersResponse.orders

                                        if (orders.isNotEmpty()) {
                                            val primaryOrder = orders[0]

                                            if (finalOrder == primaryOrder.order!!.id!!.toLong()) {
                                                orderManager.wipeDatabase()
                                                enterApplication(response)
                                                finalOrder = -1
                                            } else {

                                                val actualCost = currentOrdersResponse?.restoredData?.actualCost
                                                if (actualCost != null)
                                                    primaryOrder.order?.actualCost = actualCost
                                                if (primaryOrder.order?.status == OrderStatus.NOT_COMPLETED) {
                                                    //send request to cancel order
                                                    cancelOrder(primaryOrder)
                                                } else {
                                                    if (!orderManager.isPrimaryOrderExist()) {
                                                        orderManager.addPrimaryOrderToDb(primaryOrder)
                                                    }
                                                }

                                                if (orders.size > 1) {
                                                    val secondaryOrder = orders[1]
                                                    if (secondaryOrder.order?.status == OrderStatus.NOT_COMPLETED) {
                                                        //send request to cancel order
                                                        cancelOrder(secondaryOrder)
                                                    } else {
                                                        orderManager.addSecondaryOrderToDb(secondaryOrder)
                                                        DriverToolbar.IS_SECONDARY_ORDER_EXIST = true
                                                    }
                                                }
                                                setDrawerState(response)
                                                nextAction = if (primaryOrder.order!!.status != OrderStatus.NOT_COMPLETED) {
                                                    { restoreOrder(primaryOrder, currentOrdersResponse.restoredData, orderManager.getOrderResult(primaryOrder.id)) }
                                                } else {
                                                    { enterApplication(response) }
                                                }
                                            }
                                        } else {
                                            enterApplication(response)
                                        }
                                    }, {
                                        App.instance.getLogger()!!.log("currentOrders error ")
                                        Timber.d("Can`t get current orders")
                                    })
                        } else {
                            enterApplication(response)
                        }
                    }
                }, { it ->
                    App.instance.getLogger()!!.log("checkUserStatus error ")
                    Timber.e(it)
                })
            }
        } else {
            nextAction = { showTutorialScreen() }
        }


        disposable += Completable.timer(2, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    Timber.d("NEXT_STATE nextAction?.invoke()")
                    if (nextAction != null) {
                        nextAction?.invoke()
                    } else {
                        getOrderFromCache()
                    }

                }, { it ->
                    Timber.d(it)
                })
    }

    private fun cancelOrder(order: NewOrderInfo) {
        val request = TotalCostRequest(
                factCost = 0,
                actualCost = 0,
                factTime = 0,
                factPath = 0.0,
                longitude = 0.0,
                latitude = 0.0,
                route = listOf<Coordinate>(),
                eventTime = System.currentTimeMillis(),
                changedParams = null,
                waitingCost = 0.0,
                waitingTime = 0)
        JobFactory.instance.getJobManager()?.addJobInBackground(DataSynchronizationJob(order.order!!.id!!, request))
    }

    private fun getOrderFromCache() {
        if (orderManager.isSecondaryOrderExist()) {
            DriverToolbar.IS_SECONDARY_ORDER_EXIST = true
        }
        if (orderManager.isPrimaryOrderExist()) {
            restoreOrder(orderManager.getPrimaryOrder()!!, RestoredData(ChangedParams.NO_CHANGES,
                    orderManager.getPrimaryOrder()!!.order?.actualCost ?: 0),
                    orderManager.getOrderResult(orderManager.getPrimaryOrder()!!.id))
        }
        if (orderManager.getOrdersCount() <= 0) {
            loadingLiveData.postValue(ResponseState(NETWORK_ERROR))
        }
    }

    private fun showVerificationScreen() {
        drawerLockLiveData.postValue(true)
        setRootScreen(VERIFICATION_PHONE_SCREEN)
    }

    private fun enterApplication(response: UserStatusResponse) {
        val state = setAppState(response.driverStatus!!, response.moderationStatus!!, response.accessToWorkWithoutModeration!!)
        RxBus.publish(ProfileModeEvent(state))
        when (state) {
            AppState.NEW -> {
                nextAction = { enterAsNewUser() }
            }
            AppState.ACTIVE_ONLINE -> {
                nextAction = { enterAsActiveUser(response) }
            }
            AppState.ACTIVE_OFFLINE -> {
                nextAction = { enterAsOfflineUser() }
            }
            AppState.MODERATION -> {
                nextAction = { enterAsModerationUser() }
            }
            AppState.NOT_ACTIVE -> {
                nextAction = { enterAsNotActiveUser() }
            }
            AppState.BLOCKED -> {
                nextAction = { enterAsBlockedUser(response) }
            }
            AppState.NEED_COMPLETE -> {
                nextAction = { enterAsNeedCompleteUser() }
            }
            AppState.LIMITED -> {
                nextAction = { enterAsOfflineUser() }
            }
        }
    }

    private fun setDrawerState(response: UserStatusResponse) {
        val state = setAppState(response.driverStatus!!, response.moderationStatus!!, response.accessToWorkWithoutModeration!!)
        when (state) {
            AppState.NEW -> {
                IS_ONLINE_STATE = false
            }
            AppState.ACTIVE_ONLINE -> {
                drawerLockLiveData.postValue(false)
            }
            AppState.ACTIVE_OFFLINE -> {
                drawerLockLiveData.postValue(false)
            }
            AppState.MODERATION -> {
                drawerLockLiveData.postValue(false)
            }
            AppState.NOT_ACTIVE -> {
                drawerLockLiveData.postValue(true)
            }
            AppState.BLOCKED -> {
                drawerLockLiveData.postValue(true)
            }
            AppState.NEED_COMPLETE -> {
                drawerLockLiveData.postValue(false)
            }
            else -> {
            }
        }
    }

    private fun showTutorialScreen() {
        drawerLockLiveData.postValue(true)
        setRootScreen(TUTORIAL_SCREEN)
    }

    private fun enterAsNeedCompleteUser() {
        drawerLockLiveData.postValue(false)
        needComplete = true
        enterAsActiveUser()
    }

    private fun enterAsBlockedUser(response: UserStatusResponse) {
        drawerLockLiveData.postValue(true)
        needComplete = false
        if (response.blockedMessage != null) {
            setRootScreen(BLOCK_SCREEN, response.blockedMessage)
        } else {
            setRootScreen(BLOCK_SCREEN)
        }
    }

    private fun enterAsNotActiveUser() {
        drawerLockLiveData.postValue(true)
        needComplete = false
        setRootScreen(ACCOUNT_DISABLED_SCREEN)
    }

    private fun enterAsModerationUser() {
        drawerLockLiveData.postValue(false)
        needComplete = false
        setRootScreen(PROFILE_SCREEN, ProfileFragment.MODE_MODERATION)
    }

    private fun enterAsOfflineUser() {
        drawerLockLiveData.postValue(false)
        needComplete = false
        enterAsActiveUser()
    }

    private fun enterAsActiveUser(response: UserStatusResponse) {
        drawerLockLiveData.postValue(false)
        needComplete = false
        if (response.isMoneyEnough == true) {
            setOnlineStatus(response)
        }
        enterAsActiveUser()
    }

    private fun setOnlineStatus(response: UserStatusResponse) {
        SocketService.start(WeakReference(context))
        if (response.isOnline == true) {
            IS_ONLINE_STATE = true
            RxBus.publish(SocketServiceEvent(START))
        } else {
            IS_ONLINE_STATE = false
            RxBus.publish(SocketServiceEvent(STOP))
        }
    }


    private fun enterAsNewUser() {
        IS_ONLINE_STATE = false
        setRootScreen(VERIFICATION_PHONE_SCREEN)
    }

    fun sendDeviceKey(refreshToken: String) {
        App.instance.getLogger()!!.log("sendDeviceKey start ")
        disposable += repository.sendDeviceKey(SendDeviceKeyRequest(refreshToken)).subscribe({
            handleResponseState(it)
            App.instance.getLogger()!!.log("sendDeviceKey suc ")
            Timber.d("sendDeviceKey: $refreshToken,  ${if (it.isSuccess!!) "success" else "false"}")
        }, { App.instance.getLogger()!!.log("sendDeviceKey error ") })
    }
}