package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.Document

/**
 * Created by zorin.a on 021 21.02.18.
 */
class RequiredDocsListResponse(@SerializedName("documents") val documents: List<Document>?,
                               @SerializedName("isCarPhotoRequired") val isCarPhotoRequired: Boolean?,
                               @SerializedName("isDriverPhotoRequired") val isDriverPhotoRequired: Boolean?) : BaseResponse()