package com.wezom.taxi.driver.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by zorin.a on 25.04.2018.
 */
enum class DriverStatus(value: Int) {
    @SerializedName("0")
    NOT_ACTIVE(0),
    @SerializedName("1")
    ACTIVE(1),
    @SerializedName("2")
    NEW_DRIVER(2),
    @SerializedName("3")
    BLOCKED(3),
    @SerializedName("4")
    NEED_COMPLETE(4),
    @SerializedName("5")
    LIMITED(5)
}