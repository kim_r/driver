package com.wezom.taxi.driver.net.request

import com.google.gson.annotations.SerializedName
import java.io.Serializable


/**
 * Created by zorin.a on 022 22.02.18.
 */

data class DriverEventRequest(
        @SerializedName("longitude") val longitude: Double?,
        @SerializedName("latitude") val latitude: Double?,
        @SerializedName("eventTime") val eventTime: Long?) : Serializable