package com.wezom.taxi.driver.net.jobqueue.jobs

import android.annotation.SuppressLint
import com.wezom.taxi.driver.presentation.app.App
import timber.log.Timber

/**
 * Created by zorin.a on 17.05.2018.
 */
class LogoutJob constructor(private val token: String) : BaseJob() {
    @SuppressLint("CheckResult")
    override fun onRun() {
        Timber.d("LOGOUT LogoutJob $token")
        App.instance.getLogger()!!.log("logout start and end ")
        apiManager.logout(token).blockingGet()
    }
}