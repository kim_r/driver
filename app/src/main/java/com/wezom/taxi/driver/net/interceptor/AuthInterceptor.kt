package com.wezom.taxi.driver.net.interceptor

import android.content.SharedPreferences
import com.wezom.taxi.driver.ext.TOKEN
import com.wezom.taxi.driver.ext.string
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

/**
 * Created by zorin.a on 020 20.02.18.
 */
class AuthInterceptor @Inject constructor(prefs: SharedPreferences) : Interceptor {
    private val token: String by prefs.string(key = TOKEN)
    override fun intercept(chain: Interceptor.Chain): Response? {

        val original = chain.request()
        val request = original.newBuilder()
                .addHeader("Accept", "application/json")
        if (token.isNotEmpty()) {
            val modifiedToken = "Bearer $token"
            request.header("Authorization", modifiedToken)
        }

        return chain.proceed(request.build())
    }
}