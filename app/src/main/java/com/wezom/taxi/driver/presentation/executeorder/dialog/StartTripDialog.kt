package com.wezom.taxi.driver.presentation.executeorder.dialog

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import com.wezom.taxi.driver.R
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import java.util.concurrent.TimeUnit


/**
 * Created by zorin.a on 05.03.2018.
 */
class StartTripDialog : DialogFragment() {
    var listener: DialogClickListener? = null
    private var disposable = CompositeDisposable()
    private var count: Int = 15

    @SuppressLint("DefaultLocale")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = AlertDialog.Builder(context!!, R.style.BlackDialogTheme)
                .setTitle(getString(R.string.attention2))
                .setMessage(getString(R.string.start_trip_driver) + " $count " + getString(R.string.sec))
                .setPositiveButton(getString(R.string.start).toUpperCase()) { _, _ ->
                    disposable.dispose()
                    if (listener != null)
                        listener?.onPositiveClick()
                    dialog.dismiss()

                }
                .setNegativeButton(getString(R.string.cancel_1).toUpperCase()) { _, _ ->
                    disposable.dispose()
                    if (listener != null)
                        listener?.onNegativeClick()
                    dialog.dismiss()
                }
                .create()

        disposable += Observable.interval(0,
                1,
                TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (count == 1) {
                        disposable.dispose()
                        listener?.onPositiveClick()
                    }
                    count--
                    dialog.setMessage(getString(R.string.start_trip_driver) + " $count " + getString(R.string.sec))
                }
        return dialog
    }

    companion object {
        val TAG: String = StartTripDialog.javaClass.simpleName
    }

    interface DialogClickListener {
        fun onPositiveClick()
        fun onNegativeClick()
    }
}