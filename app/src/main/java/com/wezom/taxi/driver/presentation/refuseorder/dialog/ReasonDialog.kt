package com.wezom.taxi.driver.presentation.refuseorder.dialog

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.data.models.Reason
import com.wezom.taxi.driver.ext.toArray

/**
 * Created by udovik.s on 26.03.2018.
 */
class ReasonDialog : DialogFragment() {
    private var data: List<Reason>? = null
    private var pos: Int? = null
    var listener: ChoiceDialogSelection? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        data = arguments?.getParcelableArrayList(DATA_KEY)
        pos = arguments?.getInt(CHECKED_ITEM_KEY)

        var stringData: List<String?>? = null
        if (data != null) {
            stringData = data!!.map { reason -> reason.name.toString() }
        }
        return AlertDialog.Builder(context!!)
                .setSingleChoiceItems(toArray(stringData!!), pos!!) { _, pos ->
                    listener?.onSelected(pos, data!![pos])
                    this.dismiss()
                }
                .setNegativeButton(R.string.cancel_1, { _, _ -> this.dismiss() })
                .create()
    }

    companion object {
        val TAG: String = "ReasonDialog"
        const val DATA_KEY: String = "data_key"
        const val CHECKED_ITEM_KEY: String = "checked_item_key"

        fun newInstance(data: ArrayList<Reason>, checkedItem: Int?): ReasonDialog {
            val args = Bundle()
            args.putParcelableArrayList(DATA_KEY, data)
            checkedItem?.let { args.putInt(CHECKED_ITEM_KEY, it) }
            val fragment = ReasonDialog()
            fragment.arguments = args
            return fragment
        }
    }

    interface ChoiceDialogSelection {
        fun onSelected(position: Int, reason: Reason)
    }
}