package com.wezom.taxi.driver.presentation.customview

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import com.wezom.taxi.driver.databinding.ButtonFilterBinding
import com.wezom.taxi.driver.ext.setVisible

/**
 *Created by Zorin.A on 18.June.2019.
 */
class FilterButton : ConstraintLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context,
                attrs: AttributeSet?) : super(context,
                                              attrs)

    constructor(context: Context,
                attrs: AttributeSet?,
                attributeSetId: Int) : super(context,
                                             attrs,
                                             attributeSetId)

    private val binding = ButtonFilterBinding.inflate(LayoutInflater.from(context),
                                                      this,
                                                      true)

    var clickListener: (() -> Unit)? = null

    init {
        binding.filterButtonImage.setOnClickListener {
            clickListener?.invoke()
        }
    }

    fun updateCounter(newValue: Int) {
        binding.counter.text = newValue.toString()
    }

    fun increaseCounter() {
        val count = binding.counter.text.toString()
                .toInt()
        val newValue = count + 1
        updateCounter(newValue)
    }

    fun decreaseCounter() {
        val count = binding.counter.text.toString()
                .toInt()
        if (count > 0) {
            val newValue = count - 1
            updateCounter(newValue)
        }
    }

    fun show() {
        setVisible(true)
    }
}