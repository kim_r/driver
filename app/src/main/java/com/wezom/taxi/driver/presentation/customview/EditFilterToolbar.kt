package com.wezom.taxi.driver.presentation.customview

import android.content.Context
import android.support.v7.widget.Toolbar
import android.util.AttributeSet
import android.view.LayoutInflater
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.databinding.EditFilterToolbarBinding
import com.wezom.taxi.driver.presentation.filter.base.DeleteFilterEvent
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber

/**
 * Created by zorin.a on 022 22.02.18.
 */

class EditFilterToolbar @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, attributeSetId: Int = 0) : Toolbar(context, attrs, attributeSetId) {
    private var binding: EditFilterToolbarBinding = EditFilterToolbarBinding.inflate(LayoutInflater.from(context), this, true)

    fun setToolbarTitle(title: String) {
        binding.toolbar.title = title
    }

    private var disposable = CompositeDisposable()

    override fun onDetachedFromWindow() {
        disposable.dispose()
        Timber.d("TOOLBAR SOCKET IS ONLINE onDetachedFromWindow: ${DriverToolbar.IS_ONLINE_STATE}")
        super.onDetachedFromWindow()
    }

    init {
        disposable += RxView.clicks(binding.delete)
                .subscribe {
                    RxBus.publish(DeleteFilterEvent())
                }
    }
}