package com.wezom.taxi.driver.bus.events

import com.wezom.taxi.driver.data.models.User

class UpdateCardDataEvent(var user: User)