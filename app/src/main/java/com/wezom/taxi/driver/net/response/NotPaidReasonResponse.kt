package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.Reason

/**
 * Created by udovik.s on 27.03.2018.
 */
class NotPaidReasonResponse(@SerializedName("reasons") val reasons: List<Reason>) : BaseResponse()