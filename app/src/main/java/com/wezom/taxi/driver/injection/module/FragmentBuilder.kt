package com.wezom.taxi.driver.injection.module

import com.wezom.taxi.driver.presentation.acceptorder.AcceptOrderFragment
import com.wezom.taxi.driver.presentation.addcard.AddCardFragment
import com.wezom.taxi.driver.presentation.balance.*
import com.wezom.taxi.driver.presentation.block.BlockFragment
import com.wezom.taxi.driver.presentation.bonus_program.BonusProgramFragment
import com.wezom.taxi.driver.presentation.changephone.ChangePhoneFragment
import com.wezom.taxi.driver.presentation.changephoneverification.ChangePhoneVerificationFragment
import com.wezom.taxi.driver.presentation.completedorder.CompletedOrderFragment
import com.wezom.taxi.driver.presentation.disabled.DisabledUserFragment
import com.wezom.taxi.driver.presentation.discounts.DiscountsFragment
import com.wezom.taxi.driver.presentation.ether.EtherFragment
import com.wezom.taxi.driver.presentation.executeorder.ExecuteOrderFragment
import com.wezom.taxi.driver.presentation.filter.CreateFilterFragment
import com.wezom.taxi.driver.presentation.filter.EditFilterFragment
import com.wezom.taxi.driver.presentation.filter.FilterFragment
import com.wezom.taxi.driver.presentation.gains.GainsFragment
import com.wezom.taxi.driver.presentation.help.HelpFragment
import com.wezom.taxi.driver.presentation.history.OrderHistoryListFragment
import com.wezom.taxi.driver.presentation.imagepicker.ImagePickerFragment
import com.wezom.taxi.driver.presentation.mainmap.MainMapFragment
import com.wezom.taxi.driver.presentation.neworder.NewOrderFragment
import com.wezom.taxi.driver.presentation.notpaid.NotPaidFragment
import com.wezom.taxi.driver.presentation.orderfound.OrderFoundFragment
import com.wezom.taxi.driver.presentation.photoviewer.PhotoViewerFragment
import com.wezom.taxi.driver.presentation.picklocation.PickLocationFragment
import com.wezom.taxi.driver.presentation.picklocation.PickLocationMapFragment
import com.wezom.taxi.driver.presentation.privatestatistics.PrivateStatisticsFragment
import com.wezom.taxi.driver.presentation.profile.AboutCarFragment
import com.wezom.taxi.driver.presentation.profile.AboutUserFragment
import com.wezom.taxi.driver.presentation.profile.DocumentsFragment
import com.wezom.taxi.driver.presentation.profile.ProfileFragment
import com.wezom.taxi.driver.presentation.rating.RatingFragment
import com.wezom.taxi.driver.presentation.refuseorder.RefuseOrderFragment
import com.wezom.taxi.driver.presentation.refuseorder.TotalCostFragment
import com.wezom.taxi.driver.presentation.registration.SimpleRegistrationFragment
import com.wezom.taxi.driver.presentation.selectwaypoint.SelectWayPointFragment
import com.wezom.taxi.driver.presentation.settings.SettingSignalFragment
import com.wezom.taxi.driver.presentation.settings.SettingsFragment
import com.wezom.taxi.driver.presentation.splash.SplashFragment
import com.wezom.taxi.driver.presentation.tutorial.TutorialFragment
import com.wezom.taxi.driver.presentation.userinfo.UserInfoFragment
import com.wezom.taxi.driver.presentation.verification.AuthFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by zorin.a on 16.02.2018.
 */

@Suppress("unused")
@Module
interface FragmentBuilder {

    @ContributesAndroidInjector
    fun contributeSplashFragment(): SplashFragment

    @ContributesAndroidInjector
    fun contributeTutorialFragment(): TutorialFragment

    @ContributesAndroidInjector
    fun contributeVerificationPhoneFragment(): AuthFragment

    @ContributesAndroidInjector
    fun contributeMainMapFragment(): MainMapFragment

    @ContributesAndroidInjector
    fun contributeProfileFragment(): ProfileFragment

    @ContributesAndroidInjector
    fun contributeAboutUserFragment(): AboutUserFragment

    @ContributesAndroidInjector
    fun contributeAboutCarFragment(): AboutCarFragment

    @ContributesAndroidInjector
    fun contributeDocumentsFragment(): DocumentsFragment

    @ContributesAndroidInjector
    fun contributeAddCardFragment(): AddCardFragment

    @ContributesAndroidInjector
    fun contributePhotoViewerFragment(): PhotoViewerFragment

    @ContributesAndroidInjector
    fun contributeOrderFoundFragment(): OrderFoundFragment

    @ContributesAndroidInjector
    fun contributeOrderHistoryListFragment(): OrderHistoryListFragment

    @ContributesAndroidInjector
    fun contributeChangePhoneFragment(): ChangePhoneFragment

    @ContributesAndroidInjector
    fun contributeSettingsFragment(): SettingsFragment

    @ContributesAndroidInjector
    fun contributePrivateStatisticsFragment(): PrivateStatisticsFragment

    @ContributesAndroidInjector
    fun contributeCompletedOrderFragment(): CompletedOrderFragment

    @ContributesAndroidInjector
    fun contributeBalanceFragment(): BalanceFragment

    @ContributesAndroidInjector
    fun contributeHowToReplenishFragment(): HowToReplenishFragment

    @ContributesAndroidInjector
    fun contributeDiscountsFragment(): DiscountsFragment

    @ContributesAndroidInjector
    fun contributeGainsFragment(): GainsFragment

    @ContributesAndroidInjector
    fun contributeDebitedBalancesFragment(): DebitedBalancesFragment

    @ContributesAndroidInjector
    fun contributeReceivedBalancesFragment(): ReceivedBalancesFragment

    @ContributesAndroidInjector
    fun contributeBlockFragment(): BlockFragment

    @ContributesAndroidInjector
    fun contributeRatingFragment(): RatingFragment

    @ContributesAndroidInjector
    fun contributeHelpFragment(): HelpFragment

    @ContributesAndroidInjector
    fun contributeCancelOrderFragment(): TotalCostFragment

    @ContributesAndroidInjector
    fun contributeRefuseOrderFragment(): RefuseOrderFragment

    @ContributesAndroidInjector
    fun contributeNotPaidFragment(): NotPaidFragment

    @ContributesAndroidInjector
    fun contributeExecuteOrderFragment(): ExecuteOrderFragment

    @ContributesAndroidInjector
    fun contributeNewOrderFragment(): NewOrderFragment

    @ContributesAndroidInjector
    fun contributeChangePhoneVerificationFragment(): ChangePhoneVerificationFragment

    @ContributesAndroidInjector
    fun contributeDisabledUserFragment(): DisabledUserFragment

    @ContributesAndroidInjector
    fun contributeSelectWayPointFragment(): SelectWayPointFragment

    @ContributesAndroidInjector
    fun contributeSimpleRegistrationFragment(): SimpleRegistrationFragment

    @ContributesAndroidInjector
    fun contributeImagePickerFragment(): ImagePickerFragment

    @ContributesAndroidInjector
    fun contribuetBonusProgramFragment(): BonusProgramFragment

    @ContributesAndroidInjector
    fun contribueteEtherFragment(): EtherFragment

    @ContributesAndroidInjector
    fun contribueteAcceptOrderFragment(): AcceptOrderFragment

    @ContributesAndroidInjector
    fun contribueteUserInfoFragment(): UserInfoFragment

    @ContributesAndroidInjector
    fun contribueteFilterFragment(): FilterFragment

    @ContributesAndroidInjector
    fun contribueteCreateFilterFragment(): CreateFilterFragment

    @ContributesAndroidInjector
    fun contribuetePickLocationFragment(): PickLocationFragment

    @ContributesAndroidInjector
    fun contribueteEditFilterFragment(): EditFilterFragment

    @ContributesAndroidInjector
    fun contribuetePickLocationMapFragment(): PickLocationMapFragment

    @ContributesAndroidInjector
    fun contribueteSettingSignalFragment(): SettingSignalFragment

}
