package com.wezom.taxi.driver.presentation.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.transition.TransitionInflater
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatDelegate
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.bus.events.ResultDataEvent
import com.wezom.taxi.driver.common.*
import com.wezom.taxi.driver.common.locale.LocaleContextWrapper
import com.wezom.taxi.driver.data.db.DbManager
import com.wezom.taxi.driver.data.models.*
import com.wezom.taxi.driver.ext.THEME_MODE
import com.wezom.taxi.driver.injection.ViewModelFactory
import com.wezom.taxi.driver.net.api.ApiManager
import com.wezom.taxi.driver.net.jobqueue.JobFactory
import com.wezom.taxi.driver.net.response.FilterResponse
import com.wezom.taxi.driver.presentation.acceptorder.AcceptOrderFragment
import com.wezom.taxi.driver.presentation.addcard.AddCardFragment
import com.wezom.taxi.driver.presentation.balance.BalanceFragment
import com.wezom.taxi.driver.presentation.balance.HowToReplenishFragment
import com.wezom.taxi.driver.presentation.block.BlockFragment
import com.wezom.taxi.driver.presentation.bonus_program.BonusProgramFragment
import com.wezom.taxi.driver.presentation.changephone.ChangePhoneFragment
import com.wezom.taxi.driver.presentation.changephoneverification.ChangePhoneVerificationFragment
import com.wezom.taxi.driver.presentation.completedorder.CompletedOrderFragment
import com.wezom.taxi.driver.presentation.disabled.DisabledUserFragment
import com.wezom.taxi.driver.presentation.discounts.DiscountsFragment
import com.wezom.taxi.driver.presentation.ether.EtherFragment
import com.wezom.taxi.driver.presentation.executeorder.ExecuteOrderFragment
import com.wezom.taxi.driver.presentation.filter.CreateFilterFragment
import com.wezom.taxi.driver.presentation.filter.EditFilterFragment
import com.wezom.taxi.driver.presentation.filter.FilterFragment
import com.wezom.taxi.driver.presentation.gains.GainsFragment
import com.wezom.taxi.driver.presentation.help.HelpFragment
import com.wezom.taxi.driver.presentation.history.OrderHistoryListFragment
import com.wezom.taxi.driver.presentation.imagepicker.ImagePickerFragment
import com.wezom.taxi.driver.presentation.mainmap.MainMapFragment
import com.wezom.taxi.driver.presentation.neworder.NewOrderFragment
import com.wezom.taxi.driver.presentation.notpaid.NotPaidFragment
import com.wezom.taxi.driver.presentation.orderfound.OrderFoundFragment
import com.wezom.taxi.driver.presentation.photoviewer.PhotoViewerFragment
import com.wezom.taxi.driver.presentation.photoviewer.PhotoViewerFragment.Companion.CropOptions
import com.wezom.taxi.driver.presentation.picklocation.PickLocationFragment
import com.wezom.taxi.driver.presentation.picklocation.PickLocationMapFragment
import com.wezom.taxi.driver.presentation.privatestatistics.PrivateStatisticsFragment
import com.wezom.taxi.driver.presentation.profile.ProfileFragment
import com.wezom.taxi.driver.presentation.rating.RatingFragment
import com.wezom.taxi.driver.presentation.refuseorder.RefuseOrderFragment
import com.wezom.taxi.driver.presentation.refuseorder.TotalCostFragment
import com.wezom.taxi.driver.presentation.registration.SimpleRegistrationFragment
import com.wezom.taxi.driver.presentation.selectwaypoint.SelectWayPointFragment
import com.wezom.taxi.driver.presentation.settings.SettingSignalFragment
import com.wezom.taxi.driver.presentation.settings.SettingsFragment
import com.wezom.taxi.driver.presentation.splash.SplashFragment
import com.wezom.taxi.driver.presentation.tutorial.TutorialFragment
import com.wezom.taxi.driver.presentation.userinfo.UserInfoFragment
import com.wezom.taxi.driver.presentation.verification.AuthFragment
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by zorin.a on 16.02.2018.
 */

abstract class BaseActivity : DaggerAppCompatActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    @Inject
    lateinit var navigatorHolder: NavigatorHolder
    @Inject
    lateinit var apiManager: ApiManager //for
    @Inject
    lateinit var dbManager: DbManager

    protected var disposable = CompositeDisposable()

    private val navigator: Navigator by lazy {
        object : SupportAppNavigator(this,
                R.id.container) {
            override fun createActivityIntent(context: Context?,
                                              screenKey: String?,
                                              data: Any?): Intent? = null

            override fun createFragment(screenKey: String?,
                                        data: Any?): Fragment? = when (screenKey) {
                SPLASH_SCREEN -> SplashFragment()
                MAIN_MAP_SCREEN -> MainMapFragment()
                TUTORIAL_SCREEN -> TutorialFragment()
                PROFILE_SCREEN -> ProfileFragment.newInstance(data as Int)
                ADD_CARD_SCREEN -> AddCardFragment()
                VERIFICATION_PHONE_SCREEN -> AuthFragment()
                PHOTO_VIEWER_SCREEN -> PhotoViewerFragment.newInstance(data as Triple<String, CropOptions, Int>)
                ORDER_HISTORY_LIST_SCREEN -> OrderHistoryListFragment()
                ORDER_FOUND_SCREEN -> OrderFoundFragment.newInstance(data as NewOrderInfo)
                CHANGE_PHONE_SCREEN -> ChangePhoneFragment.newInstance(data as String)
                SETTINGS_SCREEN -> SettingsFragment()
                PRIVATE_STATISTICS_SCREEN -> PrivateStatisticsFragment()
                COMPLETED_ORDER_SCREEN -> CompletedOrderFragment.newInstance(data as Int)
                BALANCE_SCREEN -> BalanceFragment()
                HOW_TO_REPLENISH_SCREEN -> HowToReplenishFragment()
                BONUS_SCREEN -> DiscountsFragment()
                GAINS_SCREEN -> GainsFragment()
                BLOCK_SCREEN -> BlockFragment()
                RATING_SCREEN -> RatingFragment.newInstance(data as Pair<Int, Any>)
                HELP_SCREEN -> HelpFragment()
                TOTAL_COST_SCREEN -> TotalCostFragment.newInstance(data as ResultDataEvent)
                REFUSE_ORDER_SCREEN -> RefuseOrderFragment.newInstance(data as Triple<Order, HashMap<OrderStatus, List<Reason>>, ResultDataEvent>)
                NOT_PAID_SCREEN -> NotPaidFragment.newInstance(data as Int)
                EXECUTE_ORDER_SCREEN -> ExecuteOrderFragment.newInstance(data as NewOrderInfo)
                NEW_ORDER_SCREEN -> NewOrderFragment()
                CHANGE_PHONE_VERIFICATION_SCREEN -> ChangePhoneVerificationFragment.newInstance(data as String)
                ACCOUNT_DISABLED_SCREEN -> DisabledUserFragment()
                SELECT_WAY_POINT_SCREEN -> SelectWayPointFragment.newInstance(data as Order)
                SIMPLE_REGISTRATION_SCREEN -> SimpleRegistrationFragment.newInstance()
                IMAGE_PICKER_SCREEN -> ImagePickerFragment.newInstance(data as Pair<Int, String?>)
                BONUS_PROGRAM_SCREEN -> BonusProgramFragment.newInstance()
                ETHER_SCREEN -> EtherFragment.newInstance()
                ACCEPT_ORDER_SCREEN -> AcceptOrderFragment.newInstance(data as NewOrderInfo)
                USER_INFO_SCREEN -> {
                    val fragment = UserInfoFragment.newInstance(data as User)

                    fragment.sharedElementEnterTransition =
                            TransitionInflater.from(this@BaseActivity)
                                    .inflateTransition(android.R.transition.move)
                    fragment.sharedElementReturnTransition =
                            TransitionInflater.from(this@BaseActivity)
                                    .inflateTransition(android.R.transition.move)
                    fragment
                }
                FILTER_SCREEN -> FilterFragment.newInstance()
                EDIT_FILTER_SCREEN -> EditFilterFragment.newInstance(data as FilterResponse)
                CREATE_FILTER_SCREEN -> CreateFilterFragment.newInstance()
                PICK_LOCATION_SCREEN -> PickLocationFragment.newInstance()
                PICK_LOCATION_MAP_SCREEN -> PickLocationMapFragment.newInstance()
                SETTING_SIGNAL_FRAGMENT -> SettingSignalFragment.newInstance()
                else -> throw Throwable("Unknown screen")
            }

            override fun setupFragmentTransactionAnimation(command: Command,
                                                           currentFragment: Fragment?,
                                                           nextFragment: Fragment?,
                                                           fragmentTransaction: FragmentTransaction?) {
                if (currentFragment is AcceptOrderFragment && nextFragment is UserInfoFragment) {
                    fragmentTransaction?.addSharedElement(currentFragment.binding.bottomSheetLayout?.userImage,
                            "sharedImage")
                }
                fragmentTransaction?.setCustomAnimations(R.anim.fade_in,
                        R.anim.fade_out,
                        R.anim.fade_in,
                        R.anim.fade_out)

                super.setupFragmentTransactionAnimation(command,
                        currentFragment,
                        nextFragment,
                        fragmentTransaction)
            }
        }
    }

    override fun attachBaseContext(newBase: Context?) {
        val themeMode = PreferenceManager.getDefaultSharedPreferences(newBase)
                .getInt(THEME_MODE,
                        AppCompatDelegate.MODE_NIGHT_YES)
        setThemeMode(themeMode)
        super.attachBaseContext(LocaleContextWrapper.wrap(newBase!!))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        JobFactory.instance.initJobManager(apiManager,
                this.applicationContext,
                dbManager)
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.dispose()
    }

    private fun setThemeMode(themeMode: Int) {
        AppCompatDelegate.setDefaultNightMode(themeMode)
    }

    fun lockDrawer(isLock: Boolean) {
        Timber.d("isLock: $isLock")
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        drawer?.setDrawerLockMode(if (isLock) DrawerLayout.LOCK_MODE_LOCKED_CLOSED
        else DrawerLayout.LOCK_MODE_UNLOCKED)
    }
}