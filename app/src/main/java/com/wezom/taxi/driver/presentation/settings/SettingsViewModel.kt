package com.wezom.taxi.driver.presentation.settings

import android.arch.lifecycle.MutableLiveData
import android.content.SharedPreferences
import com.wezom.taxi.driver.common.*
import com.wezom.taxi.driver.ext.*
import com.wezom.taxi.driver.net.jobqueue.JobFactory
import com.wezom.taxi.driver.net.jobqueue.jobs.LogoutJob
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.presentation.customview.DriverToolbar
import com.wezom.taxi.driver.presentation.profile.ProfileFragment
import com.wezom.taxi.driver.service.LocationService
import com.wezom.taxi.driver.service.SocketService
import timber.log.Timber
import java.lang.ref.WeakReference
import javax.inject.Inject

/**
 * Created by udovik.s on 13.03.2018.
 */
class SettingsViewModel @Inject constructor(screenRouterManager: ScreenRouterManager,
                                            sharedPreferences: SharedPreferences) : BaseViewModel(screenRouterManager) {

    var prefsThemeMode: Int by sharedPreferences.int(THEME_MODE)
    var navigatorType: Int by sharedPreferences.int(NAVIGATOR)
    private var userImageForPrefs: String by sharedPreferences.string(USER_IMAGE_FOR_PREFS)

    var vibro: Boolean by sharedPreferences.boolean(VIBRO_ENABLED, true)
    var sound: Boolean by sharedPreferences.boolean(SOUND_ENABLED, true)
    //    var overlayOrdersSystem: Boolean by sharedPreferences.boolean(OVERLAY_ORDER_SYSTEM, true)
    var overlayOrdersInner: Boolean by sharedPreferences.boolean(OVERLAY_ORDER_INNER, true)

    var themeLiveData = MutableLiveData<Int>()
    var userImageLiveData = MutableLiveData<String>()
    var navigatorLiveData = MutableLiveData<Int>()
    var soundLiveData = MutableLiveData<Boolean>()
    var vibroLiveData = MutableLiveData<Boolean>()
    var overlayLiveData = MutableLiveData<Boolean>()
    var lockDrawerLiveData = MutableLiveData<Boolean>()
    var enableSystemOverlayLiveData = MutableLiveData<Boolean>()

    private var token: String by sharedPreferences.string(TOKEN)
    private var userImageString by sharedPreferences.string(USER_IMAGE_URI)
    private var carImageString by sharedPreferences.string(CAR_IMAGE_URI)
    private var userNameCached by sharedPreferences.string(USER_NAME_CACHED)
    private var userRatingCached by sharedPreferences.string(USER_RATING_CACHED)
    private var userPhone by sharedPreferences.string(USER_PHONE)
    private var firebaseToken by sharedPreferences.string(FIREBASE_TOKEN_KEY)

    private var doc1 by sharedPreferences.string(FIRST_DOC_URI)
    private var doc2 by sharedPreferences.string(SECOND_DOC_URI)
    private var doc3 by sharedPreferences.string(THIRD_DOC_URI)
    private var doc4 by sharedPreferences.string(FOURTH_DOC_URI)

    var background: Boolean by sharedPreferences.boolean(BACKGROUND, false)

    private var isCardAdded: Boolean by sharedPreferences.boolean(IS_CARD_ADDED)
    private var isNeedComplete: Boolean by sharedPreferences.boolean(IS_NEED_COMPLETE)

    fun logout() {
        val tokenCopy = "Bearer $token"
        Timber.d("LOGOUT logOut $tokenCopy")
        JobFactory.instance.getJobManager()?.addJobInBackground(LogoutJob(tokenCopy))
        userImageForPrefs = ""
        userImageString = ""
        carImageString = ""
        userNameCached = ""
        userRatingCached = ""
        userPhone = ""
        DriverToolbar.IS_ONLINE_STATE = false
        isCardAdded = false
        doc1 = ""
        doc2 = ""
        doc3 = ""
        doc4 = ""
        lockDrawerLiveData.postValue(true)
        setRootScreen(VERIFICATION_PHONE_SCREEN)
        token = ""
        firebaseToken = ""
        LocationService.stop(context = WeakReference(context))
        SocketService.stop(context = WeakReference(context))
        isNeedComplete = false

    }


    fun settingSignal() {
        switchScreen(SETTING_SIGNAL_FRAGMENT)
    }

    fun onProfileClick() {
        setRootScreen(PROFILE_SCREEN, ProfileFragment.MODE_PROFILE)
    }

    fun setVibroEnabled(enabled: Boolean) {
        vibro = enabled
    }

    fun getSound() {
        soundLiveData.postValue(sound)
    }

    fun getVibro() {
        vibroLiveData.postValue(vibro)
    }

    fun setNewTheme(themeMode: Int) {
        prefsThemeMode = themeMode
        themeLiveData.postValue(themeMode)
    }

    fun getTheme() {
        themeLiveData.postValue(prefsThemeMode)
    }

    fun getUserImage() {
        userImageLiveData.postValue(userImageForPrefs)
    }

    fun setNavigator(navigator: Int) {
        navigatorType = navigator
        navigatorLiveData.postValue(navigator)
    }

    fun getNavigator() {
        navigatorLiveData.postValue(navigatorType)
    }

    fun getOrderOverlayState() {
        val overlayState = checkIsSystemOverlayEnabled(context)
        if (!overlayState)
            overlayOrdersInner = false
        overlayLiveData.postValue(overlayOrdersInner)
        Timber.d("getOrderOverlayState overlayOrdersInner: $overlayOrdersInner")
    }

    fun setOverlayEnabled(enabled: Boolean) {
        if (enabled) {
            val systemOverlayEnabled = checkIsSystemOverlayEnabled(context)
            overlayOrdersInner = enabled
            if(!systemOverlayEnabled){
                enableSystemOverlayLiveData.postValue(true)
            }
        }else{
            overlayOrdersInner = false
        }

        Timber.d("setOverlayEnabled: $enabled")
        Timber.d("checkIsSystemOverlayEnabled: ${checkIsSystemOverlayEnabled(context)}")
        Timber.d("overlayOrdersInner: $overlayOrdersInner")
    }

}