package com.wezom.taxi.driver.taxometer

/**
 * Created by zorin.a on 08.05.2018.
 */
enum class Action { START, START_DELAYED, START_WAIT, STOP, RESUME, PAUSE, RESTORE, ACCEPT, CALC_DIST }