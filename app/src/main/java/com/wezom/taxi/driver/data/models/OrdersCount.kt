package com.wezom.taxi.driver.data.models

import com.google.gson.annotations.SerializedName

/**
 *Created by Zorin.A on 21.June.2019.
 */
class OrdersCount constructor(@SerializedName("count")
                              val count: Int,
                              @SerializedName("filteredCount")
                              val filteredCount: Int)