package com.wezom.taxi.driver.presentation.addcard

import android.content.Context
import android.support.v7.widget.Toolbar
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.ProgressBar
import com.wezom.taxi.driver.databinding.ProgressToolbarBinding

class ProgressToolbar : FrameLayout {

    private val binding: ProgressToolbarBinding by lazy {
        ProgressToolbarBinding.inflate(LayoutInflater.from(context), this, true)
    }

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
            context,
            attrs,
            defStyleAttr
    )

    val toolbar: Toolbar
        get() = binding.toolbar

    val progress: ProgressBar
        get() = binding.progress


}