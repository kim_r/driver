package com.wezom.taxi.driver.net.websocket.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class CancelOrder (@SerializedName("orderId") var orderId: Int) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(orderId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CancelOrder> {
        override fun createFromParcel(parcel: Parcel): CancelOrder {
            return CancelOrder(parcel)
        }

        override fun newArray(size: Int): Array<CancelOrder?> {
            return arrayOfNulls(size)
        }
    }
}