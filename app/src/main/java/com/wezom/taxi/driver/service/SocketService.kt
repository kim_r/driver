package com.wezom.taxi.driver.service

import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.location.Location
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.bus.events.*
import com.wezom.taxi.driver.common.BeepUtil
import com.wezom.taxi.driver.common.OrderManager
import com.wezom.taxi.driver.common.checkIsSystemOverlayEnabled
import com.wezom.taxi.driver.data.db.DbManager
import com.wezom.taxi.driver.data.models.CloseOrderAuto
import com.wezom.taxi.driver.data.models.NewOrderInfo
import com.wezom.taxi.driver.data.models.OrderStatus
import com.wezom.taxi.driver.data.models.map.Hexagon
import com.wezom.taxi.driver.ext.OVERLAY_ORDER_INNER
import com.wezom.taxi.driver.ext.roundTwoDecimal
import com.wezom.taxi.driver.ext.shortToast
import com.wezom.taxi.driver.net.api.ApiService
import com.wezom.taxi.driver.net.jobqueue.JobFactory
import com.wezom.taxi.driver.net.jobqueue.jobs.DataSynchronizationJob
import com.wezom.taxi.driver.net.request.TotalCostRequest
import com.wezom.taxi.driver.net.response.BaseResponse
import com.wezom.taxi.driver.net.websocket.SocketListener
import com.wezom.taxi.driver.net.websocket.channel.WebSocketChannel
import com.wezom.taxi.driver.presentation.customview.DriverToolbar.Companion.IS_ONLINE_STATE
import com.wezom.taxi.driver.presentation.customview.EtherSwitchButton
import com.wezom.taxi.driver.presentation.main.MainActivity
import com.wezom.taxi.driver.presentation.main.events.*
import com.wezom.taxi.driver.presentation.main.events.SocketServiceResponseEvent.Result.NEGATIVE
import com.wezom.taxi.driver.presentation.main.events.SocketServiceResponseEvent.Result.POSITIVE
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber
import java.lang.ref.WeakReference
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by zorin.a on 20.04.2018.
 */
class
SocketService : LocationService() {
    @Inject
    lateinit var socketChannel: WebSocketChannel
    @Inject
    lateinit var orderManager: OrderManager
    @Inject
    lateinit var dbManager: DbManager
    @Inject
    lateinit var apiService: ApiService
    @Inject
    lateinit var sharedPreferences: SharedPreferences

    private var handler: Handler? = Handler()
    private var stateConnect: Boolean = false
    private var preStateConnect: Boolean = false


    private val socketListener = object : SocketListener {

        override fun onCloseOrderAutomatically(order: CloseOrderAuto) {
            Timber.d("SOCKET onCloseOrderAutomatically ${order.orderId}")
            cancelOrderByCustomer(order.orderId!!,
                    locationStorage.getCachedLocation())
            RxBus.publish(CloseOrderAuto(order))
        }

        override fun onCancelOrder(orderId: Int) {
            Timber.d("SOCKET onCancelOrder $orderId")
            val loc = locationStorage.getCachedLocation()

            cancelOrderByCustomer(orderId,
                    loc)
            RxBus.publish(CancelOrderEvent(orderId))
        }

        override fun onDisableOnlineStatus(online: Boolean) {
            IS_ONLINE_STATE = online
            Timber.d("TOOLBAR SOCKET IS ONLINE onDisableOnlineStatus(): $IS_ONLINE_STATE")
            RxBus.publish(DisableOnlineStatusEvent(IS_ONLINE_STATE))
        }

        override fun onHexagonReceived(hexagon: Hexagon) {
            Timber.d("SOCKET onHexagonReceived()")
            RxBus.publish(HexagonEvent(hexagon))
        }

        override fun onNewOrder(orderInfo: NewOrderInfo) {
            Timber.d("SOCKET onNewOrder() $orderInfo")
            if (MainActivity.isForeground) {
                RxBus.publish(NewOrderInfoEvent(orderInfo))
            } else {
                if (sharedPreferences.getBoolean(OVERLAY_ORDER_INNER,
                                true) && checkIsSystemOverlayEnabled(context)) {
                    Timber.d("OVERLAY_SERVICE OverlayWindowService.startService")
                    OverlayWindowService.startService(WeakReference(context),
                            orderInfo)
                }
            }
        }

        override fun onConnected() {
            Timber.d("SOCKET STATUS onConnected()")
            stateConnect = true
            Handler(Looper.getMainLooper()).post {
                RxBus.publish(BadInternetEvent(true))
            }
            if (handler != null)
                handler!!.removeCallbacks(runnable)
            socketChannel.login()
        }


        @SuppressLint("CheckResult")
        override fun onLoginDone() {
            Timber.d("SOCKET onLoginDone()")
            Completable.fromCallable {
                IS_ONLINE_STATE = preStateConnect
                //      isSocketAlive = true
//                startLocationUpdates()
                RxBus.publish(SocketServiceResponseEvent(if (preStateConnect) POSITIVE else NEGATIVE))
            }
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .subscribe({},
                            Timber::e)
            socketChannel.getTariffiedRegions()

        }

        @SuppressLint("CheckResult")
        override fun onDisconnected() {
            Timber.d("SOCKET STATUS onDisconnected()")
            stateConnect = false
            Completable.fromCallable {
                RxBus.publish(BadInternetEvent(false))
                preStateConnect = IS_ONLINE_STATE
                unPingOnline()
                handler!!.postDelayed(runnable, 30000)
            }
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {},
                            Timber::e)
        }

        @SuppressLint("CheckResult")
        override fun onError(response: BaseResponse?) {
            Timber.d("SOCKET onError()")
            Completable.fromCallable {
                //                IS_ONLINE_STATE = false
//                stopLocationUpdates()
                response?.error?.message?.shortToast(applicationContext)
                unPingOnline()
                socketChannel.disconnect()
                // RxBus.publish(SocketServiceResponseEvent(NEGATIVE))
            }
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .subscribe({},
                            Timber::e)
        }

        @SuppressLint("CheckResult")
        override fun onConnectError() {
            Timber.d("SOCKET onConnectError")
            Completable.fromCallable {
                //                IS_ONLINE_STATE = false
                unPingOnline()
                //RxBus.publish(SocketServiceResponseEvent(NEGATIVE))
            }
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .subscribe({},
                            Timber::e)
        }

        override fun onCoordinatesSubmitted() {
            Timber.d("SOCKET SocketService onCoordinatesSubmitted")
        }

        override fun onNewBroadcast(order: NewOrderInfo) {
            Timber.d("SOCKET SocketService onNewBroadcast")
            if (order.filter) {
                BeepUtil.playNewOrderFilter()
            } else {
                BeepUtil.playNewOrder()
            }
            RxBus.publish(NewBroadcastEvent(order))
            EtherSwitchButton.cache += 1
        }

        override fun onDeleteBroadcast(orderId: Int) {
            Timber.d("SOCKET SocketService onDeleteBroadcast: id:$orderId")
            RxBus.publish(DeleteBroadcastEvent(orderId))
            if (EtherSwitchButton.cache > 0) EtherSwitchButton.cache -= 1
        }

        @SuppressLint("CheckResult")
        override fun onBroadcastOrdersCount(count: Int) {
            EtherSwitchButton.cache = count
            Completable.fromCallable {
                RxBus.publish(BroadcastCountEvent(count))
            }
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .subscribe({},
                            Timber::e)
        }
    }

    private var runnable = Runnable {
        if (!socketChannel.isConnected()!!) {
            if (!stateConnect) {
                IS_ONLINE_STATE = false
                RxBus.publish(SocketServiceResponseEvent(NEGATIVE,
                        playSound = true))
            }
        }
    }

    fun cancelOrderByCustomer(orderId: Int,
                              lastLocation: Location?) {
        Timber.d("cancelOrderByCustomer() id = $orderId")
        RxBus.publish(ShowSecondaryOrderButtonEvent(false))
        val resultData: ResultDataEvent?
        if (orderManager.primaryOrderId() == orderId) { //its primary
            val orderInfo = orderManager.getPrimaryOrder()

            if (orderInfo?.order?.status == OrderStatus.ON_THE_WAY) {
                resultData =
                        dbManager.gueryOrderResult(orderId)?.resultData ?: TaxometerService.result
                Timber.d("cancelOrderByCustomer: primary order")
                val request =
                        TotalCostRequest(factCost = resultData?.factCost
                                ?: TaxometerService.result?.factCost ?: 0,
                                actualCost = resultData?.resultCost
                                        ?: TaxometerService.result?.resultCost ?: 0,
                                factTime = resultData?.factTime ?: TaxometerService.result?.factTime
                                ?: 0,
                                factPath = resultData?.factPath?.roundTwoDecimal()
                                        ?: TaxometerService.result?.factPath ?: 0.0,
                                longitude = lastLocation?.longitude,
                                latitude = lastLocation?.latitude,
                                route = resultData?.passedRoute
                                        ?: TaxometerService.result?.passedRoute,
                                eventTime = System.currentTimeMillis(),
                                changedParams = null,
                                waitingCost = resultData?.waitingCost,
                                waitingTime = resultData?.waitingTime)

                val jm = JobFactory.instance.getJobManager()
                if (jm != null) {
                    jm.addJobInBackground(DataSynchronizationJob(orderId,
                            request))
                } else {
                    apiService.dataSynchronization(orderId,
                            request)
                            .subscribe({},
                                    {})
                }
                if (TaxometerService.isAlive) {
                    TaxometerService.stop(WeakReference(context))
                }
            } else {
                val request = TotalCostRequest(factCost = 0,
                        actualCost = 0,
                        factTime = 0,
                        factPath = 0.0,
                        longitude = lastLocation?.longitude,
                        latitude = lastLocation?.latitude,
                        route = null,
                        eventTime = System.currentTimeMillis(),
                        changedParams = null,
                        waitingCost = 0.0,
                        waitingTime = 0
                )
                val jm = JobFactory.instance.getJobManager()
                if (jm != null) {
                    jm.addJobInBackground(DataSynchronizationJob(orderId,
                            request))
                } else {
                    apiService.dataSynchronization(orderId,
                            request)
                            .subscribe({},
                                    {})
                }
            }

        } else if (orderId == orderManager.secondaryOrderId()) {// its secondary
            Timber.d("cancelOrderByCustomer: secondary order")
            orderManager.deleteSecondaryOrder()
            val request = TotalCostRequest(factCost = 0,
                    actualCost = 0,
                    factTime = 0,
                    factPath = 0.0,
                    longitude = lastLocation?.longitude,
                    latitude = lastLocation?.latitude,
                    route = null,
                    eventTime = System.currentTimeMillis(),
                    changedParams = null,
                    waitingCost = 0.0,
                    waitingTime = 0)
            val jm = JobFactory.instance.getJobManager()
            if (jm != null) {
                jm.addJobInBackground(DataSynchronizationJob(orderId,
                        request))
            } else {
                apiService.dataSynchronization(orderId,
                        request)
                        .subscribe({},
                                {})
            }
        } else {
            Timber.e("cancelOrderByCustomer: unknown order: $orderId")
        }
    }

    override fun onCreate() {
        super.onCreate()
        Timber.d("SOCKET SocketService onCreate")
        //socketChannel.sendOffline()
        pingOnline()
        initListeners()
        broadcastDriverCoordinates()
        broadcastDriverCoordinatesOnTime()
    }

    override fun onRebind(intent: Intent?) {
        super.onRebind(intent)
        pingOnline()
    }

    private fun pingOnline() {
        if (disposableOnline.size() < 1)
            disposableOnline += Observable.interval(20,
                    TimeUnit.SECONDS)
                    .subscribe {
                        Timber.d("SOCKET SocketService Ping")
                        socketChannel.sendPing()
                    }

    }

    protected var disposableOnline = CompositeDisposable()

    private fun unPingOnline() {
//        disposableOnline.dispose()
    }

    override fun onStartCommand(intent: Intent?,
                                flags: Int,
                                startId: Int): Int {
        Timber.d("SOCKET SocketService onStartCommand")
        if (socketChannel.isConnected() == false) {
            socketChannel.connect()
        }

        return Service.START_REDELIVER_INTENT
    }

    override fun onBind(intent: Intent?): IBinder? = null

    override fun onDestroy() {
        IS_ONLINE_STATE = true
        isSocketAlive = false
//        stopLocationUpdates()
        Timber.d("SOCKET onDestroy")
        socketChannel.disconnect()
        disposable.dispose()
        disposableOnline.dispose()
        super.onDestroy()
    }

    private fun broadcastDriverCoordinatesOnTime() {
        disposable += Observable.interval(5,
                30,
                TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    sedCoordinat()
                }
    }

    private fun sedCoordinat() {
        Timber.d("SOCKET_SERVICE broadcastDriverCoordinates $location")
        if (location != null && (location!!.latitude != 0.0 && location!!.longitude != 0.0)) {
            socketChannel.submitCoordinates(location)
        } else if((locationStorage.getCachedLocation().latitude != 0.0 && locationStorage.getCachedLocation().longitude != 0.0)) {
            location = locationStorage.getCachedLocation()
            socketChannel.submitCoordinates(location)
        }

    }

    private fun broadcastDriverCoordinates() {
        disposable += RxBus.listen(LocationEvent::class.java)
                .subscribe({
                    socketChannel.submitCoordinates(it.location)
                },
                        Timber::e)

//        val coordinateUpdateTime = context.defaultSharedPreferences.getLong(COORDINATES_UPDATE_TIME,
//                                                                            10000)
//        disposable += Observable.interval(0,
//                                          coordinateUpdateTime,
//                                          TimeUnit.MILLISECONDS)
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe {
//                if (location != null) {
////                        if (location!!.isFromMockProvider) {
////                            RxBus.publish(DisableOnlineStatusEvent(false))
////                            getString(R.string.fake_gps_found).longToast(this)
////                        }
//                    Timber.d("SOCKET_SERVICE broadcastDriverCoordinates $location")
//                    socketChannel.submitCoordinates(location)
//                } else {
//                    location = locationStorage.getCachedLocation()
//                }
//            }
    }

    private fun initListeners() {
        socketChannel.listener = socketListener
        disposable += RxBus.listen(QuerrryTariffiedRegions::class.java)
                .subscribe({
                    socketChannel.getTariffiedRegions()
                },
                        Timber::e)

        disposable += RxBus.listen(BroadcastPing::class.java)
                .subscribe {
                    if (it.boolean) {
                        pingOnline()
                    } else {
                        unPingOnline()
                    }
                }
    }

    companion object {
        var isSocketAlive = false
        const val NOTIFICATION_ID = 100
        const val CHANNEL_ID = "Socket channel"
        fun stop(context: WeakReference<Context>) {
            Timber.d("SOCKET_SERVICE stop")
            val intent = Intent(context.get(),
                    SocketService::class.java)
            context.get()
                    ?.stopService(intent)
        }

        fun start(context: WeakReference<Context>) {
            Timber.d("SOCKET_SERVICE start")
            val intent = Intent(context.get(),
                    SocketService::class.java)
            context.get()
                    ?.startService(intent)
        }
    }
}