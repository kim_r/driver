package com.wezom.taxi.driver.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by zorin.a on 3/11/19.
 */
data class ActionCondition constructor(
        @SerializedName("description_value")
        val descriptionValue: String?,
        @SerializedName("is_passed")
        val isPassed: Boolean?,
        @SerializedName("percent_of_completeon")
        val percentOfCompleteon: String?
)