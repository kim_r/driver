package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.Shares

/**
 * Created by udovik.s on 19.03.2018.
 */
data class DiscountsResponse(
        @SerializedName("promoCodeValue") val  promoCodeValue: String?,
        @SerializedName("url") val  url: String?,
        @SerializedName("shares") val  shares: List<Shares>?,
        @SerializedName("status") val  status: Boolean?
) : BaseResponse()