package com.wezom.taxi.driver.presentation.customview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.wezom.taxi.driver.databinding.ConditionItemBinding

/**
 * Created by zorin.a on 20.12.2018.
 */
class ConditionItemView : LinearLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, attributeSetId: Int) : super(context, attrs, attributeSetId)

    val binding: ConditionItemBinding = ConditionItemBinding.inflate(LayoutInflater.from(context), this, true)

    fun setText(text: String) {
        binding.reqDescription.text = text
    }
}