package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.Car
import com.wezom.taxi.driver.data.models.Document
import com.wezom.taxi.driver.data.models.User
import com.wezom.taxi.driver.data.models.UserRequisitesCorrectness

/**
 * Created by zorin.a on 021 21.02.18.
 */
data class ExistedProfileResponse(@SerializedName("user") val user: User,
                                  @SerializedName("car") val car: Car,
                                  @SerializedName("userCorrectness") val userCorrectness: UserRequisitesCorrectness,
                                  @SerializedName("carCorrectness") val carCorrectness: UserRequisitesCorrectness,
                                  @SerializedName("documents") val documents: List<Document>) : BaseResponse()