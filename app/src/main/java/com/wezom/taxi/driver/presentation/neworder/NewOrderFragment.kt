package com.wezom.taxi.driver.presentation.neworder

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.common.Constants
import com.wezom.taxi.driver.common.formatAddress
import com.wezom.taxi.driver.common.isPermissionGranted
import com.wezom.taxi.driver.common.loadRoundedImage
import com.wezom.taxi.driver.data.models.*
import com.wezom.taxi.driver.databinding.NewOrderBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.ext.setVisible
import com.wezom.taxi.driver.presentation.base.BaseMapFragment
import com.wezom.taxi.driver.presentation.executeorder.AddressItem
import com.wezom.taxi.driver.presentation.executeorder.ExecuteOrderFragment
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class NewOrderFragment : BaseMapFragment() {
    private lateinit var viewModel: NewOrderViewModel
    private lateinit var binding: NewOrderBinding

    private var navigatorType = Constants.DEFAULT_NAV
    private var newOrderInfo: NewOrderInfo? = null

    private val addressItemParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT)

    private val orderAddressCoordinates = mutableListOf<LatLng>()

    public fun sePlanAccept(second: Long) {
        if (second < 0)
            setTime(second * -1)
        else
            setTime(second)
        // if (minut >= orderInfo.formula?.sw?.fw!! || house > 0) {
        binding.etaName.setText(R.string.to_order)
        if (second < 0) {
            binding.etaName.setText(R.string.late)
        }
        //binding.plain.text = "$house:$minut:$sec"
    }

    public fun setTime(second: Long) {
        binding.eta.setText("")
        val house = second / 3600
        val minut = (second - 3600 * house) / 60
        val sec = second % 60
//        if (house == 0L) {
//            binding.plain.append("00:")
//        } else if (house <= 9) {
//            binding.plain.append("0$house:")
//        } else {
//            binding.plain.append("$house:")
//        }

        if (minut == 0L) {
            binding.eta.append("00:")
        } else if (minut <= 9) {
            binding.eta.append("0$minut:")
        } else {
            binding.eta.append("$minut:")
        }


        if (sec == 0L) {
            binding.eta.append("00")
        } else if (sec <= 9) {
            binding.eta.append("0$sec")
        } else {
            binding.eta.append("$sec")
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders
                .of(this, viewModelFactory)
                .getViewModelOfType()

        setBackPressFun {
            viewModel.onBackPressed()
        }
        viewModel.getRefuseReasons()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = NewOrderBinding.inflate(inflater, container, false).also {
            it.toolbar.setToolbarTitle("New")
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.isMapWhiteThemeLiveData.observe(this, Observer { isMapWhite ->
            setMapStyle(isMapWhite)
        })
        viewModel.navigatorTypeLiveData.observe(this, Observer<Int> { navigatorTypeResponse ->
            navigatorType = navigatorTypeResponse!!
        })

        viewModel.routeLiveData.observe(this, Observer { it ->
            drawRoute(it)
        })

        viewModel.getNavigator()
        viewModel.getSecondaryOrder()

        viewModel.currentOrderLiveData.observe(this, Observer { it ->
            newOrderInfo = it
            newOrderInfo!!.order!!.coordinates?.let {
                it.forEach {
                    orderAddressCoordinates.add(LatLng(it.latitude!!, it.longitude!!))
                }
                setData(newOrderInfo!!)
                startTimer()
            }
        })
    }

    private var second: Long = 0

    private fun startTimer(){
        if(newOrderInfo!!.order!!.carArrivalTime == 0L){
            newOrderInfo!!.order!!.carArrivalTime = newOrderInfo!!.order!!.currentTime!!
        }
        var currentTime = newOrderInfo!!.order!!.currentTime!!.toLong()
        if(newOrderInfo!!.order!!.preliminaryTime!!.toLong() != 0L) {
            currentTime = newOrderInfo!!.order!!.preliminaryTime!!.toLong() - currentTime
        }else{
            currentTime =newOrderInfo!!.order!!.carArrivalTime!!.toLong() - currentTime
        }
        second = currentTime / 1000

        disposable += Observable.interval(1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe({
                    second--
                    sePlanAccept(second)
                }, {
                    Timber.e(it)
                })
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        grantResults
                .filter { it == PackageManager.PERMISSION_DENIED }
                .forEach { return }

        when (requestCode) {
            ExecuteOrderFragment.CALL_PHONE_REQUEST -> {
                newOrderInfo?.order?.user!!.phone?.let {
                    dialUser(it)
                }
            }
        }
    }

    override fun onLocationAvailable(isAvailable: Boolean) {
        if (lastLocation != null && isAvailable)
            handleCamera()
    }

    //    override fun onLocationChanged(location: Location?) {
//        super.onLocationChanged(location)
//        if (lastLocation != null && isMapReady()) handleCamera()
//    }

    override fun onMapReady(map: GoogleMap?) {
        super.onMapReady(map)
        viewModel.setMapStyle()
        if (lastLocation != null) handleCamera()
    }

    private fun setData(orderInfo: NewOrderInfo) {
        val showing = orderInfo.order?.acceptedOrderShowing
        binding.run {
            map.setVisible(true)

            etaContainer.setVisible(true)
            orderInfo.order?.let {
                if (it.carArrival == 0) {
                    eta.text = getString(R.string.for_now)
                } else {
                    eta.text = it.preliminaryTime?.convertToMMss()
                }
            }

            val coef = orderInfo.order?.currentCoefficient!!
            if (showing?.isCurrentCoefficientShow == true) {
                coefficientContainer.setVisible(true)
                coefficient.text = coef.toString()
            }

            if (coef <= 1.0) {
                coefficientContainer.setVisible(false)
            }

            if (showing?.isServiceShow == true) {
                servicesContainer.setVisible(true)
                services.setActiveStatus(false)
                services.addServices(*orderInfo.order?.services?.toIntArray() ?: intArrayOf())
            }
            if (orderInfo.order?.services == null || orderInfo.order?.services!!.isEmpty()) {
                servicesContainer.setVisible(false)
            }

            if (showing?.isCommentShow == true) {
                commentContainer.setVisible(true)
                comment.text = orderInfo.order?.comment
            }
            if (orderInfo.order?.comment.isNullOrBlank()) {
                commentContainer.setVisible(false)
            }

            if (showing?.isSurchargeShow == true) {
                surchargeContainer.setVisible(true)

            }
            val estBonus: Double = orderInfo.formula?.estimatedBonuses ?: 0.0
            val surchargeValue = orderInfo.order?.payment?.surcharge ?: 0.0
            surcharge.text = (surchargeValue.plus(estBonus)).toString()

            if (showing?.isMethodShow == true) {
                customerPaymentContainer.setVisible(true)
                paymentMethodIcon.setVisible(true)
                paymentMethodIcon.setImageResource(getPaymentResource(orderInfo.order?.payment?.method!!))
            }
            if (showing?.isEstimatedCostShow == true) {
                estimatedAmount.text = orderInfo.order?.payment?.estimatedCost.toString()
            }

            orderInfo.order?.user?.name?.let {
                userName.text = it
            }

            val ratingVal = orderInfo.order?.user?.rating
            ratingVal?.let {
                userRating.text = it.toString()
            }

            if (ratingVal == 0.0f || ratingVal == null) {
                ratingContainer.setVisible(false)
            }
            orderInfo.order?.user?.rating?.let {
                userRating.text = it.toString()
            }

            contactsContainer.setVisible(true)
            navigator.setVisible(true)
            controlsContainer.setVisible(true)

            orderInfo.order?.coordinates?.let {
                populateDestination(it, orderInfo.order?.distance, orderInfo.order?.calculatedPath)
            }

            val photo = orderInfo.order?.user?.photo
            if (!photo.isNullOrBlank()) {
                loadRoundedImage(context!!, avatarImage, url = photo)
            }

            controlsContainer.setOnClickListener {
                viewModel.cancelOrder(orderInfo.order!!)
            }
            phoneImage.setOnClickListener {
                orderInfo.order?.user!!.phone?.let {
                    makeCallClick(it)
                    viewModel.sendHelpTime(orderInfo.id!!, SendHelpTimeType.PHONE)
                }
            }
        }
    }

    private fun handleCamera() {
        if (orderAddressCoordinates.isNotEmpty()) {
            updateOrderMarkers(orderAddressCoordinates)
            updateDriverMarker()
            updateMultipleTargetCamera(lastLocation, orderAddressCoordinates)
            viewModel.getMapRoute(orderAddressCoordinates)
        } else {
            updateDriverMarker()
            updateDriverCamera()
        }
    }

    private fun makeCallClick(number: String) {
        if (!isPermissionGranted(this@NewOrderFragment, Manifest.permission.CALL_PHONE)) {
            requestPermissions(arrayOf(Manifest.permission.CALL_PHONE), ExecuteOrderFragment.CALL_PHONE_REQUEST)
        } else {
            dialUser(number)
        }
    }

    private fun populateDestination(addresses: List<Address>, start: Double?, end: Double?) {
        with(binding.addressContainer) {
            removeAllViews()
            if (isAddressShow()) {
                when (addresses.size) {
                    0 -> return
                    1 -> {
                        val first = getOpeningItem(start, formatAddress(addresses[0]))
                        val second = AddressItem(context).apply {
                            setData(
                                    indicator = R.drawable.ic_address_to,
                                    itemIcon = R.drawable.ic_route,
                                    distanceText = "$end км",
                                    addressText = getString(R.string.around_town)
                            )
                        }

                        addAll(first, second)
                    }
                    2 -> {
                        val first = getOpeningItem(start, formatAddress(addresses[0]))
                        val second = AddressItem(context).apply {
                            setData(
                                    R.drawable.ic_address_to,
                                    R.drawable.ic_route,
                                    "$end км",
                                    formatAddress(addresses[1])
                            )
                        }

                        addAll(first, second)
                    }
                    else -> {
                        addresses.forEachIndexed { index, address ->
                            addView(when (index) {
                                0 -> {
                                    AddressItem(context).apply {
                                        setData(
                                                R.drawable.ic_address_from,
                                                R.drawable.ic_near_me,
                                                "$start км",
                                                formatAddress(address),
                                                showBottomSeparator = true
                                        )
                                    }
                                }

                                addresses.size - 1 -> {
                                    AddressItem(context).apply {
                                        setData(
                                                R.drawable.ic_address_to,
                                                R.drawable.ic_route,
                                                "$end км",
                                                formatAddress(address),
                                                showTopSeparator = true
                                        )
                                    }
                                }
                                else -> {
                                    AddressItem(context).apply {
                                        setData(
                                                addressText = formatAddress(address),
                                                showSeparator = true
                                        )
                                    }
                                }
                            }, addressItemParams)
                        }
                    }
                }
            } else {
                if (addresses.isNotEmpty()) {
                    val first = getOpeningItem(start, formatAddress(addresses[0]))
                    val second = AddressItem(context).apply {
                        setData(
                                itemIcon = R.drawable.ic_route,
                                distanceText = "$end км"
                        )
                    }
                    addAll(first, second)
                } else return
            }


            setVisible(true)
        }
    }

    private fun isAddressShow(): Boolean {
        return newOrderInfo?.order?.acceptedOrderShowing?.isAddressShow!!
    }

    private fun getOpeningItem(start: Double?, address: String): AddressItem {
        return AddressItem(context!!).apply {
            setData(R.drawable.ic_address_from,
                    R.drawable.ic_near_me,
                    "$start км",
                    address
            )
        }
    }

    private fun getPaymentResource(payment: PaymentMethod): Int {
        return when (payment) {
            PaymentMethod.CASH -> R.drawable.ic_cash
            PaymentMethod.CASH_AND_BONUS -> R.drawable.ic_cash
            PaymentMethod.CARD -> R.drawable.ic_card
            PaymentMethod.CARD_AND_BONUS -> R.drawable.ic_card
        }
    }

    private fun Long.convertToMMss() = SimpleDateFormat("mm:ss", Locale("uk", "UA"))
            .format(Date(this))

    private fun ViewGroup.addAll(vararg views: View) {
        views.forEach {
            addView(it, addressItemParams)
        }
    }

}