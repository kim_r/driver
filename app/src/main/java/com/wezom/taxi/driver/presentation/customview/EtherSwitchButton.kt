package com.wezom.taxi.driver.presentation.customview

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.databinding.ButtonEtherSwitchBinding
import com.wezom.taxi.driver.ext.setVisible

/**
 *Created by Zorin.A on 18.June.2019.
 */
class EtherSwitchButton : ConstraintLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context,
                attrs: AttributeSet?) : super(context,
                                              attrs)

    constructor(context: Context,
                attrs: AttributeSet?,
                attributeSetId: Int) : super(context,
                                             attrs,
                                             attributeSetId)

    private val binding = ButtonEtherSwitchBinding.inflate(LayoutInflater.from(context),
                                                           this,
                                                           true)
    var state: State = State.UNSPECIFIED
    var listener: OnEtherClickListener? = null

    init {
        if (state == State.UNSPECIFIED) {
            setVisible(false)
            if (cache > 0) {
                updateCounter(cache)
            }
        }
    }

    fun show(isShow: Boolean) {
        setVisible(isShow)
    }

    fun initAsList() {
        state = State.LIST
        binding.run {
            actionButton.setOnClickListener {
                listener?.onClick(state)
            }
            actionButton.setImageResource(R.drawable.ic_list)
        }
    }

    fun initAsMap() {
        state = State.MAP
        binding.run {
            actionButton.setOnClickListener {
                listener?.onClick(state)
            }
            actionButton.setImageResource(R.drawable.ic_map)
        }
    }

    fun updateCounter(newValue: Int) {
        binding.counter.text = newValue.toString()
    }

    fun increaseCounter() {
        val count = binding.counter.text.toString()
            .toInt()
        val newValue = count + 1
        updateCounter(newValue)
    }

    fun decreaseCounter() {
        val count = binding.counter.text.toString()
            .toInt()
        if (count > 0) {
            val newValue = count - 1
            updateCounter(newValue)
        }
    }

    enum class State {
        LIST,
        MAP,
        UNSPECIFIED
    }

    interface OnEtherClickListener {
        fun onClick(state: State)
    }

    companion object {
        var cache: Int = 0
    }
}