package com.wezom.taxi.driver.presentation.addcard

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxTextView
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.common.DateCardFormatWatcher
import com.wezom.taxi.driver.common.FourDigitCardFormatWatcher

import com.wezom.taxi.driver.databinding.AddCardBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.presentation.base.BaseFragment
import io.reactivex.rxkotlin.Observables
import io.reactivex.rxkotlin.plusAssign
import org.jetbrains.anko.support.v4.toast
import java.util.regex.Pattern

/**
 * Created by Andrew on 01.03.2018.
 */
class AddCardFragment : BaseFragment() {

    private lateinit var binding: AddCardBinding
    private lateinit var viewModel: AddCardViewModel

    private val userNamePattern = Pattern.compile("^[А-ЯЁ][а-яё]+\\s[А-ЯЁ][а-яё]+\\s[А-ЯЁ][а-яё]+\$")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setBackPressFun { viewModel.onBackPressed() }
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = AddCardBinding.inflate(inflater)!!
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            toolbar.setToolbarTitle(getString(R.string.add_card_toolbar_title))
            addCardNumberEdit.addTextChangedListener(FourDigitCardFormatWatcher())
            addCardDateEdit.addTextChangedListener(DateCardFormatWatcher())

            disposable += RxView.clicks(addCardNumberImage).subscribe {
                showInfoDialog(R.drawable.nomer_carty_picture, R.string.card_number, R.string.card_number_text)
            }
            disposable += RxView.clicks(addCardNameImage).subscribe {
                showInfoDialog(R.drawable.ic_fio_picture, R.string.add_card_name_hint, R.string.owner_name_text)
            }
            disposable += RxView.clicks(addCardDateImage).subscribe {
                showInfoDialog(R.drawable.srok_deistvia_picture, R.string.validity_of, R.string.validity_of_text)
            }
            disposable += RxView.clicks(addCardCvvImage).subscribe {
                showInfoDialog(R.drawable.ic_cvv_picture, R.string.add_card_cvv_hint, R.string.enter_cvv)
            }
            disposable += RxView.clicks(addCardSave).subscribe {
                toast("card added")
                /*   viewModel.saveCard(addCardNumberEdit.text.toString(),
                            addCardNameEdit.text.toString(),
                            addCardDateEdit.text.toString(),
                            addCardCvvEdit.text.toString())*/
            }
            setErrorListener(addCardNumberEdit)
            setErrorListener(addCardNameEdit)
            setErrorListener(addCardDateEdit)
            setErrorListener(addCardCvvEdit)

            val numberObservable = RxTextView.textChanges(addCardNumberEdit)
            val nameObservable = RxTextView.textChanges(addCardNameEdit)
            val dateObservable = RxTextView.textChanges(addCardDateEdit)
            val cardObservable = RxTextView.textChanges(addCardCvvEdit)

            val isButtonEnabled = Observables.combineLatest(numberObservable, nameObservable, dateObservable, cardObservable) { number, name, date, cvv ->
                isFieldsValid(number.toString(), name.toString(), date.toString(), cvv.toString())
            }

            disposable += isButtonEnabled.subscribe { value ->
                enableAcceptButton(value)
            }
        }
    }

    private fun setErrorListener(textView: TextView) {
        binding.run {
            when (textView.id) {
                addCardNumberEdit.id -> {
                    disposable += RxTextView.textChanges(textView).subscribe { text ->
                        if (text.isNotEmpty()) {
                            if (!(text.length == 19)) {
                                textView.error = getString(R.string.wrong_format)
                            } else {
                                //do nothing, all right
                            }
                        }
                    }
                }
                addCardNameEdit.id -> {
                    disposable += RxTextView.textChanges(textView).subscribe { text ->
                        if (text.isNotEmpty()) {
                            if (!(userNamePattern.matcher(text).matches())) {
                                textView.error = getString(R.string.wrong_format)
                            } else {
                                //do nothing, all right
                            }
                        }
                    }
                }
                addCardDateEdit.id -> {
                    disposable += RxTextView.textChanges(textView).subscribe { text ->
                        if (text.isNotEmpty()) {
                            if (!(text.length == 5)) {
                                textView.error = getString(R.string.wrong_format)
                            } else {
                                //do nothing, all right
                            }
                        }
                    }
                }
                addCardCvvEdit.id -> {
                    disposable += RxTextView.textChanges(textView).subscribe { text ->
                        if (text.isNotEmpty()) {
                            if (!(text.length == 3)) {
                                textView.error = getString(R.string.wrong_format)
                            } else {
                                //do nothing, all right
                            }
                        }
                    }
                }

            }
        }
    }

    private fun isFieldsValid(number: String, name: String, date: String, cvv: String): Boolean {
        return number.isNotEmpty() && number.length == 19 &&
                name.isNotEmpty() && userNamePattern.matcher(name).matches() &&
                date.isNotEmpty() && date.length == 5 &&
                cvv.isNotEmpty() && cvv.length == 3
    }

    private fun enableAcceptButton(value: Boolean) {
        if (value) {
            binding.addCardSave.setBackgroundColor(resources.getColor(R.color.colorAccent2))
            binding.addCardSave.isEnabled = true
        } else {
            binding.addCardSave.setBackgroundColor(resources.getColor(R.color.colorLiteGrey))
            binding.addCardSave.isEnabled = false
        }
    }
}