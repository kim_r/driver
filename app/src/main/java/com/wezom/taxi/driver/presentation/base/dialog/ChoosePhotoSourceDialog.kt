package com.wezom.taxi.driver.presentation.base.dialog

import android.Manifest
import android.app.Dialog
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import com.jakewharton.rxbinding2.view.RxView
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.common.isPermissionGranted
import com.wezom.taxi.driver.databinding.ChosePhotoSourceBinding
import com.wezom.taxi.driver.ext.setVisible
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign

/**
 * Created by zorin.a on 02.04.2018.
 */
class ChoosePhotoSourceDialog : DialogFragment() {
    private lateinit var binding: ChosePhotoSourceBinding
    private val disposable = CompositeDisposable()
    var callbacks: ChoosePhotoSourceDialog.Callbacks? = null
    var isWatchShow: Boolean? = true

    private val needPermissionsDialog by lazy {
        android.app.AlertDialog.Builder(activity)
                .setPositiveButton(R.string.ok) { self, _ ->
                    callbacks?.onSettingsOpen()
                    self.dismiss()
                }
                .setMessage(R.string.need_permission)
                .create()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        arguments?.getBoolean(IS_WATCH_AVAILABLE_KEY)?.let {
            isWatchShow = it
        }
        binding = DataBindingUtil.inflate(activity?.layoutInflater!!, R.layout.dialog_chose_source, null, false)
        handleClicks()
        if (!isWatchShow!!) {
            binding.run {
                watchContainer.setVisible(false)
            }
        }
        val builder = AlertDialog.Builder(context!!, R.style.WhiteDialogTheme)
        builder.setView(binding.root)
                .setNegativeButton(R.string.cancel) { _, _ -> dismiss() }
        return builder.create()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        grantResults.forEachIndexed { i, result ->
            if (result == PackageManager.PERMISSION_DENIED) {
                if (!shouldShowRequestPermissionRationale(permissions[i])) {
                    dismiss()
                    needPermissionsDialog.show()
                }
                return
            }
        }

        when (requestCode) {
            READ_REQUEST_CODE -> callbacks?.onGalleryClicked()
            CAMERA_REQUEST_CODE -> callbacks?.onCameraClicked()
        }
        dismiss()
    }

    override fun onDestroy() {
        disposable.dispose()
        super.onDestroy()
    }

    private fun handleClicks() {
        binding.run {
            disposable += RxView.clicks(cameraContainer).subscribe {
                if (!isPermissionGranted(this@ChoosePhotoSourceDialog, Manifest.permission.CAMERA)) {
                    requestPermissions(arrayOf(Manifest.permission.CAMERA), CAMERA_REQUEST_CODE)
                } else {
                    callbacks?.onCameraClicked()
                    dismiss()
                }
            }

            disposable += RxView.clicks(galleryContainer).subscribe {
                if (!isPermissionGranted(this@ChoosePhotoSourceDialog, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), READ_REQUEST_CODE)
                } else {
                    callbacks?.onGalleryClicked()
                    dismiss()
                }
            }

            disposable += RxView.clicks(watchContainer).subscribe {
                callbacks?.onWatchClicked()
                dismiss()
            }

            disposable += RxView.clicks(deleteContainer).subscribe {
                callbacks?.onSettingsOpen()
                dismiss()
            }
        }
    }

    companion object {
        private const val READ_REQUEST_CODE = 0
        private const val CAMERA_REQUEST_CODE = 1
        const val TAG: String = "ChoosePhotoSourceDialog"
        const val IS_WATCH_AVAILABLE_KEY = "isWatchAvailable"

        fun newInstance(isWatchAvailable: Boolean): ChoosePhotoSourceDialog {
            val dialog = ChoosePhotoSourceDialog()
            val bundle = Bundle()
            bundle.putBoolean(IS_WATCH_AVAILABLE_KEY, isWatchAvailable)
            dialog.arguments = bundle
            return dialog
        }
    }

    interface Callbacks {
        fun onGalleryClicked()
        fun onCameraClicked()
        fun onWatchClicked()
        fun onSettingsOpen()
    }
}