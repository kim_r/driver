package com.wezom.taxi.driver.common

import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.app.ActivityCompat.requestPermissions
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat

/**
 * Created by zorin.a on 27.02.2018.
 */
fun askForPermission(fragment: Fragment, permission: Array<String>, requestCode: Int) {
    fragment.requestPermissions(permission, requestCode)
}

fun askForPermission(activity: Activity, permission: Array<String>, requestCode: Int) {
    requestPermissions(activity, permission, requestCode)
}

fun isPermissionGranted(fragment: Fragment, permission: String): Boolean =
        (ContextCompat.checkSelfPermission(fragment.context!!, permission) == PackageManager.PERMISSION_GRANTED)

fun isPermissionGranted(activity: Activity, permission: String): Boolean =
        (ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED)

fun arePermissionsGranted(fragment: Fragment, permissions: Array<String>): Boolean =
        permissions.any { isPermissionGranted(fragment, it) }

fun arePermissionsGranted(activity: Activity, permissions: Array<String>): Boolean =
        permissions.any { isPermissionGranted(activity, it) }

inline fun <T : Fragment> T.givenPermission(permissions: Array<String>, requestCode: Int, crossinline block: () -> Unit) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        if (arePermissionsGranted(this, permissions)) {
            block()
        } else {
            askForPermission(this, permissions, requestCode)
        }
    } else {
        block()
    }
}

inline fun <T : Activity> T.givenPermission(permissions: Array<String>, requestCode: Int, crossinline block: () -> Unit) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        if (arePermissionsGranted(this, permissions)) {
            block()
        } else {
            askForPermission(this, permissions, requestCode)
        }
    } else {
        block()
    }
}

inline fun <T : Fragment> T.ifPermissionsGranted(permissions: Array<String>, crossinline onPermissionsGranted: () -> Unit) {
    if (arePermissionsGranted(this, permissions)) {
        onPermissionsGranted()
    }
}

inline fun Fragment.checkPermissionGranted(permission: String) =
        ContextCompat.checkSelfPermission(context!!, permission) == PackageManager.PERMISSION_GRANTED