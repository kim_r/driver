package com.wezom.taxi.driver.presentation.neworder

import android.arch.lifecycle.MutableLiveData
import android.content.SharedPreferences
import com.wezom.taxi.driver.bus.events.ResultDataEvent
import com.wezom.taxi.driver.common.*
import com.wezom.taxi.driver.common.Constants.DEFAULT_NAV
import com.wezom.taxi.driver.data.db.DbManager
import com.wezom.taxi.driver.data.models.*
import com.wezom.taxi.driver.ext.NAVIGATOR
import com.wezom.taxi.driver.ext.int
import com.wezom.taxi.driver.net.api.ApiManager
import com.wezom.taxi.driver.net.jobqueue.JobFactory
import com.wezom.taxi.driver.net.jobqueue.jobs.SendHelpTimeJob
import com.wezom.taxi.driver.net.request.SendHelpTimeRequest
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseMapViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import javax.inject.Inject


class NewOrderViewModel @Inject constructor(sharedPreferences: SharedPreferences,
                                            screenRouterManager: ScreenRouterManager,
                                            private val apiManager: ApiManager,
                                            private val dbManager: DbManager,
                                            private val orderManager: OrderManager) : BaseMapViewModel(screenRouterManager, apiManager, sharedPreferences) {
    private var navigatorType: Int by sharedPreferences.int(key = NAVIGATOR, default = DEFAULT_NAV)

    val currentOrderLiveData: MutableLiveData<NewOrderInfo> = MutableLiveData()
    var navigatorTypeLiveData = MutableLiveData<Int>()
    private var acceptedReasons: List<Reason>? = null
    private var arrivedAtPlaceReasons: List<Reason>? = null
    private var executingReasons: List<Reason>? = null
    private var resultData: ResultDataEvent? = null

    fun cancelOrder(order: Order) {
        val allReasons = HashMap<OrderStatus, List<Reason>>()
        val reasons = dbManager.queryReasonByStatus(order.status!!)
        allReasons[order.status!!] = reasons

        switchScreen(REFUSE_ORDER_SCREEN, Triple(order, allReasons, resultData))
    }

    fun getNavigator() {
        navigatorTypeLiveData.postValue(navigatorType)
    }


    fun getSecondaryOrder() {
        currentOrderLiveData.postValue(orderManager.getSecondaryOrder())
    }

    fun getRefuseReasons() {
        App.instance.getLogger()!!.log("getNotPaidReasons start ")
        apiManager.refuseReasons(OrderStatus.ACCEPTED.value).subscribe({ response ->
            App.instance.getLogger()!!.log("getNotPaidReasons suc ")
            if (response.isSuccess!!) {
                acceptedReasons = response.reasons
            }
        }, { App.instance.getLogger()!!.log("getNotPaidReasons error ")})


        App.instance.getLogger()!!.log("getNotPaidReasons start ")
        apiManager.refuseReasons(OrderStatus.ARRIVED_AT_PLACE.value).subscribe({ response ->
            App.instance.getLogger()!!.log("getNotPaidReasons suc ")
            if (response.isSuccess!!) {
                arrivedAtPlaceReasons = response!!.reasons
            }
        }, { App.instance.getLogger()!!.log("getNotPaidReasons error ")})


        App.instance.getLogger()!!.log("getNotPaidReasons start ")
        apiManager.refuseReasons(OrderStatus.ON_THE_WAY.value).subscribe({ response ->
            App.instance.getLogger()!!.log("getNotPaidReasons suc ")
            if (response.isSuccess!!) {
                executingReasons = response!!.reasons
            }
        }, { App.instance.getLogger()!!.log("getNotPaidReasons error ")})
    }

    fun sendHelpTime(id: Int, type: SendHelpTimeType) {
        JobFactory.instance.getJobManager()?.addJobInBackground(SendHelpTimeJob(id, SendHelpTimeRequest(type)))
    }
}