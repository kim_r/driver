package com.wezom.taxi.driver.presentation.settings

import android.content.SharedPreferences
import com.wezom.taxi.driver.ext.*
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import javax.inject.Inject

class SettingSignalViewModel @Inject constructor(screenRouterManager: ScreenRouterManager,
                                                 sharedPreferences: SharedPreferences) : BaseViewModel(screenRouterManager) {


    var songSignal: Boolean by sharedPreferences.boolean(SONG_SIGNAL, true)
    var signalOnline: Boolean by sharedPreferences.boolean(SIGNAL_ONLINE, true)
    var signalOnlineFilter: Boolean by sharedPreferences.boolean(SIGNAL_ONLINE_FILTER, true)
    var signalBackground: Boolean by sharedPreferences.boolean(SIGNAL_BACKGROUND, true)
    var signalBackgroundWait: Boolean by sharedPreferences.boolean(SIGNAL_BACKGROUND_WAIT, false)
}