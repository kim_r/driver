package com.wezom.taxi.driver.presentation.bonus_program

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.databinding.BonusProgramBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.net.response.CurrendDiscountResponse
import com.wezom.taxi.driver.presentation.base.BaseFragment

/**
 * Created by zorin.a on 3/11/19.
 */

class BonusProgramFragment : BaseFragment() {
    lateinit var binding: BonusProgramBinding
    lateinit var viewModel: BonusProgramViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
        viewModel.loadingLiveData.observe(this, progressObserver)
        viewModel.bonusLiveData.observe(this, Observer {
            setData(it)
        })
        setBackPressFun { viewModel.onBackPressed() }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = BonusProgramBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUi()
        viewModel.getCurrentBonusProgram()
    }

    private fun setData(data: CurrendDiscountResponse?) {
        binding.run {
            description.text = data?.description
            data?.actionConditions?.let {
                actionConditionsView.setActionConditionData(it)
            }
            data?.actionTripsInfo?.let {
                tripsConditionsView.setDrivesConditionData(it.conditions)
                tripsScaleView.setData(it)

            }
        }
    }

    private fun setupUi() {
        binding.run {
            toolbar.setToolbarTitle(getString(R.string.bonus_program))
        }
    }

    companion object {
        fun newInstance(): BonusProgramFragment {
            return BonusProgramFragment()
        }
    }
}