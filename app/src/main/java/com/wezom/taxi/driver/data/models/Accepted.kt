package com.wezom.taxi.driver.data.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Accepted(
        @SerializedName("longitude") var longitude: Double,
        @SerializedName("latitude") var latitude: Double,
        @SerializedName("eventTime") var eventTime: Long,
        @SerializedName("driverId") var driverId: Int
) : Parcelable, Serializable {
    constructor(parcel: Parcel) : this(
            parcel.readDouble(),
            parcel.readDouble(),
            parcel.readLong(),
            parcel.readInt())

    constructor() : this(longitude = 0.0, latitude = 0.0, eventTime = 0, driverId = 0)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(longitude)
        parcel.writeDouble(latitude)
        parcel.writeLong(eventTime)
        parcel.writeInt(driverId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Accepted> {
        override fun createFromParcel(parcel: Parcel): Accepted {
            return Accepted(parcel)
        }

        override fun newArray(size: Int): Array<Accepted?> {
            return arrayOfNulls(size)
        }
    }
}
