package com.wezom.taxi.driver.presentation.notpaid

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.RadioButton
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.data.models.Reason
import com.wezom.taxi.driver.databinding.NotPaidBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.ext.setVisible
import com.wezom.taxi.driver.ext.shortToast
import com.wezom.taxi.driver.presentation.base.BaseMapFragment
import com.wezom.taxi.driver.presentation.executeorder.dialog.BlockingClientDialog

/**
 * Created by udovik.s on 27.03.2018.
 */
class NotPaidFragment : BaseMapFragment() {
    //region var
    private lateinit var binding: NotPaidBinding
    private lateinit var viewModel: NotPaidViewModel

    private var orderId: Int = 0
    private var reasonsList: List<Reason>? = null

    //endregion

    //region override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setBackPressFun { viewModel.onBackPressed() }
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
        orderId = arguments!!.getInt(ORDER_ID)

        viewModel.reasonsLiveData.observe(this, Observer { it ->
            reasonsList = it!!
            updateUi(it)
        })
        viewModel.loadingLiveData.observe(this, progressObserver)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = NotPaidBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getNotPaidReasons()
        binding.run {
            toolbar.setToolbarTitle(getString(R.string.not_paid))

//            val toolbarBinding = DataBindingUtil.getBinding<ToolbarBinding>(toolbar)

            btnAccept.setOnClickListener {
                if (layoutAddComment.visibility == VISIBLE && editAddComment.text.isEmpty()) {
                    getString(R.string.add_comment_please).shortToast(context)
                } else {
                    showDialogBlockingClient(rgReasonsList.checkedRadioButtonId, editAddComment.text.toString())
                }
            }

            binding.rgReasonsList.setOnCheckedChangeListener({ _, checkedId ->
                showAddComment(reasonsList!![checkedId - 1].isCommentRequired!!)
                enableAcceptButton()
            })
        }
    }
//endregion

    //region fun
    private fun updateUi(list: List<Reason>) {
        binding.run {
            for (reason: Reason in list) {
                val radioButton: RadioButton = activity!!.layoutInflater.inflate(
                        R.layout.item_radio_button_not_paid,
                        null
                ) as RadioButton
                radioButton.text = reason.name
                radioButton.id = reason.id!!
                rgReasonsList.addView(radioButton)
            }
            btnAccept.setVisible(true)
        }
    }

    private fun showAddComment(isCommentRequired: Boolean) {
        binding.run {
            if (isCommentRequired) {
                layoutAddComment.setVisible(true)
                editAddComment.clearFocus()
                editAddComment.text.clear()
            } else {
                layoutAddComment.setVisible(false)
                editAddComment.clearFocus()
                editAddComment.text.clear()
            }
        }
    }

    private fun showDialogBlockingClient(checkedRadioButtonId: Int, editAddComment: String) {
        val dialog = BlockingClientDialog()
        dialog.listener = object : BlockingClientDialog.DialogClickListener {
            override fun onPositiveClick() {
                dialog.dismiss()
                getString(R.string.accepted).shortToast(context)
                viewModel.customerDidNotPay(
                        orderId = orderId,
                        latitude = lastLocation?.latitude,
                        longitude = lastLocation?.longitude,
                        reasonId = checkedRadioButtonId,
                        comment = editAddComment,
                        eventTime = System.currentTimeMillis()
                )
            }

            override fun onNegativeClick() {
                dialog.dismiss()
            }
        }
        dialog.show(fragmentManager, BlockingClientDialog.TAG)
    }

    private fun enableAcceptButton() {
        binding.btnAccept.setBackgroundColor(resources.getColor(R.color.colorAccent2))
        binding.btnAccept.isEnabled = true
    }
//endregion

    companion object {
        const val ORDER_ID = "order_id"
        fun newInstance(id: Int): NotPaidFragment {
            val args = Bundle()
            args.putInt(ORDER_ID, id)
            val fragment = NotPaidFragment()
            fragment.arguments = args
            return fragment
        }
    }
}