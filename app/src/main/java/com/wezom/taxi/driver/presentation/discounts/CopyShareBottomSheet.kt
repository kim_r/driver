package com.wezom.taxi.driver.presentation.discounts

import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wezom.taxi.driver.databinding.CopyShareBottomSheetBinding

class CopyShareBottomSheet : BottomSheetDialogFragment() {

    private lateinit var binding: CopyShareBottomSheetBinding

    lateinit var onCopyClick: () -> Unit
    lateinit var onSocialClick: () -> Unit

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = CopyShareBottomSheetBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            copy.setOnClickListener {
                onCopyClick()
                dismiss()
            }
            share.setOnClickListener {
                onSocialClick()
                dismiss()
            }
            cancel.setOnClickListener { dismiss() }
        }
    }
}