package com.wezom.taxi.driver.presentation.gains

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.data.models.Day
import com.wezom.taxi.driver.data.models.DayRange
import com.wezom.taxi.driver.databinding.GainsBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.ext.setVisible
import com.wezom.taxi.driver.net.response.DriverIncomesResponse
import com.wezom.taxi.driver.presentation.base.BaseFragment
import com.wezom.taxi.driver.presentation.customview.GainsEqualizer
import com.wezom.taxi.driver.presentation.gains.dialog.DateDialog
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.rxkotlin.plusAssign
import java.util.*


/**
 * Created by zorin.a on 21.03.2018.
 */

class GainsFragment : BaseFragment() {
    //region var
    lateinit var viewModel: GainsViewModel
    lateinit var binding: GainsBinding
    private val calendar = GregorianCalendar(SimpleTimeZone.getDefault())

    private var weekResponse: DriverIncomesResponse? = null
    private var currentWeekOfYear: Int = 0

    //endregion
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()

        currentWeekOfYear = Calendar.getInstance().get(Calendar.WEEK_OF_YEAR)
        viewModel.dateRangeLiveData.observe(this, Observer<DayRange> { range ->
            binding.run {
                weeklyGainsStartDate.text = "${range?.startDay}.${range?.startMonth}"
                weeklyGainsEndDate.text = "${range?.endDay}.${range?.endMonth}"
            }
        })
        viewModel.incomesLiveData.observe(this, Observer<DriverIncomesResponse> { resp ->
            weekResponse = resp
            binding.gainsEqualizer.setDataRange(weekResponse?.days)
            updateUiAsWeek(resp)
        })
        viewModel.loadingLiveData.observe(this, progressObserver)
        setOnRefreshCallback {
            val year = calendar.get(Calendar.YEAR)
            val month = calendar.get(Calendar.MONTH)
            val day = calendar.get(Calendar.DAY_OF_MONTH)
            viewModel.getIncomesOnNewDate(year, month, day)
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = GainsBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUi()

        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
        viewModel.getIncomesOnNewDate(year, month, day)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.gains_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_calendar -> showCalendar()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showCalendar() {
        val dialog = DateDialog()
        dialog.listener = object : DateDialog.DatePickerListener {
            override fun onDatePicked(year: Int, month: Int, day: Int) {
                viewModel.getIncomesOnNewDate(year, month, day)
            }
        }
        dialog.show(childFragmentManager, DateDialog.TAG)
    }

    @SuppressLint("SetTextI18n")
    private fun updateUiAsWeek(chosenWeek: DriverIncomesResponse?) {
        binding.run {
            if (chosenWeek != null) {
                TransitionManager.beginDelayedTransition(rangeControl)
                moneyValue.text = String.format("%,d", (chosenWeek.rangeTotalIncome!! + chosenWeek.rangeSurcharge!!).toInt())
                gainsFromValue.text = String.format("%,d", chosenWeek.rangeTotalIncome)
                gainsCashValue.text = String.format("%,d", chosenWeek.rangeCashIncome)
                gainsCashlessValue.text = String.format("%,d", chosenWeek.rangeNonCashIncome)
                surchargesFromValue.text = String.format("%,d", chosenWeek.rangeSurcharge.toInt())
                distanceTraveledValue.text = "${chosenWeek.rangeDistance?.toInt()} ${getString(R.string.km)}"
                onlineTimeValue.text = calcTime(chosenWeek.rangeOnlineTime!!)
                completedTripsValue.text = chosenWeek.rangeCompletedTrips.toString()
                singleDayName.setVisible(false)
            }
        }
    }

    private fun updateUiAsDay(day: Day?) {
        binding.run {
            if (day != null) {
                TransitionManager.beginDelayedTransition(rangeControl)
                gainsFromValue.text = String.format("%,d", day.totalIncome)
                gainsCashValue.text = String.format("%,d", day.cashIncome)
                gainsCashlessValue.text = String.format("%,d", day.nonCashIncome)
                surchargesFromValue.text = String.format("%,d", day.surcharge?.toInt())
                distanceTraveledValue.text = "${day.distance?.toInt()} ${getString(R.string.km)}"
                onlineTimeValue.text = calcTime(day.onlineTime!!)
                completedTripsValue.text = day.completedTrips.toString()
                singleDayName.setVisible(true)
                singleDayName.text = formatSelectedDate(day)
            }
        }
    }

    private fun formatSelectedDate(day: Day?): String {
        val c = GregorianCalendar(SimpleTimeZone.getDefault())
        c.timeInMillis = day?.date!!
        val dayOfMonth = c.get(Calendar.DAY_OF_MONTH)
        val dayOfWeekIndex = day.dayOfWeek!! - 1
        val monthIndex = c.get(Calendar.MONTH)
        val weekDay = resources.getStringArray(R.array.where_to_week_days)[dayOfWeekIndex]
        val month = resources.getStringArray(R.array.where_to_months)[monthIndex]
        return String.format(getString(R.string.for_day, weekDay, dayOfMonth, month))
    }

    private fun calcTime(respTime: Long): String {
        var time = respTime
        val timeZone = java.util.TimeZone.getTimeZone("UTC")
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.timeZone = timeZone
        if (time < 0) {
            time = Math.abs(time)
        }
        calendar.timeInMillis = time
        val min = calendar.get(Calendar.MINUTE)
        val hour = calendar.get(Calendar.HOUR)
        return if (hour > 0) context!!.getString(R.string.time_online_full, hour, min) else context!!.getString(R.string.time_online_min, min)
    }

    private fun setupUi() {
        binding.run {
            toolbar.setToolbarTitle(getString(R.string.gains))
            disposable += RxView.clicks(chevronRight).subscribe {
                if (currentWeekOfYear < Calendar.getInstance().get(Calendar.WEEK_OF_YEAR)) {
                    currentWeekOfYear++
                    incrementWeek(POSITIVE_SIDE)
                }
            }
            disposable += RxView.clicks(chevronLeft).subscribe {
                currentWeekOfYear--
                incrementWeek(NEGATIVE_SIDE)
            }
            disposable += RxView.clicks(info).subscribe {
                showInfoDialog(R.string.supplements, R.string.supplements_text)
            }
            gainsEqualizer.listener = object : GainsEqualizer.OnGainClickListener {
                override fun onItemClick(isChecked: Boolean, day: Day?) {
                    if (isChecked && day != null) {
                        updateUiAsDay(day)
                    } else {
                        updateUiAsWeek(weekResponse)
                    }
                }
            }
        }
    }

    private fun incrementWeek(incrementSide: Int) {
        calendar.add(Calendar.WEEK_OF_YEAR, incrementSide)
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        viewModel.getIncomesOnNewDate(year, month, day)
    }

    companion object {
        const val POSITIVE_SIDE = 1
        const val NEGATIVE_SIDE = -1
    }
}