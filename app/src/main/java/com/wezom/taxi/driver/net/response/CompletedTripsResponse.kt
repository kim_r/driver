package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.Trip

/**
 * Created by Andrew on 13.03.2018.
 */
class CompletedTripsResponse(@SerializedName("trips") val trips :List<Trip>) : BaseResponse()