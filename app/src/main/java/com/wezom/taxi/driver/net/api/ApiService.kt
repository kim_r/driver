package com.wezom.taxi.driver.net.api

import android.support.annotation.Keep
import com.wezom.taxi.driver.BuildConfig
import com.wezom.taxi.driver.common.Constants.DIRECTIONS_API_URL
import com.wezom.taxi.driver.data.models.*
import com.wezom.taxi.driver.net.request.*
import com.wezom.taxi.driver.net.response.*
import io.reactivex.Single
import okhttp3.MultipartBody
import retrofit2.http.*


/**
 * Created by zorin.a on 020 20.02.18.
 */

@Keep
interface ApiService {
    @POST("/driver/v1/user/phone")
    fun sendPhone(@Body
                  body: SendPhoneRequest): Single<BaseResponse>

    @POST("/driver/v1/user/confirm_code")
    fun confirmCode(@Body
                    body: ConfirmCodeRequest): Single<ConfirmCodeResponse>

    @GET("/driver/v1/car/brands")
    fun carBands(): Single<CarBrandsResponse>

    @GET("/driver/v1/car/models/{id}")
    fun carModels(@Path("id")
                  brandId: Int): Single<CarModelsResponse>

    @GET("/driver/v1/car/colors")
    fun carColors(): Single<CarColorsResponse>

    @GET("/driver/v1/car/years/{modelId}")
    fun carIssueYears(@Path("modelId")
                      modelId: Int): Single<CarIssueYearsResponse>

    @GET("/driver/v1/user/profileAndRegistration/existed")
    fun existedProfileFields(): Single<ExistedProfileResponse>

    @POST("/driver/v1/user/registration")
    fun userRegistration(@Body
                         body: RegistrationRequest): Single<BaseResponse>

    @Multipart
    @POST("/driver/v1/user/load_images")
    fun registrationDocuments(
            @Part
            userImageBody: MultipartBody.Part?,
            @Part
            carImageBody: MultipartBody.Part?,
            @Part
            doc1ImageBody: MultipartBody.Part?,
            @Part
            doc2ImageBody: MultipartBody.Part?,
            @Part
            doc3ImageBody: MultipartBody.Part?,
            @Part
            doc4ImageBody: MultipartBody.Part?): Single<BaseResponse>

    @GET("/driver/v1/user/registration/documents")
    fun requiredDocsList(): Single<RequiredDocsListResponse>

    @GET("/driver/v1/user/profile")
    fun profileAndRegistrationCorrectness(): Single<ProfileAndRegistrationCorrectnessResponse>

    @GET("/driver/v1/user/status")
    fun checkUserStatus(@Header("version")
                        version: String): Single<UserStatusResponse>

    @PATCH("/driver/v1/user/card")
    fun changeCard(@Body
                   body: ChangeCardRequest): Single<BaseResponse>

    @POST("/driver/v1/user/card")
    fun addNewCard(@Body
                   body: ChangeCardRequest): Single<BaseResponse>

    @POST("${BuildConfig.NODE_REST_API}/driver/v1/user/order/accept/{id}")
    fun acceptOrder(@Path("id")
                    id: Int,
                    @Body
                    body: AcceptOrderRequest): Single<BaseResponse>

    @POST("${BuildConfig.NODE_REST_API}/driver/v1/user/order/skip/{id}")
    fun skipOrder(@Path("id")
                  id: Int,
                  @Body
                  body: SkipOrderRequest): Single<BaseResponse>

    @GET("${BuildConfig.NODE_REST_API}/driver/v1/user/current_orders")
    fun currentOrders(): Single<CurrentOrdersResponse>

    @GET("${BuildConfig.NODE_REST_API}/driver/v1/user/order/{id}")
    fun currentOrder(@Path("id") id: Int): Single<NewOrderInfo>

    @POST("${BuildConfig.NODE_REST_API}/driver/v1/user/order/refuse/{id}")
    fun refuseOrder(@Path("id")
                    id: Int,
                    @Body
                    body: RefuseOrderRequest): Single<BaseResponse>

    @GET("/driver/v1/order/cancel_reasons")
    fun refuseReasons(@Query("status")
                      status: Int): Single<RefuseReasonResponse>

    @GET("/driver/v1/order/nonpayment_reasons")
    fun getNotPaidReasons(): Single<NotPaidReasonResponse>

    @POST("${BuildConfig.NODE_REST_API}/driver/v1/user/order/unpaid/{id}")
    fun customerDidNotPay(@Path("id")
                          id: Int,
                          @Body
                          body: NotPaidRequest): Single<BaseResponse>

    @POST("${BuildConfig.NODE_REST_API}/driver/v1/user/order/arrived/{id}")
    fun arrivedAtPlace(@Path("id")
                       id: Int,
                       @Body
                       body: DriverEventRequest): Single<BaseResponse>

    @POST("${BuildConfig.NODE_REST_API}/driver/v1/user/order/in_way/{id}")
    fun driverOnTheWay(@Path("id")
                       id: Int,
                       @Body
                       body: DriverEventRequest): Single<BaseResponse>

    @GET("/driver/v1/user/trips/completed")
    fun getCompletedOrders(@Query("limit")
                           limit: Int?,
                           @Query("offset")
                           offset: Int?): Single<CompletedTripsResponse>

    @GET("/driver/v1/user/trip/completed/{orderInfo}")
    fun getCompletedOrder(@Path("orderInfo")
                          orderId: Int): Single<CompletedTripResponse>

    @GET("/driver/v1/user/statistics")
    fun getPersonalStatistics(): Single<PersonalStatisticsResponse>

    @GET("/driver/v1/user/discounts")
    fun getDiscounts(): Single<DiscountsResponse>

    @GET("/driver/v1/user/balance/received")
    fun getReceivedBalances(@Query("startDate")
                            startDate: Long,
                            @Query("finishDate")
                            finishDate: Long): Single<BalanceResponse>

    @GET("/driver/v1/user/balance/spend")
    fun getDebitedBalances(@Query("startDate")
                           startDate: Long,
                           @Query("finishDate")
                           finishDate: Long): Single<BalanceResponse>

    @GET("/driver/v1/user/incomes")
    fun getIncomes(@Query("startDate")
                   startDate: Long,
                   @Query("finishDate")
                   endDate: Long): Single<DriverIncomesResponse>

    @POST("/driver/v1/user/order/rate/{id}")
    fun rateTrip(@Path("id")
                 id: Int,
                 @Body
                 body: RateRequest): Single<BaseResponse>

    @POST("/driver/v1/user/phone/change")
    fun changePhone(@Body
                    body: Phone): Single<BaseResponse>

    @POST("/driver/v1/user/phone/change/confirm_code")
    fun changePhoneVerification(@Body
                                body: ChangePhone): Single<BaseResponse>

    @GET("/driver/v1/help/email")
    fun getHelpEmail(): Single<HelpEmailResponse>

    @GET("/driver/v1/user/card/purchase")
    fun getCardVerificationInfo(): Single<CardVerificationInfoResponse>

    @GET(DIRECTIONS_API_URL + "/directions/json?&key=AIzaSyAWFbbzWkdh9Gw9D2d7ryL6nWrAbjZrXyk")
    fun getMapRoute(@Query("origin")
                    origin: String,
                    @Query("destination")
                    destination: String,
                    @Query("waypoints")
                    waypoints: String? = null,
                    @Query("mode")
                    mode: String? = "DRIVING"): Single<RouteResponse>

    @POST("/driver/v1/route")
    fun getMapRouteOCM( @Body
                        body: CoordinatesOSM): Single<RouteResponseOCM>

    @POST("/driver/v1/user/logout")
    fun logout(@Header("Authorization")
               token: String): Single<BaseResponse>

//    @POST("$BASE_NODE_URL/driver/v1/user/order/finish/{id}")
//    fun finishOrder(@Path("id") id: Int, @Body request: FinishOrderRequest): Single<BaseResponse>

    @POST("${BuildConfig.NODE_REST_API}/driver/v1/user/order/total_cost/{id}")
    fun totalCost(@Path("id")
                  id: Int,
                  @Body
                  body: TotalCostRequest): Single<BaseResponse>

    @POST("${BuildConfig.NODE_REST_API}/driver/v1/user/order/synchronization/{id}")
    fun dataSynchronization(@Path("id")
                            id: Int,
                            @Body
                            body: TotalCostRequest): Single<BaseResponse>

    @POST("/driver/v1/user/status")
    fun setUserOnlineStatus(@Body
                            body: SetOnlineStatusRequest): Single<UserOnlineStatusResponse>

    @POST("/driver/v1/user/device")
    fun sendDeviceKey(@Body
                      request: SendDeviceKeyRequest): Single<BaseResponse>

    @GET("/driver/v1/user/required_documents")
    fun getRequiredDocuments(): Single<RequiredDocumentsResponse>

    @POST("/driver/v1/user/help/time/{id}")
    fun sendHelpTime(@Path("id")
                     id: Int,
                     @Body
                     request: SendHelpTimeRequest): Single<BaseResponse>

    @GET("/driver/v1/user/balance/current")
    fun checkCurrentBalance(): Single<UserBalanceResponse>

    @POST("${BuildConfig.NODE_REST_API}/driver/v1/user/order/finalize/{id}")
    fun finalizeOrder(@Path("id")
                      orderId: Int,
                      @Body
                      request: OrderFinalization): Single<BaseResponse>

    @GET("/driver/v1/user/discounts/current")
    fun getDiscountDetails(): Single<CurrendDiscountResponse>

    @GET("${BuildConfig.NODE_REST_API}/driver/v1/user/order/forceClose/{id}")
    fun forceCloseOrder(@Path("id")
                        orderId: Int): Single<BaseResponse>

    @GET("${BuildConfig.NODE_REST_API}/driver/v1/user/order/broadcast")
    fun getEther(): Single<EtherResponse>

    @POST("${BuildConfig.OSM_SERVER_URL}/geo-live/search")
    fun autocomplete(@Body
                     request: AutocompleteRequest): Single<List<AutoCompleteResult>>


    @POST("/driver/v1/user/orders_filters")
    fun addFilter(@Body
                  request: CreateFilterRequest): Single<BaseResponse>

    @POST("/driver/v1/user/orders_filters/{filter_id}")
    fun editFilter(@Path("filter_id")
                   filterId: Int,
                   @Body
                   request: CreateFilterRequest): Single<BaseResponse>

    @GET("/driver/v1/user/orders_filters")
    fun getFilter(): Single<HeadFilterResponse>

    @DELETE("/driver/v1/user/orders_filters/{id}")
    fun deleteFilter(@Path("id")
                     filterId: Int): Single<HeadFilterResponse>

    @GET("/driver/v1/car/car-types")
    fun getType(): Single<HeadTypeCarResponse>

    @GET("/driver/v1/user/orders_filters/{filter_id}/deactivate")
    fun deactivateFilter(@Path("filter_id")
                         filterId: Int): Single<HeadFilterResponse>

    @GET("/driver/v1/user/orders_filters/{filter_id}/activate")
    fun activateFilter(@Path("filter_id")
                       filterId: Int): Single<HeadFilterResponse>

    @GET("/driver/v1/move")
    fun setMoveCar(): Single<BaseResponse>

    @GET("/driver/v1/stop-move")
    fun setMoveStopCar(): Single<BaseResponse>

}
