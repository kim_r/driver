package com.wezom.taxi.driver.presentation.balance

import android.arch.lifecycle.MutableLiveData
import com.wezom.taxi.driver.data.models.Income
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.repository.DriverRepository
import io.reactivex.rxkotlin.plusAssign
import javax.inject.Inject

/**
 * Created by andre on 21.03.2018.
 */
class ReceivedBalancesViewModel @Inject constructor(screenRouterManager: ScreenRouterManager, private val repository: DriverRepository) : BaseViewModel(screenRouterManager) {

    val listLiveData: MutableLiveData<List<Income>> = MutableLiveData()

    fun getReceivedBalances(startTime: Long, finishTime: Long) {
        App.instance.getLogger()!!.log("getReceivedBalances start ")
        disposable += repository.getReceivedBalances(startTime, finishTime)
                .subscribe({ response ->
                    App.instance.getLogger()!!.log("getReceivedBalances suc ")
                    listLiveData.postValue(response.income)
                }, {
                    App.instance.getLogger()!!.log("getReceivedBalances error ")
                })
    }
}