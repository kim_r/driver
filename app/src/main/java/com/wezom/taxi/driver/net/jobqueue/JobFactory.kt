package com.wezom.taxi.driver.net.jobqueue

import android.content.Context

import android.os.Build
import com.birbit.android.jobqueue.JobManager
import com.birbit.android.jobqueue.config.Configuration
import com.birbit.android.jobqueue.log.CustomLogger
import com.birbit.android.jobqueue.scheduling.FrameworkJobSchedulerService
import com.birbit.android.jobqueue.scheduling.GcmJobSchedulerService
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GooglePlayServicesUtil
import com.wezom.taxi.driver.data.db.DbManager
import com.wezom.taxi.driver.net.api.ApiManager
import com.wezom.taxi.driver.net.jobqueue.services.SchedulerJobService
import timber.log.Timber

/**
 * Created by zorin.a on 23.04.2018.
 */
class JobFactory private constructor() {
    private var jobManager: JobManager? = null

    private object Holder {
        val INSTANCE = JobFactory()
    }

    companion object {
        val instance: JobFactory by lazy { Holder.INSTANCE }
    }

    fun initJobManager(apiManager: ApiManager, context: Context, dbManager: DbManager) {
        if (jobManager == null) {
            createJobManager(apiManager, context, dbManager)
        }
    }

    private fun createJobManager(apiManager: ApiManager, context: Context, dbManager: DbManager) {
        val logger = object : CustomLogger {
            override fun isDebugEnabled(): Boolean = true

            override fun d(text: String, vararg args: Any) {
                Timber.d(String.format(text, *args))
            }

            override fun e(t: Throwable, text: String, vararg args: Any) {
                Timber.e(t, String.format(text, *args))
            }

            override fun e(text: String, vararg args: Any) {
                Timber.e(String.format(text, *args))
            }

            override fun v(text: String, vararg args: Any) {
                Timber.v(String.format(text, *args))
            }
        }
        val builder = Configuration.Builder(context)
                .minConsumerCount(1)//always keep at least one consumer ali
                .maxConsumerCount(3)//up to 3 consumers at a time
                .injector({ job ->
                    if (job is JobInjector) {
                        job.inject(apiManager, context, dbManager)
                    }
                })
                .loadFactor(3)//3 jobs per consumer
                .consumerKeepAlive(120)//wait 2 minutes
                .customLogger(logger)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.scheduler(FrameworkJobSchedulerService.createSchedulerFor(context, SchedulerJobService::class.java))
        } else {
            val enableGcm = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context)
            if (enableGcm == ConnectionResult.SUCCESS) {
                builder.scheduler(GcmJobSchedulerService.createSchedulerFor(context, GcmJobSchedulerService::class.java), true)
            }
        }
        builder.build()
        jobManager = JobManager(builder.build())
    }

    fun getJobManager(): JobManager? = jobManager
}