package com.wezom.taxi.driver.injection.module

import com.wezom.taxi.driver.service.*
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by zorin.a on 13.04.2018.
 */
@Module
interface ServiceBuilder {
    @ContributesAndroidInjector
    fun contributeLocationService(): LocationService

    @ContributesAndroidInjector
    fun contributeSocketService(): SocketService

    @ContributesAndroidInjector
    fun contributeTaxometerService(): TaxometerService

    @ContributesAndroidInjector
    fun contributeOverlayWindowService(): OverlayWindowService

    @ContributesAndroidInjector
    fun contributeMessangingService(): MessagingService

    @ContributesAndroidInjector
    fun contributeAutoCloseOrderService(): AutoCloseOrderService
}