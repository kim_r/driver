package com.wezom.taxi.driver.common

import android.content.SharedPreferences
import com.wezom.taxi.driver.data.db.DbManager
import com.wezom.taxi.driver.data.models.*
import com.wezom.taxi.driver.ext.PRIMARY_ORDER_ID
import com.wezom.taxi.driver.ext.SECONDARY_ORDER_ID
import com.wezom.taxi.driver.ext.int
import com.wezom.taxi.driver.presentation.customview.DriverToolbar
import timber.log.Timber
import java.util.*
import javax.inject.Inject

/**
 * Created by zorin.a on 11.06.2018.
 */
class OrderManager @Inject constructor(sharedPreferences: SharedPreferences, private val dbManager: DbManager) {
    private var primaryOrderId by sharedPreferences.int(PRIMARY_ORDER_ID, 0)
    private var secondaryOrderId by sharedPreferences.int(SECONDARY_ORDER_ID, 0)


    fun addPrimaryOrderToDb(orderInfo: NewOrderInfo) {
        if (orderInfo.order != null)
            primaryOrderId = orderInfo.order!!.id!!
        else
            return
        orderInfo.id = primaryOrderId
        dbManager.insertOrderInfo(orderInfo).subscribe({}, {
            Timber.e(it.message)
        })
    }

    fun addSecondaryOrderToDb(orderInfo: NewOrderInfo) {
        DriverToolbar.IS_SECONDARY_ORDER_EXIST = true
        secondaryOrderId = orderInfo.order!!.id!!
        orderInfo.id = secondaryOrderId
        dbManager.insertOrderInfo(orderInfo).subscribe({}, {
            Timber.e(it.message)
        })
    }

    fun getPrimaryOrder(): NewOrderInfo? {
        var order: NewOrderInfo = dbManager.queryOrderInfoById(primaryOrderId)
        if (order != null)
            order.order!!.currentTime = Date().time
        return order
    }

    fun getSecondaryOrder(): NewOrderInfo? {
        var order: NewOrderInfo = dbManager.queryOrderInfoById(secondaryOrderId)
        if (order != null)
            order.order!!.currentTime = Date().time
        return order
    }

    fun deletePrimaryOrder() {
        dbManager.deleteOrderById(primaryOrderId.toLong())
        primaryOrderId = 0
    }

    fun deleteSecondaryOrder() {
        dbManager.deleteOrderById(secondaryOrderId.toLong())
        secondaryOrderId = 0
        DriverToolbar.IS_SECONDARY_ORDER_EXIST = false
    }

    fun wipeDatabase() {
        primaryOrderId = 0
        secondaryOrderId = 0
        dbManager.clearDatabase()
        DriverToolbar.IS_SECONDARY_ORDER_EXIST = false
    }

    fun isPrimaryOrderExist(): Boolean = primaryOrderId > 0

    fun isSecondaryOrderExist(): Boolean = secondaryOrderId > 0

    fun primaryOrderId(): Int = primaryOrderId

    fun secondaryOrderId(): Int = secondaryOrderId

    fun acceptOrder(orderInfo: NewOrderInfo) {
        orderInfo.order?.status = OrderStatus.ACCEPTED
        if (!isPrimaryOrderExist()) {
            addPrimaryOrderToDb(orderInfo)
        } else {
            addSecondaryOrderToDb(orderInfo)
        }
    }

    fun finishOrder() {
        DriverToolbar.IS_SECONDARY_ORDER_EXIST = false
        deletePrimaryOrder()
        if (isSecondaryOrderExist()) {
            primaryOrderId = secondaryOrderId
            secondaryOrderId = 0
        }
    }

    fun getOrdersCount(): Int = dbManager.queryOrdersQuantity()

    fun updateOrder(orderInfo: NewOrderInfo) {
        orderInfo.id = orderInfo.order!!.id!!
        dbManager.updateOrderInfo(orderInfo)
    }

    fun getOrders() = dbManager.queryAll()

    fun addCoordinates(orderInfo: OrderCoordinates) {
        dbManager.insertOrderCoordinates(orderInfo)
    }

    fun clearCoordinates() {
        dbManager.clearCoordinates()
    }

    fun getCoordinates(id: Int) = dbManager.queryOrderCoordinatesById(id)


    fun getOrderResult(id: Int) = dbManager.gueryOrderResult(id)


    fun queryFinalization() = dbManager.queryFinalization()

    fun updateFinalization(orderFinalization: OrderFinalization) = dbManager.updateFinalization(orderFinalization)

    fun insertFinalization(orderFinalization: OrderFinalization) = dbManager.insertFinalization(orderFinalization)
    fun deleteFinalization() = dbManager.deleteFinalization()
}