package com.wezom.taxi.driver.data.converter

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.wezom.taxi.driver.data.models.Card
import java.util.*


class UserConverter {

    var gson = Gson()

    @TypeConverter
    fun stringToSomeObjectList(data: String?): List<Card> {
        if (data == null) {
            return Collections.emptyList()
        }

        val listType = object : TypeToken<List<Card>>() {

        }.getType()

        return gson.fromJson(data, listType)
    }

    @TypeConverter
    fun someObjectListToString(someObjects: List<Card>): String {
        return gson.toJson(someObjects)
    }
}