package com.wezom.taxi.driver.net.websocket.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class DisableOnlineStatus(@SerializedName("isOnline") var isOnline: Boolean) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readByte() != 0.toByte())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeByte(if (isOnline) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DisableOnlineStatus> {
        override fun createFromParcel(parcel: Parcel): DisableOnlineStatus {
            return DisableOnlineStatus(parcel)
        }

        override fun newArray(size: Int): Array<DisableOnlineStatus?> {
            return arrayOfNulls(size)
        }
    }
}