package com.wezom.taxi.driver.presentation.base.route

import ru.terrakok.cicerone.result.ResultListener

/**
 * Created by zorin.a on 28.04.2018.
 */
interface Routable {
    fun switchScreen(screen: String, data: Any? = null)
    fun replaceScreen(screen: String, data: Any? = null)
    fun setRootScreen(screen: String, data: Any? = null)
    fun startScreenForResult(screen: String, data: Any? = null, resultListener: ResultListener, resultCode: Int)
    fun backWithResult(resultCode: Int, result: Any)
    fun removeResultListener(resultCode: Int)
    fun backToScreen(screen: String)

    fun onBackPressed()
}