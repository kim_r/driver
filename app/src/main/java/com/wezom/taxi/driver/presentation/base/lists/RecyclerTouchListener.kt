package com.wezom.taxi.driver.presentation.base.lists

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import javax.inject.Inject


/**
 * Created by zorin.a on 15.03.2018.
 */
class RecyclerTouchListener @Inject constructor(context: Context) :
        GestureDetector.SimpleOnGestureListener(), RecyclerView.OnItemTouchListener {

    private var gestureDetector: GestureDetector = GestureDetector(context, this)

    var listener: ClickListener? = null

    var rv: RecyclerView? = null

    override fun onSingleTapUp(e: MotionEvent?): Boolean = true

    override fun onLongPress(e: MotionEvent?) {
        val child = rv?.findChildViewUnder(e!!.x, e.y)
        if (child != null && listener != null && rv != null) {
            listener!!.onLongPress(rv!!.getChildAdapterPosition(child), child)
        }
    }

    override fun onTouchEvent(rv: RecyclerView?, e: MotionEvent?) {

    }

    override fun onInterceptTouchEvent(rv: RecyclerView?, e: MotionEvent?): Boolean {
        val child = rv?.findChildViewUnder(e!!.x, e.y)
        if (child != null && listener != null && gestureDetector.onTouchEvent(e) ) {
            listener!!.onPress(rv.getChildLayoutPosition(child), child)
        }
        return false
    }

    override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {

    }

    interface ClickListener {
        fun onPress(position: Int, view: View)
        fun onLongPress(position: Int, view: View)
    }
}