package com.wezom.taxi.driver.net.websocket.messages

import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.injection.annotation.SkipSerialization

/**
 * Created by zorin.a on 20.04.2018.
 */
data class LoginMessage constructor(@SkipSerialization override var room: String,
                        @SerializedName("token") val token: String) : BaseSocketMessage()
