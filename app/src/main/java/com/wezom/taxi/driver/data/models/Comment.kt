package com.wezom.taxi.driver.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by zorin.a on 14.03.2018.
 */
data class Comment(
        @SerializedName("id") val id: Int?,
        @SerializedName("userName") val userName: String?,
        @SerializedName("comment") val comment: String?,
        @SerializedName("rating") val rating: Float?,
        @SerializedName("time") val time: Long?
)