package com.wezom.taxi.driver.net.request

import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.Coordinate
import java.io.Serializable


/**
 * Created by zorin.a on 022 22.02.18.
 */

data class RefuseOrderRequest(
        @SerializedName("reasonId") val reasonId: Int?,
        @SerializedName("comment") val comment: String? = null,
        @SerializedName("status") val status: Int? = null,
        @SerializedName("factCost") val factCost: Int?,
        @SerializedName("actualCost") val actualCost: Int?,
        @SerializedName("factTime") val factTime: Long?,
        @SerializedName("factPath") val factPath: Double?,
        @SerializedName("eventTime") val eventTime: Long?,
        @SerializedName("route") val route: List<Coordinate>?):Serializable