package com.wezom.taxi.driver.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by zorin.a on 04.05.2018.
 */
enum class OrderStatus(val value: Int) {
    @SerializedName("0")
    NEW(0),
    @SerializedName("1")
    DIRECTED(1),
    @SerializedName("2")
    ACCEPTED(2),
    @SerializedName("3")
    ARRIVED_AT_PLACE(3),
    @SerializedName("4")
    ON_THE_WAY(4), // подобрал человека и выполняет ордер
    @SerializedName("5")
    NOT_COMPLETED(5),
    @SerializedName("6")
    COMPLETED(6),
    @SerializedName("7")
    TOTAL_COST(7)
}