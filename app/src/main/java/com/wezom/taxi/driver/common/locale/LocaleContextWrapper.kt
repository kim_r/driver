package com.wezom.taxi.driver.common.locale

import android.content.Context
import android.content.ContextWrapper

/**
 * Created by zorin.a on 30.03.2018.
 */
class LocaleContextWrapper(context: Context) : ContextWrapper(context) {
    companion object {
        fun wrap(base: Context) =  LocalizationUtil.getInstance(base).createWrapper()
    }
}