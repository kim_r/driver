package com.wezom.taxi.driver.presentation.customview

import android.animation.ValueAnimator
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.transition.TransitionManager
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import com.jakewharton.rxbinding2.view.RxView
import com.wezom.taxi.driver.data.models.Day
import com.wezom.taxi.driver.databinding.EqualizerBinding
import com.wezom.taxi.driver.ext.setVisible
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign


/**
 * Created by zorin.a on 21.03.2018.
 */

class GainsEqualizer : ConstraintLayout {

    //region var
    private var maxValue: Int? = null
    private var maximumHeight: Int? = null
    private val disposable = CompositeDisposable()
    private var lastCheckedPosition = UNCHECKED_POS
    private var data: List<Day>? = null
    private var indicators: MutableList<View>? = null
    private var columns: MutableList<View>? = null
    var listener: OnGainClickListener? = null
    private var binding = EqualizerBinding.inflate(LayoutInflater.from(context), this, true)

    //endregion
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, attributeSetId: Int) : super(context, attrs, attributeSetId)

    init {
        setupView()
    }

    //region override
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val parentWidth = View.MeasureSpec.getSize(widthMeasureSpec)
        val parentHeight = View.MeasureSpec.getSize(heightMeasureSpec)
        setMeasuredDimension(parentWidth, parentHeight)
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        maximumHeight = parentHeight
    }

    override fun onDetachedFromWindow() {
        disposable.dispose()
        super.onDetachedFromWindow()
    }
    //endregion

    //region fun
    fun setDataRange(days: List<Day>?) {
        data = days
        if (moneyExists(days!!) == true) {
            reset()
            updateUi(data)
        } else {
            data = null
            reset()
            setScaleVisibility(listOf())
        }
    }

    private fun moneyExists(days: List<Day>?): Boolean? {
        val result = days?.filter { it.totalIncome!! > 0 }
        return result?.isNotEmpty()
    }

    private fun setScaleVisibility(isHighVisible: Boolean, isMediumVisible: Boolean, isLowVisible: Boolean) {
        binding.run {
            highValue.setVisible(isHighVisible)
            mediumValue.setVisible(isMediumVisible)
            lowValue.setVisible(isLowVisible)
        }
    }

    private fun updateUi(list: List<Day>?) {
        binding.run {
            setScaleVisibility(list)
            if (lastCheckedPosition != UNCHECKED_POS) {
                switchIndicator(lastCheckedPosition)
            }

            maxValue = list?.maxWith(Comparator { p0, p1 ->
                p0!!.totalIncome!! - p1!!.totalIncome!!
            })?.totalIncome

            if (maxValue == null) maxValue = 0
            highValue.text = maxValue.toString()
            mediumValue.text = ((maxValue!!.times(0.66)).toInt()).toString()
            lowValue.text = ((maxValue!!.times(0.33)).toInt()).toString()


            list?.forEach { day ->
                val dayIndex = day.dayOfWeek?.minus(1)
                setupByDay(dayIndex!!, day.totalIncome!!)
            }
        }
    }

    private fun setScaleVisibility(list: List<Day>?) {
        when (list?.size) {
            0 -> {
                setScaleVisibility(false, false, false)
            }
            1 -> {
                setScaleVisibility(true, false, false)
            }
            2 -> {
                setScaleVisibility(true, false, true)
            }
            else -> {
                setScaleVisibility(true, true, true)
            }
        }
    }

    private fun reset() {
        maxValue = 0
        columns?.forEach { column ->
            if (column.layoutParams.height > 1) {
                val animator = ValueAnimator.ofInt(column.height, 1)
                animator.addUpdateListener { animator ->
                    val value = animator!!.animatedValue as Int
                    val params = column.layoutParams
                    params.height = value
                    column.layoutParams = params
                }
                animator.interpolator = AccelerateDecelerateInterpolator()
                animator.duration = 500
                animator.start()
            }
        }
        uncheckIndicator()
        disposable.clear()
    }

    private fun setupByDay(dayIndex: Int, income: Int) {
        binding.run {
            val tmp = data?.find { day -> day.dayOfWeek == dayIndex.plus(1) }
            when (dayIndex) {
                FIRST_POS -> {
                    setColumnValue(firstColumn, income)

                    disposable += RxView.clicks(firstDayContainer).subscribe({
                        switchIndicator(FIRST_POS)
                        listener?.onItemClick(isChecked(), tmp)
                    })
                    disposable += RxView.clicks(firstColumn).subscribe({
                        switchIndicator(FIRST_POS)
                        listener?.onItemClick(isChecked(), tmp)
                    })
                }
                SECOND_POS -> {
                    setColumnValue(secondColumn, income)

                    disposable += RxView.clicks(secondDayContainer).subscribe({
                        switchIndicator(SECOND_POS)
                        listener?.onItemClick(isChecked(), tmp)
                    })
                    disposable += RxView.clicks(secondColumn).subscribe({
                        switchIndicator(SECOND_POS)
                        listener?.onItemClick(isChecked(), tmp)
                    })
                }
                THIRD_POS -> {
                    setColumnValue(thirdColumn, income)

                    disposable += RxView.clicks(thirdDayContainer).subscribe({
                        switchIndicator(THIRD_POS)
                        listener?.onItemClick(isChecked(), tmp)
                    })
                    disposable += RxView.clicks(thirdColumn).subscribe({
                        switchIndicator(THIRD_POS)
                        listener?.onItemClick(isChecked(), tmp)
                    })
                }
                FOURTH_POS -> {
                    setColumnValue(fourthColumn, income)

                    disposable += RxView.clicks(fourthDayContainer).subscribe({
                        switchIndicator(FOURTH_POS)
                        listener?.onItemClick(isChecked(), tmp)
                    })
                    disposable += RxView.clicks(fourthColumn).subscribe({
                        switchIndicator(FOURTH_POS)
                        listener?.onItemClick(isChecked(), tmp)
                    })
                }
                FIFTH_POS -> {
                    setColumnValue(fifthColumn, income)

                    disposable += RxView.clicks(fifthDayContainer).subscribe({
                        switchIndicator(FIFTH_POS)
                        listener?.onItemClick(isChecked(), tmp)
                    })
                    disposable += RxView.clicks(fifthColumn).subscribe({
                        switchIndicator(FIFTH_POS)
                        listener?.onItemClick(isChecked(), tmp)
                    })
                }
                SIXTH_POS -> {
                    setColumnValue(sixthColumn, income)

                    disposable += RxView.clicks(sixthDayContainer).subscribe({
                        switchIndicator(SIXTH_POS)
                        listener?.onItemClick(isChecked(), tmp)
                    })
                    disposable += RxView.clicks(sixthColumn).subscribe({
                        switchIndicator(SIXTH_POS)
                        listener?.onItemClick(isChecked(), tmp)
                    })
                }
                SEVENTH_POS -> {
                    setColumnValue(seventhColumn, income)

                    disposable += RxView.clicks(seventhDayContainer).subscribe({
                        switchIndicator(SEVENTH_POS)
                        listener?.onItemClick(isChecked(), tmp)
                    })
                    disposable += RxView.clicks(seventhColumn).subscribe({
                        switchIndicator(SEVENTH_POS)
                        listener?.onItemClick(isChecked(), tmp)
                    })
                }
            }
        }
    }

    private fun setColumnValue(column: View, dayIncome: Int) {
        if (dayIncome <= 0) {
            column.visibility = View.INVISIBLE
            return
        } else {
            column.setVisible(true)
        }

        val percentOfMax = dayIncome.times(100).div(maxValue!!)
        val columnHeight = maximumHeight!!.times(percentOfMax).div(100)
        val animator = ValueAnimator.ofInt(column.height, columnHeight)
        animator.addUpdateListener { animator ->
            val value = animator!!.animatedValue as Int
            val params = column.layoutParams
            if (columnHeight <= 0) params.height = 1 else params.height = value
            column.layoutParams = params
        }
        animator.interpolator = AccelerateDecelerateInterpolator()
        animator.duration = 500
        animator.start()
    }

    private fun setupView() {
        indicators = mutableListOf()
        columns = mutableListOf()
        binding.run {
            indicators!!.add(firstDayIndicator)
            indicators!!.add(secondDayIndicator)
            indicators!!.add(thirdDayIndicator)
            indicators!!.add(fourthDayIndicator)
            indicators!!.add(fifthDayIndicator)
            indicators!!.add(sixthDayIndicator)
            indicators!!.add(seventhDayIndicator)

            columns!!.add(firstColumn)
            columns!!.add(secondColumn)
            columns!!.add(thirdColumn)
            columns!!.add(fourthColumn)
            columns!!.add(fifthColumn)
            columns!!.add(sixthColumn)
            columns!!.add(seventhColumn)
        }
    }

    private fun isChecked(): Boolean = lastCheckedPosition != UNCHECKED_POS

    private fun switchIndicator(position: Int) {
        TransitionManager.beginDelayedTransition(binding.root)
        lastCheckedPosition = if (lastCheckedPosition == position) {
            indicators!![position].setVisible(false)
            UNCHECKED_POS
        } else {
            if (lastCheckedPosition != UNCHECKED_POS) {
                indicators!![lastCheckedPosition].setVisible(false)
            }
            indicators!![position].setVisible(true)
            position
        }
    }

    private fun uncheckIndicator() {
        if (lastCheckedPosition != UNCHECKED_POS) {
            indicators!![lastCheckedPosition].setVisible(false)
            lastCheckedPosition = UNCHECKED_POS
        }
    }

    //endregion
    interface OnGainClickListener {
        fun onItemClick(isChecked: Boolean, day: Day?)
    }

    companion object {
        private const val UNCHECKED_POS = -1
        private const val FIRST_POS = 0
        private const val SECOND_POS = 1
        private const val THIRD_POS = 2
        private const val FOURTH_POS = 3
        private const val FIFTH_POS = 4
        private const val SIXTH_POS = 5
        private const val SEVENTH_POS = 6
    }
}