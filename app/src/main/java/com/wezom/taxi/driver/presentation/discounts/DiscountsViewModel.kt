package com.wezom.taxi.driver.presentation.discounts

import android.arch.lifecycle.MutableLiveData
import com.wezom.taxi.driver.data.models.ResponseState
import com.wezom.taxi.driver.data.models.ResponseState.State.*
import com.wezom.taxi.driver.net.response.DiscountsResponse
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.repository.DriverRepository
import io.reactivex.rxkotlin.plusAssign
import javax.inject.Inject

/**
 * Created by udovik.s on 15.03.2018.
 */
class DiscountsViewModel @Inject constructor(screenRouterManager: ScreenRouterManager, private val repository: DriverRepository) : BaseViewModel(screenRouterManager) {
    val discountsLiveData: MutableLiveData<DiscountsResponse> = MutableLiveData()

    fun getDiscounts() {
        loadingLiveData.postValue(ResponseState(LOADING))
        App.instance.getLogger()!!.log("getDiscounts start ")
        disposable += repository.getDiscounts().subscribe({ response ->
            handleResponseState(response)
            App.instance.getLogger()!!.log("getDiscounts suc ")
            if (response?.isSuccess!!) {
                discountsLiveData.postValue(response)
            }
        }, {
            App.instance.getLogger()!!.log("getDiscounts error ")
            loadingLiveData.postValue(ResponseState(NETWORK_ERROR))
        })
    }
}