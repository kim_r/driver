package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName

/**
 * Created by zorin.a on 08.06.2018.
 */
class UserOnlineStatusResponse constructor(@SerializedName("isOnline") val isOnline: Boolean) : BaseResponse()