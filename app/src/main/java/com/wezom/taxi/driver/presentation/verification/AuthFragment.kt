package com.wezom.taxi.driver.presentation.verification

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.annotation.RequiresApi
import android.support.constraint.ConstraintSet
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.support.transition.TransitionManager
import android.support.v7.app.AlertDialog
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.*
import android.widget.EditText
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.bus.events.BadInternetEvent
import com.wezom.taxi.driver.common.intToMinutesAndSeconds
import com.wezom.taxi.driver.databinding.AuthFragmentBinding
import com.wezom.taxi.driver.ext.*
import com.wezom.taxi.driver.presentation.base.BaseFragment
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.util.concurrent.TimeUnit


private const val PHONE_MASK = "--- --- -- --"

class AuthFragment : BaseFragment() {

    private lateinit var binding: AuthFragmentBinding
    private lateinit var viewModel: AuthViewModel

    private enum class State {
        DEFAULT, SELECTED, AWAITING_SMS
    }

    private lateinit var notSelectedSet: ConstraintSet
    private lateinit var selectedSet: ConstraintSet
    private lateinit var smsSet: ConstraintSet

    private val psSendCode = PublishSubject.create<Int>()
    private val psSendPhone = PublishSubject.create<Int>()

    //    private var smsReceiver: BroadcastReceiver? = null
    private var state = State.DEFAULT

    private val phoneNumberChangeListener: TextWatcher by lazy {
        object : TextWatcher {
            private var characterAction = -1
            private var actionPosition = 1
            private var ignoreOnPhoneChange = false

            override fun afterTextChanged(s: Editable) {
                if (ignoreOnPhoneChange)
                    return

                binding.run {
                    var start = phoneInput.selectionStart
                    val phoneChars = "0123456789"
                    var str = phoneInput.text.toString()
                    if (characterAction == 3) {
                        str = str.substring(0, actionPosition) +
                                str.substring(actionPosition + 1, str.length)
                        start--
                    }

                    val builder = StringBuilder(str.length)
                    str.forEachIndexed { index, _ ->
                        val ch = str.substring(index, index + 1)
                        if (phoneChars.contains(ch))
                            builder.append(ch)
                    }
                    ignoreOnPhoneChange = true
                    var i = 0
                    while (i < builder.length) {
                        if (i < PHONE_MASK.length) {
                            if (PHONE_MASK[i] == ' ') {
                                builder.insert(i, ' ')
                                i++
                                if (start == i && characterAction != 2 && characterAction != 3) {
                                    start++
                                }
                            }
                            i++
                        } else {
                            builder.insert(i, ' ')
                            if (start == i && characterAction != 2 && characterAction != 3) {
                                start++
                            }
                            break
                        }
                    }

                    phoneInput.setText(builder.toString())

                    if (start >= 0) {
                        val selection =
                                if (start <= phoneInput.length()) start else phoneInput.length()

                        phoneInput.setSelection(selection)
                    }
                }



                ignoreOnPhoneChange = false
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                if (count == 0 && after == 1) {
                    characterAction = 1
                } else if (count == 1 && after == 0) {
                    if (s[start] == ' ' && start > 0) {
                        characterAction = 3
                        actionPosition = start - 1
                    } else {
                        characterAction = 2
                    }
                } else {
                    characterAction = -1
                }
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        }
    }

//    private val smsPermissionRationale: AlertDialog by lazy {
//        AlertDialog.Builder(context as Context).apply {
//            setCancelable(false)
//            setTitle(R.string.request_sms_permission_title)
//            setMessage(R.string.request_sms_persmission_body)
//            setPositiveButton(R.string.permit) { _, _ -> requestSmsPermission() }
//            setNegativeButton(R.string.cancel) { _, _ ->
//                navigateToSms()
//                requestAuth()
//            }
//        }.create()
//    }

    private val needPermissionDialog: AlertDialog by lazy {
        AlertDialog.Builder(context as Context).apply {
            setMessage(R.string.need_permission)
            setPositiveButton(R.string.ok) { self, _ -> self.dismiss() }
        }.create()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
        setBackPressFun { viewModel.onBackPressed() }
//        initSmsReceiver()
        viewModel.drawerLockLiveData.observe(this@AuthFragment, Observer { locked ->
            Timber.d("isLocked auth observer: $locked")
            lockDrawer(locked!!)
        })
        viewModel.loadingLiveData.observe(this, progressObserver)
        viewModel.switchToSmsLiveData.observe(this, Observer {
            navigateToSms()
        })

//        context?.registerReceiver(smsReceiver, IntentFilter(SMS_RECEIVED_ACTION))

        Timber.d("isLocked auth onViewCreated: true")
        lockDrawer(true)

        initPublishSubjects()
    }

    @SuppressLint("CheckResult")
    private fun initPublishSubjects() {
        psSendCode
                .throttleFirst(2, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                .subscribe {
                    sendCode()
//                    getString(R.string.sms_sent).longToast(context!!)
                }
        psSendPhone
                .throttleFirst(2, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                .subscribe({
                    viewModel.sendPhone(binding.run { getPhoneNumber() })
                }
                )
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = AuthFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING)
        state = State.DEFAULT
        Handler().postDelayed({
            RxBus.publish(BadInternetEvent(true))
        }
                , 1000
        )

        initConstraintSets()

        binding.run {
            phoneInput.run {
                isHorizontalFadingEdgeEnabled = false
                isHorizontalScrollBarEnabled = false
                addTextChangedListener(phoneNumberChangeListener)
                setOnClickListener {
                    handleInputAction()
                }
            }

            phoneInput.setOnFocusChangeListener { _, focused ->
                if (focused) handleInputAction()
            }

            phoneInputLayout.setOnClickListener {
                handleInputAction()
            }

            setAsHomeUp(toolbar) { _ ->
                applyTransition(State.DEFAULT)
                clearSmsCodeErrors()
                clearSmsCodeInputs()
                phoneInput.clearFocus()

            }
            setServiceCodeListeners()


            next.setOnClickListener {
                when (state) {
                    State.DEFAULT -> validatePhoneNumber()
                    State.SELECTED -> {
                        validatePhoneNumber()
                    }
                    State.AWAITING_SMS -> {
                        psSendCode.onNext(1)
                    }
                }
            }

            resendSms.setOnClickListener {
                viewModel.launchSmsDelayTimer()
                requestAuth()
            }

            viewModel.smsDelayLiveData.observe(this@AuthFragment, Observer<Int> {
                it?.let {
                    if (it == 0) {
                        resendIn.setVisible(false)
                        resendSms.setVisible(true)
                    } else {
                        resendSms.setVisible(false)
                        resendIn.run {
                            if (visibility != View.VISIBLE) {
                                alpha = 0f
                                this.setVisible(true)
                                animate()
                                        .alpha(1f)
                                        .duration = 1000
                            }
                            text = getString(R.string.resend_in, intToMinutesAndSeconds(it))
                        }
                    }
                }
            })

        }
    }

    override fun onDestroy() {
        super.onDestroy()
//        context?.unregisterReceiver(smsReceiver)
//        smsReceiver = null
    }

//    private fun initSmsReceiver() {
//        smsReceiver = object : BroadcastReceiver() {
//            override fun onReceive(context: Context?, intent: Intent?) {
//                for (message in Telephony.Sms.Intents.getMessagesFromIntent(intent)) {
//                    Timber.d("Received sms: message sender: ${message.displayOriginatingAddress}; message body: ${message.displayMessageBody}")
//
//                    if (message.displayOriginatingAddress == ALLOWED_SENDER) {
//                        val pattern = Pattern.compile("(\\d{4})")
//                        val matcher = pattern.matcher(message.displayMessageBody)
//
//                        if (matcher.find()) {
//                            val digits = matcher.group(1)
//                            Timber.d("Found: $digits")
//                            val first = digits[0]
//                            val second = digits[1]
//                            val third = digits[2]
//                            val fourth = digits[3]
//                            binding.run {
//                                smsCodeFirst.setText(first.toString())
//                                smsCodeSecond.setText(second.toString())
//                                smsCodeThird.setText(third.toString())
//                                smsCodeFourth.setText(fourth.toString())
//                            }
//                        }
//                    }
//                }
//
//            }
//        }
//    }

    private fun validatePhoneNumber() {
        val number =
                binding.phoneInput.text
                        .toString()
                        .replace("\\s+".toRegex(), "")
        if (number.length != 10)
            binding.phoneInputLayout.error = getString(R.string.invalid_phone_number)
        else {
            checkSmsPermissionAndSendAuth()
        }
    }

    private fun checkSmsPermissionAndSendAuth() {
//        if (!checkPermissionGranted(android.Manifest.permission.READ_SMS)) {
//            smsPermissionRationale.show()
//        } else {
//            binding.phoneInputLayout.error = null
//            requestAuth()
//        }

        binding.phoneInputLayout.error = null
        requestAuth()
    }

    private fun initConstraintSets() {
        notSelectedSet = ConstraintSet().apply { clone(binding.container) }
        selectedSet = ConstraintSet().apply { clone(context, R.layout.fragment_auth_enter_phone_focused) }
        smsSet = ConstraintSet().apply { clone(context, R.layout.fragment_auth_enter_code) }
    }

    private fun getCompleteCode(vararg edits: EditText): Int {
        val builder = StringBuilder()
        edits.forEach {
            builder.append(it.text.toString())
        }

        return builder.toString().toInt()
    }

    private fun sendCode() {
        if (validateSmsCodeInput()) {
            activity.hideKeyboard()
            with(binding) {
                viewModel.sendSmsCode(
                        getPhoneNumber(),
                        getCompleteCode(
                                smsCodeFirst,
                                smsCodeSecond,
                                smsCodeThird,
                                smsCodeFourth
                        )
                )

//                smsCodeFirst.setText("")
//                smsCodeSecond.setText("")
//                smsCodeThird.setText("")
//                smsCodeFourth.setText("")
            }
        }
    }


    private fun AuthFragmentBinding.getPhoneNumber() =
            "${countryCodeLabel.text}${phoneInput.text}".replace("\\s".toRegex(), "")

    private fun clearSmsCodeErrors() {
        binding.run {
            applyToEach(
                    { it.error = null },
                    smsCodeFirst,
                    smsCodeSecond,
                    smsCodeThird,
                    smsCodeFourth
            )
        }
    }

    private fun setAsHomeUp(toolbar: Toolbar, callback: ((View) -> Unit)?) {
        toolbar.run {
            navigationIcon = resources.getDrawable(R.drawable.ic_back, null)
            setNavigationOnClickListener(callback)
            title = null
        }
    }

    private fun clearSmsCodeInputs() {
        binding.run {
            applyToEach(
                    { it.setText("") },
                    smsCodeFirst,
                    smsCodeSecond,
                    smsCodeThird,
                    smsCodeFourth
            )
        }
    }

    private fun applyToEach(command: (EditText) -> Unit, vararg edits: EditText) {
        edits.forEach {
            command(it)
        }
    }

    private fun handleInputAction() {
        when (state) {
            State.DEFAULT -> {
                applyTransition(State.SELECTED)
                binding.phoneInput.requestFocus()
                activity.showKeyboard()
            }

            State.SELECTED -> {
            }

            State.AWAITING_SMS -> {
            }
        }
    }

    private fun setServiceCodeListeners() {
        binding.run {
            disposable += RxTextView
                    .textChanges(smsCodeFirst)
                    .subscribe { s: CharSequence ->
                        if (s.isNotEmpty()) {
                            smsCodeFirstContainer.isErrorEnabled = false
                            binding.smsCodeSecond.requestFocus()
                        }
                    }
            disposable += RxTextView
                    .textChanges(smsCodeSecond)
                    .subscribe { s: CharSequence ->
                        if (s.isNotEmpty()) {
                            smsCodeSecondContainer.isErrorEnabled = false
                            smsCodeThird.requestFocus()
                        }
                    }
            disposable += RxTextView
                    .textChanges(smsCodeThird)
                    .subscribe { s: CharSequence ->
                        if (s.isNotEmpty()) {
                            smsCodeThirdContainer.isErrorEnabled = false
                            smsCodeFourth.requestFocus()
                        }
                    }
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            setupInputFilters()
        else {
            setupKeyListeners()
            setupFocusChangeListeners()
        }
    }

    private fun setupFocusChangeListeners() {
        binding.run {
            smsCodeFirst.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus)
                    smsCodeFirst.setText("")
            }
            smsCodeSecond.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus)
                    smsCodeSecond.setText("")
            }
            smsCodeThird.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus)
                    smsCodeThird.setText("")
            }
            smsCodeFourth.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus)
                    smsCodeFourth.setText("")
            }
        }
    }

    private fun setupInputFilters() = binding.run {
        smsCodeSecond.filters =
                arrayOf(InputFilter { source, _, _, _, _, dend ->
                    if (dend == 0 && source.isEmpty()) {
                        smsCodeFirst.requestFocus()
                    }

                    source
                }, InputFilter.LengthFilter(1))

        smsCodeThird.filters =
                arrayOf(InputFilter { source, _, _, _, _, dend ->
                    if (dend == 0 && source.isEmpty()) {
                        smsCodeSecond.requestFocus()
                    }

                    source
                }, InputFilter.LengthFilter(1))

        smsCodeFourth.filters =
                arrayOf(InputFilter { source, _, _, _, _, dend ->
                    if (dend == 0 && source.isEmpty()) {
                        smsCodeThird.requestFocus()
                    }

                    source
                }, InputFilter.LengthFilter(1))
    }

    @RequiresApi(23)
    private fun setupKeyListeners() = binding.run {
        smsCodeSecond.setOnKeyListener { v, keyCode, event ->
            handleBackspace(v, keyCode, event, smsCodeFirst)
        }

        smsCodeThird.setOnKeyListener { v, keyCode, event ->
            handleBackspace(v, keyCode, event, smsCodeSecond)
        }

        smsCodeFourth.setOnKeyListener { v, keyCode, event ->
            handleBackspace(v, keyCode, event, smsCodeThird)
        }
    }

    private fun handleBackspace(
            v: View,
            keyCode: Int,
            event: KeyEvent,
            nextToReceiveFocus: EditText
    ): Boolean {
        if (keyCode == KeyEvent.KEYCODE_DEL
                && ((v as EditText).length() == 0)
                && event.action == KeyEvent.ACTION_DOWN) nextToReceiveFocus.requestFocus()
        return false
    }

    private fun applyTransition(newState: State) {
        TransitionManager.beginDelayedTransition(binding.container)

        val currentFocus = activity?.currentFocus
        val setToApply = when (newState) {
            State.DEFAULT -> {
                if (currentFocus != null) {
                    activity.hideKeyboard()
                }
                notSelectedSet
            }

            State.SELECTED -> {
                activity.showKeyboard()
                selectedSet
            }

            State.AWAITING_SMS -> {
                smsSet
            }
        }

        setToApply.applyTo(binding.container)
        state = newState
    }

    private fun isPhoneLengthValid(): Boolean {
        val number =
                binding.phoneInput.text
                        .toString()
                        .replace("\\s+".toRegex(), "")
        return number.length == 9
    }

    private fun validateSmsCodeInput(): Boolean {
        binding.run {
            val first = isNotEmptyInput(smsCodeFirstContainer, smsCodeFirst)
            val second = isNotEmptyInput(smsCodeSecondContainer, smsCodeSecond)
            val third = isNotEmptyInput(smsCodeThirdContainer, smsCodeThird)
            val fourth = isNotEmptyInput(smsCodeFourthContainer, smsCodeFourth)
            return first && second && third && fourth
        }
    }

    private fun isNotEmptyInput(container: TextInputLayout, edit: TextInputEditText): Boolean {
        if (edit.text.isEmpty()) {
            container.isErrorEnabled = true
            container.error = " "
            return false
        }

        return true
    }

    private fun navigateToSms() {
        Timber.d("navigateToSms()")
        binding.run {
            clearSmsCodeInputs()
            resendSms.setVisible(false)
            resendIn.setVisible(false)

            applyTransition(State.AWAITING_SMS)
            smsCodeFirst.requestFocus()
        }
        activity.showKeyboard()
    }

//    private fun requestSmsPermission() = requestPermissions(
//            arrayOf(android.Manifest.permission.READ_SMS),
//            SMS_REQUEST_CODE
//    )

    private fun requestAuth() {
        binding.run {
            //            viewModel.sendPhone(getPhoneNumber())
            psSendPhone.onNext(1)
        }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray
    ) {
        grantResults.forEachIndexed { i, result ->
            if (result == PackageManager.PERMISSION_DENIED) {
                if (!shouldShowRequestPermissionRationale(permissions[i])) {
                    needPermissionDialog.show()
                }
                return
            }
        }
        when (requestCode) {
            SMS_REQUEST_CODE -> requestAuth()
        }
    }

    companion object {
        val TAG = this::class.java.simpleName
        const val SMS_REQUEST_CODE = 1122

        const val ALLOWED_SENDER = "Hub"

    }

}

