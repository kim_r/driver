package com.wezom.taxi.driver.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by zorin.a on 05.07.2018.
 */
enum class SendHelpTimeType {
    @SerializedName("0")
    PHONE,
    @SerializedName("1")
    MESSAGE
}