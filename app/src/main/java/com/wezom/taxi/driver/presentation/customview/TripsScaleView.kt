package com.wezom.taxi.driver.presentation.customview

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import com.wezom.taxi.driver.data.models.ActionTripsInfo
import com.wezom.taxi.driver.databinding.TripsScaleViewBinding

/**
 * Created by zorin.a on 3/11/19.
 */
class TripsScaleView : ConstraintLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context,
                attrs: AttributeSet?) : super(context,
                                              attrs)

    constructor(context: Context,
                attrs: AttributeSet?,
                attrsId: Int) : super(context,
                                      attrs,
                                      attrsId)

    private val binding = TripsScaleViewBinding
            .inflate(LayoutInflater.from(context),
                     this,
                     true)

    fun setData(data: ActionTripsInfo) {
        if (data.conditions.isNullOrEmpty()) return
        val values = mutableListOf<Int>()
        data.conditions!!.forEach {
            values.add(it.tripsCount!!)
        }

        binding.run {
            tripsCountValue.text = data.completedTripsDone.toString()
            progress.setData(values,
                             data.completedTripsDone)
        }
    }
}