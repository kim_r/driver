package com.wezom.taxi.driver.data.models

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.io.Serializable


/**
 * Created by zorin.a on 01.08.2018.
 */
@Entity(tableName = "order_finalization")
data class OrderFinalization(
        @PrimaryKey
        @SerializedName("id") var id: Int,
        @Embedded(prefix = "accepted") @SerializedName("accepted") var accepted: Accepted?,
        @Embedded(prefix = "arrived") @SerializedName("arrived_at_place") var arrivedAtPlace: ArrivedAtPlace?,
        @Embedded(prefix = "way") @SerializedName("on_the_way") var onTheWay: OnTheWay?,
        @Embedded(prefix = "total") @SerializedName("total_cost") var totalCost: TotalCost?,
        @Embedded(prefix = "fo") @SerializedName("finish_order") var finishOrder: FinishOrder?
) : Serializable, Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readParcelable(Accepted::class.java.classLoader),
            parcel.readParcelable(ArrivedAtPlace::class.java.classLoader),
            parcel.readParcelable(OnTheWay::class.java.classLoader),
            parcel.readParcelable(TotalCost::class.java.classLoader),
            parcel.readParcelable(FinishOrder::class.java.classLoader))

    constructor() : this(id = 1, accepted = Accepted(), arrivedAtPlace = ArrivedAtPlace(), onTheWay = OnTheWay(), totalCost = TotalCost(), finishOrder = FinishOrder())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeParcelable(accepted, flags)
        parcel.writeParcelable(arrivedAtPlace, flags)
        parcel.writeParcelable(onTheWay, flags)
        parcel.writeParcelable(totalCost, flags)
        parcel.writeParcelable(finishOrder, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OrderFinalization> {
        override fun createFromParcel(parcel: Parcel): OrderFinalization {
            return OrderFinalization(parcel)
        }

        override fun newArray(size: Int): Array<OrderFinalization?> {
            return arrayOfNulls(size)
        }
    }
}