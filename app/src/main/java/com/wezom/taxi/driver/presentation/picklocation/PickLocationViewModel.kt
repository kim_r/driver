package com.wezom.taxi.driver.presentation.picklocation

import android.annotation.SuppressLint
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.LiveDataReactiveStreams
import com.wezom.taxi.driver.common.PICK_LOCATION_MAP_SCREEN
import com.wezom.taxi.driver.data.models.ResponseState
import com.wezom.taxi.driver.net.api.ApiManager
import com.wezom.taxi.driver.net.response.AutoCompleteResult
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.presentation.filter.CreateFilterViewModel
import ru.terrakok.cicerone.result.ResultListener
import timber.log.Timber
import javax.inject.Inject

/**
 *Created by Zorin.A on 31.July.2019.
 */
class PickLocationViewModel @Inject constructor(screenRouterManager: ScreenRouterManager,
                                                private val apiManager: ApiManager) : BaseViewModel(screenRouterManager) {

    fun navigateToMap(){
        startScreenForResult(screen = PICK_LOCATION_MAP_SCREEN,
                resultListener = ResultListener {
                    Timber.d("GOT $it")
                    val data: AutoCompleteResult = it as AutoCompleteResult
                    removeResultListener(CreateFilterViewModel.RESULT_CODE_MAP)
                    backWithResult(CreateFilterViewModel.RESULT_CODE,
                            data )
                },
                resultCode = CreateFilterViewModel.RESULT_CODE_MAP)
    }



    @SuppressLint("CheckResult")
    fun queryAutoComplete(input: String,
                          lat: Double? = 0.0,
                          lon: Double? = 0.0): LiveData<List<AutoCompleteResult>> {
        loadingLiveData.postValue(ResponseState(ResponseState.State.LOADING))
        Timber.d("NEW_TYPED_VALUE: $input")
        App.instance.getLogger()!!.log("autocomplete start amd end")
        return LiveDataReactiveStreams.fromPublisher(apiManager.autocomplete(input,
                                                                             lat,
                                                                             lon).doAfterSuccess {
            Timber.d("SUCCESS!!!")
            loadingLiveData.postValue(ResponseState(ResponseState.State.IDLE))
        }

                                                         .doOnError {
                                                             Timber.d("ERROR: ${it.message}")
                                                             loadingLiveData.postValue(ResponseState(ResponseState.State.NETWORK_ERROR))
                                                         }.toFlowable())

    }
}