package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.ActionCondition
import com.wezom.taxi.driver.data.models.ActionTripsInfo


/**
 * Created by zorin.a on 3/11/19.
 */
data class CurrendDiscountResponse constructor(
        @SerializedName("action_conditions")
        val actionConditions: List<ActionCondition>?,
        @SerializedName("action_trips_info")
        val actionTripsInfo: ActionTripsInfo?,
        @SerializedName("description")
        val description: String?
) : BaseResponse()


