package com.wezom.taxi.driver.presentation.main

import android.arch.lifecycle.MutableLiveData
import android.content.Intent
import android.content.SharedPreferences
import android.location.Location
import android.util.Log
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.common.*
import com.wezom.taxi.driver.data.models.AppState
import com.wezom.taxi.driver.data.models.ChangedParams
import com.wezom.taxi.driver.data.models.NewOrderInfo
import com.wezom.taxi.driver.data.models.ResponseState
import com.wezom.taxi.driver.data.models.RestoredData
import com.wezom.taxi.driver.data.models.notifications.*
import com.wezom.taxi.driver.ext.*
import com.wezom.taxi.driver.net.api.ApiManager
import com.wezom.taxi.driver.net.jobqueue.JobFactory
import com.wezom.taxi.driver.net.jobqueue.jobs.DataSynchronizationJob
import com.wezom.taxi.driver.net.jobqueue.jobs.LogoutJob
import com.wezom.taxi.driver.net.request.SetOnlineStatusRequest
import com.wezom.taxi.driver.net.request.TotalCostRequest
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.presentation.customview.DriverToolbar
import com.wezom.taxi.driver.presentation.main.events.DisableOnlineStatusEvent
import com.wezom.taxi.driver.presentation.main.events.SocketServiceResponseEvent
import com.wezom.taxi.driver.presentation.profile.ProfileFragment
import com.wezom.taxi.driver.service.LocationService
import com.wezom.taxi.driver.service.SocketService
import com.wezom.taxi.driver.service.TaxometerService
import com.wezom.taxi.driver.service.notification.NotificationType
import com.wezom.taxi.driver.service.notification.PushNotificationItemResolver
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber
import java.lang.ref.WeakReference
import javax.inject.Inject

/**
 * Created by zorin.a on 16.02.2018.
 */

class MainViewModel @Inject constructor(screenRouterManager: ScreenRouterManager,
                                        sharedPreferences: SharedPreferences,
                                        private val apiManager: ApiManager,
                                        private val locationStorage: LocationStorage,
                                        private val orderManager: OrderManager,
                                        private val resolver: PushNotificationItemResolver) : BaseViewModel(screenRouterManager) {

    private var token: String by sharedPreferences.string(TOKEN)
    private var userImageString by sharedPreferences.string(USER_IMAGE_URI)
    private var userImageForPrefs: String by sharedPreferences.string(USER_IMAGE_FOR_PREFS)
    private var carImageString by sharedPreferences.string(CAR_IMAGE_URI)
    private var userNameCached by sharedPreferences.string(USER_NAME_CACHED)
    private var userRatingCached by sharedPreferences.string(USER_RATING_CACHED)
    private var userPhone by sharedPreferences.string(USER_PHONE)
    private var firebaseToken by sharedPreferences.string(FIREBASE_TOKEN_KEY)

    private var doc1 by sharedPreferences.string(FIRST_DOC_URI)
    private var doc2 by sharedPreferences.string(SECOND_DOC_URI)
    private var doc3 by sharedPreferences.string(THIRD_DOC_URI)
    private var doc4 by sharedPreferences.string(FOURTH_DOC_URI)

    var background: Boolean by sharedPreferences.boolean(BACKGROUND, false)
    var timer: Boolean by sharedPreferences.boolean(TIMER, true)

    private var isCardAdded: Boolean by sharedPreferences.boolean(IS_CARD_ADDED)
    private var isNeedComplete: Boolean by sharedPreferences.boolean(IS_NEED_COMPLETE)


    var userImageLiveData = MutableLiveData<String>()
    var userNameLiveData = MutableLiveData<String>()
    var userRatingLiveData = MutableLiveData<String>()
    var lockDrawerLiveData = MutableLiveData<Boolean>()
//    var pushModelLiveData = MutableLiveData<BasicNotification>()

    fun setStatusBackground(b: Boolean) {
        background = b
        timer = true
    }

    fun setInitScreen() {
        setRootScreen(SPLASH_SCREEN)
    }

    fun switchToMainScreen() {
        if (orderManager.isPrimaryOrderExist() && orderManager.getPrimaryOrder() != null) {
            restoreOrder(orderManager.getPrimaryOrder()!!, RestoredData(ChangedParams.NO_CHANGES,
                    orderManager.getPrimaryOrder()!!.order?.actualCost ?: 0))
        } else {
            setRootScreen(MAIN_MAP_SCREEN)
        }
    }

    fun isOrderExist(){
        if (orderManager.isPrimaryOrderExist() && orderManager.getPrimaryOrder() != null) {
            restoreOrder(orderManager.getPrimaryOrder()!!, RestoredData(ChangedParams.NO_CHANGES,
                    orderManager.getPrimaryOrder()!!.order?.actualCost ?: 0),
                    orderManager.getOrderResult(orderManager.getPrimaryOrder()!!.id))
        }
    }

    fun switchToGainsScreen() {
        setRootScreen(GAINS_SCREEN)
    }

    fun switchToBalanceScreen() {
        setRootScreen(BALANCE_SCREEN)
    }

    fun switchToStatisticsScreen() {
        setRootScreen(PRIVATE_STATISTICS_SCREEN)
    }

    fun switchToBonusScreen() {
        setRootScreen(BONUS_SCREEN)
    }

    fun switchToHistoryScreen() {
        setRootScreen(ORDER_HISTORY_LIST_SCREEN)
    }

    fun switchToHelpScreen() {
        setRootScreen(HELP_SCREEN)
    }

    fun switchToBlockScreen() {
        drawerLockLiveData.postValue(true)
        setRootScreen(BLOCK_SCREEN)
    }

    fun logout() {
        val tokenCopy = "Bearer $token"
        Timber.d("LOGOUT logOut $tokenCopy")
        JobFactory.instance.getJobManager()?.addJobInBackground(LogoutJob(tokenCopy))
        userImageForPrefs = ""
        userImageString = ""
        carImageString = ""
        userNameCached = ""
        userRatingCached = ""
        userPhone = ""
        DriverToolbar.IS_ONLINE_STATE = false
        isCardAdded = false
        doc1 = ""
        doc2 = ""
        doc3 = ""
        doc4 = ""
        lockDrawerLiveData.postValue(true)
        setRootScreen(VERIFICATION_PHONE_SCREEN)
        token = ""
        firebaseToken = ""
        LocationService.stop(context = WeakReference(context))
        SocketService.stop(context = WeakReference(context))
        isNeedComplete = false

    }

    fun switchToUserProfile(appState: AppState) {
        val profileMode: Int =
                when (appState) {
                    AppState.MODERATION -> ProfileFragment.MODE_MODERATION
                    AppState.NEED_COMPLETE -> ProfileFragment.MODE_NEED_COMPLETE
                    else -> ProfileFragment.MODE_PROFILE
                }
        replaceScreen(PROFILE_SCREEN, profileMode)
    }

    fun switchToSettings() {
        setRootScreen(SETTINGS_SCREEN)
    }

    fun updateUserImage() {
        userImageLiveData.postValue(userImageString)
    }

    fun updateUserName() {
        userNameLiveData.postValue(userNameCached)
    }

    fun updateUserRating() {
        userRatingLiveData.postValue(userRatingCached)
    }

    fun checkIfOnlineAllowed() {
        //TODO: fix hardcode
//        onlineClickLiveData.postValue(Pair(true, isNeedComplete))
    }

    fun onNewOrderInfo(orderInfo: NewOrderInfo) {
        Timber.d("SOCKET onNewOrderInfo()")
        Timber.d("SOCKET order: $orderInfo")
        Timber.d("SOCKET onNewOrderInfo ordersQuantity: ${orderManager.getOrdersCount()}")

        if (orderManager.getOrdersCount() <= 2) {
            showOrderToDriver(orderInfo)
        } else {
            throw IndexOutOfBoundsException("Orders are too much in db")
        }
    }

    private fun showOrderToDriver(orderInfo: NewOrderInfo) {
        val answerTimeMillis = orderInfo.order?.answerTime!! //~15 сек
        val serverTimeMillis = orderInfo.order?.serverTime!!
        val timerTime = calcTimerTime(serverTimeMillis, answerTimeMillis)
        Timber.d(" _TIME showOrderToDriver time: $timerTime")
        if (timerTime > 0)
            switchScreen(ORDER_FOUND_SCREEN, orderInfo)
    }

    fun showSecondaryOrder() {
        switchScreen(NEW_ORDER_SCREEN)
    }

    fun sendOnlineStatus(isOnline: Boolean) {
        val request = SetOnlineStatusRequest(isOnline)
        App.instance.getLogger()!!.log("setUserOnlineStatus start ")
        disposable += apiManager.setUserOnlineStatus(request).subscribe(
                { response ->
                    if (response.isSuccess!!) {
                        App.instance.getLogger()!!.log("setUserOnlineStatus suc ")
                        handleResponseState(response)
//                    if (response.isOnline) {
//                        SocketService.start(WeakReference(context))
//                    } else {
//                        SocketService.stop(WeakReference(context))
//                    }
                        BeepUtil.playDriverStatus(response.isOnline)
                    } else {
                        if (response.error!!.code != 4)
                            response.error!!.message!!.longToast(context)
                        else
                            return@subscribe
                        App.instance.getLogger()!!.log("setUserOnlineStatus error ")
                        RxBus.publish(SocketServiceResponseEvent(SocketServiceResponseEvent.Result.NEGATIVE,
                                playSound = true))
                    }
                },
                {
                    App.instance.getLogger()!!.log("setUserOnlineStatus error")
                    RxBus.publish(DisableOnlineStatusEvent(false))
                    loadingLiveData.postValue(ResponseState(ResponseState.State.NETWORK_ERROR))
                }
        )
    }


    fun handleIntent(intent: Intent) {
        val model = resolver.dataToModel(intent)
        Timber.d("handleNotificationModel model: $model")
        model?.let {
            handleNotificationModel(model)
        }
    }

    private fun handleNotificationModel(model: BasicNotification) {
        when (model) {
            is NewOrderAvailable -> {
                switchToMainScreen()
                Log.d("OrderLog", model.order.toString())
                switchScreen(ORDER_FOUND_SCREEN, model.order)
            }

            is DriverBlocked -> {
                setInitScreen()
            }

            is Unlock -> {
                switchToMainScreen()
            }
            is CustomPush -> {
                switchToMainScreen()
            }
            is SuccessfulModeration -> {
                switchToMainScreen()
            }

            is AddedNewDiscount -> {
                switchToMainScreen()
                switchScreen(BONUS_SCREEN)
            }

            is InvalidRegistrationData -> {
                switchToMainScreen()
            }

            is CancelOrder -> {
                Timber.d("CancelOrder, order count:${orderManager.getOrdersCount()}")
                if (orderManager.getOrdersCount() >= 1) {
                    cancelOrderByCustomer(model.orderId,
                            lastLocation = locationStorage.getCachedLocation())
                } else {
                    switchToMainScreen()
                }
            }

            is DownloadingMissingPhoto -> {
                switchToMainScreen()
            }
        }
    }

    fun handleMessagingServiceIntent(typeId: Int) {
        when (typeId) {
            NotificationType.DRIVER_BLOCKED.typeId -> switchToBlockScreen()
            NotificationType.UNLOCK.typeId -> setInitScreen()
        }
    }

    private fun cancelOrderByCustomer(orderId: Int, lastLocation: Location?) {
        val resultData = TaxometerService.result
        if (orderManager.primaryOrderId() == orderId) { //its primary
            val request = TotalCostRequest(
                    factCost = resultData?.factCost
                            ?: TaxometerService.result?.factCost ?: 0,
                    actualCost = resultData?.resultCost ?: TaxometerService.result?.resultCost
                    ?: 0,
                    factTime = resultData?.factTime ?: TaxometerService.result?.factTime ?: 0,
                    factPath = resultData?.factPath?.roundTwoDecimal()
                            ?: TaxometerService.result?.factPath ?: 0.0,
                    longitude = lastLocation?.longitude,
                    latitude = lastLocation?.latitude,
                    route = resultData?.passedRoute ?: TaxometerService.result?.passedRoute,
                    eventTime = System.currentTimeMillis(),
                    changedParams = null,
                    waitingTime = resultData?.waitingTime,
                    waitingCost = resultData?.waitingCost
            )

            JobFactory.instance.getJobManager()?.addJobInBackground(DataSynchronizationJob(orderId, request))
            if (TaxometerService.isAlive)
                TaxometerService.stop(WeakReference(context))

            orderManager.finishOrder()
            if (orderManager.isPrimaryOrderExist()) {
                setRootScreen(EXECUTE_ORDER_SCREEN, orderManager.getPrimaryOrder())
            } else {
                switchToMainScreen()
            }

        } else if (orderManager.secondaryOrderId() == orderId) {// its secondary
            orderManager.deleteSecondaryOrder()
            val request = TotalCostRequest(
                    factCost = 0,
                    actualCost = 0,
                    factTime = 0,
                    factPath = 0.0,
                    longitude = lastLocation?.longitude,
                    latitude = lastLocation?.latitude,
                    route = null,
                    eventTime = System.currentTimeMillis(),
                    changedParams = null,
                    waitingTime = 0,
                    waitingCost = 0.0)
            JobFactory.instance.getJobManager()?.addJobInBackground(DataSynchronizationJob(orderId, request))

        } else {
            switchToMainScreen()
        }
    }

    fun stopSocket() {
//        SocketService.stop(WeakReference(context))
        Timber.d("TOOLBAR SOCKET IS ONLINE: stopSocket()")
    }
}