package com.wezom.taxi.driver.presentation.picklocation

import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.wezom.taxi.driver.databinding.FragmentPickLocationMapBinding
import com.wezom.taxi.driver.ext.getViewModel
import com.wezom.taxi.driver.net.response.AutoCompleteResult
import com.wezom.taxi.driver.presentation.base.BaseMapFragment
import com.wezom.taxi.driver.presentation.filter.CreateFilterViewModel
import java.util.*

/**
 *Created by Zorin.A on 31.July.2019.
 */
class PickLocationMapFragment : BaseMapFragment(), GoogleMap.OnMapClickListener {
    var marker: Marker? = null
    private var geocoder: Geocoder? = null
    private var addresses: List<Address>? = null
    override fun onMapClick(p0: LatLng?) {
        geocoder = Geocoder(context, Locale.getDefault())
        googleMap!!.clear()
        marker = googleMap!!.addMarker(MarkerOptions().position(p0!!))
        addresses = geocoder!!.getFromLocation(p0.latitude, p0.longitude, 1)
        addresses?.let {
            marker!!.title = addresses!![0].getAddressLine(0)
            marker!!.showInfoWindow()
        }
        googleMap!!.moveCamera(CameraUpdateFactory.newLatLng(p0))
        binding.save.visibility = View.VISIBLE
    }


    val viewModel by lazy {
        getViewModel() as PickLocationMapViewModel
    }

    lateinit var binding: FragmentPickLocationMapBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.loadingLiveData.observe(this,
                progressObserver)
        setBackPressFun { viewModel.onBackPressed() }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentPickLocationMapBinding.inflate(inflater,
                container,
                false)
        return binding.root
    }

    override fun onViewCreated(view: View,
                               savedInstanceState: Bundle?) {
        super.onViewCreated(view,
                savedInstanceState)
        initViews()
    }

    private fun initViews() {
        binding.run {
            save.setOnClickListener {
                val place = AutoCompleteResult(false, "", null,
                        null,
                        marker!!.title,
                        null,
                        marker!!.position.latitude.toString(),
                        marker!!.position.longitude.toString())
                marker?.let {
                    viewModel.backWithResult(CreateFilterViewModel.RESULT_CODE_MAP,
                            place)
                }
            }
        }
    }

    override fun onMapReady(map: GoogleMap?) {
        super.onMapReady(map)
        viewModel.setMapStyle()
        googleMap?.uiSettings?.isMapToolbarEnabled = false
        clearMap()
        googleMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(
                LatLng(locationStorage.getCachedLocation().latitude,
                        locationStorage.getCachedLocation().longitude), 15f))
        googleMap!!.setOnMapClickListener(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposable.clear()
    }


    companion object {
        fun newInstance(): PickLocationMapFragment = PickLocationMapFragment()
    }
}