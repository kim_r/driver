package com.wezom.taxi.driver.injection.module

import android.arch.persistence.room.Room
import android.content.Context
import com.wezom.taxi.driver.data.db.*
import com.wezom.taxi.driver.injection.scope.AppScope
import dagger.Module
import dagger.Provides


@Module
class DbModule {
    @AppScope
    @Provides
    fun providesAppDatabase(context: Context): AppDatabase =
            Room.databaseBuilder(context, AppDatabase::class.java, "hub_driver_db")
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build()

    @AppScope
    @Provides
    fun providesOrderInfosDao(database: AppDatabase): OrderInfoDao = database.orderInfoDao()

    @AppScope
    @Provides
    fun providesOrderCoordinatesDao(database: AppDatabase): OrderCoordinatesDao = database.orderCoordinatesDao()

    @AppScope
    @Provides
    fun provideReasonsDao(database: AppDatabase): ReasonsDao = database.reasonsDao()

    @AppScope
    @Provides
    fun provideOrderResultDataDao(database: AppDatabase): OrderResultDataDao = database.orderResultDataDao()

    @AppScope
    @Provides
    fun provideOrderFinalizationDao(database: AppDatabase): OrderFinalizationDao = database.orderFinalizationDao()
}