package com.wezom.taxi.driver.common

/**
 * Created by zorin.a on 16.02.2018.
 */
internal object Constants {
    const val DISK_CACHE_SIZE: Long = 10 * 1024 * 1024

    const val DIRECTIONS_API_URL = "https://maps.googleapis.com/maps/api"
    const val LOCATION_UPDATE_INTERVAL = 3000L

    const val USER_PHOTO_NAME = "user_photo"
    const val CAR_PHOTO_NAME = "car_photo"
    const val TEMP_BITMAP = "temp_bitmap"
    const val REQUEST_IMAGE_PICK = 666
    const val REQUEST_IMAGE_CAPTURE = 333
    const val ADD_CARD = 100
    const val GOOGLE_NAV_PACKAGE_NAME = "com.google.android.apps.maps"
    const val YANDEX_NAV_PACKAGE_NAME = "ru.yandex.yandexnavi"
    const val GOOGLE_NAV_MARKET_REDIRECT = "market://details?id=com.google.android.apps.maps"
    const val YANDEX_NAV_MARKET_REDIRECT = "market://details?id=ru.yandex.yandexnavi"
    const val GOOGLE_NAV_REDIRECT = "http://maps.google.com/maps?"
    const val YANDEX_NAV_REDIRECT = "yandexnavi://build_route_on_map?"
    const val TAXI_URL = "https://wezom.com/"
    const val TAXI_URL_PRIVATE_POLICY = "https://wezom.com/"
    const val TAXI_URL_USER_AGREEMENT = "https://wezom.com/"
    const val TAXI_URL_CONTRACT_OFFER = "https://wezom.com/"
    const val TAXI_URL_FAG = "https://wezom.com/"
    const val GOOGLE_NAV = 0
    const val YANDEX_NAV = 1
    const val DEFAULT_NAV = GOOGLE_NAV
    const val SINGLE_MARKER_ZOOM = 15F
    const val POINT_RESULT_CODE = 111
    const val STEP_TIME_UNSPECIFIED = -1L
}