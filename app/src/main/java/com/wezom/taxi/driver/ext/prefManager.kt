package com.wezom.taxi.driver.ext

import android.content.SharedPreferences
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * Created by zorin.a on 19.02.2018.
 */
const val TOKEN = "token"
const val IS_TUTOR_COMPLETED = "is_tutor_done"
const val IS_CARD_ADDED = "is_card_added"
const val IS_MAP_WHITE = "is_map_dark"
const val IS_INTRO_SHOWED = "is_intro_showed"
const val USER_PHONE = "user_phone"
const val USER_NAME_CACHED = "user_name"
const val USER_RATING_CACHED = "user_rating"
const val USER_BALANCE = "user_balance"
const val USER_BALANCE_ID = "user_balance_id"
const val USER_ID = "user_id"
const val COORDINATES_UPDATE_TIME = "coordinates_update_time"
const val THEME_MODE = "is_theme_white"
const val LANGUAGE = "language"
const val LANGUAGE_CHANGED = "language_changed"
const val NAVIGATOR = "navigator"
const val IS_NEED_COMPLETE = "need_complete"
const val SOFT_FILTER = "soft_filter"
const val SONG_SIGNAL = "song_signal"
const val SIGNAL_ONLINE = "signal_online"
const val SIGNAL_ONLINE_FILTER = "signal_online_filter"
const val SIGNAL_BACKGROUND = "signal_background"
const val SIGNAL_BACKGROUND_WAIT = "signal_background_wait"
const val BACKGROUND = "background"
const val TIMER = "timer"
const val TIMER_NOT_ENTER_CLIENT = "timer_not_enter_client"
const val CALL_CLIENT = "call_client"
const val FINAL_ORDER = "final_order"


//user images
const val USER_IMAGE_URI = "user_image_uri"
const val USER_IMAGE_FOR_PREFS = "user_image_for_prefs"
const val CAR_IMAGE_URI = "car_image_uri"
//docs images
const val FIRST_DOC_URI = "driver_license_1_uri"
const val SECOND_DOC_URI = "driver_license_2_uri"
const val THIRD_DOC_URI = "tech_passport_1_uri"
const val FOURTH_DOC_URI = "tech_passport_2_uri"

// orders
const val PRIMARY_ORDER_ID = "first_order_id"
const val SECONDARY_ORDER_ID = "second_order_id"
// time of last change of order status; may get unspecified value (-1)
const val LAST_ORDER_STEP_TIME = "last_order_step_time"

const val FIREBASE_TOKEN_KEY = "firebase_token_key"

const val VIBRO_ENABLED = "vibro_enabled"
const val SOUND_ENABLED = "sound_enabled"
const val OVERLAY_ORDER_INNER = "overlay_order_inner"

//Location
const val LATITUDE = "latitude"
const val LONGITUDE = "longitude"
const val LOCATION_TIME = "location_time"

fun SharedPreferences.int(key: String? = null,
                          default: Int = 0) = delegate(key,
                                                       default,
                                                       SharedPreferences::getInt,
                                                       SharedPreferences.Editor::putInt)

fun SharedPreferences.string(key: String? = null,
                             default: String = "") = delegate(key,
                                                              default,
                                                              SharedPreferences::getString,
                                                              SharedPreferences.Editor::putString)

fun SharedPreferences.boolean(key: String? = null,
                              default: Boolean = false) = delegate(key,
                                                                   default,
                                                                   SharedPreferences::getBoolean,
                                                                   SharedPreferences.Editor::putBoolean)

fun SharedPreferences.long(key: String? = null,
                           default: Long = 0) = delegate(key,
                                                         default,
                                                         SharedPreferences::getLong,
                                                         SharedPreferences.Editor::putLong)


private inline fun <T> SharedPreferences.delegate(key: String?,
                                                  defaultValue: T,
                                                  crossinline getter: SharedPreferences.(String, T) -> T,
                                                  crossinline setter: SharedPreferences.Editor.(String, T) ->
                                                  SharedPreferences.Editor): ReadWriteProperty<Any, T> {
    return object : ReadWriteProperty<Any, T> {
        override fun getValue(thisRef: Any,
                              property: KProperty<*>) = getter(key ?: property.name,
                                                               defaultValue)

        override fun setValue(thisRef: Any,
                              property: KProperty<*>,
                              value: T) = edit().setter(key ?: property.name,
                                                        value).apply()
    }
}