package com.wezom.taxi.driver.data.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


/**
 * Created by zorin.a on 021 21.02.18.
 */

data class Car(
        @SerializedName("id") var id: Int? = null,
        @SerializedName("brand") var brand: CarParameter? = null,
        //@SerializedName("type") var type: List<CarParameter>? = null,
        @SerializedName("model") var model: CarParameter? = null,
        @SerializedName("year") var year: Int? = null,
        @SerializedName("color") var color: CarParameter? = null,
        @SerializedName("number") var number: String? = null,
        @SerializedName("photo") var photo: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readParcelable(CarParameter::class.java.classLoader),
            parcel.readParcelable(CarParameter::class.java.classLoader),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readParcelable(CarParameter::class.java.classLoader),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeParcelable(brand, flags)
        parcel.writeParcelable(model, flags)
        parcel.writeValue(year)
        parcel.writeParcelable(color, flags)
        parcel.writeString(number)
        parcel.writeString(photo)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Car> {
        override fun createFromParcel(parcel: Parcel): Car {
            return Car(parcel)
        }

        override fun newArray(size: Int): Array<Car?> {
            return arrayOfNulls(size)
        }
    }


}
