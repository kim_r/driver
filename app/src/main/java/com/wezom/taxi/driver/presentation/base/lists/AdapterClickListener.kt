package com.wezom.taxi.driver.presentation.base.lists


interface AdapterClickListener<T> {
    fun onItemClick(position: Int, data: T)
}
