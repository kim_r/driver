package com.wezom.taxi.driver.data.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by zorin.a on 022 22.02.18.
 */
data class Address(
        @SerializedName("_id") var id: String?=null,
        @SerializedName("name") var name: String?=null,
        @SerializedName("longitude") var longitude: Double?=null,
        @SerializedName("latitude") var latitude: Double?=null,
        @SerializedName("number") var number: Int?=null,
        @SerializedName("entrance") var entrance: String?=null) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Double::class.java.classLoader) as? Double?,
            parcel.readValue(Double::class.java.classLoader) as? Double?,
            parcel.readValue(Int::class.java.classLoader) as? Int?,
            parcel.readString())

    constructor() : this("", "", 0.0, 0.0, 0, "")

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeValue(longitude)
        parcel.writeValue(latitude)
        parcel.writeValue(number)
        parcel.writeString(entrance)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Address> {
        override fun createFromParcel(parcel: Parcel): Address {
            return Address(parcel)
        }

        override fun newArray(size: Int): Array<Address?> {
            return arrayOfNulls(size)
        }
    }

}