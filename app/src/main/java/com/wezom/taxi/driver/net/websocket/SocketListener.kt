package com.wezom.taxi.driver.net.websocket

import com.wezom.taxi.driver.data.models.CloseOrderAuto
import com.wezom.taxi.driver.data.models.NewOrderInfo
import com.wezom.taxi.driver.data.models.map.Hexagon
import com.wezom.taxi.driver.net.response.BaseResponse

/**
 * Created by zorin.a on 20.04.2018.
 */
interface SocketListener {
    fun onConnected()
    fun onLoginDone()
    fun onDisconnected()
    fun onError(baseResponse: BaseResponse?)
    fun onConnectError()
    fun onNewOrder(order: NewOrderInfo)
    fun onCoordinatesSubmitted()
    fun onHexagonReceived(hexagon: Hexagon)
    fun onCancelOrder(orderId: Int)
    fun onDisableOnlineStatus(online: Boolean)
    fun onNewBroadcast(order: NewOrderInfo)
    fun onDeleteBroadcast(orderId: Int)
    fun onBroadcastOrdersCount(count:Int)
    fun onCloseOrderAutomatically(orderId: CloseOrderAuto)
}