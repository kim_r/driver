package com.wezom.taxi.driver.presentation.help

import android.arch.lifecycle.MutableLiveData
import com.wezom.taxi.driver.data.models.ResponseState
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.repository.DriverRepository
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.zipWith
import javax.inject.Inject

/**
 * Created by udovik.s on 23.03.2018.
 */
class HelpViewModel @Inject constructor(screenRouterManager: ScreenRouterManager, private val repository: DriverRepository) : BaseViewModel(screenRouterManager) {

    val helpEmailPhoneLiveData: MutableLiveData<Pair<String, String>> = MutableLiveData()

    fun getHelpEmailAndPhone() {
        loadingLiveData.postValue(ResponseState(ResponseState.State.LOADING))
        App.instance.getLogger()!!.log("getHelpEmail start and end ")
        val helpEmail = repository.getHelpEmail()
        val userProfile = repository.profileAndRegistrationCorrectness()
        disposable += helpEmail.zipWith(userProfile) { email, profile ->
            Pair(email.email, profile.user!!.phone)
        }.subscribe({ response ->
                loadingLiveData.postValue(ResponseState(ResponseState.State.IDLE))
                helpEmailPhoneLiveData.postValue(Pair(response.first, response.second.toString()))
        }, {
            loadingLiveData.postValue(ResponseState(ResponseState.State.NETWORK_ERROR))
        })
    }
}