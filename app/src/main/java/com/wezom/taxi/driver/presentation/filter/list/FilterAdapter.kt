package com.wezom.taxi.driver.presentation.filter.list

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import android.content.SharedPreferences
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.databinding.ItemFilterBinding
import com.wezom.taxi.driver.databinding.ItemFilterSoftBinding
import com.wezom.taxi.driver.ext.*
import com.wezom.taxi.driver.net.response.FilterResponse
import com.wezom.taxi.driver.presentation.filter.base.EditFilter
import com.wezom.taxi.driver.presentation.filter.base.FilterSwitch
import com.wezom.taxi.driver.presentation.filter.base.SoftFilterEvent
import timber.log.Timber
import javax.inject.Inject
import kotlin.properties.Delegates

/**
 *Created by Zorin.A on 26.June.2019.
 */
class FilterAdapter @Inject constructor(var sharedPreferences: SharedPreferences) : RecyclerView.Adapter<BaseItem<FilterResponse>>() {
    var callback: ((position: Int, orderInfo: FilterResponse) -> Unit)? = null
    private var content: MutableList<FilterResponse> by Delegates.observable(mutableListOf(),
            { _, _, newValue ->
                Timber.d("FilterViewHolder newValue: ${newValue.size}")
                emptyLiveData.value =
                        newValue.size
                notifyDataSetChanged()
            })

    val emptyLiveData: MutableLiveData<Int> = MutableLiveData()

    fun addAll(data: List<FilterResponse>) {

        content = data.toMutableList()
        notifyDataSetChanged()
    }

    fun addItem(dataItem: FilterResponse) {
        Timber.d("FILTER_ADAPTER addItem: $dataItem")
        content.add(0,
                dataItem)
        notifyItemInserted(0)
        emptyLiveData.postValue(content.size)
    }

    fun removeItem(orderId: Int) {
        Timber.d("FILTER_ADAPTER removeItem: $orderId")
        val order = content.find { it.id == orderId }
        val index = content.indexOf(order)
        content.remove(order)
        notifyItemRemoved(index)
        emptyLiveData.postValue(content.size)
    }

    fun clear() {
        content.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): BaseItem<FilterResponse> {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemFilterBinding.inflate(inflater,
                parent,
                false)
        when (viewType) {
            R.layout.item_filter_soft ->
                return FilterSoftViewHolder(ItemFilterSoftBinding.inflate(inflater,
                        parent,
                        false), sharedPreferences, content)
        }

        return FilterViewHolder(binding, content)
    }

    override fun getItemCount(): Int = content.size

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            R.layout.item_filter_soft
        } else
            R.layout.item_filter
    }

    override fun onBindViewHolder(holder: BaseItem<FilterResponse>,
                                  position: Int) {
        holder?.bind(content[position])
        holder?.callback = callback
    }

    class FilterViewHolder(private val binding: ItemFilterBinding,  var content: MutableList<FilterResponse>) : BaseItem<FilterResponse>(binding.root) {
        override var callback: ((position: Int, orderInfo: FilterResponse) -> Unit)? = null

        @SuppressLint("SetTextI18n")
        override fun bind(dataItem: FilterResponse) {
            binding.run {
                dataItem.name?.let {
                    name.text = dataItem.name
                } ?: run {
                    name.text = name.context.resources.getString(R.string.not_name)
                }

                activationSwitch.isChecked = dataItem.status!!

                dataItem.address?.let {
                    addressValue.text = dataItem.address
                } ?: run {
                    addressValue.visibility = View.GONE
                }

                dataItem.fromRadius?.let {
                    radiusValue.text = radiusValue.context.resources.getString(R.string.about_few_km, dataItem.fromRadius.toString())
                } ?: run {
                    radiusValue.visibility = View.GONE
                    radiusTitle.visibility = View.GONE
                }

                dataItem.toRadius?.let {
                    destinationValue.text = destinationValue.context.resources.getString(R.string.about_few_km, dataItem.toRadius.toString())
                } ?: run {
                    destinationValue.visibility = View.GONE
                    destinationTitle.visibility = View.GONE
                }

                activationSwitch.setOnCheckedChangeListener { _, isChecked ->
                    dataItem.status = isChecked
                    RxBus.publish(FilterSwitch(dataItem, activationSwitch))
                    for (item in content) {
                        if (item.status!!) {
                            return@setOnCheckedChangeListener
                        }
                    }
                    RxBus.publish(SoftFilterEvent(isChecked))
                }

                item.setOnClickListener {
                    RxBus.publish(EditFilter(dataItem))
                }

            }
        }


    }

    class FilterSoftViewHolder(private val binding: ItemFilterSoftBinding, var sharedPreferences: SharedPreferences,
                               var content: MutableList<FilterResponse>) : BaseItem<FilterResponse>(binding.root) {

        override var callback: ((position: Int, orderInfo: FilterResponse) -> Unit)? = null

        var softFilter: Boolean by sharedPreferences.boolean(SOFT_FILTER)

        @SuppressLint("SetTextI18n", "CheckResult")
        override fun bind(dataItem: FilterResponse) {
            RxBus.listen(SoftFilterEvent::class.java).subscribe {
                softFilter = it.b
                binding.activationSwitch.isChecked = it.b
            }

            binding.run {
                activationSwitch.setOnCheckedChangeListener { _, isChecked ->
                    for (item in content) {
                        if (item.status!!) {
                            softFilter = isChecked
                            return@setOnCheckedChangeListener
                        }
                    }
                    binding.activationSwitch.isChecked = false
                }
                for (item in content) {
                    if (item.status!!) {
                        activationSwitch.isChecked = softFilter
                        return
                    }
                }
                softFilter = false


            }
        }


    }
}