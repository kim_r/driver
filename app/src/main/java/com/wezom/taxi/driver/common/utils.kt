package com.wezom.taxi.driver.common

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.Settings
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.google.android.gms.maps.CameraUpdate
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.wezom.taxi.driver.BuildConfig
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.data.models.Address
import com.wezom.taxi.driver.data.models.DatesOfWeekModel
import com.wezom.taxi.driver.data.models.PaymentMethod
import com.wezom.taxi.driver.ext.setVisible
import com.wezom.taxi.driver.injection.module.GlideApp
import com.wezom.taxi.driver.presentation.main.MainActivity
import com.wezom.taxi.driver.presentation.mainmap.dialog.GpsDialog
import com.wezom.taxi.driver.presentation.mainmap.dialog.OverlayDialog
import timber.log.Timber
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by zorin.a on 28.02.2018.
 */
fun loadRoundedImage(context: Context, view: ImageView, url: String? = null, uri: Uri? = null) {
    val requestOptions = RequestOptions().circleCrop()
    GlideApp.with(context)
            .load(url ?: uri)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
            .apply(requestOptions)
            .into(view)
}

fun loadRoundedImage(context: Context, view: ImageView, url: String? = null, uri: Uri? = null, placeHolderId: Int) {
    val requestOptions = RequestOptions()
            .placeholder(placeHolderId)
            .circleCrop()
    GlideApp.with(context)
            .load(url ?: uri)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
            .placeholder(placeHolderId)
            .apply(requestOptions)
            .into(view)
}

fun loadImage(context: Context, view: ImageView, url: String? = null, uri: Uri? = null) {
    GlideApp.with(context)
            .load(url ?: uri)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(view)
}

fun loadImage(context: Context, view: ImageView, url: String? = null, uri: Uri? = null, placeHolderId: Int) {
    GlideApp.with(context)
            .load(url ?: uri)
            .placeholder(placeHolderId)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(view)
}

fun saveImageToFile(context: Context, url: String): Uri {
    val file = Glide.with(context).load(url).downloadOnly(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL).get()
    return Uri.fromFile(file)
}

fun getBitmapFromImage(image: ImageView): Bitmap? {
    val dr = image.drawable
    return dr?.let { (it as BitmapDrawable).bitmap }
}

fun pxToDp(px: Int): Int = (px / Resources.getSystem().displayMetrics.density).toInt()

fun dpToPx(dp: Int): Int = (dp * Resources.getSystem().displayMetrics.density).toInt()


fun formatDateTime(time: Long): String {
    val dateFormatter = SimpleDateFormat("dd.MM.yyyy в HH:mm")
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = time
    return dateFormatter.format(calendar.time)
}

fun formatBalanceDateTime(time: Long): String {
    val dateFormatter = SimpleDateFormat("dd.MM.yy (HH:mm)")
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = time
    return dateFormatter.format(calendar.time)
}

fun formatTime(time: Long): String {
    val dateFormatter = SimpleDateFormat("HH:mm")
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = time
    return dateFormatter.format(calendar.time)
}

fun getDaysDatesOfWeek(week: Int): DatesOfWeekModel {
    val dateFormatter = SimpleDateFormat("dd.MM.yyyy")
    val cal = Calendar.getInstance()
    val year = cal.get(Calendar.YEAR)
    cal.clear()

    cal.set(Calendar.WEEK_OF_YEAR, week)
    cal.set(Calendar.YEAR, year)
    cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
    val firstWkDay = cal.timeInMillis
    cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY)
    val lastWkDay = cal.timeInMillis
    val formattedDates = "${dateFormatter.format(firstWkDay)} - ${dateFormatter.format(lastWkDay)}"
    return DatesOfWeekModel(firstWkDay, lastWkDay, formattedDates)
}

fun formatDiscountDate(data: Long?): String {
    return SimpleDateFormat("dd.MM.yy").format(data)
}

fun storeBitmapToDisk(context: Context, bitmap: Bitmap, filename: String): Uri {
    return File.createTempFile("HUB", filename, context.cacheDir)
            .let {
                it.let {
                    FileOutputStream(it).use {
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, it)
                        it.flush()
                    }
                    Uri.fromFile(it)
                }
            }
}

fun decodeUriToBitmap(context: Context, uri: Uri): Bitmap =
        context.contentResolver.openInputStream(uri).use {
            BitmapFactory.decodeStream(it)
        }

@Throws(IOException::class)
fun createImageFile(context: Context?): File {
    // Create an image file name
    val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
    val imageFileName = "JPEG_" + timeStamp + "_"
    val storageDir = context?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)

    // Save a file: path for use with ACTION_VIEW intents
    return File.createTempFile(imageFileName, ".jpg", storageDir)
}

fun langToIntCode(language: String): Int = when (language) {
    "uk" -> 1
    else -> 0
}


fun isNavigatorInstalled(context: Context, name: String): Boolean {
    return try {
        val info = context.packageManager.getApplicationInfo(name, 0)
        true
    } catch (e: PackageManager.NameNotFoundException) {
        false
    }
}


fun showMultipleMarkesInBounds(vararg markers: Marker): CameraUpdate {
    val bounds = LatLngBounds.builder()
    markers.forEach { marker -> bounds.include(marker.position) }
    return CameraUpdateFactory.newLatLngBounds(bounds.build(), 50)
}

fun showSingleMakrerInBounds(marker: Marker): CameraUpdate =
        CameraUpdateFactory.newLatLngZoom(marker.position, Constants.SINGLE_MARKER_ZOOM)

fun formatMoney(number: Number): String {
    val dfs = DecimalFormatSymbols()
    dfs.groupingSeparator = ' '
    val df = DecimalFormat("###,###,###.00", dfs)
    return df.format(number)
}

fun formatBalanceMoney(number: Number): String {
    val dfs = DecimalFormatSymbols()
    dfs.groupingSeparator = ' '
    val df = DecimalFormat("###,###,##0.00", dfs)
    return df.format(number)
}

fun formatMoneyRounded(number: Int): String {
    val dfs = DecimalFormatSymbols()
    dfs.groupingSeparator = ' '
    val df = DecimalFormat("###,###,###", dfs)
    return df.format(number)
}

fun calcTimerTime(serverTime: Long, answerTime: Long): Long {
    val currentTimeMillis = System.currentTimeMillis() //текущий таймстамп
    Timber.d("_TIME currentTime: ${Date(currentTimeMillis)}")
    Timber.d("_TIME answerTime: ${Date(answerTime)}")
    Timber.d("_TIME serverTime: ${Date(serverTime)}")
    val cleanDifferenceTime = answerTime - serverTime //время для ответа без учёта скоса   15000
    Timber.d("_TIME cleanDifferenceTime: $cleanDifferenceTime")
    val timeSkew = 1000 /*currentTimeMillis - serverTime*/ //скос времени между серваком и локалью
    Timber.d("_TIME timeSkew: $timeSkew")
    return cleanDifferenceTime /*- timeSkew*/
}

fun viewsVisibilitySwitch(isVisible: Boolean, vararg view: View) {
    for (v in view) {
        v.setVisible(isVisible)
    }
}

fun decodePolyline(encodedPath: String): List<LatLng> {
    val len = encodedPath.length

    // For speed we preallocate to an upper bound on the final length, then
    // truncate the array before returning.
    val path = ArrayList<LatLng>()
    var index = 0
    var lat = 0
    var lng = 0

    while (index < len) {
        var result = 1
        var shift = 0
        var b: Int
        do {
            b = encodedPath[index++].toInt() - 63 - 1
            result += b shl shift
            shift += 5
        } while (b >= 0x1f)
        lat += if (result and 1 != 0) (result shr 1).inv() else result shr 1

        result = 1
        shift = 0
        do {
            b = encodedPath[index++].toInt() - 63 - 1
            result += b shl shift
            shift += 5
        } while (b >= 0x1f)
        lng += if (result and 1 != 0) (result shr 1).inv() else result shr 1

        path.add(LatLng(lat * 1e-5, lng * 1e-5))
    }

    return path
}


fun getPolygonCenterPoint(polygonPointsList: List<LatLng>): LatLng {
    var centerLatLng: LatLng? = null
    val builder = LatLngBounds.Builder()
    for (i in 0 until polygonPointsList.size) {
        builder.include(polygonPointsList[i])
    }
    val bounds = builder.build()
    centerLatLng = bounds.center

    return centerLatLng
}

fun addCoefficientText(context: Context?, map: GoogleMap?,
                       location: LatLng?, text: String?,
                       fontSize: Int): Marker? {
    var marker: Marker? = null

    if (context == null || map == null || location == null || text == null
            || fontSize <= 0) {
        return marker
    }

    val textView = TextView(context)
    textView.text = text
    textView.textSize = fontSize.toFloat()

    val paintText = textView.paint

    val boundsText = Rect()
    paintText.getTextBounds(text, 0, textView.length(), boundsText)
    paintText.textAlign = Paint.Align.CENTER

    val conf = Bitmap.Config.ARGB_8888
    val bmpText = Bitmap.createBitmap(boundsText.width() + 2, boundsText.height() + 2, conf)

    val canvasText = Canvas(bmpText)
    paintText.color = Color.BLACK

    canvasText.drawText(text, (canvasText.width / 2).toFloat(),
            (canvasText.height - boundsText.bottom).toFloat(), paintText)

    val markerOptions = MarkerOptions()
            .position(location)
            .icon(BitmapDescriptorFactory.fromBitmap(bmpText))
            .anchor(0.5f, 0.5f)

    marker = map.addMarker(markerOptions)
    return marker
}

fun formatAddress(address: Address): String {
    val entrance = address.entrance
    return if (!entrance.isNullOrEmpty() && !address.name.equals(entrance))
        "${address.name}, п ${address.entrance}" else "${address.name}"
}

fun checkGpsEnabled(context: Context): Boolean {
    val lm = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    return lm.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
            lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
}

fun showGpsDialog(context: Context, fragmentManager: android.support.v4.app.FragmentManager) {
    val dialog = GpsDialog()
    dialog.listener = object : GpsDialog.DialogClickListener {
        override fun onPositiveClick() {
            val action = Settings.ACTION_LOCATION_SOURCE_SETTINGS
            context.startActivity(Intent(action))
            dialog.dismiss()
        }
    }
    dialog.isCancelable = false
    if (fragmentManager.findFragmentByTag(GpsDialog.TAG) == null) {
        dialog.show(fragmentManager, GpsDialog.TAG)
    }
}

fun showOverlayDialog(activity: Activity, fragmentManager: android.support.v4.app.FragmentManager) {
    val dialog = OverlayDialog()
    dialog.listener = object : OverlayDialog.DialogClickListener {
        override fun onPositiveClick() {
            requestOverlayPermissions(activity)
            dialog.dismiss()
        }
    }
    dialog.isCancelable = false
    if (fragmentManager.findFragmentByTag(OverlayDialog.TAG) == null) {
        dialog.show(fragmentManager, OverlayDialog.TAG)
    }
}

fun intToMinutesAndSeconds(value: Int): String {
    val minutes = (value % 3600) / 60
    val seconds = value % 60
    return String.format("%02d:%02d", minutes, seconds)
}

fun isPreAndroidO(): Boolean = Build.VERSION.SDK_INT < Build.VERSION_CODES.O

fun callSupport(url: String, context: Context, phone: String) {
    val intent = Intent(Intent.ACTION_SENDTO)
    intent.data = Uri.fromParts("mailto", url, null)
    intent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.support_mail_subject))
    val text = " model ${Build.MODEL}\nAndroid ${Build.VERSION.RELEASE}\n App version: ${BuildConfig.VERSION_NAME}\n $phone"

    intent.putExtra(Intent.EXTRA_TEXT, text)
    context.startActivity(Intent.createChooser(intent, context.getString(R.string.sent_with)))
}

fun checkIsSystemOverlayEnabled(context: Context): Boolean {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        Settings.canDrawOverlays(context)
    else true
}


fun requestOverlayPermissions(activity: Activity) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION ,
                Uri.parse("package:${activity.packageName}"))
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        activity.startActivityForResult(intent, MainActivity.CODE_DRAW_OVER_OTHER_APP_PERMISSION)
    }
}

fun getPaymentResource(payment: PaymentMethod): Int {
    return when (payment) {
        PaymentMethod.CASH -> R.drawable.ic_cash
        PaymentMethod.CASH_AND_BONUS -> R.drawable.ic_cash
        PaymentMethod.CARD -> R.drawable.ic_card
        PaymentMethod.CARD_AND_BONUS -> R.drawable.ic_card
    }
}

fun <T> MutableList<T>.swap(index1: Int, index2: Int) {
    val tmp = this[index1] // 'this' относится к листу
    this[index1] = this[index2]
    this[index2] = tmp
}



