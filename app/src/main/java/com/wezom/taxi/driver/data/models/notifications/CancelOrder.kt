package com.wezom.taxi.driver.data.models.notifications


class CancelOrder(val typeId: String, val title: String, val body: String, val orderId: Int) : BasicNotification