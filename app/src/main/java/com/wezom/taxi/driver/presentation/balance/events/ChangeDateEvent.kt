package com.wezom.taxi.driver.presentation.balance.events

/**
 * Created by andre on 23.03.2018.
 */
class ChangeDateEvent constructor(var firstDay: Long, var lastDay: Long)