package com.wezom.taxi.driver.net.jobqueue.jobs

import android.annotation.SuppressLint
import com.wezom.taxi.driver.net.request.TotalCostRequest
import com.wezom.taxi.driver.presentation.app.App

/**
 * Created by zorin.a on 18.07.2018.
 */
class DataSynchronizationJob constructor(val orderId: Int, val request: TotalCostRequest) : BaseJob() {
    @SuppressLint("CheckResult")
    override fun onRun() {
        App.instance.getLogger()!!.log("dataSynchronization start and end")
        apiManager.dataSynchronization(orderId, request).blockingGet()
    }
}