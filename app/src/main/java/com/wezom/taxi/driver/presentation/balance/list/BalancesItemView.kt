package com.wezom.taxi.driver.presentation.balance.list

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import com.wezom.taxi.driver.common.formatBalanceDateTime
import com.wezom.taxi.driver.common.formatBalanceMoney
import com.wezom.taxi.driver.data.models.Income
import com.wezom.taxi.driver.databinding.BalancesItemBinding
import com.wezom.taxi.driver.presentation.base.lists.ItemModel

/**
 * Created by andre on 22.03.2018.
 */
class BalancesItemView : ConstraintLayout, ItemModel<Income> {

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)


    private val binding = BalancesItemBinding.inflate(LayoutInflater.from(context), this, true)

    override fun setData(data: Income) {
        binding.run {
            title.text = data.serviceName
            description.text = data.paymentName
            date.text = formatBalanceDateTime(data.time)
            bonusPrice.text = if (data.newMoney < 0) formatBalanceMoney(data.newMoney) else {
                "+".plus(formatBalanceMoney(data.newMoney))
            }
            price.text = formatBalanceMoney(data.oldMoney)
        }
    }
}