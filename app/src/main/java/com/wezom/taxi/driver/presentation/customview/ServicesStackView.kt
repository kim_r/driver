package com.wezom.taxi.driver.presentation.customview

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.common.dpToPx
import com.wezom.taxi.driver.databinding.ServicesStackBinding
import com.wezom.taxi.driver.presentation.profile.dialog.InfoDialog
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign


/**
 * Created by zorin.a on 12.03.2018.
 */
class ServicesStackView : LinearLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, attributeSetId: Int) : super(context, attrs, attributeSetId)

    private var binding: ServicesStackBinding? = ServicesStackBinding.inflate(LayoutInflater.from(context), this, true)

    private var isActive: Boolean = false
    private var disposable = CompositeDisposable()

    fun setActiveStatus(isActive: Boolean) {
        this.isActive = isActive
    }

    fun addServices(vararg services: Int) {
        services.map {
            if (isActive) {
                when (it) {
                    CARGO -> {
                        val view = createImage(R.drawable.ic_case)
                        disposable += RxView.clicks(view).subscribe({
                            showInfoDialog(R.drawable.ic_baggage_ill, R.string.bagage, R.string.bagage_text)
                        })
                        view
                    }
                    ANIMAL -> {
                        val view = createImage(R.drawable.ic_pets)
                        disposable += RxView.clicks(view).subscribe({
                            showInfoDialog(R.drawable.ic_pets_ill, R.string.animal_transportation, R.string.animal_transportation_text)
                        })
                        view
                    }
                    DO_NOT_SMOKE -> {
                        val view = createImage(R.drawable.ic_no_smoke)
                        disposable += RxView.clicks(view).subscribe({
                            showInfoDialog(R.drawable.ic_nosmoke_ill, R.string.no_smoke, R.string.no_smoke_text)
                        })
                        view
                    }
                    SILENT_DRIVER -> {
                        val view = createImage(R.drawable.ic_silence)
                        disposable += RxView.clicks(view).subscribe({
                            showInfoDialog(R.drawable.ic_silence_ill, R.string.quiet_driver, R.string.quiet_driver_text)
                        })
                        view
                    }
                    else -> {
                        return
                    }
                }
            } else {
                when (it) {
                    CARGO -> createImage(R.drawable.ic_case_gray)
                    ANIMAL -> createImage(R.drawable.ic_pets_gray)
                    DO_NOT_SMOKE -> createImage(R.drawable.ic_nosmoke_gray)
                    SILENT_DRIVER -> createImage(R.drawable.ic_silence_gray)
                    else -> {
                        return
                    }
                }
            }
        }.forEach {
            binding?.container?.addView(it as View?)
        }
    }

    private fun createImage(resId: Int): ImageView {
        val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        lp.marginStart = dpToPx(16)
        val image = ImageView(context)
        image.setImageResource(resId)
        image.layoutParams = lp
        return image
    }

    fun clear() {
        binding?.container?.removeAllViews()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        disposable.dispose()
    }

    private fun showInfoDialog(image: Int, title: Int, text: Int) {
        val dialog = InfoDialog.newInstance(image, title, text)
        dialog.listener = object : InfoDialog.DialogClickListener {
            override fun onPositiveClick() {
                dialog.dismiss()
            }

            override fun onNegativeClick() {

            }
        }
        dialog.show((context as AppCompatActivity).supportFragmentManager, InfoDialog.TAG)
    }

    companion object {
        const val CARGO: Int = 1
        const val ANIMAL: Int = 2
        const val SILENT_DRIVER: Int = 3
        const val DO_NOT_SMOKE: Int = 4
    }
}