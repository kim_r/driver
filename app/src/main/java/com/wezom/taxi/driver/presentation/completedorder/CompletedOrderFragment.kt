package com.wezom.taxi.driver.presentation.completedorder

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.jakewharton.rxbinding2.view.RxView
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.common.formatAddress
import com.wezom.taxi.driver.common.formatDateTime
import com.wezom.taxi.driver.common.isPermissionGranted
import com.wezom.taxi.driver.common.loadRoundedImage
import com.wezom.taxi.driver.data.models.PaymentMethod
import com.wezom.taxi.driver.data.models.SendHelpTimeType
import com.wezom.taxi.driver.data.models.Trip
import com.wezom.taxi.driver.databinding.CompletedOrderBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.ext.setVisible
import com.wezom.taxi.driver.presentation.acceptorder.AcceptOrderFragment
import com.wezom.taxi.driver.presentation.base.BaseMapFragment
import com.wezom.taxi.driver.presentation.executeorder.ExecuteOrderFragment.Companion.CALL_PHONE_REQUEST
import com.wezom.taxi.driver.presentation.executeorder.ExecuteOrderFragment.Companion.SEND_SMS_REQUEST
import io.reactivex.rxkotlin.plusAssign


/**
 * Created by andre on 15.03.2018.
 */
class CompletedOrderFragment : BaseMapFragment() {

    //region var
    private lateinit var binding: CompletedOrderBinding
    private lateinit var viewModel: CompletedOrderViewModel
    var orderAddressCoordinates: MutableList<LatLng> = mutableListOf()

    var orderId: Int = 0
    lateinit var trip: Trip
    //endregion

    //region override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setBackPressFun { viewModel.onBackPressed() }
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
        arguments?.getInt(ORDER_ID_KEY)?.let {
            orderId = it
        }

        viewModel.loadingLiveData.observe(this, progressObserver)
        viewModel.orderLiveData.observe(this, Observer { order ->
            trip = order!!
            updateUi(order)
            if (trip.route == null || trip.route?.isEmpty()!!) {
                binding.map.setVisible(false)
            }
            trip.route?.let {
                it.forEach {
                    orderAddressCoordinates.add(LatLng(it?.latitude!!, it.longitude!!))
                }
                viewModel.updateRoute(orderAddressCoordinates)
            }
            if (isMapReady()) {
                handleCamera()
            }
            clicks()
        })
    }

    private fun clicks() {
        binding.run {
            disposable += RxView.clicks(phoneImage).subscribe {
                viewModel.sendHelpTime(orderId, SendHelpTimeType.PHONE)
                makeCallClick()
            }
//            disposable += RxView.clicks(messageImage).subscribe {
//                sendSmsClick()
//                viewModel.sendHelpTime(orderId, SendHelpTimeType.MESSAGE)
//            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = CompletedOrderBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getOrder(orderId)

        viewModel.isMapWhiteThemeLiveData.observe(this, Observer { isMapWhite ->
            setMapStyle(isMapWhite)
        })
        viewModel.routeLiveData.observe(this, Observer { it ->
            drawRoute(it)
        })
    }

    override fun onMapReady(map: GoogleMap?) {
        super.onMapReady(map)
        viewModel.setMapStyle()
        enableMapGestures(false)
        handleCamera()
        disableMarkerClicks()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        grantResults
                .filter { it == PackageManager.PERMISSION_DENIED }
                .forEach { _ -> return }

        when (requestCode) {
            CALL_PHONE_REQUEST -> {
                trip.user.phone?.let {
                    dialUser(it)
                }
            }
            SEND_SMS_REQUEST -> {
                trip.user.phone?.let {
                    sendMessage(it)
                }
            }
        }
    }
    //endregion

    //region fun
    private fun updateUi(trip: Trip?) {
        binding.run {
            trip?.let {
                toolbar.setToolbarTitle(formatDateTime(it.time))
                contactsLayout.setVisible(true)

                for (coordinate in it.coordinates) {
                    val tv = createEntry(formatAddress(coordinate))
                    addressLayout.addView(tv)
                }

                if (it.coordinates.size == 1) {
                    val tv = createEntry(getString(R.string.around_town))
                    addressLayout.addView(tv)
                }

                it.payment.let {
                    /*  if (it.income == 0.0) {
                          priceText.setVisible(false)
                          incomeText.setVisible(false)
                      }*/

                    cardImage.setImageResource(getPaymentResource(it.method!!))

                    /* if (it.actualCost == 0.0) {
                         sumPrice.setVisible(false)
                         sum.setVisible(false)
                     }

                     if (it.surcharge == 0.0) {
                         bonusPrice.setVisible(false)
                         bonus.setVisible(false)
                     }

                     if (it.commission == 0.0) {
                         commissionPrice.setVisible(false)
                         commission.setVisible(false)
                     }*/

                    priceText.text = getString(R.string.uah_price_integer, it.income?.toInt())
                    sumPrice.text = getString(R.string.uah_price_integer, it.actualCost?.toInt())

                    if (it.surcharge == null) {
                        bonusPrice.text = getString(R.string.uah_price_integer, 0)
                    } else {
                        bonusPrice.text = getString(R.string.uah_price_integer, it.surcharge?.toInt())
                    }

                    if (trip.user.isRated == true) {
                        estimateImage.setImageResource(R.drawable.ic_star_circled_inactive)
                        ratingBar.setVisible(true)
                    } else {
                        estimateImage.setImageResource(R.drawable.ic_star_circled_active)
                        ratingBar.setVisible(false)
                        disposable += RxView.clicks(estimateImage).subscribe {
                            viewModel.openRatingScreen(trip)
                        }
                    }

                    commissionPrice.text = getString(R.string.uah_price_float, it.commission)
                    if (!it.notPaidMessage.isNullOrEmpty()) {
                        faultImage.setVisible(true)
                        faultText.setVisible(true)
                        faultText.text = it.notPaidMessage

                    } else {
                        /* faultImage.setVisible(false)
                         faultText.setVisible(false)*/
                    }
                }

                it.user.let {
                    if (TextUtils.isEmpty(it.name)) {
                        contactsLayout.setVisible(false)
                        estimateImage.setVisible(false)
                    }

                    if (TextUtils.isEmpty(it.phone)) {
                        phoneImage.setVisible(false)
                        messageImage.setVisible(false)
                    }

                    userName.text = it.name

                    if (it.totalRating == null || it.totalRating == 0.0f) {
                        ratingText.setVisible(false)
                        star.setVisible(false)
                    } else {
                        ratingText.text = it.totalRating.toString()
                        star.setVisible(true)
                        ratingText.setVisible(true)
                    }

                    it.rating?.let {
                        if (it > 0) {
                            ratingBar.setVisible(true)
                            ratingBar.rating = it
                        }
                    }

                    if (it.comment.isNullOrEmpty()) {
                        commentToRating.setVisible(false)
                    } else {
                        commentToRating.setVisible(true)
                        commentToRating.text = it.comment
                    }

                    trip.user.photo?.let { it ->
                        if (it.isNotEmpty())
                            loadRoundedImage(context!!, avatarImage, url = it)
                    }
                }

                if (it.comment.isNullOrEmpty()) {
                    comment.setVisible(false)
                    commentText.setVisible(false)
                }
                commentText.text = it.comment

                if (TextUtils.isEmpty(it.comment) && (it.services == null || it.services.isEmpty())) {
                    additionalInfo.setVisible(false)
                }
                if (TextUtils.isEmpty(it.comment) && (it.services == null || it.services.isEmpty()) && trip.user.isRated == false) {
                    additionalLayout.setVisible(false)
                }

                it.services?.let {
                    if (it.isEmpty() && AcceptOrderFragment.checkService(it)) {
                        servicesContainer.setVisible(false)
                    } else {
                        servicesContainer.setVisible(true)
                        services.setActiveStatus(false)
                        for (item in it) {
                            services.addServices(item)
                        }
                    }
                } ?: run {
                    servicesContainer.setVisible(false)
                }
                trip.cancelMessage?.let {
                    faultText.text = it
                    faultText.setVisible(true)
                    servicesContainer.setVisible(false)
                    contactsLayout.setVisible(false)
                    comment.setVisible(false)
                    commentText.setVisible(false)
                    ratingBar.setVisible(false)
                    additionalInfo.setVisible(false)
                    estimateImage.setVisible(false)
                    faultText.setTextColor(resources.getColor(R.color.colorLiteGrey))
                }
            }
        }
    }

    private fun createEntry(text: String): TextView {
        val lparams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        lparams.bottomMargin = 10
        val tv = TextView(context)
        tv.setTextColor(resources.getColor(R.color.colorLiteGrey))
        tv.layoutParams = lparams
        tv.text = text
        return tv
    }

    private fun makeCallClick() {
        if (!isPermissionGranted(this@CompletedOrderFragment, Manifest.permission.CALL_PHONE)) {
            requestPermissions(arrayOf(Manifest.permission.CALL_PHONE), CALL_PHONE_REQUEST)
        } else {
            trip.user.phone?.let {
                dialUser(it)
            }
        }
    }

    private fun getPaymentResource(payment: PaymentMethod): Int {
        return when (payment) {
            PaymentMethod.CASH -> R.drawable.ic_cash
            PaymentMethod.CASH_AND_BONUS -> R.drawable.ic_cash
            PaymentMethod.CARD -> R.drawable.ic_card
            PaymentMethod.CARD_AND_BONUS -> R.drawable.ic_card
        }
    }

//    private fun sendSmsClick() {
//        if (!isPermissionGranted(this@CompletedOrderFragment, Manifest.permission.SEND_SMS)) {
//            requestPermissions(arrayOf(Manifest.permission.SEND_SMS), SEND_SMS_REQUEST)
//        } else {
//            trip.user.phone?.let {
//                sendMessage(it)
//            }
//        }
//    }

    private fun sendMessage(it: String) {
        if (isViberInstalled()) {
            openMessengerChooserDialog(it)
        } else {
            sendSms(it)
        }
    }


    private fun handleCamera() {
        if (orderAddressCoordinates.isNotEmpty()) {
            updateStartEndOrderMarkers(orderAddressCoordinates)
            updateMultipleTargetCamera(coordinates = orderAddressCoordinates)
        }
    }

//endregion

    companion object {
        const val ORDER_ID_KEY = "order_id"
        fun newInstance(orderId: Int): CompletedOrderFragment {
            val args = Bundle()
            args.putInt(ORDER_ID_KEY, orderId)
            val fragment = CompletedOrderFragment()
            fragment.arguments = args
            return fragment
        }
    }
}