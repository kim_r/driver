package com.wezom.taxi.driver.net.jobqueue

import android.content.Context
import com.wezom.taxi.driver.data.db.DbManager
import com.wezom.taxi.driver.net.api.ApiManager

/**
 * Created by zorin.a on 020 20.02.18.
 */

interface JobInjector {
    fun inject(apiManager: ApiManager, context: Context, dbManager: DbManager)
}