package com.wezom.taxi.driver.bus.events

/**
 * Created by zorin.a on 03.08.2018.
 */
class TaxometerTimeEvent constructor(val timeMillis: Long)