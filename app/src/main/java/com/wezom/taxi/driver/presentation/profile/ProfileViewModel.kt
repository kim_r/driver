package com.wezom.taxi.driver.presentation.profile

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import android.content.SharedPreferences
import android.net.Uri
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.bus.events.ProfileModeEvent
import com.wezom.taxi.driver.bus.events.UpdateCardDataEvent
import com.wezom.taxi.driver.common.*
import com.wezom.taxi.driver.data.models.Car
import com.wezom.taxi.driver.data.models.ResponseState
import com.wezom.taxi.driver.data.models.ResponseState.State.*
import com.wezom.taxi.driver.data.models.User
import com.wezom.taxi.driver.ext.*
import com.wezom.taxi.driver.net.request.RegistrationRequest
import com.wezom.taxi.driver.net.response.ProfileAndRegistrationCorrectnessResponse
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.presentation.profile.events.UpdateUserDataEvent
import com.wezom.taxi.driver.presentation.profile.events.UpdateUserImageEvent
import com.wezom.taxi.driver.repository.DriverRepository
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import timber.log.Timber
import javax.inject.Inject


/**
 * Created by zorin.a on 28.02.2018.
 */
class ProfileViewModel @Inject constructor(screenRouterManager: ScreenRouterManager,
                                           private val repository: DriverRepository,
                                           sharedPreferences: SharedPreferences) : BaseViewModel(screenRouterManager) {
    //region var
    private var userNameCached: String by sharedPreferences.string(USER_NAME_CACHED)
    private var userBalanceId by sharedPreferences.int(USER_BALANCE_ID)

    private var userImageUri: String by sharedPreferences.string(USER_IMAGE_URI)
    private var userImageForPrefs: String by sharedPreferences.string(USER_IMAGE_FOR_PREFS)
    private var carImageUri: String by sharedPreferences.string(CAR_IMAGE_URI)
    private var doc1Uri: String by sharedPreferences.string(FIRST_DOC_URI)
    private var doc2Uri: String by sharedPreferences.string(SECOND_DOC_URI)
    private var doc3Uri: String by sharedPreferences.string(THIRD_DOC_URI)
    private var doc4Uri: String by sharedPreferences.string(FOURTH_DOC_URI)

    private var userBitmapPart: MultipartBody.Part? = null
    private var carBitmapPart: MultipartBody.Part? = null
    private var doc1Part: MultipartBody.Part? = null
    private var doc2Part: MultipartBody.Part? = null
    private var doc3Part: MultipartBody.Part? = null
    private var doc4Part: MultipartBody.Part? = null

    var profileLiveData = MutableLiveData<ProfileAndRegistrationCorrectnessResponse>()
    //endregion

    //region fun
    fun getProfile() {
        loadingLiveData.postValue(ResponseState(LOADING))
        App.instance.getLogger()!!.log("profileAndRegistrationCorrectness start ")
        disposable += repository.profileAndRegistrationCorrectness()
                .subscribe({ response ->
                    App.instance.getLogger()!!.log("profileAndRegistrationCorrectness suc ")
                    handleResponseState(response)
                    if (response.isSuccess!!) {
                        profileLiveData.postValue(response)
                        val user = response.user
                        if (user != null) {
                            user.balanceId?.let {
                                userBalanceId = it
                            }
                        }
                        response.photos?.userPhoto?.let {
                            //update photo for settings screen
                            userImageForPrefs = it.url
                        }
                    }
                }, {
                    App.instance.getLogger()!!.log("profileAndRegistrationCorrectness error ")
                    loadingLiveData.postValue(ResponseState(NETWORK_ERROR))
                })
    }

    fun getProfileCard() {
        App.instance.getLogger()!!.log("profileAndRegistrationCorrectness start ")
        disposable += repository.profileAndRegistrationCorrectness()
                .subscribe({ response ->
                    App.instance.getLogger()!!.log("profileAndRegistrationCorrectness suc ")
                    if (response.isSuccess!!) {
                        RxBus.publish(UpdateCardDataEvent(response!!.user!!))
                    }
                }, {
                    App.instance.getLogger()!!.log("profileAndRegistrationCorrectness error ")
                    loadingLiveData.postValue(ResponseState(NETWORK_ERROR))
                })
    }

    @SuppressLint("CheckResult")
    fun sendUserData(user: User?, car: Car?, type: ArrayList<Int>?) {
        loadingLiveData.postValue(ResponseState(LOADING))
        App.instance.getLogger()!!.log("userRegistration start ")
        Single.fromCallable {
            val request = RegistrationRequest(user!!, car!!, type!!)
            disposable += repository.userRegistration(request)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ response ->
                        App.instance.getLogger()!!.log("userRegistration suc ")
                        loadingLiveData.postValue(ResponseState(ResponseState.State.IDLE))
                        handleResponseState(response)
                        if (response.isSuccess!!) {
                            context.getString(R.string.data_updated).shortToast(context)
                            userNameCached = user?.name!!
                            RxBus.publish(UpdateUserDataEvent())
                            getDriverState()
                        } else {
                            response.error!!.message!!.shortToast(context)
                        }
                    }, {
                        loadingLiveData.postValue(ResponseState(NETWORK_ERROR))
                    })
        }
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe({ _ ->
                }, {
                    App.instance.getLogger()!!.log("userRegistration error ")
                    Timber.e(it)
                })

    }

    @SuppressLint("CheckResult")
    fun sendImages() {
        Timber.d("sendImages()")
        if (noChangesToImages()) {
            context.getString(R.string.no_new_images).longToast(context)
        } else {
            loadingLiveData.postValue(ResponseState(ResponseState.State.LOADING))
            Single.fromCallable { prepareParts() }
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe({ _ ->
                        App.instance.getLogger()!!.log("registrationDocument start ")
                        disposable += repository.registrationDocument(userBitmapPart, carBitmapPart, doc1Part, doc2Part, doc3Part, doc4Part)
                                .subscribe(
                                        {
                                            App.instance.getLogger()!!.log("registrationDocument suc ")
                                            if (it.isSuccess!!) {
                                                if (isValid(userImageUri)) {
                                                    userImageForPrefs = userImageUri
                                                    RxBus.publish(UpdateUserImageEvent())//update user data
                                                }
                                                loadingLiveData.postValue(ResponseState(ResponseState.State.IDLE))
                                                context.getString(R.string.images_sent).longToast(context)
                                                clearImagesCache()
                                            } else {
                                                loadingLiveData.postValue(ResponseState(ResponseState.State.NETWORK_ERROR, message = it.error?.message))
                                            }
                                        },
                                        {
                                            App.instance.getLogger()!!.log("registrationDocument error ")
                                            loadingLiveData.postValue(ResponseState(ResponseState.State.NETWORK_ERROR, message = it.message))
                                        })


                    }, {
                        loadingLiveData.postValue(ResponseState(ResponseState.State.NETWORK_ERROR, message = it.message))
                    })
        }
    }

    private fun noChangesToImages(): Boolean {
        return !isValid(userImageUri)
                && !isValid(carImageUri)
                && !isValid(doc1Uri)
                && !isValid(doc2Uri)
                && !isValid(doc3Uri)
                && !isValid(doc4Uri)
    }

    private fun prepareParts() {
        if (isValid(userImageUri)) {
            userBitmapPart = createPart(userImageUri, "user[photo]")
        }

        if (isValid(carImageUri)) {
            carBitmapPart = createPart(carImageUri, "car[photo]")
        }

        if (isValid(doc1Uri)) {
            doc1Part = createPart(doc1Uri, "docs[1]")
        }

        if (isValid(doc2Uri)) {
            doc2Part = createPart(doc2Uri, "docs[2]")
        }

        if (isValid(doc3Uri)) {
            doc3Part = createPart(doc3Uri, "docs[3]")
        }

        if (isValid(doc4Uri)) {
            doc4Part = createPart(doc4Uri, "docs[4]")
        }
    }

    private fun isValid(input: String): Boolean = !(input.isEmpty() || input.startsWith("http"))

    private fun createPart(imageUriString: String, fileName: String): MultipartBody.Part? {
        val bitmap = decodeUriToBitmap(context, Uri.parse(imageUriString))
        val imageBody = MultipartBody.Part.createFormData(fileName,
                fileName,
                RequestBody.create(MediaType.parse("image/jpeg"),
                        bitmap.toByteArray()))
        bitmap.recycle()
        return imageBody
    }

//    fun register(user: User?, car: Car?) {
//        Timber.d("register() user car")
//        loadingLiveData.postValue(ResponseState(LOADING))
//
//        val userSingle = createUserAndCarSingle(user!!, car!!)
//
//
//        disposable += userSingle
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe({ response ->
//                    handleResponseState(response)
//                    if (response.isSuccess!!) {
//                        Timber.d("user data sent")
//                        userNameCached = user?.name!!
//                        RxBus.publish(UpdateUserDataEvent::class.java)//update user data
//                        loadingLiveData.postValue(ResponseState(IDLE))
//                        enterAsActiveUser()
//                        clearImagesCache()
//                        if (docsList.isNotEmpty()) {
//                            disposable += Single
//                                    .zip(docsList) {
//                                        var result = true
//
//                                        for (i in it) {
//                                            if (i is BaseResponse && i.isSuccess != true) {
//                                                Timber.e("Send doc failed:message=${i.error?.message}code=${i.error?.code}")
//                                                result = false
//                                            }
//                                        }
//                                        result
//                                    }.subscribeOn(Schedulers.io())
//                                    .observeOn(AndroidSchedulers.mainThread())
//                                    .subscribe({
//                                        Timber.d("docs sent: $it")
//                                        if (it) {
//                                            clearImagesCache()
//                                            enterAsActiveUser()
//                                            Timber.d("docs data sent")
//                                            loadingLiveData.postValue(ResponseState(IDLE))
//                                        }
//                                    }, {
//                                        loadingLiveData.postValue(ResponseState(NETWORK_ERROR, message = it.message))
//                                    })
////
//                        } else {
//                            loadingLiveData.postValue(ResponseState(IDLE))
//                            clearImagesCache()
//
//                        }
//                    }
//                }, { t ->
//                    loadingLiveData.postValue(ResponseState(NETWORK_ERROR, message = t.message))
//                })
//    }

    @SuppressLint("CheckResult")
    fun getDriverState() {
        App.instance.getLogger()!!.log("checkUserStatus start ")
        disposable += repository.checkUserStatus()
                .subscribe { response ->
                    App.instance.getLogger()!!.log("checkUserStatus suc ")
                    val state = setAppState(response.driverStatus!!, response.moderationStatus!!, response.accessToWorkWithoutModeration!!)
                    RxBus.publish(ProfileModeEvent(state))
                }
    }


    private fun clearImagesCache() {
        carImageUri = ""
        userImageUri = ""
        doc1Uri = ""
        doc2Uri = ""
        doc3Uri = ""
        doc4Uri = ""

        userBitmapPart = null
        carBitmapPart = null
        doc1Part = null
        doc2Part = null
        doc3Part = null
        doc4Part = null
    }

    //   private fun createUserAndCarSingle(user: User, car: Car): Single<BaseResponse> {
//        loadingLiveData.postValue(ResponseState(LOADING))
//        var userBitmapPart: MultipartBody.Part? = null
//        var carBitmapPart: MultipartBody.Part? = null

//        if (!userImageUri.isBlank() && !userImageUri.startsWith("http")) {
//            val userBitmap = decodeUriToBitmap(context, Uri.parse(userImageUri))
//            val userImageFile = userBitmap.let {
//                File(context.cacheDir, "usertemp").apply {
//                    createNewFile()
//                    putBitmap(userBitmap)
//                }
//            }
//            userBitmapPart = MultipartBody.Part.createFormData(
//                    "user[photo]",
//                    userImageFile.name,
//                    RequestBody.create(MediaType.parse("image/*"), userImageFile)
//            )
//        }

//        if (!carImageUri.isBlank()) {
//            val carBitmap = decodeUriToBitmap(context, Uri.parse(carImageUri))
//            val carImageFile = carBitmap.let {
//                File(context.cacheDir, "cartemp").apply {
//                    createNewFile()
//                    putBitmap(carBitmap)
//                }
//            }
//            carBitmapPart = MultipartBody.Part.createFormData(
//                    "car[photo]",
//                    carImageFile.name,
//                    RequestBody.create(MediaType.parse("image/*"), carImageFile)
//            )
//        }

//        val userMap = HashMap<String, RequestBody>()
//        userMap["user[name]"] = user?.name.toRequestBody()
//        userMap["user[surname]"] = user?.surname.toRequestBody()
//        userMap["user[email]"] = user?.email.toRequestBody()
//        val invitationCode = user?.invitationCode
//        if (!invitationCode.isNullOrBlank()) {
//            userMap["user[invitationCode]"] = invitationCode.toRequestBody()
//        }
//
//        val carMap = HashMap<String, RequestBody>()
//        carMap["car[brandId]"] = car?.brand?.id.toString().toRequestBody()
//        carMap["car[modelId]"] = car?.model?.id.toString().toRequestBody()
//        carMap["car[year]"] = car?.year.toString().toRequestBody()
//        carMap["car[colorId]"] = car?.color?.id.toString().toRequestBody()
//        carMap["car[number]"] = car?.number.toString().toRequestBody()
    //val request = RegistrationRequest(user, car)
    //   return repository.userRegistration(request)
//    }

//    private fun createDocuments(): List<Single<BaseResponse>> {
//        val list = ArrayList<Single<BaseResponse>>()
//
//        if (doc1Uri.isNotBlank()) {
//            val p1 = prepareDoc(doc1Uri, 1)
//            list += repository.registrationDocument(p1.first, p1.second)
//        }
//        if (doc2Uri.isNotBlank()) {
//            val p2 = prepareDoc(doc2Uri, 2)
//            list += repository.registrationDocument(p2.first, p2.second)
//        }
//        if (doc3Uri.isNotBlank()) {
//            val p3 = prepareDoc(doc3Uri, 3)
//            list += repository.registrationDocument(p3.first, p3.second)
//        }
//        if (doc4Uri.isNotBlank()) {
//            val p4 = prepareDoc(doc4Uri, 4)
//            list += repository.registrationDocument(p4.first, p4.second)
//        }
//        return list
//    }

//    private fun prepareDoc(docUri: String, docId: Int): Pair<Map<String, RequestBody>, MultipartBody.Part> {
//
//        Timber.d("prepare doc: $docId docUri: $docUri")
//        val map = HashMap<String, RequestBody>()
//        map["id"] = docId.toString().toRequestBody()
//
//        val bitmap = decodeUriToBitmap(context, Uri.parse(docUri))
//        val docImageFile = bitmap.let {
//            File(context.cacheDir, "temp_$docId").apply {
//                createNewFile()
//                putBitmap(bitmap)
//            }
//        }
//        val doc = MultipartBody.Part.createFormData(
//                "photo",
//                docImageFile.name,
//                RequestBody.create(MediaType.parse("image/*"), docImageFile)
//
//        )
//        return Pair(map, doc)
//    }


//endregion
}