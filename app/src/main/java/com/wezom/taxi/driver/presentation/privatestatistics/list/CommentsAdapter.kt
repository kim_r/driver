package com.wezom.taxi.driver.presentation.privatestatistics.list

import android.view.ViewGroup
import com.wezom.taxi.driver.data.models.Comment
import com.wezom.taxi.driver.presentation.base.lists.BaseAdapter
import com.wezom.taxi.driver.presentation.base.lists.BaseViewHolder
import javax.inject.Inject

/**
 * Created by zorin.a on 15.03.2018.
 */
class CommentsAdapter @Inject constructor() :
        BaseAdapter<Comment, CommentsItemView, BaseViewHolder<CommentsItemView>>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<CommentsItemView> {
        val itemView = CommentsItemView(parent.context)
        return BaseViewHolder(itemView)
    }
}