package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.map.GeocodedWayPoint
import com.wezom.taxi.driver.data.models.map.Route

/**
 * Created by zorin.a on 11.05.2018.
 */
class RouteResponse constructor(@SerializedName("status")
                                val status: String? = null,
                                @SerializedName("geocoded_waypoints")
                                var geocodedWaypoints: List<GeocodedWayPoint>? = null,
                                @SerializedName("routes")
                                val routes: List<Route>? = null
)