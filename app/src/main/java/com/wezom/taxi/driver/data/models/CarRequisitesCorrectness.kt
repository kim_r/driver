package com.wezom.taxi.driver.data.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


/**
 * Created by zorin.a on 021 21.02.18.
 */

data class CarRequisitesCorrectness(
        @SerializedName("isBrandCorrect") val isBrandCorrect: Boolean?,
        @SerializedName("isModelCorrect") val isModelCorrect: Boolean?,
        @SerializedName("isYearCorrect") val isYearCorrect: Boolean?,
        @SerializedName("isColorCorrect") val isColorCorrect: Boolean?,
        @SerializedName("isNumberCorrect") val isNumberCorrect: Boolean?,
        @SerializedName("isPhotoCorrect") val isPhotoCorrect: Boolean?) : Parcelable {
    constructor(source: Parcel) : this(
            source.readValue(Boolean::class.java.classLoader) as Boolean?,
            source.readValue(Boolean::class.java.classLoader) as Boolean?,
            source.readValue(Boolean::class.java.classLoader) as Boolean?,
            source.readValue(Boolean::class.java.classLoader) as Boolean?,
            source.readValue(Boolean::class.java.classLoader) as Boolean?,
            source.readValue(Boolean::class.java.classLoader) as Boolean?
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeValue(isBrandCorrect)
        writeValue(isModelCorrect)
        writeValue(isYearCorrect)
        writeValue(isColorCorrect)
        writeValue(isNumberCorrect)
        writeValue(isPhotoCorrect)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<CarRequisitesCorrectness> = object : Parcelable.Creator<CarRequisitesCorrectness> {
            override fun createFromParcel(source: Parcel): CarRequisitesCorrectness = CarRequisitesCorrectness(source)
            override fun newArray(size: Int): Array<CarRequisitesCorrectness?> = arrayOfNulls(size)
        }
    }
}