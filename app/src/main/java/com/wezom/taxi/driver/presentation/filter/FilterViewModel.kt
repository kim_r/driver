package com.wezom.taxi.driver.presentation.filter

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.widget.Switch
import com.wezom.taxi.driver.common.CREATE_FILTER_SCREEN
import com.wezom.taxi.driver.common.EDIT_FILTER_SCREEN
import com.wezom.taxi.driver.data.models.ResponseState
import com.wezom.taxi.driver.ext.longToast
import com.wezom.taxi.driver.net.response.FilterResponse
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.repository.DriverRepository
import javax.inject.Inject

/**
 *Created by Zorin.A on 25.June.2019.
 */
class FilterViewModel @Inject constructor(
        private val repository: DriverRepository,
        private val ctx: Context,
        screenRouterManager: ScreenRouterManager) : BaseViewModel(screenRouterManager) {

    val filterLiveData = MutableLiveData<List<FilterResponse>>()

    fun addFilter() {
        replaceScreen(CREATE_FILTER_SCREEN)
    }

    fun editFilter(filter: FilterResponse) {
        replaceScreen(EDIT_FILTER_SCREEN, filter)
    }

    @SuppressLint("CheckResult")
    fun getFilter() {
        loadingLiveData.postValue(ResponseState(ResponseState.State.LOADING))
        App.instance.getLogger()!!.log("getFilter start")
        repository.getFilter().subscribe({ it ->
            App.instance.getLogger()!!.log("getFilter suc")
            if (it.isSuccess!!) {
                filterLiveData.value = it.result
                loadingLiveData.postValue(ResponseState(ResponseState.State.IDLE))
            }
        }, {
            App.instance.getLogger()!!.log("getFilter error")
            loadingLiveData.postValue(ResponseState(ResponseState.State.ERROR, message = it?.message)) })
    }

    @SuppressLint("CheckResult")
    fun activateFilter(filterId: Int, switch: Switch) {
        repository.activateFilter(filterId).subscribe({
            if (it.isSuccess!!) {
            } else {
                switch.isChecked = false
                it?.error!!.message!!.longToast(ctx)
            }
        }, { it?.message!!.longToast(ctx) })
    }

    @SuppressLint("CheckResult")
    fun deactivateFilter(filterId: Int, switch: Switch) {
        App.instance.getLogger()!!.log("deactivateFilter start")
        repository.deactivateFilter(filterId).subscribe({
            App.instance.getLogger()!!.log("deactivateFilter suc")
            if (it.isSuccess!!) {
            } else {
                switch.isChecked = false
                it?.error!!.message!!.longToast(ctx)
            }
        }, {
            App.instance.getLogger()!!.log("deactivateFilter error")
            it?.message!!.longToast(ctx) })
    }

}