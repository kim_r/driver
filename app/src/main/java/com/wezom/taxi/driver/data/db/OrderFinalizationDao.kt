package com.wezom.taxi.driver.data.db

import android.arch.persistence.room.*
import com.wezom.taxi.driver.data.models.OrderFinalization

@Dao
interface OrderFinalizationDao {
    @Query("SELECT * FROM 'order_finalization'")
    fun queryFinalization(): OrderFinalization?

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateFinalization(orderFinalization: OrderFinalization)


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFinalization(orderFinalization: OrderFinalization): Long

    @Delete
    fun delete(orderFinalization: OrderFinalization)

    @Query("DELETE FROM 'order_finalization'")
    fun clearDatabase()
}