package com.wezom.taxi.driver.presentation.settings.dialog

import android.app.Dialog
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.databinding.ThemeSwitchDialogBinding

/**
 * Created by udovik.s on 14.03.2018.
 */
class PickThemeDialog : DialogFragment() {

    private lateinit var binding: ThemeSwitchDialogBinding
    var listener: ThemeClickListener? = null
    private var pos: Int? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        pos = arguments?.getInt(CHECKED_ITEM_KEY)
        binding = DataBindingUtil.inflate(activity!!.layoutInflater, R.layout.theme_switch_dialog, null, false)
        updateUi()
        val builder = AlertDialog.Builder(context!!, R.style.WhiteDialogTheme)
        builder.setView(binding.root)

        return builder.create()
    }

    fun updateUi() {
        binding.run {
            title.text = getString(R.string.theme_choice)
            when (pos) {
                DARK -> darkTheme.isChecked = true
                LIGHT -> lightTheme.isChecked = true
            }
            container.setOnCheckedChangeListener { group, checkedId ->
                when (checkedId) {
                    darkTheme.id -> listener?.onThemeSelected(DARK)
                    lightTheme.id -> listener?.onThemeSelected(LIGHT)
                }
            }
        }
    }

    interface ThemeClickListener {
        fun onThemeSelected(position: Int)
    }

    companion object {
        val TAG: String = PickThemeDialog::class.java.simpleName
        const val CHECKED_ITEM_KEY: String = "checked_item_key"
        const val DARK = 0
        const val LIGHT = 1
        fun newInstance(checkedItem: Int? = 0): PickThemeDialog {
            val args = Bundle()
            val fragment = PickThemeDialog()
            checkedItem?.let { args.putInt(CHECKED_ITEM_KEY, it) }
            fragment.arguments = args
            return fragment
        }
    }
}