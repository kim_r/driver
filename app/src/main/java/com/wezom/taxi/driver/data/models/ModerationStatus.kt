package com.wezom.taxi.driver.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by zorin.a on 25.04.2018.
 */
enum class ModerationStatus(value: Int) {
    @SerializedName("0")
    NOT_VERIFIED(0),
    @SerializedName("1")
    VERIFIED(1),
    @SerializedName("2")
    MODERATION(2)
}