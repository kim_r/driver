package com.wezom.taxi.driver.data.models.notifications


class SuccessfulModeration(val typeId: String, val title: String, val body: String): BasicNotification