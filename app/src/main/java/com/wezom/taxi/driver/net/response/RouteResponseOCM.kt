package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.Error

class RouteResponseOCM {
    @SerializedName("success")
    var isSuccess: Boolean? = null
    @SerializedName("error")
    var error: Error? = null
    @SerializedName("data")
    var routeOCM: RouteOCM? = null


    constructor(isSuccess: Boolean?, error: Error?, routeOCM: RouteOCM?) {
        this.isSuccess = isSuccess
        this.error = error
        this.routeOCM = routeOCM
    }

    constructor()
}