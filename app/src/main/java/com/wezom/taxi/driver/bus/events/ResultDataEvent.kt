package com.wezom.taxi.driver.bus.events

import android.arch.persistence.room.Embedded
import android.os.Parcel
import android.os.Parcelable
import com.wezom.taxi.driver.data.models.*
import java.util.concurrent.CopyOnWriteArrayList

/**
 * Created by zorin.a on 16.05.2018.
 */
class ResultDataEvent constructor(var resultCost: Int? = null,
                                  var factCost: Int? = null,
                                  var estimatedCost: Int? = null,
                                  var resultF3: Int? = null,
                                  var bonus: Int? = null,
                                  var estimatedBonus: Int? = null,
                                  var surcharge: Int? = null,
                                  var paymentMethod: PaymentMethod? = null,
                                  var changedParams: ChangedParams? = null,
                                  @Embedded(prefix = "result_") var user: User? = null,
                                  var address: List<Address>? = null,
                                  var factTime: Long? = null,
                                  var factPath: Double? = null,
                                  var waitingTime: Long? = null,
                                  var waitingCost: Double? = null,
                                  @Embedded(prefix = "result_order_") var currentCoordinate: Coordinate? = null,
                                  @Embedded(prefix = "result_") var passedRoute: CopyOnWriteArrayList<Coordinate>? = null,
                                  @Embedded(prefix = "result_order") var orderInfo: NewOrderInfo? = null,
                                  var isEstimatedCostTaken: Boolean? = null,
                                  var counterTimer: Long? = null,
                                  var realCounterTimer: Long? = null,
                                  var realSecond: Long? = null,
                                  var startTimeWaitPassager: Long? = null,
                                  var countTrip: Long? = null,
                                  var passedDistanceInsideCity: Double? = null,
                                  var passedDistanceOutsideCity: Double? = null

) : Parcelable {
    constructor(source: Parcel) : this(
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader)?.let { PaymentMethod.values()[it as Int] },
            source.readValue(Int::class.java.classLoader)?.let { ChangedParams.values()[it as Int] },
            source.readParcelable<User>(User::class.java.classLoader),
            source.createTypedArrayList(Address.CREATOR),
            source.readValue(Long::class.java.classLoader) as Long?,
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readValue(Long::class.java.classLoader) as Long?,
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readParcelable<Coordinate>(Coordinate::class.java.classLoader),
            source.readSerializable() as CopyOnWriteArrayList<Coordinate>?,
            source.readParcelable<NewOrderInfo>(NewOrderInfo::class.java.classLoader),
            source.readValue(Boolean::class.java.classLoader) as? Boolean,
            source.readValue(Int::class.java.classLoader) as Long?,
            source.readValue(Int::class.java.classLoader) as Long?,
            source.readValue(Int::class.java.classLoader) as Long?,
            source.readValue(Int::class.java.classLoader) as Long?,
            source.readValue(Int::class.java.classLoader) as Long?,
            source.readValue(Int::class.java.classLoader) as Double?,
            source.readValue(Int::class.java.classLoader) as Double?
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeValue(resultCost)
        writeValue(factCost)
        writeValue(estimatedCost)
        writeValue(resultF3)
        writeValue(bonus)
        writeValue(estimatedBonus)
        writeValue(surcharge)
        writeValue(paymentMethod?.ordinal)
        writeValue(changedParams?.ordinal)
        writeParcelable(user, 0)
        writeTypedList(address)
        writeValue(factTime)
        writeValue(factPath)
        writeValue(waitingTime)
        writeValue(waitingCost)
        writeParcelable(currentCoordinate, 0)
        writeSerializable(passedRoute)
        writeParcelable(orderInfo, 0)
        writeValue(isEstimatedCostTaken)
        writeValue(counterTimer)
        writeValue(realCounterTimer)
        writeValue(realSecond)
        writeValue(startTimeWaitPassager)
        writeValue(countTrip)
        writeValue(passedDistanceInsideCity)
        writeValue(passedDistanceOutsideCity)
    }

    override fun toString(): String {
        return "ResultDataEvent(result=$resultCost, factResultPrice=$factCost, bonus=$bonus, " +
                "surcharge=$surcharge, paymentMethod=$paymentMethod, changedParams=$changedParams, " +
                "user=$user, address=$address, factTime=$factTime, factPath=$factPath, " +
                "currentCoordinate=$currentCoordinate, passedRoute=$passedRoute, orderInfo=$orderInfo)"
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<ResultDataEvent> = object : Parcelable.Creator<ResultDataEvent> {
            override fun createFromParcel(source: Parcel): ResultDataEvent = ResultDataEvent(source)
            override fun newArray(size: Int): Array<ResultDataEvent?> = arrayOfNulls(size)
        }
    }


}

