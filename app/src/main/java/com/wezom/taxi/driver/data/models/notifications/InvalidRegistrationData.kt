package com.wezom.taxi.driver.data.models.notifications

class InvalidRegistrationData(val typeId: String, val title: String, val body: String, val isOrderRunning: Boolean) : BasicNotification