package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName


/**
 * Created by zorin.a on 25.06.2018.
 */

data class RequiredDocumentsResponse constructor(@SerializedName("necessaryDocumentsId") val necessaryDocumentsId: List<Int>,
                                                 @SerializedName("isCarPhotoRequired") val isCarPhotoRequired: Boolean,
                                                 @SerializedName("isDriverPhotoRequired") val isDriverPhotoRequired: Boolean) : BaseResponse(

)