package com.wezom.taxi.driver.net.request

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class CreateFilterRequest
constructor(
        @SerializedName("name") var name: String?,
        @SerializedName("address") var address: String?,
        @SerializedName("from_latitude") var fromLatitude: Double?,
        @SerializedName("from_longitude") var fromLongitude: Double?,
        @SerializedName("from_radius") var fromRadius: Double?,
        @SerializedName("to_latitude") var toLatitude: Double?,
        @SerializedName("to_longitude") var toLongitude: Double?,
        @SerializedName("to_radius") var toRadius: Double?
) : Serializable {

    constructor() : this(null,
            null,
            null,
            null,
            null,
            null,
            null,
            null)

    constructor(name: String?) : this(name,
            null,
            null,
            null,
            null,
            null,
            null,
            null)


    constructor(name: String?,
                fromLatitude: Double?,
                fromLongitude: Double?,
                fromRadius: Double?) : this(name,
            null,
            fromLatitude,
            fromLongitude,
            fromRadius,
            null,
            null,
            null)

    constructor(name: String?,
                toLatitude: Double?,
                toLongitude: Double?,
                toRadius: Double?,
                address: String?) : this(name,
            address,
            null,
            null,
            null,
            toLatitude,
            toLongitude,
            toRadius)
}