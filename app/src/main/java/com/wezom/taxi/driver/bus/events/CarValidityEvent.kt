package com.wezom.taxi.driver.bus.events

/**
 * Created by zorin.a on 25.06.2018.
 */
data class CarValidityEvent constructor(val isBrandValid: Boolean, val isModelValid: Boolean,
                                        val isColorValid: Boolean, val isYearValid: Boolean, val isNumberValid: Boolean)