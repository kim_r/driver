package com.wezom.taxi.driver.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by udovik.s on 18.04.2018.
 */
data class Phone(
        @SerializedName("phone") val phone: String?
)