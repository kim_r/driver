package com.wezom.taxi.driver.bus

import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable

/**
 * Created by zorin.a on 23 23.02.18.
 */
object RxBus {
    private val publisher = PublishRelay.create<Any>()

    fun publish(event: Any) =  publisher.accept(event)

    fun <T> listen(eventType: Class<T>): Observable<T> = publisher.ofType(eventType)
}