package com.wezom.taxi.driver.presentation.registration

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.DialogInterface
import android.os.Bundle
import android.text.InputFilter
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.Switch
import android.widget.TextView
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.data.models.Car
import com.wezom.taxi.driver.data.models.CarParameter
import com.wezom.taxi.driver.data.models.User
import com.wezom.taxi.driver.databinding.SimpleRegistrationBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.ext.hideKeyboard
import com.wezom.taxi.driver.ext.isEmpty
import com.wezom.taxi.driver.ext.setErrorColor
import com.wezom.taxi.driver.presentation.base.BaseFragment
import com.wezom.taxi.driver.presentation.profile.AboutCarFragment
import com.wezom.taxi.driver.presentation.profile.dialog.CarChoiceDialog
import com.wezom.taxi.driver.presentation.profile.dialog.CarColorDialog
import com.wezom.taxi.driver.presentation.profile.dialog.CarYearDialog
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxCompoundButton
import com.jakewharton.rxbinding2.widget.RxTextView
import com.jakewharton.rxbinding2.widget.TextViewTextChangeEvent
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber
import java.util.concurrent.TimeUnit

/**
 * Created by zorin.a on 07.12.2018.
 */
class SimpleRegistrationFragment : BaseFragment() {
    //region var
    private lateinit var binding: SimpleRegistrationBinding
    private lateinit var viewModel: SimpleRegistrationViewModel

    private var carBrands: List<CarParameter>? = null
    private var carModels: List<CarParameter>? = null
    private var carColors: List<CarParameter>? = null
    private var carType: List<CarParameter>? = null
    private var carYears: List<Int>? = null

    private var checkedBrandPos = AboutCarFragment.UNSELECTED
    private var checkedModelPos = AboutCarFragment.UNSELECTED
    private var checkedColorPos = AboutCarFragment.UNSELECTED
    private var checkedYearPos = AboutCarFragment.UNSELECTED

    private var chosenBrand: CarParameter? = null
    private var chosenModel: CarParameter? = null
    private var typeCar: MutableList<CarParameter>? = null
    private var chosenCarColor: CarParameter? = null
    private var chosenCarYear: Int? = null
    private var isAgreementAccepted: Boolean = false

    private val carBrandObserver = Observer<List<CarParameter>> { brands ->
        carBrands = brands
    }
    private val carTypeObserver = Observer<List<CarParameter>> { type ->
        type?.let {
            typeCar = mutableListOf(type[0])
            carType = type
        }
    }
    private val carModelObserver = Observer<List<CarParameter>> { models ->
        carModels = models
    }
    private val carColorObserver = Observer<List<CarParameter>> { colors ->
        carColors = colors
    }
    private val carYearsObserver = Observer<List<Int>> { years ->
        carYears = years
    }
    private val kbObserver = Observer<Boolean> {
        activity?.hideKeyboard()
        Timber.d("kbObserver: $it`")
    }
    //endregion

    //region override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
        setBackPressFun { viewModel.onBackPressed() }
        initLiveData()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = SimpleRegistrationBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        initViews()
        initClicks()
        getData()
    }

    private fun initClicks() {
        binding.run {
            disposable += RxView.clicks(carBrandEditText).subscribe({

                val dialog = CarChoiceDialog.newInstance(carBrands as ArrayList<CarParameter>, checkedBrandPos)
                dialog.show(childFragmentManager, CarChoiceDialog.TAG)
                dialog.listener = object : CarChoiceDialog.ChoiceDialogSelection {
                    override fun onSelected(pos: Int, CarParameter: CarParameter) {
                        viewModel.getCarModels(CarParameter.id)
                        chosenBrand = CarParameter
                        checkedBrandPos = pos
                        binding.carBrandEditText.setText(CarParameter.name)

                        binding.carModelEditText.text = null
                        checkedModelPos = AboutCarFragment.UNSELECTED

                        binding.yearEditText.text = null
                        checkedYearPos = AboutCarFragment.UNSELECTED

                        chosenModel = null
                        chosenCarYear = null
                    }
                }
            }, { Timber.e(it) })

            disposable += RxView.clicks(carModelEditText).subscribe({
                if (carModels != null) {
                    val dialog = CarChoiceDialog.newInstance(carModels as ArrayList<CarParameter>, checkedModelPos)
                    dialog.show(childFragmentManager, CarChoiceDialog.TAG)
                    dialog.listener = object : CarChoiceDialog.ChoiceDialogSelection {
                        override fun onSelected(pos: Int, CarParameter: CarParameter) {
                            chosenModel = CarParameter
                            checkedModelPos = pos
                            binding.carModelEditText.setText(CarParameter.name)
                            if (isBrandAndModelChecked()) {
                                viewModel.getCarIssueYears(carModels?.get(checkedModelPos)!!.id)
                            }
//                            publishCar()
                        }
                    }
                }
            }, { Timber.e(it) })

            disposable += RxView.clicks(colorEditText).subscribe({
                if (carColors != null) {
                    val dialog = CarColorDialog.newInstance(carColors as ArrayList<CarParameter>, checkedColorPos)
                    dialog.show(childFragmentManager, CarColorDialog.TAG)
                    dialog.listener = object : CarColorDialog.ChoiceDialogSelection {
                        override fun onSelected(pos: Int, carColor: CarParameter) {
                            chosenCarColor = carColor
                            checkedColorPos = pos
                            binding.colorEditText.setText(carColor.name)
//                            publishCar()
                        }
                    }
                }
            }, { Timber.e(it) })

            disposable += RxView.clicks(yearEditText).subscribe({
                if (carYears != null) {
                    val dialog = CarYearDialog.newInstance(carYears as ArrayList<Int>, checkedYearPos)
                    dialog.show(childFragmentManager, CarYearDialog.TAG)
                    dialog.listener = object : CarYearDialog.ChoiceDialogSelection {
                        override fun onSelected(position: Int, carYear: Int) {
                            chosenCarYear = carYear
                            checkedYearPos = position
                            binding.yearEditText.setText(carYear.toString())
//                            publishCar()
                        }
                    }
                }
            }, { Timber.e(it) })

//            economSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
//                carType?.let {
//                    if (isChecked) {
//                        typeCar!!.add(carType!![0])
//                    } else {
//                        typeCar!!.removeAt(0)
//                    }
//                }
//            }
//            uniSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
//                carType?.let {
//                    if (isChecked) {
//                        typeCar!!.add(carType!![1])
//                    } else {
//                        typeCar!!.removeAt(1)
//                    }
//                }
//            }
//            minibusSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
//                carType?.let {
//                    if (isChecked) {
//                        typeCar!!.add(carType!![2])
//                    } else {
//                        typeCar!!.removeAt(2)
//                    }
//                }
//            }

            disposable += RxView.clicks(doRegisterButton).subscribe {
                tryRegister()
            }
        }
    }

    private fun tryRegister() {
        if (isDataCorrect()) {
            if (isAgreementAccepted) {
                register()
            } else {
                showErrorDialog(error = getString(R.string.have_to_accept_agreement))
            }
        } else {
            showErrorDialog(getString(R.string.error), generateRegisterErrorBody(),
                    DialogInterface.OnClickListener { p0, p1 ->
                        disposable += Single.timer(100, TimeUnit.MILLISECONDS)
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({
                                    //crunches to make fields highlighted
                                    isDataCorrect()
                                }, {})
                    })
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.registration_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == R.id.action_register) {
            tryRegister()
            true
        } else {
            false
        }
    }

    //endregion

    //region fun
    private fun initViews() {
        binding.run {
            toolbar.setToolbarTitle(getString(R.string.registration))
            stateView.isOnlyDialogMode = true

            val name = RxTextView.textChangeEvents(nameEditText)
            val surname = RxTextView.textChangeEvents(surnameEditText)
            val brand = RxTextView.textChangeEvents(carBrandEditText)
            val model = RxTextView.textChangeEvents(carModelEditText)
            val color = RxTextView.textChangeEvents(colorEditText)
            val year = RxTextView.textChangeEvents(yearEditText)
            val number = RxTextView.textChangeEvents(carNumberEditText)
            val reg = RxCompoundButton.checkedChanges(registerCheckbox)

            val list: MutableList<Observable<TextViewTextChangeEvent>> = mutableListOf()
            list.add(name)
            list.add(surname)
            list.add(brand)
            list.add(model)
            list.add(color)
            list.add(year)
            list.add(number)

            disposable += Observable.merge(list).subscribe {
                validateRegistrationButtonState()
            }
            disposable += reg.subscribe { isChecked ->
                isAgreementAccepted = isChecked
                validateRegistrationButtonState()
            }

            carNumberEditText.filters + InputFilter.AllCaps()
            inviteCodeEditText.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    tryRegister()
                    return@OnEditorActionListener true
                }
                false
            })
        }
    }

    private fun initLiveData() {
        viewModel.drawerLockLiveData.observe(this@SimpleRegistrationFragment, Observer { locked ->
            Timber.d("isLocked profile observer: $locked")
            lockDrawer(locked!!)
        })
        viewModel.loadingLiveData.observe(this, progressObserver)
        viewModel.carBrandsLiveData.observe(this, carBrandObserver)
        viewModel.carTypeLiveData.observe(this, carTypeObserver)
        viewModel.carModelsLiveData.observe(this, carModelObserver)
        viewModel.carColorsLiveData.observe(this, carColorObserver)
        viewModel.carYearsLiveData.observe(this, carYearsObserver)
        viewModel.keyboardLiveData.observe(this, kbObserver)
    }

    private fun isBrandAndModelChecked(): Boolean = checkedBrandPos != AboutCarFragment.UNSELECTED && checkedModelPos != AboutCarFragment.UNSELECTED

    private fun getData() {
        viewModel.getCarBrands()
        viewModel.getCarColors()
        viewModel.getType()
    }

    private fun validateRegistrationButtonState() {
        if (isAgreementAccepted && isDataCorrect()) {
            binding.doRegisterButton.setBackgroundColor(resources.getColor(R.color.colorAccent2))
        } else {
            binding.doRegisterButton.setBackgroundColor(resources.getColor(R.color.colorLiteGrey))
        }
    }

    private fun isDataCorrect(): Boolean {
        binding.run {
            return !checkIsEmpty(nameEditText) and
                    !checkIsEmpty(surnameEditText) and
                    !checkIsEmpty(carBrandEditText) and
                    !checkIsEmpty(carModelEditText) and
                    !checkIsEmpty(colorEditText) and
//                    !checkIsEnable(arrayListOf(economSwitch, uniSwitch, minibusSwitch)) and
                    !checkIsEmpty(yearEditText) and
                    !checkIsEmpty(carNumberEditText)
        }
    }

    private fun checkIsEmpty(inputView: EditText): Boolean {
        return if (inputView.isEmpty()) {
            inputView.setErrorColor()
            true
        } else {
            false
        }
    }

    private fun checkIsEnable(inputView: List<Switch>): Boolean {
        for (item in inputView)
            if (item.isChecked)
                return false
        return true
    }

    private fun generateRegisterErrorBody(): String {
        val intro = "${getString(R.string.fill_all_user_fields)} \n"
        val listEmptyFields = mutableListOf<String>()

        binding.run {
            if (checkIsEmpty(nameEditText)) {
                listEmptyFields.add(getString(R.string.name))
            }
            if (checkIsEmpty(surnameEditText)) {
                listEmptyFields.add(getString(R.string.surname))
            }
            if (checkIsEmpty(carBrandEditText)) {
                listEmptyFields.add(getString(R.string.car_brand))
            }
            if (checkIsEmpty(carModelEditText)) {
                listEmptyFields.add(getString(R.string.car_model))
            }
            if (checkIsEmpty(colorEditText)) {
                listEmptyFields.add(getString(R.string.color))
            }
            if (checkIsEmpty(yearEditText)) {
                listEmptyFields.add(getString(R.string.manufacturing_year))
            }
            if (checkIsEmpty(carNumberEditText)) {
                listEmptyFields.add(getString(R.string.car_number))
            }
//            if (checkIsEnable(arrayListOf(economSwitch, uniSwitch, minibusSwitch))) {
//                listEmptyFields.add(getString(R.string.type_car))
//            }
        }

        val str = StringBuilder()
        str.append(intro)

        for (f in listEmptyFields) {
            str.appendln(f)
        }
        return str.toString()
    }

    private fun register() {
        binding.run {
            val car = Car().apply {
                model = chosenModel
                brand = chosenBrand
                //type = typeCar
                color = chosenCarColor
                year = chosenCarYear
                number = carNumberEditText.text.toString().toUpperCase()
            }
            val user = User().apply {
                name = nameEditText.text.toString()
                surname = surnameEditText.text.toString()
                invitationCode = inviteCodeEditText.text.toString()
            }
            viewModel.register(user, car)
        }
    }
//endregion

    companion object {
        fun newInstance(): SimpleRegistrationFragment {
            return SimpleRegistrationFragment()
        }
    }
}