package com.wezom.taxi.driver.data.models

import com.google.gson.annotations.SerializedName



/**
 * Created by andre on 22.03.2018.
 */
class Income (
    @SerializedName("id") var id: Int,
    @SerializedName("serviceName") var serviceName: String,
    @SerializedName("paymentName") var paymentName: String,
    @SerializedName("time") var time: Long,
    @SerializedName("newMoney") var newMoney: Double,
    @SerializedName("oldMoney") var oldMoney: Double
)