package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.NewOrderInfo
import com.wezom.taxi.driver.data.models.RestoredData


/**
 * Created by zorin.a on 022 22.02.18.
 */

data class CurrentOrdersResponse(@SerializedName("orders") val orders: List<NewOrderInfo>,
                                 @SerializedName("restoredData") val restoredData: RestoredData?) : BaseResponse()
