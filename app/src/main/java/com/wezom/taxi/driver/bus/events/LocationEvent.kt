package com.wezom.taxi.driver.bus.events

import android.location.Location

class LocationEvent constructor(val location: Location)