package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName

class RouteOCM {
    @SerializedName("distance")
    var distance: Double? = null
    @SerializedName("distanceInsideCity")
    var distanceInsideCity: Double? = null
    @SerializedName("distanceOutsideCity")
    var distanceOutsideCity: Double? = null
    @SerializedName("duration")
    var duration: Double? = null
    @SerializedName("route")
    var pointsOCM: List<PointsOCM>? = null


    constructor(distance: Double?, distanceInsideCity: Double?, distanceOutsideCity: Double?, duration: Double?,
                pointsOCM: List<PointsOCM>?) {
        this.distance = distance
        this.distanceInsideCity = distanceInsideCity
        this.distanceOutsideCity = distanceOutsideCity
        this.duration = duration
        this.pointsOCM = pointsOCM
    }

    constructor()

}