package com.wezom.taxi.driver.bus.events

/**
 * Created by zorin.a on 25.06.2018.
 */
data class UserValidityEvent constructor(val nameCorrect: Boolean,
                                         val surnameCorrect: Boolean,
                                         val mailCorrect: Boolean? = true)