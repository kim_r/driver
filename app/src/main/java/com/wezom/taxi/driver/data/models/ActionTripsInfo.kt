package com.wezom.taxi.driver.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by zorin.a on 3/11/19.
 */
data class ActionTripsInfo(
        @SerializedName("completed_trips_done")
        val completedTripsDone: Int?,
        @SerializedName("conditions")
        var conditions: List<Condition>?
)