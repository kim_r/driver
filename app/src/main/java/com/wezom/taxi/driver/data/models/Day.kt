package com.wezom.taxi.driver.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by zorin.a on 22.03.2018.
 */
data class Day(
        @SerializedName("id") val id: Int?,
        @SerializedName("dayOfWeek") val dayOfWeek: Int?,
        @SerializedName("date") val date: Long?,
        @SerializedName("completedTrips") val completedTrips: Int?,
        @SerializedName("onlineTime") val onlineTime: Long?,
        @SerializedName("distance") val distance: Double?,
        @SerializedName("totalIncome") val totalIncome: Int?,
        @SerializedName("cashIncome") val cashIncome: Int?,
        @SerializedName("nonCashIncome") val nonCashIncome: Int?,
        @SerializedName("surcharge") val surcharge: Double?
)