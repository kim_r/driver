package com.wezom.taxi.driver.data.models

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by andre on 23.03.2018.
 */
class DatesOfWeekModel(
        val firstDay: Long,
        val lastDay: Long,
        val formatetDates: String
): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readLong(),
            parcel.readLong(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(firstDay)
        parcel.writeLong(lastDay)
        parcel.writeString(formatetDates)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<DatesOfWeekModel> {
        override fun createFromParcel(parcel: Parcel): DatesOfWeekModel = DatesOfWeekModel(parcel)


        override fun newArray(size: Int): Array<DatesOfWeekModel?> = arrayOfNulls(size)
    }
}