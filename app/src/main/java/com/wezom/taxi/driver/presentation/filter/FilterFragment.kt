package com.wezom.taxi.driver.presentation.filter

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.databinding.FragmentFilterBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.net.response.FilterResponse
import com.wezom.taxi.driver.presentation.base.BaseFragment
import com.wezom.taxi.driver.presentation.filter.base.EditFilter
import com.wezom.taxi.driver.presentation.filter.base.FilterSwitch
import com.wezom.taxi.driver.presentation.filter.list.FilterAdapter
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber
import javax.inject.Inject

/**
 *Created by Zorin.A on 25.June.2019.
 */
class FilterFragment : BaseFragment() {
    lateinit var viewModel: FilterViewModel
    lateinit var binding: FragmentFilterBinding

    @Inject
    lateinit var adapter: FilterAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this,
                viewModelFactory)
                .getViewModelOfType()
        setBackPressFun {
            viewModel.onBackPressed()
        }

        viewModel.loadingLiveData.observe(this,
                progressObserver)

        viewModel.filterLiveData.observe(this,
                Observer {
                    Timber.d("FILTER_ADAPTER filterLiveData size: ${it?.size}")
                    val myList: MutableList<FilterResponse> = it!!.toMutableList()
                    myList.reverse()
                    if (myList.size > 0)
                        myList.add(myList[0])
                    myList.reverse()
                    adapter.addAll(myList)
                    if (it.size > 0)
                        binding.emptyText.visibility = View.GONE
                    else {
                        binding.emptyText.visibility = View.VISIBLE
                    }
                })
        viewModel.getFilter()
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentFilterBinding.inflate(inflater,
                container,
                false)
        binding.setLifecycleOwner(this)
        return binding.root
    }


    @SuppressLint("CheckResult")
    private fun listeners() {
        disposable += RxBus.listen(FilterSwitch::class.java).subscribe {
            if (it.filter.status!!) {
                viewModel.activateFilter(it.filter.id!!, it.switch)
            } else {
                viewModel.deactivateFilter(it.filter.id!!, it.switch)
            }
        }

        disposable += RxBus.listen(EditFilter::class.java).subscribe {
            viewModel.editFilter(it.filter)
        }
    }


    override fun onViewCreated(view: View,
                               savedInstanceState: Bundle?) {
        super.onViewCreated(view,
                savedInstanceState)
        initViews()
        listeners()
    }

    private fun initViews() {
        binding.run {
            toolbar.setToolbarTitle(getString(R.string.filter))
            actionButton.setOnClickListener {
                viewModel.addFilter()
            }

            filterList.setHasFixedSize(true)
//            decorator.space = 8
//            etherList.addItemDecoration(decorator)
//            adapter.callback = { position: Int, order: NewOrderInfo ->
//                viewModel.enterAcceptOrderScreen(order)
//            }
            filterList.adapter = adapter
        }
    }

    companion object {
        fun newInstance(): FilterFragment {
            return FilterFragment()
        }
    }
}