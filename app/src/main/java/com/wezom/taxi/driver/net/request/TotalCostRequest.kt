package com.wezom.taxi.driver.net.request

import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.ChangedParams
import com.wezom.taxi.driver.data.models.Coordinate
import java.io.Serializable


/**
 * Created by zorin.a on 01.06.2018.
 */

data class TotalCostRequest(
        @SerializedName("factCost") val factCost: Int?,
        @SerializedName("actualCost") val actualCost: Int?,
        @SerializedName("fact_time") val factTime: Long?,
        @SerializedName("fact_path") val factPath: Double?,
        @SerializedName("waitingTime") val waitingTime: Long?,
        @SerializedName("waitingCost") val waitingCost: Double?,
        @SerializedName("longitude") val longitude: Double?,
        @SerializedName("latitude") val latitude: Double?,
        @SerializedName("route") val route: List<Coordinate?>?,
        @SerializedName("eventTime") val eventTime: Long?,
        @SerializedName("changedParams") val changedParams: ChangedParams?,
        @SerializedName("isEstimatedCostTaken") val isEstimatedCostTaken: Boolean? = null,
        @SerializedName("resultF3") val resultF3: Int? = null
) : Serializable