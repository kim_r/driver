package com.wezom.taxi.driver.presentation.gains

import android.arch.lifecycle.MutableLiveData
import com.wezom.taxi.driver.data.models.DayRange
import com.wezom.taxi.driver.data.models.ResponseState
import com.wezom.taxi.driver.data.models.ResponseState.State.ERROR
import com.wezom.taxi.driver.data.models.ResponseState.State.IDLE
import com.wezom.taxi.driver.data.models.ResponseState.State.LOADING
import com.wezom.taxi.driver.data.models.ResponseState.State.NETWORK_ERROR
import com.wezom.taxi.driver.net.response.DriverIncomesResponse
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.repository.DriverRepository
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber
import java.text.DecimalFormat
import java.util.*
import javax.inject.Inject

/**
 * Created by zorin.a on 21.03.2018.
 */
class GainsViewModel @Inject constructor(
    screenRouterManager: ScreenRouterManager,
    private val repository: DriverRepository
) : BaseViewModel(screenRouterManager) {

    var incomesLiveData = MutableLiveData<DriverIncomesResponse>()
    var dateRangeLiveData = MutableLiveData<DayRange>()

    private fun getIncomes(startDate: Long, endDate: Long) {
        loadingLiveData.postValue(ResponseState(LOADING))
        App.instance.getLogger()!!.log("getIncomes start ")
        disposable += repository.getIncomes(startDate, endDate).subscribe(
            { response ->
                handleResponseState(response)
                App.instance.getLogger()!!.log("getIncomes suc ")
                if (response?.isSuccess!!) {
                    incomesLiveData.postValue(response)
                    loadingLiveData.postValue(ResponseState(IDLE))
                } else {
                    loadingLiveData.postValue(ResponseState(ERROR))
                }
            },
            { throwable ->
                App.instance.getLogger()!!.log("getIncomes error ")
                loadingLiveData.postValue(ResponseState(NETWORK_ERROR))
                Timber.e(throwable)
            }
        )
    }

    fun getIncomesOnNewDate(year: Int, month: Int, day: Int) {
        val calendar = GregorianCalendar(SimpleTimeZone.getDefault())
        calendar.set(year, month, day)
        val dayOfWeekOffset = calendar.get(Calendar.DAY_OF_WEEK) - 2  //because start index is 0, not 1
        Timber.d("dayOfWeekOffset: $dayOfWeekOffset")
        val firstDayOfWeek = day - dayOfWeekOffset
        calendar.set(year, month, firstDayOfWeek)
        val startDay = calendar.get(Calendar.DAY_OF_MONTH)
        val startMonth = calendar.get(Calendar.MONTH) + 1 //cause of 0 is first
        Timber.d("startDayOfWeek: $startDay")
        val startPeriodTime = calendar.timeInMillis
        calendar.add(Calendar.DAY_OF_YEAR, 6)
        val endDay = calendar.get(Calendar.DAY_OF_MONTH)
        val endMonth = calendar.get(Calendar.MONTH) + 1 //cause of 0 is first
        val endPeriodTime = calendar.timeInMillis

        Timber.d("startPeriodTime: $startPeriodTime")
        Timber.d("endPeriodTime: $endPeriodTime")
        getIncomes(startPeriodTime, endPeriodTime)
        val df = DecimalFormat("00")
        dateRangeLiveData.postValue(DayRange(df.format(startDay), df.format(startMonth), df.format(endDay), df.format(endMonth)))
    }
}