package com.wezom.taxi.driver.presentation.history.list

import android.view.ViewGroup
import com.wezom.taxi.driver.data.models.Trip
import com.wezom.taxi.driver.presentation.base.lists.AdapterClickListener
import com.wezom.taxi.driver.presentation.base.lists.BaseAdapter
import com.wezom.taxi.driver.presentation.base.lists.BaseViewHolder
import javax.inject.Inject

/**
 * Created by Andrew on 14.03.2018.
 */
class OrderHistoryAdapter @Inject constructor() :
        BaseAdapter<Trip, OrderHistoryItemView, BaseViewHolder<OrderHistoryItemView>>() {
    private lateinit var listener: AdapterClickListener<Trip>

    fun setClickListener(listener: AdapterClickListener<Trip>) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<OrderHistoryItemView> {
        val itemView = OrderHistoryItemView(parent.context)
        itemView.setClickListener(listener)
        return BaseViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<OrderHistoryItemView>, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.setIsRecyclable(false)
    }
}