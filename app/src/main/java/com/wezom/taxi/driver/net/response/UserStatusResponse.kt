package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.DriverStatus
import com.wezom.taxi.driver.data.models.ModerationStatus
import com.wezom.taxi.driver.data.models.User


/**
 * Created by zorin.a on 021 21.02.18.
 */

data class UserStatusResponse(
        @SerializedName("isOnline") val isOnline: Boolean? = null,
        @SerializedName("balance") val balance: Double? = null,
        @SerializedName("blockedMessage") val blockedMessage: String? = null,
        @SerializedName("blockedEndDate") val blockedEndDate: String? = null,
        @SerializedName("isMoneyEnough") val isMoneyEnough: Boolean? = null,
        @SerializedName("isCurrentOrderExist") val isCurrentOrderExist: Boolean? = null,
        @SerializedName("moderationStatus") val moderationStatus: ModerationStatus? = null,
        @SerializedName("driverStatus") val driverStatus: DriverStatus? = null,
        @SerializedName("accessToWorkWithoutModeration") val accessToWorkWithoutModeration: Boolean? = null,
        @SerializedName("coordinatesUpdateTime") val coordinatesUpdateTime: Long? = null,
        @SerializedName("user") val user: User? = null) : BaseResponse()
