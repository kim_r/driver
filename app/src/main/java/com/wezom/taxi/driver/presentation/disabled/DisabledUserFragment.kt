package com.wezom.taxi.driver.presentation.disabled

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wezom.taxi.driver.databinding.DisabledUserBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.ext.hideKeyboard
import com.wezom.taxi.driver.presentation.base.BaseFragment

/**
 * Created by zorin.a on 26.04.2018.
 */
class DisabledUserFragment : BaseFragment() {

    lateinit var viewModel: DisabledUserViewModel
    lateinit var binding: DisabledUserBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity.hideKeyboard()
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DisabledUserBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            nextAccount.setOnClickListener { viewModel.switchAccount() }
        }
    }
}