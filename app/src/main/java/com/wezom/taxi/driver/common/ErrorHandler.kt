package com.wezom.taxi.driver.common

import android.content.Context
import android.content.SharedPreferences
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.bus.events.DrawerLockEvent
import com.wezom.taxi.driver.bus.events.ShowDialogEvent
import com.wezom.taxi.driver.ext.*
import com.wezom.taxi.driver.net.jobqueue.JobFactory
import com.wezom.taxi.driver.net.jobqueue.jobs.LogoutJob
import com.wezom.taxi.driver.net.response.UserStatusResponse
import com.wezom.taxi.driver.presentation.addcard.AddCardActivity
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.route.Routable
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.presentation.main.events.DisableOnlineStatusEvent
import com.wezom.taxi.driver.presentation.main.events.SocketServiceResponseEvent
import com.wezom.taxi.driver.presentation.profile.ProfileFragment
import com.wezom.taxi.driver.repository.DriverRepository
import com.wezom.taxi.driver.service.LocationService
import com.wezom.taxi.driver.service.SocketService
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.lang.ref.WeakReference

/**
 * Created by zorin.a on 12.06.2018.
 */

class ErrorHandler constructor(screenRouterManager: ScreenRouterManager,
                               private val repository: DriverRepository,
                               sharedPreferences: SharedPreferences,
                               private val context: Context) : Routable by screenRouterManager, ErrorHandle {

    //region prefs
    private var token: String by sharedPreferences.string(TOKEN)

    private var userImageString: String by sharedPreferences.string(USER_IMAGE_URI)
    private var carImageString: String by sharedPreferences.string(CAR_IMAGE_URI)
    private var userName: String by sharedPreferences.string(USER_NAME_CACHED)
    private var userRating: String by sharedPreferences.string(USER_RATING_CACHED)
    private var userPhone by sharedPreferences.string(USER_PHONE)
    private var userBalanceId by sharedPreferences.int(USER_BALANCE_ID)

    private var doc1 by sharedPreferences.string(FIRST_DOC_URI)
    private var doc2 by sharedPreferences.string(SECOND_DOC_URI)
    private var doc3 by sharedPreferences.string(THIRD_DOC_URI)
    private var doc4 by sharedPreferences.string(FOURTH_DOC_URI)

    private var isCardAdded: Boolean by sharedPreferences.boolean(IS_CARD_ADDED)
    //endregion

    override fun onError(code: Int, message: String?) {
        when (code) {
            NO_ACCESS_TO_WORK -> {
                enterAsBlockedUser(UserStatusResponse(blockedMessage = message))
            }
            NOT_ENOUGH_MONEY -> {
                if (message != null) {
                    showErrorDialog(message)
                }
                RxBus.publish(SocketServiceResponseEvent(SocketServiceResponseEvent.Result.NEGATIVE))
                SocketService.stop(context = WeakReference(context))
            }
            NEED_ACTIVE_CARD -> {
                RxBus.publish(SocketServiceResponseEvent(SocketServiceResponseEvent.Result.NEGATIVE))
                SocketService.stop(context = WeakReference(context))
                showErrorDialog(message!!) { switchToAddCardScreen() }
            }
            DRIVER_ALREADY_IN_THIS_STATUS -> {
                //do nothing
            }
            SERVER_UNAVAILABLE -> {
                if (message != null) {
                    showErrorDialog(message)
                }
            }
            USER_INACTIVE -> {
                enterAsNotActiveUser()
            }
            CANT_CREATE_USER_IN_DB -> {
                if (message != null) {
                    showErrorDialog(message)
                }
            }
            CANT_CREATE_SESSION_IN_DB -> {
                if (message != null) {
                    showErrorDialog(message)
                }
            }
            CANT_REMOVE_SESSION -> {
                if (message != null) {
                    showErrorDialog(message)
                }
            }
            NO_ACCESS_FOR_THIS_ACTION -> {
                if (message != null) {
                    showErrorDialog(message)
                }
            }
            NO_VERIFICATION_DATA_FOUND -> {
                if (message != null) {
                    showErrorDialog(message)
                }
            }

            DRIVER_DID_NOT_PASS_MODERATION -> {
                if (message != null) {
                    showErrorDialog(message)
                }
                RxBus.publish(DisableOnlineStatusEvent(false))
                enterAsModerationUser()
            }

            DRIVER_NOT_VERIFIED -> {
                if (message != null) {
                    showErrorDialog(message)
                }
                RxBus.publish(DisableOnlineStatusEvent(false))
            }

            DRIVER_PROFILE_IS_NOT_FILLED_TO_END -> {
                if (message != null) {
                    showErrorDialog(message)
                }
                enterAsModerationUser()
            }

            DRIVER_IS_NEW -> {
                if (message != null) {
                    showErrorDialog(message)
                }
                newDriver()
            }

            BAD_REQUEST -> {
                if (message != null) {
                    showErrorDialog(message)
                }
            }
            UNAUTHENTICATED -> {
                logOut()
            }
            FORBIDDEN -> {
                if (message != null) {
                    showErrorDialog(message)
                }
            }
            NOT_FOUND -> {
                if (message != null) {
                    showErrorDialog(message)
                }
            }
            UNPROCESSABLE_ENTRY -> {
                if (message != null) {
                    showErrorDialog(message)
                }
            }
            DRIVER_HAVE_LOW_RATING -> {
                if (message != null) {
                    showErrorDialog(message)
                }
            }
        }
    }

    private fun logOut() {
        val tokenCopy = "Bearer $token"
        Timber.d("LOGOUT logOut $tokenCopy")
        JobFactory.instance.getJobManager()?.addJobInBackground(LogoutJob(tokenCopy))
        userImageString = ""
        carImageString = ""
        userName = ""
        userRating = ""
        userPhone = ""
        isCardAdded = false
        userBalanceId = 0
        doc1 = ""
        doc2 = ""
        doc3 = ""
        doc4 = ""
        token = ""
        setRootScreen(VERIFICATION_PHONE_SCREEN)
        LocationService.stop(context = WeakReference(context))
        SocketService.stop(context = WeakReference(context))
    }

    private fun enterAsNotActiveUser() {
        RxBus.publish(DrawerLockEvent(true))
        setRootScreen(ACCOUNT_DISABLED_SCREEN)
    }

    private fun enterAsBlockedUser(response: UserStatusResponse) {
        RxBus.publish(DrawerLockEvent(true))
        if (response.blockedMessage != null) {
            setRootScreen(BLOCK_SCREEN, response.blockedMessage)
        } else {
            setRootScreen(BLOCK_SCREEN)
        }
    }

    private fun enterAsModerationUser() {
        setRootScreen(PROFILE_SCREEN, ProfileFragment.MODE_MODERATION)
    }

    private fun newDriver() {
        setRootScreen(SIMPLE_REGISTRATION_SCREEN)
    }

    private fun enterAsOfflineUser() {
        RxBus.publish(DrawerLockEvent(false))
        setRootScreen(MAIN_MAP_SCREEN)
    }

    private fun showErrorDialog(errorMessage: String, action: (() -> Unit)? = null) {
        RxBus.publish(ShowDialogEvent(context.getString(R.string.error), errorMessage, action))
    }

    private fun switchToAddCardScreen() {
        App.instance.getLogger()!!.log("getCardVerificationInfo start ")
        repository.getCardVerificationInfo()
                .subscribeBy(
                        onSuccess = {
                            App.instance.getLogger()!!.log("getCardVerificationInfo suc ")
                            if (it.isSuccess == true) {
                                AddCardActivity.startAddCardActivity(context, it.paymentInfo)
                            }
                        }, onError = {  App.instance.getLogger()!!.log("getCardVerificationInfo error ")}
                )
    }

    companion object {
        const val NO_ACCESS_TO_WORK = 1
        const val NOT_ENOUGH_MONEY = 2
        const val NEED_ACTIVE_CARD = 3
        const val DRIVER_ALREADY_IN_THIS_STATUS = 4
        const val SERVER_UNAVAILABLE = 5
        const val USER_INACTIVE = 6
        const val CANT_CREATE_USER_IN_DB = 7
        const val CANT_CREATE_SESSION_IN_DB = 8
        const val CANT_REMOVE_SESSION = 9
        const val NO_ACCESS_FOR_THIS_ACTION = 10
        const val NO_VERIFICATION_DATA_FOUND = 16
        const val DRIVER_NOT_VERIFIED = 30
        const val DRIVER_PROFILE_IS_NOT_FILLED_TO_END = 31
        const val DRIVER_IS_NEW = 32
        const val DRIVER_DID_NOT_PASS_MODERATION = 35
        const val DRIVER_HAVE_LOW_RATING = 36
        const val BAD_REQUEST = 400
        const val UNAUTHENTICATED = 401
        const val FORBIDDEN = 403
        const val NOT_FOUND = 404
        const val UNPROCESSABLE_ENTRY = 422
    }
}

interface ErrorHandle {
    fun onError(code: Int, message: String? = null)
}

