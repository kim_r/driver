package com.wezom.taxi.driver.presentation.filter

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.common.LocationStorage
import com.wezom.taxi.driver.databinding.FragmentCreateFilterBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.ext.hideKeyboard
import com.wezom.taxi.driver.ext.setVisible
import com.wezom.taxi.driver.net.request.CreateFilterRequest
import com.wezom.taxi.driver.net.response.AutoCompleteResult
import com.wezom.taxi.driver.presentation.base.BaseFragment
import timber.log.Timber
import javax.inject.Inject

/**
 *Created by Zorin.A on 26.June.2019.
 */
class CreateFilterFragment : BaseFragment() {
    lateinit var viewModel: CreateFilterViewModel
    lateinit var binding: FragmentCreateFilterBinding
    lateinit var point: AutoCompleteResult

    @Inject
    lateinit var locationStorage: LocationStorage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this,
                viewModelFactory)
                .getViewModelOfType()
        setBackPressFun {
            hideKeyboard()
            viewModel.onBackPressed()
        }
        viewModel.resultLiveData.observe(this,
                Observer {
                    binding.run {
                        it?.let {
                            addressValue.text = it
                            enableDestinationPointUi(it.isNotBlank())
                            isActiveDestination.isChecked = true
                        }
                    }
                })

        viewModel.resultPointLiveData.observe(this,
                Observer {
                    binding.run {
                        it?.let {
                            point = it
                        }
                    }
                })
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentCreateFilterBinding.inflate(inflater,
                container,
                false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onViewCreated(view: View,
                               savedInstanceState: Bundle?) {
        super.onViewCreated(view,
                savedInstanceState)
        initViews()

    }

    private fun initViews() {
        binding.run {
            toolbar.setToolbarTitle(getString(R.string.create_filter))
            radiusValue.text = getString(R.string.about_few_km,
                    (spinnerRadius.progress / 10.0).toString())
            destinationValue.text = getString(R.string.about_few_km,
                    (spinnerDestination.progress / 10.0).toString())

            spinnerRadius.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar?,
                                               value: Int,
                                               p2: Boolean) {
                    radiusValue.text = getString(R.string.about_few_km,
                            if (value <= 5) "0.5" else (value / 10.0).toString())
                }

                override fun onStartTrackingTouch(p0: SeekBar?) {
                }

                override fun onStopTrackingTouch(p0: SeekBar?) {
                }
            })

            spinnerDestination.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(p0: SeekBar?,
                                               value: Int,
                                               p2: Boolean) {
                    destinationValue.text = getString(R.string.about_few_km,
                            if (value <= 5) "0.5" else (value / 10.0).toString())
                }

                override fun onStartTrackingTouch(p0: SeekBar?) {
                }

                override fun onStopTrackingTouch(p0: SeekBar?) {
                }
            })

            spinnerRadius.isEnabled = isActiveRadius.isChecked
            isActiveRadius.setOnCheckedChangeListener { _, isChecked ->
                Timber.d("IS_CHECKED: $isChecked")
                spinnerRadius.isEnabled = isChecked
            }

            spinnerDestination.isEnabled = isActiveDestination.isChecked
            isActiveDestination.setOnCheckedChangeListener { _, isChecked ->
                Timber.d("IS_CHECKED: $isChecked")
                if (!isChecked) {
                    enableDestinationPointUi(false)
                }

            }
            isActiveDestination.setOnClickListener {
                if (isActiveDestination.isChecked) viewModel.openPickPlaceScreen()
                isActiveDestination.isChecked = false
            }

            save.setOnClickListener {
                val createFilterRequest = CreateFilterRequest()
                if (binding.title.text.isNotEmpty()) {
                    createFilterRequest.name = binding.title.text.toString()
                }
                if (binding.isActiveRadius.isChecked) {
                    createFilterRequest.fromLatitude = locationStorage.getCachedLocation().latitude
                    createFilterRequest.fromLongitude = locationStorage.getCachedLocation().longitude
                    createFilterRequest.fromRadius = spinnerRadius.progress / 10.0
                }

                if (binding.isActiveDestination.isChecked) {
                    createFilterRequest.toLatitude = point.lat
                    createFilterRequest.toLongitude = point.lon
                    createFilterRequest.toRadius = spinnerDestination.progress / 10.0
                    createFilterRequest.address = binding.addressValue.text.toString()

                }
                viewModel.createFilter(createFilterRequest)
                hideKeyboard()
            }
        }
    }


    override fun onStop() {
        super.onStop()
        hideKeyboard()
    }

    private fun hideKeyboard() {
        view?.let { activity?.hideKeyboard() }
    }

    private fun enableDestinationPointUi(enabled: Boolean) {
        binding.run {
            TransitionManager.beginDelayedTransition(destinationContainer)
            destinationValue setVisible enabled
            spinnerDestination setVisible enabled
            destinationMaxValue setVisible enabled
            addressLabel setVisible enabled
            addressValue setVisible enabled
            spinnerDestination.isEnabled = enabled
        }
    }

    companion object {
        fun newInstance(): CreateFilterFragment = CreateFilterFragment()
    }
}