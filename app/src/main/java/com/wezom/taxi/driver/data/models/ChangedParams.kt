package com.wezom.taxi.driver.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by zorin.a on 28.03.2018.
 */
enum class ChangedParams {
    @SerializedName("0")
    NO_CHANGES,
    @SerializedName("1")
    TIME,
    @SerializedName("2")
    DISTANCE,
    @SerializedName("3")
    DISTANCE_AND_TIME
}