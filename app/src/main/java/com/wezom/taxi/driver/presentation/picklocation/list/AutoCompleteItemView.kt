package com.wezom.taxi.driver.presentation.picklocation.list

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.wezom.taxi.driver.databinding.ItemAutocompleteBinding
import com.wezom.taxi.driver.net.response.AutoCompleteResult
import com.wezom.taxi.driver.presentation.base.lists.AdapterClickListener
import com.wezom.taxi.driver.presentation.base.lists.ItemModel

/**
 *Created by Zorin.A on 01.August.2019.
 */
class AutoCompleteItemView : FrameLayout,
                             ItemModel<AutoCompleteResult> {
    var listener: AdapterClickListener<AutoCompleteResult>? = null
    var position:Int = 0

    override fun setData(data: AutoCompleteResult) {
        binding.run {
            title.text = data.title
            subtitle.text = data.subtitle
            root.setOnClickListener {
                listener?.onItemClick(position,
                                      data)
            }
        }
    }

    private val binding = ItemAutocompleteBinding.inflate(LayoutInflater.from(context),
                                                          this,
                                                          true)

    constructor(context: Context) : this(context,
                                         null)

    constructor(context: Context,
                attrs: AttributeSet?) : this(context,
                                             attrs,
                                             0)

    constructor(context: Context,
                attrs: AttributeSet?,
                defStyleAttr: Int) : super(context,
                                           attrs,
                                           defStyleAttr)
}