package com.wezom.taxi.driver.injection.module

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.ExclusionStrategy
import com.google.gson.FieldAttributes
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.wezom.taxi.driver.BuildConfig
import com.wezom.taxi.driver.common.Constants.DISK_CACHE_SIZE
import com.wezom.taxi.driver.common.ErrorHandler
import com.wezom.taxi.driver.injection.annotation.SkipSerialization
import com.wezom.taxi.driver.injection.qualifiers.RestOkHttpClient
import com.wezom.taxi.driver.injection.qualifiers.SocketOkHttpClient
import com.wezom.taxi.driver.injection.scope.AppScope
import com.wezom.taxi.driver.net.api.ApiService
import com.wezom.taxi.driver.net.interceptor.AuthInterceptor
import com.wezom.taxi.driver.net.websocket.channel.SocketChannel
import com.wezom.taxi.driver.net.websocket.channel.WebSocketChannel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.repository.DriverRepository
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.io.File
import java.util.concurrent.TimeUnit

/**
 * Created by zorin.a on 16.02.2018.
 */

@Module
internal object NetworkModule {
    @RestOkHttpClient
    @AppScope
    @Provides
    @JvmStatic
    fun provideOkHttpClient(context: Context,
                            authInterceptor: AuthInterceptor,
                            @RestOkHttpClient httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        val client by lazy {
            OkHttpClient
                    .Builder()
                    .addInterceptor(authInterceptor)
                    .addInterceptor(httpLoggingInterceptor)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .retryOnConnectionFailure(true)
                    .cache(Cache(File(context.cacheDir, "taxi_http_cache"), DISK_CACHE_SIZE))
                    .build()
        }
        return client
    }

    @SocketOkHttpClient
    @AppScope
    @Provides
    @JvmStatic
    fun provideSocketHttpClient(@SocketOkHttpClient httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient =
            OkHttpClient.Builder()
                    .retryOnConnectionFailure(true)
                    .addInterceptor(httpLoggingInterceptor)
                    .build()

    @AppScope
    @Provides
    @JvmStatic
    fun provideSocketChannel(@SocketOkHttpClient okHttpClient: OkHttpClient, gson: Gson, sharedPreferences: SharedPreferences): SocketChannel =
            WebSocketChannel(okHttpClient, gson, sharedPreferences)

    @RestOkHttpClient
    @AppScope
    @Provides
    @JvmStatic
    fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        val interceptor by lazy {
            HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { log ->
                Timber.tag("OK_HTTP").d(log)
            })
        }
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }

    @SocketOkHttpClient
    @AppScope
    @Provides
    @JvmStatic
    fun provideLoggingInterceptorForSocket(): HttpLoggingInterceptor {
        val interceptor by lazy {
            HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { log ->
                Timber.tag("OK_SOCKET").d(log)
            })
        }
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        return interceptor
    }

    @AppScope
    @Provides
    @JvmStatic
    fun provideRetrofit(@RestOkHttpClient okHttpClient: OkHttpClient): Retrofit {
        val retrofit by lazy {
            Retrofit.Builder()
                    .baseUrl(BuildConfig.REST_API)
                    .client(okHttpClient)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(GsonBuilder().serializeNulls().create()))
                    .build()
        }
        return retrofit
    }

    @AppScope
    @Provides
    @JvmStatic
    fun provideGson(): Gson {

        val builder = GsonBuilder()
                .serializeNulls()
                .addSerializationExclusionStrategy(object : ExclusionStrategy {
            override fun shouldSkipClass(clazz: Class<*>?): Boolean {
                return false
            }

            override fun shouldSkipField(f: FieldAttributes?): Boolean {
                return f?.getAnnotation(SkipSerialization::class.java) != null
            }
        })
        return builder.create()
    }

    @AppScope
    @Provides
    @JvmStatic
    fun provideApiService(retrofit: Retrofit): ApiService {
        val service by lazy { retrofit.create(ApiService::class.java) }
        return service
    }

    @AppScope
    @Provides
    @JvmStatic
    fun provideAuthInterceptor(prefs: SharedPreferences): AuthInterceptor {
        val interceptor by lazy {
            AuthInterceptor(prefs)
        }
        return interceptor
    }

    @AppScope
    @Provides
    @JvmStatic
    fun provideWebSocket(@SocketOkHttpClient client: OkHttpClient, gson: Gson,
                         sharedPreferences: SharedPreferences): WebSocketChannel {

        return WebSocketChannel(client, gson, sharedPreferences)
    }

    @AppScope
    @Provides
    @JvmStatic
    fun provideErrorHandler(screenRouterManager: ScreenRouterManager, driverRepository: DriverRepository,
                            sharedPreferences: SharedPreferences, context: Context): ErrorHandler {

        return ErrorHandler(screenRouterManager, driverRepository, sharedPreferences, context)
    }
}


