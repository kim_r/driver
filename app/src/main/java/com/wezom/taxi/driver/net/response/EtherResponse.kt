package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.NewOrderInfo

/**
 *Created by Zorin.A on 19.June.2019.
 */
class EtherResponse constructor(
        @SerializedName("orders")
        val orders: List<NewOrderInfo>) : BaseResponse()