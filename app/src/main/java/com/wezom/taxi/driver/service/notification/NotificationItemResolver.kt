package com.wezom.taxi.driver.service.notification

import android.content.Intent
import com.google.gson.GsonBuilder
import com.wezom.taxi.driver.data.models.NewOrderInfo
import com.wezom.taxi.driver.data.models.Order
import com.wezom.taxi.driver.data.models.notifications.*
import com.wezom.taxi.driver.net.websocket.models.Formula
import javax.inject.Inject

class PushNotificationItemResolver @Inject constructor() {

    val gson = GsonBuilder().create()

    fun dataToModel(intent: Intent): BasicNotification? {
        var typeId = 0
        intent.extras["typeId"]?.let {
            typeId = it.toString().toInt()
        }

        if (typeId == 0) return null

        when (typeId) {
            NotificationType.NEW_ORDER_AVAILABLE.typeId -> {
                val order = gson.fromJson(getIntentData("order", intent), Order::class.java)
                val formula = gson.fromJson(getIntentData("formula", intent), Formula::class.java)
                return NewOrderAvailable(
                        typeId = getIntentData("typeId", intent),
                        title = getIntentData("title", intent),
                        body = getIntentData("body", intent),
                        order = NewOrderInfo(
                                id = order.id!!,
                                startTimeWaitPassager = 0,
                                filter = false,
                                order = order,
                                formula = formula
                        ))
            }

            NotificationType.DRIVER_BLOCKED.typeId -> {
                val mapFromIntent = intentToMap(intent)
                val jsonString = mapToJsonString(mapFromIntent)
                return gson.fromJson(jsonString, DriverBlocked::class.java)
            }

            NotificationType.UNLOCK.typeId -> {
                val mapFromIntent = intentToMap(intent)
                val jsonString = mapToJsonString(mapFromIntent)
                return gson.fromJson(jsonString, Unlock::class.java)
            }

            NotificationType.CUSTOM_PUSH.typeId -> {
                val mapFromIntent = intentToMap(intent)
                val jsonString = mapToJsonString(mapFromIntent)
                return gson.fromJson(jsonString, CustomPush::class.java)
            }

            NotificationType.SUCCESSFUL_MODERATION.typeId -> {
                val mapFromIntent = intentToMap(intent)
                val jsonString = mapToJsonString(mapFromIntent)
                return gson.fromJson(jsonString, SuccessfulModeration::class.java)
            }

            NotificationType.ADDED_NEW_DISCOUNT.typeId -> {
                val mapFromIntent = intentToMap(intent)
                val jsonString = mapToJsonString(mapFromIntent)
                return gson.fromJson(jsonString, AddedNewDiscount::class.java)
            }

            NotificationType.INVALID_REGISTRATION_DATA.typeId -> {
                val mapFromIntent = intentToMap(intent)
                val jsonString = mapToJsonString(mapFromIntent)
                return gson.fromJson(jsonString, InvalidRegistrationData::class.java)
            }

            NotificationType.CANCEL_ORDER.typeId -> {
                val mapFromIntent = intentToMap(intent)
                val jsonString = mapToJsonString(mapFromIntent)
                return gson.fromJson(jsonString, CancelOrder::class.java)
            }

            NotificationType.DOWNLOADING_MISSING_PHOTO.typeId -> {
                val mapFromIntent = intentToMap(intent)
                val jsonString = mapToJsonString(mapFromIntent)
                return gson.fromJson(jsonString, DownloadingMissingPhoto::class.java)
            }
            else -> throw Exception("Unknown push type")
        }

    }

    private fun mapToJsonString(map: Map<String, String>): String = gson.toJson(map)

    private fun intentToMap(intent: Intent): Map<String, String> {
        var map = mutableMapOf<String, String>()
        for (key in intent.extras.keySet()) {
            map.put(key, getIntentData(key, intent))
        }
        return map
    }

    private fun getIntentData(key: String, intent: Intent?): String = intent?.extras?.get(key).toString()
}