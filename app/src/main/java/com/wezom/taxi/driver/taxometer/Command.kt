package com.wezom.taxi.driver.taxometer

import android.os.Parcel
import android.os.Parcelable
import com.wezom.taxi.driver.data.models.NewOrderInfo

/**
 * Created by zorin.a on 08.05.2018.
 */
data class Command constructor(var action: Action, var data: NewOrderInfo? = null, var restoredCost: Int = 0) : Parcelable {
    constructor(source: Parcel) : this(
            Action.values()[source.readInt()],
            source.readParcelable<NewOrderInfo>(NewOrderInfo::class.java.classLoader)
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(action.ordinal)
        writeParcelable(data, 0)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Command> = object : Parcelable.Creator<Command> {
            override fun createFromParcel(source: Parcel): Command = Command(source)
            override fun newArray(size: Int): Array<Command?> = arrayOfNulls(size)
        }
    }
}
