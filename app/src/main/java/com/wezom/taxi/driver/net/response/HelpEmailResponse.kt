package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName

/**
 * Created by udovik.s on 20.04.2018.
 */
class HelpEmailResponse(@SerializedName("email") val email: String) : BaseResponse()