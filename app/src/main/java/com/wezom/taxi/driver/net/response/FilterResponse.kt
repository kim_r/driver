package com.wezom.taxi.driver.net.response

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class FilterResponse(
        @SerializedName("id") val id: Int?,
        @SerializedName("name") val name: String?,
        @SerializedName("status") var status: Boolean?,
        @SerializedName("address") val address: String?,
        @SerializedName("from_latitude") var fromLatitude: String?,
        @SerializedName("from_longitude") var fromLongitude: String?,
        @SerializedName("from_radius") val fromRadius: Double?,
        @SerializedName("to_latitude") val toLatitude: String?,
        @SerializedName("to_longitude") val toLongitude: String?,
        @SerializedName("to_radius") val toRadius: Double?
): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Double,
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Double) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(name)
        parcel.writeValue(status)
        parcel.writeString(address)
        parcel.writeString(fromLatitude)
        parcel.writeString(fromLongitude)
        parcel.writeValue(fromRadius)
        parcel.writeString(toLatitude)
        parcel.writeString(toLongitude)
        parcel.writeValue(toRadius)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FilterResponse> {
        override fun createFromParcel(parcel: Parcel): FilterResponse {
            return FilterResponse(parcel)
        }

        override fun newArray(size: Int): Array<FilterResponse?> {
            return arrayOfNulls(size)
        }
    }
}
