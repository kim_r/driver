package com.wezom.taxi.driver.presentation.tutorial

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

/**
 * Created by zorin.a on 023 23.02.18.
 */
class TutorialPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    private var fragments: MutableList<Fragment> = ArrayList()

    override fun getItem(position: Int): Fragment = fragments[position]

    override fun getCount(): Int = fragments.size

    fun addFragment(fragment: Fragment) = fragments.add(fragment)
}


