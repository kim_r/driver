package com.wezom.taxi.driver.net.jobqueue.jobs

import android.annotation.SuppressLint
import com.wezom.taxi.driver.data.models.RateRequest
import com.wezom.taxi.driver.presentation.app.App

/**
 * Created by zorin.a on 18.05.2018.
 */
class RateTripJob(private val id: Int, private val body: RateRequest) : BaseJob() {
    @SuppressLint("CheckResult")
    override fun onRun() {
        App.instance.getLogger()!!.log("rateTrip start and end ")
        apiManager.rateTrip(id, body).blockingGet()
    }
}