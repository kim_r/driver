package com.wezom.taxi.driver.data.models

import android.arch.persistence.room.Embedded
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by zorin.a on 28.04.2018.
 */
data class Coordinate(@Embedded(prefix = "result_") @SerializedName("_id") var id: String? = null,
                      @SerializedName("latitude") var latitude: Double?,
                      @SerializedName("longitude") var longitude: Double?,
                      @SerializedName("degree") var degree: Int?

) : Parcelable, Serializable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readValue(Int::class.java.classLoader) as? Int)

    constructor() : this(id = "", latitude = 0.0, longitude = 0.0, degree = 0)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeValue(latitude)
        parcel.writeValue(longitude)
        parcel.writeValue(degree)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Coordinate> {
        override fun createFromParcel(parcel: Parcel): Coordinate {
            return Coordinate(parcel)
        }

        override fun newArray(size: Int): Array<Coordinate?> {
            return arrayOfNulls(size)
        }
    }
}
