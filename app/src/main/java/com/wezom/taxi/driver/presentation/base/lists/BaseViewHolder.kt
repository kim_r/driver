package com.wezom.taxi.driver.presentation.base.lists

import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Created by zorin.a on 14.03.2018.
 */
class BaseViewHolder<out T1 : View> constructor(val view: T1) : RecyclerView.ViewHolder(view)