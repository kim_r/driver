package com.wezom.taxi.driver.data.models

import com.google.gson.annotations.SerializedName


/**
 * Created by zorin.a on 04.06.2018.
 */

data class RestoredData(
        @SerializedName("changedParams") val changedParams: ChangedParams?,
        @SerializedName("actualCost") val actualCost: Int? = 0,
        @SerializedName("isEstimatedCostTaken") val isEstimatedCostTaken: Boolean? = null,
        @SerializedName("estimatedBonus") val estimatedBonus: Int? = 0,
        @SerializedName("resultF3") val resultF3: Int? = 0,
        @SerializedName("waitTime") val waitTime: Long? = 0

)