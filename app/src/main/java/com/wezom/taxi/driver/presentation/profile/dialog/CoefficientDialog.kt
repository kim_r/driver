package com.wezom.taxi.driver.presentation.profile.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.databinding.CoeficientDialogBinding

/**
 * Created by zorin.a on 05.03.2018.
 */
class CoefficientDialog : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val binding = DataBindingUtil.inflate<CoeficientDialogBinding>(LayoutInflater.from(context!!), R.layout.dialog_coefficient, null, false)
        return AlertDialog.Builder(context, R.style.WhiteDialogTheme)
                .setPositiveButton(R.string.ok, { _, i -> this.dismiss() })
                .setView(binding.root)
                .create()
    }

    companion object {
        val TAG: String = CoefficientDialog.javaClass.simpleName
    }
}