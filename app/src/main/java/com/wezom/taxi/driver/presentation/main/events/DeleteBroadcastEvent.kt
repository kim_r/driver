package com.wezom.taxi.driver.presentation.main.events

/**
 *Created by Zorin.A on 19.June.2019.
 */
class DeleteBroadcastEvent constructor(val orderId: Int)