package com.wezom.taxi.driver.data.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "order_coordinates")
data class OrderCoordinates(@PrimaryKey
                            var id: Int,
                            var coordinates: List<Coordinate>?)