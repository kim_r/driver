package com.wezom.taxi.driver.net.jobqueue.jobs

import android.annotation.SuppressLint
import com.wezom.taxi.driver.net.request.NotPaidRequest
import com.wezom.taxi.driver.presentation.app.App

/**
 * Created by zorin.a on 04.06.2018.
 */
class CustomerDidNotPayJob constructor(private val id: Int, private val request: NotPaidRequest) : BaseJob() {
    @SuppressLint("CheckResult")
    override fun onRun() {
        App.instance.getLogger()!!.log("customerDidNotPay start and end ")
        apiManager.customerDidNotPay(id, request).blockingGet()
    }
}