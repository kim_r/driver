package com.wezom.taxi.driver.presentation.registration

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import android.content.SharedPreferences
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.data.models.Car
import com.wezom.taxi.driver.data.models.CarParameter
import com.wezom.taxi.driver.data.models.ResponseState
import com.wezom.taxi.driver.data.models.User
import com.wezom.taxi.driver.ext.USER_NAME_CACHED
import com.wezom.taxi.driver.ext.string
import com.wezom.taxi.driver.net.request.RegistrationRequest
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.presentation.profile.events.UpdateUserDataEvent
import com.wezom.taxi.driver.repository.DriverRepository
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by zorin.a on 07.12.2018.
 */

class SimpleRegistrationViewModel @Inject constructor(screenRouterManager: ScreenRouterManager,
                                                      sharedPreferences: SharedPreferences,
                                                      private val repository: DriverRepository) : BaseViewModel(screenRouterManager) {
    val carBrandsLiveData = MutableLiveData<List<CarParameter>>()
    val carModelsLiveData = MutableLiveData<List<CarParameter>>()
    val carColorsLiveData = MutableLiveData<List<CarParameter>>()
    val carTypeLiveData = MutableLiveData<List<CarParameter>>()
    val carYearsLiveData = MutableLiveData<List<Int>>()
    val keyboardLiveData = MutableLiveData<Boolean>()
    private var userName: String by sharedPreferences.string(USER_NAME_CACHED)


    @SuppressLint("CheckResult")
    fun getCarBrands() {
        App.instance.getLogger()!!.log("carBrands start ")
        repository.carBrands()
                .subscribe({ response ->
                    App.instance.getLogger()!!.log("carBrands suc ")
                    carBrandsLiveData.postValue(response.brands)
                },
                        {
                            App.instance.getLogger()!!.log("carBrands error ")
                            Timber.e(it)
                        })
    }

    @SuppressLint("CheckResult")
    fun getCarModels(id: Int) {
        App.instance.getLogger()!!.log("carModels start ")
        repository.carModels(id)
                .subscribe({ response ->
                    App.instance.getLogger()!!.log("carModels suc ")
                    carModelsLiveData.postValue(response.models)
                },
                        {
                            App.instance.getLogger()!!.log("carModels error ")
                            Timber.e(it) })
    }

    @SuppressLint("CheckResult")
    fun getCarColors() {
        App.instance.getLogger()!!.log("carColors start ")
        repository.carColors()
                .subscribe({ response ->
                    App.instance.getLogger()!!.log("carColors suc ")
                    carColorsLiveData.postValue(response.colors)
                },
                        {
                            App.instance.getLogger()!!.log("carColors error ")
                            Timber.e(it) })
    }

    @SuppressLint("CheckResult")
    fun getType() {
        App.instance.getLogger()!!.log("getType start")
        repository.getType()
                .subscribe({ response ->
                    App.instance.getLogger()!!.log("getType suc")
                    carTypeLiveData.postValue(response.result)
                },
                        {
                            App.instance.getLogger()!!.log("getType error")
                            Timber.e(it) })
    }

    @SuppressLint("CheckResult")
    fun getCarIssueYears(modelId: Int) {
        App.instance.getLogger()!!.log("carIssueYears start ")
        repository.carIssueYears(modelId)
                .subscribe({ response ->
                    App.instance.getLogger()!!.log("carIssueYears suc ")
                    carYearsLiveData.postValue(response.years)
                },
                        {
                            App.instance.getLogger()!!.log("carIssueYears error ")
                            Timber.e(it) })
    }

    @SuppressLint("CheckResult")
    fun register(user: User,
                 car: Car) {
        loadingLiveData.postValue(ResponseState(ResponseState.State.LOADING))
        val request = RegistrationRequest(user, car)
        App.instance.getLogger()!!.log("userRegistration start ")
        repository.userRegistration(request)
                .subscribe({
                    App.instance.getLogger()!!.log("userRegistration suc ")
                    handleResponseState(it)
                    keyboardLiveData.postValue(true)
                    if (it?.isSuccess!!) {
                        enterAsActiveUser()
                        userName = user.name ?: ""
                        RxBus.publish(UpdateUserDataEvent())

                    } else {
                        loadingLiveData.postValue(ResponseState(ResponseState.State.ERROR,
                                message = it.error?.message))
                    }
                },
                        {
                            App.instance.getLogger()!!.log("userRegistration error ")
                            loadingLiveData.postValue(ResponseState(ResponseState.State.NETWORK_ERROR))
                        })
    }
}