package com.wezom.taxi.driver.presentation.balance

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.OrientationHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.data.models.Income
import com.wezom.taxi.driver.databinding.DebitedBalancesBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.presentation.balance.events.ChangeDateEvent
import com.wezom.taxi.driver.presentation.balance.list.BalansesAdapter
import com.wezom.taxi.driver.presentation.base.BaseFragment
import javax.inject.Inject

/**
 * Created by andre on 21.03.2018.
 */
class DebitedBalancesFragment : BaseFragment() {

    lateinit var binding: DebitedBalancesBinding
    lateinit var viewModel: DebitedBalancesViewModel
    @Inject
    internal lateinit var adapter: BalansesAdapter


    private val listObserver = Observer<List<Income>> { income ->
        if (income != null) adapter.setData(income)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
        viewModel.listLiveData.observe(this, listObserver)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DebitedBalancesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()

        RxBus.listen(ChangeDateEvent::class.java).subscribe({ userData ->
            viewModel.getDebitedBalances(userData.firstDay, userData.lastDay)
        })
    }

    private fun setupRecyclerView() {
        binding.run {
            recyclerView.adapter = adapter
            recyclerView.addItemDecoration(DividerItemDecoration(activity!!, DividerItemDecoration.VERTICAL))
            recyclerView.layoutManager = GridLayoutManager(activity, OrientationHelper.VERTICAL, 1, false)
        }

    }
}