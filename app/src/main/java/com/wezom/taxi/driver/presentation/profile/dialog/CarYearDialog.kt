package com.wezom.taxi.driver.presentation.profile.dialog

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.widget.ArrayAdapter
import com.wezom.taxi.driver.R

/**
 * Created by zorin.a on 07.03.2018.
 */
class CarYearDialog : DialogFragment() {
    private var data: List<Int>? = null
    private var pos: Int? = null
    var listener: ChoiceDialogSelection? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        data = arguments?.getIntegerArrayList(DATA_KEY)
        pos = arguments?.getInt(CHECKED_ITEM_KEY)

        var stringData: List<String?>? = null
        if (data != null) {
            stringData = data!!.map({ year -> year.toString() })
        }

        val adapter = ArrayAdapter<String>(
                context, R.layout.custom_list_item_single_choice, stringData)

        return AlertDialog.Builder(context!!)
                .setSingleChoiceItems(adapter, pos!!, { _, pos ->
                    listener?.onSelected(pos, data!![pos])
                    this.dismiss()
                }).create()
    }

    companion object {
        val TAG: String = CarYearDialog.javaClass.simpleName
        const val DATA_KEY: String = "data_key"
        const val CHECKED_ITEM_KEY: String = "checked_item_key"

        fun newInstance(data: ArrayList<Int>, checkedItem: Int? = 0): CarYearDialog {
            val args = Bundle()
            args.putIntegerArrayList(DATA_KEY, data)
            checkedItem?.let { args.putInt(CHECKED_ITEM_KEY, it) }
            var fragment = CarYearDialog()
            fragment.arguments = args
            return fragment
        }
    }

    interface ChoiceDialogSelection {
        fun onSelected(position: Int, carYear: Int)
    }
}