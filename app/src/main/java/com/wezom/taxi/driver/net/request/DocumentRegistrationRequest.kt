package com.wezom.taxi.driver.net.request

import com.google.gson.annotations.SerializedName

/**
 * Created by zorin.a on 021 21.02.18.
 */
data class DocumentRegistrationRequest(@SerializedName("id") val id: Int,
                                       @SerializedName("photo") val photo: String? = null)