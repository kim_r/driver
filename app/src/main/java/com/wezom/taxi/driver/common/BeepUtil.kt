package com.wezom.taxi.driver.common

import android.content.Context
import android.content.SharedPreferences
import android.media.AudioAttributes
import android.media.SoundPool
import android.os.Looper
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.ext.*
import com.wezom.taxi.driver.presentation.app.AppLifecycleTracker
import com.wezom.taxi.driver.presentation.customview.DriverToolbar
import org.jetbrains.anko.defaultSharedPreferences

object BeepUtil { // fixme

    private lateinit var prefs: SharedPreferences

    private val attr = AudioAttributes.Builder()
            .setUsage(AudioAttributes.USAGE_MEDIA)
            .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
            .build()

    private var soundPool: SoundPool? = null

    private var onlineNow = false
    private var onlineSoundId: Int? = null
    private var offlineSoundId: Int? = null


    private var newOrderSoundId: Int? = null
    private var massageSoundId: Int? = null

//    var songSignal: Boolean by prefs.boolean(SONG_SIGNAL, true)
//    var signalOnline: Boolean by prefs.boolean(SIGNAL_ONLINE, true)
//    var signalOnlineFilter: Boolean by prefs.boolean(SIGNAL_ONLINE_FILTER, true)
//    var signalBackground: Boolean by prefs.boolean(SIGNAL_BACKGROUND, true)
    // var background: Boolean by sharedPreferences.boolean(BACKGROUND, false)

    fun init(context: Context, isOnline: Boolean) {
        prefs = context.defaultSharedPreferences
        onlineNow = isOnline
        soundPool = SoundPool.Builder()
                .setMaxStreams(1)
                .setAudioAttributes(attr)
                .build()
        onlineSoundId = soundPool?.load(context, R.raw.online_sound, 1)
        offlineSoundId = soundPool?.load(context, R.raw.offline_sound, 1)
        massageSoundId = soundPool?.load(context, R.raw.custom_sound, 1)
        newOrderSoundId = soundPool?.load(context, R.raw.new_order_sound_efir, 1)
    }

    fun playSongForSetting() {
        newOrderSoundId?.let { soundPool?.play(it, 1F, 1F, 1, 0, 1F) }
    }

    fun playMassage() {
        if (prefs.getBoolean(BACKGROUND, false) && !prefs.getBoolean(SIGNAL_BACKGROUND, true)) {
            return
        }
        if (prefs.getBoolean(SONG_SIGNAL, true)) {
            if (!prefs.getBoolean(SOUND_ENABLED, true)) return
            massageSoundId?.let {
                soundPool?.play(it, 1F, 1F, 1, 24, 1F)
            }
        }
    }

    fun stop() {
        soundPool?.autoPause()
    }

    fun playDriverStatus(isOnline: Boolean) {
        if (prefs.getBoolean(BACKGROUND, false) && !prefs.getBoolean(SIGNAL_BACKGROUND, true)) {
            return
        }
        if (prefs.getBoolean(SONG_SIGNAL, true)) {
            if (onlineNow == isOnline || !prefs.getBoolean(SOUND_ENABLED, true)) return
            if (isOnline) {
                onlineSoundId?.let { soundPool?.play(it, 1F, 1F, 1, 0, 1F) }
            } else {
                offlineSoundId?.let { soundPool?.play(it, 1F, 1F, 1, 0, 1F) }
            }
            onlineNow = isOnline
        }
    }

    fun playNewOrder() {
        if (!DriverToolbar.IS_ONLINE_STATE)
            if (!prefs.getBoolean(SIGNAL_BACKGROUND_WAIT, false) && !AppLifecycleTracker.activityVisible) {
                return
            }
        if (prefs.getBoolean(TIMER, true)) {
            if (prefs.getBoolean(BACKGROUND, false) && !prefs.getBoolean(SIGNAL_BACKGROUND, true)) {
                return
            }
            if (prefs.getBoolean(SIGNAL_ONLINE, true)) {
                if (!prefs.getBoolean(SOUND_ENABLED, true)) return
                newOrderSoundId?.let { soundPool?.play(it, 1F, 1F, 1, 0, 1F) }
            }
            counter()
        }
    }

    fun playNewOrderFilter() {
        if (!DriverToolbar.IS_ONLINE_STATE)
            if (!prefs.getBoolean(SIGNAL_BACKGROUND_WAIT, false) && !AppLifecycleTracker.activityVisible) {
                return
            }
        if (prefs.getBoolean(TIMER, true)) {
            if (prefs.getBoolean(BACKGROUND, false) && !prefs.getBoolean(SIGNAL_BACKGROUND, true)) {
                return
            }
            if (prefs.getBoolean(SIGNAL_ONLINE_FILTER, true)) {
                if (!prefs.getBoolean(SOUND_ENABLED, true)) return
                newOrderSoundId?.let { soundPool?.play(it, 1F, 1F, 1, 0, 1F) }
            }
            counter()
        }
    }


    fun counter() {
        prefs.edit().putBoolean(TIMER, false).apply()
        android.os.Handler(Looper.getMainLooper()).postDelayed({
            prefs.edit().putBoolean(TIMER, true).apply()
        }, 5000)
    }

    fun clear() {
        soundPool?.release()
        soundPool = null
    }
}