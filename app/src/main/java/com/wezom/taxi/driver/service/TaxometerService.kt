package com.wezom.taxi.driver.service

import android.Manifest
import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.support.v4.content.ContextCompat
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.bus.events.*
import com.wezom.taxi.driver.common.isPreAndroidO
import com.wezom.taxi.driver.data.db.DbManager
import com.wezom.taxi.driver.data.models.*
import com.wezom.taxi.driver.ext.COORDINATES_UPDATE_TIME
import com.wezom.taxi.driver.ext.TIMER_NOT_ENTER_CLIENT
import com.wezom.taxi.driver.ext.shortToast
import com.wezom.taxi.driver.ext.showServiceNotification
import com.wezom.taxi.driver.net.api.ApiManager
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.service.AutoCloseOrderService.Companion.TAXI_TAXOMETER_CHANNEL
import com.wezom.taxi.driver.taxometer.Action
import com.wezom.taxi.driver.taxometer.Command
import com.wezom.taxi.driver.taxometer.Taxometer
import com.wezom.taxi.driver.taxometer.Taxometer.TaxometerMode
import com.wezom.taxi.driver.taxometer.Taxometer.TaxometerMode.DELAYED_START
import com.wezom.taxi.driver.taxometer.Taxometer.TaxometerMode.INSTANT_START
import dagger.android.DaggerService
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import timber.log.Timber.d
import java.lang.ref.WeakReference
import java.math.BigDecimal
import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.math.roundToLong

/**
 * Created by zorin.a on 08.05.2018.
 */
class TaxometerService : DaggerService(), GoogleApiClient.ConnectionCallbacks,
        LocationListener, GoogleApiClient.OnConnectionFailedListener {

    //region var
    @Inject
    lateinit var context: Context

    @Inject
    lateinit var sharedPreferences: SharedPreferences

    @Inject
    lateinit var dbManager: DbManager

    @Inject
    lateinit var apiManager: ApiManager

    private var timerEnterClient: Int = 0
    private var orderInfo: NewOrderInfo? = null
    private var isPaused = false
    private var currentSpeed: Double = 0.0
    private var activationTime: Long = 0
    private var passedDistanceFull: Double = 0.0
    private var passedDistanceInsideCity: Double = 0.0
    private var passedDistanceOutsideCity: Double = 0.0
    private var currentLocation: Location? = null
    private var taxometer: Taxometer? = null
    private var coordinateUpdateTime: Long = 0
    private var restoredCost: Int? = 0
    private var disposable = CompositeDisposable()

    private var newLocalDistance: Float = 0.0F
    private var lastLocalDistance: Float = 0.0F
    private var cityCoordinates: List<Coordinate>? = null

    private var startWaitTaxometerCounter: Boolean = false

    private var timeWayStatic: Double = 0.0

    private val googleApiClient: GoogleApiClient by lazy(mode = LazyThreadSafetyMode.SYNCHRONIZED) {
        GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build()
    }
    //endregion

    //region override
    override fun onCreate() {
        super.onCreate()

        showNotification()
        d("TAXOMETER onCreate")

        googleApiClient.connect()
        coordinateUpdateTime = sharedPreferences.getLong(COORDINATES_UPDATE_TIME, 10000)
        isAlive = true
        startUpdates()

    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        d("TAXOMETER onStartCommand")

        intent!!.extras?.getParcelable<Command>(KEY_ACTION)?.let { it ->
            val command = it
            command.data?.let {
                orderInfo = command.data
                cityCoordinates = orderInfo!!.formula?.coordinates!!
            }
            executeCommand(it)
        }

        if (disposable.size() <= 0) {
            startUpdates()
        }
        return Service.START_REDELIVER_INTENT
    }

    override fun onDestroy() {
        d("TAXOMETER onDestroy")
        if (googleApiClient.isConnected)
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this)
        googleApiClient.disconnect()
        disposable.dispose()
        disposable = CompositeDisposable()
        disposableTime.dispose()
        disposableTime = CompositeDisposable()
        realSecond = 0
        countMoveWait = 0
        countWait = 0
        counterTimer = 0
        realSecond = 0
        realCounterTimer = 0
        isAlive = false
        timerEnterClient = 0
        result = null
        super.onDestroy()
    }

    override fun onConnected(bundle: Bundle?) {
        d("onConnected")
        startWaitTaxometerCounter = false
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            val tmp = LocationServices.FusedLocationApi.getLastLocation(googleApiClient)
            if (tmp != null) {
                //this.currentLocation = tmp
                this.lastLocation = tmp
                this.lastLocationWait = tmp
            }
            val locationRequest = LocationRequest()
            locationRequest.interval = (TaxometerService.LOCATION_UPDATE_TIME_SECONDS * 1000).toLong()
            locationRequest.fastestInterval = (TaxometerService.LOCATION_UPDATE_TIME_SECONDS * 1000).toLong()
            locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this, Looper.getMainLooper())

//            if (orderInfo?.startTimeWaitPassager!! < taxometer?.timeToWaitFree!! * secondInMin) {
//                orderInfo?.startTimeWaitPassager = 0
//            }

            timeWayStatic = orderInfo?.formula?.sw?.fwt!!

            timeAccept = orderInfo?.order!!.calculatedTime!!
            timerEnterClient = sharedPreferences.getInt(TIMER_NOT_ENTER_CLIENT, 0)


            dbManager.gueryOrderResult(orderInfo!!.id)?.let {
                result = it.resultData
                counterTimer = result!!.counterTimer!!
                //realCounterTimer = result!!.realCounterTimer!!
                realSecond = result!!.realSecond!!
                countTrip = result!!.countTrip!!
                d("ORDER RESULT FROM DB $result")
                if (orderInfo?.startTimeWaitPassager == 0L)
                    orderInfo?.startTimeWaitPassager = result!!.startTimeWaitPassager!!
            }

            if (!isWait) {
                RxBus.publish(PlanTaxometr(0))
                if (orderInfo?.formula?.enableWait == 1)
                    startWait()
                else
                    moveWait = true

                disposable += Observable.interval(1, TimeUnit.SECONDS)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()).subscribe({
                            countTrip++
                            if (moveWait)
                                RxBus.publish(PlanTaxometrTrip(countTrip))
                        }, {
                            Timber.e(it)
                        })
            } else if (orderInfo!!.order!!.calculatedTime!! <= 0 && !isAccept) {
                startWait()
            } else {
                startDistanceStatus()
                disposable += Observable.interval(1, TimeUnit.SECONDS)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()).subscribe({
                            sharedPreferences.edit().putInt(TIMER_NOT_ENTER_CLIENT, timerEnterClient++).apply()
                            timeAccept--
                            RxBus.publish(PlanTaxometrAccept(timeAccept))
                            if (!isAccept && timeAccept <= 0) {
                                disposable.dispose()
                                disposable = CompositeDisposable()
                                startWait()
                            }
                        }, {
                            Timber.e(it)
                        })
            }

            disposable += RxBus.listen(CalcFinalPathEvent::class.java).subscribe {
                lastLocation = it.lastLocation
                currentLocation = it.firstLocation
                isCalcPath = false
                calcDistance(lastLocation)
            }
        }
    }

    private fun startDistanceStatus() {
        disposable += Observable.interval(1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe({
                    if (currentLocation != null)
                        calcDistanceStatus(currentLocation)
                }, {
                    Timber.e(it)
                })
    }

    private fun startWait() {
//        disposable += Observable.interval(1, TimeUnit.SECONDS)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread()).subscribe({
//                    if (currentLocation != null)
//                        calcStopCarWait(currentLocation!!)
//                }, {
//                    Timber.e(it)
//                })
        startWaitTaxometerCounter = true
    }

    private var timeAccept: Long = 0

    private var isAccept = false

    private var countTrip: Long = 0


    override fun onConnectionSuspended(p0: Int) {
        googleApiClient.reconnect()
    }

    override fun onConnectionFailed(result: ConnectionResult) {
        "Taxometer connection failed".shortToast(context)
    }


    override fun onLocationChanged(location: Location) {
        if (location.latitude == 0.0 && location.longitude == 0.0) return
        currentSpeed = calcSpeed(location)
//        Timber.d("speed = ${currentSpeed} km|h + ${location.accuracy}")
        if (this.currentLocation == null) {
            this.currentLocation = location
            this.lastLocationWait = location
        }
//            if (!isWait)
//            else {
//                this.lastLocationWait = location
//                this.currentLocation = location
//            }
        calcDistance(location)
        calcDistanceStatus(location)
        if (startWaitTaxometerCounter) {
            calcStopCarWait(location)
        }
    }

    private var lastLocation: Location? = null
    private var reapeat: Boolean = false

    private var disposableTime = CompositeDisposable()

    var realSecond: Long = 0


    override fun onBind(intent: Intent?): IBinder? = null
    //endregion

    //region fun
    private fun calcSpeed(location: Location): Double {
        return if (location.speed > 0) roundToKmHour((location.speed * 3.6), 3, BigDecimal.ROUND_HALF_UP) else 0.0
    }

    private var countTripStatus: Int = 0
    private fun calcDistanceStatus(newLocation: Location?) {
        if (lastLocationWait == null) {
            lastLocationWait = location
            return
        }
        currentSpeed = calcSpeed(newLocation!!)
        if (currentSpeed >= 2) {
            if (countTripStatus >= 2) {
                countTripStatus = 0
                val location = Location("FirstLocationClient")
                location.latitude = orderInfo!!.order!!.coordinates!!.first().latitude!!
                location.longitude = orderInfo!!.order!!.coordinates!!.first().longitude!!
                try {
                    if (location.distanceTo(newLocation) >= 250)
                        RxBus.publish(MoveDriverTaxometr())
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                }
            } else {
                countTripStatus++
            }
        } else {
            if (countTripStatus <= -2) {
                countTripStatus = 0
            } else {
                countTripStatus--
            }
        }
    }

    private var isCalcPath: Boolean = false

    @SuppressLint("CheckResult")
    private fun calcDistance(newLocation: Location?) { //all data per 3 seconds
        if (isCalcPath || currentLocation == null || newLocation == null || newLocation.accuracy >= 100) return

        newLocalDistance = currentLocation!!.distanceTo(newLocation) * 0.001F //meters to km
//        if (taxometer != null)
//            if (newLocalDistance >= 0.2) {
//                isCalcPath = true
//                val coordinatesToOCM: MutableList<PointsNumOCM> = ArrayList()
//                coordinatesToOCM.add(PointsNumOCM(currentLocation!!.longitude, currentLocation!!.latitude, 0))
//                coordinatesToOCM.add(PointsNumOCM(newLocation.longitude, newLocation.latitude, 1))
//                apiManager.getMapRouteOCM(CoordinatesOSM(coordinatesToOCM)).subscribe({ response ->
//                    Timber.d("getMapRouteOSM done ${response.routeOCM!!.distance}")
//                    if (response.isSuccess!!) {
//                        passedDistanceFull += response.routeOCM!!.distance!!
//                        passedDistanceInsideCity += response.routeOCM!!.distanceInsideCity!!
//                        passedDistanceOutsideCity += response.routeOCM!!.distanceOutsideCity!!
//                        if (taxometer != null) {
//                            for (point in response.routeOCM!!.pointsOCM!!) {
//                                taxometer!!.addLocalion(point.latitude!!, point.longitude!!)
//                            }
//                        }
//                        lastLocalDistance = newLocalDistance
//                        currentLocation = newLocation
//                        updateDate()
//                    } else {
//                        calcDictance(newLocation)
//                        Timber.d("getMapRouteOSM error in ")
//                    }
//                    isCalcPath = false
//                }, { t ->
//                    calcDictance(newLocation)
//                    Timber.d("getMapRouteOSM error ")
//                    App.instance.getLogger()!!.log("getMapRouteOSM error ")
//                    isCalcPath = false
//                })
//                return
//            }
        calcDistanceOffline(newLocation)

        d("lastLocalDistance: ${lastLocalDistance.div(LOCATION_UPDATE_TIME_SECONDS) * 1000} m")
        d("newLocalDistance: ${newLocalDistance.div(LOCATION_UPDATE_TIME_SECONDS) * 1000} m")
        d("newLocalDistance currentSpeed: $currentSpeed")
        d("passedDistanceFull: $passedDistanceFull")
        d("passedDistanceInsideCity: $passedDistanceInsideCity")
        d("passedDistanceOutsideCity: $passedDistanceOutsideCity")


    }

    private fun calcDistanceOffline(newLocation: Location) {
        if (newLocation.latitude == 0.0 && newLocation.longitude == 0.0) return
        val accuracy = newLocation.accuracy
        passedDistanceFull += newLocalDistance

        if (Taxometer.checkIsInsideCity(newLocation, cityCoordinates!!)) {
            passedDistanceInsideCity += newLocalDistance
        } else {
            passedDistanceOutsideCity += newLocalDistance
        }
        if (taxometer != null) {
//            if (taxometer!!.getRoute().size == 0){
//                taxometer!!.addLocalion(orderInfo!!.order!!.coordinates!!.first().latitude!!, orderInfo!!.order!!.coordinates!!.first().longitude!!)
//            }
            taxometer?.addLocalion(newLocation.latitude, newLocation.longitude)
        }


        Timber.d("increase passed distance by  ${newLocalDistance.div(LOCATION_UPDATE_TIME_SECONDS) * 1000} m")
//        } else {
//            Timber.d("skip newLocalDistance value: ${newLocalDistance.div(LOCATION_UPDATE_TIME_SECONDS) * 1000} m")
//        }

        lastLocalDistance = newLocalDistance
        currentLocation = newLocation



        if (accuracy > ACCURACY_THRESHOLD) {
            //show gps accuracy low dialog
            RxBus.publish(BadGpsEvent(false))
        } else {
            //dismiss gps accuracy low dialog
            RxBus.publish(BadGpsEvent(true))
        }

        if (taxometer != null)
            Timber.d("calcDistance offline ${taxometer!!.getRoute().size}   $newLocalDistance")

        updateDate()
    }

    private fun isConditionsRight(accuracy: Float, velocity: Float): Boolean {
        return newLocalDistance.div(LOCATION_UPDATE_TIME_SECONDS) * 1000 in MIN_LOCAL_DISTANCE..MAX_LOCAL_DISTANCE &&
                accuracy <= ACCURACY_THRESHOLD &&
                velocity < VELOCITY_THRESHOLD
    }

    private fun roundToKmHour(rawSpeed: Double, precision: Int, roundingMode: Int): Double {
        val bd = BigDecimal(rawSpeed)
        val rounded = bd.setScale(precision, roundingMode)
        return rounded.toDouble()
    }

    private fun showNotification() {
        val notification = this.showServiceNotification(
                TAXI_TAXOMETER_CHANNEL,
                getString(R.string.taxometer), getString(R.string.taxometer_in_progress)
        )
        startForeground(NOTIFICATION_ID, notification)
    }


    private var countWait = -1
    private var moveWait = false
    private var lastLocationWait: Location? = null
    private val metersWait = 10
    private var countMoveWait = 0

    var counterTimer: Long = 0
    var realCounterTimer: Long = 0
    var location: Location? = null

//    private var temp = 0

    private fun calcStopCarWait(location: Location) {
        //        if (lastLocationWait == null) {
//            lastLocationWait = location
//            return
//        }
        //Timber.d("NEW_TEX status $countWait $moveWait ${lastLocationWait?.distanceTo(location)!!}")
//        if (lastLocationWait?.distanceTo(location)!! < metersWait && countWait >= 0) {
//            countWait++
//            countMoveWait = 0
//            if (countWait >= 5) {
//                countWait = -1
//                moveWait = false
//            }
//        } else if (lastLocationWait?.distanceTo(location)!! > metersWait && countWait <= -1) {
//            countWait--
//            countMoveWait = 0
//            if (countWait <= -4) {
//                countWait = 0
//                moveWait = true
//                disposableTime.dispose()
//                disposableTime = CompositeDisposable()
//            }
//        }
//        if (lastLocationWait?.distanceTo(location)!! > metersWait && countWait >= 0) {
//            if (countMoveWait >= 5) {
//                countMoveWait = 0
//                countWait = 0
//            } else
//                countMoveWait++
//        } else if (lastLocationWait?.distanceTo(location)!! < metersWait && countWait <= -1) {
//            if (countMoveWait >= 5) {
//                countMoveWait = 0
//                countWait = -1
//            } else
//                countMoveWait++
//        }

//        temp++
//        if (temp in 10..20) {
//            currentSpeed = 20.0
//        } else if (temp >= 20) {
//            currentSpeed = 0.0
//        }

        // " ${currentSpeed} km|h + ${location.accuracy}".longToast(context)
        currentSpeed = calcSpeed(location)
        if (currentSpeed >= 2) {
            if (countMoveWait >= 2) {
                countMoveWait = 0
                moveWait = true
            } else {
                countMoveWait++
            }
        } else {
            if (countMoveWait <= -2) {
                moveWait = false
                countMoveWait = 0
            } else {
                countMoveWait--
            }
        }
        if (!moveWait) {
            if (disposableTime.size() == 0) {
                reapeat = true
                counterTimer = 0
                disposableTime += Observable.interval(0, 1, TimeUnit.SECONDS)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()).subscribe({
                            if (orderInfo != null) {
                                d("NEW_TEX status $counterTimer $realSecond $realCounterTimer $timeWayStatic")
                                sharedPreferences.edit().putInt(TIMER_NOT_ENTER_CLIENT, timerEnterClient++).apply()
                                if (!moveWait) {
                                    counterTimer++
                                    realCounterTimer++
                                    if (isWait) {
                                        RxBus.publish(PlanTaxometrSecond(counterTimer))
                                    } else {
                                        RxBus.publish(PlanTaxometr(counterTimer))
                                        if (realCounterTimer == (timeWayStatic * 60).toLong() && reapeat) {
                                            realSecond += taxometer?.timeToStopFree!! * 60
                                            reapeat = false
                                        } else if (realCounterTimer > (timeWayStatic * 60).toLong()) {
                                            realSecond++
                                        }
                                        startUpdatesWait()
                                    }
                                } else {
                                    disposableTime.dispose()
                                    disposableTime = CompositeDisposable()
                                    RxBus.publish(PlanTaxometrWait(realCounterTimer))
                                    realCounterTimer = 0
                                    RxBus.publish(PlanTaxometrSecond(0))
                                }
                            }

                        }, {
                            Timber.e(it)
                        })
            }
        } else {
            disposableTime.dispose()
            disposableTime = CompositeDisposable()
            RxBus.publish(PlanTaxometrWait(realCounterTimer))
            realCounterTimer = 0
            RxBus.publish(PlanTaxometrSecond(0))
        }
        lastLocationWait = location
    }

    var isWait: Boolean = false


    private fun executeCommand(command: Command?) {
        d("TAXOMETER executeCommand ${command?.action?.name}")
        isWait = false
        if (command?.action == null) {
            if (taxometer == null) {
                this.restoredCost = command?.restoredCost
                startTaxometer(INSTANT_START)
            }
        } else when (command.action) {
            Action.START -> {
                resetData()
                startTaxometer(INSTANT_START)
            }

            Action.START_WAIT -> {
                isWait = true
                isAccept = false
                resetData()
            }

            Action.START_DELAYED -> {
                resetData()
                startTaxometer(DELAYED_START)
            }

            Action.ACCEPT -> {
                isWait = true
                isAccept = true
                resetData()

            }

            Action.RESUME -> {
                resumeTaxometer()
            }

            Action.PAUSE -> {
                pauseTaxometer()
            }

            Action.STOP -> {
                stopTaxometer()
            }
            Action.RESTORE -> {
                dbManager.gueryOrderResult(orderInfo!!.id)?.let {
                    result = it.resultData
                    d("ORDER RESULT FROM DB $result")
                }

                if (taxometer == null) {
                    this.restoredCost = command.restoredCost
                    startTaxometer(INSTANT_START)
                }
            }
        }
    }

    private fun resumeTaxometer() {
        d("TAXOMETER resumeTaxometer")
        isPaused = false
    }

    private fun calcPriceForStop(secondToStop: Long, pricePerMinInStop: Double): Long {
        val minut: Long = (secondToStop) / 60
        return (pricePerMinInStop * minut).roundToLong()
    }

    private var time = System.currentTimeMillis() - 121000

    private fun startUpdatesWait() {
        d("TAXOMETER startUpdates")
        if (taxometer != null && !isPaused && currentLocation != null) {
            taxometer?.updateData(
                    calcPriceForStop(realSecond, orderInfo?.formula!!.sw!!.wmp!!.toDouble()) + calcPriceForStop(orderInfo?.startTimeWaitPassager!!, orderInfo?.formula!!.sw!!.cm!!.toDouble()),
                    realSecond + orderInfo?.startTimeWaitPassager!!,
                    isCalcFinalResultFun(),
                    isCalcFinalResultFactCostFun())

            if ((time + 120000) < System.currentTimeMillis()) {
                App.instance.getLogger()!!.log("Wait time id order ${orderInfo!!.order!!.id}  time = ${realSecond + orderInfo?.startTimeWaitPassager!!} second")
                time = System.currentTimeMillis()
            }
        }
    }

    private fun isCalcFinalResultFun(): Boolean {
        if (orderInfo!!.order!!.coordinates!!.size >= 2) {
            val loc2 = Location("")
            loc2.latitude = orderInfo!!.order!!.coordinates!![orderInfo!!.order!!.coordinates!!.size - 1].latitude!!
            loc2.longitude = orderInfo!!.order!!.coordinates!![orderInfo!!.order!!.coordinates!!.size - 1].longitude!!
            return currentLocation!!.distanceTo(loc2) <= 500
        }
        return orderInfo!!.order!!.coordinates!!.size >= 2
    }

    private fun isCalcFinalResultFactCostFun(): Boolean {
        if (orderInfo!!.order!!.coordinates!!.size == 2) {
            val loc2 = Location("")
            loc2.latitude = orderInfo!!.order!!.coordinates!![orderInfo!!.order!!.coordinates!!.size - 1].latitude!!
            loc2.longitude = orderInfo!!.order!!.coordinates!![orderInfo!!.order!!.coordinates!!.size - 1].longitude!!
            return currentLocation!!.distanceTo(loc2) < 500
        }
        return false
    }


    private fun startUpdates() {
        d("TAXOMETER startUpdates")
//        disposable += Observable.interval(0, LOCATION_UPDATE_TIME_SECONDS.toLong(), TimeUnit.SECONDS).subscribe({
//            updateDate()
//        }, {
//            Timber.e(it)
//        })
    }

    private fun updateDate() {
        if (taxometer != null && !isPaused && currentLocation != null) {

            var location = Location("startLocation")
            location.latitude = orderInfo?.order!!.coordinates!![0].latitude!!
            location.longitude = orderInfo?.order!!.coordinates!![0].longitude!!

            taxometer?.updateData(
                    isInsideCity = Taxometer.checkIsInsideCity(currentLocation!!, cityCoordinates),
                    isStartInsideCity = Taxometer.checkIsInsideCity(location, cityCoordinates),
                    currentSpeed = currentSpeed,
                    currentPriceWait = calcPriceForStop(realSecond, orderInfo?.formula!!.sw!!.wmp!!.toDouble()) + calcPriceForStop(orderInfo?.startTimeWaitPassager!!, orderInfo?.formula!!.sw!!.cm!!.toDouble()),
                    currentTimeWait = realSecond + orderInfo?.startTimeWaitPassager!!,
                    passedDistanceFull = passedDistanceFull,
                    passedDistanceInsideCity = passedDistanceInsideCity,
                    passedDistanceOutsideCity = passedDistanceOutsideCity,
                    location = currentLocation,
                    lastLocalDistance = lastLocalDistance,
                    isCalcFinalResult = isCalcFinalResultFun(),
                    isCalcFinalResultFactCost = isCalcFinalResultFactCostFun())
        }
    }

    private fun pauseTaxometer() {
        d("TAXOMETER pauseTaxometer")
        isPaused = true
    }

    private fun stopTaxometer() {
        d("TAXOMETER stopTaxometer")
        taxometer?.stop()
        taxometer = null
        stopService(Intent(context, TaxometerService::class.java))
    }

    private fun startTaxometer(mode: TaxometerMode) {
        d("TAXOMETER startTaxometer")

        val f = orderInfo!!.formula
        var onTheWay: OnTheWay? = dbManager.queryFinalization()?.let {
            it.onTheWay!!
        }?.let { it }

        if (onTheWay == null) {
            onTheWay = OnTheWay(orderInfo!!.order!!.coordinates!!.first().longitude!!, orderInfo!!.order!!.coordinates!!.first().latitude!!, System.currentTimeMillis())
        }

        taxometer = Taxometer(
                onTheWay,
                mode = mode,
                pricePerMinInStop = f?.sw!!.fwt!!,
                timeToStopFree = f.sw!!.fwt!!.toLong(),
                timeToWaitFree = f.sw!!.fw!!,
                activationTime = System.currentTimeMillis(),
                loadingPrice = f.lc!!,
                pricePerMinAwaiting = f.sw!!.cm!!,
                pricePerMinInTrip = f.sm!!.cf!!,
                pricePerKmInsideCity = f.cr!!.cT!!,
                pricePerKmOutsideCity = f.cr!!.cTb!!,
                priceForAdditionalServices = f.opt!!,
                allowablePercent = f.allowablePrice!!,
                coefficient = orderInfo!!.order?.currentCoefficient!!,
                bonuses = f.bonuses!!,
                preOrderPrice = f.pO!!,
                estimatedCost = orderInfo!!.order?.payment!!.estimatedCost!!,
                estimatedCostBonusless = orderInfo!!.order?.payment!!.estimatedCostBonusless!!,
                minPrice = f.minPrice!!,
                calculatedTime = orderInfo!!.order?.calculatedTime!!,
                calculatedPath = orderInfo!!.order?.calculatedPath!!,
                increasedPriceBy = f.increasedPriceBy!!,
                listener = object : Taxometer.TaxometerResultListener {
                    override fun onResult(resultCost: Double,
                                          factCost: Double,
                                          estimatedCost: Int,
                                          resultF3: Int,
                                          passedDistance: Double,
                                          route: CopyOnWriteArrayList<Coordinate>,
                                          factTimeMillis: Long,
                                          waitingTime: Long,
                                          waitingCost: Double,
                                          changedParams: ChangedParams,
                                          isEstimatedCostTaken: Boolean?,
                                          passedDistanceInsideCity: Double,
                                          passedDistanceOutsideCity: Double) {
                        val result = ResultDataEvent(
                                resultCost = Math.round(resultCost).toInt() + restoredCost!!,
                                factCost = Math.round(factCost).toInt(),
                                estimatedCost = estimatedCost,
                                resultF3 = resultF3,
                                bonus = Math.round(f.bonuses!!).toInt(),
                                estimatedBonus = f.estimatedBonuses?.toInt(),
                                surcharge = Math.round(orderInfo!!.order!!.payment?.surcharge!!).toInt(),
                                paymentMethod = orderInfo!!.order!!.payment!!.method!!,
                                changedParams = changedParams,
                                user = orderInfo!!.order!!.user!!,
                                address = orderInfo!!.order!!.coordinates!!,
                                factTime = factTimeMillis,
                                factPath = passedDistance, //meters to km
                                waitingTime = waitingTime,
                                waitingCost = waitingCost,
                                currentCoordinate = Coordinate(latitude = currentLocation?.latitude,
                                        longitude = currentLocation?.longitude,
                                        degree = currentLocation?.bearing!!.toInt()),
                                passedRoute = route,
                                orderInfo = orderInfo,
                                isEstimatedCostTaken = isEstimatedCostTaken,
                                counterTimer = counterTimer,
                                realCounterTimer = realCounterTimer,
                                realSecond = realSecond,
                                startTimeWaitPassager = orderInfo?.startTimeWaitPassager,
                                countTrip = countTrip,
                                passedDistanceInsideCity = passedDistanceInsideCity,
                                passedDistanceOutsideCity = passedDistanceOutsideCity

                        )

                        if (result.passedRoute!!.isNotEmpty() && result.orderInfo != null) {
                            dbManager.insertOrderResultData(OrderResultData(result.orderInfo!!.id, result))
                            dbManager.insertOrderCoordinates(OrderCoordinates(result.orderInfo!!.id, result.passedRoute))
                        }

                        d("TAXOMETER route size: ${route.size}")
                        TaxometerService.result = result
                        if (!route.isNullOrEmpty() && route.size > 1) {
                            d("1")
                            try {
//                                Looper.prepare()
                                d("trying to post DoneOrderEvent")
                                Handler().post {
                                    d("posting DoneOrderEvent with result: $result")
                                    RxBus.publish(DoneOrderEvent(result))
                                }
                            } catch (e: Exception) {
                                Timber.e(e)
                                e.printStackTrace()
                            }
                        } else {
                            d("Route is null")
                        }

                    }
                },
                coordinateUpdateTime = coordinateUpdateTime)


        dbManager.queryOrderCoordinatesById(orderInfo!!.id)?.let {
            taxometer?.let { it1 ->
                it1.initRoute(it.coordinates!!)
            }

            currentLocation = Location("currentLocation")
            currentLocation!!.latitude = it.coordinates!!.last().latitude!!
            currentLocation!!.longitude = it.coordinates!!.last().longitude!!
        }

        dbManager.gueryOrderResult(orderInfo!!.id)?.let {
            if (taxometer != null) {
                taxometer!!.restoreTaxometr(it.resultData.resultF3!!.toDouble(),
                        it.resultData.factCost!!.toDouble(),
                        it.resultData.resultCost!!.toDouble(),
                        it.resultData.factTime!!,
                        it.resultData.factPath!!,
                        it.resultData.changedParams!!,
                        it.resultData.isEstimatedCostTaken!!)
            }
            passedDistanceFull = it.resultData.factPath!!
            passedDistanceInsideCity = it.resultData.passedDistanceInsideCity!!
            passedDistanceOutsideCity = it.resultData.passedDistanceOutsideCity!!

        }
    }

    private fun resetData() {
        d("TAXOMETER resetData")
        isPaused = false
        currentLocation = null
        passedDistanceFull = 0.0
        passedDistanceInsideCity = 0.0
        passedDistanceOutsideCity = 0.0
        currentSpeed = 0.0
        activationTime = System.currentTimeMillis()
        disposable.dispose()
        disposable = CompositeDisposable()
        disposableTime.dispose()
        disposableTime = CompositeDisposable()
        countMoveWait = 0
        countWait = 0
        counterTimer = 0
        realSecond = 0
        realCounterTimer = 0
        isAlive = false
        timerEnterClient = 0
        result = null
        startWaitTaxometerCounter = false
        disposable.dispose()
        disposable = CompositeDisposable()
        disposableTime.dispose()
        disposableTime = CompositeDisposable()
    }
    //endregion

    companion object {
        private const val KEY_ACTION = "action"
        private const val KEY_LOCATION = "location"
        private const val KEY_FIRST_LOCATION = "FirstLocation"
        private const val KEY_TIME = "time"
        const val NOTIFICATION_ID = 101
        const val LOCATION_UPDATE_TIME_SECONDS: Int = 5
        const val ACCURACY_THRESHOLD: Int = 100
        const val VELOCITY_THRESHOLD: Int = 5
        const val MIN_LOCAL_DISTANCE: Float = 0.4F
        const val MAX_LOCAL_DISTANCE: Float = 10000000F

        var isAlive = false
        var result: ResultDataEvent? = null

        fun start(context: WeakReference<Context>, orderInfo: NewOrderInfo) {
            d("TAXOMETER start")
            val intent = Intent(context.get(), TaxometerService::class.java)
            intent.putExtra(KEY_ACTION, Command(Action.START, orderInfo))
            launch(context, intent)
        }

        fun startWait(context: WeakReference<Context>, orderInfo: NewOrderInfo) {
            d("TAXOMETER startWait")
            val intent = Intent(context.get(), TaxometerService::class.java)
            intent.putExtra(KEY_ACTION, Command(Action.START_WAIT, orderInfo))
            launch(context, intent)
        }

        fun startAcceptTime(context: WeakReference<Context>, orderInfo: NewOrderInfo) {
            d("TAXOMETER startWait")
            val intent = Intent(context.get(), TaxometerService::class.java)
            intent.putExtra(KEY_ACTION, Command(Action.ACCEPT, orderInfo))
            launch(context, intent)
        }

        fun startDelayed(context: WeakReference<Context>, orderInfo: NewOrderInfo) {
            d("TAXOMETER startDelayed")
            val intent = Intent(context.get(), TaxometerService::class.java)
            intent.putExtra(KEY_ACTION, Command(Action.START_DELAYED, orderInfo))
            launch(context, intent)
        }

//        fun resume(context: WeakReference<Context>) {
//            val intent = Intent(context.get(), TaxometerService::class.java)
//            intent.putExtra(KEY_ACTION, Command(Action.RESUME))
//            context.get()?.startService(intent)
//        }

        fun pause(context: WeakReference<Context>) {
            val intent = Intent(context.get(), TaxometerService::class.java)
            intent.putExtra(KEY_ACTION, Command(Action.PAUSE))
            launch(context, intent)
        }

        fun restore(context: WeakReference<Context>, orderInfo: NewOrderInfo, restoredCost: Int) {
            d("TAXOMETER restore")
            val intent = Intent(context.get(), TaxometerService::class.java)
            intent.putExtra(KEY_ACTION, Command(Action.RESTORE, orderInfo, restoredCost))
            launch(context, intent)
        }

        fun stop(context: WeakReference<Context?>) {
            d("TAXOMETER stop")
            val intent = Intent(context.get(), TaxometerService::class.java)
            intent.putExtra(KEY_ACTION, Command(Action.STOP))
            context.get()?.stopService(intent)
        }


        private fun launch(context: WeakReference<Context>, intent: Intent) {
            d("TAXOMETER launch")
            if (isPreAndroidO()) {
                context.get()?.startService(intent)
            } else {
                context.get()?.startForegroundService(intent)
            }
        }
    }
}