package com.wezom.taxi.driver.presentation.history.list

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.view.LayoutInflater
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.common.formatAddress
import com.wezom.taxi.driver.common.formatDateTime
import com.wezom.taxi.driver.data.models.Trip
import com.wezom.taxi.driver.databinding.ItemViewOrderHistoryBinding
import com.wezom.taxi.driver.ext.setVisible
import com.wezom.taxi.driver.presentation.base.lists.AdapterClickListener
import com.wezom.taxi.driver.presentation.base.lists.ItemModel

/**
 * Created by Andrew on 14.03.2018.
 */

class OrderHistoryItemView : ConstraintLayout, ItemModel<Trip> {
    private lateinit var listener: AdapterClickListener<Trip>
    private val binding = ItemViewOrderHistoryBinding.inflate(
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater,
            this,
            true
    )

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int)
            : super(context, attrs, defStyleAttr)

    fun setClickListener(listener: AdapterClickListener<Trip>) {
        this.listener = listener
    }

    override fun setData(data: Trip) {
        with(binding) {
            binding.orderHistoryDateText.text = formatDateTime(data.time)
            binding.orderHistoryPriceText.text = data.income.toInt().toString()
            val ratingVal = data.user.rating
            if (ratingVal == null || ratingVal == 0.0f) {
                orderHistoryRatingText.setVisible(false)
                orderHistoryRatingImage.setVisible(false)

            } else {
                orderHistoryRatingText.text = data.user.rating?.toString()
            }

            orderHistoryNameText.text = data.user.name
            orderHistoryFromText.text = formatAddress(data.startAddress)
            orderHistoryToImage.setVisible(true)
            if (data.finishAddress == null) {
                orderHistoryToText.text = context.getString(R.string.around_town)
            } else {
                orderHistoryToText.text = formatAddress(data.finishAddress)
            }

            if (!data.notPaidMessage.isNullOrEmpty()) {
                orderHistoryFaultImage.setVisible(true)
                orderHistoryFaultText.setVisible(true)
                orderHistoryFaultText.text = data.notPaidMessage
            } else {
                orderHistoryFaultImage.setVisible(false)
                orderHistoryFaultText.setVisible(false)
            }

            if (!data.cancelMessage.isNullOrEmpty()) {
                orderHistoryCancelText.setVisible(true)
                orderHistoryCancelText.text = data.cancelMessage
                infoLayout.setVisible(false)
            } else {
                orderHistoryCancelText.setVisible(false)
            }

            itemViewHistory.setOnClickListener({
                val position = (parent as RecyclerView).getChildAdapterPosition(this@OrderHistoryItemView)
                listener.onItemClick(position, data)
            })
        }
    }
}