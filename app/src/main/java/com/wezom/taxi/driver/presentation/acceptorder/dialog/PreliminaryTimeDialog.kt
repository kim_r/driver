package com.wezom.taxi.driver.presentation.acceptorder.dialog

import android.app.Dialog
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.databinding.DialogPreliminaryTimeBinding
import com.wezom.taxi.driver.presentation.profile.dialog.InfoDialog.Companion.KEY_IS_NEGATIVE
/**
 *Created by Zorin.A on 20.June.2019.
 */
class PreliminaryTimeDialog : DialogFragment() {

    var listener: DialogClickListener? = null

    private lateinit var binding: DialogPreliminaryTimeBinding

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        binding = DataBindingUtil.inflate(activity!!.layoutInflater,
                                          R.layout.dialog_preliminary_time,
                                          null,
                                          false)
        updateUi(arguments?.getString(KEY_TIME))

        val isNegativeButtonEnable = arguments?.getBoolean(KEY_IS_NEGATIVE)

        val builder = AlertDialog.Builder(context!!,
                                          R.style.WhiteDialogTheme)


        builder.setView(binding.root)
            .setPositiveButton(R.string.ok) { _, _ -> listener?.onPositiveClick() }

        if (isNegativeButtonEnable!!) builder.setNegativeButton(R.string.cancel_1) { _, _ -> listener?.onNegativeClick() }

        return builder.create()
    }

    private fun updateUi(time: String? = "00:00") {
        binding.run {
            textForTime.text = time
        }
    }

    interface DialogClickListener {
        fun onPositiveClick()
        fun onNegativeClick()
    }

    companion object {
        val TAG: String = "PreliminaryTimeDialog"
        const val KEY_TIME = "key_time"

        fun newInstance(time: String): PreliminaryTimeDialog {
            val args = Bundle()
            args.putString(KEY_TIME,
                           time)

            val fragment = PreliminaryTimeDialog()
            fragment.arguments = args
            return fragment
        }
    }
}