package com.wezom.taxi.driver.common

import android.content.SharedPreferences
import android.location.Location
import com.wezom.taxi.driver.ext.LATITUDE
import com.wezom.taxi.driver.ext.LOCATION_TIME
import com.wezom.taxi.driver.ext.LONGITUDE
import com.wezom.taxi.driver.ext.long
import javax.inject.Inject

/**
 * Created by zorin.a on 10.01.2019.
 */
class LocationStorage @Inject constructor(preferences: SharedPreferences) {
    private var lat: Long by preferences.long(LATITUDE, 0)
    private var lon: Long by preferences.long(LONGITUDE, 0)
    private var locationTime: Long by preferences.long(LOCATION_TIME)

    /**
     * @param location - location to be cache
     */
    fun storeLocation(location: Location) {
        val rawLat = convertToLong(location.latitude)
        val rawLon = convertToLong(location.longitude)
        lat = rawLat
        lon = rawLon
        locationTime = location.time
    }

    /**
     * get last cache location
     */
    fun getCachedLocation(): Location = Location("LocationStorage").apply {
        latitude = convertToDouble(lat)
        longitude = convertToDouble(lon)
    }

    private fun convertToLong(input: Double) = input.toRawBits()
    private fun convertToDouble(input: Long) = Double.fromBits(input)

}