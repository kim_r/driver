package com.wezom.taxi.driver.presentation.picklocation

import android.content.SharedPreferences
import com.wezom.taxi.driver.net.api.ApiManager
import com.wezom.taxi.driver.presentation.base.BaseMapViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import javax.inject.Inject

/**
 *Created by Zorin.A on 31.July.2019.
 */
class PickLocationMapViewModel @Inject constructor(screenRouterManager: ScreenRouterManager,
                                                   sharedPreferences: SharedPreferences,
                                                   private val apiManager: ApiManager) : BaseMapViewModel(screenRouterManager, apiManager, sharedPreferences) {


}