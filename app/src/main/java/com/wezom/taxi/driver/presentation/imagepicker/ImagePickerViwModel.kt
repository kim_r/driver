package com.wezom.taxi.driver.presentation.imagepicker

import android.arch.lifecycle.MutableLiveData
import android.content.SharedPreferences
import android.net.Uri
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.common.*
import com.wezom.taxi.driver.ext.*
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.presentation.photoviewer.PhotoViewerFragment
import com.wezom.taxi.driver.presentation.photoviewer.PhotoViewerFragment.Companion.CropOptions.*
import com.wezom.taxi.driver.presentation.profile.DocumentsFragment
import ru.terrakok.cicerone.result.ResultListener
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by zorin.a on 19.12.2018.
 */
class ImagePickerViwModel @Inject constructor(screenRouterManager: ScreenRouterManager,
                                              sharedPreferences: SharedPreferences) : BaseViewModel(screenRouterManager) {
    val screenPrepareLiveData = MutableLiveData<ImagePickerData>()

    private var userImageUriString: String by sharedPreferences.string(USER_IMAGE_URI)
    private var carImageUriString: String by sharedPreferences.string(CAR_IMAGE_URI)
    private var doc1UriString: String by sharedPreferences.string(FIRST_DOC_URI)
    private var doc2UriString: String by sharedPreferences.string(SECOND_DOC_URI)
    private var doc3UriString: String by sharedPreferences.string(THIRD_DOC_URI)
    private var doc4UriString: String by sharedPreferences.string(FOURTH_DOC_URI)
    var imageLiveData = MutableLiveData<String>()

    fun prepareImage(source: String?) {
        Timber.d("PHOTO_VIEWER_SCREEN prepareImage")
        imageLiveData.postValue(source)
    }

    fun prepareScreen(imageId: Int?, source: String?) {
        Timber.d("PHOTO_VIEWER_SCREEN prepareScreen")
        var data: ImagePickerData? = null
        when (imageId) {
            DocumentsFragment.USER_IMG_ID -> {
                val requirements = mutableListOf<Int>()
                requirements.add(R.string.profile_req_1)
                requirements.add(R.string.profile_req_2)
                requirements.add(R.string.profile_req_3)
                data = ImagePickerData(
                        title = R.string.profile_photo,
                        description = R.string.profile_photo_description,
                        requirementsTitle = R.string.photo_requirements_title,
                        requirements = requirements,
                        source = source,
                        placeholderRes = R.drawable.ic_driver_info,
                        isShowDescriptionTitle = true
                )
            }

            DocumentsFragment.CAR_IMG_ID -> {
                data = ImagePickerData(
                        title = R.string.car_photo,
                        description = R.string.car_photo_description,
                        requirementsTitle = R.string.photo_requirements_title,
                        requirements = null,
                        source = source,
                        placeholderRes = R.drawable.ic_car_info,
                        isShowDescriptionTitle = false
                )
            }

            DocumentsFragment.DOC_1_IMG_ID -> {
                val requirements = mutableListOf<Int>()
                requirements.add(R.string.doc1_req_1)
                requirements.add(R.string.doc1_req_2)
                requirements.add(R.string.doc1_req_3)
                data = ImagePickerData(
                        title = R.string.dialog_document_title1,
                        description = R.string.doc1_photo_description,
                        requirementsTitle = R.string.photo_requirements_title,
                        requirements = requirements,
                        source = source,
                        placeholderRes = R.drawable.ic_doc1_info,
                        isShowDescriptionTitle = true
                )
            }

            DocumentsFragment.DOC_2_IMG_ID -> {
                val requirements = mutableListOf<Int>()
                requirements.add(R.string.doc2_req_1)
                requirements.add(R.string.doc2_req_2)
                requirements.add(R.string.doc2_req_3)
                data = ImagePickerData(
                        title = R.string.dialog_document_title2,
                        description = R.string.doc2_photo_description,
                        requirementsTitle = R.string.photo_requirements_title,
                        requirements = requirements,
                        source = source,
                        placeholderRes = R.drawable.ic_doc2_info,
                        isShowDescriptionTitle = true
                )
            }

            DocumentsFragment.DOC_3_IMG_ID -> {//TODO:from here
                val requirements = mutableListOf<Int>().apply {
                    add(R.string.doc3_req_1)
                    add(R.string.doc3_req_2)
                    add(R.string.doc3_req_3)
                }
                data = ImagePickerData(
                        title = R.string.dialog_document_title3,
                        description = R.string.doc3_photo_description,
                        requirementsTitle = R.string.photo_requirements_title,
                        requirements = requirements,
                        source = source,
                        placeholderRes = R.drawable.ic_doc3_info,
                        isShowDescriptionTitle = true
                )
            }

            DocumentsFragment.DOC_4_IMG_ID -> {
                val requirements = mutableListOf<Int>()
                requirements.add(R.string.doc4_req_1)
                requirements.add(R.string.doc4_req_2)
                requirements.add(R.string.doc4_req_3)
                data = ImagePickerData(
                        title = R.string.dialog_document_title4,
                        description = R.string.doc4_photo_description,
                        requirementsTitle = R.string.photo_requirements_title,
                        requirements = requirements,
                        source = source,
                        placeholderRes = R.drawable.ic_doc4_info,
                        isShowDescriptionTitle = true
                )
            }
        }
        screenPrepareLiveData.postValue(data)
    }

    fun cropImage(source: String, imageId: Int) {
        Timber.d("PHOTO_VIEWER_SCREEN cropImage")
        val cropMode = getCropModeForDoc(imageId)

        startScreenForResult(PHOTO_VIEWER_SCREEN, Triple(source, cropMode, imageId), ResultListener { it ->
            val tmp = it as Pair<String, Int> //tmp uri and image id

            Timber.d("PHOTO_VIEWER_SCREEN ResultListener callback")
            when (tmp.second) {
                DocumentsFragment.USER_IMG_ID -> {
                    userImageUriString = saveCroppedImageUri(tmp.first, "user").toString()
//                    imageLiveData.postValue(userImageUriString)
//                    tmpSource = userImageUriString
                }
                DocumentsFragment.CAR_IMG_ID -> {
                    carImageUriString = saveCroppedImageUri(tmp.first, "car").toString()
//                    imageLiveData.postValue(carImageUriString)
//                    tmpSource = carImageUriString
                }
                DocumentsFragment.DOC_1_IMG_ID -> {
                    doc1UriString = saveCroppedImageUri(tmp.first, "doc1").toString()
//                    imageLiveData.postValue(doc1UriString) //get tmp uri of cropped file
//                    tmpSource = doc1UriString
                }
                DocumentsFragment.DOC_2_IMG_ID -> {
                    doc2UriString = saveCroppedImageUri(tmp.first, "doc2").toString()
//                    imageLiveData.postValue(doc2UriString)
//                    tmpSource = doc2UriString
                }
                DocumentsFragment.DOC_3_IMG_ID -> {
                    doc3UriString = saveCroppedImageUri(tmp.first, "doc3").toString()
//                    imageLiveData.postValue(doc3UriString)
//                    tmpSource = doc3UriString
                }
                DocumentsFragment.DOC_4_IMG_ID -> {
                    doc4UriString = saveCroppedImageUri(tmp.first, "doc4").toString()
//                    imageLiveData.postValue(doc4UriString)
//                    tmpSource = doc4UriString
                }
            }
            removeResultListener(imageId)
            backToScreen(PROFILE_SCREEN)
        }, imageId)
    }

    private fun getCropModeForDoc(imageId: Int): PhotoViewerFragment.Companion.CropOptions {
        return when (imageId) {
            DocumentsFragment.USER_IMG_ID -> CROP_ROUNDED
            DocumentsFragment.CAR_IMG_ID -> RATIO_16_12
            else -> {
                FREE
            }
        }
    }


    private fun saveCroppedImageUri(croppedUriString: String?, fileName: String): Uri {
        val bitmap = decodeUriToBitmap(context!!, Uri.parse(croppedUriString))
        val uri = storeBitmapToDisk(context!!, bitmap, fileName)
        return uri
    }
}