package com.wezom.taxi.driver.service.notification

import android.support.annotation.RawRes
import com.wezom.taxi.driver.R

enum class NotificationType(val typeId: Int, @RawRes val sound: Int) {

    NEW_ORDER_AVAILABLE(23, R.raw.new_order_sound),
    DRIVER_BLOCKED(24, R.raw.custom_sound),
    UNLOCK(28, R.raw.custom_sound),
    CUSTOM_PUSH(50, R.raw.custom_sound),
    SUCCESSFUL_MODERATION(27, R.raw.custom_sound),
    ADDED_NEW_DISCOUNT(25, R.raw.custom_sound),
    INVALID_REGISTRATION_DATA(21, R.raw.custom_sound),
    CANCEL_ORDER(22, R.raw.custom_sound),
    DOWNLOADING_MISSING_PHOTO(26, R.raw.custom_sound),
    LIMITED(29, R.raw.custom_sound);

    companion object {
        private val map: Map<Int, NotificationType>

        init {
            map = NotificationType.values()
                    .fold(hashMapOf()) { acc, pushType ->
                        acc[pushType.typeId] = pushType
                        acc
                    }
        }

        fun getPushType(typeId: Int?): NotificationType? {
            return map[typeId]
        }
    }
}