package com.wezom.taxi.driver.data.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.io.Serializable


/**
 * Created by zorin.a on 022 22.02.18.
 */

//reason for refuse order, for example
@Entity(tableName = "order_reasons")
data class Reason(
        @PrimaryKey(autoGenerate = true)
        var localId: Long?,
        @SerializedName("id") var id: Int?,
        @SerializedName("name") var name: String?,
        @SerializedName("isRequiredMessage") var isRequiredMessage: Boolean?,
        @SerializedName("timeToShowAfter") var timeToShowAfter: Long?,//+System.currentTimemillis
        @SerializedName("isCommentRequired") var isCommentRequired: Boolean?,
        var orderStatusToCorrespondFor: OrderStatus?,
        var finalTime: Long?
) : Parcelable, Serializable {
    constructor(source: Parcel) : this(
            source.readValue(Long::class.java.classLoader) as Long?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readValue(Boolean::class.java.classLoader) as Boolean?,
            source.readValue(Long::class.java.classLoader) as Long?,
            source.readValue(Boolean::class.java.classLoader) as Boolean?,
            source.readValue(Int::class.java.classLoader)?.let { OrderStatus.values()[it as Int] },
            source.readValue(Long::class.java.classLoader) as Long?
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeValue(localId)
        writeValue(id)
        writeString(name)
        writeValue(isRequiredMessage)
        writeValue(timeToShowAfter)
        writeValue(isCommentRequired)
        writeValue(orderStatusToCorrespondFor?.ordinal)
        writeValue(finalTime)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Reason> = object : Parcelable.Creator<Reason> {
            override fun createFromParcel(source: Parcel): Reason = Reason(source)
            override fun newArray(size: Int): Array<Reason?> = arrayOfNulls(size)
        }
    }
}
