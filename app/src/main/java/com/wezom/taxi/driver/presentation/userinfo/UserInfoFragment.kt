package com.wezom.taxi.driver.presentation.userinfo

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.common.loadImage
import com.wezom.taxi.driver.data.models.User
import com.wezom.taxi.driver.databinding.FragmentUserInfoBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.presentation.base.BaseFragment

/**
 *Created by Zorin.A on 21.June.2019.
 */
class UserInfoFragment : BaseFragment() {
    lateinit var viewModel: UserInfoViewModel
    lateinit var binding: FragmentUserInfoBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this,
                                          viewModelFactory)
            .getViewModelOfType()

        arguments?.let {
            viewModel.user = it.getParcelable(USER_KEY) as User
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentUserInfoBinding.inflate(inflater,
                                                  container,
                                                  false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onViewCreated(view: View,
                               savedInstanceState: Bundle?) {
        super.onViewCreated(view,
                            savedInstanceState)
        initViews()
    }

    @SuppressLint("SetTextI18n")
    private fun initViews() {
        binding.run {
            backButton.setOnClickListener {
                viewModel.onBackPressed()
            }
            val user = viewModel.user
            loadImage(context!!,
                      userImage,
                      user.photoOrigin,
                      placeHolderId = R.drawable.ic_ava_vector40px)
            userName.text = user.name
            user.rating?.let {
                ratingLabel.text = it.toString()
            }
            positiveDrivesLabel.text = "${positiveDrivesLabel.text}:"
            negativeDrivesLabel.text = "${negativeDrivesLabel.text}:"
            positiveDrivesVaule.text = (user.doneOrdersCount ?: 0).toString()
            negativeDrivesValue.text = (user.undoneOrdersCount ?: 0).toString()
        }
    }

    companion object {
        const val USER_KEY = "user_key"
        fun newInstance(user: User): UserInfoFragment {
            val bundle = Bundle()
            bundle.putParcelable(USER_KEY,
                                 user)
            val fragment = UserInfoFragment()
            fragment.arguments = bundle
            return fragment
        }
    }
}