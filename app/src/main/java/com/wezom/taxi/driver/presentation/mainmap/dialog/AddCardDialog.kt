package com.wezom.taxi.driver.presentation.mainmap.dialog

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import com.wezom.taxi.driver.R


/**
 * Created by zorin.a on 05.03.2018.
 */
class AddCardDialog : DialogFragment() {
    var listener: DialogClickListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(context!!, R.style.BlackDialogTheme)
                .setTitle("")
                .setMessage(context!!.getString(R.string.add_card_message))
                .setPositiveButton(R.string.ok, { dialogInterface, which -> listener?.onPositiveClick() })
                .setNegativeButton(R.string.cancel, { dialogInterface, which -> listener?.onNegativeClick() })
                .create()
    }

    companion object {
        val TAG: String = AddCardDialog.javaClass.simpleName
    }

    interface DialogClickListener {
        fun onPositiveClick()
        fun onNegativeClick()
    }
}