package com.wezom.taxi.driver.presentation.executeorder.dialog

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import com.wezom.taxi.driver.R


/**
 * Created by zorin.a on 05.03.2018.
 */
class NotToEndLocationClientDialog : DialogFragment() {
    var listener: DialogClickListener? = null

    @SuppressLint("DefaultLocale")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(context!!, R.style.BlackDialogTheme)
                .setTitle(getString(R.string.attention2))
                .setMessage(getString(R.string.not_in_end_location))
                .setPositiveButton(getString(R.string.end).toUpperCase()) { _, _ ->
                    if(listener != null)
                    listener?.onPositiveClick() }
                .setNegativeButton(getString(R.string.cancel_1).toUpperCase()) { _, _ ->
                    if(listener != null)
                    listener?.onNegativeClick() }
                .create()
    }

    companion object {
        val TAG: String = NotToEndLocationClientDialog.javaClass.simpleName
    }

    interface DialogClickListener {
        fun onPositiveClick()
        fun onNegativeClick()
    }
}