package com.wezom.taxi.driver.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by zorin.a on 020 20.02.18.
 */
class Error {
    @SerializedName("code")
    val code: Int? = null
    @SerializedName("message")
    val message: String? = null
}