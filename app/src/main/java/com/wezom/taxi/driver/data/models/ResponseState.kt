package com.wezom.taxi.driver.data.models

/**
 * Created by zorin.a on 19.03.2018.
 */
class ResponseState(var state: State, var message: String? = null, var errorCode: Int? = null) {
    enum class State { IDLE, LOADING, EMPTY, ERROR, NETWORK_ERROR }

}