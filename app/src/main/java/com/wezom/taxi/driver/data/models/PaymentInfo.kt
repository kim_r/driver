package com.wezom.taxi.driver.data.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


data class PaymentInfo(
        @SerializedName("merchantSignature")
        val merchantSignature: String = "",
        @SerializedName("amount")
        val amount: Int = 0,
        @SerializedName("apiVersion")
        val apiVersion: Int = 0,
        @SerializedName("merchantAccount")
        val merchantAccount: String = "",
        @SerializedName("clientEmail")
        val clientEmail: String = "",
        @SerializedName("merchantDomainName")
        val merchantDomainName: String = "",
        @SerializedName("serviceUrl")
        val serviceUrl: String = "",
        @SerializedName("orderReference")
        val orderReference: String = "",
        @SerializedName("currency")
        val currency: String = "",
        @SerializedName("clientPhone")
        val clientPhone: String = "",
        //new properties
        @SerializedName("orderDate")
        val orderDate: String = "",
        @SerializedName("productName")
        val productName: String = "",
        @SerializedName("productCount")
        val productCount: Int = 0,
        @SerializedName("productPrice")
        val productPrice: Int = 0,
        @SerializedName("merchantTransactionType")
        val merchantTransactionType: String = "",
        @SerializedName("merchantTransactionSecureType")
        val merchantTransactionSecureType: String = "",
        @SerializedName("holdTimeout")
        val holdTimeout: Int = 0
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),

            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(merchantSignature)
        parcel.writeInt(amount)
        parcel.writeInt(apiVersion)
        parcel.writeString(merchantAccount)
        parcel.writeString(clientEmail)
        parcel.writeString(merchantDomainName)
        parcel.writeString(serviceUrl)
        parcel.writeString(orderReference)
        parcel.writeString(currency)
        parcel.writeString(clientPhone)

        parcel.writeString(orderDate)
        parcel.writeString(productName)
        parcel.writeInt(productCount)
        parcel.writeInt(productPrice)
        parcel.writeString(merchantTransactionType)
        parcel.writeString(merchantTransactionSecureType)
        parcel.writeInt(holdTimeout)



    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PaymentInfo> {
        override fun createFromParcel(parcel: Parcel): PaymentInfo {
            return PaymentInfo(parcel)
        }

        override fun newArray(size: Int): Array<PaymentInfo?> {
            return arrayOfNulls(size)
        }
    }
}