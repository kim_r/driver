package com.wezom.taxi.driver.presentation.customview

import android.content.Context
import android.content.DialogInterface
import android.support.design.widget.Snackbar
import android.support.transition.TransitionManager
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AlertDialog
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.data.models.ResponseState
import com.wezom.taxi.driver.databinding.StateBinding
import com.wezom.taxi.driver.ext.setVisible
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by zorin.a on 01.04.2018.
 */

class StateView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0) : FrameLayout(context, attrs, defStyleAttr) {

    var retryListener: OnRetryListener? = null
    val disposable: CompositeDisposable = CompositeDisposable()
    val binding = StateBinding.inflate(LayoutInflater.from(context), this, true)
    private var retrySnackBar: Snackbar? = null
    private var swipeLayout: SwipeRefreshLayout? = null
    var isOnlyDialogMode: Boolean = false

       override fun onDetachedFromWindow() {
        disposable.dispose()
        super.onDetachedFromWindow()
    }

    fun setMessageText(text: String) {
        binding.message.text = text
    }

    fun setState(rs: ResponseState) {
        when (rs.state) {
            ResponseState.State.IDLE -> {
                switchVisibility(false, false, false)
                swipeLayout?.isRefreshing = false

            }
            ResponseState.State.LOADING -> {
                switchVisibility(true, false, false)
            }
            ResponseState.State.EMPTY -> {
                if (isOnlyDialogMode) {
                    switchVisibility(false, false, false)
                } else {
                    switchVisibility(false, true, true)
                }

                binding.message.text = context.getString(R.string.empty_data)
                swipeLayout?.isRefreshing = false
            }
            ResponseState.State.ERROR -> {
                if (isOnlyDialogMode) {
                    switchVisibility(false, false, false)
                } else {
                    switchVisibility(false, true, true)
                }
                swipeLayout?.isRefreshing = false
                binding.message.text = context.getString(R.string.error)
            }
            ResponseState.State.NETWORK_ERROR -> {
                if (isOnlyDialogMode) {
                    switchVisibility(false, false, false)
                } else {
                    switchVisibility(false, true, false)
                }
                swipeLayout?.isRefreshing = false
            }
        }
    }

    fun showNetworkErrorDialog(title: Int? = null, errorCode: Int? = null) =
            createDialog(context.getString(title ?: R.string.error),
                    context.getString(errorCode ?: R.string.network_error))

    fun showErrorDialog(errorMessage: String) {
        createDialog(context.getString(R.string.error),
                errorMessage)
    }

    private fun createDialog(title: String? = "", message: String) =
            AlertDialog.Builder(context!!).setTitle(title)
                    .setMessage(message)
                    .setPositiveButton(R.string.ok) { _, _ ->
                        DialogInterface.OnClickListener { dialog, which -> dialog?.dismiss() }
                    }
                    .create()
                    .show()

    private fun switchVisibility(isProgressShow: Boolean, isRetryShow: Boolean, isMessageShow: Boolean) {
        binding.run {
            TransitionManager.beginDelayedTransition(root)
            progress.setVisible(isProgressShow)
            showRetry(isRetryShow)
            message.setVisible(isMessageShow)
        }
    }

    private fun showRetry(retryShow: Boolean) {
        if (retryShow) {
            retrySnackBar = Snackbar.make(binding.stateViewCoordinator,
                    context.getString(R.string.network_error),
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(context.getString(R.string.retry)) {
                        retryListener?.onRetryClicked()
                    }
            retrySnackBar?.show()
        } else {
            if (retrySnackBar != null)
                retrySnackBar?.dismiss()
        }
    }

    interface OnRetryListener {
        fun onRetryClicked()
    }

    fun setRefreshLayout(srl: SwipeRefreshLayout) {
        this.swipeLayout = srl
    }

}