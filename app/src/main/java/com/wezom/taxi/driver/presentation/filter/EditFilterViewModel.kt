package com.wezom.taxi.driver.presentation.filter

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.widget.Toast
import com.wezom.taxi.driver.common.FILTER_SCREEN
import com.wezom.taxi.driver.common.PICK_LOCATION_SCREEN
import com.wezom.taxi.driver.data.models.ResponseState
import com.wezom.taxi.driver.net.request.CreateFilterRequest
import com.wezom.taxi.driver.net.response.AutoCompleteResult
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.repository.DriverRepository
import ru.terrakok.cicerone.result.ResultListener
import timber.log.Timber
import javax.inject.Inject

/**
 *Created by Zorin.A on 26.June.2019.
 */
class EditFilterViewModel @Inject constructor(
        private val repository: DriverRepository,
        private val ctx: Context,
        screenRouterManager: ScreenRouterManager) : BaseViewModel(screenRouterManager) {
    var resultLiveData = MutableLiveData<String>()
    var resultPointLiveData = MutableLiveData<AutoCompleteResult>()


    fun openPickPlaceScreen() {
        startScreenForResult(screen = PICK_LOCATION_SCREEN,
                resultListener = ResultListener {
                    Timber.d("GOT $it")
                    val data: AutoCompleteResult = it as AutoCompleteResult
                    resultLiveData.postValue(data.place + "," + data.obj)
                    resultPointLiveData.postValue(data)
                    removeResultListener(RESULT_CODE)
                },
                resultCode = RESULT_CODE)
    }

    @SuppressLint("CheckResult")
    fun editFilter(filterId: Int, filter: CreateFilterRequest) {
        loadingLiveData.postValue(ResponseState(ResponseState.State.LOADING))
        App.instance.getLogger()!!.log("editFilter start")
        repository.editFilter(filterId, filter).subscribe({
            App.instance.getLogger()!!.log("editFilter suc")
            if (it.isSuccess!!) {
                handleResponseState(it)
                replaceScreen(FILTER_SCREEN)
            } else {
                Toast.makeText(ctx, it.error!!.message, Toast.LENGTH_LONG).show()
                Timber.e(it.error!!.message)
            }
        }, { it ->
            App.instance.getLogger()!!.log("editFilter error")
            loadingLiveData.postValue(ResponseState(ResponseState.State.NETWORK_ERROR)) })
    }


    fun deleteFilter(filterId: Int) {
        loadingLiveData.postValue(ResponseState(ResponseState.State.LOADING))
        App.instance.getLogger()!!.log("deleteFilter start")
        repository.deleteFilter(filterId).subscribe({
            App.instance.getLogger()!!.log("deleteFilter suc")
            if (it.isSuccess!!) {
                replaceScreen(FILTER_SCREEN)
                loadingLiveData.postValue(ResponseState(ResponseState.State.IDLE))
            } else {
                loadingLiveData.postValue(ResponseState(ResponseState.State.ERROR, message = it.error?.message))
                errorHandler.onError(it.error!!.code!!, it.error.message)
                Timber.e(it.error.message)
            }
        }, { it ->
            App.instance.getLogger()!!.log("deleteFilter error")
            loadingLiveData.postValue(ResponseState(ResponseState.State.NETWORK_ERROR)) })
    }

    companion object {
        const val RESULT_CODE = 1
    }
}