package com.wezom.taxi.driver.data.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by udovik.s on 22.03.2018.
 */
data class RateRequest(@SerializedName("rate") val rate: Int?,
                       @SerializedName("comment") val comment: String? = null,
                       @SerializedName("eventTime") val eventTime: Long? = null) : Serializable