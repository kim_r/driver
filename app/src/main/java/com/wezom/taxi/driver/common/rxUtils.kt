package com.wezom.taxi.driver.common

import io.reactivex.ObservableTransformer
import io.reactivex.SingleTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by zorin.a on 020 20.02.18.
 */
fun <T : Any> applyObservableSchedulers(): ObservableTransformer<T, T> {
    return ObservableTransformer { upstream ->
        upstream.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }
}

fun <T : Any> applySingleSchedulers(): SingleTransformer<T, T> {
    return SingleTransformer { upstream ->
        upstream.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }
}