package com.wezom.taxi.driver.presentation.customview

import android.content.Context
import android.graphics.*
import android.support.v4.content.ContextCompat
import android.text.TextPaint
import android.util.AttributeSet
import android.view.View
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.common.dpToPx

import timber.log.Timber


/**
 * Created by zorin.a on 3/12/19.
 */
class DrivesProgressView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private var values: List<Int>? = null
    private var currentValue: Int = 0
    private var maxValue: Int = 0

    private var fontSize = dpToPx(12)
    private var parentWidth = 0
    private var parentHeight = 0
    private var centerHorizontal = 0f
    private var centerVertical = 0f
    private val paddingHorizontal = dpToPx(16).toFloat()
    private val paddingVertical = dpToPx(16).toFloat()
    private val progressHeight = dpToPx(6).toFloat()

    private val progressPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        strokeWidth = 1.0f
        style = Paint.Style.FILL_AND_STROKE
        color = ContextCompat.getColor(context, R.color.colorAccent)
    }
    private val progressPaintBg: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        strokeWidth = 1.0f
        style = Paint.Style.FILL_AND_STROKE
        color = ContextCompat.getColor(context, R.color.colorLiteGrey)
    }
    private val strokesPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        strokeWidth = dpToPx(2).toFloat()
        style = Paint.Style.STROKE
        color = ContextCompat.getColor(context, R.color.colorWhite)
    }
    private val textPaint = TextPaint(Paint.ANTI_ALIAS_FLAG).apply {
        textSize = fontSize.toFloat()
        color = Color.WHITE
        style = Paint.Style.FILL
        textAlign = Paint.Align.CENTER
    }
    private val imagePaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val checkImg by lazy {
        val drawable = ContextCompat.getDrawable(context, R.drawable.ic_verified_bonus_condition)!!
        val icon = Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(icon)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        icon
    }

    override fun onDraw(canvas: Canvas?) {
        Timber.d("DRAW_onDraw")
        //progressbar background
        createScaleBackground(canvas)

        if (!values.isNullOrEmpty()) {
            val part = (parentWidth - paddingHorizontal * 2).div(values!!.size) //single part of scales

            createForegroundProgress(part, canvas)
            createScale(canvas, part)

            //draw scale values
            for (index in 0..values!!.size) {
                val space = paddingHorizontal + part.times(index)
                when (index) {
                    0 -> {
                        createScaleText(canvas, 0.toString(), space)
                    }
                    else -> {
                        createScaleText(canvas, values!![index - 1].toString(), space)
                    }
                }
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        parentWidth = View.MeasureSpec.getSize(widthMeasureSpec)
        parentHeight = View.MeasureSpec.getSize(heightMeasureSpec)
        centerHorizontal = parentWidth * 0.5f
        centerVertical = parentHeight * 0.5f

        this.setMeasuredDimension(parentWidth, parentHeight)
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    private fun createScaleBackground(canvas: Canvas?) {
        canvas?.drawRect(
                paddingHorizontal,
                centerVertical - progressHeight * 0.5f,
                parentWidth.toFloat() - paddingHorizontal,
                centerVertical + progressHeight * 0.5f,
                progressPaintBg)
    }

    private fun createForegroundProgress(part: Float, canvas: Canvas?) {
        val tmpValues = mutableListOf<Int>() //for convenient, include first value - 0
        tmpValues.add(0)
        tmpValues.addAll(values!!)
        var currentAndClosestValuesDiff = Math.abs(tmpValues[0].toFloat() - currentValue)
        var closestToCurrentIndex = 0
        var closestToCurrentValue = tmpValues[0].toFloat()
        var maxCompletedIndex = 0

//get closest value and index to current progress
        tmpValues.forEachIndexed { index, value ->
            val tmpDiff = Math.abs(value.toFloat() - currentValue)
            Timber.d("DRAW_ index: $index, value: $value, diff: $currentAndClosestValuesDiff,  tmpDiff:$tmpDiff, currValue: $currentValue")
            if (currentAndClosestValuesDiff >= tmpDiff) {
                currentAndClosestValuesDiff = tmpDiff
                closestToCurrentIndex = index
                closestToCurrentValue = value.toFloat()
            }
            if (currentValue >= value) {
                maxCompletedIndex = index
            }
        }

        var nextClosestValue = 0
        if (currentAndClosestValuesDiff != 0f) { // check is nearest value difference is not 0
            if (currentValue < closestToCurrentValue) {
                currentAndClosestValuesDiff *= -1
            }

            nextClosestValue = if (currentAndClosestValuesDiff < 0) {
                if (closestToCurrentIndex > 0) {
                    tmpValues[closestToCurrentIndex - 1]
                } else {
                    tmpValues[closestToCurrentIndex]
                }
            } else {
                tmpValues[closestToCurrentIndex + 1]
            }
        }
        val closestDiff = Math.abs(closestToCurrentValue - nextClosestValue)

        val scaleShare = currentAndClosestValuesDiff.div(closestDiff)
        val diffPart = part * scaleShare
        Timber.d("DRAW_ diff: $currentAndClosestValuesDiff")
        Timber.d("DRAW_ closest index: $closestToCurrentIndex")
        Timber.d("DRAW_ closest value: $closestToCurrentValue")
        Timber.d("DRAW_ next closest value: $nextClosestValue")
        Timber.d("DRAW_ closestDiff: $closestDiff")
        Timber.d("DRAW_ maxCompletedIndex: $maxCompletedIndex")
        val fullPart = part * closestToCurrentIndex + paddingHorizontal + diffPart

        canvas?.drawRect(
                paddingHorizontal,
                centerVertical - progressHeight * 0.5f,
                fullPart,
                centerVertical + progressHeight * 0.5f,
                progressPaint)

        if (maxCompletedIndex > 0)
            canvas?.drawBitmap(
                    checkImg,
                    part * maxCompletedIndex + paddingHorizontal - checkImg.width * 0.5f,
                    parentHeight - (paddingVertical + checkImg.height),
                    imagePaint)
    }

    private fun createScale(canvas: Canvas?, part: Float) {
        for (index in 0..values!!.size + 1) {
            val space = paddingHorizontal + part.times(index)

            drawVerticalScaleLines(canvas, space)
        }
    }

    private fun createScaleText(canvas: Canvas?, value: String, space: Float) {
        canvas?.drawText(
                value,
                space,
                centerVertical + progressHeight + fontSize,
                textPaint)
    }

    private fun drawVerticalScaleLines(canvas: Canvas?, space: Float) {
        canvas?.drawLine(
                space,
                centerVertical - progressHeight * 0.5f,
                space,
                centerVertical + progressHeight * 0.5f,
                strokesPaint
        )
    }

    fun setData(values: List<Int>, progress: Int? = 0) {
        this.values = values
        this.currentValue = progress ?: 0
        this.maxValue = values.max() ?: 0
        if (currentValue > maxValue) currentValue = maxValue
        invalidate()
    }
}