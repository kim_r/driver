package com.wezom.taxi.driver.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by zorin.a on 28.03.2018.
 */
enum class PaymentMethod {
    @SerializedName("1")
    CASH,
    @SerializedName("2")
    CASH_AND_BONUS,
    @SerializedName("3")
    CARD,
    @SerializedName("4")
    CARD_AND_BONUS
}