package com.wezom.taxi.driver.presentation.orderfound

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.widget.Toast
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.bus.events.AutoCloseOrderEvent
import com.wezom.taxi.driver.common.*
import com.wezom.taxi.driver.data.db.DbManager
import com.wezom.taxi.driver.data.models.*
import com.wezom.taxi.driver.ext.SOUND_ENABLED
import com.wezom.taxi.driver.ext.USER_ID
import com.wezom.taxi.driver.ext.boolean
import com.wezom.taxi.driver.ext.int
import com.wezom.taxi.driver.net.api.ApiManager
import com.wezom.taxi.driver.net.request.AcceptOrderRequest
import com.wezom.taxi.driver.net.request.SkipOrderRequest
import com.wezom.taxi.driver.net.response.NotPaidReasonResponse
import com.wezom.taxi.driver.net.response.RefuseReasonResponse
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseMapViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.presentation.customview.DriverToolbar.Companion.IS_SECONDARY_ORDER_EXIST
import com.wezom.taxi.driver.presentation.main.events.ShowSecondaryOrderButtonEvent
import com.wezom.taxi.driver.repository.DriverRepository
import com.wezom.taxi.driver.service.TaxometerService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.lang.ref.WeakReference
import javax.inject.Inject

/**
 * Created by zorin.a on 12.03.2018.
 */
class OrderFoundViewModel @Inject constructor(
        screenRouterManager: ScreenRouterManager,
        private val apiManager: ApiManager,
        sharedPreferences: SharedPreferences,
        private val dbManager: DbManager,
        private val orderManager: OrderManager,
        private var appMediaPlayer: AppMediaPlayer,
        private val locationStorage: LocationStorage,
        private val repository: DriverRepository) : BaseMapViewModel(screenRouterManager, apiManager, sharedPreferences) {

    private var userId by sharedPreferences.int(USER_ID)
    private var isSoundEnabled by sharedPreferences.boolean(SOUND_ENABLED, true)

    @SuppressLint("CheckResult")
    fun acceptOrder(orderInfo: NewOrderInfo, lon: Double?, lat: Double?) {
        val request = AcceptOrderRequest(longitude = lon, latitude = lat, eventTime = System.currentTimeMillis())
        App.instance.getLogger()!!.log("acceptOrder start ")
        apiManager.acceptOrder(orderInfo.order!!.id!!, request).subscribe({ response ->
            TaxometerService.stop(WeakReference(context))
            if (response?.isSuccess!!) {
                App.instance.getLogger()!!.log("currentOrder start ")
                App.instance.getLogger()!!.log("acceptOrder suc ")
//                dbManager.deleteOrderResult(orderInfo.id)
                apiManager.currentOrder(orderInfo.order!!.id!!).subscribe({ currentOrder ->
                    handleResponseState(response = response)
                    App.instance.getLogger()!!.log("currentOrder suc ")
                    Timber.d("AcceptOrderRequest ${orderInfo.order!!.distance}")
                    currentOrder.order!!.distanceFromDriver = orderInfo.order!!.distance!! * 1000
                    orderInfo.order = currentOrder.order
                    Timber.d("AcceptOrderRequest ${orderInfo.order!!.distanceFromDriver}")
                    orderManager.acceptOrder(orderInfo)
                    var finalization = orderManager.queryFinalization()
                    if (finalization == null) {
                        finalization = OrderFinalization()
                    }
                    finalization.accepted = Accepted(
                            longitude = lon ?: locationStorage.getCachedLocation().longitude,
                            latitude = lat ?: locationStorage.getCachedLocation().latitude,
                            eventTime = System.currentTimeMillis(),
                            driverId = userId)
                    orderManager.insertFinalization(finalization)

                    if (orderManager.isSecondaryOrderExist()) {
                        showSecondaryOrderButton()
                        onBackPressed()
                    } else {
                        setRootScreen(EXECUTE_ORDER_SCREEN, orderInfo)
                    }
                }, { App.instance.getLogger()!!.log("currentOrder error ") })
            } else {
                Toast.makeText(context,
                        response.error!!.message,
                        Toast.LENGTH_LONG)
                        .show()
                RxBus.publish(AutoCloseOrderEvent())
            }
        }, {
            RxBus.publish(AutoCloseOrderEvent())
            App.instance.getLogger()!!.log("acceptOrder error ")
        }) //don nothing on error
//        JobFactory.instance.getJobManager().addJobInBackground(AcceptOrderJob(orderInfo.order!!.id!!, request))
    }

    @SuppressLint("CheckResult")
    fun getRefuseReasons() {
        dbManager.clearReasonsTable()
        App.instance.getLogger()!!.log("getNotPaidReasons start ")
        apiManager.getNotPaidReasons()
                .flatMap { notPaidReasonResponse: NotPaidReasonResponse ->
                    if (notPaidReasonResponse.isSuccess!!) {
                        Timber.e("NOT_PAID_REASONS: ${notPaidReasonResponse.reasons}")
                        val mutable = notPaidReasonResponse.reasons
                        mutable.map { reason -> reason.orderStatusToCorrespondFor = OrderStatus.NOT_COMPLETED }
                        dbManager.insertReasons(mutable)
                        Timber.d("insert getNotPaidReasons reasons: $mutable")
                    }
                    return@flatMap apiManager.refuseReasons(OrderStatus.ACCEPTED.value)
                }
                .flatMap { refuserReasonReaponse1: RefuseReasonResponse ->
                    if (refuserReasonReaponse1.isSuccess!!) {
                        val mutable = refuserReasonReaponse1.reasons
                        mutable.map { reason -> reason.orderStatusToCorrespondFor = OrderStatus.ACCEPTED }
                        dbManager.insertReasons(mutable)
                        Timber.d("insert reasons: $mutable")
                    }
                    return@flatMap apiManager.refuseReasons(OrderStatus.ARRIVED_AT_PLACE.value)
                }
                .flatMap { refuserReasonReaponse2: RefuseReasonResponse ->
                    if (refuserReasonReaponse2.isSuccess!!) {
                        val mutable = refuserReasonReaponse2.reasons
                        mutable.map { reason -> reason.orderStatusToCorrespondFor = OrderStatus.ARRIVED_AT_PLACE }
                        dbManager.insertReasons(mutable)
                        Timber.d("insert reasons: $mutable")
                    }
                    return@flatMap apiManager.refuseReasons(OrderStatus.ON_THE_WAY.value)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    App.instance.getLogger()!!.log("getNotPaidReasons suc ")
                    if (response.isSuccess!!) {
                        val mutable = response.reasons
                        mutable.map { reason -> reason.orderStatusToCorrespondFor = OrderStatus.ON_THE_WAY }
                        dbManager.insertReasons(mutable)
                        Timber.d("insert reasons: $mutable")
                    }
                }, {
                    App.instance.getLogger()!!.log("getNotPaidReasons error ")
                    Timber.e(it)
                })

    }

    private fun showSecondaryOrderButton() {
        IS_SECONDARY_ORDER_EXIST = true
        RxBus.publish(ShowSecondaryOrderButtonEvent(true))
    }

    fun exitOrder(id: Int) {
        App.instance.getLogger()!!.log("skipOrder start ")
        repository.skipOrder(id, SkipOrderRequest(System.currentTimeMillis())).subscribe({
            App.instance.getLogger()!!.log("skipOrder suc ")
        }, {
            App.instance.getLogger()!!.log("skipOrder error ")
        })
        onBackPressed()
    }

    fun playSound() {
        if (isSoundEnabled) {
            Timber.d("hubMediaPlayer.playSound(R.raw.new_order_sound)")
            appMediaPlayer.playSound(R.raw.new_order_sound, true)
        }
    }

    fun stopSound() {
        appMediaPlayer.stopSound()
    }
}