package com.wezom.taxi.driver.data.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by zorin.a on 12.12.2018.
 */
class Photos constructor(
        @SerializedName("userPhoto") val userPhoto: Photo?,
        @SerializedName("carPhoto") val carPhoto: Photo?,
        @SerializedName("documents") var documents: List<Document>?

) : Parcelable {
    constructor(source: Parcel) : this(
            source.readParcelable<Photo>(Photo::class.java.classLoader),
            source.readParcelable<Photo>(Photo::class.java.classLoader),
            source.createTypedArrayList(Document.CREATOR)
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeParcelable(userPhoto, 0)
        writeParcelable(carPhoto, 0)
        writeTypedList(documents)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Photos> = object : Parcelable.Creator<Photos> {
            override fun createFromParcel(source: Parcel): Photos = Photos(source)
            override fun newArray(size: Int): Array<Photos?> = arrayOfNulls(size)
        }
    }
}