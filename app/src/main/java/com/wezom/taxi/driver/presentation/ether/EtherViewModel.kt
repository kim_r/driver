package com.wezom.taxi.driver.presentation.ether

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.common.*
import com.wezom.taxi.driver.data.models.NewOrderInfo
import com.wezom.taxi.driver.data.models.ResponseState
import com.wezom.taxi.driver.net.api.ApiManager
import com.wezom.taxi.driver.net.request.CreateFilterRequest
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.presentation.main.events.FilterCountEvent
import timber.log.Timber
import javax.inject.Inject

/**
 *Created by Zorin.A on 18.June.2019.
 */
class EtherViewModel @Inject constructor(routerManager: ScreenRouterManager,
                                         private val orderManager: OrderManager,
                                         private val locationStorage: LocationStorage,
                                         private val apiManager: ApiManager) : BaseViewModel(routerManager) {
    val etherLiveData = MutableLiveData<List<NewOrderInfo>>()

    fun performMapClick() {
        if (orderManager.isPrimaryOrderExist()) {
            setRootScreen(EXECUTE_ORDER_SCREEN,
                    orderManager.getPrimaryOrder())
        } else {
            //just continue on road
            setRootScreen(MAIN_MAP_SCREEN)
        }
    }

    @SuppressLint("CheckResult")
    fun getFilter() {
        App.instance.getLogger()!!.log("getFilter start")
        apiManager.getFilter().subscribe({
            App.instance.getLogger()!!.log("getFilter suc")
            if (it.isSuccess!!) {
                var count: Int = 0;
                it.result?.let {
                    for (item in it) {
                        if (item.status!!) {
                            count++
                        }
                        item.fromLatitude?.let {
                            item.fromLatitude = locationStorage.getCachedLocation().latitude.toString()
                            item.fromLongitude = locationStorage.getCachedLocation().longitude.toString()
                            val filter = CreateFilterRequest(item.name,
                                    item.address, item.fromLatitude?.toDouble(),
                                    item.fromLongitude?.toDouble(), item.fromRadius,
                                    item.toLatitude?.toDouble(), item.toLongitude?.toDouble(), item.toRadius)
                            item.id?.let { it1 -> editFilter(it1, filter) }
                        }
                    }
                    RxBus.publish(FilterCountEvent(count))
                }
            }
        }, { it ->
            App.instance.getLogger()!!.log("getFilter error")
            Timber.e(it.message) })
    }

    @SuppressLint("CheckResult")
    fun editFilter(filterId: Int, filter: CreateFilterRequest) {
        loadingLiveData.postValue(ResponseState(ResponseState.State.LOADING))
        App.instance.getLogger()!!.log("editFilter start")
        apiManager.editFilter(filterId, filter).subscribe({
            App.instance.getLogger()!!.log("editFilter suc")
            if (it.isSuccess!!) {
                handleResponseState(it)
            } else {
                Timber.e(it.error!!.message)
            }
        }, { it ->
            App.instance.getLogger()!!.log("editFilter error")
            loadingLiveData.postValue(ResponseState(ResponseState.State.NETWORK_ERROR)) })
    }

    @SuppressLint("CheckResult")
    fun getEther() {
        loadingLiveData.postValue(ResponseState(ResponseState.State.LOADING))
        App.instance.getLogger()!!.log("getEther start ")
        apiManager.getEther()
                .subscribe({
                    App.instance.getLogger()!!.log("getEther suc ")
                    handleResponseState(it)
                    if (it.isSuccess!!) {
                        etherLiveData.value = it.orders
                    }
                },
                        {
                            App.instance.getLogger()!!.log("getEther error ")
                            loadingLiveData.postValue(ResponseState(ResponseState.State.NETWORK_ERROR))
                        })
    }

    fun enterAcceptOrderScreen(order: NewOrderInfo) {
        switchScreen(ACCEPT_ORDER_SCREEN,
                order)
    }

    fun showFilterScreen() {
        switchScreen(FILTER_SCREEN)
    }
}