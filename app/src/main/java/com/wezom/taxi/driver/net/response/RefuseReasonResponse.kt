package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.Reason

/**
 * Created by zorin.a on 022 22.02.18.
 */
class RefuseReasonResponse(@SerializedName("reasons") val reasons: List<Reason>) : BaseResponse()


