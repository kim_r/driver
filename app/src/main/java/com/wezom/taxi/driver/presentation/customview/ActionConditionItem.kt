package com.wezom.taxi.driver.presentation.customview

import android.annotation.SuppressLint
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.data.models.ActionCondition
import com.wezom.taxi.driver.databinding.BonusProgramConditionBinding
import com.wezom.taxi.driver.presentation.base.lists.ItemModel

/**
 * Created by zorin.a on 3/11/19.
 */
class ActionConditionItem : ConstraintLayout,
                            ItemModel<Triple<ActionCondition, String, String>> {
    constructor(context: Context) : super(context)
    constructor(context: Context,
                attrs: AttributeSet?) : super(context,
                                              attrs)

    constructor(context: Context,
                attrs: AttributeSet?,
                attributeSetId: Int) : super(context,
                                             attrs,
                                             attributeSetId)

    var binding: BonusProgramConditionBinding =
            BonusProgramConditionBinding.inflate(LayoutInflater.from(context),
                                                 this,
                                                 true)

    @SuppressLint("SetTextI18n")
    override fun setData(data: Triple<ActionCondition, String, String>) {
        binding.run {
            conditionTitle.text = data.second
            completeonPercent.text = data.third
            statusIcon.setImageResource(if (data.first.isPassed!!) {
                R.drawable.ic_verified_bonus_condition
            } else {
                R.drawable.ic_rejected_bonus_condition
            })
        }
    }
}