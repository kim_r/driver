package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName


/**
 * Created by zorin.a on 01.08.2018.
 */

data class UserBalanceResponse (
        @SerializedName("photo") var photo: String?,
        @SerializedName("balance") val balance: Double?,
        @SerializedName("balanceId") val balanceId: Int?,
        @SerializedName("rating") val rating: Double?) : BaseResponse()