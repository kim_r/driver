package com.wezom.taxi.driver.injection.qualifiers

import javax.inject.Qualifier

/**
 * Created by zorin.a on 020 20.02.18.
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class SocketOkHttpClient