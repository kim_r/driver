package com.wezom.taxi.driver.presentation.customview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.wezom.taxi.driver.databinding.LinearProgressBinding
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.concurrent.TimeUnit

/**
 * Created by zorin.a on 12.03.2018.
 */
class LinearProgressBar : FrameLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, attributeSetId: Int) : super(context, attrs, attributeSetId)

    private val disposable = CompositeDisposable()
    private val binding = LinearProgressBinding.inflate(LayoutInflater.from(context), this, true)
    var listener: DoneListener? = null

    init {
        binding.progressbar.progress = 0
    }

    fun startTimer(amount: Long) {
        val inSeconds: Long = TimeUnit.MILLISECONDS.toSeconds(amount)

        binding.run {
            progressbar.progress = 0
            progressbar.max = amount.toInt()
            Timber.d("setProgress max: ${inSeconds.toInt()}")

            disposable += Observable.interval(100, TimeUnit.MILLISECONDS)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .takeUntil {
                        it * 100 >= amount
                    }
                    .subscribe { it ->
                        Timber.d("$it")
                        binding.progressbar.progress = it.toInt() * 100
                        if (it * 100 >= amount) {
                            listener?.onComplete()
                            stopTimer()
                        }
                    }
        }
    }

    fun stopTimer() {
        disposable.dispose()
    }

    fun setProgress(value: Int) {
        binding.progressbar.progress = value
    }

    fun setMax(value: Int) {
        binding.progressbar.max = value
    }

    override fun onDetachedFromWindow() {
        disposable.dispose()
        super.onDetachedFromWindow()
    }

    interface DoneListener {
        fun onComplete()
    }

}