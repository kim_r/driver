package com.wezom.taxi.driver.net.request

import com.google.gson.annotations.SerializedName


/**
 * Created by zorin.a on 021 21.02.18.
 */

data class ChangeCardRequest(
        @SerializedName("number") val number: Long?,
        @SerializedName("expMonth") val expMonth: Int?,
        @SerializedName("expYear") val expYear: Int?,
        @SerializedName("cardCvv") val cardCvv: Int?,
        @SerializedName("name") val name: String?
)