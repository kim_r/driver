package com.wezom.taxi.driver.net.request

import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.Car
import com.wezom.taxi.driver.data.models.User


/**
 * Created by zorin.a on 021 21.02.18.
 */

data class UserRegistrationRequest(@SerializedName("user") val user: User?,
                                   @SerializedName("car") val car: Car?)
