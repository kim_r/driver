package com.wezom.taxi.driver.data.models

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.net.websocket.models.Formula


/**
 * Created by zorin.a on 19.04.2018.
 */

@Entity(tableName = "order_info")
data class NewOrderInfo(
        @PrimaryKey
        @SerializedName("id") var id: Int,
        @SerializedName("isFiltered") var filter: Boolean,
        @SerializedName("startTimeWaitPassager") var startTimeWaitPassager: Long,
        @Embedded(prefix = "order_") @SerializedName("order") var order: Order?,
        @Embedded(prefix = "formula_") @SerializedName("formula") var formula: Formula?) : Parcelable {


    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readByte() != 0.toByte(),
            parcel.readLong(),
            parcel.readParcelable(Order::class.java.classLoader),
            parcel.readParcelable(Formula::class.java.classLoader)) {
    }

    override fun toString(): String {
        return "NewOrderInfo{id=$id, order=$order, formula=$formula}"
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeByte(if (filter) 1 else 0)
        parcel.writeLong(startTimeWaitPassager)
        parcel.writeParcelable(order, flags)
        parcel.writeParcelable(formula, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<NewOrderInfo> {
        override fun createFromParcel(parcel: Parcel): NewOrderInfo {
            return NewOrderInfo(parcel)
        }

        override fun newArray(size: Int): Array<NewOrderInfo?> {
            return arrayOfNulls(size)
        }
    }


}

