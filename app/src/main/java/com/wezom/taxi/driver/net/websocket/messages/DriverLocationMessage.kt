package com.wezom.taxi.driver.net.websocket.messages

import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.Coordinate
import com.wezom.taxi.driver.injection.annotation.SkipSerialization

/**
 * Created by zorin.a on 21.05.2018.
 */
class DriverLocationMessage constructor(@SkipSerialization override var room: String,
                                        @SerializedName("address") val coordinate: Coordinate) : BaseSocketMessage()