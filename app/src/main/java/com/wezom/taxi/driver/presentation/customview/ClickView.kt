package com.wezom.taxi.driver.presentation.customview

import android.content.Context
import android.util.AttributeSet
import android.view.View
import kotlin.properties.Delegates

/**
 *Created by Zorin.A on 26.July.2019.
 */
class ClickView @JvmOverloads constructor(context: Context,
                                          attrs: AttributeSet? = null,
                                          defStyleAttr: Int = 0) : View(context,
                                                                        attrs,
                                                                        defStyleAttr) {
    private var clickCount by Delegates.observable(0) { _, _, newValue ->
//        if (newValue == 3) Toast.makeText(context,
//                                          hexToAscii("646576656c6f7065723a205a6f72696e2e4120616e642048616e7a"),
//                                          Toast.LENGTH_LONG).show()
    }

    private fun hexToAscii(hexStr: String): String {
        val output = StringBuilder("")
        var i = 0
        while (i < hexStr.length) {
            val str = hexStr.substring(i,
                                       i + 2)
            output.append(Integer.parseInt(str,
                                           16).toChar())
            i += 2
        }
        return output.toString()
    }

    init {
        setOnClickListener {
            clickCount += 1
        }
    }
}