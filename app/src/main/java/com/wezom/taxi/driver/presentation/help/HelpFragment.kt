package com.wezom.taxi.driver.presentation.help

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView
import com.wezom.taxi.driver.BuildConfig
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.common.Constants.TAXI_URL_FAG
import com.wezom.taxi.driver.common.Constants.TAXI_URL_CONTRACT_OFFER
import com.wezom.taxi.driver.common.Constants.TAXI_URL_PRIVATE_POLICY
import com.wezom.taxi.driver.common.Constants.TAXI_URL_USER_AGREEMENT
import com.wezom.taxi.driver.common.callSupport
import com.wezom.taxi.driver.databinding.HelpBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.presentation.base.BaseFragment
import io.reactivex.rxkotlin.plusAssign

/**
 * Created by udovik.s on 23.03.2018.
 */
class HelpFragment : BaseFragment() {
    //region var
    private lateinit var binding: HelpBinding
    private lateinit var viewModel: HelpViewModel

    private val helpEmailAndPhoneObserver = Observer<Pair<String, String>> { response ->
        callSupport(response!!.first, context!!, response.second)
    }
    //endregion

    //region verride
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setBackPressFun { viewModel.onBackPressed() }
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
        viewModel.loadingLiveData.observe(this, progressObserver)
        viewModel.helpEmailPhoneLiveData.observe(this, helpEmailAndPhoneObserver)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = HelpBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            toolbar.setToolbarTitle(getString(R.string.help))

            disposable += RxView.clicks(tvPrivatePolicy).subscribe({
                switchToSite(TAXI_URL_PRIVATE_POLICY)
            })

            disposable += RxView.clicks(tvUserAgreement).subscribe({
                switchToSite(TAXI_URL_USER_AGREEMENT)
            })

            disposable += RxView.clicks(tvContractOffer).subscribe({
                switchToSite(TAXI_URL_CONTRACT_OFFER)
            })

            disposable += RxView.clicks(tvMoreInfoOnSite).subscribe({
                switchToSite(TAXI_URL_FAG)
            })

            disposable += RxView.clicks(btnCallSupport).subscribe({
                viewModel.getHelpEmailAndPhone()
            })
            tvVersion.text = "${getString(R.string.version)} ${BuildConfig.VERSION_NAME}"
        }
    }
    //endregion

    //region fun


    private fun switchToSite(url: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        context!!.startActivity(intent)
    }
    //endregion
}