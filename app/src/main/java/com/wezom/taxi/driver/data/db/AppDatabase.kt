package com.wezom.taxi.driver.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.wezom.taxi.driver.data.models.*

@Database(entities = [
    NewOrderInfo::class,
    OrderCoordinates::class,
    Reason::class,
    OrderResultData::class,
    OrderFinalization::class],
        version = 30, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun orderInfoDao(): OrderInfoDao

    abstract fun orderCoordinatesDao(): OrderCoordinatesDao

    abstract fun reasonsDao(): ReasonsDao

    abstract fun orderResultDataDao(): OrderResultDataDao

    abstract fun orderFinalizationDao(): OrderFinalizationDao
}