package com.wezom.taxi.driver.net.jobqueue.jobs

import android.annotation.SuppressLint
import com.wezom.taxi.driver.net.request.SendHelpTimeRequest
import com.wezom.taxi.driver.presentation.app.App

/**
 * Created by zorin.a on 05.07.2018.
 */
class SendHelpTimeJob(val orderId: Int, val request: SendHelpTimeRequest) : BaseJob() {
    @SuppressLint("CheckResult")
    override fun onRun() {
        App.instance.getLogger()!!.log("sendHelpTime start and end")
        apiManager.sendHelpTime(orderId, request).blockingGet()
    }
}