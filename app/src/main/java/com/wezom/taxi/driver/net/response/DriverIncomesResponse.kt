package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.Day


/**
 * Created by zorin.a on 22.03.2018.
 */

data class DriverIncomesResponse(
        @SerializedName("rangeCompletedTrips") val rangeCompletedTrips: Int?,
        @SerializedName("rangeOnlineTime") val rangeOnlineTime: Long?,
        @SerializedName("rangeDistance") val rangeDistance: Double?,
        @SerializedName("rangeTotalIncome") val rangeTotalIncome: Int?,
        @SerializedName("rangeCashIncome") val rangeCashIncome: Int?,
        @SerializedName("rangeNonCashIncome") val rangeNonCashIncome: Int?,
        @SerializedName("rangeSurcharge") val rangeSurcharge: Double?,
        @SerializedName("days") val days: List<Day>?
) : BaseResponse()