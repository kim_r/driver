package com.wezom.taxi.driver.net.api

import android.content.SharedPreferences
import com.wezom.taxi.driver.BuildConfig
import com.wezom.taxi.driver.common.applySingleSchedulers
import com.wezom.taxi.driver.data.models.*
import com.wezom.taxi.driver.ext.LANGUAGE
import com.wezom.taxi.driver.ext.string
import com.wezom.taxi.driver.net.request.*
import com.wezom.taxi.driver.net.response.*
import io.reactivex.Single
import okhttp3.MultipartBody
import javax.inject.Inject

/**
 * Created by zorin.a on 020 20.02.18.
 */
class ApiManager @Inject constructor(private val apiService: ApiService,
                                     sharedPreferences: SharedPreferences) {
    val lang by sharedPreferences.string(LANGUAGE)

    fun sendPhone(sendPhoneRequest: SendPhoneRequest): Single<BaseResponse> =
            apiService.sendPhone(sendPhoneRequest).compose(applySingleSchedulers())

    fun confirmCode(confirmCodeRequest: ConfirmCodeRequest): Single<ConfirmCodeResponse> =
            apiService.confirmCode(confirmCodeRequest).compose(applySingleSchedulers())

    fun carBands(): Single<CarBrandsResponse> =
            apiService.carBands().compose(applySingleSchedulers())

    fun carModels(brandId: Int): Single<CarModelsResponse> =
            apiService.carModels(brandId).compose(applySingleSchedulers())

    fun carColors(): Single<CarColorsResponse> =
            apiService.carColors().compose(applySingleSchedulers())

    fun carIssueYears(modelId: Int): Single<CarIssueYearsResponse> =
            apiService.carIssueYears(modelId).compose(applySingleSchedulers())

    fun existedProfileFields(): Single<ExistedProfileResponse> =
            apiService.existedProfileFields().compose(applySingleSchedulers())

    fun userRegistration(request: RegistrationRequest): Single<BaseResponse> =
            apiService.userRegistration(request).compose(applySingleSchedulers())

    fun registrationDocument(userImageBody: MultipartBody.Part?,
                             carImageBody: MultipartBody.Part?,
                             doc1ImageBody: MultipartBody.Part?,
                             doc2ImageBody: MultipartBody.Part?,
                             doc3ImageBody: MultipartBody.Part?,
                             doc4ImageBody: MultipartBody.Part?): Single<BaseResponse> =
            apiService.registrationDocuments(userImageBody,
                    carImageBody,
                    doc1ImageBody,
                    doc2ImageBody,
                    doc3ImageBody,
                    doc4ImageBody).compose(applySingleSchedulers())

    fun requiredDocsList(): Single<RequiredDocsListResponse> =
            apiService.requiredDocsList().compose(applySingleSchedulers())

    fun profileAndRegistrationCorrectness(): Single<ProfileAndRegistrationCorrectnessResponse> =
            apiService.profileAndRegistrationCorrectness().compose(applySingleSchedulers())

    fun checkUserStatus(): Single<UserStatusResponse> =
            apiService.checkUserStatus(BuildConfig.VERSION_CODE.toString()).compose(applySingleSchedulers())

    fun changeCard(request: ChangeCardRequest): Single<BaseResponse> =
            apiService.changeCard(request).compose(applySingleSchedulers())

    fun addNewCard(request: ChangeCardRequest): Single<BaseResponse> =
            apiService.addNewCard(request).compose(applySingleSchedulers())

    fun acceptOrder(id: Int,
                    request: AcceptOrderRequest): Single<BaseResponse> = apiService.acceptOrder(id,
            request).compose(applySingleSchedulers())

    fun skipOrder(id: Int,
                  request: SkipOrderRequest): Single<BaseResponse> = apiService.skipOrder(id,
            request).compose(applySingleSchedulers())

    fun currentOrders(): Single<CurrentOrdersResponse> =
            apiService.currentOrders().compose(applySingleSchedulers())


    fun currentOrder(id:Int): Single<NewOrderInfo> =
            apiService.currentOrder(id).compose(applySingleSchedulers())

    fun refuseOrder(id: Int,
                    request: RefuseOrderRequest): Single<BaseResponse> = apiService.refuseOrder(id,
            request).compose(applySingleSchedulers())

    fun refuseReasons(stageId: Int): Single<RefuseReasonResponse> =
            apiService.refuseReasons(stageId).compose(applySingleSchedulers())

    fun getNotPaidReasons(): Single<NotPaidReasonResponse> =
            apiService.getNotPaidReasons().compose(applySingleSchedulers())

    fun customerDidNotPay(id: Int,
                          body: NotPaidRequest): Single<BaseResponse> =
            apiService.customerDidNotPay(id,
                    body).compose(applySingleSchedulers())

    fun arrivedAtPlace(id: Int,
                       request: DriverEventRequest): Single<BaseResponse> =
            apiService.arrivedAtPlace(id,
                    request).compose(applySingleSchedulers())

    fun driverOnTheWay(id: Int,
                       request: DriverEventRequest): Single<BaseResponse> =
            apiService.driverOnTheWay(id,
                    request).compose(applySingleSchedulers())

    fun getCompletedOrders(linit: Int?,
                           offset: Int?): Single<CompletedTripsResponse> =
            apiService.getCompletedOrders(linit,
                    offset).compose(applySingleSchedulers())

    fun getCompletedOrder(orderId: Int): Single<CompletedTripResponse> =
            apiService.getCompletedOrder(orderId).compose(applySingleSchedulers())

    fun getPersonalStatistics(): Single<PersonalStatisticsResponse> =
            apiService.getPersonalStatistics().compose(applySingleSchedulers())

    fun getDiscounts(): Single<DiscountsResponse> =
            apiService.getDiscounts().compose(applySingleSchedulers())

    fun getIncomes(startDate: Long,
                   endDate: Long): Single<DriverIncomesResponse> = apiService.getIncomes(startDate,
            endDate).compose(applySingleSchedulers())

    fun getReceivedBalances(startTime: Long,
                            finishTime: Long): Single<BalanceResponse> =
            apiService.getReceivedBalances(startTime,
                    finishTime).compose(applySingleSchedulers())

    fun getDebitedBalances(startTime: Long,
                           finishTime: Long): Single<BalanceResponse> =
            apiService.getDebitedBalances(startTime,
                    finishTime).compose(applySingleSchedulers())

    fun rateTrip(id: Int,
                 body: RateRequest): Single<BaseResponse> = apiService.rateTrip(id,
            body).compose(applySingleSchedulers())

    fun changePhone(phone: String): Single<BaseResponse> =
            apiService.changePhone(Phone(phone)).compose(applySingleSchedulers())

    fun changePhoneVerification(phone: String,
                                code: Int): Single<BaseResponse> =
            apiService.changePhoneVerification(ChangePhone(phone,
                    code)).compose(applySingleSchedulers())

    fun getHelpEmail(): Single<HelpEmailResponse> =
            apiService.getHelpEmail().compose(applySingleSchedulers())

    fun getCardVerificationInfo(): Single<CardVerificationInfoResponse> =
            apiService.getCardVerificationInfo().compose(applySingleSchedulers())

    fun getMapRoute(origin: String,
                    destination: String,
                    waypoints: String? = null,
                    mode: String? = "DRIVING"): Single<RouteResponse> =
            apiService.getMapRoute(origin,
                    destination,
                    waypoints,
                    mode).compose(applySingleSchedulers())


    fun getMapRouteOCM(body: CoordinatesOSM): Single<RouteResponseOCM> =
            apiService.getMapRouteOCM(body).compose(applySingleSchedulers())

    fun logout(token: String): Single<BaseResponse> =
            apiService.logout(token).compose(applySingleSchedulers())

//    fun finishOrder(orderId: Int, request: FinishOrderRequest): Single<BaseResponse> = apiService.finishOrder(orderId, request).compose(applySingleSchedulers())

    fun setUserOnlineStatus(online: SetOnlineStatusRequest): Single<UserOnlineStatusResponse> =
            apiService.setUserOnlineStatus(online).compose(applySingleSchedulers())

    fun totalCost(id: Int,
                  request: TotalCostRequest): Single<BaseResponse> = apiService.totalCost(id,
            request).compose(applySingleSchedulers())

    fun sendDeviceKey(request: SendDeviceKeyRequest): Single<BaseResponse> =
            apiService.sendDeviceKey(request).compose(applySingleSchedulers())

    fun dataSynchronization(orderId: Int,
                            request: TotalCostRequest): Single<BaseResponse> =
            apiService.dataSynchronization(orderId,
                    request).compose(applySingleSchedulers())

    fun getRequiredDocuments(): Single<RequiredDocumentsResponse> =
            apiService.getRequiredDocuments().compose(applySingleSchedulers())

    fun sendHelpTime(id: Int,
                     request: SendHelpTimeRequest): Single<BaseResponse> =
            apiService.sendHelpTime(id,
                    request).compose(applySingleSchedulers())

    fun checkCurrentBalance(): Single<UserBalanceResponse> =
            apiService.checkCurrentBalance().compose(applySingleSchedulers())

    fun finalizeOrder(orderId: Int,
                      request: OrderFinalization): Single<BaseResponse> =
            apiService.finalizeOrder(orderId,
                    request).compose(applySingleSchedulers())

    fun getDiscountDetails(): Single<CurrendDiscountResponse> =
            apiService.getDiscountDetails().compose(applySingleSchedulers())

    fun forceCloseOrder(orderId: Int): Single<BaseResponse> =
            apiService.forceCloseOrder(orderId).compose(applySingleSchedulers())

    fun getEther(): Single<EtherResponse> = apiService.getEther().compose(applySingleSchedulers())

    fun autocomplete(query: String,
                     lat: Double? = 0.0,
                     lon: Double? = 0.0): Single<List<AutoCompleteResult>> {
        val request = AutocompleteRequest(query,
                lang,
                lat,
                lon)
        return apiService.autocomplete(request)
                .compose(applySingleSchedulers())

    }

    fun saveFilter(filter: CreateFilterRequest):
            Single<BaseResponse> = apiService.addFilter(filter).compose(applySingleSchedulers())

    fun editFilter( filterId: Int, filter: CreateFilterRequest):
            Single<BaseResponse> = apiService.editFilter(filterId, filter).compose(applySingleSchedulers())


    fun getFilter(): Single<HeadFilterResponse> = apiService.getFilter().compose(applySingleSchedulers())

    fun deleteFilter(filterId: Int): Single<HeadFilterResponse> = apiService.deleteFilter(filterId).compose(applySingleSchedulers())

    fun getType(): Single<HeadTypeCarResponse> = apiService.getType().compose(applySingleSchedulers())


    fun deactivateFilter(filterId: Int): Single<HeadFilterResponse> = apiService.deactivateFilter(filterId).compose(applySingleSchedulers())

    fun activateFilter(filterId: Int): Single<HeadFilterResponse> = apiService.activateFilter(filterId).compose(applySingleSchedulers())

    fun setMoveCar(): Single<BaseResponse> =
            apiService.setMoveCar().compose(applySingleSchedulers())

    fun setMoveStopCar(): Single<BaseResponse> =
            apiService.setMoveStopCar().compose(applySingleSchedulers())
}