package com.wezom.taxi.driver.presentation.addcard

import android.arch.lifecycle.MutableLiveData
import com.wezom.taxi.driver.data.models.PaymentInfo
import com.wezom.taxi.driver.net.api.ApiManager
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

/**
 * Created by Andrew on 01.03.2018.
 */
class AddCardViewModel @Inject constructor(screenRouterManager: ScreenRouterManager, private val apiManager: ApiManager) : BaseViewModel(screenRouterManager) {

    val addCardParams: MutableLiveData<PaymentInfo> = MutableLiveData()

    fun requestCardVerificationInfo() {
        disposable += apiManager.getCardVerificationInfo().subscribeBy(
                onSuccess = {
                    addCardParams.postValue(it.paymentInfo)
                }
        )
    }

}