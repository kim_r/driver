package com.wezom.taxi.driver.presentation.mainmap

import android.Manifest
import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.PolygonOptions
import com.jakewharton.rxbinding2.view.RxView
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.bus.events.GpsEvent
import com.wezom.taxi.driver.bus.events.QuerrryTariffiedRegions
import com.wezom.taxi.driver.common.*
import com.wezom.taxi.driver.data.models.Coordinate
import com.wezom.taxi.driver.data.models.PaymentInfo
import com.wezom.taxi.driver.data.models.map.Area
import com.wezom.taxi.driver.databinding.MainMapFragmentBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.ext.longToast
import com.wezom.taxi.driver.ext.setVisible
import com.wezom.taxi.driver.presentation.addcard.AddCardActivity.Companion.startAddCardActivity
import com.wezom.taxi.driver.presentation.base.BaseMapFragment
import com.wezom.taxi.driver.presentation.customview.DriverToolbar
import com.wezom.taxi.driver.presentation.customview.DriverToolbar.Companion.IS_ONLINE_STATE
import com.wezom.taxi.driver.presentation.customview.EtherSwitchButton
import com.wezom.taxi.driver.presentation.customview.IntroView
import com.wezom.taxi.driver.presentation.main.MainActivity
import com.wezom.taxi.driver.presentation.main.events.*
import com.wezom.taxi.driver.presentation.main.events.SocketServiceResponseEvent.Result.NEGATIVE
import com.wezom.taxi.driver.presentation.main.events.SocketServiceResponseEvent.Result.POSITIVE
import com.wezom.taxi.driver.presentation.profile.dialog.CoefficientDialog
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber

/**
 * Created by zorin.a on 26.02.2018.
 */
class MainMapFragment : BaseMapFragment() {
    //region var
    private var isMapZoomed: Boolean = false
    private var hexagonEvent: HexagonEvent? = null
    private lateinit var binding: MainMapFragmentBinding
    private lateinit var viewModel: MainMapViewModel
    private var introView: IntroView? = null
    private val screenThemeObserver: Observer<Boolean> = Observer { isMapWhite ->
        setScreenTheme(isMapWhite)
    }

    override fun onDetach() {
        super.onDetach()

    }

    override fun onDestroyView() {
        super.onDestroyView()

    }

    //    private val introObserver: Observer<Boolean> = Observer { isIntroShown ->
//        if (!isIntroShown!!) {
//            introView = IntroView(context!!).apply { elevation = 20f }
//            introView?.listener = object : SwipeListener {
//                override fun onSwipeDone() {
//                    viewModel.onSwipeIntroView()
//                }
//            }
//            val itemParams = ConstraintLayout.LayoutParams(
//                    ConstraintLayout.LayoutParams.MATCH_PARENT,
//                    ConstraintLayout.LayoutParams.MATCH_PARENT)
//            binding.root.addView(introView, itemParams)
//        }
//    }
    private val paymentInfoObserver = Observer<PaymentInfo> {
        Timber.d("New paymentInfo: $it")
        it?.let {
            startAddCardActivity(activity!!,
                    it)
        }
    }
    //endregion

    //region override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this,
                viewModelFactory)
                .getViewModelOfType()

        setBackPressFun { viewModel.onBackPressed() }

//        viewModel.introShowedLiveData.observe(this, introObserver)
        viewModel.userBalanceLiveData.observe(this,
                Observer { balanceValue ->
                    val amount =
                            if (balanceValue?.toFloat() == 0.0f) getString(R.string.balance_mask_zero) else formatBalanceMoney(balanceValue?.toFloat()!!)
                    binding.balanceValue.text = amount
                })

        viewModel.onIntroDissmissLiveData.observe(this,
                Observer {
                    binding.root.removeView(introView)
                })
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = MainMapFragmentBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View,
                               savedInstanceState: Bundle?) {
        super.onViewCreated(view,
                savedInstanceState)
        initViews()
        viewModel.isMapWhiteThemeLiveData.observe(this,
                screenThemeObserver)
        viewModel.addCardLiveData.observe(this,
                paymentInfoObserver)
        isMapZoomed = false
        initListeners()
        viewModel.checkCurrentBalance()

        checkOverlayWindowPermissions()
    }

    override fun onMapReady(map: GoogleMap?) {
        super.onMapReady(map)
        viewModel.setMapStyle()
        googleMap?.uiSettings?.isMapToolbarEnabled = false
        drawHexagonListener()
        clearMap()

        if (hexagonEvent != null) {
            drawHexagons(hexagonEvent!!)
        } else {
            if (DriverToolbar.IS_ONLINE_STATE) {
                RxBus.publish(QuerrryTariffiedRegions())
            }
        }
    }

    override fun onActivityResult(requestCode: Int,
                                  resultCode: Int,
                                  data: Intent?) {
        if (requestCode == MainActivity.CODE_DRAW_OVER_OTHER_APP_PERMISSION) {
            if (resultCode == Activity.RESULT_OK) {
                "Overlay window permissions granted".longToast(context!!)
            } else {
                "Overlay window need permissions".longToast(context!!)
            }
        }
        super.onActivityResult(requestCode,
                resultCode,
                data)
    }

//endregion

    //region fun
    private fun initListeners() {
        binding.gradientInfo.setVisible(IS_ONLINE_STATE)
        disposable += RxBus.listen(SocketServiceResponseEvent::class.java)
                .subscribe { response ->
                    when (response.result) {
                        POSITIVE -> {
                            binding.gradientInfo.setVisible(true)
                        }
                        NEGATIVE -> {
                            binding.gradientInfo.setVisible(false)
                        }
                    }
                }

        disposable += RxBus.listen(GpsEvent::class.java)
                .subscribe {
                    if (!it.isEnabled) {
                        showGpsDialog(context!!,
                                fragmentManager!!)
                    }
                }
    }

    private fun setScreenTheme(isMapWhite: Boolean?) {
        setMapStyle(isMapWhite)
        binding.run {
            if (isMapWhite == true) {
                themeButton.setImageResource(R.drawable.ic_btn_map_light)
                coefficientButton.setImageResource(R.drawable.ic_btn_coeff_light)
                gradientInfo.background = resources.getDrawable(R.drawable.ic_gradient_light)
                searchOrderMessage.setTextColor(resources.getColor(R.color.colorDarkerGrey3))
            } else {
                themeButton.setImageResource(R.drawable.ic_btn_day_grey)
                coefficientButton.setImageResource(R.drawable.ic_btn_coefficient_grey)
                gradientInfo.background = resources.getDrawable(R.drawable.ic_gradient_dark)
                searchOrderMessage.setTextColor(resources.getColor(R.color.colorWhite2))
            }
        }
    }

    private fun initViews() {
        binding.run {
            disposable += RxView.clicks(balanceLabel)
                    .subscribe {
                        viewModel.switchToBalanceScreen()
                    }
            disposable += RxView.clicks(balanceValue)
                    .subscribe {
                        viewModel.switchToBalanceScreen()
                    }
            disposable += RxView.clicks(myLocation)
                    .subscribe {
                        if (!isPermissionGranted(this@MainMapFragment,
                                        Manifest.permission.ACCESS_FINE_LOCATION)) {
                            requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                                    LOCATION_PERMISSIONS_REQUEST)
                        } else {
                            onPermissionsGranted()
                            updateDriverLocation()
                        }
                    }
            disposable += RxView.clicks(info)
                    .subscribe {
                        showInfoDialog(R.string.balance,
                                R.string.balance_text)
                    }
            disposable += RxView.clicks(themeButton)
                    .subscribe {
                        viewModel.switchMapStyle()
                    }
            disposable += RxView.clicks(coefficientButton)
                    .subscribe {
                        val dialog = CoefficientDialog()
                        dialog.show(childFragmentManager,
                                CoefficientDialog.TAG)
                    }
            viewModel.checkIntroShown()

            toolbar.binding.run {
                etherButton.initAsList()
                etherButton.listener = object : EtherSwitchButton.OnEtherClickListener {
                    override fun onClick(state: EtherSwitchButton.State) {
                        viewModel.switchEtherScreen()
                    }
                }
            }
        }
    }

    override fun onLocationChanged(location: Location) {
        super.onLocationChanged(location)
        if (!isMapZoomed) {
            updateDriverCamera()
            isMapZoomed = true
        }
        updateDriverMarker()
    }

    private fun updateDriverLocation() {
        updateDriverMarker()
        updateDriverCamera()
    }

    private fun drawHexagonListener() {
        disposable += RxBus.listen(HexagonEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    hexagonEvent = it
                    drawHexagons(it)
                },
                        { throwable ->
                            Timber.e("ERROR HEXAGON %s",
                                    throwable)
                        })
    }

    private fun drawHexagons(it: HexagonEvent) {
        val polyOptions = mutableListOf<PolygonOptions>()
        clearMap()
        addCarMarker()
        updateDriverMarker()
        it.hexagon.area.let {
            for (areaItem: Area in it) {
                val rectOptions = PolygonOptions()
                for (coordinates: Coordinate in areaItem.coordinates) {
                    rectOptions.add(LatLng(coordinates.latitude!!,
                            coordinates.longitude!!))
                }
                areaItem.coefficient.let {
                    when {
                        it <= 1.3 -> rectOptions.fillColor(ContextCompat.getColor(activity!!,
                                R.color.colorCoefficient1))
                        it in 1.4..1.6 -> rectOptions.fillColor(ContextCompat.getColor(activity!!,
                                R.color.colorCoefficient2))
                        it in 1.7..1.9 -> rectOptions.fillColor(ContextCompat.getColor(activity!!,
                                R.color.colorCoefficient3))
                        else -> rectOptions.fillColor(ContextCompat.getColor(activity!!,
                                R.color.colorCoefficient4))
                    }
                    addCoefficientText(context,
                            googleMap,
                            getPolygonCenterPoint(rectOptions.points),
                            it.toString(),
                            14)
                }
                rectOptions.strokeColor(ContextCompat.getColor(activity!!,
                        R.color.strokeColor))
                        .strokeWidth(4F)
                polyOptions.add(rectOptions)
                googleMap?.addPolygon(rectOptions)
            }
        }
    }

    private fun checkOverlayWindowPermissions() {
        if (!checkIsSystemOverlayEnabled(context!!) && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            showOverlayDialog(activity!!,
                    fragmentManager!!)
        }
    }

//endregion
}