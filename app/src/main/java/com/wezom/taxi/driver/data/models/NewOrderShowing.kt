package com.wezom.taxi.driver.data.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by zorin.a on 28.04.2018.
 */

data class NewOrderShowing(@SerializedName("isKilometerPriceShow") var isKilometerPriceShow: Boolean?,
                           @SerializedName("isCalculatedPathShow") var isCalculatedPathShow: Boolean?,
                           @SerializedName("isDistanceShow") var isDistanceShow: Boolean?,
                           @SerializedName("isRatingShow") var isRatingShow: Boolean?,
                           @SerializedName("isCurrentCoefficientShow") var isCurrentCoefficientShow: Boolean?,
                           @SerializedName("isFinishAddressShow") var isFinishAddressShow: Boolean?,
                           @SerializedName("isServicesShow") var isServicesShow: Boolean?,
                           @SerializedName("isMethodShow") var isMethodShow: Boolean?,
                           @SerializedName("isEstimatedCostShow") var isEstimatedCostShow: Boolean?,
                           @SerializedName("isCommentShow") var isCommentShow: Boolean?,
                           @SerializedName("isSurchargeShow") var isSurchargeShow: Boolean?) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean)

    constructor() : this(false, false, false, false, false, false, false, false, false, false, false)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(isKilometerPriceShow)
        parcel.writeValue(isCalculatedPathShow)
        parcel.writeValue(isDistanceShow)
        parcel.writeValue(isRatingShow)
        parcel.writeValue(isCurrentCoefficientShow)
        parcel.writeValue(isFinishAddressShow)
        parcel.writeValue(isServicesShow)
        parcel.writeValue(isMethodShow)
        parcel.writeValue(isEstimatedCostShow)
        parcel.writeValue(isCommentShow)
        parcel.writeValue(isSurchargeShow)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<NewOrderShowing> {
        override fun createFromParcel(parcel: Parcel): NewOrderShowing {
            return NewOrderShowing(parcel)
        }

        override fun newArray(size: Int): Array<NewOrderShowing?> {
            return arrayOfNulls(size)
        }
    }

}