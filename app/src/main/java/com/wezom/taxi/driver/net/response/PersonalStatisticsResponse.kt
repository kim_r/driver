package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName
import com.wezom.taxi.driver.data.models.Driver


/**
 * Created by zorin.a on 14.03.2018.
 */
class PersonalStatisticsResponse(
        @SerializedName("driver") val driver: Driver ) : BaseResponse()