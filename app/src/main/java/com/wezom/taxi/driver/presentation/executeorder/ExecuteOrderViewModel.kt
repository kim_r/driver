package com.wezom.taxi.driver.presentation.executeorder

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.location.Location
import android.net.Uri
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.bus.events.*
import com.wezom.taxi.driver.common.*
import com.wezom.taxi.driver.data.db.DbManager
import com.wezom.taxi.driver.data.models.*
import com.wezom.taxi.driver.data.models.OrderStatus.ACCEPTED
import com.wezom.taxi.driver.data.models.OrderStatus.ARRIVED_AT_PLACE
import com.wezom.taxi.driver.data.models.OrderStatus.ON_THE_WAY
import com.wezom.taxi.driver.data.models.OrderStatus.TOTAL_COST
import com.wezom.taxi.driver.ext.*
import com.wezom.taxi.driver.net.api.ApiManager
import com.wezom.taxi.driver.net.jobqueue.JobFactory
import com.wezom.taxi.driver.net.jobqueue.jobs.*
import com.wezom.taxi.driver.net.request.*
import com.wezom.taxi.driver.net.response.NotPaidReasonResponse
import com.wezom.taxi.driver.net.response.RefuseReasonResponse
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.presentation.main.events.ShowSecondaryOrderButtonEvent
import com.wezom.taxi.driver.repository.DriverRepository
import com.wezom.taxi.driver.service.TaxometerService
import com.wezom.taxi.driver.taxometer.Taxometer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import ru.terrakok.cicerone.result.ResultListener
import timber.log.Timber
import java.lang.ref.WeakReference
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.collections.HashMap

class ExecuteOrderViewModel @Inject constructor(
        private val apiManager: ApiManager,
        private val orderManager: OrderManager,
        private val dbManager: DbManager,
        private val locationStorage: LocationStorage,
        private val repository: DriverRepository,
        screenRouterManager: ScreenRouterManager,
        sharedPreferences: SharedPreferences) : BaseViewModel(screenRouterManager) {

    private var passedDistanceFull: Double = 0.0
    private var passedDistanceInsideCity: Double = 0.0
    private var passedDistanceOutsideCity: Double = 0.0
    private var currentLocation: Location? = null
    private var taxometer: Taxometer? = null

    var timerEnterClient: Int by sharedPreferences.int(TIMER_NOT_ENTER_CLIENT, 0)

    private var navigatorType: Int by sharedPreferences.int(key = NAVIGATOR, default = Constants.DEFAULT_NAV)
    private var lastStepTime: Long by sharedPreferences.long(key = LAST_ORDER_STEP_TIME, default = Constants.STEP_TIME_UNSPECIFIED)

    private var resultData: ResultDataEvent? = null

    var timerLiveData = MutableLiveData<Long>()
    var realTimerLiveData = MutableLiveData<Long>()

    fun isStepTimeUnspecified(): Boolean {
        return lastStepTime == Constants.STEP_TIME_UNSPECIFIED
    }

    fun saveOrderStepTime(time: Long = System.currentTimeMillis()) {
        lastStepTime = time
    }

    fun isNextOrderStepAllowed(delayInSeconds: Long): Boolean {
        return (System.currentTimeMillis() - lastStepTime) > TimeUnit.SECONDS.toMillis(delayInSeconds)
    }


    @SuppressLint("CheckResult")
    fun getRefuseReasons() {
        dbManager.clearReasonsTable()
        App.instance.getLogger()!!.log("getNotPaidReasons start ")
        apiManager.getNotPaidReasons()
                .flatMap { notPaidReasonResponse: NotPaidReasonResponse ->
                    if (notPaidReasonResponse.isSuccess!!) {
                        Timber.e("NOT_PAID_REASONS: ${notPaidReasonResponse.reasons}")
                        val mutable = notPaidReasonResponse.reasons
                        mutable.map { reason -> reason.orderStatusToCorrespondFor = OrderStatus.NOT_COMPLETED }
                        dbManager.insertReasons(mutable)
                        Timber.d("insert getNotPaidReasons reasons: $mutable")
                    }
                    return@flatMap apiManager.refuseReasons(OrderStatus.ACCEPTED.value)
                }
                .flatMap { refuserReasonReaponse1: RefuseReasonResponse ->
                    if (refuserReasonReaponse1.isSuccess!!) {
                        val mutable = refuserReasonReaponse1.reasons
                        mutable.map { reason -> reason.orderStatusToCorrespondFor = OrderStatus.ACCEPTED }
                        dbManager.insertReasons(mutable)
                        Timber.d("insert reasons: $mutable")
                    }
                    return@flatMap apiManager.refuseReasons(OrderStatus.ARRIVED_AT_PLACE.value)
                }
                .flatMap { refuserReasonReaponse2: RefuseReasonResponse ->
                    if (refuserReasonReaponse2.isSuccess!!) {
                        val mutable = refuserReasonReaponse2.reasons
                        mutable.map { reason -> reason.orderStatusToCorrespondFor = OrderStatus.ARRIVED_AT_PLACE }
                        dbManager.insertReasons(mutable)
                        Timber.d("insert reasons: $mutable")
                    }
                    return@flatMap apiManager.refuseReasons(OrderStatus.ON_THE_WAY.value)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    App.instance.getLogger()!!.log("getNotPaidReasons suc ")
                    if (response.isSuccess!!) {
                        val mutable = response.reasons
                        mutable.map { reason -> reason.orderStatusToCorrespondFor = OrderStatus.ON_THE_WAY }
                        dbManager.insertReasons(mutable)
                        Timber.d("insert reasons: $mutable")
                    }
                }, {
                    App.instance.getLogger()!!.log("getNotPaidReasons error ")
                    Timber.e(it)
                })

    }

    fun getReason(): List<Reason> {
        return dbManager.queryAllReasons()
    }

    @SuppressLint("CheckResult")
    fun cancelOrder(order: Order) {
        val allReasons = HashMap<OrderStatus, List<Reason>>()
        val reasons = dbManager.queryReasonByStatus(order.status!!).distinctBy { item -> item.id }
        allReasons[order.status!!] = reasons
        Timber.d("reasons size = ${reasons.size}")

        resultData = dbManager.gueryOrderResult(order.id!!)?.resultData

        dbManager.clearReasonsTable()

//        if (reasons.isEmpty()) {
        App.instance.getLogger()!!.log("getNotPaidReasons start ")
        apiManager.getNotPaidReasons()
                .flatMap { notPaidReasonResponse: NotPaidReasonResponse ->
                    if (notPaidReasonResponse.isSuccess!!) {
                        Timber.e("NOT_PAID_REASONS: ${notPaidReasonResponse.reasons}")
                        val mutable = notPaidReasonResponse.reasons
                        mutable.map { reason -> reason.orderStatusToCorrespondFor = OrderStatus.NOT_COMPLETED }
                        dbManager.insertReasons(mutable)
                        Timber.d("insert getNotPaidReasons reasons: ${mutable}")
                    }
                    return@flatMap apiManager.refuseReasons(OrderStatus.ACCEPTED.value)
                }
                .flatMap { refuserReasonReaponse1: RefuseReasonResponse ->
                    if (refuserReasonReaponse1.isSuccess!!) {
                        val mutable = refuserReasonReaponse1.reasons
                        mutable.map { reason -> reason.orderStatusToCorrespondFor = OrderStatus.ACCEPTED }
                        dbManager.insertReasons(mutable)
                        Timber.d("insert reasons: ${mutable}")
                    }
                    return@flatMap apiManager.refuseReasons(OrderStatus.ARRIVED_AT_PLACE.value)
                }
                .flatMap { refuserReasonReaponse2: RefuseReasonResponse ->
                    if (refuserReasonReaponse2.isSuccess!!) {
                        val mutable = refuserReasonReaponse2.reasons
                        mutable.map { reason -> reason.orderStatusToCorrespondFor = OrderStatus.ARRIVED_AT_PLACE }
                        dbManager.insertReasons(mutable)
                        Timber.d("insert reasons: ${mutable}")
                    }
                    return@flatMap apiManager.refuseReasons(OrderStatus.ON_THE_WAY.value)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    App.instance.getLogger()!!.log("getNotPaidReasons suc ")
                    if (response.isSuccess!!) {
                        val mutable = response.reasons
                        mutable.map { reason -> reason.orderStatusToCorrespondFor = OrderStatus.ON_THE_WAY }
                        dbManager.insertReasons(mutable)
                        Timber.d("insert reasons: ${mutable}")
                        for (reason: Reason in mutable) {
                            if (reason.id == 19) {
                                if ((timerEnterClient * 1000) < reason.timeToShowAfter!!) {
                                    mutable.toMutableList().remove(reason)
                                    break
                                }
                            }
                        }
                        val reasons = dbManager.queryReasonByStatus(order.status!!)
                        allReasons[order.status!!] = reasons
                        switchScreen(REFUSE_ORDER_SCREEN, Triple(order, allReasons, resultData))
                    }
                }, {
                    App.instance.getLogger()!!.log("getNotPaidReasons error ")
                    Timber.e(it)
                })
//
//        } else {
//            allReasons[order.status!!] = reasons
//            switchScreen(REFUSE_ORDER_SCREEN, Triple(order, allReasons, resultData))
//        }
    }

    fun onNavigatorClick(lastLocation: Location, order: Order) {
        when (navigatorType) {
            Constants.GOOGLE_NAV -> {
                performNavigatorAction(lastLocation, context, order, Constants.GOOGLE_NAV_PACKAGE_NAME, Constants.GOOGLE_NAV_MARKET_REDIRECT)
            }
            Constants.YANDEX_NAV -> {
                performNavigatorAction(lastLocation, context, order, Constants.YANDEX_NAV_PACKAGE_NAME, Constants.YANDEX_NAV_MARKET_REDIRECT)
            }
        }
    }

//    fun closeOrder(orderInfo: NewOrderInfo) {
//
//        WeakReference(context).get()!!.stopService(Intent(WeakReference(context).get(), TaxometerService::class.java))
//        WeakReference(context).get()!!.stopService(Intent(WeakReference(context).get(), AutoCloseOrderService::class.java))
//
//        val rateScreenData = Pair<Int, Any>(RatingFragment.MODE_INSTANT_RATE,
//                orderInfo!!)
//        orderManager.finishOrder()
//        setRootScreen(RATING_SCREEN,
//                rateScreenData)
//        lastStepTime = Constants.STEP_TIME_UNSPECIFIED
//
//        val finalization = orderManager.queryFinalization()
//        if (finalization != null) {
//            orderManager.queryFinalization()
//                    ?.let {
//                        val fo =
//                                FinishOrder(longitude = 0.0,
//                                        latitude = 0.0,
//                                        eventTime = System.currentTimeMillis())
//                        it.finishOrder = fo
//                        JobFactory.instance.getJobManager()
//                                ?.addJobInBackground(FinalizeOrderJob(orderId = orderInfo!!.id,
//                                        request = it))
//                    }
//        } else {
//            App.instance.getLogger()!!.log("forceCloseOrder start ")
//            orderInfo?.order?.id?.let {
//                repository.forceCloseOrder(it)
//                        .subscribe({
//                            App.instance.getLogger()!!.log("forceCloseOrder suc ")
//                            Toast.makeText(context,
//                                    R.string.force_close,
//                                    Toast.LENGTH_LONG)
//                                    .show()
//
//                        },
//                                {
//                                    App.instance.getLogger()!!.log("forceCloseOrder error ")
//                                    Toast.makeText(context,
//                                            "Error force close",
//                                            Toast.LENGTH_LONG)
//                                            .show()
//                                })
//            }
//        }
//    }

    fun finishOrder(orderInfo: NewOrderInfo, lastLocation: Location?) {
        Timber.d("finish order")
        disposable += RxBus.listen(DoneOrderEvent::class.java).subscribe {
            Timber.d("listening")
            disposable.dispose()
            resultData = it.resultData
            if (resultData == null) {
                resultData = TaxometerService.result
            }
            if (resultData == null) {
                resultData = dbManager.gueryOrderResult(orderInfo.id)?.resultData
            }

            if (TaxometerService.isAlive)
                TaxometerService.stop(WeakReference(context))

            var cachedCoordinates: List<Coordinate>? = emptyList()
            orderManager.getCoordinates(orderInfo.order!!.id!!)?.let { it1 ->
                cachedCoordinates = it1.coordinates
            }
            if (resultData == null) {
                resultData = ResultDataEvent(
                        resultCost = orderInfo.formula!!.minPrice,
                        factCost = orderInfo.formula!!.minPrice,
                        estimatedCost = orderInfo.formula!!.minPrice,
                        resultF3 = orderInfo.formula!!.minPrice,
                        bonus = 0,
                        estimatedBonus = orderInfo.formula!!.estimatedBonuses?.toInt(),
                        surcharge = Math.round(orderInfo.order!!.payment?.surcharge!!).toInt(),
                        paymentMethod = orderInfo.order!!.payment!!.method!!,
                        changedParams = null,
                        user = orderInfo.order!!.user!!,
                        address = orderInfo.order!!.coordinates!!,
                        factTime = 0,
                        factPath = 0.0, //meters to km
                        waitingTime = 0L,
                        waitingCost = 0.0,
                        currentCoordinate = null,
                        passedRoute = null,
                        orderInfo = orderInfo,
                        isEstimatedCostTaken = false
                )
            }

            val request = TotalCostRequest(
                    factCost = resultData?.factCost,
                    actualCost = resultData?.resultCost,
                    factTime = resultData?.factTime,
                    factPath = resultData?.factPath?.roundTwoDecimal(),
                    longitude = lastLocation?.longitude ?: resultData!!.currentCoordinate?.longitude
                    ?: resultData?.passedRoute?.last()?.latitude
                    ?: cachedCoordinates?.last()?.longitude,
                    latitude = lastLocation?.latitude ?: resultData!!.currentCoordinate?.latitude
                    ?: resultData?.passedRoute?.last()?.latitude
                    ?: cachedCoordinates?.last()?.latitude,
                    route = resultData?.passedRoute ?: cachedCoordinates,
                    eventTime = System.currentTimeMillis(),
                    changedParams = resultData?.changedParams,
                    isEstimatedCostTaken = resultData?.isEstimatedCostTaken,
                    resultF3 = resultData?.resultF3!!,
                    waitingTime = resultData?.waitingTime!!,
                    waitingCost = resultData?.waitingCost!!
            )

            var finalization = orderManager.queryFinalization()
            if (finalization == null) {
                finalization = OrderFinalization()
            }
            finalization.totalCost = TotalCost(
                    factCost = resultData!!.factCost,
                    actualCost = resultData!!.resultCost,
                    factTime = resultData!!.factTime,
                    factPath = resultData!!.factPath?.roundTwoDecimal(),
                    longitude = lastLocation?.longitude ?: resultData!!.currentCoordinate?.longitude
                    ?: resultData?.passedRoute?.last()?.longitude
                    ?: cachedCoordinates?.last()?.longitude,
                    latitude = lastLocation?.latitude ?: resultData!!.currentCoordinate?.latitude
                    ?: resultData?.passedRoute?.last()?.latitude
                    ?: cachedCoordinates?.last()?.latitude,
                    route = resultData?.passedRoute ?: cachedCoordinates,
                    eventTime = System.currentTimeMillis(),
                    changedParams = resultData!!.changedParams?.ordinal,
                    resultF3 = resultData?.resultF3!!,
                    waitingTime = resultData?.waitingTime!!,
                    waitingCost = resultData?.waitingCost!!)

            orderInfo.order?.actualCost = resultData!!.resultCost
            orderInfo.order?.status = TOTAL_COST
            orderManager.updateOrder(orderInfo)
            Timber.d("taxometer order = ${orderInfo.order!!.id} " +
                    "request = ${request.factCost} " +
                    "resultData = ${resultData!!.factCost}")
            try {
                JobFactory.instance.getJobManager()?.addJobInBackground(TotalCostJob(orderInfo.order!!.id!!, request))
                orderManager.insertFinalization(finalization)
            } catch (e: Exception) {
                Timber.d("taxometr order error = ${orderInfo.order!!.id} ")
            } finally {
                setRootScreen(TOTAL_COST_SCREEN, resultData)
            }

        }

        if (resultData == null) {
            resultData = TaxometerService.result
        }
        if (resultData == null) {
//            resultData!!.orderInfo = orderInfo
            resultData = dbManager.gueryOrderResult(orderInfo.id!!)?.resultData
        }
        var cachedCoordinates: List<Coordinate>? = emptyList()
        orderManager.getCoordinates(orderInfo.order!!.id!!)?.let {
            cachedCoordinates = it.coordinates
        }
        var firstLocation = Location("FirstLocation")
        if (resultData?.passedRoute != null && cachedCoordinates!!.isNotEmpty()) {
            firstLocation.latitude = resultData?.passedRoute!!.last().latitude!!
            firstLocation.longitude = resultData?.passedRoute!!.last().longitude!!
        } else if (cachedCoordinates != null && cachedCoordinates!!.isNotEmpty()) {
            firstLocation.latitude = cachedCoordinates?.last()!!.latitude!!
            firstLocation.longitude = cachedCoordinates?.last()!!.longitude!!
        } else {
            var finalization = orderManager.queryFinalization()
            if (finalization != null) {
                firstLocation.latitude = finalization!!.onTheWay!!.latitude
                firstLocation.longitude = finalization!!.onTheWay!!.longitude
            } else {
                firstLocation.latitude = orderInfo.order!!.coordinates!!.first().latitude!!
                firstLocation.longitude = orderInfo.order!!.coordinates!!.first().longitude!!
            }
        }
        if (firstLocation.latitude == 0.0 && firstLocation.longitude == 0.0) {
            if (lastLocation != null) {
                firstLocation = lastLocation
            }
        }

        if (lastLocation != null && click) {
            click = false
            RxBus.publish(CalcFinalPathEvent(firstLocation, lastLocation))
        }
    }

    public fun autoCloseOrder(orderInfo: NewOrderInfo, lastLocation: Location?) {
        if (resultData == null) {
            resultData = TaxometerService.result
        }
        if (resultData == null) {
            resultData = dbManager.gueryOrderResult(orderInfo.id!!)?.resultData
        }


        if (TaxometerService.isAlive)
            TaxometerService.stop(WeakReference(context))

        var cachedCoordinates: List<Coordinate>? = emptyList()
        orderManager.getCoordinates(orderInfo.order!!.id!!)?.let {
            cachedCoordinates = it.coordinates
        }
        if (resultData == null) {
            resultData = ResultDataEvent(
                    resultCost = orderInfo!!.formula!!.minPrice,
                    factCost = orderInfo!!.formula!!.minPrice,
                    estimatedCost = orderInfo!!.formula!!.minPrice,
                    resultF3 = orderInfo!!.formula!!.minPrice,
                    bonus = 0,
                    estimatedBonus = orderInfo!!.formula!!.estimatedBonuses?.toInt(),
                    surcharge = Math.round(orderInfo!!.order!!.payment?.surcharge!!).toInt(),
                    paymentMethod = orderInfo!!.order!!.payment!!.method!!,
                    changedParams = null,
                    user = orderInfo!!.order!!.user!!,
                    address = orderInfo!!.order!!.coordinates!!,
                    factTime = 0,
                    factPath = 0.0, //meters to km
                    waitingTime = 0L,
                    waitingCost = 0.0,
                    currentCoordinate = null,
                    passedRoute = null,
                    orderInfo = orderInfo,
                    isEstimatedCostTaken = false
            )
        }

        val request = TotalCostRequest(
                factCost = resultData?.factCost,
                actualCost = resultData?.resultCost,
                factTime = resultData?.factTime,
                factPath = resultData?.factPath?.roundTwoDecimal(),
                longitude = lastLocation?.longitude ?: resultData!!.currentCoordinate?.longitude
                ?: resultData?.passedRoute?.last()?.latitude
                ?: cachedCoordinates?.last()?.longitude,
                latitude = lastLocation?.latitude ?: resultData!!.currentCoordinate?.latitude
                ?: resultData?.passedRoute?.last()?.latitude
                ?: cachedCoordinates?.last()?.latitude,
                route = resultData?.passedRoute ?: cachedCoordinates,
                eventTime = System.currentTimeMillis(),
                changedParams = resultData?.changedParams,
                isEstimatedCostTaken = resultData?.isEstimatedCostTaken,
                resultF3 = resultData?.resultF3!!,
                waitingTime = resultData?.waitingTime!!,
                waitingCost = resultData?.waitingCost!!
        )

        var finalization = orderManager.queryFinalization()
        if (finalization == null) {
            finalization = OrderFinalization()
        }
        finalization.totalCost = TotalCost(
                factCost = resultData!!.factCost,
                actualCost = resultData!!.resultCost,
                factTime = resultData!!.factTime,
                factPath = resultData!!.factPath?.roundTwoDecimal(),
                longitude = lastLocation?.longitude ?: resultData!!.currentCoordinate?.longitude
                ?: resultData?.passedRoute?.last()?.longitude
                ?: cachedCoordinates?.last()?.longitude,
                latitude = lastLocation?.latitude ?: resultData!!.currentCoordinate?.latitude
                ?: resultData?.passedRoute?.last()?.latitude
                ?: cachedCoordinates?.last()?.latitude,
                route = resultData?.passedRoute ?: cachedCoordinates,
                eventTime = System.currentTimeMillis(),
                changedParams = resultData!!.changedParams?.ordinal,
                resultF3 = resultData?.resultF3!!,
                waitingTime = resultData?.waitingTime!!,
                waitingCost = resultData?.waitingCost!!)

        orderInfo.order?.actualCost = resultData!!.resultCost
        orderInfo.order?.status = TOTAL_COST
        orderManager.updateOrder(orderInfo)
        JobFactory.instance.getJobManager()?.addJobInBackground(TotalCostJob(orderInfo.order!!.id!!, request))
        orderManager.insertFinalization(finalization)
        setRootScreen(TOTAL_COST_SCREEN, resultData)
    }

    private var click: Boolean = true


    fun arrivedAtPlace(id: Int, longitude: Double?, latitude: Double?, eventTime: Long) {
        val reasons = dbManager.queryReasonByStatus(OrderStatus.ARRIVED_AT_PLACE)
        reasons.asSequence().filter { reason -> reason.timeToShowAfter != null }
                .map { reason -> reason.finalTime = System.currentTimeMillis() + reason.timeToShowAfter!! }.toList()

        /*     val orderFinalization : OrderFinalization()
              dbManager.insertFinalization()*/

        val job = ArrivedAtPlaceJob(id, DriverEventRequest(longitude, latitude, eventTime))
        JobFactory.instance.getJobManager()?.addJobInBackground(job)
    }

    fun onTheWay(id: Int, longitude: Double?, latitude: Double?, eventTime: Long) {
        val job = OnTheWayJob(id, DriverEventRequest(longitude, latitude, eventTime))
        JobFactory.instance.getJobManager()?.addJobInBackground(job)
    }

    fun updateOrder(orderInfo: NewOrderInfo, lastLocation: Location?) {
        orderManager.updateOrder(orderInfo)

        var finalization = orderManager.queryFinalization()

        if (finalization == null) {
            finalization = OrderFinalization()
        }

        orderInfo.order?.let {
            when (it.status) {
                ARRIVED_AT_PLACE -> {
                    finalization.arrivedAtPlace = ArrivedAtPlace(
                            longitude = lastLocation?.longitude
                                    ?: locationStorage.getCachedLocation().longitude,
                            latitude = lastLocation?.latitude
                                    ?: locationStorage.getCachedLocation().latitude,
                            eventTime = System.currentTimeMillis())
                }

                ON_THE_WAY -> {
                    finalization.onTheWay = OnTheWay(
                            longitude = lastLocation?.longitude
                                    ?: locationStorage.getCachedLocation().longitude,
                            latitude = lastLocation?.latitude
                                    ?: locationStorage.getCachedLocation().latitude,
                            eventTime = System.currentTimeMillis()
                    )
                }

            }
            orderManager.insertFinalization(finalization)
        }
    }

    fun cancelOrderByCustomer(orderId: Int, location: Location?) {
        Timber.d("cancelOrderByCustomer() id = $orderId")
        RxBus.publish(ShowSecondaryOrderButtonEvent(false))
        if (orderManager.primaryOrderId() == orderId) {  //its primary
            val orderInfo = orderManager.getPrimaryOrder()

            performNextAction(location)
            if (TaxometerService.isAlive)
                TaxometerService.stop(WeakReference(context))
        }
    }

    fun cancelOrderAuto(orderId: Int, location: Location?) {
        Timber.d("cancelOrderByCustomer() id = $orderId")
        RxBus.publish(ShowSecondaryOrderButtonEvent(false))
        if (orderManager.primaryOrderId() == orderId) {  //its primary
            orderManager.deletePrimaryOrder()
            orderManager.deleteSecondaryOrder()
            performNextAction(location)
        }
    }

    fun sendHelpTime(id: Int, type: SendHelpTimeType) {
        JobFactory.instance.getJobManager()?.addJobInBackground(SendHelpTimeJob(id, SendHelpTimeRequest(type)))
    }

    fun isAddressShow() = orderManager.getPrimaryOrder()?.order?.acceptedOrderShowing?.isAddressShow

    private fun performNextAction(lastLocation: Location?) {
        if (orderManager.isSecondaryOrderExist()) {
            setRootScreen(EXECUTE_ORDER_SCREEN, orderManager.getSecondaryOrder())
        } else {
            //just continue on road
            setRootScreen(MAIN_MAP_SCREEN)
        }

        var finalization = orderManager.queryFinalization()
        if (finalization == null) {
            finalization = OrderFinalization()
        }

        finalization.finishOrder = FinishOrder(
                longitude = lastLocation?.longitude
                        ?: locationStorage.getCachedLocation().longitude,
                latitude = lastLocation?.latitude
                        ?: locationStorage.getCachedLocation().latitude,
                eventTime = System.currentTimeMillis()
        )
        orderManager.insertFinalization(finalization)

        orderManager.finishOrder()
    }

    private fun performNavigatorAction(lastLocation: Location, context: Context, order: Order, packageName: String, redirectUri: String) {
        if (isNavigatorInstalled(context, packageName)) {
            if (order.status == ACCEPTED) {
                if (order.coordinates!!.size > 2) {
                    val fakeCoords = order.coordinates!!.toMutableList()
                    fakeCoords.add(0, Address(latitude = lastLocation.latitude, longitude = lastLocation.longitude))
                    val fakeOrder = order.copy()
                    fakeOrder.coordinates = fakeCoords
                    switchToSelectWayPointScreen(fakeOrder)
                } else {
                    showNavigator(Address(longitude = lastLocation.longitude, latitude = lastLocation.latitude), order.coordinates!![0], packageName)
                }
            } else {
                order.coordinates?.let {
                    if (it.size > 2) {
                        switchToSelectWayPointScreen(order)
                    } else {
                        showNavigator(order.coordinates!![0], order.coordinates!![order.coordinates!!.size - 1], packageName)
                    }
                }
            }
        } else {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(redirectUri)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }
    }

    private fun showNavigator(startPoint: Address, finishPoint: Address, packageName: String) {
        val latFrom = startPoint.latitude
        val lonFrom = startPoint.longitude
        val latTo = finishPoint.latitude
        val lonTo = finishPoint.longitude

        val uri = if (packageName == Constants.YANDEX_NAV_PACKAGE_NAME)
            Uri.parse(Constants.YANDEX_NAV_REDIRECT + "lat_from=$latFrom&lon_from=$lonFrom&lat_to=$latTo&lon_to=$lonTo")
        else Uri.parse(Constants.GOOGLE_NAV_REDIRECT + "saddr=$latFrom,$lonFrom&daddr=$latTo,$lonTo")

        val intent = Intent(Intent.ACTION_VIEW, uri)
        intent.`package` = packageName
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(intent)
    }

    private fun switchToSelectWayPointScreen(order: Order) {
        startScreenForResult(SELECT_WAY_POINT_SCREEN, order, ResultListener { it: Any ->
            removeResultListener(Constants.POINT_RESULT_CODE)
            when (navigatorType) {
                Constants.GOOGLE_NAV -> {
                    showNavigator(order.coordinates!![0], order.coordinates!![it as Int], Constants.GOOGLE_NAV_PACKAGE_NAME)
                }
                Constants.YANDEX_NAV -> {
                    showNavigator(order.coordinates!![0], order.coordinates!![it as Int], Constants.YANDEX_NAV_PACKAGE_NAME)
                }
            }
        }, Constants.POINT_RESULT_CODE)
    }

    fun switchEtherScreen() {
        setRootScreen(ETHER_SCREEN)
    }
}