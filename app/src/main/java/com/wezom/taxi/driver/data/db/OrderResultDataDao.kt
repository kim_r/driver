package com.wezom.taxi.driver.data.db

import android.arch.persistence.room.*
import com.wezom.taxi.driver.data.models.OrderResultData


@Dao
interface OrderResultDataDao {

    @Query("SELECT * FROM 'order_result_data' WHERE result_data_id = :id")
    fun queryOrderResultData(id: Int): OrderResultData

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrderResultData(orderResult: OrderResultData): Long

    @Delete
    fun delete(orderResult: OrderResultData)

    @Delete
    fun delete(listOfOrderInfo: List<OrderResultData>): Int

    @Query("DELETE FROM 'order_result_data' WHERE result_data_id = :id")
    fun deleteOrderResultById(id: Long)

    @Query("DELETE FROM 'order_result_data'")
    fun clearOrderResult()

}