package com.wezom.taxi.driver.bus.events

/**
 * Created by zorin.a on 12.06.2018.
 */
class DrawerLockEvent constructor(val isBlocked: Boolean)