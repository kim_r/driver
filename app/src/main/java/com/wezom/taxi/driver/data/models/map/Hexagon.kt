package com.wezom.taxi.driver.data.models.map

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Hexagon(@SerializedName("area") var area: List<Area>) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.createTypedArrayList(Area))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(area)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Hexagon> {
        override fun createFromParcel(parcel: Parcel): Hexagon {
            return Hexagon(parcel)
        }

        override fun newArray(size: Int): Array<Hexagon?> {
            return arrayOfNulls(size)
        }
    }
}