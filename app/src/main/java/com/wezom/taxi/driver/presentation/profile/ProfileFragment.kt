package com.wezom.taxi.driver.presentation.profile

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.*
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.bus.events.UpdateCardEvent
import com.wezom.taxi.driver.common.VERIFICATION_PHONE_SCREEN
import com.wezom.taxi.driver.data.models.*
import com.wezom.taxi.driver.databinding.ProfileBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.ext.hideKeyboard
import com.wezom.taxi.driver.ext.shortToast
import com.wezom.taxi.driver.presentation.base.BaseFragment
import com.wezom.taxi.driver.presentation.profile.ProfilePagerAdapter.Companion.ABOUT_CAR_ENTRY
import com.wezom.taxi.driver.presentation.profile.ProfilePagerAdapter.Companion.ABOUT_USER_ENTRY
import com.wezom.taxi.driver.presentation.profile.ProfilePagerAdapter.Companion.DOCUMENTS_ENTRY
import com.wezom.taxi.driver.presentation.profile.events.CarDetailsEvent
import com.wezom.taxi.driver.presentation.profile.events.ProfileActionClickEvent
import com.wezom.taxi.driver.presentation.profile.events.ProfileActionClickEvent.ActionType.*
import com.wezom.taxi.driver.presentation.profile.events.UserDetailsEvent
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber

/**
 * Created by zorin.a on 28.02.2018.
 */

class ProfileFragment : BaseFragment() {
    //region var
    private lateinit var binding: ProfileBinding
    private lateinit var viewModel: ProfileViewModel
    private lateinit var adapter: ProfilePagerAdapter
    private var user: User? = null
    private var userCorrectness: UserRequisitesCorrectness? = null
    private var car: Car? = null
    private var type: ArrayList<Int>? = null
    private var carCorrectness: CarRequisitesCorrectness? = null
    private var photos: Photos? = null
    private var mode: Int = 0
    private val SEPARATOR = ", "
    private var pagerPosition: Int = 0
    private val pageChangeListener: ViewPager.OnPageChangeListener by lazy {
        object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
                Timber.d("PAGER: onPageScrollStateChanged: state $state")
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                Timber.d("PAGER: onPageScrolled: position $position")
                pagerPosition = position
            }

            override fun onPageSelected(position: Int) {
                Timber.d("PAGER: onPageSelected: position $position")
                activity?.hideKeyboard()
            }
        }
    }

    //endregion

    //region override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        setBackPressFun {
            viewModel.switchScreen(VERIFICATION_PHONE_SCREEN)
        }
        mode = arguments?.getInt(MODE_KEY)!!
        Timber.d("PROFILE MODE $mode")
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()

        viewModel.drawerLockLiveData.observe(this@ProfileFragment, Observer { locked ->
            Timber.d("isLocked profile observer: $locked")
            lockDrawer(locked!!)
        })

        if (mode == MODE_PROFILE || mode == MODE_MODERATION || mode == MODE_NEED_COMPLETE) {
            viewModel.getProfile()
        }

        setupObservers()
        sharedListeners()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = ProfileBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        when (mode) {
            MODE_PROFILE -> setAsProfile()
            MODE_MODERATION -> setAsModeration()
            MODE_NEED_COMPLETE -> setAsProfile()
        }
        binding.stateView.isOnlyDialogMode = true
    }

    //endregion

    //region fun

    private fun performSaveActionClick() {
        if (pagerPosition == 0 || pagerPosition == 1) {
            RxBus.publish(ProfileActionClickEvent(SAVE_USER_DATA))
        } else {
            RxBus.publish(ProfileActionClickEvent(SAVE_IMAGES))
        }
    }

    private fun sharedListeners() {
        disposable += RxBus.listen(ProfileActionClickEvent::class.java).subscribe { action ->
            when (action.actionType) {
                SAVE_USER_DATA -> {
                    if (isValidUserData(user) && isValidCarData(car)) {
                        viewModel.sendUserData(user, car, type)
                    }
                }
                SAVE_IMAGES -> {
                    viewModel.sendImages()
                    Timber.d("PPP SAVE_IMAGES")
                }
            }
        }

        disposable += RxBus.listen(UpdateCardEvent::class.java).subscribe {
            viewModel.getProfileCard()
        }

        disposable += RxBus.listen(UserDetailsEvent::class.java).subscribe { userData ->
            //update user data
            user?.name = userData.name
            user?.surname = userData.surname
            user?.email = userData.mail
            user?.invitationCode = userData.inviteCode
        }

        disposable += RxBus.listen(CarDetailsEvent::class.java).subscribe { action: CarDetailsEvent ->
            car?.brand = action.brand
            car?.model = action.model
            type = action.type
            car?.color = action.color
            car?.number = action.number
            car?.year = action.year
        }
    }

    private fun isValidCarData(car: Car?): Boolean {
        if (car?.brand == null || car.model?.id == null
                || type.isNullOrEmpty()
                || car.color?.id == null || car.year == null ||
                car.number.isNullOrEmpty()) {
            val errorBuilder = StringBuilder()
            errorBuilder.append(getString(R.string.fill_all_user_fields))
            errorBuilder.append(" ")
            if (car?.brand == null) {
                errorBuilder.append(getString(R.string.car_brand))
                errorBuilder.append(SEPARATOR)
            }
            if (car?.model?.id == null) {
                errorBuilder.append(getString(R.string.car_model))
                errorBuilder.append(SEPARATOR)
            }
            if (car?.color?.id == null) {
                errorBuilder.append(getString(R.string.color))
                errorBuilder.append(SEPARATOR)
            }
            if (car?.year == null) {
                errorBuilder.append(getString(R.string.manufacturing_year))
                errorBuilder.append(SEPARATOR)
            }
            if (car?.number.isNullOrEmpty()) {
                errorBuilder.append(getString(R.string.car_number))
                errorBuilder.append(SEPARATOR)
            }
            if (type.isNullOrEmpty()) {
                errorBuilder.append(getString(R.string.type_car))
                errorBuilder.append(SEPARATOR)
            }
            var error = errorBuilder.toString()
            error = error.substring(0, error.length - SEPARATOR.length)
            error.shortToast(context)
            return false
        }
        return true
    }

    private fun isValidUserData(user: User?): Boolean {
        if (user == null || user.name.isNullOrEmpty() || user.surname.isNullOrEmpty()) {
            val errorBuilder = StringBuilder()

            errorBuilder.append(getString(R.string.fill_all_user_fields))
            errorBuilder.append(" ")
            if (user?.name.isNullOrEmpty()) {
                errorBuilder.append(getString(R.string.name))
                errorBuilder.append(SEPARATOR)
            }
            if (user?.surname.isNullOrEmpty()) {
                errorBuilder.append(getString(R.string.surname))
                errorBuilder.append(SEPARATOR)
            }
            var error = errorBuilder.toString()
            error = error.substring(0, error.length - SEPARATOR.length)
            error.shortToast(context)
            return false
        }
        return true
    }

    private fun setupObservers() {
        viewModel.loadingLiveData.observe(this, progressObserver)
        viewModel.profileLiveData.observe(this, Observer { profileAndCorrectness ->
            user = profileAndCorrectness?.user
            car = profileAndCorrectness?.car
            type = profileAndCorrectness?.orderType
            photos = profileAndCorrectness?.photos
            userCorrectness = profileAndCorrectness?.userCorrectness
            carCorrectness = profileAndCorrectness?.carCorrectness
            updateProfile()
        })
    }

    private fun setAsModeration() {
        binding.toolbar.setToolbarTitle(getString(R.string.moderation))
        updateProfile()
    }

    private fun setAsProfile() {
        binding.run {
            toolbar.setToolbarTitle(getString(R.string.profile))
            //this is crunch to handle back press in different states from different fragments
            val drawerToggle = getDrawerToggle()
            enableBackAction(false, drawerToggle)
        }
        updateProfile()
    }

    private fun updateProfile() {
        if (user != null && car != null) {
            binding.run {
                viewPager.offscreenPageLimit = 3
                viewPager.addOnPageChangeListener(pageChangeListener)
                adapter = ProfilePagerAdapter(childFragmentManager)
                adapter.addEntry(ABOUT_USER_ENTRY, AboutUserFragment.newInstance(mode, user, userCorrectness), getString(R.string.about_myself))
                adapter.addEntry(ABOUT_CAR_ENTRY, AboutCarFragment.newInstance(mode, car, carCorrectness, type), getString(R.string.about_car))
                adapter.addEntry(DOCUMENTS_ENTRY, DocumentsFragment.newInstance(mode, photos), getString(R.string.documents))
                viewPager.adapter = adapter
                tabLayout.setupWithViewPager(binding.viewPager)
            }
        }
    }
//endregion

    companion object {
        const val MODE_KEY = "profile_mode"
        const val MODE_PROFILE = 0
        const val MODE_MODERATION = 2
        const val MODE_NEED_COMPLETE = 3

        fun newInstance(mode: Int): ProfileFragment {
            val args = Bundle()
            args.putInt(MODE_KEY, mode)
            val fragment = ProfileFragment()
            fragment.arguments = args
            return fragment
        }
    }
}