package com.wezom.taxi.driver.presentation.discounts.list

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.common.formatDiscountDate
import com.wezom.taxi.driver.common.loadImage
import com.wezom.taxi.driver.data.models.Shares
import com.wezom.taxi.driver.databinding.DiscountItemBinding
import com.wezom.taxi.driver.presentation.base.lists.ItemModel

/**
 * Created by udovik.s on 19.03.2018.
 */
class DiscountsItemView : ConstraintLayout, ItemModel<Shares> {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, attributeSetId: Int) : super(context, attrs, attributeSetId)

    private val binding = DiscountItemBinding.inflate(LayoutInflater.from(context), this, true)

    override fun setData(data: Shares) {
        binding.run {
            if (data.image != null) loadImage(context, logo, data.image) else logo.setImageResource(R.drawable.ic_bonus_ill)
            title.text = data.name
            description.text = data.description
            date.text = formatDiscountDate(data.date)
        }
    }
}