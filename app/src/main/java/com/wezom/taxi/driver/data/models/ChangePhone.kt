package com.wezom.taxi.driver.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by udovik.s on 17.04.2018.
 */
data class ChangePhone(
        @SerializedName("phone") val phone: String?,
        @SerializedName("code") val code: Int?
)