package com.wezom.taxi.driver.presentation.mainmap

import android.arch.lifecycle.MutableLiveData
import android.content.SharedPreferences
import com.wezom.taxi.driver.bus.RxBus
import com.wezom.taxi.driver.common.*
import com.wezom.taxi.driver.data.models.PaymentInfo
import com.wezom.taxi.driver.ext.*
import com.wezom.taxi.driver.net.api.ApiManager
import com.wezom.taxi.driver.presentation.app.App
import com.wezom.taxi.driver.presentation.base.BaseMapViewModel
import com.wezom.taxi.driver.presentation.base.route.ScreenRouterManager
import com.wezom.taxi.driver.presentation.profile.events.UpdateUserDataEvent
import com.wezom.taxi.driver.presentation.profile.events.UpdateUserImageEvent
import com.wezom.taxi.driver.repository.DriverRepository
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

/**
 * Created by zorin.a on 26.02.2018.
 */
class MainMapViewModel @Inject constructor(screenRouterManager: ScreenRouterManager,
                                           sharedPreferences: SharedPreferences,
                                           private val apiManager: ApiManager,
                                           private val repository: DriverRepository) : BaseMapViewModel(screenRouterManager, apiManager, sharedPreferences) {
    //    private var isIntroShowed: Boolean by sharedPreferences.boolean(IS_INTRO_SHOWED)
    private var userRating: String by sharedPreferences.string(USER_RATING_CACHED)
    private var userBalance: String by sharedPreferences.string(key = USER_BALANCE, default = "00.00")
    private var userImageString: String by sharedPreferences.string(USER_IMAGE_URI)
    private var userImageForPrefs: String by sharedPreferences.string(USER_IMAGE_FOR_PREFS)


    //    val introShowedLiveData = MutableLiveData<Boolean>()
    val onIntroDissmissLiveData = MutableLiveData<Any>()
    val userBalanceLiveData = MutableLiveData<String>()
    val addCardLiveData: MutableLiveData<PaymentInfo> = MutableLiveData()

    fun addCard() {
        App.instance.getLogger()!!.log("getCardVerificationInfo start ")
        disposable += repository.getCardVerificationInfo()
                .subscribeBy(
                        onSuccess = {
                            App.instance.getLogger()!!.log("getCardVerificationInfo suc ")
                            if (it.isSuccess == true) {
                                addCardLiveData.postValue(it.paymentInfo)
                            }
                        }, onError = {
                    App.instance.getLogger()!!.log("getCardVerificationInfo error")
                }
                )
    }

    fun switchToBalanceScreen() {
        switchScreen(BALANCE_SCREEN)
    }

    fun checkIntroShown() {
//        introShowedLiveData.postValue(isIntroShowed)
    }

    fun checkCurrentBalance() {
        App.instance.getLogger()!!.log("checkCurrentBalance start ")
        disposable += apiManager.checkCurrentBalance().subscribe({ it ->
            App.instance.getLogger()!!.log("checkCurrentBalance suc ")
            if (it!!.isSuccess!!) {
                it.balance?.let {
                    userBalance = it.toString()
                    userBalanceLiveData.value = userBalance
                }
                userBalanceLiveData.postValue(userBalance)
                it.rating?.let {
                    userRating = it.toString()
                    RxBus.publish(UpdateUserDataEvent())
                }
                it.photo?.let {
                    userImageString = it
                    RxBus.publish(UpdateUserImageEvent())
                    userImageForPrefs = it
                }
            }
        }, { App.instance.getLogger()!!.log("checkCurrentBalance end ")})
    }

    fun onSwipeIntroView() {
//        isIntroShowed = true
        onIntroDissmissLiveData.postValue(true)
    }

    fun switchEtherScreen() {
        setRootScreen(ETHER_SCREEN)
    }

}