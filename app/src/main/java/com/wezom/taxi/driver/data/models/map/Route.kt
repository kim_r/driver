package com.wezom.taxi.driver.data.models.map

import com.google.gson.annotations.SerializedName

/**
 * Created by zorin.a on 11.05.2018.
 */
class Route constructor(@SerializedName("overview_polyline")
                       var overviewPolyline: OverviewPolyline? = null)