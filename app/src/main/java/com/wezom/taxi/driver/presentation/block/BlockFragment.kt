package com.wezom.taxi.driver.presentation.block

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView
import com.wezom.taxi.driver.R
import com.wezom.taxi.driver.common.callSupport
import com.wezom.taxi.driver.databinding.BlockBinding
import com.wezom.taxi.driver.ext.getViewModelOfType
import com.wezom.taxi.driver.presentation.base.BaseFragment
import io.reactivex.rxkotlin.plusAssign
import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by udovik.s on 22.03.2018.
 */
class BlockFragment : BaseFragment() {

    //region var
    private lateinit var binding: BlockBinding
    private lateinit var viewModel: BlockViewModel

    private val helpEmailAndPhoneObserver = Observer<Pair<String, String>> { response ->
        callSupport(response!!.first, context!!, response.second)
    }

    private val messageBlockObserver = Observer<String> { response ->
        binding.blockText2.text = response.toString()
    }
    @SuppressLint("SimpleDateFormat")
    private val timeBlockObserver = Observer<String> { response ->
        val originalFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
        val targetFormat = SimpleDateFormat("dd.MM.yyyy")
        val targetFormatHm = SimpleDateFormat("HH:mm")
        val date = originalFormat.parse(response)
        val formattedDate = targetFormat.format(date)
        val formattedDateHm = targetFormatHm.format(date)
        binding.blockText1.text = resources.getString(R.string.block_text_1, formattedDate, formattedDateHm)
    }

    @SuppressLint("SimpleDateFormat")
    private val emptyBlockObserver = Observer<String> { response ->
        binding.blockText1.text = resources.getString(R.string.block_text_4)
    }
    //endregion

    //region override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).getViewModelOfType()
        viewModel.loadingLiveData.observe(this, progressObserver)
        viewModel.helpEmailPhoneLiveData.observe(this, helpEmailAndPhoneObserver)
        viewModel.blockMessageLiveData.observe(this, messageBlockObserver)
        viewModel.blockTimeLiveData.observe(this, timeBlockObserver)
        viewModel.emptyTimeLiveData.observe(this, emptyBlockObserver)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = BlockBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getUserStatus()

        disposable += RxView.clicks(binding.btnCallSupport).subscribe {
            viewModel.getHelpEmailAndPhone()
        }
    }
    //endregion

    // region fun
    //endregion

}