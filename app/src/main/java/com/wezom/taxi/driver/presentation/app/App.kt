package com.wezom.taxi.driver.presentation.app

import android.content.Context
import android.support.multidex.MultiDex
import com.wezom.taxi.driver.BuildConfig
import com.wezom.taxi.driver.injection.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import timber.log.Timber
import com.logentries.logger.AndroidLogger


/**
 * Created by zorin.a on 16.02.2018.
 */

class App : DaggerApplication() {

    private var logger: AndroidLogger? = null

    public fun getLogger(): AndroidLogger? {
        return logger
    }

    override fun onCreate() {
        super.onCreate()
        initTimber()
        registerActivityLifecycleCallbacks(AppLifecycleTracker())
        instance = this
        logger = AndroidLogger.createInstance(applicationContext, false, true, false, null, 0, "f8a11804-d11b-425c-be42-585d8e20eaa3", true)
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    companion object {
        lateinit var instance: App
            private set
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> =
            DaggerAppComponent.builder().bindApplication(this).build()
}