package com.wezom.taxi.driver.presentation.profile.events

import android.net.Uri

/**
 * Created by zorin.a on 19.03.2018.
 */
class UserDetailsEvent constructor(var name: String, var surname: String, var phone: String, var mail: String, var inviteCode: String, var photoUri: Uri?)