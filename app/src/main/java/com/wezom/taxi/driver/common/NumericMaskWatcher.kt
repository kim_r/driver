package com.wezom.taxi.driver.common

import android.text.Editable
import android.text.TextWatcher

class NumericMaskWatcher(private val mask: String,
                         private val sub: Char = '-',
                         private val countryCode: String? = null
) : TextWatcher {

    private var block = false // ignore changes if true
    private var removing = false

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        removing = count == 0
    }

    override fun afterTextChanged(s: Editable?) {
        if (block || removing)
            return

        if (countryCode != null && !s.toString().startsWith(countryCode)) {
            s?.insert(0, countryCode)
        }

        var text = ""
        var formatted = ""

        // remove excess characters
        for (char in s.toString()) {
            if (char.isDigit()) {
                text += char
            }
        }

        for (char in mask) {
            if (text.isEmpty())
                break
            if (char != sub) {
                formatted += char
                val first = text.first()
                if (first == char) {
                    text = text.drop(1)
                }
            } else {
                formatted += text.first()
                text = text.drop(1)
            }
        }

        block = true

        s?.clear()
        s?.append(formatted)

        block = false
    }
}