package com.wezom.taxi.driver.net.response

import com.google.gson.annotations.SerializedName

class PointsOCM {
    @SerializedName("longitude")
    var longitude: Double? = null
    @SerializedName("latitude")
    var latitude: Double? = null

    constructor(longitude: Double?, latitude: Double?) {
        this.longitude = longitude
        this.latitude = latitude
    }

    constructor()

}