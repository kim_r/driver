if (window.Element && !Element.prototype.closest) {
    Element.prototype.closest = 
    function(s) {
        var matches = (this.document || this.ownerDocument).querySelectorAll(s),
            i,
            el = this;
        do {
            i = matches.length;
            while (--i >= 0 && matches.item(i) !== el) {};
        } while ((i < 0) && (el = el.parentElement)); 
        return el;
    };
}

function addClass( classname, element ) {
    var cn = element.className;
    if( cn.indexOf( classname ) != -1 ) {
        return;
    }
    if( cn != '' ) {
        classname = ' '+classname;
    }
    element.className = cn+classname;
}

if (document.forms.length > 0) {
	var _form = document.forms[0];
    var inputs = _form.querySelectorAll('input.form-control');

	for (var i = 0; i < inputs.length; i++) {
		
		var item = inputs[i];

		item.removeAttribute('placeholder');

		if (item.value.length > 0) {
			addClass('is-active', item.closest('.form-group'));
        }

		item.addEventListener('focusin', function(e){
			addClass('is-active', e.target.closest('.form-group'))
		});

		item.addEventListener('focusout', function(e){
			if (e.target.value.length < 1) {
				e.target.closest('.form-group').className = e.target.closest('.form-group').className.replace(new RegExp('(?:^|\\s)'+ 'is-active' + '(?:\\s|$)'), ' ');
	        }
		});
	}

    var fio = document.getElementById('cardpay-cardholder');
    var fioTxt, descr;

    switch (document.documentElement.lang) {
        case 'ua':
        case 'uk':
            fioTxt = 'Ваш ПIБ';
//            descr = '<span>Не забудьте відкрити карту для оплати в інтернеті з відповідним лімітом</span>';
            break;
        case 'en':
            fioTxt = 'Your full name';
//            descr = '<span>Do not forget to open your card payment limit if it is enabled</span>';
            break;
        default:
            fioTxt = 'Ваше ФИО';
//            descr = '<span>Не забудьте открыть карту для оплаты в интернете с соответствующим лимитом</span>';
    }

    fio.closest('.form-group').querySelector('label').textContent = fioTxt;

//    var div = document.createElement('div');
//    addClass('form-group-descr', div);
//    div.innerHTML = descr;
//
//    _form.appendChild(div);
}